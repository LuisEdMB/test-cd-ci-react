export const customTypography = {
    useNextVariants: true, 
    fontSize: 14, 
    fontFamily: [
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
}
export const customPalette = {
    primary: {
        light: '#3394c6',
        main: '#007ab8',
        dark: '#005580', 
        contrastText: '#fff'
    }, 
    secondary:{
        light: '#f6685e',
        main: '#f44336',
        dark: '#aa2e25', 
        contrastText: '#fff'
    },
    background: {
        default: '#eee'
    },
    table:{
        light: '#3394c6',
        main: '#007ab8',
        dark: '#005580', 
        hover: '#007ab89e',
        striped: '#007ab814',
        contrastText: 'white'
    }
}
var add = 0;

export const customOverrides = {
    MuiBadge:{
        badge:{
            fontSize: "0.35em"
        }
    },
    MuiInputLabel: { 
        root: { 
            fontSize: 14 + add, 
        },
    },
    MuiInput: { 
        root: { 
            fontSize: 14 + add, 
        },
    },
    MuiOutlinedInput:{
        input:{
            fontSize: 14 + add, 
            padding:16
        }
    },
    MuiFormLabel:{
        root: {
            fontSize: 12 + add,  
        },
    },
    MuiFormControl:{
        marginNormal:{
            marginTop: 8
        }, 
    },
    MuiMenuItem: {
        root: {
            fontSize: 14, 
            padding: ".2em"
        }, 
    }, 
    MuiButton: {
        root: {
            fontSize: 14 + add, 
        }, 
    }, 
    MuiFormHelperText: {
        root: { 
            marginTop: 2
        }
    }, 
    MuiSvgIcon:{
        fontSizeSmall:{
            fontSize:18 + add, 
        }
    }, 
    MuiTypography: {
        subtitle1: {
            fontSize:14 + add, 
        }
    }, 
    MuiTableRow:{
        head:{
            height:35
        },
        root:{
            height:25
        }
    }, 
    MuiTableCell:{
        body:{
            fontSize:"11px",
            color: "rgba(0, 0, 0, 0.70)"
        }
    },
    MuiToolbar:{
        regular: {
            padding:"0 .5em"
        }
    }, 
    MuiSnackbarContent:{
        root:{
            fontSize:12+ add, 
        }
    }, 
    MuiExpansionPanelSummary:{
        // root:{
        //     padding: "0 12px",
        //     minHeight: 10
        // },
        // content:{
        //     '&$content': {
        //         margin:".5em 0",
        //     },
        // }, 
        // expanded:{
        //     '&$expanded': {
        //         margin:0, 
        //         minHeight:10
        //     },
        // },
        root: {
            backgroundColor: 'rgba(0, 0, 0, .03)',
            borderBottom: '1px solid rgba(0, 0, 0, .125)',
            marginBottom: -1,
            minHeight: 0,
            padding: "0 12px",
            '&$expanded': {
                minHeight: 0,
            },
          },
          content: {
            '&$expanded': {
              margin: '12px 0',
            },
          },
        expanded: {},
    },
    MuiExpansionPanelDetails:{ 
        root:{
            padding: 12
        }
    },
    MuiStepper:{
        root:{
            padding:"12px"
        }
    }, 
    MuiPaper:{
        elevation2: {
            boxShadow: "none"
        }
    }, 
    MuiLinearProgress:{
        root:{
            height: 2
        }
    }
}
