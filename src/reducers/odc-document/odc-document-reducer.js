import {
    ODC_EXPRESS_DOCUMENTS_REQUEST, 
    ODC_EXPRESS_DOCUMENTS_SUCCESS, 
    ODC_EXPRESS_DOCUMENTS_INVALID, 
    ODC_EXPRESS_DOCUMENTS_ERROR,

    ODC_GET_DOCUMENTS_REQUEST, 
    ODC_GET_DOCUMENTS_SUCCESS, 
    ODC_GET_DOCUMENTS_INVALID, 
    ODC_GET_DOCUMENTS_ERROR,

    ODC_DOWNLOAD_DOCUMENT_REQUEST,
    ODC_DOWNLOAD_DOCUMENT_SUCCESS, 
    ODC_DOWNLOAD_DOCUMENT_INVALID, 
    ODC_DOWNLOAD_DOCUMENT_ERROR,
    
    ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_REQUEST,
    ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_SUCCCESS,  
    ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_INVALID, 
    ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_ERROR,

    ODC_REGISTER_PENDING_DOCUMENTS_REQUEST,
    ODC_REGISTER_PENDING_DOCUMENTS_SUCCESS,
    ODC_REGISTER_PENDING_DOCUMENTS_INVALID,
    ODC_REGISTER_PENDING_DOCUMENTS_ERROR
} from '../../actions/odc-document/odc-document';

var stateInitital = { 
    expressDocument:{
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false, 
        extra: null,
    },
    downloadDocument:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null,
    },
    documents:{
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false, 
        extra: null,
    },
    downloadReprintAttachedDocument:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null,
    },
    registerPendingDocuments: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null,
    }
};

const odcDocumentReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case ODC_EXPRESS_DOCUMENTS_REQUEST:
            return {
                ...state,
                expressDocument:{
                    ...state.expressDocument,
                    loading:true
                }
            }
        case ODC_EXPRESS_DOCUMENTS_SUCCESS:
            return { 
                ...state,
                expressDocument:{
                    ...state.expressDocument,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false, 
                    error: null
                }
            }
        case ODC_EXPRESS_DOCUMENTS_INVALID: 
            return {
                ...state,
                expressDocument:{
                    ...state.expressDocument,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case ODC_EXPRESS_DOCUMENTS_ERROR:
            return { 
                ...state,
                expressDocument:{
                    ...state.expressDocument, 
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case ODC_GET_DOCUMENTS_REQUEST:
            return {
                ...state,
                documents:{
                    ...state.documents,
                    loading:true
                }
            }
        case ODC_GET_DOCUMENTS_SUCCESS:
            return { 
                ...state,
                documents:{
                    ...state.documents,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false, 
                    error: null
                }
            }
        case ODC_GET_DOCUMENTS_INVALID: 
            return {
                ...state,
                documents:{
                    ...state.documents,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case ODC_GET_DOCUMENTS_ERROR:
            return { 
                ...state,
                documents:{
                    ...state.documents, 
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case ODC_DOWNLOAD_DOCUMENT_REQUEST:
            return {
                ...state,
                downloadDocument:{
                    ...state.downloadDocument,
                    loading: true, 
                }
            }
        case ODC_DOWNLOAD_DOCUMENT_SUCCESS:
            return { 
                ...state,
                downloadDocument:{
                    ...state.downloadDocument,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false, 
                    error: null,
                }
            }
        case ODC_DOWNLOAD_DOCUMENT_INVALID: 
            return {
                ...state,
                downloadDocument:{
                    ...state.downloadDocument,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_DOWNLOAD_DOCUMENT_ERROR:
            return { 
                ...state,
                downloadDocument:{
                    ...state.downloadDocument, 
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }

        case ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_REQUEST:
            return {
                ...state,
                downloadReprintAttachedDocument:{
                    ...state.downloadReprintAttachedDocument,
                    loading: true, 
                }
            }
        case ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_SUCCCESS:
            return { 
                ...state,
                downloadReprintAttachedDocument:{
                    ...state.downloadReprintAttachedDocument,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false, 
                    error: null,
                }
            }
        case ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_INVALID: 
            return {
                ...state,
                downloadReprintAttachedDocument:{
                    ...state.downloadReprintAttachedDocument,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_ERROR:
            return { 
                ...state,
                downloadReprintAttachedDocument:{
                    ...state.downloadReprintAttachedDocument, 
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_REGISTER_PENDING_DOCUMENTS_REQUEST:
            return {
                ...state,
                registerPendingDocuments: {
                    response: false,
                    success: false,
                    data: null,
                    error: null,
                    loading: true,
                    extra: null
                }
            }
        case ODC_REGISTER_PENDING_DOCUMENTS_SUCCESS:
            return {
                ...state,
                registerPendingDocuments: {
                    response: true,
                    success: true,
                    data: action.data,
                    error: null,
                    loading: false,
                    extra: null
                }
            }
        case ODC_REGISTER_PENDING_DOCUMENTS_INVALID:
            return {
                ...state,
                registerPendingDocuments: {
                    response: true,
                    success: false,
                    data: null,
                    error: action.error,
                    loading: false,
                    extra: null
                }
            }
        case ODC_REGISTER_PENDING_DOCUMENTS_ERROR:
            return {
                ...state,
                registerPendingDocuments: {
                    response: action.response,
                    success: false,
                    data: null,
                    error: action.error,
                    loading: false,
                    extra: null
                }
            }
        default:
            return state;
    }
}
export default odcDocumentReducer;