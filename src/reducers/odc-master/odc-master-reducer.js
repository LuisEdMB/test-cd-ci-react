import {
    // Preload Client
    ODC_MASTER_PRELOAD_CLIENT_REQUEST,
    ODC_MASTER_PRELOAD_CLIENT_SUCCESS,
    ODC_MASTER_PRELOAD_CLIENT_INVALID,
    ODC_MASTER_PRELOAD_CLIENT_ERROR,

    // Preload Additional Client
    ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_REQUEST,
    ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_SUCCESS,
    ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_INVALID,
    ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_ERROR,

    // Register - Update Client
    ODC_MASTER_REGISTER_UPDATE_CLIENT_REQUEST,
    ODC_MASTER_REGISTER_UPDATE_CLIENT_SUCCESS,
    ODC_MASTER_REGISTER_UPDATE_CLIENT_INVALID,
    ODC_MASTER_REGISTER_UPDATE_CLIENT_ERROR,

    // Register Account
    ODC_MASTER_REGISTER_ACCOUNT_REQUEST,
    ODC_MASTER_REGISTER_ACCOUNT_SUCCESS,
    ODC_MASTER_REGISTER_ACCOUNT_INVALID,
    ODC_MASTER_REGISTER_ACCOUNT_ERROR,

    // Register Credit Card
    ODC_MASTER_REGISTER_CREDIT_CARD_REQUEST,
    ODC_MASTER_REGISTER_CREDIT_CARD_SUCCESS,
    ODC_MASTER_REGISTER_CREDIT_CARD_INVALID,
    ODC_MASTER_REGISTER_CREDIT_CARD_ERROR,

    // Register Credit Card for External
    ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_REQUEST,
    ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_SUCCESS,
    ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_INVALID,
    ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_ERROR,

    // Update Blocking Code Credit Card
    ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_REQUEST,
    ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_SUCCESS,
    ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_INVALID,
    ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_ERROR,

    // Update Blocking Code Credit Card for External
    ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_REQUEST,
    ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_SUCCESS,
    ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_INVALID,
    ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_ERROR
} from '../../actions/odc-master/odc-master'

const initialState = {
    clientPreloaded: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    additionalClientPreloaded: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    clientRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    accountRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    creditCardRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    creditCardForExternalRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    blockingCodeCreditCardUpdated: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    blockingCodeCreditCardForExternalUpdated: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    }
}

const odcMasterReducer = (state = initialState, action) => {
    switch (action.type) {
        // Preload Client
        case ODC_MASTER_PRELOAD_CLIENT_REQUEST:
            return {
                ...state,
                clientPreloaded: {
                    ...initialState.clientPreloaded,
                    loading: true
                }
            }
        case ODC_MASTER_PRELOAD_CLIENT_SUCCESS:
            return {
                ...state,
                clientPreloaded: {
                    ...initialState.clientPreloaded,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_MASTER_PRELOAD_CLIENT_INVALID:
            return {
                ...state,
                clientPreloaded: {
                    ...initialState.clientPreloaded,
                    error: action.error,
                    response: true
                }
            }
        case ODC_MASTER_PRELOAD_CLIENT_ERROR:
            return {
                ...state,
                clientPreloaded: {
                    ...initialState.clientPreloaded,
                    response: action.response,
                    error: action.error
                }
            }

        // Preload Additional Client
        case ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_REQUEST:
            return {
                ...state,
                additionalClientPreloaded: {
                    ...initialState.additionalClientPreloaded,
                    loading: true
                }
            }
        case ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_SUCCESS:
            return {
                ...state,
                additionalClientPreloaded: {
                    ...initialState.additionalClientPreloaded,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_INVALID:
            return {
                ...state,
                additionalClientPreloaded: {
                    ...initialState.additionalClientPreloaded,
                    error: action.error,
                    response: true
                }
            }
        case ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_ERROR:
            return {
                ...state,
                additionalClientPreloaded: {
                    ...initialState.additionalClientPreloaded,
                    response: action.response,
                    error: action.error
                }
            }

        // Register - Update Client
        case ODC_MASTER_REGISTER_UPDATE_CLIENT_REQUEST:
            return {
                ...state,
                clientRegistered: {
                    ...initialState.clientRegistered,
                    loading: true
                }
            }
        case ODC_MASTER_REGISTER_UPDATE_CLIENT_SUCCESS:
            return {
                ...state,
                clientRegistered: {
                    ...initialState.clientRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_MASTER_REGISTER_UPDATE_CLIENT_INVALID:
            return {
                ...state,
                clientRegistered: {
                    ...initialState.clientRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_MASTER_REGISTER_UPDATE_CLIENT_ERROR:
            return {
                ...state,
                clientRegistered: {
                    ...initialState.clientRegistered,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Register Account
        case ODC_MASTER_REGISTER_ACCOUNT_REQUEST:
            return {
                ...state,
                accountRegistered: {
                    ...initialState.accountRegistered,
                    loading: true
                }
            }
        case ODC_MASTER_REGISTER_ACCOUNT_SUCCESS:
            return {
                ...state,
                accountRegistered: {
                    ...initialState.accountRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_MASTER_REGISTER_ACCOUNT_INVALID:
            return {
                ...state,
                accountRegistered: {
                    ...initialState.accountRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_MASTER_REGISTER_ACCOUNT_ERROR:
            return {
                ...state,
                accountRegistered: {
                    ...initialState.accountRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Credit Card
        case ODC_MASTER_REGISTER_CREDIT_CARD_REQUEST:
            return {
                ...state,
                creditCardRegistered: {
                    ...initialState.creditCardRegistered,
                    loading: true
                }
            }
        case ODC_MASTER_REGISTER_CREDIT_CARD_SUCCESS:
            return {
                ...state,
                creditCardRegistered: {
                    ...initialState.creditCardRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_MASTER_REGISTER_CREDIT_CARD_INVALID:
            return {
                ...state,
                creditCardRegistered: {
                    ...initialState.creditCardRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_MASTER_REGISTER_CREDIT_CARD_ERROR:
            return {
                ...state,
                creditCardRegistered: {
                    ...initialState.creditCardRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Credit Card for External
        case ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_REQUEST:
            return {
                ...state,
                creditCardForExternalRegistered: {
                    ...initialState.creditCardForExternalRegistered,
                    loading: true
                }
            }
        case ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_SUCCESS:
            return {
                ...state,
                creditCardForExternalRegistered: {
                    ...initialState.creditCardForExternalRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_INVALID:
            return {
                ...state,
                creditCardForExternalRegistered: {
                    ...initialState.creditCardForExternalRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_ERROR:
            return {
                ...state,
                creditCardForExternalRegistered: {
                    ...initialState.creditCardForExternalRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Update Blocking Code Credit Card
        case ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_REQUEST:
            return {
                ...state,
                blockingCodeCreditCardUpdated: {
                    ...initialState.blockingCodeCreditCardUpdated,
                    loading: true
                }
            }
        case ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_SUCCESS:
            return {
                ...state,
                blockingCodeCreditCardUpdated: {
                    ...initialState.blockingCodeCreditCardUpdated,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_INVALID:
            return {
                ...state,
                blockingCodeCreditCardUpdated: {
                    ...initialState.blockingCodeCreditCardUpdated,
                    error: action.error,
                    response: true
                }
            }
        case ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_ERROR:
            return {
                ...state,
                blockingCodeCreditCardUpdated: {
                    ...initialState.blockingCodeCreditCardUpdated,
                    response: action.response,
                    error: action.error
                }
            }

        // Update Blocking Code Credit Card for External
        case ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_REQUEST:
            return {
                ...state,
                blockingCodeCreditCardForExternalUpdated: {
                    ...initialState.blockingCodeCreditCardForExternalUpdated,
                    loading: true
                }
            }
        case ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_SUCCESS:
            return {
                ...state,
                blockingCodeCreditCardForExternalUpdated: {
                    ...initialState.blockingCodeCreditCardForExternalUpdated,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_INVALID:
            return {
                ...state,
                blockingCodeCreditCardForExternalUpdated: {
                    ...initialState.blockingCodeCreditCardForExternalUpdated,
                    error: action.error,
                    response: true
                }
            }
        case ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_ERROR:
            return {
                ...state,
                blockingCodeCreditCardForExternalUpdated: {
                    ...initialState.blockingCodeCreditCardForExternalUpdated,
                    response: action.response,
                    error: action.error
                }
            }
        
        default:
            return state
    }
}

export default odcMasterReducer
