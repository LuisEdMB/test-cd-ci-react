import {
    DISTRICT_REQUEST,
    DISTRICT_SUCCESS,
    DISTRICT_INVALID,
    DISTRICT_ERROR, 
    HOME_ADDRESS_DISTRICT_REQUEST, 
    HOME_ADDRESS_DISTRICT_SUCCESS, 
    HOME_ADDRESS_DISTRICT_INVALID,
    HOME_ADDRESS_DISTRICT_ERROR, 
    WORK_ADDRESS_DISTRICT_REQUEST,
    WORK_ADDRESS_DISTRICT_INVALID,
    WORK_ADDRESS_DISTRICT_SUCCESS, 
    WORK_ADDRESS_DISTRICT_ERROR 
} from '../../actions/ubigeo/district';

var stateInitital = { 
    district:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    }, 
    homeAddressDistrict:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    }, 
    workAddressDistrict:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    }, 
};

const districtReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case DISTRICT_REQUEST:
            return {
                ...state,
                district:{
                    ...state.district, 
                    loading:false
                }
            }
        case DISTRICT_SUCCESS:
            return { 
                ...state,
                district:{
                    ...state.district, 
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case DISTRICT_INVALID: 
            return {
                ...state,
                district:{
                    ...state.district, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }       
        case DISTRICT_ERROR: 
            return {
                ...state,
                district:{
                    ...state.district, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        case HOME_ADDRESS_DISTRICT_REQUEST:
            return {
                ...state,
                homeAddressDistrict:{
                    ...state.homeAddressDistrict, 
                    loading:false
                }
            }
        case HOME_ADDRESS_DISTRICT_SUCCESS:
            return { 
                ...state,
                homeAddressDistrict:{
                    ...state.homeAddressDistrict, 
                    data:action.data,
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case HOME_ADDRESS_DISTRICT_INVALID: 
            return {
                ...state,
                homeAddressDistrict:{
                    ...state.homeAddressDistrict, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        case HOME_ADDRESS_DISTRICT_ERROR: 
            return {
                ...state,
                homeAddressDistrict:{
                    ...state.homeAddressDistrict, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        case WORK_ADDRESS_DISTRICT_REQUEST:
            return {
                ...state,
                workAddressDistrict:{
                    ...state.workAddressDistrict, 
                    loading:false
                }
            }
        case WORK_ADDRESS_DISTRICT_SUCCESS:
            return { 
                ...state,
                workAddressDistrict:{
                    ...state.workAddressDistrict, 
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case WORK_ADDRESS_DISTRICT_INVALID: 
            return {
                ...state,
                workAddressDistrict:{
                    ...state.workAddressDistrict, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        case WORK_ADDRESS_DISTRICT_ERROR: 
            return {
                ...state,
                workAddressDistrict:{
                    ...state.workAddressDistrict, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        default:
            return state;
    }
}
export default districtReducer;