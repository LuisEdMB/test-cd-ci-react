import {
    DEPARTMENT_REQUEST,
    DEPARTMENT_SUCCESS,
    DEPARTMENT_INVALID,
    DEPARTMENT_ERROR, 
} from '../../actions/ubigeo/department';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false, 
};

const departmentReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case DEPARTMENT_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case DEPARTMENT_SUCCESS:
            return {
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case DEPARTMENT_INVALID: 
            return {
                ...state,
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
                
            }
        case DEPARTMENT_ERROR: 
            return {
                ...state,
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
                
            }
        default:
            return state;
    }
}
export default departmentReducer;
