import {
    PROVINCE_REQUEST,
    PROVINCE_SUCCESS,
    PROVINCE_INVALID,
    PROVINCE_ERROR, 
    HOME_ADDRESS_PROVINCE_REQUEST, 
    HOME_ADDRESS_PROVINCE_SUCCESS, 
    HOME_ADDRESS_PROVINCE_INVALID,
    HOME_ADDRESS_PROVINCE_ERROR, 
    WORK_ADDRESS_PROVINCE_REQUEST, 
    WORK_ADDRESS_PROVINCE_SUCCESS, 
    WORK_ADDRESS_PROVINCE_INVALID, 
    WORK_ADDRESS_PROVINCE_ERROR 
} from '../../actions/ubigeo/province';

var stateInitital = { 
    province:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    }, 
    homeAddressProvince:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    }, 
    workAddressProvince:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    }, 
};

const provinceReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case PROVINCE_REQUEST:
            return {
                ...state,
                province:{
                    ...state.province, 
                    loading:true
                }
            }
        case PROVINCE_SUCCESS:
            return { 
                ...state,
                province:{
                    ...state.province, 
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case PROVINCE_INVALID: 
            return {
                ...state,
                province:{
                    ...state.province, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        case PROVINCE_ERROR: 
            return {
                ...state,
                province:{
                    ...state.province, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        case HOME_ADDRESS_PROVINCE_REQUEST:
            return {
                ...state,
                homeAddressProvince:{
                    ...state.homeAddressProvince, 
                    loading:true
                }
            }
        case HOME_ADDRESS_PROVINCE_SUCCESS:
            return { 
                ...state,
                homeAddressProvince:{
                    ...state.homeAddressProvince, 
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case HOME_ADDRESS_PROVINCE_INVALID: 
            return {
                ...state,
                homeAddressProvince:{
                    ...state.homeAddressProvince, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        case HOME_ADDRESS_PROVINCE_ERROR: 
            return {
                ...state,
                homeAddressProvince:{
                    ...state.homeAddressProvince, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        case WORK_ADDRESS_PROVINCE_REQUEST:
            return {
                ...state,
                workAddressProvince:{
                    ...state.workAddressProvince, 
                    loading:true
                }
            }
        case WORK_ADDRESS_PROVINCE_SUCCESS:
            return { 
                ...state,
                workAddressProvince:{
                    ...state.workAddressProvince, 
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case WORK_ADDRESS_PROVINCE_INVALID: 
            return {
                ...state,
                workAddressProvince:{
                    ...state.workAddressProvince, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        case WORK_ADDRESS_PROVINCE_ERROR: 
            return {
                ...state,
                workAddressProvince:{
                    ...state.workAddressProvince, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        default:
            return state;
    }
}
export default provinceReducer;