import {
    // Actions 
    GET_PCT_REQUEST,
    GET_PCT_SUCCESS,
    GET_PCT_INVALID, 
    GET_PCT_ERROR 
} from '../../actions/generic/pct';

var stateInitital = { 
    response:false,
    success:false,
    data:null, 
    error:null,
    loading:false
};

const pctReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case GET_PCT_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case GET_PCT_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            } 
        case GET_PCT_INVALID: 
            return {
                ...state, 
                data:null,
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case GET_PCT_ERROR: 
            return {
                ...state, 
                data:null,
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default pctReducer;
