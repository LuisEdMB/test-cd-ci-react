import {
    BIOMETRIC_ZYTRUST_SERVICE_REQUEST,
    BIOMETRIC_ZYTRUST_SERVICE_SUCCESS,
    BIOMETRIC_ZYTRUST_SERVICE_INVALID,
    BIOMETRIC_ZYTRUST_SERVICE_ERROR, 
    
    VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_REQUEST,
    VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_SUCCESS, 
    VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_INVALID, 
    VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_ERROR

} from '../../actions/generic/zytrust'

var stateInitital = { 
    responseBioZytrustService:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
    },
    validateBioZytrustService:{
        response:false,
        success:false,
        data:{
            online: false,
            message: ""
        }, 
        error:null,
        loading:false, 
    }
};

const zytrustReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case BIOMETRIC_ZYTRUST_SERVICE_REQUEST:
            return {
                ...state,
                responseBioZytrustService:{
                    ...state.responseBioZytrustService, 
                    loading:true, 
                    success:false,
                }
            }
        case BIOMETRIC_ZYTRUST_SERVICE_INVALID: 
            return {
                ...state, 
                responseBioZytrustService:{
                    ...state.responseBioZytrustService,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case BIOMETRIC_ZYTRUST_SERVICE_SUCCESS:
            return { 
                ...state,
                responseBioZytrustService:{
                    ...state.responseBioZytrustService,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    warning:false,
                    retry:false,
                    error:null
                }    
            }
        case BIOMETRIC_ZYTRUST_SERVICE_ERROR: 
            return {
                ...state, 
                responseBioZytrustService:{
                    ...state.responseBioZytrustService,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_REQUEST:
            return {
                ...state,
                validateBioZytrustService:{
                    ...state.validateBioZytrustService, 
                    loading:true, 
                    success:false,
                }
            }
        case VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_INVALID: 
            return {
                ...state, 
                validateBioZytrustService:{
                    ...state.validateBioZytrustService,
                    data: action.data, 
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_SUCCESS:
            return { 
                ...state,
                validateBioZytrustService:{
                    ...state.validateBioZytrustService,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false,
                    error:null
                }    
            }
        case VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_ERROR: 
            return {
                ...state, 
                validateBioZytrustService:{
                    ...state.validateBioZytrustService,
                    data: action.data, 
                    success: false,
                    loading: false,
                    response:action.response,
                    error:action.error,
                }
            }
        default:
            return state;
    }
}

export default zytrustReducer
