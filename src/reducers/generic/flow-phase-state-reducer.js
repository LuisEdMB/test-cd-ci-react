import {
    // Actions 
    GET_REPORT_FLOW_PHASE_STATE_REQUEST,
    GET_REPORT_FLOW_PHASE_STATE_SUCCESS,
    GET_REPORT_FLOW_PHASE_STATE_INVALID,
    GET_REPORT_FLOW_PHASE_STATE_ERROR
} from '../../actions/generic/flow-phase-state';

var stateInitital = { 
    response: false,
    success: false,
    data: [], 
    error: null,
    loading: false
};

const flowPhaseStateReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case GET_REPORT_FLOW_PHASE_STATE_REQUEST:
            return {
                ...state,
                loading: true 
            }
        case GET_REPORT_FLOW_PHASE_STATE_SUCCESS:
            return { 
                ...state,
                data: action.data, 
                success: true,
                response: true,
                loading: false,
                error: null
            } 
        case GET_REPORT_FLOW_PHASE_STATE_INVALID: 
            return {
                ...state, 
                data: [],
                success: false,
                loading: false,
                response: action.response,
                error: action.error,
            }
        case GET_REPORT_FLOW_PHASE_STATE_ERROR: 
            return {
                ...state, 
                data: [],
                success: false,
                loading: false,
                response: action.response,
                error: action.error,
            }
        default:
            return state;
    }
}
export default flowPhaseStateReducer;
