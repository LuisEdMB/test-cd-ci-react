import {
    GET_ENTIDAD_FINANCIERA_REQUEST,
    GET_ENTIDAD_FINANCIERA_SUCCESS,
    GET_ENTIDAD_FINANCIERA_INVALID,
    GET_ENTIDAD_FINANCIERA_ERROR
} from '../../actions/generic/entidad-financiera';

var initialState = { 
    response: false,
    success: false,
    data: null, 
    error: null,
    loading: false
};

const financialEntityReducer = (state = initialState, action) =>
{
    switch(action.type){
        case GET_ENTIDAD_FINANCIERA_REQUEST:
            return {
                ...initialState,
                loading: true
            }
        case GET_ENTIDAD_FINANCIERA_SUCCESS:
            return {
                ...initialState,
                data: action.data,
                success: true,
                response: true
            }
        case GET_ENTIDAD_FINANCIERA_INVALID:
            return {
                ...initialState,
                error: action.error,
                response: true
            }
        case GET_ENTIDAD_FINANCIERA_ERROR:
            return {
                ...initialState,
                response: action.response,
                error: action.error
            }
        default:
            return state;
    }
}

export default financialEntityReducer;