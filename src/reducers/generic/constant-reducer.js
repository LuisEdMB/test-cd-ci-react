import {
    ODC_CONSTANT_REQUEST,
    ODC_CONSTANT_SUCCESS,
    ODC_CONSTANT_INVALID,
    ODC_CONSTANT_ERROR
} from '../../actions/generic/constant';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const constantODCReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case ODC_CONSTANT_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case ODC_CONSTANT_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case ODC_CONSTANT_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case ODC_CONSTANT_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default constantODCReducer;
