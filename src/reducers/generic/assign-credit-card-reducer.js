import {
    // Actions 
    ODC_CREDIT_CARD_ASSIGNMENT_REQUEST, 
    ODC_CREDIT_CARD_ASSIGNMENT_SUCCESS, 
    ODC_CREDIT_CARD_ASSIGNMENT_INVALID,
    ODC_CREDIT_CARD_ASSIGNMENT_ERROR
} from '../../actions/generic/assign-credit-card';

var stateInitital = { 
    response: false,
    success: false,
    data: null, 
    error: null,
    loading: false
};

const assignCreditCardReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case ODC_CREDIT_CARD_ASSIGNMENT_REQUEST:
            return {
                ...state,
                loading: true 
            }
        case ODC_CREDIT_CARD_ASSIGNMENT_SUCCESS:
            return { 
                ...state,
                data: action.data, 
                success: true,
                response: true,
                loading: false,
                error: null
            } 
        case ODC_CREDIT_CARD_ASSIGNMENT_INVALID: 
            return {
                ...state, 
                data: null,
                success: false,
                loading: false,
                response: action.response,
                error: action.error,
            }
        case ODC_CREDIT_CARD_ASSIGNMENT_ERROR: 
            return {
                ...state, 
                data: null,
                success: false,
                loading: false,
                response: action.response,
                error: action.error,
            }
        default:
            return state;
    }
}
export default assignCreditCardReducer;
