import {
    // Actions 
    ODC_BIOMETRIC_ACTIVITY_REQUEST,
    ODC_BIOMETRIC_ACTIVITY_SUCCESS,
    ODC_BIOMETRIC_ACTIVITY_INVALID,
    ODC_BIOMETRIC_ACTIVITY_ERROR,
    ODC_BIOMETRIC_ACTIVITY_ADITIONAL_REQUEST,
    ODC_BIOMETRIC_ACTIVITY_ADITIONAL_SUCCESS,
    ODC_BIOMETRIC_ACTIVITY_ADITIONAL_INVALID,
    ODC_BIOMETRIC_ACTIVITY_ADITIONAL_ERROR,
    ODC_BIOMETRIC_VALIDATE_REQUEST,
    ODC_BIOMETRIC_VALIDATE_SUCCESS,
    ODC_BIOMETRIC_VALIDATE_INVALID,
    ODC_BIOMETRIC_VALIDATE_ERROR,
    ODC_BIOMETRIC_VALIDATE_ADITIONAL_REQUEST,
    ODC_BIOMETRIC_VALIDATE_ADITIONAL_SUCCESS,
    ODC_BIOMETRIC_VALIDATE_ADITIONAL_INVALID,
    ODC_BIOMETRIC_VALIDATE_ADITIONAL_ERROR
} from '../../actions/generic/biometric';

var stateInitital = { 
    biometricActivity:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false,
        extra: null
    },
    biometricActivityAditional:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false,
        extra: null
    },
    biometricValidation:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false,
        extra: null
    },
    biometricValidationAditional:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false,
        extra: null
    }
};

const biometricActivityReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case ODC_BIOMETRIC_ACTIVITY_REQUEST:
            return {
                ...state,
                biometricActivity:{
                    ...state.biometricActivity,
                    loading:true 
                }
            }
        case ODC_BIOMETRIC_ACTIVITY_SUCCESS:
            return { 
                ...state,
                biometricActivity:{
                    ...state.biometricActivity,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false,
                    error: null, 
                    extra: action.extra
                }
            } 
        case ODC_BIOMETRIC_ACTIVITY_INVALID: 
            return {
                ...state, 
                biometricActivity:{
                    ...state.biometricActivity,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_BIOMETRIC_ACTIVITY_ERROR: 
            return {
                ...state, 
                biometricActivity:{
                    ...state.biometricActivity,
                    data: null,
                    extra: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_BIOMETRIC_ACTIVITY_ADITIONAL_REQUEST:
            return {
                ...state,
                biometricActivityAditional:{
                    ...state.biometricActivityAditional,
                    loading:true 
                }
            }
        case ODC_BIOMETRIC_ACTIVITY_ADITIONAL_SUCCESS:
            return { 
                ...state,
                biometricActivityAditional:{
                    ...state.biometricActivityAditional,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false,
                    error: null, 
                    extra: action.extra
                }
            } 
        case ODC_BIOMETRIC_ACTIVITY_ADITIONAL_INVALID: 
            return {
                ...state, 
                biometricActivityAditional:{
                    ...state.biometricActivityAditional,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_BIOMETRIC_ACTIVITY_ADITIONAL_ERROR: 
            return {
                ...state, 
                biometricActivityAditional:{
                    ...state.biometricActivityAditional,
                    data: null,
                    extra: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_BIOMETRIC_VALIDATE_REQUEST:
            return {
                ...state,
                biometricValidation:{
                    ...state.biometricValidation,
                    loading:true 
                }
            }
        case ODC_BIOMETRIC_VALIDATE_SUCCESS:
            return { 
                ...state,
                biometricValidation:{
                    ...state.biometricValidation,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false,
                    error: null, 
                    extra: action.extra
                }
            } 
        case ODC_BIOMETRIC_VALIDATE_INVALID: 
            return {
                ...state, 
                biometricValidation:{
                    ...state.biometricValidation,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_BIOMETRIC_VALIDATE_ERROR: 
            return {
                ...state, 
                biometricValidation:{
                    ...state.biometricValidation,
                    data: null,
                    extra: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_BIOMETRIC_VALIDATE_ADITIONAL_REQUEST:
            return {
                ...state,
                biometricValidationAditional:{
                    ...state.biometricValidationAditional,
                    loading:true 
                }
            }
        case ODC_BIOMETRIC_VALIDATE_ADITIONAL_SUCCESS:
            return { 
                ...state,
                biometricValidationAditional:{
                    ...state.biometricValidationAditional,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false,
                    error: null, 
                    extra: action.extra
                }
            } 
        case ODC_BIOMETRIC_VALIDATE_ADITIONAL_INVALID: 
            return {
                ...state, 
                biometricValidationAditional:{
                    ...state.biometricValidationAditional,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_BIOMETRIC_VALIDATE_ADITIONAL_ERROR: 
            return {
                ...state, 
                biometricValidationAditional:{
                    ...state.biometricValidationAditional,
                    data: null,
                    extra: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        default:
            return state;
    }
}
export default biometricActivityReducer;
