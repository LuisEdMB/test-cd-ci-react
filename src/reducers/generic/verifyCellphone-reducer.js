import {
  SEND_SMS_CODE_REQUEST,
  SEND_SMS_CODE_SUCCESS,
  SEND_SMS_CODE_INVALID,
  SEND_SMS_CODE_ERROR,

  VERIFY_SMS_CODE_REQUEST,
  VERIFY_SMS_CODE_SUCCESS,
  VERIFY_SMS_CODE_INVALID,
  VERIFY_SMS_CODE_ERROR,

  UPDATE_CLIENT_CELLPHONE_REQUEST,
  UPDATE_CLIENT_CELLPHONE_SUCCESS,
  UPDATE_CLIENT_CELLPHONE_INVALID,
  UPDATE_CLIENT_CELLPHONE_ERROR
} from "../../actions/generic/verifyCellphone";

var stateInitital = {
  sendSMSCodeService: {
    response: false,
    success: false,
    data: null,
    error: null,
    loading: false,
  },
  verifySMSCodeService: {
    response: false,
    success: false,
    data: null,
    error: null,
    loading: false,
  },
  updateClientSMSService:{
    response: false,
    success: false,
    data: null,
    error: null,
    loading: false,
  }
};

const verifyCellphoneReducer = (state = stateInitital, action) => {
  switch (action.type) {
    case SEND_SMS_CODE_REQUEST:
      return {
        ...state,
        sendSMSCodeService: {
          ...state.sendSMSCodeService,
          loading: true,
        },
      };
    case SEND_SMS_CODE_INVALID:
      return {
        ...state,
        sendSMSCodeService: {
          ...state.sendSMSCodeService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    case SEND_SMS_CODE_SUCCESS:
      return {
        ...state,
        sendSMSCodeService: {
          ...state.sendSMSCodeService,
          data: action.data,
          success: true,
          response: true,
          loading: false,
          error: null,
          extra: action.extra,
        },
      };
    case SEND_SMS_CODE_ERROR:
      return {
        ...state,
        sendSMSCodeService: {
          ...state.sendSMSCodeService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    case VERIFY_SMS_CODE_REQUEST:
      return {
        ...state,
        verifySMSCodeService: {
          ...state.verifySMSCodeService,
          loading: true,
        },
      };
    case VERIFY_SMS_CODE_INVALID:
      return {
        ...state,
        verifySMSCodeService: {
          ...state.verifySMSCodeService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    case VERIFY_SMS_CODE_SUCCESS:
      return {
        ...state,
        verifySMSCodeService: {
          ...state.verifySMSCodeService,
          data: action.data,
          success: true,
          response: true,
          loading: false,
          error: null,
          extra: action.extra,
        },
      };
    case VERIFY_SMS_CODE_ERROR:
      return {
        ...state,
        verifySMSCodeService: {
          ...state.verifySMSCodeService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    case UPDATE_CLIENT_CELLPHONE_REQUEST:
      return {
        ...state,
        updateClientSMSService: {
          ...state.updateClientSMSService,
          loading: true,
        },
      };
    case UPDATE_CLIENT_CELLPHONE_INVALID:
      return {
        ...state,
        updateClientSMSService: {
          ...state.updateClientSMSService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    case UPDATE_CLIENT_CELLPHONE_SUCCESS:
      return {
        ...state,
        updateClientSMSService: {
          ...state.updateClientSMSService,
          data: action.data,
          success: true,
          response: true,
          loading: false,
          error: null,
          extra: action.extra,
        },
      };
    case UPDATE_CLIENT_CELLPHONE_ERROR:
      return {
        ...state,
        updateClientSMSService: {
          ...state.updateClientSMSService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    default:
      return state;
  }
};

export default verifyCellphoneReducer;
