import {
    // Actions 
    GET_AGENCY_REQUEST,
    GET_AGENCY_SUCCESS,
    GET_AGENCY_INVALID,
    GET_AGENCY_ERROR 
} from '../../actions/generic/agency';

var stateInitital = { 
    response: false,
    success: false,
    data: [], 
    error: null,
    loading: false
};

const agencyReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case GET_AGENCY_REQUEST:
            return {
                ...state,
                loading: true 
            }
        case GET_AGENCY_SUCCESS:
            return { 
                ...state,
                data: action.data, 
                success: true,
                response: true,
                loading: false,
                error: null
            } 
        case GET_AGENCY_INVALID: 
            return {
                ...state, 
                data: [],
                success: false,
                loading: false,
                response: action.response,
                error: action.error,
            }
        case GET_AGENCY_ERROR: 
            return {
                ...state, 
                data: [],
                success: false,
                loading: false,
                response: action.response,
                error: action.error,
            }
        default:
            return state;
    }
}
export default agencyReducer;
