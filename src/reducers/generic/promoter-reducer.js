import {
    GET_PROMOTER_REQUEST, 
    GET_PROMOTER_SUCCESS,  
    GET_PROMOTER_INVALID,  
    GET_PROMOTER_ERROR  
} from '../../actions/generic/promoter';

var stateInitital = { 
    response:false,
    success:false,
    data:null, 
    error:null,
    loading:false
};

const promoterReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case GET_PROMOTER_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case GET_PROMOTER_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case GET_PROMOTER_INVALID: 
            return {
                ...state, 
                data:null,
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }        
        case GET_PROMOTER_ERROR: 
            return {
                ...state, 
                data:null,
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default promoterReducer;