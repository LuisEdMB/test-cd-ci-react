import {
    // Actions 
    GET_PROCESS_REQUEST,
    GET_PROCESS_SUCCESS,
    GET_PROCESS_INVALID,
    GET_PROCESS_ERROR,
    GET_SUBPROCESS_REQUEST,
    GET_SUBPROCESS_SUCCESS,
    GET_SUBPROCESS_INVALID,
    GET_SUBPROCESS_ERROR,

    GET_PROCESS_FLOW_PHASE_STATE_REQUEST,
    GET_PROCESS_FLOW_PHASE_STATE_SUCCESS,
    GET_PROCESS_FLOW_PHASE_STATE_INVALID ,
    GET_PROCESS_FLOW_PHASE_STATE_ERROR 

} from '../../actions/generic/process-subProcess';

var stateInitital = { 
    process:{
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false
    },
    subProcess:{
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false
    },
    flowPhaseState:{
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false
    }
};

const processSubProcessReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case GET_PROCESS_REQUEST:
            return {
                ...state,
                process: {              
                    ...state.process,
                    loading: true 
                }
            }
        case GET_PROCESS_SUCCESS:
            return { 
                ...state,
                process: {
                    ...state.process,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false,
                    error: null
                }
            } 
        case GET_PROCESS_INVALID: 
            return {
                ...state, 
                process: {
                    ...state.process,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case GET_PROCESS_ERROR: 
            return {
                ...state, 
                process: {
                    ...state.process,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case GET_SUBPROCESS_REQUEST:
            return {
                ...state,
                subproceso: {              
                    ...state.subproceso,
                    loading: true 
                }
            }
        case GET_SUBPROCESS_SUCCESS:
            return { 
                ...state,
                subProcess: {
                    ...state.subProcess,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false,
                    error: null
                }
            } 
        case GET_SUBPROCESS_INVALID: 
            return {
                ...state, 
                subProcess: {
                    ...state.subProcess,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case GET_SUBPROCESS_ERROR: 
            return {
                ...state, 
                subProcess: {
                    ...state.subProcess,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }

        case GET_PROCESS_FLOW_PHASE_STATE_REQUEST:
            return {
                ...state,
                flowPhaseState: {              
                    ...state.flowPhaseState,
                    loading: true 
                }
            }
        case GET_PROCESS_FLOW_PHASE_STATE_SUCCESS:
            return { 
                ...state,
                flowPhaseState: {
                    ...state.flowPhaseState,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false,
                    error: null
                }
            } 
        case GET_PROCESS_FLOW_PHASE_STATE_INVALID: 
            return {
                ...state, 
                flowPhaseState: {
                    ...state.flowPhaseState,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case GET_PROCESS_FLOW_PHASE_STATE_ERROR: 
            return {
                ...state, 
                flowPhaseState: {
                    ...state.flowPhaseState,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        default:
            return state;
    }
}
export default processSubProcessReducer;
