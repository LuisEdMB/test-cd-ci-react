import {
  SEND_MAIL_CODE_REQUEST,
  SEND_MAIL_CODE_SUCCESS,
  SEND_MAIL_CODE_INVALID,
  SEND_MAIL_CODE_ERROR,

  VERIFY_MAIL_CODE_REQUEST,
  VERIFY_MAIL_CODE_SUCCESS,
  VERIFY_MAIL_CODE_INVALID,
  VERIFY_MAIL_CODE_ERROR,

  UPDATE_CLIENT_MAIL_REQUEST,
  UPDATE_CLIENT_MAIL_SUCCESS,
  UPDATE_CLIENT_MAIL_INVALID,
  UPDATE_CLIENT_MAIL_ERROR
} from "../../actions/generic/verifyEmail";

var stateInitital = {
  sendMailCodeService: {
    response: false,
    success: false,
    data: null,
    error: null,
    loading: false,
  },
  verifyMailCodeService: {
    response: false,
    success: false,
    data: null,
    error: null,
    loading: false,
  },
  updateClientMailService:{
    response: false,
    success: false,
    data: null,
    error: null,
    loading: false,
  }
};

const verifyEmailReducer = (state = stateInitital, action) => {
  switch (action.type) {
    case SEND_MAIL_CODE_REQUEST:
      return {
        ...state,
        sendMailCodeService: {
          ...state.sendMailCodeService,
          loading: true,
        },
      };
    case SEND_MAIL_CODE_INVALID:
      return {
        ...state,
        sendMailCodeService: {
          ...state.sendMailCodeService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    case SEND_MAIL_CODE_SUCCESS:
      return {
        ...state,
        sendMailCodeService: {
          ...state.sendMailCodeService,
          data: action.data,
          success: true,
          response: true,
          loading: false,
          error: null,
          extra: action.extra,
        },
      };
    case SEND_MAIL_CODE_ERROR:
      return {
        ...state,
        sendMailCodeService: {
          ...state.sendMailCodeService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    case VERIFY_MAIL_CODE_REQUEST:
      return {
        ...state,
        verifyMailCodeService: {
          ...state.verifyMailCodeService,
          loading: true,
        },
      };
    case VERIFY_MAIL_CODE_INVALID:
      return {
        ...state,
        verifyMailCodeService: {
          ...state.verifyMailCodeService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    case VERIFY_MAIL_CODE_SUCCESS:
      return {
        ...state,
        verifyMailCodeService: {
          ...state.verifyMailCodeService,
          data: action.data,
          success: true,
          response: true,
          loading: false,
          error: null,
          extra: action.extra,
        },
      };
    case VERIFY_MAIL_CODE_ERROR:
      return {
        ...state,
        updateClientMailService: {
          ...state.updateClientMailService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    case UPDATE_CLIENT_MAIL_REQUEST:
      return {
        ...state,
        updateClientMailService: {
          ...state.updateClientMailService,
          loading: true,
        },
      };
    case UPDATE_CLIENT_MAIL_INVALID:
      return {
        ...state,
        updateClientMailService: {
          ...state.updateClientMailService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    case UPDATE_CLIENT_MAIL_SUCCESS:
      return {
        ...state,
        updateClientMailService: {
          ...state.updateClientMailService,
          data: action.data,
          success: true,
          response: true,
          loading: false,
          error: null,
          extra: action.extra,
        },
      };
    case UPDATE_CLIENT_MAIL_ERROR:
      return {
        ...state,
        updateClientMailService: {
          ...state.updateClientMailService,
          data: null,
          extra: null,
          success: false,
          loading: false,
          response: action.response,
          error: action.error,
        },
      };
    default:
      return state;
  }
};

export default verifyEmailReducer;
