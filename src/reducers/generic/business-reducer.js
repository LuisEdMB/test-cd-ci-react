import {
    BUSINESS_REQUEST,
    BUSINESS_SUCCESS,
    BUSINESS_INVALID,
    BUSINESS_ERROR,
    BUSINESS_FILTER_REQUEST,
    BUSINESS_FILTER_SUCCESS,
    BUSINESS_FILTER_INVALID,
    BUSINESS_FILTER_ERROR 
} from '../../actions/generic/business';

var stateInitital = { 
    business:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    },
    filteredBusiness:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    }
};

const businessReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case BUSINESS_REQUEST:
            return {
                ...state,
                business:{
                    ...state.business, 
                    loading:true
                }
            }
        case BUSINESS_SUCCESS:
            return { 
                ...state,
                business:{
                    ...state.business, 
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case BUSINESS_INVALID: 
            return {
                ...state, 
                business:{
                    ...state.business, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        case BUSINESS_ERROR: 
            return {
                ...state, 
                business:{
                    ...state.business, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        case BUSINESS_FILTER_REQUEST: 
            return{
                ...state,
                filteredBusiness:{
                    ...state.filteredBusiness, 
                    loading:true
                }
            }
        case BUSINESS_FILTER_INVALID: 
            return {
                ...state,
                filteredBusiness:{
                    ...state.filteredBusiness, 
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case BUSINESS_FILTER_SUCCESS: 
            return {
                ...state,
                filteredBusiness:{
                    ...state.filteredBusiness, 
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case BUSINESS_FILTER_ERROR: 
            return {
                ...state, 
                filteredBusiness:{
                    ...state.filteredBusiness, 
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error
                }
            }
        default:
            return state;
    }
}
export default businessReducer;
