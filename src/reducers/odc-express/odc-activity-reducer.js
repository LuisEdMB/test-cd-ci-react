import {
    ODC_EXPRESS_CANCEL_ACTIVITY_REQUEST,
    ODC_EXPRESS_CANCEL_ACTIVITY_SUCCESS, 
    ODC_EXPRESS_CANCEL_ACTIVITY_INVALID, 
    ODC_EXPRESS_CANCEL_ACTIVITY_ERROR, 
    // Activity - Generic
    ODC_EXPRESS_GENERIC_ACTIVITY_REQUEST,
    ODC_EXPRESS_GENERIC_ACTIVITY_SUCCESS,
    ODC_EXPRESS_GENERIC_ACTIVITY_INVALID,
    ODC_EXPRESS_GENERIC_ACTIVITY_ERROR,
    // Activity - Generic - Simple
    ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_REQUEST, 
    ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_SUCCESS,
    ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_INVALID, 
    ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_ERROR,
    // Activity - Confirmation
    ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_CONFIRMATION_REQUEST, 
    ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_CONFIRMATION_SUCCESS,
    ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_CONFIRMATION_INVALID, 
    ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_CONFIRMATION_ERROR, 
    
} from '../../actions/odc-express/odc-activity';

var stateInitital = { 
    cancelActivity:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null,
    },
    genericActivity:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null,
        next:false
    },
    simpleGenericActivity:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null,
        next:false
    },
    simpleGenericActivityConfirmation:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null,
        next:false
    }
};

const odcActivityReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case ODC_EXPRESS_CANCEL_ACTIVITY_REQUEST:
            return {
                ...state,
                cancelActivity:{
                    ...state.cancelActivity,
                    loading:true, 
                    next:false
                }
            }
        case ODC_EXPRESS_CANCEL_ACTIVITY_SUCCESS:
            return { 
                ...state,
                cancelActivity:{
                    ...state.cancelActivity,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false, 
                    error:null,
                    next:action.next,
                    extra: action.extra,
                }
            }
        case ODC_EXPRESS_CANCEL_ACTIVITY_INVALID: 
            return {
                ...state,
                cancelActivity:{
                    ...state.cancelActivity,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        case ODC_EXPRESS_CANCEL_ACTIVITY_ERROR:
            return { 
                ...state,
                cancelActivity:{
                    ...state.cancelActivity, 
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        case ODC_EXPRESS_GENERIC_ACTIVITY_REQUEST:
            return {
                ...state,
                genericActivity:{
                    ...state.genericActivity,
                    loading:true, 
                    next:false
                }
            }
        case ODC_EXPRESS_GENERIC_ACTIVITY_SUCCESS:
            return { 
                ...state,
                genericActivity:{
                    ...state.genericActivity,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false, 
                    error:null,
                    extra: action.extra,
                    next:action.next
                }
            }
        case ODC_EXPRESS_GENERIC_ACTIVITY_INVALID: 
            return {
                ...state,
                genericActivity:{
                    ...state.genericActivity,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        case ODC_EXPRESS_GENERIC_ACTIVITY_ERROR:
            return { 
                ...state,
                genericActivity:{
                    ...state.genericActivity, 
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        
        case ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_REQUEST:
            return {
                ...state,
                simpleGenericActivity:{
                    ...state.simpleGenericActivity,
                    loading:true, 
                    next:false
                }
            }
        case ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_SUCCESS:
            return { 
                ...state,
                simpleGenericActivity:{
                    ...state.simpleGenericActivity,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false, 
                    error:null,
                    next:action.next,
                    extra: action.extra,
                }
            }
        case ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_INVALID: 
            return {
                ...state,
                simpleGenericActivity:{
                    ...state.simpleGenericActivity,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        case ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_ERROR:
            return { 
                ...state,
                simpleGenericActivity:{
                    ...state.simpleGenericActivity, 
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        //////////////////////////////////////////////
        case ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_CONFIRMATION_REQUEST:
            return {
                ...state,
                simpleGenericActivityConfirmation:{
                    ...state.simpleGenericActivityConfirmation,
                    loading:true, 
                    next:false
                }
            }
        case ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_CONFIRMATION_SUCCESS:
            return { 
                ...state,
                simpleGenericActivityConfirmation:{
                    ...state.simpleGenericActivityConfirmation,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false, 
                    error:null,
                    next:action.next,
                    extra: action.extra,
                }
            }
        case ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_CONFIRMATION_INVALID: 
            return {
                ...state,
                simpleGenericActivityConfirmation:{
                    ...state.simpleGenericActivityConfirmation,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        case ODC_EXPRESS_SIMPLE_GENERIC_ACTIVITY_CONFIRMATION_ERROR:
            return { 
                ...state,
                simpleGenericActivityConfirmation:{
                    ...state.simpleGenericActivityConfirmation, 
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        default:
            return state;
    }
}
export default odcActivityReducer;