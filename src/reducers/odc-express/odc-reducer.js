import {
    ODC_EXPRESS_FIRST_CALL_REQUEST,
    ODC_EXPRESS_FIRST_CALL_SUCCESS,
    ODC_EXPRESS_FIRST_CALL_INVALID,
    ODC_EXPRESS_FIRST_CALL_ERROR, 

    ODC_EXPRESS_SECOND_CALL_REQUEST,
    ODC_EXPRESS_SECOND_CALL_SUCCESS,
    ODC_EXPRESS_SECOND_CALL_INVALID,
    ODC_EXPRESS_SECOND_CALL_ERROR, 

    ODC_EXPRESS_OFFER_SUMMARY_REQUEST,
    ODC_EXPRESS_OFFER_SUMMARY_SUCCESS,
    ODC_EXPRESS_OFFER_SUMMARY_INVALID,
    ODC_EXPRESS_OFFER_SUMMARY_ERROR,

    ODC_EXPRESS_COMMERCIAL_OFFER_REQUEST,
    ODC_EXPRESS_COMMERCIAL_OFFER_SUCCESS,
    ODC_EXPRESS_COMMERCIAL_OFFER_INVALID,
    ODC_EXPRESS_COMMERCIAL_OFFER_ERROR, 

    ODC_EXPRESS_PENDING_EMBOSSING_REQUEST,
    ODC_EXPRESS_PENDING_EMBOSSING_SUCCESS,
    ODC_EXPRESS_PENDING_EMBOSSING_INVALID,
    ODC_EXPRESS_PENDING_EMBOSSING_ERROR, 

    ODC_EXPRESS_PENDING_PROCESSES_REQUEST,
    ODC_EXPRESS_PENDING_PROCESSES_SUCCESS,
    ODC_EXPRESS_PENDING_PROCESSES_INVALID,
    ODC_EXPRESS_PENDING_PROCESSES_ERROR,

    ODC_EXPRESS_CONTINUE_PROCESS_REQUEST,
    ODC_EXPRESS_CONTINUE_PROCESS_SUCCESS,
    ODC_EXPRESS_CONTINUE_PROCESS_INVALID,
    ODC_EXPRESS_CONTINUE_PROCESS_ERROR,

    ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_REQUEST,
    ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_INVALID,
    ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_SUCCESS,
    ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_ERROR,

    ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_REQUEST,
    ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_INVALID,
    ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_SUCCESS,
    ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_ERROR,

    ODC_EXPRESS_CANCEL_PROCESS_REQUEST,
    ODC_EXPRESS_CANCEL_PROCESS_SUCCESS,
    ODC_EXPRESS_CANCEL_PROCESS_INVALID,
    ODC_EXPRESS_CANCEL_PROCESS_ERROR,

    ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_REQUEST,
    ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS,
    ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_INVALID,
    ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_ERROR,

    ODC_EXPRESS_REJECT_PROCESS_REQUEST,
    ODC_EXPRESS_REJECT_PROCESS_SUCCESS,
    ODC_EXPRESS_REJECT_PROCESS_INVALID,
    ODC_EXPRESS_REJECT_PROCESS_ERROR,

    ODC_EXPRESS_PENDING_ACTIVATE_SAE_REQUEST,
    ODC_EXPRESS_PENDING_ACTIVATE_SAE_SUCCESS,
    ODC_EXPRESS_PENDING_ACTIVATE_SAE_INVALID, 
    ODC_EXPRESS_PENDING_ACTIVATE_SAE_ERROR,

    ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_REQUEST,
    ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_SUCCESS,
    ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_INVALID, 
    ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_ERROR,

    ODC_EXPRESS_LOAD_CLIENT_REQUEST,
    ODC_EXPRESS_LOAD_CLIENT_SUCCESS,
    ODC_EXPRESS_LOAD_CLIENT_INVALID,
    ODC_EXPRESS_LOAD_CLIENT_ERROR,

    ODC_EXPRESS_UPDATE_CLIENT_REQUEST,
    ODC_EXPRESS_UPDATE_CLIENT_SUCCESS,
    ODC_EXPRESS_UPDATE_CLIENT_INVALID,
    ODC_EXPRESS_UPDATE_CLIENT_ERROR,

    ODC_EXPRESS_UPDATE_CLIENT_DATA_REQUEST,
    ODC_EXPRESS_UPDATE_CLIENT_DATA_SUCCESS,
    ODC_EXPRESS_UPDATE_CLIENT_DATA_INVALID,
    ODC_EXPRESS_UPDATE_CLIENT_DATA_ERROR,

    ODC_EXPRESS_UPDATE_CLIENT_SAVE_REQUEST,
    ODC_EXPRESS_UPDATE_CLIENT_SAVE_SUCCESS,
    ODC_EXPRESS_UPDATE_CLIENT_SAVE_INVALID,
    ODC_EXPRESS_UPDATE_CLIENT_SAVE_ERROR,

    ODC_EXPRESS_CREATE_BUSINESS_REQUEST,
    ODC_EXPRESS_CREATE_BUSINESS_SUCCESS,
    ODC_EXPRESS_CREATE_BUSINESS_INVALID,
    ODC_EXPRESS_CREATE_BUSINESS_ERROR,

    ODC_EXPRESS_SEARCH_BUSINESS_REQUEST,
    ODC_EXPRESS_SEARCH_BUSINESS_SUCCESS,
    ODC_EXPRESS_SEARCH_BUSINESS_INVALID,
    ODC_EXPRESS_SEARCH_BUSINESS_ERROR,

    ODC_EXPRESS_UPDATE_BUSINESS_REQUEST,
    ODC_EXPRESS_UPDATE_BUSINESS_SUCCESS,
    ODC_EXPRESS_UPDATE_BUSINESS_INVALID,
    ODC_EXPRESS_UPDATE_BUSINESS_ERROR,

    ODC_EXPRESS_DELETE_BUSINESS_REQUEST,
    ODC_EXPRESS_DELETE_BUSINESS_SUCCESS,
    ODC_EXPRESS_DELETE_BUSINESS_INVALID,
    ODC_EXPRESS_DELETE_BUSINESS_ERROR,

    ODC_EXPRESS_CREATE_COMMENT_REQUEST,
    ODC_EXPRESS_CREATE_COMMENT_SUCCESS,
    ODC_EXPRESS_CREATE_COMMENT_INVALID,
    ODC_EXPRESS_CREATE_COMMENT_ERROR,

    ODC_EXPRESS_LOAD_COMMENT_REQUEST,
    ODC_EXPRESS_LOAD_COMMENT_SUCCESS,
    ODC_EXPRESS_LOAD_COMMENT_INVALID,
    ODC_EXPRESS_LOAD_COMMENT_ERROR,

    // Actions - Verify Solicitude is Blocked
    ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_REQUEST,
    ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_SUCCESS,
    ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_INVALID,
    ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_ERROR,

    // Actions - Block Solicitude
    ODC_EXPRESS_BLOCK_SOLICITUDE_REQUEST,
    ODC_EXPRESS_BLOCK_SOLICITUDE_SUCCESS,
    ODC_EXPRESS_BLOCK_SOLICITUDE_INVALID,
    ODC_EXPRESS_BLOCK_SOLICITUDE_ERROR,

    // Actions - Unlock Solicitude
    ODC_EXPRESS_UNLOCK_SOLICITUDE_REQUEST,
    ODC_EXPRESS_UNLOCK_SOLICITUDE_SUCCESS,
    ODC_EXPRESS_UNLOCK_SOLICITUDE_INVALID,
    ODC_EXPRESS_UNLOCK_SOLICITUDE_ERROR

} from '../../actions/odc-express/odc';

var stateInitital = { 
    firstCall:{
        response:false,
        success:false,
        data:{
            client:null, 
            cda:null,
            adn:null
        }, 
        error:null,
        loading:false, 
        extra:null
    },
    secondCall:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
    },
    pendingProcesses:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    }, 
    pendingProcessesActivation:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    },
    pendingProcessesCancellation:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    },
    cancelProcess:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    },
    authorizeActivationProcess:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    },
    process:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
    },
    commercialOffer:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    pendingEmbossing:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null,
    },
    offerSummary:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null
    },
    rejectProcess:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null
    },
    pendingActivateSAE:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    pendingActivateSAEPrototype:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    loadClient: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    UpdateClientQuery: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    UpdateClientQueryData: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    updateClientSave: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    createBusiness: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    SearchBusiness: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    updateBusiness: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    deleteBusiness: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    createComment: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    loadComment: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    solicitudeBlockedVerified: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    solicitudeBlocked: {
        loading: false,
        data: null,
        success: false,
        error: null,
        errorCode: null,
        response: false
    },
    solicitudeUnlocked: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    }
};

const odcReducer = (state = stateInitital, action) =>
{
    switch (action.type) {
        /* First Call */
        case ODC_EXPRESS_FIRST_CALL_REQUEST:
            return {
                ...state,
                firstCall:{
                    ...state.firstCall,
                    loading:true, 
                }
            }
        case ODC_EXPRESS_FIRST_CALL_SUCCESS:
            return { 
                ...state,
                firstCall:{
                    ...state.firstCall,
                    data:action.data, 
                    extra:action.extra,
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_EXPRESS_FIRST_CALL_INVALID: 
            return {
                ...state,
                firstCall:{
                    ...state.firstCall,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_FIRST_CALL_ERROR:
            return { 
                ...state,
                firstCall:{
                    ...state.firstCall, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /* Second Call */
        case ODC_EXPRESS_SECOND_CALL_REQUEST:
            return {
                ...state, 
                secondCall:{
                    ...state.secondCall,
                    loading:true, 
                }
            }
        case ODC_EXPRESS_SECOND_CALL_SUCCESS:
            return { 
                ...state,
                secondCall:{
                    ...state.secondCall,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_EXPRESS_SECOND_CALL_INVALID: 
            return {
                ...state,
                secondCall:{
                    ...state.secondCall,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_SECOND_CALL_ERROR:
            return { 
                ...state,
                secondCall:{
                    ...state.secondCall, 
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_REQUEST:
            return {
                ...state,
                pendingProcesses:{
                    ...state.pendingProcesses,
                    loading:true 
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_SUCCESS:
            return { 
                ...state,
                pendingProcesses:{
                    ...state.pendingProcesses,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_INVALID: 
            return {
                ...state, 
                pendingProcesses:{
                    ...state.pendingProcesses,
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_ERROR: 
            return {
                ...state, 
                pendingProcesses:{
                    ...state.pendingProcesses,
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_CONTINUE_PROCESS_REQUEST:
            return {
                ...state,
                process:{
                    ...state.process,
                    loading:true, 
                }
            }
        case ODC_EXPRESS_CONTINUE_PROCESS_SUCCESS:
            return { 
                ...state,
                process:{
                    ...state.process,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_EXPRESS_CONTINUE_PROCESS_INVALID: 
            return {
                ...state,
                process:{
                    ...state.process,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_CONTINUE_PROCESS_ERROR:
            return { 
                ...state,
                process:{
                    ...state.process, 
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_REQUEST:
            return {
                ...state,
                pendingProcessesActivation:{
                    ...state.pendingProcessesActivation,
                    loading:true 
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_SUCCESS:
            return { 
                ...state,
                pendingProcessesActivation:{
                    ...state.pendingProcessesActivation,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_INVALID: 
            return {
                ...state, 
                pendingProcessesActivation:{
                    ...state.pendingProcessesActivation,
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_ERROR: 
            return {
                ...state, 
                pendingProcessesActivation:{
                    ...state.pendingProcessesActivation,
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_REQUEST:
            return {
                ...state,
                pendingProcessesCancellation:{
                    ...state.pendingProcessesCancellation,
                    loading:true 
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_SUCCESS:
            return { 
                ...state,
                pendingProcessesCancellation:{
                    ...state.pendingProcessesCancellation,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_INVALID: 
            return {
                ...state, 
                pendingProcessesCancellation:{
                    ...state.pendingProcessesCancellation,
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_ERROR: 
            return {
                ...state, 
                pendingProcessesCancellation:{
                    ...state.pendingProcessesCancellation,
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /* Reject Process */
        case ODC_EXPRESS_CANCEL_PROCESS_REQUEST:  
            return {
                ...state,
                cancelProcess:{
                    ...state.cancelProcess,
                    loading:true 
                }
            }
        case ODC_EXPRESS_CANCEL_PROCESS_SUCCESS:  
            return {
                ...state,
                cancelProcess:{
                    ...state.cancelProcess,
                    data: action.data,
                    success: true,
                    response: true,
                    loading: false,
                    error: null 
                }
            }
        case ODC_EXPRESS_CANCEL_PROCESS_INVALID:  
            return {
                ...state, 
                cancelProcess:{
                    ...state.cancelProcess,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_EXPRESS_CANCEL_PROCESS_ERROR:  
            return {
                ...state,
                cancelProcess:{
                    ...state.cancelProcess,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        /* Authorize Activation Process */
        case ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_REQUEST:
            return {
                ...state,
                authorizeActivationProcess:{
                    ...state.authorizeActivationProcess,
                    loading:true 
                }
            }
        case ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS:
            return { 
                ...state,
                authorizeActivationProcess:{
                    ...state.authorizeActivationProcess,
                    data: action.data,
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_INVALID: 
            return {
                ...state, 
                authorizeActivationProcess:{
                    ...state.authorizeActivationProcess,
                    data: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_ERROR: 
            return {
                ...state, 
                authorizeActivationProcess:{
                    ...state.authorizeActivationProcess,
                    data: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /* Reject Process */
        case ODC_EXPRESS_REJECT_PROCESS_REQUEST:  
            return {
                ...state,
                rejectProcess:{
                    ...state.rejectProcess,
                    loading:true 
                }
            }
        case ODC_EXPRESS_REJECT_PROCESS_SUCCESS:  
            return {
                ...state,
                rejectProcess:{
                    ...state.rejectProcess,
                    data: action.data,
                    success: true,
                    response: true,
                    loading: false,
                    error: null 
                }
            }
        case ODC_EXPRESS_REJECT_PROCESS_INVALID:  
            return {
                ...state, 
                rejectProcess:{
                    ...state.rejectProcess,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_EXPRESS_REJECT_PROCESS_ERROR:  
            return {
                ...state,
                rejectProcess:{
                    ...state.rejectProcess,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        /* Offer Summary */
        case ODC_EXPRESS_OFFER_SUMMARY_REQUEST:
            return {
                ...state,
                offerSummary:{
                    ...state.offerSummary,
                    loading:true, 
                }
            }
        case ODC_EXPRESS_OFFER_SUMMARY_SUCCESS:
            return { 
                ...state,
                offerSummary:{
                    ...state.offerSummary,
                    data:action.data, 
                    extra:action.extra,
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_EXPRESS_OFFER_SUMMARY_INVALID: 
            return {
                ...state,
                offerSummary:{
                    ...state.offerSummary,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_OFFER_SUMMARY_ERROR:
            return { 
                ...state,
                offerSummary:{
                    ...state.offerSummary, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }   
        /* Commercial Offer */
        case ODC_EXPRESS_COMMERCIAL_OFFER_REQUEST:
            return {
                ...state,
                commercialOffer:{
                    ...state.commercialOffer,
                    loading:true, 
                }
            }
        case ODC_EXPRESS_COMMERCIAL_OFFER_SUCCESS:
            return { 
                ...state,
                commercialOffer:{
                    ...state.commercialOffer,
                    data:action.data, 
                    extra:action.extra,
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_EXPRESS_COMMERCIAL_OFFER_INVALID: 
            return {
                ...state,
                commercialOffer:{
                    ...state.commercialOffer,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_COMMERCIAL_OFFER_ERROR:
            return { 
                ...state,
                commercialOffer:{
                    ...state.commercialOffer, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /* Pending Embossing */
        case ODC_EXPRESS_PENDING_EMBOSSING_REQUEST:
            return {
                ...state,
                pendingEmbossing:{
                    ...state.pendingEmbossing,
                    loading:true, 
                }
            }
        case ODC_EXPRESS_PENDING_EMBOSSING_SUCCESS:
            return { 
                ...state,
                pendingEmbossing:{
                    ...state.pendingEmbossing,
                    data:action.data, 
                    extra:action.extra,
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_EXPRESS_PENDING_EMBOSSING_INVALID: 
            return {
                ...state,
                pendingEmbossing:{
                    ...state.pendingEmbossing,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_PENDING_EMBOSSING_ERROR:
            return { 
                ...state,
                pendingEmbossing:{
                    ...state.pendingEmbossing, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }

        case ODC_EXPRESS_PENDING_ACTIVATE_SAE_REQUEST:
            return {
                ...state,
                pendingActivateSAE:{
                    ...state.pendingActivateSAE,
                    loading:true 
                }
            }
        case ODC_EXPRESS_PENDING_ACTIVATE_SAE_SUCCESS:
            return { 
                ...state,
                pendingActivateSAE:{
                    ...state.pendingActivateSAE,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_EXPRESS_PENDING_ACTIVATE_SAE_INVALID: 
            return {
                ...state, 
                pendingActivateSAE:{
                    ...state.pendingActivateSAE,
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_EXPRESS_PENDING_ACTIVATE_SAE_ERROR: 
            return {
                ...state, 
                pendingActivateSAE:{
                    ...state.pendingActivateSAE,
                    data:[],
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }

            ////////////////////////////////////////////

            case ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_REQUEST:
                return {
                    ...state,
                    pendingActivateSAEPrototype:{
                        ...state.pendingActivateSAEPrototype,
                        loading:true 
                    }
                }
            case ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_SUCCESS:
                return { 
                    ...state,
                    pendingActivateSAEPrototype:{
                        ...state.pendingActivateSAEPrototype,
                        data:action.data, 
                        success:true,
                        response:true,
                        loading:false,
                        error:null
                    }
                }
            case ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_INVALID: 
                return {
                    ...state, 
                    pendingActivateSAEPrototype:{
                        ...state.pendingActivateSAEPrototype,
                        data:[],
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
            case ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_ERROR: 
                return {
                    ...state, 
                    pendingActivateSAEPrototype:{
                        ...state.pendingActivateSAEPrototype,
                        data:[],
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }    
        /* Load Client */
        case ODC_EXPRESS_LOAD_CLIENT_REQUEST:
            return {
                ...state,
                loadClient:{
                    ...state.loadClient,
                    loading:true 
                }
            }
        case ODC_EXPRESS_LOAD_CLIENT_SUCCESS:
            return { 
                ...state,
                loadClient:{
                    ...state.loadClient,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_EXPRESS_LOAD_CLIENT_INVALID: 
            return {
                ...state, 
                loadClient:{
                    ...state.loadClient,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_EXPRESS_LOAD_CLIENT_ERROR: 
            return {
                ...state, 
                loadClient:{
                    ...state.loadClient,
                    data: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /**Update client search */
        case ODC_EXPRESS_UPDATE_CLIENT_REQUEST:
            return {
                ...state,
                UpdateClientQuery: {
                    ...state.UpdateClientQuery,
                    loading:true
                }
            }
        case ODC_EXPRESS_UPDATE_CLIENT_SUCCESS:
            return {
                ...state,
                UpdateClientQuery: {
                    ...state.UpdateClientQuery,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_EXPRESS_UPDATE_CLIENT_INVALID:
            return {
                ...state, 
                UpdateClientQuery:{
                    ...state.UpdateClientQuery,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_EXPRESS_UPDATE_CLIENT_ERROR:
            return {
                ...state, 
                UpdateClientQuery:{
                    ...state.UpdateClientQuery,
                    data: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
            /**Update client address */
            case ODC_EXPRESS_UPDATE_CLIENT_DATA_REQUEST:
                return {
                    ...state,
                    UpdateClientQueryData: {
                        ...state.UpdateClientQueryData,
                        loading:true
                    }
                }
            case ODC_EXPRESS_UPDATE_CLIENT_DATA_SUCCESS:
                return {
                    ...state,
                    UpdateClientQueryData: {
                        ...state.UpdateClientQueryData,
                        data:action.data, 
                        success:true,
                        response:true,
                        loading:false,
                        error:null
                    }
                }
            case ODC_EXPRESS_UPDATE_CLIENT_DATA_INVALID:
                return {
                    ...state, 
                    UpdateClientQueryData:{
                        ...state.UpdateClientQueryData,
                        data: null,
                        success: false,
                        loading: false,
                        response: action.response,
                        error: action.error,
                    }
                }
            case ODC_EXPRESS_UPDATE_CLIENT_DATA_ERROR:
                return {
                    ...state, 
                    UpdateClientQueryData:{
                        ...state.UpdateClientQueryData,
                        data: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
            /**Save Update client data */
            case ODC_EXPRESS_UPDATE_CLIENT_SAVE_REQUEST:
                return {
                    ...state,
                    updateClientSave: {
                        ...state.updateClientSave,
                        loading:true
                    }
                }
            case ODC_EXPRESS_UPDATE_CLIENT_SAVE_SUCCESS:
                return {
                    ...state,
                    updateClientSave: {
                        ...state.updateClientSave,
                        data:action.data, 
                        success:true,
                        response:true,
                        loading:false,
                        error:null
                    }
                }
            case ODC_EXPRESS_UPDATE_CLIENT_SAVE_INVALID:
                return {
                    ...state, 
                    updateClientSave:{
                        ...state.updateClientSave,
                        data: null,
                        success: false,
                        loading: false,
                        response: action.response,
                        error: action.error,
                    }
                }
            case ODC_EXPRESS_UPDATE_CLIENT_SAVE_ERROR:
                return {
                    ...state, 
                    updateClientSave:{
                        ...state.updateClientSave,
                        data: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
            /**Create business */
            case ODC_EXPRESS_CREATE_BUSINESS_REQUEST:
                return {
                    ...state,
                    createBusiness: {
                        ...state.createBusiness,
                        loading:true
                    }
                }
            case ODC_EXPRESS_CREATE_BUSINESS_SUCCESS:
                return {
                    ...state,
                    createBusiness: {
                        ...state.createBusiness,
                        data:action.data, 
                        success:true,
                        response:true,
                        loading:false,
                        error:null
                    }
                }
            case ODC_EXPRESS_CREATE_BUSINESS_INVALID:
                return {
                    ...state, 
                    createBusiness:{
                        ...state.createBusiness,
                        data: null,
                        success: false,
                        loading: false,
                        response: action.response,
                        error: action.error,
                    }
                }
            case ODC_EXPRESS_CREATE_BUSINESS_ERROR:
                return {
                    ...state, 
                    createBusiness:{
                        ...state.createBusiness,
                        data: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
            /**Search business */
            case ODC_EXPRESS_SEARCH_BUSINESS_REQUEST:
                return {
                    ...state,
                    SearchBusiness: {
                        ...state.SearchBusiness,
                        loading:true
                    }
                }
            case ODC_EXPRESS_SEARCH_BUSINESS_SUCCESS:
                return {
                    ...state,
                    SearchBusiness: {
                        ...state.SearchBusiness,
                        data:action.data, 
                        success:true,
                        response:true,
                        loading:false,
                        error:null
                    }
                }
            case ODC_EXPRESS_SEARCH_BUSINESS_INVALID:
                return {
                    ...state, 
                    SearchBusiness:{
                        ...state.SearchBusiness,
                        data: null,
                        success: false,
                        loading: false,
                        response: action.response,
                        error: action.error,
                    }
                }
            case ODC_EXPRESS_SEARCH_BUSINESS_ERROR:
                return {
                    ...state, 
                    SearchBusiness:{
                        ...state.SearchBusiness,
                        data: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
            /**Update Business */
            case ODC_EXPRESS_UPDATE_BUSINESS_REQUEST:
                return {
                    ...state,
                    updateBusiness: {
                        ...state.updateBusiness,
                        loading:true
                    }
                }
            case ODC_EXPRESS_UPDATE_BUSINESS_SUCCESS:
                return {
                    ...state,
                    updateBusiness: {
                        ...state.updateBusiness,
                        data:action.data, 
                        success:true,
                        response:true,
                        loading:false,
                        error:null
                    }
                }
            case ODC_EXPRESS_UPDATE_BUSINESS_INVALID:
                return {
                    ...state, 
                    updateBusiness:{
                        ...state.updateBusiness,
                        data: null,
                        success: false,
                        loading: false,
                        response: action.response,
                        error: action.error,
                    }
                }
            case ODC_EXPRESS_UPDATE_BUSINESS_ERROR:
                return {
                    ...state, 
                    updateBusiness:{
                        ...state.updateBusiness,
                        data: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
            /**Delete Business */
            case ODC_EXPRESS_DELETE_BUSINESS_REQUEST:
                return {
                    ...state,
                    deleteBusiness: {
                        ...state.deleteBusiness,
                        loading:true
                    }
                }
            case ODC_EXPRESS_DELETE_BUSINESS_SUCCESS:
                return {
                    ...state,
                    deleteBusiness: {
                        ...state.deleteBusiness,
                        data:action.data, 
                        success:true,
                        response:true,
                        loading:false,
                        error:null
                    }
                }
            case ODC_EXPRESS_DELETE_BUSINESS_INVALID:
                return {
                    ...state, 
                    deleteBusiness:{
                        ...state.deleteBusiness,
                        data: null,
                        success: false,
                        loading: false,
                        response: action.response,
                        error: action.error,
                    }
                }
            case ODC_EXPRESS_DELETE_BUSINESS_ERROR:
                return {
                    ...state, 
                    deleteBusiness:{
                        ...state.deleteBusiness,
                        data: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
            /**Create comment */    
            case ODC_EXPRESS_CREATE_COMMENT_REQUEST:
                return {
                    ...state,
                    createComment: {
                        ...state.createComment,
                        loading:true
                    }
                }
            case ODC_EXPRESS_CREATE_COMMENT_SUCCESS:
                return {
                    ...state,
                    createComment: {
                        ...state.createComment,
                        data:action.data, 
                        success:true,
                        response:true,
                        loading:false,
                        error:null
                    }
                }
            case ODC_EXPRESS_CREATE_COMMENT_INVALID:
                return {
                    ...state, 
                    createComment:{
                        ...state.createComment,
                        data: null,
                        success: false,
                        loading: false,
                        response: action.response,
                        error: action.error,
                    }
                }
            case ODC_EXPRESS_CREATE_COMMENT_ERROR:
                return {
                    ...state, 
                    createComment:{
                        ...state.deleteBusiness,
                        data: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
            /**Load Comment */    
            case ODC_EXPRESS_LOAD_COMMENT_REQUEST:
                return {
                    ...state,
                    loadComment: {
                        ...state.loadComment,
                        loading:true
                    }
                }
            case ODC_EXPRESS_LOAD_COMMENT_SUCCESS:
                return {
                    ...state,
                    loadComment: {
                        ...state.loadComment,
                        data:action.data, 
                        success:true,
                        response:true,
                        loading:false,
                        error:null
                    }
                }
            case ODC_EXPRESS_LOAD_COMMENT_INVALID:
                return {
                    ...state, 
                    loadComment:{
                        ...state.loadComment,
                        data: null,
                        success: false,
                        loading: false,
                        response: action.response,
                        error: action.error,
                    }
                }
            case ODC_EXPRESS_LOAD_COMMENT_ERROR:
                return {
                    ...state, 
                    loadComment:{
                        ...state.loadComment,
                        data: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
            // Verify Solicitude is Blocked
            case ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_REQUEST:
                return {
                    ...state,
                    solicitudeBlockedVerified: {
                        ...stateInitital.solicitudeBlockedVerified,
                        loading: true
                    }
                }
            case ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_SUCCESS:
                return {
                    ...state,
                    solicitudeBlockedVerified: {
                        ...stateInitital.solicitudeBlockedVerified,
                        data: action.data,
                        success: true,
                        response: true
                    }
                }
            case ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_INVALID:
                return {
                    ...state,
                    solicitudeBlockedVerified: {
                        ...stateInitital.solicitudeBlockedVerified,
                        error: action.error,
                        response: true
                    }
                }
            case ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_ERROR:
                return {
                    ...state,
                    solicitudeBlockedVerified: {
                        ...stateInitital.solicitudeBlockedVerified,
                        response: action.response,
                        error: action.error
                    }
                }
            // Block Solicitude
            case ODC_EXPRESS_BLOCK_SOLICITUDE_REQUEST:
                return {
                    ...state,
                    solicitudeBlocked: {
                        ...stateInitital.solicitudeBlocked,
                        loading: true
                    }
                }
            case ODC_EXPRESS_BLOCK_SOLICITUDE_SUCCESS:
                return {
                    ...state,
                    solicitudeBlocked: {
                        ...stateInitital.solicitudeBlocked,
                        data: action.data,
                        success: true,
                        response: true
                    }
                }
            case ODC_EXPRESS_BLOCK_SOLICITUDE_INVALID:
                return {
                    ...state,
                    solicitudeBlocked: {
                        ...stateInitital.solicitudeBlocked,
                        error: action.error,
                        errorCode: action.errorCode,
                        response: true
                    }
                }
            case ODC_EXPRESS_BLOCK_SOLICITUDE_ERROR:
                return {
                    ...state,
                    solicitudeBlocked: {
                        ...stateInitital.solicitudeBlocked,
                        response: action.response,
                        error: action.error,
                        errorCode: action.errorCode
                    }
                }
            // Unlock Solicitude
            case ODC_EXPRESS_UNLOCK_SOLICITUDE_REQUEST:
                return {
                    ...state,
                    solicitudeUnlocked: {
                        ...stateInitital.solicitudeUnlocked,
                        loading: true
                    }
                }
            case ODC_EXPRESS_UNLOCK_SOLICITUDE_SUCCESS:
                return {
                    ...state,
                    solicitudeUnlocked: {
                        ...stateInitital.solicitudeUnlocked,
                        data: action.data,
                        success: true,
                        response: true
                    }
                }
            case ODC_EXPRESS_UNLOCK_SOLICITUDE_INVALID:
                return {
                    ...state,
                    solicitudeUnlocked: {
                        ...stateInitital.solicitudeUnlocked,
                        error: action.error,
                        response: true
                    }
                }
            case ODC_EXPRESS_UNLOCK_SOLICITUDE_ERROR:
                return {
                    ...state,
                    solicitudeUnlocked: {
                        ...stateInitital.solicitudeUnlocked,
                        response: action.response,
                        error: action.error
                    }
                }
        default:
            return state;
    }
}
export default odcReducer;