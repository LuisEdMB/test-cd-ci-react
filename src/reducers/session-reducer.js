import { 
    AUTH_USER_LOGIN_REQUEST, 
    AUTH_USER_LOGIN_SUCCESS, 
    AUTH_USER_LOGIN_INVALID,
    AUTH_USER_LOGIN_ERROR,
    AUTH_USER_LOGOUT_REQUEST,
    AUTH_USER_LOGOUT_SUCCESS,
    AUTH_USER_LOGOUT_ERROR,
 } from '../actions/session';

var stateInitital = { 
    response:false,
    success:false,
    data:null, 
    error:null,
    loading:false, 
    authenticated: false
};

const sessionReducer = (state = stateInitital, action) =>
{
    switch (action.type) {
        case AUTH_USER_LOGIN_REQUEST:
            return { 
                ...state,
                loading: true, 
                error:null
            }
        case AUTH_USER_LOGIN_SUCCESS:
            return { 
                ...state,
                data: action.data,
                loading: false,
                response:true,
                success:true,
                authenticated:true,
                error: null
            }
        case AUTH_USER_LOGIN_INVALID: 
            return{
                ...state,
                data:null,
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case AUTH_USER_LOGIN_ERROR: 
            return {
                ...state,
                data: null,
                loading: false,
                authenticated : false,
                response:true,
                success:true,
                error:action.error, 
            }
        case AUTH_USER_LOGOUT_REQUEST:
            return { 
                ...state,
                loading: true
            }
        case AUTH_USER_LOGOUT_SUCCESS:
            return {
                ...state,
                data: null,
                loading: false,
                authenticated : false, 
                error: null
            }
        case AUTH_USER_LOGOUT_ERROR:
            return {
                ...state,
                loading: false,
                authenticated : true, 
                error: action.error, 
                response:action.response,
            }
       
        
        default:
            return state;
    }
}
export default sessionReducer;