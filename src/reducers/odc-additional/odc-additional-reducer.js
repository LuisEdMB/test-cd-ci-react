import {
    // Consult Client
    ODC_ADDITIONAL_CONSULT_CLIENT_REQUEST,
    ODC_ADDITIONAL_CONSULT_CLIENT_SUCCESS,
    ODC_ADDITIONAL_CONSULT_CLIENT_INVALID,
    ODC_ADDITIONAL_CONSULT_CLIENT_ERROR,

    // Validate Additional Client Already Belongs To Holder Client
    ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_REQUEST,
    ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_SUCCESS,
    ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_INVALID,
    ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_ERROR,

    // Register Additional Client
    ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_REQUEST,
    ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_SUCCESS,
    ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_INVALID,
    ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_ERROR,

    // Register Additional Detail
    ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_REQUEST,
    ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_SUCCESS,
    ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_INVALID,
    ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_ERROR,

    // Register Pending Emboss
    ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_REQUEST,
    ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_SUCCESS,
    ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_INVALID,
    ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_ERROR,

    // Register Contingency Process
    ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_REQUEST,
    ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_SUCCESS,
    ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_INVALID,
    ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_ERROR,

    // Register Activity
    ODC_ADDITIONAL_REGISTER_ACTIVITY_REQUEST,
    ODC_ADDITIONAL_REGISTER_ACTIVITY_SUCCESS,
    ODC_ADDITIONAL_REGISTER_ACTIVITY_INVALID,
    ODC_ADDITIONAL_REGISTER_ACTIVITY_ERROR,

    // Cancel Activity
    ODC_ADDITIONAL_CANCEL_ACTIVITY_REQUEST,
    ODC_ADDITIONAL_CANCEL_ACTIVITY_SUCCESS,
    ODC_ADDITIONAL_CANCEL_ACTIVITY_INVALID,
    ODC_ADDITIONAL_CANCEL_ACTIVITY_ERROR,

    // Continue Process
    ODC_ADDITIONAL_CONTINUE_PROCESS_REQUEST,
    ODC_ADDITIONAL_CONTINUE_PROCESS_SUCCESS,
    ODC_ADDITIONAL_CONTINUE_PROCESS_INVALID,
    ODC_ADDITIONAL_CONTINUE_PROCESS_ERROR,

    // Register Continue Process Activity
    ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_REQUEST,
    ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_SUCCESS,
    ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_INVALID,
    ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_ERROR,

    // Get Pending Processes Activation
    ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_REQUEST,
    ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_SUCCESS,
    ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_INVALID,
    ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_ERROR,

    // Authorize Activation Process
    ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_REQUEST,
    ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS,
    ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_INVALID,
    ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_ERROR,

    // Reject Activation Process
    ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_REQUEST,
    ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_SUCCESS,
    ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_INVALID,
    ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_ERROR,

    // Get Pending Processes
    ODC_ADDITIONAL_GET_PENDING_PROCESSES_REQUEST,
    ODC_ADDITIONAL_GET_PENDING_PROCESSES_SUCCESS,
    ODC_ADDITIONAL_GET_PENDING_PROCESSES_INVALID,
    ODC_ADDITIONAL_GET_PENDING_PROCESSES_ERROR,

    // Register Observation Validated
    ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_REQUEST,
    ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_SUCCESS,
    ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_INVALID,
    ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_ERROR
} from '../../actions/odc-additional/odc-additional'

const initialState = {
    clientConsulted: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    additionalClientValidated: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    additionalClientRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    additionalDetailRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    pendingEmbossRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    contingencyProcessRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    activityRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    activityCanceled: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    process: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    processActivityRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    pendingActivationProcesses: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    activationProcessAuthorized: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    activationProcessRejected: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    pendingProcesses: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    observationValidatedRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    }
}

const odcAdditionalReducer = (state = initialState, action) => {
    switch (action.type) {
        // Consult Client
        case ODC_ADDITIONAL_CONSULT_CLIENT_REQUEST:
            return {
                ...state,
                clientConsulted: {
                    ...initialState.clientConsulted,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_CONSULT_CLIENT_SUCCESS:
            return {
                ...state,
                clientConsulted: {
                    ...initialState.clientConsulted,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_CONSULT_CLIENT_INVALID:
            return {
                ...state,
                clientConsulted: {
                    ...initialState.clientConsulted,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_CONSULT_CLIENT_ERROR:
            return {
                ...state,
                clientConsulted: {
                    ...initialState.clientConsulted,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Validate Additional Client Already Belongs To Holder Client
        case ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_REQUEST:
            return {
                ...state,
                additionalClientValidated: {
                    ...initialState.additionalClientValidated,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_SUCCESS:
            return {
                ...state,
                additionalClientValidated: {
                    ...initialState.additionalClientValidated,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_INVALID:
            return {
                ...state,
                additionalClientValidated: {
                    ...initialState.additionalClientValidated,
                    error: action.error,
                    success: action.success,
                    response: true
                }
            }
        case ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_ERROR:
            return {
                ...state,
                additionalClientValidated: {
                    ...initialState.additionalClientValidated,
                    response: action.response,
                    error: action.error,
                    success: action.success
                }
            }

        // Register Additional Client
        case ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_REQUEST:
            return {
                ...state,
                additionalClientRegistered: {
                    ...initialState.additionalClientRegistered,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_SUCCESS:
            return {
                ...state,
                additionalClientRegistered: {
                    ...initialState.additionalClientRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_INVALID:
            return {
                ...state,
                additionalClientRegistered: {
                    ...initialState.additionalClientRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_ERROR:
            return {
                ...state,
                additionalClientRegistered: {
                    ...initialState.additionalClientRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Additional Detail
        case ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_REQUEST:
            return {
                ...state,
                additionalDetailRegistered: {
                    ...initialState.additionalDetailRegistered,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_SUCCESS:
            return {
                ...state,
                additionalDetailRegistered: {
                    ...initialState.additionalDetailRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_INVALID:
            return {
                ...state,
                additionalDetailRegistered: {
                    ...initialState.additionalDetailRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_ERROR:
            return {
                ...state,
                additionalDetailRegistered: {
                    ...initialState.additionalDetailRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Pending Emboss
        case ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_REQUEST:
            return {
                ...state,
                pendingEmbossRegistered: {
                    ...initialState.pendingEmbossRegistered,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_SUCCESS:
            return {
                ...state,
                pendingEmbossRegistered: {
                    ...initialState.pendingEmbossRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_INVALID:
            return {
                ...state,
                pendingEmbossRegistered: {
                    ...initialState.pendingEmbossRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_ERROR:
            return {
                ...state,
                pendingEmbossRegistered: {
                    ...initialState.pendingEmbossRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Contingency Process
        case ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_REQUEST:
            return {
                ...state,
                contingencyProcessRegistered: {
                    ...initialState.contingencyProcessRegistered,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_SUCCESS:
            return {
                ...state,
                contingencyProcessRegistered: {
                    ...initialState.contingencyProcessRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_INVALID:
            return {
                ...state,
                contingencyProcessRegistered: {
                    ...initialState.contingencyProcessRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_ERROR:
            return {
                ...state,
                contingencyProcessRegistered: {
                    ...initialState.contingencyProcessRegistered,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Register Activity
        case ODC_ADDITIONAL_REGISTER_ACTIVITY_REQUEST:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_ACTIVITY_SUCCESS:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_ACTIVITY_INVALID:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_ACTIVITY_ERROR:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Cancel Activity
        case ODC_ADDITIONAL_CANCEL_ACTIVITY_REQUEST:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_CANCEL_ACTIVITY_SUCCESS:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_CANCEL_ACTIVITY_INVALID:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_CANCEL_ACTIVITY_ERROR:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    response: action.response,
                    error: action.error
                }
            }

        // Continue Process
        case ODC_ADDITIONAL_CONTINUE_PROCESS_REQUEST:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_CONTINUE_PROCESS_SUCCESS:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_CONTINUE_PROCESS_INVALID:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_CONTINUE_PROCESS_ERROR:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Continue Process Activity
        case ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_REQUEST:
            return {
                ...state,
                processActivityRegistered: {
                    ...initialState.processActivityRegistered,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_SUCCESS:
            return {
                ...state,
                processActivityRegistered: {
                    ...initialState.processActivityRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_INVALID:
            return {
                ...state,
                processActivityRegistered: {
                    ...initialState.processActivityRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_ERROR:
            return {
                ...state,
                processActivityRegistered: {
                    ...initialState.processActivityRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Get Pending Processes Activation
        case ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_REQUEST:
            return {
                ...state,
                pendingActivationProcesses: {
                    ...initialState.pendingActivationProcesses,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_SUCCESS:
            return {
                ...state,
                pendingActivationProcesses: {
                    ...initialState.pendingActivationProcesses,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_INVALID:
            return {
                ...state,
                pendingActivationProcesses: {
                    ...initialState.pendingActivationProcesses,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_ERROR:
            return {
                ...state,
                pendingActivationProcesses: {
                    ...initialState.pendingActivationProcesses,
                    response: action.response,
                    error: action.error
                }
            }

        // Authorize Activation Process
        case ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_REQUEST:
            return {
                ...state,
                activationProcessAuthorized: {
                    ...initialState.activationProcessAuthorized,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS:
            return {
                ...state,
                activationProcessAuthorized: {
                    ...initialState.activationProcessAuthorized,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_INVALID:
            return {
                ...state,
                activationProcessAuthorized: {
                    ...initialState.activationProcessAuthorized,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_ERROR:
            return {
                ...state,
                activationProcessAuthorized: {
                    ...initialState.activationProcessAuthorized,
                    response: action.response,
                    error: action.error
                }
            }

        // Reject Activation Process
        case ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_REQUEST:
            return {
                ...state,
                activationProcessRejected: {
                    ...initialState.activationProcessRejected,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_SUCCESS:
            return {
                ...state,
                activationProcessRejected: {
                    ...initialState.activationProcessRejected,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_INVALID:
            return {
                ...state,
                activationProcessRejected: {
                    ...initialState.activationProcessRejected,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_ERROR:
            return {
                ...state,
                activationProcessRejected: {
                    ...initialState.activationProcessRejected,
                    response: action.response,
                    error: action.error
                }
            }

        // Get Pending Processes
        case ODC_ADDITIONAL_GET_PENDING_PROCESSES_REQUEST:
            return {
                ...state,
                pendingProcesses: {
                    ...initialState.pendingProcesses,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_GET_PENDING_PROCESSES_SUCCESS:
            return {
                ...state,
                pendingProcesses: {
                    ...initialState.pendingProcesses,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_GET_PENDING_PROCESSES_INVALID:
            return {
                ...state,
                pendingProcesses: {
                    ...initialState.pendingProcesses,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_GET_PENDING_PROCESSES_ERROR:
            return {
                ...state,
                pendingProcesses: {
                    ...initialState.pendingProcesses,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Observation Validated
        case ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_REQUEST:
            return {
                ...state,
                observationValidatedRegistered: {
                    ...initialState.observationValidatedRegistered,
                    loading: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_SUCCESS:
            return {
                ...state,
                observationValidatedRegistered: {
                    ...initialState.observationValidatedRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_INVALID:
            return {
                ...state,
                observationValidatedRegistered: {
                    ...initialState.observationValidatedRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_ERROR:
            return {
                ...state,
                observationValidatedRegistered: {
                    ...initialState.observationValidatedRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        default:
            return state
    }
}

export default odcAdditionalReducer