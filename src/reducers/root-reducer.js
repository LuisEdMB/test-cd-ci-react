import { combineReducers } from 'redux';
import sessionReducer from './session-reducer';
//List Value
import academicDegreeReducer from './value-list/academic-degree-reducer';
import accountStatusReducer from './value-list/account-status-reducer';
import countryReducer from './value-list/country-reducer';
import creditCardBinReducer from './value-list/credit-card-bin-reducer';
import creditCardBrandReducer from "./value-list/credit-card-brand-reducer";
import creditCardColorReducer from "./value-list/credit-card-color-reducer";
import economicActivityReducer from './value-list/economic-activity-reducer';
import employmentSituationReducer from './value-list/employment-situation-reducer';
import familyRelationshipReducer from './value-list/family-relationship-reducer';
import genderReducer from './value-list/gender-reducer';
import housingTypeReducer from './value-list/housing-type-reducer';
import identificationDocumentTypeReducer from './value-list/identification-document-type-reducer';
import jobTitleReducer from './value-list/job-title-reducer'; 
import maritalStatusReducer from './value-list/marital-status-reducer';
import mediaReducer from './value-list/media-reducer';
import paymentDateReducer from './value-list/payment-date-reducer'; 
import phaseFlowStateReducer from './value-list/phase-flow-state-reducer';
import reasonEmbossingReducer from './value-list/reason-embossing-reducer';
import reasonReprintReducer from './value-list/reason-reprint-reducer';
import sendCorrespondenceReducer from './value-list/send-correspondence-reducer';
import typeResidenceReducer from './value-list/type-residence-reducer';
import viaReducer from './value-list/via-reducer';
import zoneReducer from './value-list/zone-reducer';
import operationTypeReducer from './value-list/operation-type-reducer';
import productReducer from './value-list/product-reducer';
import disbursementTypeReducer from './value-list/disbursement-type-reducer';
import productColorReducer from './value-list/product-color-reducer'
// Generic
import businessReducer from './generic/business-reducer';
import constantODCReducer from './generic/constant-reducer';
import verifyEmailReducer from './generic/verifyEmail-reducer';
import verifyCellphoneReducer from './generic/verifyCellphone-reducer';
import zytrustReducer from './generic/zytrust-reducer';
import promoterReducer from './generic/promoter-reducer';
import pctReducer from './generic/pct-reducer';
import biometricReducer from './generic/biometric-reducer';
import assignCreditCardReducer from './generic/assign-credit-card-reducer';
import flowPhaseStateReducer from './generic/flow-phase-state-reducer';
import agencyReducer from './generic/agency-reducer';
import processSubProcessReducer from './generic/procesoSubproceso-reducer';
import financialEntityReducer from './generic/entidad-financiera-reducer'
// Ubigeo
import departmentReducer from './ubigeo/department-reducer';
import districtReducer from './ubigeo/district-reducer';
import provinceReducer from './ubigeo/province-reducer';
// PMP 
import pmpReducer from './pmp/pmp-reducer';
import pmpRegularReducer from './pmp/pmp-regular-reducer';
// PCI
import pciReducer from './pci/pci-reducer'
// ODC
import odcActivityReducer from './odc-express/odc-activity-reducer';
import odcReducer from './odc-express/odc-reducer';
import odcRegularActivityReducer from './odc-regular/odc-activity-reducer';
import odcRegularReducer from './odc-regular/odc-regular-reducer'
import odcExistingClientReducer from './odc-existing-client/odc-existing-client-reducer'
// ODC Master
import odcMasterReducer from './odc-master/odc-master-reducer'
// Reprint
import odcReprintReducer from './odc-reprint/odc-reprint-reducer'
// Additional
import odcAdditionalReducer from './odc-additional/odc-additional-reducer';
// Digitization 
import odcDigitizationReducer from './odc-digitization/odc-digitization-reducer';
// Document
import odcDocumentReducer from './odc-document/odc-document-reducer';
// Report
import odcReportReducer from './odc-report/odc-report';

const rootReducer = combineReducers({
    academicDegreeReducer,  
    accountStatusReducer, 
    biometricReducer, 
    businessReducer, 
    constantODCReducer, 
    countryReducer,
    creditCardBinReducer, 
    creditCardBrandReducer,
    creditCardColorReducer,
    departmentReducer, 
    districtReducer, 
    economicActivityReducer, 
    employmentSituationReducer, 
    familyRelationshipReducer,
    genderReducer, 
    housingTypeReducer, 
    identificationDocumentTypeReducer, 
    jobTitleReducer, 
    maritalStatusReducer, 
    mediaReducer, 
    odcActivityReducer, 
    odcReducer,
    odcRegularActivityReducer,
    odcRegularReducer,
    odcExistingClientReducer,
    odcMasterReducer,
    odcReprintReducer,
    odcDocumentReducer,
    paymentDateReducer, 
    pctReducer,
    phaseFlowStateReducer, 
    promoterReducer, 
    provinceReducer, 
    reasonEmbossingReducer, 
    reasonReprintReducer,
    sendCorrespondenceReducer, 
    sessionReducer, 
    typeResidenceReducer, 
    viaReducer, 
    zoneReducer,
    verifyEmailReducer,
    verifyCellphoneReducer,
    zytrustReducer, 
    odcAdditionalReducer,
    pmpReducer,
    pmpRegularReducer,
    pciReducer,
    assignCreditCardReducer,
    odcDigitizationReducer, 
    operationTypeReducer, 
    flowPhaseStateReducer, 
    agencyReducer, 
    odcReportReducer,
    productReducer,
    disbursementTypeReducer,
    productColorReducer,
    processSubProcessReducer,
    financialEntityReducer
});

export default rootReducer;