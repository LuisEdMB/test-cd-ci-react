import {
    ODC_REGULAR_CONSULT_CLIENT_REQUEST,
    ODC_REGULAR_CONSULT_CLIENT_SUCCESS,
    ODC_REGULAR_CONSULT_CLIENT_INVALID,
    ODC_REGULAR_CONSULT_CLIENT_ERROR,

    ODC_REGULAR_CONTINUE_PROCESS_REQUEST,
    ODC_REGULAR_CONTINUE_PROCESS_SUCCESS,
    ODC_REGULAR_CONTINUE_PROCESS_INVALID,
    ODC_REGULAR_CONTINUE_PROCESS_ERROR,

    ODC_REGULAR_LOAD_CLIENT_REQUEST,
    ODC_REGULAR_LOAD_CLIENT_SUCCESS,
    ODC_REGULAR_LOAD_CLIENT_INVALID,
    ODC_REGULAR_LOAD_CLIENT_ERROR,

    ODC_REGULAR_REGISTER_CLIENT_REQUEST,
    ODC_REGULAR_REGISTER_CLIENT_SUCCESS,
    ODC_REGULAR_REGISTER_CLIENT_INVALID,
    ODC_REGULAR_REGISTER_CLIENT_ERROR,

    ODC_REGULAR_VALIDATION_CLIENT_DNI_REQUEST,
    ODC_REGULAR_VALIDATION_CLIENT_DNI_SUCCESS,
    ODC_REGULAR_VALIDATION_CLIENT_DNI_INVALID,
    ODC_REGULAR_VALIDATION_CLIENT_DNI_ERROR,

    PMP_REGULAR_CLIENTE_VALIDA_CUENTA_REQUEST,
    PMP_REGULAR_CLIENTE_VALIDA_CUENTA_SUCCESS,
    PMP_REGULAR_CLIENTE_VALIDA_CUENTA_INVALID,
    PMP_REGULAR_CLIENTE_VALIDA_CUENTA_ERROR,

    ODC_REGULAR_OFFER_SUMMARY_REQUEST,
    ODC_REGULAR_OFFER_SUMMARY_SUCCESS,
    ODC_REGULAR_OFFER_SUMMARY_INVALID,
    ODC_REGULAR_OFFER_SUMMARY_ERROR,

    ODC_REGULAR_COMMERCIAL_OFFER_REQUEST,
    ODC_REGULAR_COMMERCIAL_OFFER_SUCCESS,
    ODC_REGULAR_COMMERCIAL_OFFER_INVALID, 
    ODC_REGULAR_COMMERCIAL_OFFER_ERROR,

    ODC_REGULAR_PENDING_EMBOSSING_REQUEST,
    ODC_REGULAR_PENDING_EMBOSSING_SUCCESS,
    ODC_REGULAR_PENDING_EMBOSSING_INVALID,
    ODC_REGULAR_PENDING_EMBOSSING_ERROR,

    ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_REQUEST,
    ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS,
    ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_INVALID,
    ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_ERROR,

    ODC_REGULAR_REJECT_PROCESS_REQUEST,
    ODC_REGULAR_REJECT_PROCESS_SUCCESS,
    ODC_REGULAR_REJECT_PROCESS_INVALID,
    ODC_REGULAR_REJECT_PROCESS_ERROR,
} from '../../actions/odc-regular/odc-regular';

var stateInitital = { 
    consultClient:{
        response:false,
        success:false,
        data:{
            client:null, 
            firstCall:null,
            secondCall:null,
            adn:null
        }, 
        error:null,
        loading:false, 
        extra:null
    },
    process:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
    },
    commercialOffer:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    pendingEmbossing:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null,
    },
    offerSummary:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null
    },
    loadClient: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    registerClient: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    validationClientDNI: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    validationClientAccount: {
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    authorizeActivationProcess:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    rejectProcess:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
};

const odcRegularReducer = (state = stateInitital, action) =>
{
    switch (action.type) {

        /* Regular Consult Client */
        case ODC_REGULAR_CONSULT_CLIENT_REQUEST:
            return {
                ...state,
                consultClient:{
                    ...state.consultClient,
                    loading:true, 
                }
            }
        case ODC_REGULAR_CONSULT_CLIENT_SUCCESS:
            return { 
                ...state,
                consultClient:{
                    ...state.consultClient,
                    data:action.data, 
                    extra:action.extra,
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_REGULAR_CONSULT_CLIENT_INVALID: 
            return {
                ...state,
                consultClient:{
                    ...state.consultClient,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_REGULAR_CONSULT_CLIENT_ERROR:
            return { 
                ...state,
                consultClient:{
                    ...state.consultClient, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /* Regular Continue Process */
        case ODC_REGULAR_CONTINUE_PROCESS_REQUEST:
            return {
                ...state,
                process:{
                    ...state.process,
                    loading:true, 
                }
            }
        case ODC_REGULAR_CONTINUE_PROCESS_SUCCESS:
            return { 
                ...state,
                process:{
                    ...state.process,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_REGULAR_CONTINUE_PROCESS_INVALID: 
            return {
                ...state,
                process:{
                    ...state.process,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_REGULAR_CONTINUE_PROCESS_ERROR:
            return { 
                ...state,
                process:{
                    ...state.process, 
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /* Regular Load Client */
        case ODC_REGULAR_LOAD_CLIENT_REQUEST:
            return {
                ...state,
                loadClient:{
                    ...state.loadClient,
                    loading:true 
                }
            }
        case ODC_REGULAR_LOAD_CLIENT_SUCCESS:
            return { 
                ...state,
                loadClient:{
                    ...state.loadClient,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_REGULAR_LOAD_CLIENT_INVALID: 
            return {
                ...state, 
                loadClient:{
                    ...state.loadClient,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_REGULAR_LOAD_CLIENT_ERROR: 
            return {
                ...state, 
                loadClient:{
                    ...state.loadClient,
                    data: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /** Regular Update Client Save */
        case ODC_REGULAR_REGISTER_CLIENT_REQUEST:
            return {
                ...state,
                registerClient: {
                    ...state.registerClient,
                    loading:true
                }
            }
        case ODC_REGULAR_REGISTER_CLIENT_SUCCESS:
            return {
                ...state,
                registerClient: {
                    ...state.registerClient,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_REGULAR_REGISTER_CLIENT_INVALID:
            return {
                ...state, 
                registerClient:{
                    ...state.registerClient,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_REGULAR_REGISTER_CLIENT_ERROR:
            return {
                ...state, 
                registerClient:{
                    ...state.registerClient,
                    data: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /**Regular Validation Client DNI */
        case ODC_REGULAR_VALIDATION_CLIENT_DNI_REQUEST:
            return {
                ...state,
                validationClientDNI: {
                    ...state.validationClientDNI,
                    loading:true
                }
            }
        case ODC_REGULAR_VALIDATION_CLIENT_DNI_SUCCESS:
            return {
                ...state,
                validationClientDNI: {
                    ...state.validationClientDNI,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_REGULAR_VALIDATION_CLIENT_DNI_INVALID:
            return {
                ...state, 
                validationClientDNI:{
                    ...state.validationClientDNI,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_REGULAR_VALIDATION_CLIENT_DNI_ERROR:
            return {
                ...state, 
                validationClientDNI:{
                    ...state.validationClientDNI,
                    data: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /**Regular Client Valida Cuenta */
        case PMP_REGULAR_CLIENTE_VALIDA_CUENTA_REQUEST:
            return {
                ...state,
                validationClientAccount: {
                    ...state.validationClientAccount,
                    loading:true
                }
            }
        case PMP_REGULAR_CLIENTE_VALIDA_CUENTA_SUCCESS:
            return {
                ...state,
                validationClientAccount: {
                    ...state.validationClientAccount,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case PMP_REGULAR_CLIENTE_VALIDA_CUENTA_INVALID:
            return {
                ...state, 
                validationClientAccount:{
                    ...state.validationClientAccount,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case PMP_REGULAR_CLIENTE_VALIDA_CUENTA_ERROR:
            return {
                ...state, 
                validationClientAccount:{
                    ...state.validationClientAccount,
                    data: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /* Offer Summary */
        case ODC_REGULAR_OFFER_SUMMARY_REQUEST:
            return {
                ...state,
                offerSummary:{
                    ...state.offerSummary,
                    loading:true, 
                }
            }
        case ODC_REGULAR_OFFER_SUMMARY_SUCCESS:
            return { 
                ...state,
                offerSummary:{
                    ...state.offerSummary,
                    data:action.data, 
                    extra:action.extra,
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_REGULAR_OFFER_SUMMARY_INVALID: 
            return {
                ...state,
                offerSummary:{
                    ...state.offerSummary,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_REGULAR_OFFER_SUMMARY_ERROR:
            return { 
                ...state,
                offerSummary:{
                    ...state.offerSummary, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }   
        /* Commercial Offer */
        case ODC_REGULAR_COMMERCIAL_OFFER_REQUEST:
            return {
                ...state,
                commercialOffer:{
                    ...state.commercialOffer,
                    loading:true, 
                }
            }
        case ODC_REGULAR_COMMERCIAL_OFFER_SUCCESS:
            return { 
                ...state,
                commercialOffer:{
                    ...state.commercialOffer,
                    data:action.data, 
                    extra:action.extra,
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_REGULAR_COMMERCIAL_OFFER_INVALID: 
            return {
                ...state,
                commercialOffer:{
                    ...state.commercialOffer,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_REGULAR_COMMERCIAL_OFFER_ERROR:
            return { 
                ...state,
                commercialOffer:{
                    ...state.commercialOffer, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
            /* Pending Embossing */
        case ODC_REGULAR_PENDING_EMBOSSING_REQUEST:
            return {
                ...state,
                pendingEmbossing:{
                    ...state.pendingEmbossing,
                    loading:true, 
                }
            }
        case ODC_REGULAR_PENDING_EMBOSSING_SUCCESS:
            return { 
                ...state,
                pendingEmbossing:{
                    ...state.pendingEmbossing,
                    data:action.data, 
                    extra:action.extra,
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_REGULAR_PENDING_EMBOSSING_INVALID: 
            return {
                ...state,
                pendingEmbossing:{
                    ...state.pendingEmbossing,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_REGULAR_PENDING_EMBOSSING_ERROR:
            return { 
                ...state,
                pendingEmbossing:{
                    ...state.pendingEmbossing, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }

        /* Authorize Activation Process */
        case ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_REQUEST:
            return {
                ...state,
                authorizeActivationProcess:{
                    ...state.authorizeActivationProcess,
                    loading:true 
                }
            }
        case ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS:
            return { 
                ...state,
                authorizeActivationProcess:{
                    ...state.authorizeActivationProcess,
                    data: action.data,
                    success:true,
                    response:true,
                    loading:false,
                    error:null
                }
            }
        case ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_INVALID: 
            return {
                ...state, 
                authorizeActivationProcess:{
                    ...state.authorizeActivationProcess,
                    data: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_ERROR: 
            return {
                ...state, 
                authorizeActivationProcess:{
                    ...state.authorizeActivationProcess,
                    data: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        /* Reject Process */
        case ODC_REGULAR_REJECT_PROCESS_REQUEST:  
            return {
                ...state,
                rejectProcess:{
                    ...state.rejectProcess,
                    loading:true 
                }
            }
        case ODC_REGULAR_REJECT_PROCESS_SUCCESS:  
            return {
                ...state,
                rejectProcess:{
                    ...state.rejectProcess,
                    data: action.data,
                    success: true,
                    response: true,
                    loading: false,
                    error: null 
                }
            }
        case ODC_REGULAR_REJECT_PROCESS_INVALID:  
            return {
                ...state, 
                rejectProcess:{
                    ...state.rejectProcess,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_REGULAR_REJECT_PROCESS_ERROR:  
            return {
                ...state,
                rejectProcess:{
                    ...state.rejectProcess,
                    data: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        default:
            return state;
    }
}

export default odcRegularReducer;