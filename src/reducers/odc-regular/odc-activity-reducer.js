import {
    ODC_REGULAR_CANCEL_ACTIVITY_REQUEST,
    ODC_REGULAR_CANCEL_ACTIVITY_SUCCESS, 
    ODC_REGULAR_CANCEL_ACTIVITY_INVALID, 
    ODC_REGULAR_CANCEL_ACTIVITY_ERROR, 
    // Activity - Generic
    ODC_REGULAR_GENERIC_ACTIVITY_REQUEST,
    ODC_REGULAR_GENERIC_ACTIVITY_SUCCESS,
    ODC_REGULAR_GENERIC_ACTIVITY_INVALID,
    ODC_REGULAR_GENERIC_ACTIVITY_ERROR,
    // Activity - Generic - Simple
    ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_REQUEST, 
    ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_SUCCESS,
    ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_INVALID, 
    ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_ERROR,
    
} from '../../actions/odc-regular/odc-activity';

var stateInitital = { 
    cancelActivity:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null,
    },
    genericActivity:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null,
        next:false
    },
    simpleGenericActivity:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null,
        next:false
    },
    simpleGenericActivityConfirmation:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null,
        next:false
    }
};

const odcActivityReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case ODC_REGULAR_CANCEL_ACTIVITY_REQUEST:
            return {
                ...state,
                cancelActivity:{
                    ...state.cancelActivity,
                    loading:true, 
                    next:false
                }
            }
        case ODC_REGULAR_CANCEL_ACTIVITY_SUCCESS:
            return { 
                ...state,
                cancelActivity:{
                    ...state.cancelActivity,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false, 
                    error:null,
                    next:action.next,
                    extra: action.extra,
                }
            }
        case ODC_REGULAR_CANCEL_ACTIVITY_INVALID: 
            return {
                ...state,
                cancelActivity:{
                    ...state.cancelActivity,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        case ODC_REGULAR_CANCEL_ACTIVITY_ERROR:
            return { 
                ...state,
                cancelActivity:{
                    ...state.cancelActivity, 
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        case ODC_REGULAR_GENERIC_ACTIVITY_REQUEST:
            return {
                ...state,
                genericActivity:{
                    ...state.genericActivity,
                    loading:true, 
                    next:false
                }
            }
        case ODC_REGULAR_GENERIC_ACTIVITY_SUCCESS:
            return { 
                ...state,
                genericActivity:{
                    ...state.genericActivity,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false, 
                    error:null,
                    extra: action.extra,
                    next:action.next
                }
            }
        case ODC_REGULAR_GENERIC_ACTIVITY_INVALID: 
            return {
                ...state,
                genericActivity:{
                    ...state.genericActivity,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        case ODC_REGULAR_GENERIC_ACTIVITY_ERROR:
            return { 
                ...state,
                genericActivity:{
                    ...state.genericActivity, 
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        
        case ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_REQUEST:
            return {
                ...state,
                simpleGenericActivity:{
                    ...state.simpleGenericActivity,
                    loading:true, 
                    next:false
                }
            }
        case ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_SUCCESS:
            return { 
                ...state,
                simpleGenericActivity:{
                    ...state.simpleGenericActivity,
                    data:action.data, 
                    success:true,
                    response:true,
                    loading:false, 
                    error:null,
                    next:action.next,
                    extra: action.extra,
                }
            }
        case ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_INVALID: 
            return {
                ...state,
                simpleGenericActivity:{
                    ...state.simpleGenericActivity,
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        case ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_ERROR:
            return { 
                ...state,
                simpleGenericActivity:{
                    ...state.simpleGenericActivity, 
                    data:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                    next:false
                }
            }
        default:
            return state;
    }
}
export default odcActivityReducer;