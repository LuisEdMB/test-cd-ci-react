import {
    GET_PRODUCT_REQUEST, 
    GET_PRODUCT_SUCCESS, 
    GET_PRODUCT_INVALID, 
    GET_PRODUCT_ERROR
} from '../../actions/value-list/product';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const productReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case GET_PRODUCT_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case GET_PRODUCT_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case GET_PRODUCT_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case GET_PRODUCT_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default productReducer;