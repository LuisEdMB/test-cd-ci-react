import {
    IDENTIFICATION_DOCUMENT_TYPE_REQUEST,
    IDENTIFICATION_DOCUMENT_TYPE_SUCCESS,
    IDENTIFICATION_DOCUMENT_TYPE_INVALID,
    IDENTIFICATION_DOCUMENT_TYPE_ERROR
} from '../../actions/value-list/identification-document-type';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const identificationDocumentTypeReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case IDENTIFICATION_DOCUMENT_TYPE_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case IDENTIFICATION_DOCUMENT_TYPE_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case IDENTIFICATION_DOCUMENT_TYPE_INVALID:
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case IDENTIFICATION_DOCUMENT_TYPE_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default identificationDocumentTypeReducer;

