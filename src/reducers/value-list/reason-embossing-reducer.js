import {
    REASON_EMBOSSING_REQUEST, 
    REASON_EMBOSSING_SUCCESS, 
    REASON_EMBOSSING_INVALID,
    REASON_EMBOSSING_ERROR, 
} from '../../actions/value-list/reason-embossing';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const reasonEmbossingReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case REASON_EMBOSSING_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case REASON_EMBOSSING_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case REASON_EMBOSSING_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case REASON_EMBOSSING_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default reasonEmbossingReducer;