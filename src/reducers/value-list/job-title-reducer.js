import {
    JOB_TITLE_REQUEST,
    JOB_TITLE_SUCCESS,
    JOB_TITLE_INVALID,
    JOB_TITLE_ERROR
} from '../../actions/value-list/job-title';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const jobTitleReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case JOB_TITLE_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case JOB_TITLE_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            } 
        case JOB_TITLE_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case JOB_TITLE_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default jobTitleReducer;
