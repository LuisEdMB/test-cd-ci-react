import {
    VIA_REQUEST,
    VIA_SUCCESS,
    VIA_INVALID,
    VIA_ERROR
} from '../../actions/value-list/via';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const viaReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case VIA_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case VIA_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case VIA_INVALID:
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case VIA_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default viaReducer;
