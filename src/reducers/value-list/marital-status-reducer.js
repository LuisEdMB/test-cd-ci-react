import {
    MARITAL_STATUS_REQUEST,
    MARITAL_STATUS_SUCCESS,
    MARITAL_STATUS_INVALID,
    MARITAL_STATUS_ERROR
} from '../../actions/value-list/marital-status';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const maritalStatusReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case MARITAL_STATUS_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case MARITAL_STATUS_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case MARITAL_STATUS_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case MARITAL_STATUS_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default maritalStatusReducer;
