import {
    COUNTRY_REQUEST,
    COUNTRY_SUCCESS,
    COUNTRY_INVALID,
    COUNTRY_ERROR
} from '../../actions/value-list/country';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const countryReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case COUNTRY_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case COUNTRY_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case COUNTRY_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case COUNTRY_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default countryReducer;
