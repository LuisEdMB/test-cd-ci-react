import {
    FAMILY_RELATIONSHIP_REQUEST,
    FAMILY_RELATIONSHIP_SUCCESS,
    FAMILY_RELATIONSHIP_INVALID,
    FAMILY_RELATIONSHIP_ERROR 
} from '../../actions/value-list/family-relationship';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const familyRelationshipReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case FAMILY_RELATIONSHIP_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case FAMILY_RELATIONSHIP_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case FAMILY_RELATIONSHIP_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case FAMILY_RELATIONSHIP_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default familyRelationshipReducer;
