import {
    ZONE_REQUEST,
    ZONE_SUCCESS,
    ZONE_INVALID,
    ZONE_ERROR
} from '../../actions/value-list/zone';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const zoneReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case ZONE_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case ZONE_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case ZONE_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case ZONE_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default zoneReducer;
