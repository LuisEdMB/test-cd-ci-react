import {
    TYPE_RESIDENCE_REQUEST,
    TYPE_RESIDENCE_SUCCESS,
    TYPE_RESIDENCE_INVALID,
    TYPE_RESIDENCE_ERROR
} from '../../actions/value-list/type-residence';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const typeResidenceReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case TYPE_RESIDENCE_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case TYPE_RESIDENCE_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case TYPE_RESIDENCE_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case TYPE_RESIDENCE_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default typeResidenceReducer;
