import {
    ODC_PHASE_FLOW_STATE_REQUEST,
    ODC_PHASE_FLOW_STATE_SUCCESS, 
    ODC_PHASE_FLOW_STATE_ERROR
} from '../../actions/value-list/phase-flow-state';

var stateInitital = { 
    response:false,
    success:false,
    data:null, 
    error:null,
    loading:false
};

const phaseFlowStateReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case ODC_PHASE_FLOW_STATE_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case ODC_PHASE_FLOW_STATE_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case ODC_PHASE_FLOW_STATE_ERROR: 
            return {
                ...state, 
                data:null,
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default phaseFlowStateReducer;