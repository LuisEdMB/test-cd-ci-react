import {
    OPERATION_TYPE_REQUEST,
    OPERATION_TYPE_SUCCESS,
    OPERATION_TYPE_INVALID,
    OPERATION_TYPE_ERROR
} from '../../actions/value-list/opetarion-type';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const operationTypeReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case OPERATION_TYPE_REQUEST:
            return {
                ...state,
                loading: true 
            }
        case OPERATION_TYPE_SUCCESS:
            return { 
                ...state,
                data: action.data, 
                success: true,
                response: true,
                loading: false,
                error: null
            }
        case OPERATION_TYPE_INVALID: 
            return {
                ...state, 
                data: [],
                success: false,
                loading: false,
                response: action.response,
                error: action.error,
            }
        case OPERATION_TYPE_ERROR: 
            return {
                ...state, 
                data: [],
                success: false,
                loading: false,
                response: action.response,
                error: action.error,
            }
        default:
            return state;
    }
}
export default operationTypeReducer;
