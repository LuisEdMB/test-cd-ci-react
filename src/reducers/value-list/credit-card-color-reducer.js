import {
    CREDIT_CARD_COLOR_REQUEST,
    CREDIT_CARD_COLOR_SUCCESS,  
    CREDIT_CARD_COLOR_INVALID,
    CREDIT_CARD_COLOR_ERROR
} from '../../actions/value-list/credit-card-color';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const creditCardColorReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case CREDIT_CARD_COLOR_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case CREDIT_CARD_COLOR_INVALID:
            return{
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case CREDIT_CARD_COLOR_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case CREDIT_CARD_COLOR_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default creditCardColorReducer;
