import {
    GENDER_REQUEST,
    GENDER_SUCCESS,
    GENDER_INVALID,
    GENDER_ERROR
} from '../../actions/value-list/gender';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const genderReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case GENDER_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case GENDER_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case GENDER_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case GENDER_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default genderReducer;
