import {
    ACCOUNT_STATUS_REQUEST,
    ACCOUNT_STATUS_SUCCESS,  
    ACCOUNT_STATUS_INVALID,  
    ACCOUNT_STATUS_ERROR  
} from '../../actions/value-list/account-status';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const accountStatusReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case ACCOUNT_STATUS_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case ACCOUNT_STATUS_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case ACCOUNT_STATUS_INVALID:
            return{
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case ACCOUNT_STATUS_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default accountStatusReducer;
