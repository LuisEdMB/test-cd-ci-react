import {
    ECONOMIC_ACTIVITY_REQUEST,
    ECONOMIC_ACTIVITY_SUCCESS, 
    ECONOMIC_ACTIVITY_INVALID,
    ECONOMIC_ACTIVITY_ERROR 
} from '../../actions/value-list/economic-activity';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const economicActivityReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case ECONOMIC_ACTIVITY_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case ECONOMIC_ACTIVITY_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case ECONOMIC_ACTIVITY_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case ECONOMIC_ACTIVITY_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default economicActivityReducer;