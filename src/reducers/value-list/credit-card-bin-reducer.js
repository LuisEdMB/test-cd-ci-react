import {
    CREDIT_CARD_BIN_REQUEST, 
    CREDIT_CARD_BIN_SUCCESS, 
    CREDIT_CARD_BIN_INVALID, 
    CREDIT_CARD_BIN_ERROR
} from '../../actions/value-list/credit-card-bin';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const creditCardBinReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case CREDIT_CARD_BIN_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case CREDIT_CARD_BIN_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case CREDIT_CARD_BIN_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case CREDIT_CARD_BIN_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default creditCardBinReducer;