import {
    EMPLOYMENT_SITUATION_REQUEST,
    EMPLOYMENT_SITUATION_SUCCESS,
    EMPLOYMENT_SITUATION_INVALID,
    EMPLOYMENT_SITUATION_ERROR
} from '../../actions/value-list/employment-situation';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const employmentSituationReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case EMPLOYMENT_SITUATION_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case EMPLOYMENT_SITUATION_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case EMPLOYMENT_SITUATION_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case EMPLOYMENT_SITUATION_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default employmentSituationReducer;
