import {
    SEND_CORRESPONDENCE_REQUEST,
    SEND_CORRESPONDENCE_SUCCESS,  
    SEND_CORRESPONDENCE_INVALID,  
    SEND_CORRESPONDENCE_ERROR 
} from '../../actions/value-list/send-correspondence';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const sendCorrespondenceReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case SEND_CORRESPONDENCE_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case SEND_CORRESPONDENCE_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case SEND_CORRESPONDENCE_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case SEND_CORRESPONDENCE_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default sendCorrespondenceReducer;