import {
    MEDIA_REQUEST,
    MEDIA_SUCCESS,
    MEDIA_INVALID, 
    MEDIA_ERROR
} from '../../actions/value-list/media';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const mediaReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case MEDIA_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case MEDIA_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case MEDIA_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case MEDIA_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default mediaReducer;