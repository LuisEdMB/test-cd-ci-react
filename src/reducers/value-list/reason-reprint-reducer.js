import {
    REASON_REPRINT_REQUEST,
    REASON_REPRINT_SUCCESS, 
    REASON_REPRINT_INVALID,
    REASON_REPRINT_ERROR 
} from '../../actions/value-list/reason-reprint';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const reasonReprintReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case REASON_REPRINT_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case REASON_REPRINT_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case REASON_REPRINT_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case REASON_REPRINT_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default reasonReprintReducer;