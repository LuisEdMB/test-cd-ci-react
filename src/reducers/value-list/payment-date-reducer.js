import {
    PAYMENT_DATE_REQUEST,
    PAYMENT_DATE_SUCCESS,
    PAYMENT_DATE_INVALID,
    PAYMENT_DATE_ERROR
} from '../../actions/value-list/payment-date';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const paymentDateReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case PAYMENT_DATE_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case PAYMENT_DATE_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case PAYMENT_DATE_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case PAYMENT_DATE_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default paymentDateReducer;
