import {
    CREDIT_CARD_BRAND_REQUEST,
    CREDIT_CARD_BRAND_SUCCESS,  
    CREDIT_CARD_BRAND_INVALID,
    CREDIT_CARD_BRAND_ERROR
} from '../../actions/value-list/credit-card-brand';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const creditCardBrandReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case CREDIT_CARD_BRAND_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case CREDIT_CARD_BRAND_INVALID:
            return{
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case CREDIT_CARD_BRAND_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case CREDIT_CARD_BRAND_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default creditCardBrandReducer;
