import {
    HOUSING_TYPE_REQUEST,
    HOUSING_TYPE_SUCCESS,
    HOUSING_TYPE_INVALID,
    HOUSING_TYPE_ERROR
} from '../../actions/value-list/housing-type';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const housingTypeReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case HOUSING_TYPE_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case HOUSING_TYPE_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case HOUSING_TYPE_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case HOUSING_TYPE_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default housingTypeReducer;
