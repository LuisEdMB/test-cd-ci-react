import {
    DISBURSEMENT_REQUEST,
    DISBURSEMENT_SUCCESS,
    DISBURSEMENT_INVALID,
    DISBURSEMENT_ERROR
} from '../../actions/value-list/disbursement-type';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const disbursementTypeReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case DISBURSEMENT_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case DISBURSEMENT_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case DISBURSEMENT_INVALID: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case DISBURSEMENT_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default disbursementTypeReducer;