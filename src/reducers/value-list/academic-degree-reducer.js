import {
    ACADEMIC_DEGREE_REQUEST,
    ACADEMIC_DEGREE_SUCCESS,
    ACADEMIC_DEGREE_INVALID,
    ACADEMIC_DEGREE_ERROR
} from '../../actions/value-list/academic-degree';

var stateInitital = { 
    response:false,
    success:false,
    data:[], 
    error:null,
    loading:false
};

const academicDegreeReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case ACADEMIC_DEGREE_REQUEST:
            return {
                ...state,
                loading:true 
            }
        case ACADEMIC_DEGREE_SUCCESS:
            return { 
                ...state,
                data:action.data, 
                success:true,
                response:true,
                loading:false,
                error:null
            }
        case ACADEMIC_DEGREE_INVALID:
            return{
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        case ACADEMIC_DEGREE_ERROR: 
            return {
                ...state, 
                data:[],
                success:false,
                loading:false,
                response:action.response,
                error:action.error,
            }
        default:
            return state;
    }
}
export default academicDegreeReducer;
