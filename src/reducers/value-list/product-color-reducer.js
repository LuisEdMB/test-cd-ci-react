import {
    // Get All Product Colors
    ODC_GET_ALL_PRODUCT_COLORS_REQUEST,
    ODC_GET_ALL_PRODUCT_COLORS_SUCCESS,
    ODC_GET_ALL_PRODUCT_COLORS_INVALID,
    ODC_GET_ALL_PRODUCT_COLORS_ERROR
} from '../../actions/value-list/product-color'

const initialState = {
    loading: false,
    data: [],
    success: false,
    error: null,
    response: false
}

const productColorReducer = (state = initialState, action) => {
    switch (action.type) {
        // Get All Product Colors
        case ODC_GET_ALL_PRODUCT_COLORS_REQUEST:
            return {
                ...initialState,
                loading: true
            }
        
        case ODC_GET_ALL_PRODUCT_COLORS_SUCCESS:
            return {
                ...initialState,
                data: action.data?.coloresProductos || [],
                success: true,
                response: true
            }

        case ODC_GET_ALL_PRODUCT_COLORS_INVALID:
            return {
                ...initialState,
                error: action.error,
                response: true
            }

        case ODC_GET_ALL_PRODUCT_COLORS_ERROR:
            return {
                ...initialState,
                response: action.response,
                error: action.error
            }

        default:
            return state
    }
}

export default productColorReducer