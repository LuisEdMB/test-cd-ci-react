import {
    ODC_DIGITIZATION_INBOX_SEND_REQUEST,
    ODC_DIGITIZATION_INBOX_SEND_SUCCESS,
    ODC_DIGITIZATION_INBOX_SEND_INVALID,
    ODC_DIGITIZATION_INBOX_SEND_ERROR, 

    ODC_DIGITIZATION_INBOX_RECEIVED_REQUEST,
    ODC_DIGITIZATION_INBOX_RECEIVED_SUCCESS,
    ODC_DIGITIZATION_INBOX_RECEIVED_INVALID,
    ODC_DIGITIZATION_INBOX_RECEIVED_ERROR,

    ODC_DIGITIZATION_INBOX_MONITORING_REQUEST,
    ODC_DIGITIZATION_INBOX_MONITORING_SUCCESS,
    ODC_DIGITIZATION_INBOX_MONITORING_INVALID,
    ODC_DIGITIZATION_INBOX_MONITORING_ERROR,


    ODC_DIGITIZATION_SEND_LEGAJO_REQUEST,
    ODC_DIGITIZATION_SEND_LEGAJO_SUCCESS,
    ODC_DIGITIZATION_SEND_LEGAJO_INVALID,
    ODC_DIGITIZATION_SEND_LEGAJO_ERROR, 

    ODC_DIGITIZATION_OBSERVE_LEGAJO_REQUEST,
    ODC_DIGITIZATION_OBSERVE_LEGAJO_SUCCESS,
    ODC_DIGITIZATION_OBSERVE_LEGAJO_INVALID,
    ODC_DIGITIZATION_OBSERVE_LEGAJO_ERROR,

    ODC_DIGITIZATION_REGULARIZE_LEGAJO_REQUEST,
    ODC_DIGITIZATION_REGULARIZE_LEGAJO_SUCCESS, 
    ODC_DIGITIZATION_REGULARIZE_LEGAJO_INVALID, 
    ODC_DIGITIZATION_REGULARIZE_LEGAJO_ERROR,

    ODC_DIGITIZATION_RECEIVE_LEGAJO_REQUEST,
    ODC_DIGITIZATION_RECEIVE_LEGAJO_SUCCESS, 
    ODC_DIGITIZATION_RECEIVE_LEGAJO_INVALID,
    ODC_DIGITIZATION_RECEIVE_LEGAJO_ERROR

} from '../../actions/odc-digitization/odc-digitization';

var stateInitital = { 
    inboxSend:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false,
        send: false,
        regularize: false
    },
    inboxReceived:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    },
    inboxMonitoring:{
        response:false,
        success:false,
        data:[], 
        error:null,
        loading:false
    },
    sendLegajo:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null
    }, 
    receivedLegajo:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null
    },
    observeLegajo:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null
    },
    regularizeLegajo:{
        response:false,
        success:false,
        data:null, 
        error:null,
        loading:false, 
        extra:null
    }
};

const odcDigitizationReducer = (state = stateInitital, action) =>
{
    switch (action.type) {
        case ODC_DIGITIZATION_INBOX_SEND_REQUEST:
            return {
                ...state,
                inboxSend:{
                    ...state.inboxSend,
                    loading:true, 
                }
            }
        case ODC_DIGITIZATION_INBOX_SEND_SUCCESS:
            return { 
                ...state,
                inboxSend:{
                    ...state.inboxSend,
                    data:action.data, 
                    extra:action.extra,
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_DIGITIZATION_INBOX_SEND_INVALID: 
            return {
                ...state,
                inboxSend:{
                    ...state.inboxSend,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_DIGITIZATION_INBOX_SEND_ERROR:
            return { 
                ...state,
                inboxSend:{
                    ...state.inboxSend, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        
        case ODC_DIGITIZATION_INBOX_RECEIVED_REQUEST:
            return {
                ...state,
                inboxReceived:{
                    ...state.inboxReceived,
                    loading:true, 
                }
            }
        case ODC_DIGITIZATION_INBOX_RECEIVED_SUCCESS:
            return { 
                ...state,
                inboxReceived:{
                    ...state.inboxReceived,
                    data:action.data, 
                    extra:action.extra,
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_DIGITIZATION_INBOX_RECEIVED_INVALID: 
            return {
                ...state,
                inboxReceived:{
                    ...state.inboxReceived,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_DIGITIZATION_INBOX_RECEIVED_ERROR:
            return { 
                ...state,
                inboxReceived:{
                    ...state.inboxReceived, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }

        case ODC_DIGITIZATION_INBOX_MONITORING_REQUEST:
                return {
                    ...state,
                    inboxMonitoring:{
                        ...state.inboxMonitoring,
                        loading:true, 
                    }
                }
        case ODC_DIGITIZATION_INBOX_MONITORING_SUCCESS:
            return { 
                ...state,
                inboxMonitoring:{
                    ...state.inboxMonitoring,
                    data:action.data, 
                    extra:action.extra,
                    success:true,
                    response:true,
                    loading:false, 
                    error:action.error
                }
            }
        case ODC_DIGITIZATION_INBOX_MONITORING_INVALID: 
            return {
                ...state,
                inboxMonitoring:{
                    ...state.inboxMonitoring,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_DIGITIZATION_INBOX_MONITORING_ERROR:
            return { 
                ...state,
                inboxMonitoring:{
                    ...state.inboxMonitoring, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }

        case ODC_DIGITIZATION_SEND_LEGAJO_REQUEST:
            return {
                ...state,
                sendLegajo:{
                    ...state.sendLegajo,
                    loading:true, 
                }
            }
        case ODC_DIGITIZATION_SEND_LEGAJO_SUCCESS:
            return { 
                ...state,
                sendLegajo:{
                    ...state.sendLegajo,
                    data: action.data, 
                    extra: action.extra,
                    success: true,
                    response: true,
                    loading: false, 
                    error: action.error
                }
            }

        case ODC_DIGITIZATION_SEND_LEGAJO_INVALID: 
            return {
                ...state,
                sendLegajo:{
                    ...state.sendLegajo,
                    data: null,
                    extra: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_DIGITIZATION_SEND_LEGAJO_ERROR:
            return { 
                ...state,
                inboxMonitoring:{
                    ...state.inboxMonitoring, 
                    data: null,
                    extra: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        
        case ODC_DIGITIZATION_REGULARIZE_LEGAJO_REQUEST:
            return {
                ...state,
                regularizeLegajo:{
                    ...state.regularizeLegajo,
                    loading:true, 
                }
            }
        case ODC_DIGITIZATION_REGULARIZE_LEGAJO_SUCCESS:
            return { 
                ...state,
                regularizeLegajo:{
                    ...state.regularizeLegajo,
                    data: action.data,
                    extra: action.extra,
                    success: true,
                    response: true,
                    loading: false, 
                    error: action.error
                }
            }

        case ODC_DIGITIZATION_REGULARIZE_LEGAJO_INVALID: 
            return {
                ...state,
                regularizeLegajo:{
                    ...state.regularizeLegajo,
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case ODC_DIGITIZATION_REGULARIZE_LEGAJO_ERROR:
            return { 
                ...state,
                regularizeLegajo:{
                    ...state.regularizeLegajo, 
                    data:null,
                    extra:null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }

        case ODC_DIGITIZATION_OBSERVE_LEGAJO_REQUEST:
            return {
                ...state,
                observeLegajo:{
                    ...state.observeLegajo,
                    loading: true, 
                }
            }
        case ODC_DIGITIZATION_OBSERVE_LEGAJO_SUCCESS:
            return { 
                ...state,
                observeLegajo:{
                    ...state.observeLegajo,
                    data: action.data, 
                    extra: action.extra,
                    success: true,
                    response: true,
                    loading: false, 
                    error: action.error
                }
            }
        case ODC_DIGITIZATION_OBSERVE_LEGAJO_INVALID: 
            return {
                ...state,
                observeLegajo:{
                    ...state.observeLegajo,
                    data: null,
                    extra: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_DIGITIZATION_OBSERVE_LEGAJO_ERROR:
            return { 
                ...state,
                observeLegajo:{
                    ...state.observeLegajo, 
                    data: null,
                    extra: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }

        case ODC_DIGITIZATION_RECEIVE_LEGAJO_REQUEST:
            return {
                ...state,
                receivedLegajo:{
                    ...state.receivedLegajo,
                    loading: true, 
                }
            }
        case ODC_DIGITIZATION_RECEIVE_LEGAJO_SUCCESS:
            return { 
                ...state,
                receivedLegajo:{
                    ...state.receivedLegajo,
                    data: action.data, 
                    extra: action.extra,
                    success: true,
                    response: true,
                    loading: false, 
                    error: action.error
                }
            }
        case ODC_DIGITIZATION_RECEIVE_LEGAJO_INVALID: 
            return {
                ...state,
                receivedLegajo:{
                    ...state.receivedLegajo,
                    data: null,
                    extra: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case ODC_DIGITIZATION_RECEIVE_LEGAJO_ERROR:
            return { 
                ...state,
                receivedLegajo:{
                    ...state.receivedLegajo, 
                    data: null,
                    extra: null,
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        default:
            return state;
    }
}
export default odcDigitizationReducer;