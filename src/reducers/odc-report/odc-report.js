import {
    REPORT_ORIGINATED_CARDS_REQUEST,
    REPORT_ORIGINATED_CARDS_SUCCESS,
    REPORT_ORIGINATED_CARDS_INVALID,
    REPORT_ORIGINATED_CARDS_ERROR,

    REPORT_ACTIVATED_CARDS_REQUEST,
    REPORT_ACTIVATED_CARDS_SUCCESS,
    REPORT_ACTIVATED_CARDS_INVALID,
    REPORT_ACTIVATED_CARDS_ERROR,

    REPORT_ORIGINATED_CARDS_INDICATORS_REQUEST,
    REPORT_ORIGINATED_CARDS_INDICATORS_SUCCESS,
    REPORT_ORIGINATED_CARDS_INDICATORS_INVALID, 
    REPORT_ORIGINATED_CARDS_INDICATORS_ERROR, 

    REPORT_ORIGINATION_OPERATIONS_REQUEST,
    REPORT_ORIGINATION_OPERATIONS_SUCCESS, 
    REPORT_ORIGINATION_OPERATIONS_INVALID, 
    REPORT_ORIGINATION_OPERATIONS_ERROR,

    REPORT_ORIGINATION_OPERATIONS_SAE_REQUEST,
    REPORT_ORIGINATION_OPERATIONS_SAE_SUCCESS, 
    REPORT_ORIGINATION_OPERATIONS_SAE_INVALID, 
    REPORT_ORIGINATION_OPERATIONS_SAE_ERROR,

    REPORT_ORIGINATIONS_OPERATIONS_SAE_REQUEST,
    REPORT_ORIGINATIONS_OPERATIONS_SAE_SUCCESS, 
    REPORT_ORIGINATIONS_OPERATIONS_SAE_INVALID, 
    REPORT_ORIGINATIONS_OPERATIONS_SAE_ERROR,

    REPORT_ORIGINATION_OPERATIONS_INDICATORS_REQUEST,
    REPORT_ORIGINATION_OPERATIONS_INDICATORS_SUCCESS,
    REPORT_ORIGINATION_OPERATIONS_INDICATORS_INVALID,
    REPORT_ORIGINATION_OPERATIONS_INDICATORS_ERROR,

    REPORT_CLIENT_CONSULT_REQUEST, 
    REPORT_CLIENT_CONSULT_SUCCESS ,
    REPORT_CLIENT_CONSULT_INVALID , 
    REPORT_CLIENT_CONSULT_ERROR,

    REPORT_DAILY_DOCUMENT_REQUEST,
    REPORT_DAILY_DOCUMENT_SUCCESS,
    REPORT_DAILY_DOCUMENT_INVALID,
    REPORT_DAILY_DOCUMENT_ERROR

} from '../../actions/odc-report/odc-report';

var stateInitital = { 
    originatedCards:{
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false, 
        extra: null
    },
    activatedCards:{
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false, 
        extra: null
    },
    originatedCardsIndicators:{
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false, 
        extra: null
    },
    originationOperations: {
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false, 
        extra: null
    },
    originacionOperacionesSae: {
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false, 
        extra: null
    }, 
    originationOperationsIndicators: {
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false, 
        extra: null
    },
    clientConsult:{
        response: false,
        success: false,
        data: [], 
        error: null,
        loading: false, 
        extra: null
    },
    digitalDocumentDayli: {
        loading: false
    }
};

const odcReportReducer = (state = stateInitital, action) =>
{
    switch(action.type){
        case REPORT_ORIGINATED_CARDS_REQUEST:
            return {
                ...state,
                originatedCards:{
                    ...state.originatedCards,
                    loading:true, 
                }
            }
        case REPORT_ORIGINATED_CARDS_SUCCESS:
            return { 
                ...state,
                originatedCards:{
                    ...state.originatedCards,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false, 
                    error: null
                }
            }
        case REPORT_ORIGINATED_CARDS_INVALID: 
            return {
                ...state,
                originatedCards:{
                    ...state.originatedCards,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case REPORT_ORIGINATED_CARDS_ERROR:
            return { 
                ...state,
                originatedCards:{
                    ...state.originatedCards, 
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        case REPORT_ACTIVATED_CARDS_REQUEST:
            return {
                ...state,
                activatedCards:{
                    ...state.activatedCards,
                    loading:true, 
                }
            }
        case REPORT_ACTIVATED_CARDS_SUCCESS:
            return { 
                ...state,
                activatedCards:{
                    ...state.activatedCards,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false, 
                    error: null
                }
            }
        case REPORT_ACTIVATED_CARDS_INVALID: 
            return {
                ...state,
                activatedCards:{
                    ...state.activatedCards,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case REPORT_ACTIVATED_CARDS_ERROR:
            return { 
                ...state,
                activatedCards:{
                    ...state.activatedCards, 
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }

        case REPORT_ORIGINATED_CARDS_INDICATORS_REQUEST:
            return {
                ...state,
                originatedCardsIndicators:{
                    ...state.originatedCardsIndicators,
                    loading:true, 
                }
            }
        case REPORT_ORIGINATED_CARDS_INDICATORS_SUCCESS:
            return { 
                ...state,
                originatedCardsIndicators:{
                    ...state.originatedCardsIndicators,
                    data: action.data, 
                    success: true,
                    response: true,
                    loading: false, 
                    error: null
                }
            }
        case REPORT_ORIGINATED_CARDS_INDICATORS_INVALID: 
            return {
                ...state,
                originatedCardsIndicators:{
                    ...state.originatedCardsIndicators,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case REPORT_ORIGINATED_CARDS_INDICATORS_ERROR:
            return { 
                ...state,
                originatedCardsIndicators:{
                    ...state.originatedCardsIndicators, 
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }



        case REPORT_ORIGINATION_OPERATIONS_REQUEST:
            return {
                ...state,
                originationOperations:{
                    ...state.originationOperations,
                    loading: true
                }
            }
        case REPORT_ORIGINATION_OPERATIONS_SUCCESS:
            return { 
                ...state,
                originationOperations:{
                    ...state.originationOperations,
                    data:action.data, 
                    success: true,
                    response: true,
                    loading: false, 
                    error: null
                }
            }
        case REPORT_ORIGINATION_OPERATIONS_INVALID: 
            return {
                ...state,
                originationOperations:{
                    ...state.originationOperations,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case REPORT_ORIGINATION_OPERATIONS_ERROR:
            return { 
                ...state,
                originationOperations:{
                    ...state.originationOperations, 
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }

        case REPORT_ORIGINATION_OPERATIONS_INDICATORS_REQUEST:
            return {
                ...state,
                originationOperationsIndicators:{
                    ...state.pendingEmbossingActivation,
                    loading: true
                }
            }
        case REPORT_ORIGINATION_OPERATIONS_INDICATORS_SUCCESS:
            return { 
                ...state,
                originationOperationsIndicators:{
                    ...state.originationOperationsIndicators,
                    data:action.data, 
                    success: true,
                    response: true,
                    loading: false, 
                    error: null
                }
            }
        case REPORT_ORIGINATION_OPERATIONS_INDICATORS_INVALID: 
            return {
                ...state,
                originationOperationsIndicators:{
                    ...state.originationOperationsIndicators,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case REPORT_ORIGINATION_OPERATIONS_INDICATORS_ERROR:
            return { 
                ...state,
                originationOperationsIndicators:{
                    ...state.originationOperationsIndicators, 
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }

        case REPORT_CLIENT_CONSULT_REQUEST:
            return {
                ...state,
                clientConsult:{
                    ...state.clientConsult,
                    loading: true
                }
            }
        case REPORT_CLIENT_CONSULT_SUCCESS:
            return { 
                ...state,
                clientConsult:{
                    ...state.clientConsult,
                    data:action.data, 
                    success: true,
                    response: true,
                    loading: false, 
                    error: null
                }
            }
        case REPORT_CLIENT_CONSULT_INVALID: 
            return {
                ...state,
                clientConsult:{
                    ...state.clientConsult,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case REPORT_CLIENT_CONSULT_ERROR:
            return { 
                ...state,
                clientConsult:{
                    ...state.clientConsult, 
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
        //////////////////////
        case REPORT_ORIGINATION_OPERATIONS_SAE_REQUEST:
        case REPORT_ORIGINATIONS_OPERATIONS_SAE_REQUEST:
            return {
                ...state,
                originacionOperacionesSae:{
                    ...state.originacionOperacionesSae,
                    loading: true
                }
            }
        case REPORT_ORIGINATION_OPERATIONS_SAE_SUCCESS:
        case REPORT_ORIGINATIONS_OPERATIONS_SAE_SUCCESS:
            return { 
                ...state,
                originacionOperacionesSae:{
                    ...state.originacionOperacionesSae,
                    data:action.data, 
                    success: true,
                    response: true,
                    loading: false, 
                    error: null
                }
            }
        case REPORT_ORIGINATION_OPERATIONS_SAE_INVALID: 
        case REPORT_ORIGINATIONS_OPERATIONS_SAE_INVALID: 
            return {
                ...state,
                originacionOperacionesSae:{
                    ...state.originacionOperacionesSae,
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error
                }
            }
        case REPORT_ORIGINATION_OPERATIONS_SAE_ERROR:
        case REPORT_ORIGINATIONS_OPERATIONS_SAE_ERROR:
            return { 
                ...state,
                originacionOperacionesSae:{
                    ...state.originacionOperacionesSae, 
                    data: [],
                    success: false,
                    loading: false,
                    response: action.response,
                    error: action.error,
                }
            }
            case REPORT_DAILY_DOCUMENT_REQUEST:
                return {
                    ...state,
                    digitalDocumentDayli: {
                        loading: true
                    }
                }
            case REPORT_DAILY_DOCUMENT_SUCCESS:
            case REPORT_DAILY_DOCUMENT_INVALID:
            case REPORT_DAILY_DOCUMENT_ERROR:
                return { 
                    ...state,
                    digitalDocumentDayli: {
                        loading: false
                    }
                }
        default:
            return state;
    }
}
export default odcReportReducer;