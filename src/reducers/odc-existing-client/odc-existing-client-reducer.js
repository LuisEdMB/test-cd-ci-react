import {
    // Consult Offer
    ODC_EXISTING_CLIENT_CONSULT_OFFER_REQUEST,
    ODC_EXISTING_CLIENT_CONSULT_OFFER_SUCCESS,
    ODC_EXISTING_CLIENT_CONSULT_OFFER_INVALID,
    ODC_EXISTING_CLIENT_CONSULT_OFFER_ERROR,

    // Get Client Information
    ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_REQUEST,
    ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_SUCCESS,
    ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_INVALID,
    ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_ERROR,

    // Update Client Information
    ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_REQUEST,
    ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_SUCCESS,
    ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_INVALID,
    ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_ERROR,

    // Register Commercial Offer
    ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_REQUEST,
    ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_SUCCESS,
    ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_INVALID,
    ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_ERROR,

    // Register Account & Card
    ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_REQUEST,
    ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_SUCCESS,
    ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_INVALID,
    ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_ERROR,

    // Continue Process
    ODC_EXISTING_CLIENT_CONTINUE_PROCESS_REQUEST,
    ODC_EXISTING_CLIENT_CONTINUE_PROCESS_SUCCESS,
    ODC_EXISTING_CLIENT_CONTINUE_PROCESS_INVALID,
    ODC_EXISTING_CLIENT_CONTINUE_PROCESS_ERROR,

    // Cancel Origination
    ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_REQUEST,
    ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_SUCCESS,
    ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_INVALID,
    ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_ERROR,

    // Finish Call Origination
    ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_REQUEST,
    ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_SUCCESS,
    ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_INVALID,
    ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_ERROR,

    // Register Activity
    ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_REQUEST,
    ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_SUCCESS,
    ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_INVALID,
    ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_ERROR,

    // Register Summary Activity
    ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_REQUEST,
    ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_SUCCESS,
    ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_INVALID,
    ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_ERROR,

    // Register Reject Activity
    ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_REQUEST,
    ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_SUCCESS,
    ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_INVALID,
    ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_ERROR,

    // Cancel Activity
    ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_REQUEST,
    ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_SUCCESS,
    ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_INVALID,
    ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_ERROR
} from '../../actions/odc-existing-client/odc-existing-client'

var initialState = {
    offerConsulted: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    clientInformation: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    clientInformationUpdated: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    commercialOfferRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    accountCardRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    process: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    originationCanceled: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    callOriginationFinished: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    activityRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    summaryActivityRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    activityRejectedRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    activityCanceled: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    }
}

const odcExistingClientReducer = (state = initialState, action) => {
    switch (action.type) {
        // Consult Offer
        case ODC_EXISTING_CLIENT_CONSULT_OFFER_REQUEST:
            return {
                ...state,
                offerConsulted: {
                    ...initialState.offerConsulted,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_CONSULT_OFFER_SUCCESS:
            return {
                ...state,
                offerConsulted: {
                    ...initialState.offerConsulted,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_CONSULT_OFFER_INVALID:
            return {
                ...state,
                offerConsulted: {
                    ...initialState.offerConsulted,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_CONSULT_OFFER_ERROR:
            return {
                ...state,
                offerConsulted: {
                    ...initialState.offerConsulted,
                    response: action.response,
                    error: action.error
                }
            }

        // Get Client Information
        case ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_REQUEST:
            return {
                ...state,
                clientInformation: {
                    ...initialState.clientInformation,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_SUCCESS:
            return {
                ...state,
                clientInformation: {
                    ...initialState.clientInformation,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_INVALID:
            return {
                ...state,
                clientInformation: {
                    ...initialState.clientInformation,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_ERROR:
            return {
                ...state,
                clientInformation: {
                    ...initialState.clientInformation,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Update Client Information
        case ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_REQUEST:
            return {
                ...state,
                clientInformationUpdated: {
                    ...initialState.clientInformationUpdated,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_SUCCESS:
            return {
                ...state,
                clientInformationUpdated: {
                    ...initialState.clientInformationUpdated,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_INVALID:
            return {
                ...state,
                clientInformationUpdated: {
                    ...initialState.clientInformationUpdated,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_ERROR:
            return {
                ...state,
                clientInformationUpdated: {
                    ...initialState.clientInformationUpdated,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Register Commercial Offer
        case ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_REQUEST:
            return {
                ...state,
                commercialOfferRegistered: {
                    ...initialState.commercialOfferRegistered,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_SUCCESS:
            return {
                ...state,
                commercialOfferRegistered: {
                    ...initialState.commercialOfferRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_INVALID:
            return {
                ...state,
                commercialOfferRegistered: {
                    ...initialState.commercialOfferRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_ERROR:
            return {
                ...state,
                commercialOfferRegistered: {
                    ...initialState.commercialOfferRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Account & Card
        case ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_REQUEST:
            return {
                ...state,
                accountCardRegistered: {
                    ...initialState.accountCardRegistered,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_SUCCESS:
            return {
                ...state,
                accountCardRegistered: {
                    ...initialState.accountCardRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_INVALID:
            return {
                ...state,
                accountCardRegistered: {
                    ...initialState.accountCardRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_ERROR:
            return {
                ...state,
                accountCardRegistered: {
                    ...initialState.accountCardRegistered,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Continue Process
        case ODC_EXISTING_CLIENT_CONTINUE_PROCESS_REQUEST:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_CONTINUE_PROCESS_SUCCESS:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_CONTINUE_PROCESS_INVALID:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_CONTINUE_PROCESS_ERROR:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Cancel Origination
        case ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_REQUEST:
            return {
                ...state,
                originationCanceled: {
                    ...initialState.originationCanceled,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_SUCCESS:
            return {
                ...state,
                originationCanceled: {
                    ...initialState.originationCanceled,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_INVALID:
            return {
                ...state,
                originationCanceled: {
                    ...initialState.originationCanceled,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_ERROR:
            return {
                ...state,
                originationCanceled: {
                    ...initialState.originationCanceled,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Finish Call Origination
        case ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_REQUEST:
            return {
                ...state,
                callOriginationFinished: {
                    ...initialState.callOriginationFinished,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_SUCCESS:
            return {
                ...state,
                callOriginationFinished: {
                    ...initialState.callOriginationFinished,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_INVALID:
            return {
                ...state,
                callOriginationFinished: {
                    ...initialState.callOriginationFinished,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_ERROR:
            return {
                ...state,
                callOriginationFinished: {
                    ...initialState.callOriginationFinished,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Register Activity
        case ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_REQUEST:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_SUCCESS:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_INVALID:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_ERROR:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Register Summary Activity
        case ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_REQUEST:
            return {
                ...state,
                summaryActivityRegistered: {
                    ...initialState.summaryActivityRegistered,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_SUCCESS:
            return {
                ...state,
                summaryActivityRegistered: {
                    ...initialState.summaryActivityRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_INVALID:
            return {
                ...state,
                summaryActivityRegistered: {
                    ...initialState.summaryActivityRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_ERROR:
            return {
                ...state,
                summaryActivityRegistered: {
                    ...initialState.summaryActivityRegistered,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Register Reject Activity
        case ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_REQUEST:
            return {
                ...state,
                activityRejectedRegistered: {
                    ...initialState.activityRejectedRegistered,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_SUCCESS:
            return {
                ...state,
                activityRejectedRegistered: {
                    ...initialState.activityRejectedRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_INVALID:
            return {
                ...state,
                activityRejectedRegistered: {
                    ...initialState.activityRejectedRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_ERROR:
            return {
                ...state,
                activityRejectedRegistered: {
                    ...initialState.activityRejectedRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Cancel Activity
        case ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_REQUEST:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    loading: true
                }
            }
        case ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_SUCCESS:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_INVALID:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    error: action.error,
                    response: true
                }
            }
        case ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_ERROR:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    response: action.response,
                    error: action.error
                }
            }
        default:
            return state
    }
}

export default odcExistingClientReducer