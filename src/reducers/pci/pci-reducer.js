import {
    // Encrypt Cardnumber
    PCI_ENCRYPT_CARDNUMBER_REQUEST,
    PCI_ENCRYPT_CARDNUMBER_SUCCESS,
    PCI_ENCRYPT_CARDNUMBER_INVALID,
    PCI_ENCRYPT_CARDNUMBER_ERROR,

    // Decrypt Cardnumber
    PCI_DECRYPT_CARDNUMBER_REQUEST,
    PCI_DECRYPT_CARDNUMBER_SUCCESS,
    PCI_DECRYPT_CARDNUMBER_INVALID,
    PCI_DECRYPT_CARDNUMBER_ERROR
} from '../../actions/pci/pci'

const initialState = {
    cardnumberEncrypted: {
        loading: false,
        data: null,
        success: false,
        error: null,
        errorCode: null,
        response: false
    },
    cardnumberDecrypted: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    }
}

const pciReducer = (state = initialState, action) => {
    switch (action.type) {
        // Encrypt Cardnumber
        case PCI_ENCRYPT_CARDNUMBER_REQUEST:
            return {
                ...state,
                cardnumberEncrypted: {
                    ...initialState.cardnumberEncrypted,
                    loading: true
                }
            }
        case PCI_ENCRYPT_CARDNUMBER_SUCCESS:
            return {
                ...state,
                cardnumberEncrypted: {
                    ...initialState.cardnumberEncrypted,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case PCI_ENCRYPT_CARDNUMBER_INVALID:
            return {
                ...state,
                cardnumberEncrypted: {
                    ...initialState.cardnumberEncrypted,
                    error: action.error,
                    errorCode: action.errorCode,
                    response: true
                }
            }
        case PCI_ENCRYPT_CARDNUMBER_ERROR:
            return {
                ...state,
                cardnumberEncrypted: {
                    ...initialState.cardnumberEncrypted,
                    response: action.response,
                    error: action.error,
                    errorCode: action.errorCode
                }
            }

        // Decrypt Cardnumber
        case PCI_DECRYPT_CARDNUMBER_REQUEST:
            return {
                ...state,
                cardnumberDecrypted: {
                    ...initialState.cardnumberDecrypted,
                    loading: true
                }
            }
        case PCI_DECRYPT_CARDNUMBER_SUCCESS:
            return {
                ...state,
                cardnumberDecrypted: {
                    ...initialState.cardnumberDecrypted,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case PCI_DECRYPT_CARDNUMBER_INVALID:
            return {
                ...state,
                cardnumberDecrypted: {
                    ...initialState.cardnumberDecrypted,
                    error: action.error,
                    response: true
                }
            }
        case PCI_DECRYPT_CARDNUMBER_ERROR:
            return {
                ...state,
                cardnumberDecrypted: {
                    ...initialState.cardnumberDecrypted,
                    response: action.response,
                    error: action.error
                }
            }

        default:
            return state
    }
}

export default pciReducer