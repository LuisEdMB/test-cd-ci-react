import {
    // Consult Client
    ODC_REPRINT_CONSULT_CLIENT_REQUEST,
    ODC_REPRINT_CONSULT_CLIENT_SUCCESS,
    ODC_REPRINT_CONSULT_CLIENT_INVALID,
    ODC_REPRINT_CONSULT_CLIENT_ERROR,

    // Upload Document
    ODC_REPRINT_UPLOAD_DOCUMENT_REQUEST,
    ODC_REPRINT_UPLOAD_DOCUMENT_SUCCESS,
    ODC_REPRINT_UPLOAD_DOCUMENT_INVALID,
    ODC_REPRINT_UPLOAD_DOCUMENT_ERROR,

    // Register Reprint Reason
    ODC_REPRINT_REGISTER_REPRINT_REASON_REQUEST,
    ODC_REPRINT_REGISTER_REPRINT_REASON_SUCCESS,
    ODC_REPRINT_REGISTER_REPRINT_REASON_INVALID,
    ODC_REPRINT_REGISTER_REPRINT_REASON_ERROR,

    // Register Reprint Detail
    ODC_REPRINT_REGISTER_REPRINT_DETAIL_REQUEST,
    ODC_REPRINT_REGISTER_REPRINT_DETAIL_SUCCESS,
    ODC_REPRINT_REGISTER_REPRINT_DETAIL_INVALID,
    ODC_REPRINT_REGISTER_REPRINT_DETAIL_ERROR,

    // Register Pending Emboss
    ODC_REPRINT_REGISTER_PENDING_EMBOSS_REQUEST,
    ODC_REPRINT_REGISTER_PENDING_EMBOSS_SUCCESS,
    ODC_REPRINT_REGISTER_PENDING_EMBOSS_INVALID,
    ODC_REPRINT_REGISTER_PENDING_EMBOSS_ERROR,

    // Register Contingency Process
    ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_REQUEST,
    ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_SUCCESS,
    ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_INVALID,
    ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_ERROR,

    // Register Activity
    ODC_REPRINT_REGISTER_ACTIVITY_REQUEST,
    ODC_REPRINT_REGISTER_ACTIVITY_SUCCESS,
    ODC_REPRINT_REGISTER_ACTIVITY_INVALID,
    ODC_REPRINT_REGISTER_ACTIVITY_ERROR,

    // Cancel Activity
    ODC_REPRINT_CANCEL_ACTIVITY_REQUEST,
    ODC_REPRINT_CANCEL_ACTIVITY_SUCCESS,
    ODC_REPRINT_CANCEL_ACTIVITY_INVALID,
    ODC_REPRINT_CANCEL_ACTIVITY_ERROR,

    // Continue Process
    ODC_REPRINT_CONTINUE_PROCESS_REQUEST,
    ODC_REPRINT_CONTINUE_PROCESS_SUCCESS,
    ODC_REPRINT_CONTINUE_PROCESS_INVALID,
    ODC_REPRINT_CONTINUE_PROCESS_ERROR,

    // Register Continue Process Activity
    ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_REQUEST,
    ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_SUCCESS,
    ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_INVALID,
    ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_ERROR,

    // Get Pending Processes Activation
    ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_REQUEST,
    ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_SUCCESS,
    ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_INVALID,
    ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_ERROR,

    // Authorize Activation Process
    ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_REQUEST,
    ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS,
    ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_INVALID,
    ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_ERROR,

    // Reject Activation Process
    ODC_REPRINT_REJECT_ACTIVATION_PROCESS_REQUEST,
    ODC_REPRINT_REJECT_ACTIVATION_PROCESS_SUCCESS,
    ODC_REPRINT_REJECT_ACTIVATION_PROCESS_INVALID,
    ODC_REPRINT_REJECT_ACTIVATION_PROCESS_ERROR,

    // Get Pending Processes
    ODC_REPRINT_GET_PENDING_PROCESSES_REQUEST,
    ODC_REPRINT_GET_PENDING_PROCESSES_SUCCESS,
    ODC_REPRINT_GET_PENDING_PROCESSES_INVALID,
    ODC_REPRINT_GET_PENDING_PROCESSES_ERROR,

    // Register Observation Validated
    ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_REQUEST,
    ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_SUCCESS,
    ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_INVALID,
    ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_ERROR,

    // Get Reprints For Searching
    ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_REQUEST,
    ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_SUCCESS,
    ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_INVALID,
    ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_ERROR
} from '../../actions/odc-reprint/odc-reprint'

const initialState = {
    clientConsulted: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    documentUploaded: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    reprintReasonRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    reprintDetailRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    pendingEmbossRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    contingencyProcessRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    activityRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    activityCanceled: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    process: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    processActivityRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    pendingActivationProcesses: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    activationProcessAuthorized: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    activationProcessRejected: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    pendingProcesses: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    observationValidatedRegistered: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    reprintsForSearching: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    }
}

const odcReprintReducer = (state = initialState, action) => {
    switch (action.type) {
        // Consult Client
        case ODC_REPRINT_CONSULT_CLIENT_REQUEST:
            return {
                ...state,
                clientConsulted: {
                    ...initialState.clientConsulted,
                    loading: true
                }
            }
        case ODC_REPRINT_CONSULT_CLIENT_SUCCESS:
            return {
                ...state,
                clientConsulted: {
                    ...initialState.clientConsulted,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_CONSULT_CLIENT_INVALID:
            return {
                ...state,
                clientConsulted: {
                    ...initialState.clientConsulted,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_CONSULT_CLIENT_ERROR:
            return {
                ...state,
                clientConsulted: {
                    ...initialState.clientConsulted,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Upload Document
        case ODC_REPRINT_UPLOAD_DOCUMENT_REQUEST:
            return {
                ...state,
                documentUploaded: {
                    ...initialState.documentUploaded,
                    loading: true
                }
            }
        case ODC_REPRINT_UPLOAD_DOCUMENT_SUCCESS:
            return {
                ...state,
                documentUploaded: {
                    ...initialState.documentUploaded,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_UPLOAD_DOCUMENT_INVALID:
            return {
                ...state,
                documentUploaded: {
                    ...initialState.documentUploaded,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_UPLOAD_DOCUMENT_ERROR:
            return {
                ...state,
                documentUploaded: {
                    ...initialState.documentUploaded,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Reprint Reason
        case ODC_REPRINT_REGISTER_REPRINT_REASON_REQUEST:
            return {
                ...state,
                reprintReasonRegistered: {
                    ...initialState.reprintReasonRegistered,
                    loading: true
                }
            }
        case ODC_REPRINT_REGISTER_REPRINT_REASON_SUCCESS:
            return {
                ...state,
                reprintReasonRegistered: {
                    ...initialState.reprintReasonRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_REPRINT_REASON_INVALID:
            return {
                ...state,
                reprintReasonRegistered: {
                    ...initialState.reprintReasonRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_REPRINT_REASON_ERROR:
            return {
                ...state,
                reprintReasonRegistered: {
                    ...initialState.reprintReasonRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Reprint Detail
        case ODC_REPRINT_REGISTER_REPRINT_DETAIL_REQUEST:
            return {
                ...state,
                reprintDetailRegistered: {
                    ...initialState.reprintDetailRegistered,
                    loading: true
                }
            }
        case ODC_REPRINT_REGISTER_REPRINT_DETAIL_SUCCESS:
            return {
                ...state,
                reprintDetailRegistered: {
                    ...initialState.reprintDetailRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_REPRINT_DETAIL_INVALID:
            return {
                ...state,
                reprintDetailRegistered: {
                    ...initialState.reprintDetailRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_REPRINT_DETAIL_ERROR:
            return {
                ...state,
                reprintDetailRegistered: {
                    ...initialState.reprintDetailRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Pending Emboss
        case ODC_REPRINT_REGISTER_PENDING_EMBOSS_REQUEST:
            return {
                ...state,
                pendingEmbossRegistered: {
                    ...initialState.pendingEmbossRegistered,
                    loading: true
                }
            }
        case ODC_REPRINT_REGISTER_PENDING_EMBOSS_SUCCESS:
            return {
                ...state,
                pendingEmbossRegistered: {
                    ...initialState.pendingEmbossRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_PENDING_EMBOSS_INVALID:
            return {
                ...state,
                pendingEmbossRegistered: {
                    ...initialState.pendingEmbossRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_PENDING_EMBOSS_ERROR:
            return {
                ...state,
                pendingEmbossRegistered: {
                    ...initialState.pendingEmbossRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Contingency Process
        case ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_REQUEST:
            return {
                ...state,
                contingencyProcessRegistered: {
                    ...initialState.contingencyProcessRegistered,
                    loading: true
                }
            }
        case ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_SUCCESS:
            return {
                ...state,
                contingencyProcessRegistered: {
                    ...initialState.contingencyProcessRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_INVALID:
            return {
                ...state,
                contingencyProcessRegistered: {
                    ...initialState.contingencyProcessRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_ERROR:
            return {
                ...state,
                contingencyProcessRegistered: {
                    ...initialState.contingencyProcessRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Activity
        case ODC_REPRINT_REGISTER_ACTIVITY_REQUEST:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    loading: true
                }
            }
        case ODC_REPRINT_REGISTER_ACTIVITY_SUCCESS:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_ACTIVITY_INVALID:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_ACTIVITY_ERROR:
            return {
                ...state,
                activityRegistered: {
                    ...initialState.activityRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Cancel Activity
        case ODC_REPRINT_CANCEL_ACTIVITY_REQUEST:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    loading: true
                }
            }
        case ODC_REPRINT_CANCEL_ACTIVITY_SUCCESS:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_CANCEL_ACTIVITY_INVALID:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_CANCEL_ACTIVITY_ERROR:
            return {
                ...state,
                activityCanceled: {
                    ...initialState.activityCanceled,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Continue Process
        case ODC_REPRINT_CONTINUE_PROCESS_REQUEST:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    loading: true
                }
            }
        case ODC_REPRINT_CONTINUE_PROCESS_SUCCESS:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_CONTINUE_PROCESS_INVALID:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_CONTINUE_PROCESS_ERROR:
            return {
                ...state,
                process: {
                    ...initialState.process,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Continue Process Activity
        case ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_REQUEST:
            return {
                ...state,
                processActivityRegistered: {
                    ...initialState.processActivityRegistered,
                    loading: true
                }
            }
        case ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_SUCCESS:
            return {
                ...state,
                processActivityRegistered: {
                    ...initialState.processActivityRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_INVALID:
            return {
                ...state,
                processActivityRegistered: {
                    ...initialState.processActivityRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_ERROR:
            return {
                ...state,
                processActivityRegistered: {
                    ...initialState.processActivityRegistered,
                    response: action.response,
                    error: action.error
                }
            }
        
        // Get Pending Processes Activation
        case ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_REQUEST:
            return {
                ...state,
                pendingActivationProcesses: {
                    ...initialState.pendingActivationProcesses,
                    loading: true
                }
            }
        case ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_SUCCESS:
            return {
                ...state,
                pendingActivationProcesses: {
                    ...initialState.pendingActivationProcesses,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_INVALID:
            return {
                ...state,
                pendingActivationProcesses: {
                    ...initialState.pendingActivationProcesses,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_ERROR:
            return {
                ...state,
                pendingActivationProcesses: {
                    ...initialState.pendingActivationProcesses,
                    response: action.response,
                    error: action.error
                }
            }

        // Authorize Activation Process
        case ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_REQUEST:
            return {
                ...state,
                activationProcessAuthorized: {
                    ...initialState.activationProcessAuthorized,
                    loading: true
                }
            }
        case ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS:
            return {
                ...state,
                activationProcessAuthorized: {
                    ...initialState.activationProcessAuthorized,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_INVALID:
            return {
                ...state,
                activationProcessAuthorized: {
                    ...initialState.activationProcessAuthorized,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_ERROR:
            return {
                ...state,
                activationProcessAuthorized: {
                    ...initialState.activationProcessAuthorized,
                    response: action.response,
                    error: action.error
                }
            }

        // Reject Activation Process
        case ODC_REPRINT_REJECT_ACTIVATION_PROCESS_REQUEST:
            return {
                ...state,
                activationProcessRejected: {
                    ...initialState.activationProcessRejected,
                    loading: true
                }
            }
        case ODC_REPRINT_REJECT_ACTIVATION_PROCESS_SUCCESS:
            return {
                ...state,
                activationProcessRejected: {
                    ...initialState.activationProcessRejected,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_REJECT_ACTIVATION_PROCESS_INVALID:
            return {
                ...state,
                activationProcessRejected: {
                    ...initialState.activationProcessRejected,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_REJECT_ACTIVATION_PROCESS_ERROR:
            return {
                ...state,
                activationProcessRejected: {
                    ...initialState.activationProcessRejected,
                    response: action.response,
                    error: action.error
                }
            }

        // Get Pending Processes
        case ODC_REPRINT_GET_PENDING_PROCESSES_REQUEST:
            return {
                ...state,
                pendingProcesses: {
                    ...initialState.pendingProcesses,
                    loading: true
                }
            }
        case ODC_REPRINT_GET_PENDING_PROCESSES_SUCCESS:
            return {
                ...state,
                pendingProcesses: {
                    ...initialState.pendingProcesses,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_GET_PENDING_PROCESSES_INVALID:
            return {
                ...state,
                pendingProcesses: {
                    ...initialState.pendingProcesses,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_GET_PENDING_PROCESSES_ERROR:
            return {
                ...state,
                pendingProcesses: {
                    ...initialState.pendingProcesses,
                    response: action.response,
                    error: action.error
                }
            }

        // Register Observation Validated
        case ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_REQUEST:
            return {
                ...state,
                observationValidatedRegistered: {
                    ...initialState.observationValidatedRegistered,
                    loading: true
                }
            }
        case ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_SUCCESS:
            return {
                ...state,
                observationValidatedRegistered: {
                    ...initialState.observationValidatedRegistered,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_INVALID:
            return {
                ...state,
                observationValidatedRegistered: {
                    ...initialState.observationValidatedRegistered,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_ERROR:
            return {
                ...state,
                observationValidatedRegistered: {
                    ...initialState.observationValidatedRegistered,
                    response: action.response,
                    error: action.error
                }
            }

        // Get Reprints For Searching
        case ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_REQUEST:
            return {
                ...state,
                reprintsForSearching: {
                    ...initialState.reprintsForSearching,
                    loading: true
                }
            }
        case ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_SUCCESS:
            return {
                ...state,
                reprintsForSearching: {
                    ...initialState.reprintsForSearching,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_INVALID:
            return {
                ...state,
                reprintsForSearching: {
                    ...initialState.reprintsForSearching,
                    error: action.error,
                    response: true
                }
            }
        case ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_ERROR:
            return {
                ...state,
                reprintsForSearching: {
                    ...initialState.reprintsForSearching,
                    response: action.response,
                    error: action.error
                }
            }

        default:
            return state
    }
}

export default odcReprintReducer