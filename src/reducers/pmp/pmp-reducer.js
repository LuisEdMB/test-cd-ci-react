import {

    PMP_CREATE_CLIENT_REQUEST,
    PMP_CREATE_CLIENT_INVALID,
    PMP_CREATE_CLIENT_SUCCESS, 
    PMP_CREATE_CLIENT_ERROR,

    PMP_UPDATE_CLIENT_REQUEST,
    PMP_UPDATE_CLIENT_INVALID,
    PMP_UPDATE_CLIENT_SUCCESS, 
    PMP_UPDATE_CLIENT_ERROR,

    PMP_CREATE_RELATIONSHIP_REQUEST,
    PMP_CREATE_RELATIONSHIP_INVALID,
    PMP_CREATE_RELATIONSHIP_SUCCESS,
    PMP_CREATE_RELATIONSHIP_ERROR,

    PMP_CREATE_ACCOUNT_REQUEST,
    PMP_CREATE_ACCOUNT_INVALID,
    PMP_CREATE_ACCOUNT_SUCCESS,
    PMP_CREATE_ACCOUNT_ERROR,

    PMP_CREATE_CREDIT_CARD_REQUEST,
    PMP_CREATE_CREDIT_CARD_INVALID,
    PMP_CREATE_CREDIT_CARD_SUCCESS,
    PMP_CREATE_CREDIT_CARD_ERROR,

    CLIENTE_VALIDA_CUENTA_REQUEST,
    CLIENTE_VALIDA_CUENTA_INVALID,
    CLIENTE_VALIDA_CUENTA_SUCCESS,
    CLIENTE_VALIDA_CUENTA_ERROR,

    PMP_BLOCK_TYPE_CREDIT_CARD_REQUEST,
    PMP_BLOCK_TYPE_CREDIT_CARD_SUCCESS,
    PMP_BLOCK_TYPE_CREDIT_CARD_INVALID, 
    PMP_BLOCK_TYPE_CREDIT_CARD_ERROR,

    PMP_CREATE_CREDIT_CARD_SAE_REQUEST,
    PMP_CREATE_CREDIT_CARD_SAE_INVALID,
    PMP_CREATE_CREDIT_CARD_SAE_SUCCESS,
    PMP_CREATE_CREDIT_CARD_SAE_ERROR,

    PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_REQUEST,
    PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_INVALID,
    PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_SUCCESS,
    PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_ERROR,

    // Block Credit Card Reprint
    PMP_REPRINT_BLOCK_CREDIT_CARD_REQUEST,
    PMP_REPRINT_BLOCK_CREDIT_CARD_SUCCESS,
    PMP_REPRINT_BLOCK_CREDIT_CARD_INVALID,
    PMP_REPRINT_BLOCK_CREDIT_CARD_ERROR,

    // Activate Credit Card Reprint
    PMP_REPRINT_ACTIVATE_CREDIT_CARD_REQUEST,
    PMP_REPRINT_ACTIVATE_CREDIT_CARD_SUCCESS,
    PMP_REPRINT_ACTIVATE_CREDIT_CARD_INVALID,
    PMP_REPRINT_ACTIVATE_CREDIT_CARD_ERROR
} from '../../actions/pmp/pmp';

var stateInitital = { 
    createClient:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    updateClient:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    createRelationship:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    createAccount:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    clienteValidaTC:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    createCreditCard:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    blockTypeCreditCard:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    createSAE:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null, 
    },
    updateClientSavePMP:{
        response: false,
        success: false,
        data: null, 
        error: null,
        loading: false, 
        extra: null
    },
    creditCardReprintBlocked: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    },
    creditCardReprintActivated: {
        loading: false,
        data: null,
        success: false,
        error: null,
        response: false
    }
};

const pmpReducer = (state = stateInitital, action) =>
{
    switch (action.type) {
        case PMP_CREATE_CLIENT_REQUEST:
            return {
                ...state, 
                createClient:{
                    ...state.createClient,
                    loading:true, 
                }
            }
        case PMP_CREATE_CLIENT_SUCCESS:
            return { 
                ...state,
                createClient:{
                    ...state.createClient,
                    data: action.data, 
                    extra: action.extra,
                    success: true,
                    response: true,
                    loading: false,
                    error: null
                }
            }
        case PMP_CREATE_CLIENT_INVALID: 
            return {
                ...state,
                createClient:{
                    ...state.createClient,
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case PMP_CREATE_CLIENT_ERROR:
            return { 
                ...state,
                createClient:{
                    ...state.createClient, 
                    data: null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case PMP_UPDATE_CLIENT_REQUEST:
            return {
                ...state, 
                updateClient:{
                    ...state.updateClient,
                    loading:true, 
                }
            }
        case PMP_UPDATE_CLIENT_SUCCESS:
            return { 
                ...state,
                updateClient:{
                    ...state.updateClient,
                    data: action.data, 
                    extra: action.extra,
                    success: true,
                    response: true,
                    loading: false,
                    error: null
                }
            }
        case PMP_UPDATE_CLIENT_INVALID: 
            return {
                ...state,
                updateClient:{
                    ...state.updateClient,
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case PMP_UPDATE_CLIENT_ERROR:
            return { 
                ...state,
                updateClient:{
                    ...state.updateClient, 
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }

        case PMP_CREATE_RELATIONSHIP_REQUEST:
            return {
                ...state, 
                createRelationship:{
                    ...state.createRelationship,
                    loading:true, 
                }
            }
        case PMP_CREATE_RELATIONSHIP_SUCCESS:
            return { 
                ...state,
                createRelationship:{
                    ...state.createRelationship,
                    data: action.data, 
                    extra: action.extra,
                    success: true,
                    response: true,
                    loading: false,
                    error: null
                }
            }
        case PMP_CREATE_RELATIONSHIP_INVALID: 
            return {
                ...state,
                createRelationship:{
                    ...state.createRelationship,
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case PMP_CREATE_RELATIONSHIP_ERROR:
            return { 
                ...state,
                createRelationship:{
                    ...state.createRelationship, 
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }

        
        case PMP_CREATE_ACCOUNT_REQUEST:
            return {
                ...state, 
                createAccount:{
                    ...state.createAccount,
                    loading:true, 
                }
            }
        case PMP_CREATE_ACCOUNT_SUCCESS:
            return { 
                ...state,
                createAccount:{
                    ...state.createAccount,
                    data: action.data, 
                    extra: action.extra,
                    success: true,
                    response: true,
                    loading: false,
                    error: null
                }
            }
        case PMP_CREATE_ACCOUNT_INVALID: 
            return {
                ...state,
                createAccount:{
                    ...state.createAccount,
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case PMP_CREATE_ACCOUNT_ERROR:
            return { 
                ...state,
                createAccount:{
                    ...state.createAccount, 
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
            case CLIENTE_VALIDA_CUENTA_REQUEST:
                return {
                    ...state, 
                    clienteValidaTC:{
                        ...state.clienteValidaTC,
                        loading:true, 
                    }
                }
            case CLIENTE_VALIDA_CUENTA_SUCCESS:
                return { 
                    ...state,
                    clienteValidaTC:{
                        ...state.clienteValidaTC,
                        data: action.data, 
                        extra: action.extra,
                        success: true,
                        response: true,
                        loading: false,
                        error: null
                    }
                }
            case CLIENTE_VALIDA_CUENTA_INVALID: 
                return {
                    ...state,
                    clienteValidaTC:{
                        ...state.clienteValidaTC,
                        data:null,
                        extra: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
            case CLIENTE_VALIDA_CUENTA_ERROR:
                return { 
                    ...state,
                    clienteValidaTC:{
                        ...state.clienteValidaTC, 
                        data:null,
                        extra: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
        case PMP_CREATE_CREDIT_CARD_REQUEST:
            return {
                ...state, 
                createCreditCard:{
                    ...state.createCreditCard,
                    loading:true, 
                }
            }
        case PMP_CREATE_CREDIT_CARD_SUCCESS:
            return { 
                ...state,
                createCreditCard:{
                    ...state.createCreditCard,
                    data: action.data, 
                    extra: action.extra,
                    success: true,
                    response: true,
                    loading: false,
                    error: null
                }
            }
        case PMP_CREATE_CREDIT_CARD_INVALID: 
            return {
                ...state,
                createCreditCard:{
                    ...state.createCreditCard,
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case PMP_CREATE_CREDIT_CARD_ERROR:
            return { 
                ...state,
                createCreditCard:{
                    ...state.createCreditCard, 
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case PMP_BLOCK_TYPE_CREDIT_CARD_REQUEST:
            return {
                ...state, 
                blockTypeCreditCard:{
                    ...state.blockTypeCreditCard,
                    loading:true, 
                }
            }
        case PMP_BLOCK_TYPE_CREDIT_CARD_SUCCESS:
            return { 
                ...state,
                blockTypeCreditCard:{
                    ...state.blockTypeCreditCard,
                    data: action.data, 
                    extra: action.extra,
                    success: true,
                    response: true,
                    loading: false,
                    error: null
                }
            }
        case PMP_BLOCK_TYPE_CREDIT_CARD_INVALID: 
            return {
                ...state,
                blockTypeCreditCard:{
                    ...state.blockTypeCreditCard,
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case PMP_BLOCK_TYPE_CREDIT_CARD_ERROR:
            return { 
                ...state,
                blockTypeCreditCard:{
                    ...state.blockTypeCreditCard, 
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }

        case PMP_CREATE_CREDIT_CARD_SAE_REQUEST:
            return {
                ...state, 
                createSAE:{
                    ...state.createSAE,
                    loading:true, 
                }
            }
        case PMP_CREATE_CREDIT_CARD_SAE_SUCCESS:
            return { 
                ...state,
                createSAE:{
                    ...state.createSAE,
                    data: action.data, 
                    extra: action.extra,
                    success: true,
                    response: true,
                    loading: false,
                    error: null
                }
            }
        case PMP_CREATE_CREDIT_CARD_SAE_INVALID: 
            return {
                ...state,
                createSAE:{
                    ...state.createSAE,
                    data:null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
        case PMP_CREATE_CREDIT_CARD_SAE_ERROR:
            return { 
                ...state,
                createSAE:{
                    ...state.createSAE, 
                    data: null,
                    extra: null,
                    success:false,
                    loading:false,
                    response:action.response,
                    error:action.error,
                }
            }
            case PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_REQUEST:
                return {
                    ...state, 
                    updateClientSavePMP:{
                        ...state.updateClientSavePMP,
                        loading:true, 
                    }
                }
            case PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_SUCCESS:
                return { 
                    ...state,
                    updateClientSavePMP:{
                        ...state.updateClientSavePMP,
                        data: action.data, 
                        extra: action.extra,
                        success: true,
                        response: true,
                        loading: false,
                        error: null
                    }
                }
            case PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_INVALID: 
                return {
                    ...state,
                    updateClientSavePMP:{
                        ...state.updateClientSavePMP,
                        data:null,
                        extra: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
            case PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_ERROR:
                return { 
                    ...state,
                    updateClientSavePMP:{
                        ...state.updateClientSavePMP, 
                        data: null,
                        extra: null,
                        success:false,
                        loading:false,
                        response:action.response,
                        error:action.error,
                    }
                }
        
        // Block Credit Card Reprint
        case PMP_REPRINT_BLOCK_CREDIT_CARD_REQUEST:
            return {
                ...state,
                creditCardReprintBlocked: {
                    ...stateInitital.creditCardReprintBlocked,
                    loading: true
                }
            }
        case PMP_REPRINT_BLOCK_CREDIT_CARD_SUCCESS:
            return {
                ...state,
                creditCardReprintBlocked: {
                    ...stateInitital.creditCardReprintBlocked,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case PMP_REPRINT_BLOCK_CREDIT_CARD_INVALID:
            return {
                ...state,
                creditCardReprintBlocked: {
                    ...stateInitital.creditCardReprintBlocked,
                    error: action.error,
                    response: true
                }
            }
        case PMP_REPRINT_BLOCK_CREDIT_CARD_ERROR:
            return {
                ...state,
                creditCardReprintBlocked: {
                    ...stateInitital.creditCardReprintBlocked,
                    response: action.response,
                    error: action.error
                }
            }

        // Block Credit Card Reprint
        case PMP_REPRINT_ACTIVATE_CREDIT_CARD_REQUEST:
            return {
                ...state,
                creditCardReprintActivated: {
                    ...stateInitital.creditCardReprintActivated,
                    loading: true
                }
            }
        case PMP_REPRINT_ACTIVATE_CREDIT_CARD_SUCCESS:
            return {
                ...state,
                creditCardReprintActivated: {
                    ...stateInitital.creditCardReprintActivated,
                    data: action.data,
                    success: true,
                    response: true
                }
            }
        case PMP_REPRINT_ACTIVATE_CREDIT_CARD_INVALID:
            return {
                ...state,
                creditCardReprintActivated: {
                    ...stateInitital.creditCardReprintActivated,
                    error: action.error,
                    response: true
                }
            }
        case PMP_REPRINT_ACTIVATE_CREDIT_CARD_ERROR:
            return {
                ...state,
                creditCardReprintActivated: {
                    ...stateInitital.creditCardReprintActivated,
                    response: action.response,
                    error: action.error
                }
            }
        
        default:
            return state;
    }
}
export default pmpReducer;