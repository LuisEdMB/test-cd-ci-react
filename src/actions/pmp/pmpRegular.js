import axios from 'axios';
import * as moment from 'moment';
import { URL_ODC_REGULAR, SERVER_ERROR_MESSAGE } from '../config';
import { decrypt, getDeviceData, HtmlEncode } from '../../utils/Utils';

// Actions 
export const PMP_CREATE_CLIENT_REQUEST = 'PMP_CREATE_CLIENT_REQUEST'; 
export const PMP_CREATE_CLIENT_INVALID = 'PMP_CREATE_CLIENT_INVALID'; 
export const PMP_CREATE_CLIENT_SUCCESS = 'PMP_CREATE_CLIENT_SUCCESS'; 
export const PMP_CREATE_CLIENT_ERROR = 'PMP_CREATE_CLIENT_ERROR';

export const PMP_UPDATE_CLIENT_REQUEST = 'PMP_UPDATE_CLIENT_REQUEST'; 
export const PMP_UPDATE_CLIENT_INVALID = 'PMP_UPDATE_CLIENT_INVALID'; 
export const PMP_UPDATE_CLIENT_SUCCESS = 'PMP_UPDATE_CLIENT_SUCCESS'; 
export const PMP_UPDATE_CLIENT_ERROR = 'PMP_UPDATE_CLIENT_ERROR';

export const PMP_CREATE_RELATIONSHIP_REQUEST = 'PMP_CREATE_RELATIONSHIP_REQUEST'; 
export const PMP_CREATE_RELATIONSHIP_INVALID = 'PMP_CREATE_RELATIONSHIP_INVALID'; 
export const PMP_CREATE_RELATIONSHIP_SUCCESS = 'PMP_CREATE_RELATIONSHIP_SUCCESS'; 
export const PMP_CREATE_RELATIONSHIP_ERROR = 'PMP_CREATE_RELATIONSHIP_ERROR';

export const PMP_CREATE_ACCOUNT_REQUEST = 'PMP_CREATE_ACCOUNT_REQUEST'; 
export const PMP_CREATE_ACCOUNT_INVALID = 'PMP_CREATE_ACCOUNT_INVALID'; 
export const PMP_CREATE_ACCOUNT_SUCCESS = 'PMP_CREATE_ACCOUNT_SUCCESS'; 
export const PMP_CREATE_ACCOUNT_ERROR = 'PMP_CREATE_ACCOUNT_ERROR';

export const PMP_CREATE_CREDIT_CARD_REQUEST = 'PMP_CREATE_CREDIT_CARD_REQUEST'; 
export const PMP_CREATE_CREDIT_CARD_INVALID = 'PMP_CREATE_CREDIT_CARD_INVALID'; 
export const PMP_CREATE_CREDIT_CARD_SUCCESS = 'PMP_CREATE_CREDIT_CARD_SUCCESS'; 
export const PMP_CREATE_CREDIT_CARD_ERROR = 'PMP_CREATE_CREDIT_CARD_ERROR';

export const CLIENTE_VALIDA_CUENTA_REQUEST = 'CLIENTE_VALIDA_CUENTA_REQUEST'; 
export const CLIENTE_VALIDA_CUENTA_INVALID = 'CLIENTE_VALIDA_CUENTA_INVALID'; 
export const CLIENTE_VALIDA_CUENTA_SUCCESS = 'CLIENTE_VALIDA_CUENTA_SUCCESS'; 
export const CLIENTE_VALIDA_CUENTA_ERROR = 'CLIENTE_VALIDA_CUENTA_ERROR';

export const PMP_CREATE_CREDIT_CARD_SAE_REQUEST = 'PMP_CREATE_CREDIT_CARD_SAE_REQUEST'; 
export const PMP_CREATE_CREDIT_CARD_SAE_INVALID = 'PMP_CREATE_CREDIT_CARD_SAE_INVALID'; 
export const PMP_CREATE_CREDIT_CARD_SAE_SUCCESS = 'PMP_CREATE_CREDIT_CARD_SAE_SUCCESS'; 
export const PMP_CREATE_CREDIT_CARD_SAE_ERROR = 'PMP_CREATE_CREDIT_CARD_SAE_ERROR';

export const PMP_BLOCK_TYPE_CREDIT_CARD_REQUEST = 'PMP_BLOCK_TYPE_CREDIT_CARD_REQUEST'; 
export const PMP_BLOCK_TYPE_CREDIT_CARD_SUCCESS = 'PMP_BLOCK_TYPE_CREDIT_CARD_SUCCESS'; 
export const PMP_BLOCK_TYPE_CREDIT_CARD_INVALID = 'PMP_BLOCK_TYPE_CREDIT_CARD_INVALID'; 
export const PMP_BLOCK_TYPE_CREDIT_CARD_ERROR = 'PMP_BLOCK_TYPE_CREDIT_CARD_ERROR';

// Actions - Update-Client-Data-Save-PMP
export const PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_REQUEST = 'PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_REQUEST';
export const PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_SUCCESS = 'PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_SUCCESS';
export const PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_INVALID = 'PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_INVALID';
export const PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_ERROR = 'PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_ERROR';


export function createClient(data, extra=null){
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Begin Request
        dispatch({type: PMP_CREATE_CLIENT_REQUEST});
        let sendData = JSON.stringify({
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: device.ip,
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),

            cod_solicitud: data.cod_solicitud? data.cod_solicitud: 0,
            cod_solicitud_completa: data.cod_solicitud_completa? data.cod_solicitud_completa: "",
            cod_cliente: data.cod_cliente? data.cod_cliente: 0,
            cod_cliente_titular: data.cod_cliente_titular? data.cod_cliente_titular: 0,

            terceraLlamadaPmpCrearCliente:{
                tipo_doc: data.terceraLlamadaPmpCrearCliente.tipo_doc,
                nro_doc: data.terceraLlamadaPmpCrearCliente.nro_doc,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg: decryptData.username,
                des_ter_reg: device.ip,
                
                organization: data.terceraLlamadaPmpCrearCliente.organization,
                identityDocumentType: data.terceraLlamadaPmpCrearCliente.identityDocumentType && data.terceraLlamadaPmpCrearCliente.identityDocumentType.substring(0, 30),
                identityDocumentNumber: data.terceraLlamadaPmpCrearCliente.identityDocumentNumber && data.terceraLlamadaPmpCrearCliente.identityDocumentNumber.substring(0, 30),
                nameLine1Cliente: data.terceraLlamadaPmpCrearCliente.nameLine1Cliente && data.terceraLlamadaPmpCrearCliente.nameLine1Cliente.substring(0, 30),
                statusCliente: data.terceraLlamadaPmpCrearCliente.statusCliente,
                nameLine2Cliente: data.terceraLlamadaPmpCrearCliente.nameLine2Cliente && data.terceraLlamadaPmpCrearCliente.nameLine2Cliente.substring(0, 25),
                nameLine3Cliente: data.terceraLlamadaPmpCrearCliente.nameLine3Cliente && data.terceraLlamadaPmpCrearCliente.nameLine3Cliente.substring(0, 25),
                stmtMsgIndicCliente: data.terceraLlamadaPmpCrearCliente.stmtMsgIndicCliente,
                addrLine1Cliente: data.terceraLlamadaPmpCrearCliente.addrLine1Cliente && data.terceraLlamadaPmpCrearCliente.addrLine1Cliente.substring(0, 30),
                mailyngListCliente: data.terceraLlamadaPmpCrearCliente.mailyngListCliente, 
                addrLine2Cliente: data.terceraLlamadaPmpCrearCliente.addrLine2Cliente && data.terceraLlamadaPmpCrearCliente.addrLine2Cliente.substring(0, 30),
                carrierRoute12Cliente: data.terceraLlamadaPmpCrearCliente.carrierRoute12Cliente,
                carrierRoute34Cliente: data.terceraLlamadaPmpCrearCliente.carrierRoute34Cliente,
                cityCliente: data.terceraLlamadaPmpCrearCliente.cityCliente && data.terceraLlamadaPmpCrearCliente.cityCliente.substring(0, 22),
                stateCliente: data.terceraLlamadaPmpCrearCliente.stateCliente && data.terceraLlamadaPmpCrearCliente.stateCliente.substring(0,3),
                zipCodeCliente: data.terceraLlamadaPmpCrearCliente.zipCodeCliente.toString(),
                nameTypeIndNl1Cliente: data.terceraLlamadaPmpCrearCliente.nameTypeIndNl1Cliente,
                nameTypeIndNl2Cliente: data.terceraLlamadaPmpCrearCliente.nameTypeIndNl2Cliente,
                nameTypeIndNl3Cliente: data.terceraLlamadaPmpCrearCliente.nameTypeIndNl3Cliente,
                ownRentResidFlagCliente: data.terceraLlamadaPmpCrearCliente.ownRentResidFlagCliente,
                genderCliente: data.terceraLlamadaPmpCrearCliente.genderCliente,
                relativeNameCliente: data.terceraLlamadaPmpCrearCliente.relativeNameCliente && data.terceraLlamadaPmpCrearCliente.relativeNameCliente.substring(0, 30),
                numberOfDependentsCliente: data.terceraLlamadaPmpCrearCliente.numberOfDependentsCliente,
                makerEmployerCliente: HtmlEncode(data.terceraLlamadaPmpCrearCliente.makerEmployerCliente) && HtmlEncode(data.terceraLlamadaPmpCrearCliente.makerEmployerCliente.substring(0, 30)),
                makerPhoneFlagCliente: data.terceraLlamadaPmpCrearCliente.makerPhoneFlagCliente,
                makerEmpPhoneCliente: data.terceraLlamadaPmpCrearCliente.makerEmpPhoneCliente && data.terceraLlamadaPmpCrearCliente.makerEmpPhoneCliente.substring(0, 12),
                makerPositionCliente: data.terceraLlamadaPmpCrearCliente.makerPositionCliente && data.terceraLlamadaPmpCrearCliente.makerPositionCliente.substring(0, 30),
                makerHomePhoneCliente: data.terceraLlamadaPmpCrearCliente.makerHomePhoneCliente && data.terceraLlamadaPmpCrearCliente.makerHomePhoneCliente.substring(0, 12),
                makerSsanFlagCliente: data.terceraLlamadaPmpCrearCliente.makerSsanFlagCliente,
                makerSsanCliente: data.terceraLlamadaPmpCrearCliente.makerSsanCliente,
                dlStateCliente: data.terceraLlamadaPmpCrearCliente.dlStateCliente,
                dlNumberCliente: data.terceraLlamadaPmpCrearCliente.dlNumberCliente,
                languageIndicCliente: data.terceraLlamadaPmpCrearCliente.languageIndicCliente,
                w9IndicCliente: data.terceraLlamadaPmpCrearCliente.w9IndicCliente,
                w9DateSentCliente: data.terceraLlamadaPmpCrearCliente.w9DateSentCliente,
                w9DateFiledCliente: data.terceraLlamadaPmpCrearCliente.w9DateFiledCliente,
                coMakerNameCliente: data.terceraLlamadaPmpCrearCliente.coMakerNameCliente,
                coNameTypeIndCliente: data.terceraLlamadaPmpCrearCliente.coNameTypeIndCliente,
                coMakerEmployerCliente: HtmlEncode(data.terceraLlamadaPmpCrearCliente.coMakerEmployerCliente),
                coMakerPhoneFlagCliente: data.terceraLlamadaPmpCrearCliente.coMakerPhoneFlagCliente,
                coMakerEmpPhoneCliente: data.terceraLlamadaPmpCrearCliente.coMakerEmpPhoneCliente && data.terceraLlamadaPmpCrearCliente.coMakerEmpPhoneCliente.substring(0, 11),
                coMakerPositionCliente: data.terceraLlamadaPmpCrearCliente.coMakerPositionCliente,
                coMakerHomePhoneCliente: data.terceraLlamadaPmpCrearCliente.coMakerHomePhoneCliente,
                coMakerSsanFlagCliente: data.terceraLlamadaPmpCrearCliente.coMakerSsanFlagCliente,
                coMakerSsanCliente: data.terceraLlamadaPmpCrearCliente.coMakerSsanCliente,
                userDemo1Cliente: data.terceraLlamadaPmpCrearCliente.userDemo1Cliente,
                userDemo2Cliente: data.terceraLlamadaPmpCrearCliente.userDemo2Cliente,
                memoLine1Cliente: data.terceraLlamadaPmpCrearCliente.memoLine1Cliente,
                memoLine2Cliente: data.terceraLlamadaPmpCrearCliente.memoLine2Cliente,
                lastMaintDateCliente: data.terceraLlamadaPmpCrearCliente.lastMaintDateCliente,
                customerNumberCliente: data.terceraLlamadaPmpCrearCliente.customerNumberCliente,
                relationshipNumberCliente: data.terceraLlamadaPmpCrearCliente.relationshipNumberCliente,
                accountNumberCliente: data.terceraLlamadaPmpCrearCliente.accountNumberCliente,
                cardNumberCliente: data.terceraLlamadaPmpCrearCliente.cardNumberCliente, 
                cardSeqCliente: data.terceraLlamadaPmpCrearCliente.cardSeqCliente,
                celNumberCliente: data.terceraLlamadaPmpCrearCliente.celNumberCliente,
                trackTypeCliente: data.terceraLlamadaPmpCrearCliente.trackTypeCliente,
                trackNameCliente: data.terceraLlamadaPmpCrearCliente.trackNameCliente && data.terceraLlamadaPmpCrearCliente.trackNameCliente.substring(0, 30),
                numberCliente: data.terceraLlamadaPmpCrearCliente.numberCliente && data.terceraLlamadaPmpCrearCliente.numberCliente.substring(0, 6),
                departmentNumberCliente: data.terceraLlamadaPmpCrearCliente.departmentNumberCliente && data.terceraLlamadaPmpCrearCliente.departmentNumberCliente.substring(0, 6),
                officeCliente: data.terceraLlamadaPmpCrearCliente.officeCliente && data.terceraLlamadaPmpCrearCliente.officeCliente.substring(0, 3),
                floorCliente: data.terceraLlamadaPmpCrearCliente.floorCliente && data.terceraLlamadaPmpCrearCliente.floorCliente.substring(0, 3),
                blockCliente: data.terceraLlamadaPmpCrearCliente.blockCliente && data.terceraLlamadaPmpCrearCliente.blockCliente.substring(0, 3),
                lotCliente: data.terceraLlamadaPmpCrearCliente.lotCliente && data.terceraLlamadaPmpCrearCliente.lotCliente.substring(0, 3),
                interiorCliente: data.terceraLlamadaPmpCrearCliente.interiorCliente && data.terceraLlamadaPmpCrearCliente.interiorCliente.substring(0, 3),
                sectorCliente: data.terceraLlamadaPmpCrearCliente.sectorCliente && data.terceraLlamadaPmpCrearCliente.sectorCliente.substring(0, 3),
                kilometerCliente: data.terceraLlamadaPmpCrearCliente.kilometerCliente && data.terceraLlamadaPmpCrearCliente.kilometerCliente.substring(0, 3),
                zoneTypeCliente: data.terceraLlamadaPmpCrearCliente.zoneTypeCliente && data.terceraLlamadaPmpCrearCliente.zoneTypeCliente.substring(0, 30),
                zoneNameCliente: data.terceraLlamadaPmpCrearCliente.zoneNameCliente,
                referenceCliente: data.terceraLlamadaPmpCrearCliente.referenceCliente,
                geoLocationCliente: data.terceraLlamadaPmpCrearCliente.geoLocationCliente,
                workTypeCliente: data.terceraLlamadaPmpCrearCliente.workTypeCliente,
                workTrackNameCliente: data.terceraLlamadaPmpCrearCliente.workTrackNameCliente && data.terceraLlamadaPmpCrearCliente.workTrackNameCliente.substring(0, 30),
                workNumberCliente: data.terceraLlamadaPmpCrearCliente.workNumberCliente && data.terceraLlamadaPmpCrearCliente.workNumberCliente.substring(0, 6),
                workDepartmentNumberCliente: data.terceraLlamadaPmpCrearCliente.workDepartmentNumberCliente && data.terceraLlamadaPmpCrearCliente.workDepartmentNumberCliente.substring(0, 6),
                workOfficeCliente: data.terceraLlamadaPmpCrearCliente.workOfficeCliente && data.terceraLlamadaPmpCrearCliente.workOfficeCliente.substring(0, 3),
                workFloorCliente: data.terceraLlamadaPmpCrearCliente.workFloorCliente && data.terceraLlamadaPmpCrearCliente.workFloorCliente.substring(0, 3),
                workBlockCliente: data.terceraLlamadaPmpCrearCliente.workBlockCliente && data.terceraLlamadaPmpCrearCliente.workBlockCliente.substring(0, 3),
                workLotCliente: data.terceraLlamadaPmpCrearCliente.workLotCliente && data.terceraLlamadaPmpCrearCliente.workLotCliente.substring(0, 3),
                workInteriorCliente: data.terceraLlamadaPmpCrearCliente.workInteriorCliente && data.terceraLlamadaPmpCrearCliente.workInteriorCliente.substring(0, 3),
                workSectorCliente: data.terceraLlamadaPmpCrearCliente.workSectorCliente && data.terceraLlamadaPmpCrearCliente.workSectorCliente.substring(0, 3),
                workKilometerCliente: data.terceraLlamadaPmpCrearCliente.workKilometerCliente && data.terceraLlamadaPmpCrearCliente.workKilometerCliente.substring(0, 3),
                workZoneTypeCliente: data.terceraLlamadaPmpCrearCliente.workZoneTypeCliente,
                workZoneNameCliente: data.terceraLlamadaPmpCrearCliente.workZoneNameCliente && data.terceraLlamadaPmpCrearCliente.workZoneNameCliente.substring(0, 30),
                workReferenceCliente: data.terceraLlamadaPmpCrearCliente.workReferenceCliente,
                workGeoLocationCliente: data.terceraLlamadaPmpCrearCliente.workGeoLocationCliente,
                dualCoinFlagCliente: data.terceraLlamadaPmpCrearCliente.dualCoinFlagCliente,
                homeAddressModificationDateCliente: data.terceraLlamadaPmpCrearCliente.homeAddressModificationDateCliente,
                workAddressModificationDateCliente: data.terceraLlamadaPmpCrearCliente.workAddressModificationDateCliente
            }
        });
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Pmp/PmpCrearCliente`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    // Launch Dispatch 
                    if(response.data.success){
                        dispatch({ type: PMP_CREATE_CLIENT_SUCCESS, data:response.data, extra:extra }); 
                    }
                    else{
                        dispatch({ 
                            type: PMP_CREATE_CLIENT_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: PMP_CREATE_CLIENT_ERROR, error:newError, response: response});
                return error.response? error.response: null;
            });
    }
}
export function updateClient(data, extra=null){
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Begin Request
        dispatch({type: PMP_UPDATE_CLIENT_REQUEST});
        let sendData = JSON.stringify({
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: device.ip,
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),

            cod_solicitud: data.cod_solicitud? data.cod_solicitud: 0,
            cod_solicitud_completa: data.cod_solicitud_completa? data.cod_solicitud_completa: "",
            cod_cliente: data.cod_cliente? data.cod_cliente: 0,
            cod_cliente_titular: data.cod_cliente_titular? data.cod_cliente_titular: 0,

            terceraLlamadaPmpActualizarCliente:{
                tipo_doc: data.terceraLlamadaPmpActualizarCliente.tipo_doc,
                nro_doc: data.terceraLlamadaPmpActualizarCliente.nro_doc,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg: decryptData.username,
                des_ter_reg: device.ip,

                organization: data.terceraLlamadaPmpActualizarCliente.organization,
                identityDocumentType: data.terceraLlamadaPmpActualizarCliente.identityDocumentType && data.terceraLlamadaPmpActualizarCliente.identityDocumentType.substring(0, 30),
                identityDocumentNumber: data.terceraLlamadaPmpActualizarCliente.identityDocumentNumber && data.terceraLlamadaPmpActualizarCliente.identityDocumentNumber.substring(0, 30),
                nameLine1Cliente: data.terceraLlamadaPmpActualizarCliente.nameLine1Cliente && data.terceraLlamadaPmpActualizarCliente.nameLine1Cliente.substring(0, 30),
                statusCliente: data.terceraLlamadaPmpActualizarCliente.statusCliente,
                nameLine2Cliente: data.terceraLlamadaPmpActualizarCliente.nameLine2Cliente && data.terceraLlamadaPmpActualizarCliente.nameLine2Cliente.substring(0, 25),
                nameLine3Cliente: data.terceraLlamadaPmpActualizarCliente.nameLine3Cliente && data.terceraLlamadaPmpActualizarCliente.nameLine3Cliente.substring(0, 25),
                stmtMsgIndicCliente: data.terceraLlamadaPmpActualizarCliente.stmtMsgIndicCliente,
                addrLine1Cliente: data.terceraLlamadaPmpActualizarCliente.addrLine1Cliente && data.terceraLlamadaPmpActualizarCliente.addrLine1Cliente.substring(0, 30),
                mailyngListCliente: data.terceraLlamadaPmpActualizarCliente.mailyngListCliente,
                addrLine2Cliente: data.terceraLlamadaPmpActualizarCliente.addrLine2Cliente && data.terceraLlamadaPmpActualizarCliente.addrLine2Cliente.substring(0, 30),
                carrierRoute12Cliente: data.terceraLlamadaPmpActualizarCliente.carrierRoute12Cliente,
                carrierRoute34Cliente: data.terceraLlamadaPmpActualizarCliente.carrierRoute34Cliente,
                cityCliente: data.terceraLlamadaPmpActualizarCliente.cityCliente && data.terceraLlamadaPmpActualizarCliente.cityCliente.substring(0, 22),
                stateCliente: data.terceraLlamadaPmpActualizarCliente.stateCliente && data.terceraLlamadaPmpActualizarCliente.stateCliente.substring(0,3),
                zipCodeCliente: data.terceraLlamadaPmpActualizarCliente.zipCodeCliente.toString(),
                nameTypeIndNl1Cliente: data.terceraLlamadaPmpActualizarCliente.nameTypeIndNl1Cliente,
                nameTypeIndNl2Cliente: data.terceraLlamadaPmpActualizarCliente.nameTypeIndNl2Cliente,
                nameTypeIndNl3Cliente: data.terceraLlamadaPmpActualizarCliente.nameTypeIndNl3Cliente,
                ownRentResidFlagCliente: data.terceraLlamadaPmpActualizarCliente.ownRentResidFlagCliente,
                genderCliente: data.terceraLlamadaPmpActualizarCliente.genderCliente,
                relativeNameCliente: data.terceraLlamadaPmpActualizarCliente.relativeNameCliente && data.terceraLlamadaPmpActualizarCliente.relativeNameCliente.substring(0, 30),
                numberOfDependentsCliente: data.terceraLlamadaPmpActualizarCliente.numberOfDependentsCliente,
                makerEmployerCliente: HtmlEncode(data.terceraLlamadaPmpActualizarCliente.makerEmployerCliente) && HtmlEncode(data.terceraLlamadaPmpActualizarCliente.makerEmployerCliente.substring(0, 30)),
                makerPhoneFlagCliente: data.terceraLlamadaPmpActualizarCliente.makerPhoneFlagCliente,
                makerEmpPhoneCliente: data.terceraLlamadaPmpActualizarCliente.makerEmpPhoneCliente && data.terceraLlamadaPmpActualizarCliente.makerEmpPhoneCliente.substring(0, 12),
                makerPositionCliente: data.terceraLlamadaPmpActualizarCliente.makerPositionCliente && data.terceraLlamadaPmpActualizarCliente.makerPositionCliente.substring(0, 30),
                makerHomePhoneCliente: data.terceraLlamadaPmpActualizarCliente.makerHomePhoneCliente && data.terceraLlamadaPmpActualizarCliente.makerHomePhoneCliente.substring(0, 12),
                makerSsanFlagCliente: data.terceraLlamadaPmpActualizarCliente.makerSsanFlagCliente,
                makerSsanCliente: data.terceraLlamadaPmpActualizarCliente.makerSsanCliente,
                dlStateCliente: data.terceraLlamadaPmpActualizarCliente.dlStateCliente,
                dlNumberCliente: data.terceraLlamadaPmpActualizarCliente.dlNumberCliente,
                languageIndicCliente: data.terceraLlamadaPmpActualizarCliente.languageIndicCliente,
                w9IndicCliente: data.terceraLlamadaPmpActualizarCliente.w9IndicCliente,
                w9DateSentCliente: data.terceraLlamadaPmpActualizarCliente.w9DateSentCliente,
                w9DateFiledCliente: data.terceraLlamadaPmpActualizarCliente.w9DateFiledCliente,
                coMakerNameCliente: data.terceraLlamadaPmpActualizarCliente.coMakerNameCliente,
                coNameTypeIndCliente: data.terceraLlamadaPmpActualizarCliente.coNameTypeIndCliente,
                coMakerEmployerCliente: HtmlEncode(data.terceraLlamadaPmpActualizarCliente.coMakerEmployerCliente),
                coMakerPhoneFlagCliente: data.terceraLlamadaPmpActualizarCliente.coMakerPhoneFlagCliente,
                coMakerEmpPhoneCliente: data.terceraLlamadaPmpActualizarCliente.coMakerEmpPhoneCliente && data.terceraLlamadaPmpActualizarCliente.coMakerEmpPhoneCliente.substring(0, 11),
                coMakerPositionCliente: data.terceraLlamadaPmpActualizarCliente.coMakerPositionCliente,
                coMakerHomePhoneCliente: data.terceraLlamadaPmpActualizarCliente.coMakerHomePhoneCliente,
                coMakerSsanFlagCliente: data.terceraLlamadaPmpActualizarCliente.coMakerSsanFlagCliente,
                coMakerSsanCliente: data.terceraLlamadaPmpActualizarCliente.coMakerSsanCliente,
                userDemo1Cliente: data.terceraLlamadaPmpActualizarCliente.userDemo1Cliente,
                userDemo2Cliente: data.terceraLlamadaPmpActualizarCliente.userDemo2Cliente,
                memoLine1Cliente: data.terceraLlamadaPmpActualizarCliente.memoLine1Cliente,
                memoLine2Cliente: data.terceraLlamadaPmpActualizarCliente.memoLine2Cliente,
                lastMaintDateCliente: data.terceraLlamadaPmpActualizarCliente.lastMaintDateCliente,
                customerNumberCliente: data.terceraLlamadaPmpActualizarCliente.customerNumberCliente,
                relationshipNumberCliente: data.terceraLlamadaPmpActualizarCliente.relationshipNumberCliente,
                accountNumberCliente: data.terceraLlamadaPmpActualizarCliente.accountNumberCliente,
                cardNumberCliente: data.terceraLlamadaPmpActualizarCliente.cardNumberCliente,
                cardSeqCliente: data.terceraLlamadaPmpActualizarCliente.cardSeqCliente,
                celNumberCliente: data.terceraLlamadaPmpActualizarCliente.celNumberCliente,
                trackTypeCliente: data.terceraLlamadaPmpActualizarCliente.trackTypeCliente,
                trackNameCliente: data.terceraLlamadaPmpActualizarCliente.trackNameCliente && data.terceraLlamadaPmpActualizarCliente.trackNameCliente.substring(0, 30),
                numberCliente: data.terceraLlamadaPmpActualizarCliente.numberCliente && data.terceraLlamadaPmpActualizarCliente.numberCliente.substring(0, 6),
                departmentNumberCliente: data.terceraLlamadaPmpActualizarCliente.departmentNumberCliente && data.terceraLlamadaPmpActualizarCliente.departmentNumberCliente.substring(0, 6),
                officeCliente: data.terceraLlamadaPmpActualizarCliente.officeCliente && data.terceraLlamadaPmpActualizarCliente.officeCliente.substring(0, 3),
                floorCliente: data.terceraLlamadaPmpActualizarCliente.floorCliente && data.terceraLlamadaPmpActualizarCliente.floorCliente.substring(0, 3),
                blockCliente: data.terceraLlamadaPmpActualizarCliente.blockCliente && data.terceraLlamadaPmpActualizarCliente.blockCliente.substring(0, 3),
                lotCliente: data.terceraLlamadaPmpActualizarCliente.lotCliente && data.terceraLlamadaPmpActualizarCliente.lotCliente.substring(0, 3),
                interiorCliente: data.terceraLlamadaPmpActualizarCliente.interiorCliente && data.terceraLlamadaPmpActualizarCliente.interiorCliente.substring(0, 3),
                sectorCliente: data.terceraLlamadaPmpActualizarCliente.sectorCliente && data.terceraLlamadaPmpActualizarCliente.sectorCliente.substring(0, 3),
                kilometerCliente: data.terceraLlamadaPmpActualizarCliente.kilometerCliente && data.terceraLlamadaPmpActualizarCliente.kilometerCliente.substring(0, 3),
                zoneTypeCliente: data.terceraLlamadaPmpActualizarCliente.zoneTypeCliente && data.terceraLlamadaPmpActualizarCliente.zoneTypeCliente.substring(0, 30),
                zoneNameCliente: data.terceraLlamadaPmpActualizarCliente.zoneNameCliente,
                referenceCliente: data.terceraLlamadaPmpActualizarCliente.referenceCliente,
                geoLocationCliente: data.terceraLlamadaPmpActualizarCliente.geoLocationCliente,
                workTypeCliente: data.terceraLlamadaPmpActualizarCliente.workTypeCliente,
                workTrackNameCliente: data.terceraLlamadaPmpActualizarCliente.workTrackNameCliente && data.terceraLlamadaPmpActualizarCliente.workTrackNameCliente.substring(0, 30),
                workNumberCliente: data.terceraLlamadaPmpActualizarCliente.workNumberCliente && data.terceraLlamadaPmpActualizarCliente.workNumberCliente.substring(0, 6),
                workDepartmentNumberCliente: data.terceraLlamadaPmpActualizarCliente.workDepartmentNumberCliente && data.terceraLlamadaPmpActualizarCliente.workDepartmentNumberCliente.substring(0, 6),
                workOfficeCliente: data.terceraLlamadaPmpActualizarCliente.workOfficeCliente && data.terceraLlamadaPmpActualizarCliente.workOfficeCliente.substring(0, 3),
                workFloorCliente: data.terceraLlamadaPmpActualizarCliente.workFloorCliente && data.terceraLlamadaPmpActualizarCliente.workFloorCliente.substring(0, 3),
                workBlockCliente: data.terceraLlamadaPmpActualizarCliente.workBlockCliente && data.terceraLlamadaPmpActualizarCliente.workBlockCliente.substring(0, 3),
                workLotCliente: data.terceraLlamadaPmpActualizarCliente.workLotCliente && data.terceraLlamadaPmpActualizarCliente.workLotCliente.substring(0, 3),
                workInteriorCliente: data.terceraLlamadaPmpActualizarCliente.workInteriorCliente && data.terceraLlamadaPmpActualizarCliente.workInteriorCliente.substring(0, 3),
                workSectorCliente: data.terceraLlamadaPmpActualizarCliente.workSectorCliente && data.terceraLlamadaPmpActualizarCliente.workSectorCliente.substring(0, 3),
                workKilometerCliente: data.terceraLlamadaPmpActualizarCliente.workKilometerCliente && data.terceraLlamadaPmpActualizarCliente.workKilometerCliente.substring(0, 3),
                workZoneTypeCliente: data.terceraLlamadaPmpActualizarCliente.workZoneTypeCliente,
                workZoneNameCliente: data.terceraLlamadaPmpActualizarCliente.workZoneNameCliente && data.terceraLlamadaPmpActualizarCliente.workZoneNameCliente.substring(0, 30),
                workReferenceCliente: data.terceraLlamadaPmpActualizarCliente.workReferenceCliente,
                workGeoLocationCliente: data.terceraLlamadaPmpActualizarCliente.workGeoLocationCliente,
                dualCoinFlagCliente: data.terceraLlamadaPmpActualizarCliente.dualCoinFlagCliente,
                homeAddressModificationDateCliente: data.terceraLlamadaPmpActualizarCliente.homeAddressModificationDateCliente,
                workAddressModificationDateCliente: data.terceraLlamadaPmpActualizarCliente.workAddressModificationDateCliente
            }
        });
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Pmp/PmpActualizarCliente`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    // Launch Dispatch 
                    if(response.data.success){
                        dispatch({ type: PMP_UPDATE_CLIENT_SUCCESS, data:response.data, extra:extra }); 
                    }
                    else{
                        dispatch({ 
                            type: PMP_UPDATE_CLIENT_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: PMP_UPDATE_CLIENT_ERROR, error:newError, response: response});
                return error.response? error.response: null;
            });
    }
}
export function createRelationship(data, extra=null){
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        let sendData = JSON.stringify({
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: device.ip,
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_solicitud: data.cod_solicitud? data.cod_solicitud: 0,
            cod_solicitud_completa: data.cod_solicitud_completa? data.cod_solicitud_completa: "",
            cod_cliente: data.cod_cliente? data.cod_cliente: 0,
            cod_cliente_titular: data.cod_cliente_titular? data.cod_cliente_titular: 0,
            terceraLlamadaPmpCrearRelacion: {
                tipo_doc:data.terceraLlamadaPmpCrearRelacion.tipo_doc,
                nro_doc:data.terceraLlamadaPmpCrearRelacion.nro_doc,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg:decryptData.username,
                des_ter_reg: device.ip,
                organization:data.terceraLlamadaPmpCrearRelacion.organization,
                identityDocumentType:data.terceraLlamadaPmpCrearRelacion.identityDocumentType,
                identityDocumentNumber:data.terceraLlamadaPmpCrearRelacion.identityDocumentNumber,
                billingCycleRelacion:data.terceraLlamadaPmpCrearRelacion.billingCycleRelacion, // Fecha Facturacion
                relationshipCustomerNameRelacion:data.terceraLlamadaPmpCrearRelacion.relationshipCustomerNameRelacion.substring(0, 19),
                primaryAccountRelacion:data.terceraLlamadaPmpCrearRelacion.primaryAccountRelacion,
                numberOfSubAccountsRelacion:data.terceraLlamadaPmpCrearRelacion.numberOfSubAccountsRelacion,
                statusRelacionRelacion:data.terceraLlamadaPmpCrearRelacion.statusRelacionRelacion,
                relationshipCreditLimitRelacion:data.terceraLlamadaPmpCrearRelacion.relationshipCreditLimitRelacion, // Linea Diponible
                relationshipAvailableCreditRelacion:data.terceraLlamadaPmpCrearRelacion.relationshipAvailableCreditRelacion, 
                blockRelacion:data.terceraLlamadaPmpCrearRelacion.blockRelacion,
                mailRelacion:data.terceraLlamadaPmpCrearRelacion.mailRelacion,
                subAccountsCreditLimitRelacion:data.terceraLlamadaPmpCrearRelacion.subAccountsCreditLimitRelacion,
                subAccountsModificationRelacion:data.terceraLlamadaPmpCrearRelacion.subAccountsModificationRelacion,
                billingLvlModificationRelacion:data.terceraLlamadaPmpCrearRelacion.billingLvlModificationRelacion,
                wvMembLvRelacion:data.terceraLlamadaPmpCrearRelacion.wvMembLvRelacion,
                wvMembMdfyRelacion:data.terceraLlamadaPmpCrearRelacion.wvMembMdfyRelacion,
                dualCurrencyFlagRelacion:data.terceraLlamadaPmpCrearRelacion.dualCurrencyFlagRelacion,
                customerNumberRelacion:data.terceraLlamadaPmpCrearRelacion.customerNumberRelacion,
                relationshipNumberRelacion:data.terceraLlamadaPmpCrearRelacion.relationshipNumberRelacion,
                accountNumberRelacion:data.terceraLlamadaPmpCrearRelacion.accountNumberRelacion,
                cardNumberRelacion:data.terceraLlamadaPmpCrearRelacion.cardNumberRelacion,
                cardSequenceRelacion:data.terceraLlamadaPmpCrearRelacion.cardSequenceRelacion
            },
        });
        // Begin Request
        dispatch({type: PMP_CREATE_RELATIONSHIP_REQUEST});
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Pmp/PmpCrearRelacion`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    // Launch Dispatch 
                    if(response.data.success){
                        dispatch({ type: PMP_CREATE_RELATIONSHIP_SUCCESS, data: response.data, extra: extra }); 
                    }
                    else{
                        dispatch({ 
                            type: PMP_CREATE_RELATIONSHIP_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: PMP_CREATE_RELATIONSHIP_ERROR, error:newError, response: response});
                return error;
            });
    }
}
export function createAccount(data, extra=null){  
    //let data = response;
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        let sendData = JSON.stringify({
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: device.ip,
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_solicitud: data.cod_solicitud? data.cod_solicitud: 0,
            cod_solicitud_completa: data.cod_solicitud_completa? data.cod_solicitud_completa: "",
            cod_cliente: data.cod_cliente? data.cod_cliente: 0,
            cod_cliente_titular: data.cod_cliente_titular? data.cod_cliente_titular: 0,
            terceraLlamadaPmpCrearCuenta:{
                tipo_doc:data.terceraLlamadaPmpCrearCuenta.tipo_doc,
                nro_doc:data.terceraLlamadaPmpCrearCuenta.nro_doc,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg:decryptData.username,
                des_ter_reg: device.ip,
                organization:data.terceraLlamadaPmpCrearCuenta.organization,
                identityDocumentType:data.terceraLlamadaPmpCrearCuenta.identityDocumentType, //DU,
                identityDocumentNumber:data.terceraLlamadaPmpCrearCuenta.identityDocumentNumber,
                isoMessageTypeCuenta:data.terceraLlamadaPmpCrearCuenta.isoMessageTypeCuenta, // No Define
                transactionNumberCuenta:data.terceraLlamadaPmpCrearCuenta.transactionNumberCuenta, // No Define
                logCuenta:data.terceraLlamadaPmpCrearCuenta.logCuenta, // MASTER CARD - VISA
                accountNumberAutoCuenta:data.terceraLlamadaPmpCrearCuenta.accountNumberAutoCuenta,
                relationNumberCuenta:data.terceraLlamadaPmpCrearCuenta.relationNumberCuenta, // Code Relationship
                shortNameCuenta:data.terceraLlamadaPmpCrearCuenta.shortNameCuenta.substring(0, 15), // Shor Name
                creditLimitCuenta:data.terceraLlamadaPmpCrearCuenta.creditLimitCuenta, // Line Available
                relPrimAcctFlagCuenta:data.terceraLlamadaPmpCrearCuenta.relPrimAcctFlagCuenta, // Constant
                pmtAchFlagCuenta:data.terceraLlamadaPmpCrearCuenta.pmtAchFlagCuenta, // Null
                pmtAchDbTypeCuenta:data.terceraLlamadaPmpCrearCuenta.pmtAchDbTypeCuenta, // Soles or Dolar
                pmtAchDbNbrCuenta:data.terceraLlamadaPmpCrearCuenta.pmtAchDbNbrCuenta, // Charge Account
                intStatusCuenta:data.terceraLlamadaPmpCrearCuenta.intStatusCuenta, // Constant
                altCustNbrCuenta:data.terceraLlamadaPmpCrearCuenta.altCustNbrCuenta, // Code Customer
                colatCodeCuenta:data.terceraLlamadaPmpCrearCuenta.colatCodeCuenta, // Null
                flexBillFlagCuenta:data.terceraLlamadaPmpCrearCuenta.flexBillFlagCuenta, // Null
                dateAppliCuenta:data.terceraLlamadaPmpCrearCuenta.dateAppliCuenta, // Zero
                dateOpenedCuenta:data.terceraLlamadaPmpCrearCuenta.dateOpenedCuenta, // Zero
                relCardSchemeCuenta:data.terceraLlamadaPmpCrearCuenta.relCardSchemeCuenta, // Constant
                grtstCardExprDateCuenta:data.terceraLlamadaPmpCrearCuenta.grtstCardExprDateCuenta, // Zero
                billingCycleCuenta:data.terceraLlamadaPmpCrearCuenta.billingCycleCuenta, // Billing Cicle
                blockCode1Cuenta:data.terceraLlamadaPmpCrearCuenta.blockCode1Cuenta, // Block Account
                blockCode2Cuenta:data.terceraLlamadaPmpCrearCuenta.blockCode2Cuenta, // Block Account
                blockDate1Cuenta:data.terceraLlamadaPmpCrearCuenta.blockDate1Cuenta, // Date Block
                blockDate2Cuenta:data.terceraLlamadaPmpCrearCuenta.blockDate2Cuenta, // Date Block
                ownerFlagCuenta:data.terceraLlamadaPmpCrearCuenta.ownerFlagCuenta, // Null
                acctRestructureCuenta:data.terceraLlamadaPmpCrearCuenta.acctRestructureCuenta, // Null
                cardExpDateCuenta:data.terceraLlamadaPmpCrearCuenta.cardExpDateCuenta, // Date Exp.
                sourceCodeCuenta:data.terceraLlamadaPmpCrearCuenta.sourceCodeCuenta, // Code Promotion
                collCardCuenta:data.terceraLlamadaPmpCrearCuenta.collCardCuenta, //Constant
                acctDisplayCuenta:data.terceraLlamadaPmpCrearCuenta.acctDisplayCuenta, //Contanst 
                stateOfResidCuenta:data.terceraLlamadaPmpCrearCuenta.stateOfResidCuenta, // Code PCT OK 
                stateOfIssueCuenta:data.terceraLlamadaPmpCrearCuenta.stateOfIssueCuenta, // Code PCT OK
                pctCtlIdCuenta:data.terceraLlamadaPmpCrearCuenta.pctCtlIdCuenta, // Campaign PCT
                pctStartDateCuenta:data.terceraLlamadaPmpCrearCuenta.pctStartDateCuenta, // Date Start PCT
                pctExpirDateCuenta:data.terceraLlamadaPmpCrearCuenta.pctExpirDateCuenta, // Date End PCT
                pctLevelCuenta:data.terceraLlamadaPmpCrearCuenta.pctLevelCuenta, // Level PCT
                pctLevelStartDateCuenta:data.terceraLlamadaPmpCrearCuenta.pctLevelStartDateCuenta, // Date start Level PCT
                pctLevelExpirDateCuenta:data.terceraLlamadaPmpCrearCuenta.pctLevelExpirDateCuenta, // Date end Level PCT
                officerCodeCuenta:data.terceraLlamadaPmpCrearCuenta.officerCodeCuenta, //Officer 
                pmtSkipFlagCuenta:data.terceraLlamadaPmpCrearCuenta.pmtSkipFlagCuenta, //Constant
                fsFlagCuenta:data.terceraLlamadaPmpCrearCuenta.fsFlagCuenta, // Constant
                fraudReportingFlagCuenta:data.terceraLlamadaPmpCrearCuenta.fraudReportingFlagCuenta,//Constant
                waiveCashAvailCrCuenta:data.terceraLlamadaPmpCrearCuenta.waiveCashAvailCrCuenta, // Waive Cash Y or N
                makersDobCuenta:data.terceraLlamadaPmpCrearCuenta.makersDobCuenta, //Birthday Maker
                comakersDobCuenta:data.terceraLlamadaPmpCrearCuenta.comakersDobCuenta, // Birthday Co-Maker
                cashPlanNbrCuenta:data.terceraLlamadaPmpCrearCuenta.cashPlanNbrCuenta, //Plan Cash
                retailPlanNbrCuenta:data.terceraLlamadaPmpCrearCuenta.retailPlanNbrCuenta, // Plan Rail
                userDate1Cuenta:data.terceraLlamadaPmpCrearCuenta.userDate1Cuenta,
                userDate2Cuenta:data.terceraLlamadaPmpCrearCuenta.userDate2Cuenta,
                userDate3Cuenta:data.terceraLlamadaPmpCrearCuenta.userDate3Cuenta,
                userDate4Cuenta:data.terceraLlamadaPmpCrearCuenta.userDate4Cuenta,
                userDate5Cuenta:data.terceraLlamadaPmpCrearCuenta.userDate5Cuenta,
                userDate6Cuenta:data.terceraLlamadaPmpCrearCuenta.userDate6Cuenta,
                userAmt1Cuenta:data.terceraLlamadaPmpCrearCuenta.userAmt1Cuenta,
                userAmt2Cuenta:data.terceraLlamadaPmpCrearCuenta.userAmt2Cuenta,
                userAmt3Cuenta:data.terceraLlamadaPmpCrearCuenta.userAmt3Cuenta,
                userAmt4Cuenta:data.terceraLlamadaPmpCrearCuenta.userAmt4Cuenta,
                userAmt5Cuenta:data.terceraLlamadaPmpCrearCuenta.userAmt5Cuenta,
                userAmt6Cuenta:data.terceraLlamadaPmpCrearCuenta.userAmt6Cuenta,
                userMsg1IndCuenta:data.terceraLlamadaPmpCrearCuenta.userMsg1IndCuenta,
                userMsg2IndCuenta:data.terceraLlamadaPmpCrearCuenta.userMsg2IndCuenta,
                userMsg3IndCuenta:data.terceraLlamadaPmpCrearCuenta.userMsg3IndCuenta,
                origBankNbrCuenta:data.terceraLlamadaPmpCrearCuenta.origBankNbrCuenta,
                origBranchNbrCuenta:data.terceraLlamadaPmpCrearCuenta.origBranchNbrCuenta,
                origStoreNbrCuenta:data.terceraLlamadaPmpCrearCuenta.origStoreNbrCuenta,
                owingBankNbrCuenta:data.terceraLlamadaPmpCrearCuenta.owingBankNbrCuenta,
                owingBranchNbrCuenta:data.terceraLlamadaPmpCrearCuenta.owingBranchNbrCuenta,
                owingStoreNbrCuenta:data.terceraLlamadaPmpCrearCuenta.owingStoreNbrCuenta,
                pendBankNbrCuenta:data.terceraLlamadaPmpCrearCuenta.pendBankNbrCuenta,
                pendBranchNbrCuenta:data.terceraLlamadaPmpCrearCuenta.pendBranchNbrCuenta,
                pendStoreNbrCuenta:data.terceraLlamadaPmpCrearCuenta.pendStoreNbrCuenta,
                incomeCuenta:data.terceraLlamadaPmpCrearCuenta.incomeCuenta,
                customerNumberCuenta:data.terceraLlamadaPmpCrearCuenta.customerNumberCuenta, // Code Customer
                relationshipNumberCuenta:data.terceraLlamadaPmpCrearCuenta.relationshipNumberCuenta, // Code Relationship
                accountNumberCuenta:data.terceraLlamadaPmpCrearCuenta.accountNumberCuenta,
                cardNumberCuenta:data.terceraLlamadaPmpCrearCuenta.cardNumberCuenta,
                cardSeqCuenta:data.terceraLlamadaPmpCrearCuenta.cardSeqCuenta,
                ambsPlanPmtOvrdFlagCuenta:data.terceraLlamadaPmpCrearCuenta.ambsPlanPmtOvrdFlagCuenta, // Flag Fixed Payment
                ambsFixedPmtAmtPctCuenta:data.terceraLlamadaPmpCrearCuenta.ambsFixedPmtAmtPctCuenta // Fixed Payment Amount
            }
        
        });
        // Begin Request
        dispatch({type: PMP_CREATE_ACCOUNT_REQUEST});
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Pmp/PmpCrearCuenta`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    // Launch Dispatch 
                    if(response.data.success){
                        dispatch({ type: PMP_CREATE_ACCOUNT_SUCCESS, data: response.data, extra: extra }); 
                    }
                    else{
                        dispatch({ 
                            type: PMP_CREATE_ACCOUNT_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true });
                    }
                }
                return response;
            })
            .catch(error => {

                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: PMP_CREATE_ACCOUNT_ERROR, error:newError, response: response});
                return error;
            });
    }
}

export function clienteValidaCuentaTC(data){
    return (dispatch) => {
        dispatch({type: CLIENTE_VALIDA_CUENTA_REQUEST});
        return axios
            .post(`${URL_ODC_REGULAR}/Cliente/ValidacionCuentaTC`, data, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })
            .then(response => {
                if(response.data){
                    // Launch Dispatch 
                    if(response.data.success){
                        dispatch({ type: CLIENTE_VALIDA_CUENTA_SUCCESS, data: response.data, extra: null }); 
                    }
                    else{
                        dispatch({ 
                            type: CLIENTE_VALIDA_CUENTA_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: CLIENTE_VALIDA_CUENTA_ERROR, error:newError, response: response});
                return error;
            });
    }
}

export function createCreditCard(data, extra=null){
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        let sendData = JSON.stringify({
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: device.ip,
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_solicitud: data.cod_solicitud? data.cod_solicitud: 0,
            cod_solicitud_completa: data.cod_solicitud_completa? data.cod_solicitud_completa: "",
            cod_cliente: data.cod_cliente? data.cod_cliente: 0,
            cod_cliente_titular: data.cod_cliente_titular? data.cod_cliente_titular: 0,
            terceraLlamadaPmpCrearTarjeta: {
                tipo_doc:data.terceraLlamadaPmpCrearTarjeta.tipo_doc,
                nro_doc:data.terceraLlamadaPmpCrearTarjeta.nro_doc,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg: decryptData.username,
                des_ter_reg: device.ip,
                organization:data.terceraLlamadaPmpCrearTarjeta.organization,
                identityDocumentType:data.terceraLlamadaPmpCrearTarjeta.identityDocumentType,
                identityDocumentNumber:data.terceraLlamadaPmpCrearTarjeta.identityDocumentNumber,
                isoMessageTypeTarjeta:data.terceraLlamadaPmpCrearTarjeta.isoMessageTypeTarjeta,
                transactionNumberTarjeta: data.terceraLlamadaPmpCrearTarjeta.transactionNumberTarjeta,
                logTarjeta:data.terceraLlamadaPmpCrearTarjeta.logTarjeta,
                accountNumberTarjeta:data.terceraLlamadaPmpCrearTarjeta.accountNumberTarjeta,
                accountCardNumberTarjeta: data.terceraLlamadaPmpCrearTarjeta.accountCardNumberTarjeta,
                cardTypeTarjeta: data.terceraLlamadaPmpCrearTarjeta.cardTypeTarjeta,
                cardSequenceTarjeta: data.terceraLlamadaPmpCrearTarjeta.cardSequenceTarjeta,
                statusTarjeta: data.terceraLlamadaPmpCrearTarjeta.statusTarjeta,
                nmbrCardsOutstTarjeta: data.terceraLlamadaPmpCrearTarjeta.nmbrCardsOutstTarjeta,
                nmbrCardsRtnTarjeta: data.terceraLlamadaPmpCrearTarjeta.nmbrCardsRtnTarjeta,
                cardActionTarjeta: data.terceraLlamadaPmpCrearTarjeta.cardActionTarjeta,
                nbrRqtdTarjeta: data.terceraLlamadaPmpCrearTarjeta.nbrRqtdTarjeta,
                typeOfCardTarjeta: data.terceraLlamadaPmpCrearTarjeta.typeOfCardTarjeta,
                rqtdCardTypeTarjeta: data.terceraLlamadaPmpCrearTarjeta.rqtdCardTypeTarjeta,
                expireDateTarjeta: data.terceraLlamadaPmpCrearTarjeta.expireDateTarjeta,
                embrName1Tarjeta:data.terceraLlamadaPmpCrearTarjeta.embrName1Tarjeta && data.terceraLlamadaPmpCrearTarjeta.embrName1Tarjeta.substring(0, 26),
                embrName2Tarjeta:data.terceraLlamadaPmpCrearTarjeta.embrName2Tarjeta && data.terceraLlamadaPmpCrearTarjeta.embrName2Tarjeta.substring(0, 26),
                pinOffsetTarjeta:data.terceraLlamadaPmpCrearTarjeta.pinOffsetTarjeta,
                posServiceCodeTarjeta:data.terceraLlamadaPmpCrearTarjeta.posServiceCodeTarjeta,
                dteWarningBulletinTarjeta:data.terceraLlamadaPmpCrearTarjeta.dteWarningBulletinTarjeta,
                addrLine1Tarjeta:data.terceraLlamadaPmpCrearTarjeta.addrLine1Tarjeta && data.terceraLlamadaPmpCrearTarjeta.addrLine1Tarjeta.substring(0, 30),
                addrLine2Tarjeta:data.terceraLlamadaPmpCrearTarjeta.addrLine2Tarjeta,
                cityTarjeta:data.terceraLlamadaPmpCrearTarjeta.cityTarjeta && data.terceraLlamadaPmpCrearTarjeta.cityTarjeta.substring(0,2),
                stateTarjeta:data.terceraLlamadaPmpCrearTarjeta.stateTarjeta && data.terceraLlamadaPmpCrearTarjeta.stateTarjeta.substring(0,3),
                zipTarjeta:data.terceraLlamadaPmpCrearTarjeta.zipTarjeta && data.terceraLlamadaPmpCrearTarjeta.zipTarjeta.toString(),
                languageCodeTarjeta:data.terceraLlamadaPmpCrearTarjeta.languageCodeTarjeta,
                requestedCardTypeTarjeta:data.terceraLlamadaPmpCrearTarjeta.requestedCardTypeTarjeta,
                warningBulletinTarjeta:data.terceraLlamadaPmpCrearTarjeta.warningBulletinTarjeta,
                posServiceCode2Tarjeta:data.terceraLlamadaPmpCrearTarjeta.posServiceCode2Tarjeta,
                blockCodeTarjeta:data.terceraLlamadaPmpCrearTarjeta.blockCodeTarjeta,
                feeTarjeta:data.terceraLlamadaPmpCrearTarjeta.feeTarjeta,
                user1Tarjeta:data.terceraLlamadaPmpCrearTarjeta.user1Tarjeta,
                user2Tarjeta:data.terceraLlamadaPmpCrearTarjeta.user2Tarjeta,
                currFirstUsageFlagTarjeta:data.terceraLlamadaPmpCrearTarjeta.currFirstUsageFlagTarjeta,
                priorFirstUsageFlagTarjeta:data.terceraLlamadaPmpCrearTarjeta.priorFirstUsageFlagTarjeta,
                dateOpenedTarjeta:data.terceraLlamadaPmpCrearTarjeta.dateOpenedTarjeta,
                dateNextExprTarjeta:data.terceraLlamadaPmpCrearTarjeta.dateNextExprTarjeta,
                dateLstPlasticIssueTarjeta:data.terceraLlamadaPmpCrearTarjeta.dateLstPlasticIssueTarjeta,
                dateLastExprTarjeta:data.terceraLlamadaPmpCrearTarjeta.dateLastExprTarjeta,
                dateFirstCardVerifyTarjeta:data.terceraLlamadaPmpCrearTarjeta.dateFirstCardVerifyTarjeta,
                lastCardActionTarjeta:data.terceraLlamadaPmpCrearTarjeta.lastCardActionTarjeta,
                dateExceptionPurgeTarjeta:data.terceraLlamadaPmpCrearTarjeta.dateExceptionPurgeTarjeta,
                excptnRespCodeTarjeta:data.terceraLlamadaPmpCrearTarjeta.excptnRespCodeTarjeta,
                regionCodeTarjeta:data.terceraLlamadaPmpCrearTarjeta.regionCodeTarjeta,
                currentCVC2Tarjeta:data.terceraLlamadaPmpCrearTarjeta.currentCVC2Tarjeta,
                customerNumberTarjeta:data.terceraLlamadaPmpCrearTarjeta.customerNumberTarjeta,
                relationshipNumberTarjeta:data.terceraLlamadaPmpCrearTarjeta.relationshipNumberTarjeta,
                cardAccountNumberTarjeta:data.terceraLlamadaPmpCrearTarjeta.cardAccountNumberTarjeta,
                cardNumberTarjeta:data.terceraLlamadaPmpCrearTarjeta.cardNumberTarjeta,
                cardSeqTarjeta:data.terceraLlamadaPmpCrearTarjeta.cardSeqTarjeta,
                trackTypeTarjeta:data.terceraLlamadaPmpCrearTarjeta.trackTypeTarjeta,
                trackNameTarjeta:data.terceraLlamadaPmpCrearTarjeta.trackNameTarjeta && data.terceraLlamadaPmpCrearTarjeta.trackNameTarjeta.substring(0, 30),
                numberTarjeta:data.terceraLlamadaPmpCrearTarjeta.numberTarjeta && data.terceraLlamadaPmpCrearTarjeta.numberTarjeta.substring(0, 6),
                departmentNumberTarjeta:data.terceraLlamadaPmpCrearTarjeta.departmentNumberTarjeta && data.terceraLlamadaPmpCrearTarjeta.departmentNumberTarjeta.substring(0, 6),
                officeTarjeta:data.terceraLlamadaPmpCrearTarjeta.officeTarjeta && data.terceraLlamadaPmpCrearTarjeta.officeTarjeta.substring(0, 3),
                floorTarjeta:data.terceraLlamadaPmpCrearTarjeta.floorTarjeta && data.terceraLlamadaPmpCrearTarjeta.floorTarjeta.substring(0, 3),
                blockTarjeta:data.terceraLlamadaPmpCrearTarjeta.blockTarjeta && data.terceraLlamadaPmpCrearTarjeta.blockTarjeta.substring(0, 3),
                lotTarjeta:data.terceraLlamadaPmpCrearTarjeta.lotTarjeta && data.terceraLlamadaPmpCrearTarjeta.lotTarjeta.substring(0, 3),
                insideTarjeta:data.terceraLlamadaPmpCrearTarjeta.insideTarjeta && data.terceraLlamadaPmpCrearTarjeta.insideTarjeta.substring(0, 3),
                sectorTarjeta:data.terceraLlamadaPmpCrearTarjeta.sectorTarjeta && data.terceraLlamadaPmpCrearTarjeta.sectorTarjeta.substring(0, 3),
                kilometerTarjeta:data.terceraLlamadaPmpCrearTarjeta.kilometerTarjeta && data.terceraLlamadaPmpCrearTarjeta.kilometerTarjeta.substring(0, 3),
                zoneTypeTarjeta:data.terceraLlamadaPmpCrearTarjeta.zoneTypeTarjeta && data.terceraLlamadaPmpCrearTarjeta.zoneTypeTarjeta.substring(0, 30),
                zoneNameTarjeta:data.terceraLlamadaPmpCrearTarjeta.zoneNameTarjeta && data.terceraLlamadaPmpCrearTarjeta.zoneNameTarjeta.substring(0, 30),
                referenceTarjeta:data.terceraLlamadaPmpCrearTarjeta.referenceTarjeta,
                addressFlagTypeTarjeta:data.terceraLlamadaPmpCrearTarjeta.addressFlagTypeTarjeta,
                originalBranchTarjeta:data.terceraLlamadaPmpCrearTarjeta.originalBranchTarjeta,
                mandatory1Tarjeta:data.terceraLlamadaPmpCrearTarjeta.mandatory1Tarjeta,
                mandatory2Tarjeta:data.terceraLlamadaPmpCrearTarjeta.mandatory2Tarjeta,
                actualBranchTarjeta:data.terceraLlamadaPmpCrearTarjeta.actualBranchTarjeta,
                promotionCodeTarjeta:data.terceraLlamadaPmpCrearTarjeta.promotionCodeTarjeta,
                geoLocationTarjeta:data.terceraLlamadaPmpCrearTarjeta.geoLocationTarjeta,
                changeReasonTarjeta:data.terceraLlamadaPmpCrearTarjeta.changeReasonTarjeta,
                indentityDocType1Tarjeta:data.terceraLlamadaPmpCrearTarjeta.indentityDocType1Tarjeta,
                documentNmbr1Tarjeta:data.terceraLlamadaPmpCrearTarjeta.documentNmbr1Tarjeta,
                relationship1Tarjeta:data.terceraLlamadaPmpCrearTarjeta.relationship1Tarjeta,
                indentityDocType2Tarjeta:data.terceraLlamadaPmpCrearTarjeta.indentityDocType2Tarjeta,
                documentNmbr2Tarjeta:data.terceraLlamadaPmpCrearTarjeta.documentNmbr2Tarjeta,
                relationship2Tarjeta:data.terceraLlamadaPmpCrearTarjeta.relationship2Tarjeta,
                emissionTypeTarjeta:data.terceraLlamadaPmpCrearTarjeta.emissionTypeTarjeta,
                dualCoinFlagTarjeta:data.terceraLlamadaPmpCrearTarjeta.dualCoinFlagTarjeta,
                cardLastModificationDateTarjeta: data.terceraLlamadaPmpCrearTarjeta.cardLastModificationDateTarjeta,
                broadcastTypeTarjeta: data.terceraLlamadaPmpCrearTarjeta.broadcastTypeTarjeta,
                adiNameTarjeta: data.terceraLlamadaPmpCrearTarjeta.adiNameTarjeta,
                adiLastNamesTarjeta: data.terceraLlamadaPmpCrearTarjeta.adiLastNamesTarjeta,
                adiDocTypeTarjeta: data.terceraLlamadaPmpCrearTarjeta.adiDocTypeTarjeta,
                adiDocNmbrTarjeta: data.terceraLlamadaPmpCrearTarjeta.adiDocNmbrTarjeta,
                adiNitTarjeta: data.terceraLlamadaPmpCrearTarjeta.adiNitTarjeta
            }
        });
        // Begin Request
        dispatch({type: PMP_CREATE_CREDIT_CARD_REQUEST});
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Pmp/PmpCrearTarjeta`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    // Launch Dispatch 
                    if(response.data.success){
                        dispatch({ type: PMP_CREATE_CREDIT_CARD_SUCCESS, data: response.data, extra: extra }); 
                    }
                    else{
                        dispatch({ 
                            type: PMP_CREATE_CREDIT_CARD_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: PMP_CREATE_CREDIT_CARD_ERROR, error:newError, response: response});
                return error;
            });
    }
}
export function blockTypeCreditCard(data, extra=null){
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        let sendData = JSON.stringify({
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: device.ip,
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_solicitud: data.cod_solicitud? data.cod_solicitud: 0,
            cod_solicitud_completa: data.cod_solicitud_completa? data.cod_solicitud_completa: "",
            cod_solicitud_antigua: data.cod_solicitud_antigua || 0,
            cod_solicitud_completa_antigua: data.cod_solicitud_completa_antigua || '',
            cod_cliente: data.cod_cliente? data.cod_cliente: 0,
            cod_cliente_titular: data.cod_cliente_titular? data.cod_cliente_titular: 0,
            terceraLlamadaPmpTipoBloqueTarjeta: {
                tipo_doc: data.terceraLlamadaPmpTipoBloqueTarjeta.tipo_doc,
                nro_doc: data.terceraLlamadaPmpTipoBloqueTarjeta.nro_doc,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg:decryptData.username,
                des_ter_reg: device.ip,
                organization:data.terceraLlamadaPmpTipoBloqueTarjeta.organization,
                identityDocumentType:data.terceraLlamadaPmpTipoBloqueTarjeta.identityDocumentType,
                identityDocumentNumber:data.terceraLlamadaPmpTipoBloqueTarjeta.identityDocumentNumber,
                isoMessageTypeActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.isoMessageTypeActivar,
                transactionNumberActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.transactionNumberActivar,
                cardNumberActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.cardNumberActivar,
                cardSequenceActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.cardSequenceActivar,
                amedBlockCodeActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.amedBlockCodeActivar,//"K",
                amedCardActionActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.amedCardActionActivar,
                amedRqtdCardTypeActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.amedRqtdCardTypeActivar,
                amedCurrFirstUsageFlagActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.amedCurrFirstUsageFlagActivar,
                amedPriorFirstUsageFlagActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.amedPriorFirstUsageFlagActivar,
                amedMotiveBlockadeActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.amedMotiveBlockadeActivar,
                internacionUsaRegionActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.internacionUsaRegionActivar,
                boletineoInterUsaDateActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.boletineoInterUsaDateActivar,
                interCanRegionActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.interCanRegionActivar,
                boletineoInterCanDateActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.boletineoInterCanDateActivar,
                internacionalCAmerRegionActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.internacionalCAmerRegionActivar,
                boletineoInterCAmerRegionActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.boletineoInterCAmerRegionActivar,
                interAsiaRegionActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.interAsiaRegionActivar,
                bolitineoInterAsiaDateActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.bolitineoInterAsiaDateActivar,
                interRegionEuropaActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.interRegionEuropaActivar,
                bolitineoInterDateEuropaActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.bolitineoInterDateEuropaActivar,
                interEuropaRegionActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.interEuropaRegionActivar,
                bolitineoInterEuropaDateActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.bolitineoInterEuropaDateActivar,
                principalAccountActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.principalAccountActivar,
                documentTypeActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.documentTypeActivar,
                documentNumerActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.documentNumerActivar,
                clientNameActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.clientNameActivar,
                clientAddressActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.clientAddressActivar,
                birthDateActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.birthDateActivar,
                telephoneActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.telephoneActivar,
                cardNameActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.cardNameActivar,
                cardAddressActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.cardAddressActivar,
                cardTypeActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.cardTypeActivar,
                applicantNameActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.applicantNameActivar,
                applicantDocumentActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.applicantDocumentActivar,
                applicantTelephoneActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.applicantTelephoneActivar,
                relationshipActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.relationshipActivar,
                blockadeUserActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.blockadeUserActivar,
                blockadeNumberActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.blockadeNumberActivar,
                blockadeDateActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.blockadeDateActivar,
                blockadeTimeActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.blockadeTimeActivar,
                userInitialsActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.userInitialsActivar,
                emisionTypeActivar:data.terceraLlamadaPmpTipoBloqueTarjeta.emisionTypeActivar
            }
        });
        // Begin Request
        dispatch({type: PMP_BLOCK_TYPE_CREDIT_CARD_REQUEST});
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Pmp/PmpTipoBloqueoTarjeta`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    // Launch Dispatch 
                    if(response.data.success){
                        dispatch({ type: PMP_BLOCK_TYPE_CREDIT_CARD_SUCCESS, data: response.data, extra: extra }); 
                    }
                    else{
                        dispatch({ 
                            type: PMP_BLOCK_TYPE_CREDIT_CARD_INVALID, 
                            data: null, 
                            error: response.data.errorMessage,
                            response: true });
                    }
                    return response;
                }
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: PMP_BLOCK_TYPE_CREDIT_CARD_ERROR, error:newError, response: response});
                return error;
            });
    }
}

export function createSAE(data, extra=null){
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        let sendData = JSON.stringify({
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: data.terceraLlamadaPmpCrearSAE.numreferencia? data.terceraLlamadaPmpCrearSAE.numreferencia: device.ip,
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_solicitud: data.cod_solicitud? data.cod_solicitud: 0,
            cod_solicitud_completa: data.cod_solicitud_completa? data.cod_solicitud_completa: "",
            cod_cliente: data.cod_cliente? data.cod_cliente: 0,
            cod_cliente_titular: data.cod_cliente_titular? data.cod_cliente_titular: 0,
            terceraLlamadaPmpCrearSAE: {
                tipo_doc: data.terceraLlamadaPmpCrearSAE.tipo_doc,
                nro_doc: data.terceraLlamadaPmpCrearSAE.nro_doc,
                fec_reg: data.terceraLlamadaPmpCrearSAE.fec_reg? data.terceraLlamadaPmpCrearSAE.fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg: data.terceraLlamadaPmpCrearSAE.des_usu_reg? data.terceraLlamadaPmpCrearSAE.des_usu_reg: decryptData.username,
                des_ter_reg: data.terceraLlamadaPmpCrearSAE.numreferencia? data.terceraLlamadaPmpCrearSAE.numreferencia: device.ip,
                sid: data.terceraLlamadaPmpCrearSAE.sid? data.terceraLlamadaPmpCrearSAE.sid: "", // odc example
                cui: data.terceraLlamadaPmpCrearSAE.cui? data.terceraLlamadaPmpCrearSAE.cui: "0000000000000000",
                cid: data.terceraLlamadaPmpCrearSAE.cid? data.terceraLlamadaPmpCrearSAE.cid: "", // odc example
                codemisor: data.terceraLlamadaPmpCrearSAE.codemisor? data.terceraLlamadaPmpCrearSAE.codemisor: "", // 641 example
                codusuario: data.terceraLlamadaPmpCrearSAE.codusuario? data.terceraLlamadaPmpCrearSAE.codusuario: "",
                numterminal: data.terceraLlamadaPmpCrearSAE.numreferencia? data.terceraLlamadaPmpCrearSAE.numreferencia: device.ip,
                numreferencia: data.terceraLlamadaPmpCrearSAE.numreferencia? data.terceraLlamadaPmpCrearSAE.numreferencia: "",
                fechatxnterminal: data.terceraLlamadaPmpCrearSAE.fechatxnterminal? data.terceraLlamadaPmpCrearSAE.fechatxnterminal: moment().format('YYYY-MM-DD').replace(/-/g, ""), //"20191120",
                horatxnterminal: data.terceraLlamadaPmpCrearSAE.horatxnterminal? data.terceraLlamadaPmpCrearSAE.horatxnterminal: moment().format('HH:mm:ss').replace(/:/g, ""), //"173900",
                numerocuenta: data.terceraLlamadaPmpCrearSAE.numerocuenta? data.terceraLlamadaPmpCrearSAE.numerocuenta: "", //0005292060100010073 example
                codigopct: data.terceraLlamadaPmpCrearSAE.codigopct? data.terceraLlamadaPmpCrearSAE.codigopct: "", //001 example
                lineaasignada: data.terceraLlamadaPmpCrearSAE.lineaasignada? data.terceraLlamadaPmpCrearSAE.lineaasignada: 0, //1000 example
                fechainicio: data.terceraLlamadaPmpCrearSAE.fechainicio? data.terceraLlamadaPmpCrearSAE.fechainicio: "00000000",
                fechafin: data.terceraLlamadaPmpCrearSAE.fechafin? data.terceraLlamadaPmpCrearSAE.fechafin: "00000000"
            }
        });
        // Begin Request
        dispatch({type: PMP_CREATE_CREDIT_CARD_SAE_REQUEST});
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Pmp/PmpCrearSAE`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    // Launch Dispatch 
                    if(response.data.success){
                        dispatch({ type: PMP_CREATE_CREDIT_CARD_SAE_SUCCESS, data: response.data, extra: extra }); 
                    }
                    else{
                        dispatch({ 
                            type: PMP_CREATE_CREDIT_CARD_SAE_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true });
                    }
                    return response;
                }
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: PMP_CREATE_CREDIT_CARD_SAE_ERROR, error:newError, response: response});
                return error;
            });
    }
}

export function updateClientSavePMP(data, extra=null){
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Begin Request
        dispatch({type: PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_REQUEST});
        let sendData = JSON.stringify({
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: device.ip,
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),

            cod_solicitud: data.cod_solicitud? data.cod_solicitud: 0,
            cod_solicitud_completa: data.cod_solicitud_completa? data.cod_solicitud_completa: "",
            cod_cliente: data.cod_cliente? data.cod_cliente: 0,
            cod_cliente_titular: data.cod_cliente_titular? data.cod_cliente_titular: 0,

            terceraLlamadaPmpActualizarClienteSinSolicitud:{
                tipo_doc: data.terceraLlamadaPmpActualizarClienteSinSolicitud.tipo_doc,
                nro_doc: data.terceraLlamadaPmpActualizarClienteSinSolicitud.nro_doc,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg: decryptData.username,
                des_ter_reg: device.ip,  

                organization: data.terceraLlamadaPmpActualizarClienteSinSolicitud.organization,
                identityDocumentType: data.terceraLlamadaPmpActualizarClienteSinSolicitud.identityDocumentType && data.terceraLlamadaPmpActualizarClienteSinSolicitud.identityDocumentType.substring(0, 30),
                identityDocumentNumber: data.terceraLlamadaPmpActualizarClienteSinSolicitud.identityDocumentNumber && data.terceraLlamadaPmpActualizarClienteSinSolicitud.identityDocumentNumber.substring(0, 30),
                nameLine1Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.nameLine1Cliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.nameLine1Cliente.substring(0, 30),
                statusCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.statusCliente,
                nameLine2Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.nameLine2Cliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.nameLine2Cliente.substring(0, 25),
                nameLine3Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.nameLine3Cliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.nameLine3Cliente.substring(0, 25),
                stmtMsgIndicCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.stmtMsgIndicCliente,
                addrLine1Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.addrLine1Cliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.addrLine1Cliente.substring(0, 30),
                mailyngListCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.mailyngListCliente, 
                addrLine2Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.addrLine2Cliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.addrLine2Cliente.substring(0, 30),
                carrierRoute12Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.carrierRoute12Cliente,
                carrierRoute34Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.carrierRoute34Cliente,
                cityCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.cityCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.cityCliente.substring(0, 22),
                stateCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.stateCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.stateCliente.substring(0,3),
                zipCodeCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.zipCodeCliente.toString(),
                nameTypeIndNl1Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.nameTypeIndNl1Cliente,
                nameTypeIndNl2Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.nameTypeIndNl2Cliente,
                nameTypeIndNl3Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.nameTypeIndNl3Cliente,
                ownRentResidFlagCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.ownRentResidFlagCliente,
                genderCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.genderCliente,
                relativeNameCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.relativeNameCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.relativeNameCliente.substring(0, 30),
                numberOfDependentsCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.numberOfDependentsCliente,
                makerEmployerCliente: HtmlEncode(data.terceraLlamadaPmpActualizarClienteSinSolicitud.makerEmployerCliente) && HtmlEncode(data.terceraLlamadaPmpActualizarClienteSinSolicitud.makerEmployerCliente.substring(0, 30)),
                makerPhoneFlagCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.makerPhoneFlagCliente,
                makerEmpPhoneCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.makerEmpPhoneCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.makerEmpPhoneCliente.substring(0, 12),
                makerPositionCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.makerPositionCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.makerPositionCliente.substring(0, 30),
                makerHomePhoneCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.makerHomePhoneCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.makerHomePhoneCliente.substring(0, 12),
                makerSsanFlagCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.makerSsanFlagCliente,
                makerSsanCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.makerSsanCliente,
                dlStateCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.dlStateCliente,
                dlNumberCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.dlNumberCliente,
                languageIndicCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.languageIndicCliente,
                w9IndicCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.w9IndicCliente,
                w9DateSentCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.w9DateSentCliente,
                w9DateFiledCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.w9DateFiledCliente,
                coMakerNameCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.coMakerNameCliente,
                coNameTypeIndCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.coNameTypeIndCliente,
                coMakerEmployerCliente: HtmlEncode(data.terceraLlamadaPmpActualizarClienteSinSolicitud.coMakerEmployerCliente),
                coMakerPhoneFlagCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.coMakerPhoneFlagCliente,
                coMakerEmpPhoneCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.coMakerEmpPhoneCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.coMakerEmpPhoneCliente.substring(0, 11),
                coMakerPositionCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.coMakerPositionCliente,
                coMakerHomePhoneCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.coMakerHomePhoneCliente,
                coMakerSsanFlagCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.coMakerSsanFlagCliente,
                coMakerSsanCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.coMakerSsanCliente,
                userDemo1Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.userDemo1Cliente,
                userDemo2Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.userDemo2Cliente,
                memoLine1Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.memoLine1Cliente,
                memoLine2Cliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.memoLine2Cliente,
                lastMaintDateCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.lastMaintDateCliente,
                customerNumberCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.customerNumberCliente,
                relationshipNumberCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.relationshipNumberCliente,
                accountNumberCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.accountNumberCliente,
                cardNumberCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.cardNumberCliente, 
                cardSeqCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.cardSeqCliente,
                celNumberCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.celNumberCliente,
                trackTypeCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.trackTypeCliente,
                trackNameCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.trackNameCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.trackNameCliente.substring(0, 30),
                numberCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.numberCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.numberCliente.substring(0, 6),
                departmentNumberCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.departmentNumberCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.departmentNumberCliente.substring(0, 6),
                officeCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.officeCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.officeCliente.substring(0, 3),
                floorCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.floorCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.floorCliente.substring(0, 3),
                blockCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.blockCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.blockCliente.substring(0, 3),
                lotCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.lotCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.lotCliente.substring(0, 3),
                interiorCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.interiorCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.interiorCliente.substring(0, 3),
                sectorCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.sectorCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.sectorCliente.substring(0, 3),
                kilometerCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.kilometerCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.kilometerCliente.substring(0, 3),
                zoneTypeCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.zoneTypeCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.zoneTypeCliente.substring(0, 30),
                zoneNameCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.zoneNameCliente,
                referenceCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.referenceCliente,
                geoLocationCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.geoLocationCliente,
                workTypeCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workTypeCliente,
                workTrackNameCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workTrackNameCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.workTrackNameCliente.substring(0, 30),
                workNumberCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workNumberCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.workNumberCliente.substring(0, 6),
                workDepartmentNumberCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workDepartmentNumberCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.workDepartmentNumberCliente.substring(0, 6),
                workOfficeCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workOfficeCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.workOfficeCliente.substring(0, 3),
                workFloorCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workFloorCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.workFloorCliente.substring(0, 3),
                workBlockCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workBlockCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.workBlockCliente.substring(0, 3),
                workLotCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workLotCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.workLotCliente.substring(0, 3),
                workInteriorCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workInteriorCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.workInteriorCliente.substring(0, 3),
                workSectorCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workSectorCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.workSectorCliente.substring(0, 3),
                workKilometerCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workKilometerCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.workKilometerCliente.substring(0, 3),
                workZoneTypeCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workZoneTypeCliente,
                workZoneNameCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workZoneNameCliente && data.terceraLlamadaPmpActualizarClienteSinSolicitud.workZoneNameCliente.substring(0, 30),
                workReferenceCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workReferenceCliente,
                workGeoLocationCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workGeoLocationCliente,
                dualCoinFlagCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.dualCoinFlagCliente,
                homeAddressModificationDateCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.homeAddressModificationDateCliente,
                workAddressModificationDateCliente: data.terceraLlamadaPmpActualizarClienteSinSolicitud.workAddressModificationDateCliente
            }
        });

        
        return new Promise( (resolve, reject) => {
            axios
                .post(`${URL_ODC_REGULAR}/api/Regular/Pmp/PmpActualizarClienteSinSolicitud`, sendData, {
                    headers: {
                        'Accept':'application/json',
                        'Content-Type':'application/json',
                    }
                })
                .then(response => {
                    if(response.data){
                        // Launch Dispatch
                        if(response.data.success){
                            dispatch({ type: PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_SUCCESS, data:response.data, extra:extra });
                        }
                        else{
                            dispatch({
                                type: PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_INVALID,
                                data: null,
                                error: response.errorMessage,
                                response: true });
                        }
                    }
                    resolve( response);
                })
                .catch(error => {
                    // Error Request
                    let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE;
                    let response = error.response? true : false;
                    dispatch({type: PMP_EXPRESS_UPDATE_CLIENT_SAVE_PMP_ERROR, error:newError, response: response});
                    reject(error.response)
                });
        } )
    }
}