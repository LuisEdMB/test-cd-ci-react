import axios from 'axios';
import { callApi } from '../generic/api'
import * as moment from 'moment';
import { decrypt, getDeviceData } from '../../utils/Utils';
// URL - SERVER
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';
// Actions - First Call
export const ODC_EXPRESS_FIRST_CALL_REQUEST = 'ODC_EXPRESS_FIRST_CALL_REQUEST';
export const ODC_EXPRESS_FIRST_CALL_SUCCESS = 'ODC_EXPRESS_FIRST_CALL_SUCCESS';
export const ODC_EXPRESS_FIRST_CALL_INVALID = 'ODC_EXPRESS_FIRST_CALL_INVALID';
export const ODC_EXPRESS_FIRST_CALL_ERROR = 'ODC_EXPRESS_FIRST_CALL_ERROR';
// Actions - Second Call
export const ODC_EXPRESS_SECOND_CALL_REQUEST = 'ODC_EXPRESS_SECOND_CALL_REQUEST';
export const ODC_EXPRESS_SECOND_CALL_SUCCESS = 'ODC_EXPRESS_SECOND_CALL_SUCCESS';
export const ODC_EXPRESS_SECOND_CALL_INVALID = 'ODC_EXPRESS_SECOND_CALL_INVALID';
export const ODC_EXPRESS_SECOND_CALL_ERROR = 'ODC_EXPRESS_SECOND_CALL_ERROR';
// Actions - Summary 
export const ODC_EXPRESS_OFFER_SUMMARY_REQUEST = 'ODC_EXPRESS_OFFER_SUMMARY_REQUEST';
export const ODC_EXPRESS_OFFER_SUMMARY_SUCCESS = 'ODC_EXPRESS_OFFER_SUMMARY_SUCCESS';
export const ODC_EXPRESS_OFFER_SUMMARY_INVALID = 'ODC_EXPRESS_OFFER_SUMMARY_INVALID';
export const ODC_EXPRESS_OFFER_SUMMARY_ERROR = 'ODC_EXPRESS_OFFER_SUMMARY_ERROR';
// Actions - Commercial Offer
export const ODC_EXPRESS_COMMERCIAL_OFFER_REQUEST = 'ODC_EXPRESS_COMMERCIAL_OFFER_REQUEST';
export const ODC_EXPRESS_COMMERCIAL_OFFER_SUCCESS = 'ODC_EXPRESS_COMMERCIAL_OFFER_SUCCESS';
export const ODC_EXPRESS_COMMERCIAL_OFFER_INVALID = 'ODC_EXPRESS_COMMERCIAL_OFFER_INVALID';
export const ODC_EXPRESS_COMMERCIAL_OFFER_ERROR = 'ODC_EXPRESS_COMMERCIAL_OFFER_ERROR';
// Actions - Embossing
export const ODC_EXPRESS_PENDING_EMBOSSING_REQUEST = 'ODC_EXPRESS_PENDING_EMBOSSING_REQUEST';
export const ODC_EXPRESS_PENDING_EMBOSSING_SUCCESS = 'ODC_EXPRESS_PENDING_EMBOSSING_SUCCESS';
export const ODC_EXPRESS_PENDING_EMBOSSING_INVALID = 'ODC_EXPRESS_PENDING_EMBOSSING_INVALID';
export const ODC_EXPRESS_PENDING_EMBOSSING_ERROR = 'ODC_EXPRESS_PENDING_EMBOSSING_ERROR';
// Actions - Pending Processes
export const ODC_EXPRESS_PENDING_PROCESSES_REQUEST = 'ODC_EXPRESS_PENDING_PROCESSES_REQUEST';
export const ODC_EXPRESS_PENDING_PROCESSES_SUCCESS = 'ODC_EXPRESS_PENDING_PROCESSES_SUCCESS';
export const ODC_EXPRESS_PENDING_PROCESSES_INVALID = 'ODC_EXPRESS_PENDING_PROCESSES_INVALID';
export const ODC_EXPRESS_PENDING_PROCESSES_ERROR = 'ODC_EXPRESS_PENDING_PROCESSES_ERROR';
// Actions - Continue Process
export const ODC_EXPRESS_CONTINUE_PROCESS_REQUEST = 'ODC_EXPRESS_CONTINUE_PROCESS_REQUEST';
export const ODC_EXPRESS_CONTINUE_PROCESS_SUCCESS = 'ODC_EXPRESS_CONTINUE_PROCESS_SUCCESS';
export const ODC_EXPRESS_CONTINUE_PROCESS_INVALID = 'ODC_EXPRESS_CONTINUE_PROCESS_INVALID';
export const ODC_EXPRESS_CONTINUE_PROCESS_ERROR = 'ODC_EXPRESS_CONTINUE_PROCESS_ERROR';
// Actions - Pending Processes Activation
export const ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_REQUEST = 'ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_REQUEST';
export const ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_INVALID = 'ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_INVALID';
export const ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_SUCCESS = 'ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_SUCCESS';
export const ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_ERROR = 'ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_ERROR';
// Actions - Pending Processes Activation
export const ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_REQUEST = 'ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_REQUEST';
export const ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_INVALID = 'ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_INVALID';
export const ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_SUCCESS = 'ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_SUCCESS';
export const ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_ERROR = 'ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_ERROR';
// Actions - authorize Activation Process
export const ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_REQUEST = 'ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_REQUEST';
export const ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS = 'ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS';
export const ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_INVALID = 'ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_INVALID';
export const ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_ERROR = 'ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_ERROR';
// Actions - Reject Process 
export const ODC_EXPRESS_REJECT_PROCESS_REQUEST = 'ODC_EXPRESS_REJECT_PROCESS_REQUEST'
export const ODC_EXPRESS_REJECT_PROCESS_INVALID = 'ODC_EXPRESS_REJECT_PROCESS_INVALID'
export const ODC_EXPRESS_REJECT_PROCESS_SUCCESS = 'ODC_EXPRESS_REJECT_PROCESS_SUCCESS'
export const ODC_EXPRESS_REJECT_PROCESS_ERROR = 'ODC_EXPRESS_REJECT_PROCESS_ERROR'
// Actions - Reject Process 
export const ODC_EXPRESS_CANCEL_PROCESS_REQUEST = 'ODC_EXPRESS_CANCEL_PROCESS_REQUEST'
export const ODC_EXPRESS_CANCEL_PROCESS_INVALID = 'ODC_EXPRESS_CANCEL_PROCESS_INVALID'
export const ODC_EXPRESS_CANCEL_PROCESS_SUCCESS = 'ODC_EXPRESS_CANCEL_PROCESS_SUCCESS'
export const ODC_EXPRESS_CANCEL_PROCESS_ERROR = 'ODC_EXPRESS_CANCEL_PROCESS_ERROR'
// Actions - Pending Activate SAE
export const ODC_EXPRESS_PENDING_ACTIVATE_SAE_REQUEST = 'ODC_EXPRESS_PENDING_ACTIVATE_SAE_REQUEST';
export const ODC_EXPRESS_PENDING_ACTIVATE_SAE_SUCCESS = 'ODC_EXPRESS_PENDING_ACTIVATE_SAE_SUCCESS';
export const ODC_EXPRESS_PENDING_ACTIVATE_SAE_INVALID = 'ODC_EXPRESS_PENDING_ACTIVATE_SAE_INVALID';
export const ODC_EXPRESS_PENDING_ACTIVATE_SAE_ERROR = 'ODC_EXPRESS_PENDING_ACTIVATE_SAE_ERROR';
// Actions - Pending Activate SAE Prototype
export const ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_REQUEST = 'ODC_EXPRESS_PENDING_ACTIVATE_SAE_REQUEST';
export const ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_SUCCESS = 'ODC_EXPRESS_PENDING_ACTIVATE_SAE_SUCCESS';
export const ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_INVALID = 'ODC_EXPRESS_PENDING_ACTIVATE_SAE_INVALID';
export const ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_ERROR = 'ODC_EXPRESS_PENDING_ACTIVATE_SAE_ERROR';
// PreloaderClient
export const ODC_EXPRESS_LOAD_CLIENT_REQUEST = 'ODC_EXPRESS_LOAD_CLIENT_REQUEST';
export const ODC_EXPRESS_LOAD_CLIENT_SUCCESS = 'ODC_EXPRESS_LOAD_CLIENT_SUCCESS';
export const ODC_EXPRESS_LOAD_CLIENT_INVALID = 'ODC_EXPRESS_LOAD_CLIENT_INVALID';
export const ODC_EXPRESS_LOAD_CLIENT_ERROR = 'ODC_EXPRESS_LOAD_CLIENT_ERROR';
// Actions- Update-Client-Module-load-client
export const ODC_EXPRESS_UPDATE_CLIENT_REQUEST = 'ODC_EXPRESS_UPDATE_CLIENT_REQUEST';
export const ODC_EXPRESS_UPDATE_CLIENT_SUCCESS = 'ODC_EXPRESS_UPDATE_CLIENT_SUCCESS';
export const ODC_EXPRESS_UPDATE_CLIENT_INVALID = 'ODC_EXPRESS_UPDATE_CLIENT_INVALID';
export const ODC_EXPRESS_UPDATE_CLIENT_ERROR = 'ODC_EXPRESS_UPDATE_CLIENT_ERROR';
// Actions- Update-Client-Module-load-client-data
export const ODC_EXPRESS_UPDATE_CLIENT_DATA_REQUEST = 'ODC_EXPRESS_UPDATE_CLIENT_DATA_REQUEST';
export const ODC_EXPRESS_UPDATE_CLIENT_DATA_SUCCESS = 'ODC_EXPRESS_UPDATE_CLIENT_DATA_SUCCESS';
export const ODC_EXPRESS_UPDATE_CLIENT_DATA_INVALID = 'ODC_EXPRESS_UPDATE_CLIENT_DATA_INVALID';
export const ODC_EXPRESS_UPDATE_CLIENT_DATA_ERROR = 'ODC_EXPRESS_UPDATE_CLIENT_DATA_ERROR';
// Actions - Update-Client-Data-Save
export const ODC_EXPRESS_UPDATE_CLIENT_SAVE_REQUEST = 'ODC_EXPRESS_UPDATE_CLIENT_SAVE_REQUEST';
export const ODC_EXPRESS_UPDATE_CLIENT_SAVE_SUCCESS = 'ODC_EXPRESS_UPDATE_CLIENT_SAVE_SUCCESS';
export const ODC_EXPRESS_UPDATE_CLIENT_SAVE_INVALID = 'ODC_EXPRESS_UPDATE_CLIENT_SAVE_INVALID';
export const ODC_EXPRESS_UPDATE_CLIENT_SAVE_ERROR = 'ODC_EXPRESS_UPDATE_CLIENT_SAVE_ERROR';
// Actions-Create-Business
export const ODC_EXPRESS_CREATE_BUSINESS_REQUEST = 'ODC_EXPRESS_CREATE_BUSINESS_REQUEST';
export const ODC_EXPRESS_CREATE_BUSINESS_SUCCESS = 'ODC_EXPRESS_CREATE_BUSINESS_SUCCESS';
export const ODC_EXPRESS_CREATE_BUSINESS_INVALID = 'ODC_EXPRESS_CREATE_BUSINESS_INVALID';
export const ODC_EXPRESS_CREATE_BUSINESS_ERROR = 'ODC_EXPRESS_CREATE_BUSINESS_ERROR';
// Actions-Search-Business
export const ODC_EXPRESS_SEARCH_BUSINESS_REQUEST = 'ODC_EXPRESS_SEARCH_BUSINESS_REQUEST';
export const ODC_EXPRESS_SEARCH_BUSINESS_SUCCESS = 'ODC_EXPRESS_SEARCH_BUSINESS_SUCCESS';
export const ODC_EXPRESS_SEARCH_BUSINESS_INVALID = 'ODC_EXPRESS_SEARCH_BUSINESS_INVALID';
export const ODC_EXPRESS_SEARCH_BUSINESS_ERROR = 'ODC_EXPRESS_SEARCH_BUSINESS_ERROR';
// Actions-Update-Business
export const ODC_EXPRESS_UPDATE_BUSINESS_REQUEST = 'ODC_EXPRESS_UPDATE_BUSINESS_REQUEST';
export const ODC_EXPRESS_UPDATE_BUSINESS_SUCCESS = 'ODC_EXPRESS_UPDATE_BUSINESS_SUCCESS';
export const ODC_EXPRESS_UPDATE_BUSINESS_INVALID = 'ODC_EXPRESS_UPDATE_BUSINESS_INVALID';
export const ODC_EXPRESS_UPDATE_BUSINESS_ERROR = 'ODC_EXPRESS_UPDATE_BUSINESS_ERROR';
// Actions.Delete-Business
export const ODC_EXPRESS_DELETE_BUSINESS_REQUEST = 'ODC_EXPRESS_DELETE_BUSINESS_REQUEST';
export const ODC_EXPRESS_DELETE_BUSINESS_SUCCESS = 'ODC_EXPRESS_DELETE_BUSINESS_SUCCESS';
export const ODC_EXPRESS_DELETE_BUSINESS_INVALID = 'ODC_EXPRESS_DELETE_BUSINESS_INVALID';
export const ODC_EXPRESS_DELETE_BUSINESS_ERROR = 'ODC_EXPRESS_DELETE_BUSINESS_ERROR';
// Actions-Create-comment
export const ODC_EXPRESS_CREATE_COMMENT_REQUEST = 'ODC_EXPRESS_CREATE_COMMENT_REQUEST';
export const ODC_EXPRESS_CREATE_COMMENT_SUCCESS = 'ODC_EXPRESS_CREATE_COMMENT_SUCCESS';
export const ODC_EXPRESS_CREATE_COMMENT_INVALID = 'ODC_EXPRESS_CREATE_COMMENT_INVALID';
export const ODC_EXPRESS_CREATE_COMMENT_ERROR = 'ODC_EXPRESS_CREATE_COMMENT_ERROR';
// Actions-Load-Comment
export const ODC_EXPRESS_LOAD_COMMENT_REQUEST = 'ODC_EXPRESS_LOAD_COMMENT_REQUEST';
export const ODC_EXPRESS_LOAD_COMMENT_SUCCESS = 'ODC_EXPRESS_LOAD_COMMENT_SUCCESS';
export const ODC_EXPRESS_LOAD_COMMENT_INVALID = 'ODC_EXPRESS_LOAD_COMMENT_INVALID';
export const ODC_EXPRESS_LOAD_COMMENT_ERROR = 'ODC_EXPRESS_LOAD_COMMENT_ERROR';
// Actions - Verify Solicitude is Blocked
export const ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_REQUEST = 'ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_REQUEST'
export const ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_SUCCESS = 'ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_SUCCESS'
export const ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_INVALID = 'ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_INVALID'
export const ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_ERROR = 'ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_ERROR'
// Actions - Block Solicitude
export const ODC_EXPRESS_BLOCK_SOLICITUDE_REQUEST = 'ODC_EXPRESS_BLOCK_SOLICITUDE_REQUEST'
export const ODC_EXPRESS_BLOCK_SOLICITUDE_SUCCESS = 'ODC_EXPRESS_BLOCK_SOLICITUDE_SUCCESS'
export const ODC_EXPRESS_BLOCK_SOLICITUDE_INVALID = 'ODC_EXPRESS_BLOCK_SOLICITUDE_INVALID'
export const ODC_EXPRESS_BLOCK_SOLICITUDE_ERROR = 'ODC_EXPRESS_BLOCK_SOLICITUDE_ERROR'
// Actions - Unlock Solicitude
export const ODC_EXPRESS_UNLOCK_SOLICITUDE_REQUEST = 'ODC_EXPRESS_UNLOCK_SOLICITUDE_REQUEST'
export const ODC_EXPRESS_UNLOCK_SOLICITUDE_SUCCESS = 'ODC_EXPRESS_UNLOCK_SOLICITUDE_SUCCESS'
export const ODC_EXPRESS_UNLOCK_SOLICITUDE_INVALID = 'ODC_EXPRESS_UNLOCK_SOLICITUDE_INVALID'
export const ODC_EXPRESS_UNLOCK_SOLICITUDE_ERROR = 'ODC_EXPRESS_UNLOCK_SOLICITUDE_ERROR'

// POST
export function firstCall(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({ type: ODC_EXPRESS_FIRST_CALL_REQUEST });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        // Launch Dispatch - Server
        let ODCFirstCallData = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_cliente: data.cod_cliente,
            cod_tipo_solicitud: data.cod_tipo_solicitud,
            cod_tipo_relacion: data.cod_tipo_relacion,
            cod_tipo_solicitud_requerimiento: data.cod_tipo_solicitud_requerimiento,
            cod_etapa_maestro: data.cod_etapa_maestro
        });
        return axios
            .post(`${URL_ODC}/Cliente/ConsultaCliente`, ODCFirstCallData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {

                    if (response.data.success) {
                        let { sae } = response.data;

                        let {
                            primeraLlamadaCda,
                            fraude, solicitud_EnProceso_Siebel,
                            adn, scoring, validacionesTotales,
                            cod_cliente,
                            cod_solicitud,
                            cod_solicitud_actividad,
                            cod_solicitud_completa,
                            productosLineasCredito,
                            num_linea_cec,
                            num_linea_disponible,
                            num_linea_global,
                            num_linea_tc_cda
                        } = response.data;

                        // Origination 
                        let origination = {
                            number: cod_solicitud ? cod_solicitud : 0,
                            activityNumber: cod_solicitud_actividad ? cod_solicitud_actividad : 0,
                            fullNumber: cod_solicitud_completa ? cod_solicitud_completa : "",
                        }

                        // Client
                        let client = {
                            id: cod_cliente ? cod_cliente : 0,
                            documentTypeId: extra.documentTypeId,
                            documentType: data.tipo_doc_letra,
                            documentNumber: data.nro_doc,
                            documentTypeInternalValue: extra.documentTypeInternalValue
                        };

                        if (scoring) {
                            client = {
                                ...client,
                                fullName: scoring.row.nombres,
                            }
                        }

                        // ADN
                        if (adn) {
                            client = {
                                ...client,
                                email: adn.email !== null ? adn.email : ""
                            }
                            adn = {
                                lic: adn.lic === "N" || adn.lic === null ? 1 : 0,
                                pep: adn.pep === "N" || adn.pep === null ? 1 : 0,
                                error: adn.responseDesc
                            }
                        }

                        if (sae) {
                            sae = {
                                lineSAE: sae.linea_sae ? sae.linea_sae : 0,
                                lineAvailable: sae.linea_tc ? sae.linea_tc : 0,
                                maxAmount: sae.monto_max ? sae.monto_max : 0,
                                pctSAE1: sae.pct_sae_1 ? sae.pct_sae_1 : "",
                                pctSAE2: sae.pct_sae_2 ? sae.pct_sae_2 : "",
                                baseDate: sae.fecha_act ? sae.fecha_act : "",
                                block: sae.flg_bq2 ? sae.flg_bq2 : ""
                            };
                        }

                        let dataFC = {
                            origination: {
                                ...origination
                            },
                            cda: {
                                ...primeraLlamadaCda
                            },
                            client: {
                                ...client,
                            },
                            adn: {
                                ...adn
                            },
                            creditProducts: productosLineasCredito,
                            fraudPrevention: fraude ? fraude : null,
                            siebel: {
                                ...solicitud_EnProceso_Siebel
                            },
                            validations: validacionesTotales,
                            sae: {
                                ...sae
                            },
                            initialLineAvailable: {
                                lineAvailableEc: num_linea_cec || 0,
                                lineAvailable: num_linea_disponible || 0,
                                globalLineaAvailable: num_linea_global || 0,
                                lineAvailableTc: num_linea_tc_cda || 0
                            }
                        }
                        dispatch({ type: ODC_EXPRESS_FIRST_CALL_SUCCESS, data: dataFC, error: response.data.errorMessage, extra: extra });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_FIRST_CALL_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });

                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_FIRST_CALL_ERROR, error: newError, response: response });
                return error;
            });
    }
}
// POST
export function secondCall(data) {
    return (dispatch) => {
        // Begin Request
        dispatch({ type: ODC_EXPRESS_SECOND_CALL_REQUEST });
        const dataEncrypt = sessionStorage.getItem('data');
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        let ODCSecondCallData = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            tcType: "VISA",
            maritalStatus: data.maritalStatus,
            dateJoinedCompany: data.dateJoinedCompany,
            birthDate: data.birthDate,
            nationality: data.nationality,
            studiesLevel: data.studiesLevel,
            occupation: data.occupation,
            employerRUC: data.employerRUC,
            gender: data.gender,
            laborSituation: data.laborSituation,
            cellPhone: data.cellPhone,
            homePhone: data.homePhone,
            workPhone: data.workPhone,
            housingType: data.housingType,
            //Register
            cod_cliente: data.cod_cliente,
            cod_tipo_documento: data.cod_tipo_documento,
            des_nro_documento: data.des_nro_documento,
            cod_cliente_documento: data.cod_cliente_documento,
            des_primer_nom: data.des_primer_nom,
            des_segundo_nom: data.des_segundo_nom,
            des_ape_paterno: data.des_ape_paterno,
            des_ape_materno: data.des_ape_materno,
            fec_nacimiento: data.fec_nacimiento,
            cod_genero: data.cod_genero,
            cod_estado_civil: data.cod_estado_civil, // Change 
            cod_nacionalidad: data.cod_nacionalidad,
            cod_pais_residencia: data.cod_pais_residencia,
            cod_gradoacademico: data.cod_gradoacademico,
            des_correo: data.des_correo,
            des_telef_celular: data.des_telef_celular,
            des_telef_fijo: data.des_telef_fijo,
            cod_tipo_cliente: data.cod_tipo_cliente,
            cod_situacion_laboral: data.cod_situacion_laboral,
            cod_cargo_profesion: data.cod_cargo_profesion,
            cod_actividad_economica: data.cod_actividad_economica,
            fec_ingreso_laboral: data.fec_ingreso_laboral,
            des_ruc: data.des_ruc,
            des_razon_social: data.des_razon_social,
            num_ingreso_bruto: data.num_ingreso_bruto,
            des_telefono_laboral: data.des_telefono_laboral,
            des_anexo_laboral: data.des_anexo_laboral,
            flg_autoriza_datos_per: data.flg_autoriza_datos_per,
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_fecha_pago: data.cod_fecha_pago,
            cod_envio_correspondencia: data.cod_envio_correspondencia,
            cod_estado_cuenta: data.cod_estado_cuenta,
            flg_retiro_efectivo: data.flg_retiro_efectivo,
            // flg_alerta_uso_tar:data.flg_alerta_uso_tar, 
            // cod_envio_comunicacion:data.cod_envio_comunicacion,
            // flg_envio_comunicacion:data.flg_envio_comunicacion,
            flg_consumo_internet: data.flg_consumo_internet,
            flg_consumo_extranjero: data.flg_consumo_extranjero,
            flg_retirar_efectivo: data.flg_retirar_efectivo,
            flg_sobregirar_credito: data.flg_sobregirar_credito,
            flg_notificacion_email: data.flg_notificacion_email,
            num_monto_minimo: data.num_monto_minimo,
            cod_resp_promotor: data.cod_resp_promotor,
            cod_resp_registro: data.cod_resp_registro,
            cod_resp_actualiza: data.cod_resp_actualiza,
            cod_agencia: decryptData.agency ? decryptData.agency.ide_agencia : 0,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            nom_actividad: data.nom_actividad,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_valor_color: data.cod_valor_color,
            disp_efec_porcentaje: data.disp_efec_porcentaje, // Default
            disp_efec_monto: data.disp_efec_monto, // Default
            cod_valor_marca: data.cod_valor_marca, // Default
            linea_credito_oferta: data.linea_credito_oferta, // Default
            linea_credito_final: data.linea_credito_final, // Default
            list_direccion: data.list_direccion,
            cod_producto: data.cod_producto,
            cod_motivo_emboce: data.cod_motivo_emboce,
            des_motivo_comentario_emboce: data.des_motivo_comentario_emboce,
            cod_laboral: data.cod_laboral,

            flg_biometria: data.flg_biometria,
            cod_tipo_relacion: data.cod_tipo_relacion,
            cod_tipo_solicitud: data.cod_tipo_solicitud,
            cod_tipo_solicitud_requerimiento: data.cod_tipo_solicitud_requerimiento,

        })
        return axios
            .post(`${URL_ODC}/Oferta/OfertaComercial`, ODCSecondCallData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_SECOND_CALL_SUCCESS, data: response.data });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_SECOND_CALL_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_SECOND_CALL_ERROR, error: newError, response: response });
                return error;
            });
    }

}
// POST
export function updateClientSave(data) {
    return (dispatch) => {
        // Begin Request
        dispatch({ type: ODC_EXPRESS_UPDATE_CLIENT_SAVE_REQUEST });
        const dataEncrypt = sessionStorage.getItem('data');
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        let ODCUpdateClientData = JSON.stringify({

            ent_tt_ori_cliente: {
                cod_cliente: data.cod_cliente,
                cod_tipo_cliente: data.cod_tipo_cliente,
                cod_tipo_documento: 1,
                des_nro_documento: data.des_nro_documento,
                cod_cliente_documento: data.cod_cliente_documento,
                des_primer_nom: data.des_primer_nom,
                des_segundo_nom: data.des_segundo_nom,
                des_ape_paterno: data.des_ape_paterno,
                des_ape_materno: data.des_ape_materno,
                fec_nacimiento: data.fec_nacimiento,
                cod_genero: data.cod_genero,
                cod_estado_civil: data.cod_estado_civil,
                cod_pais_residencia: data.cod_pais_residencia,
                cod_nacionalidad: data.cod_nacionalidad,
                des_nacionalidad: data.des_nacionalidad,
                cod_gradoacademico: data.cod_gradoacademico,
                des_gradoacademico: data.des_gradoacademico,
                des_correo: data.des_correo,
                des_telef_celular: data.des_telef_celular,
                des_telef_fijo: data.des_telef_fijo,
                cod_laboral: data.cod_laboral,
                cod_situacion_laboral: data.cod_situacion_laboral,
                //cod_cargo_profesion: data.data.cod_situacion_laboral,
                cod_actividad_economica: data.cod_actividad_economica,
                fec_ingreso_laboral: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_ruc: data.des_ruc,
                des_razon_social: data.des_razon_social,
                num_ingreso_bruto: data.num_ingreso_bruto,
                des_telefono_laboral: data.des_telefono_laboral,
                des_anexo_laboral: data.des_anexo_laboral,
                flg_autoriza_datos_per: false,
                flg_visible: true,
                flg_eliminado: false,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg: decryptData.username,
                des_ter_reg: device.ip,
                fec_act: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_act: decryptData.username,
                des_ter_act: ""

            },
            list_tt_ori_direccion: [
                {
                    cod_direccion: data.list_tt_ori_direccion[0].cod_direccion,
                    cod_cliente: data.cod_cliente,
                    cod_tipo_direccion: data.list_tt_ori_direccion[0].cod_tipo_direccion,
                    des_tipo_direccion: data.list_tt_ori_direccion[0].des_tipo_direccion,
                    cod_ubigeo: data.list_tt_ori_direccion[0].cod_ubigeo,
                    cod_via: data.list_tt_ori_direccion[0].cod_via,
                    des_nombre_via: data.list_tt_ori_direccion[0].des_nombre_via,
                    des_nro: data.list_tt_ori_direccion[0].des_nro,
                    des_departamento: data.list_tt_ori_direccion[0].des_departamento,
                    des_interior: data.list_tt_ori_direccion[0].des_interior,
                    des_manzana: data.list_tt_ori_direccion[0].des_manzana,
                    des_lote: data.list_tt_ori_direccion[0].des_lote,
                    cod_zona: data.list_tt_ori_direccion[0].cod_zona,
                    des_nombre_zona: data.list_tt_ori_direccion[0].des_nombre_zona,
                    cod_tipo_residencia: data.list_tt_ori_direccion[0].cod_tipo_residencia,
                    cod_tipo_vivienda: data.list_tt_ori_direccion[0].cod_tipo_vivienda,
                    des_referencia_domicilio: data.list_tt_ori_direccion[0].des_referencia_domicilio,
                    des_tipo_vivienda: data.list_tt_ori_direccion[0].des_tipo_vivienda,
                    flg_visible: true,
                    flg_eliminado: false,
                    fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                    des_usu_reg: decryptData.username,
                    des_ter_reg: device.ip,
                    fec_act: null,
                    des_usu_act: "",
                    des_ter_act: "",
                },
                {
                    cod_direccion: data.list_tt_ori_direccion[1].cod_direccion,
                    cod_cliente: data.cod_cliente,
                    cod_tipo_direccion: data.list_tt_ori_direccion[1].cod_tipo_direccion,
                    des_tipo_direccion: data.list_tt_ori_direccion[1].des_tipo_direccion,
                    cod_ubigeo: data.list_tt_ori_direccion[1].cod_ubigeo,
                    cod_via: data.list_tt_ori_direccion[1].cod_via,
                    des_nombre_via: data.list_tt_ori_direccion[1].des_nombre_via,
                    des_nro: data.list_tt_ori_direccion[1].des_nro,
                    des_departamento: data.list_tt_ori_direccion[1].des_departamento,
                    des_interior: data.list_tt_ori_direccion[1].des_interior,
                    des_manzana: data.list_tt_ori_direccion[1].des_manzana,
                    des_lote: data.list_tt_ori_direccion[1].des_lote,
                    cod_zona: data.list_tt_ori_direccion[1].cod_zona,
                    des_nombre_zona: data.list_tt_ori_direccion[1].des_nombre_zona,
                    cod_tipo_residencia: data.list_tt_ori_direccion[1].cod_tipo_residencia,
                    cod_tipo_vivienda: data.list_tt_ori_direccion[1].cod_tipo_vivienda,
                    des_referencia_domicilio: data.list_tt_ori_direccion[1].des_referencia_domicilio,
                    flg_visible: true,
                    flg_eliminado: false,
                    fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                    des_usu_reg: decryptData.username,
                    des_ter_reg: device.ip,
                    fec_act: null,
                    des_usu_act: "",
                    des_ter_act: "",
                    des_tipo_vivienda: data.list_tt_ori_direccion[1].des_tipo_vivienda,
                }
            ],
            ent_tt_ori_cliente_actividad: {
                cod_cliente_actividad: 0,
                cod_cliente: data.cod_cliente,
                nom_actividad: "Actualizacion cliente",
                des_usu_responsable: decryptData.username,
                cod_flujo_fase_estado_actual: 31213,
                cod_flujo_fase_estado_anterior: 31211,
                fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                fec_actividad_fin: null,
                des_xml_request: "",
                des_xml_response: "",
                des_json_request: "",
                des_json_response: "",
                cod_error_servicio: "",
                des_error_message_servicio: "",
                flg_visible: true,
                flg_eliminado: false,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg: decryptData.username,
                des_ter_reg: device.ip,
                fec_act: null,
                des_usu_act: "",
                des_ter_act: ""
            },
        })
        return new Promise((resolve, reject) => {
            axios
                .post(`${URL_ODC}/Cliente/ActualizarClienteSinSolicitud`, ODCUpdateClientData, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    }
                })
                .then(response => {
                    // Launch Dispatch
                    if (response.data) {
                        if (response.data.success) {
                            dispatch({ type: ODC_EXPRESS_UPDATE_CLIENT_SAVE_SUCCESS, data: response.data });
                        }
                        else {
                            dispatch({
                                type: ODC_EXPRESS_UPDATE_CLIENT_SAVE_INVALID,
                                data: null,
                                error: response.data.errorMessage,
                                response: true
                            });
                        }
                    }
                    resolve(response);
                })
                .catch(error => {
                    // Error Request
                    let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                    let response = error.response ? true : false;
                    dispatch({ type: ODC_EXPRESS_UPDATE_CLIENT_SAVE_ERROR, error: newError, response: response });
                    reject(error);
                });
        })
    }

}
// POST
export function updateBusiness(data) {
    return (dispatch) => {
        // Begin Request
        dispatch({ type: ODC_EXPRESS_UPDATE_BUSINESS_REQUEST });
        const dataEncrypt = sessionStorage.getItem('data');
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        let ODCUpdateClientData = JSON.stringify({
            empresa: {
                cod_emp: data.cod_emp,
                nom_emp: data.nom_emp,
                ruc_emp: data.ruc_emp,
                des_observaciones: data.des_observaciones,

                flg_visible: data.flg_visible,
                flg_eliminado: !data.flg_visible,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg: decryptData.username,
                des_ter_reg: device.ip,
                fec_act: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_act: decryptData.username,
                des_ter_act: ""
            }
        })
        return new Promise((resolve, reject) => {
            axios
                .post(`${URL_ODC}/Empresa/ActualizarEmpresa`, ODCUpdateClientData, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    }
                })
                .then(response => {
                    // Launch Dispatch
                    if (response.data) {
                        if (response.data.success) {
                            dispatch({ type: ODC_EXPRESS_UPDATE_BUSINESS_SUCCESS, data: response.data });
                        }
                        else {
                            dispatch({
                                type: ODC_EXPRESS_UPDATE_BUSINESS_INVALID,
                                data: null,
                                error: response.data.errorMessage,
                                response: true
                            });
                        }
                    }
                    resolve(response);
                })
                .catch(error => {
                    // Error Request
                    let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                    let response = error.response ? true : false;

                    dispatch({ type: ODC_EXPRESS_UPDATE_BUSINESS_ERROR, error: newError, response: response });
                    reject(error);
                });
        })
    }

}
// GET
export function pendingProcesses(data) {
    return (dispatch) => {
        //Begin Request
        dispatch({ type: ODC_EXPRESS_PENDING_PROCESSES_REQUEST });
        return axios
            .get(`${URL_ODC}/Originacion/Solicitudes?cod_tipo_documento=${data.cod_tipo_documento}&des_nro_documento=${data.des_nro_documento}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_PENDING_PROCESSES_SUCCESS, data: response.data.solicitudes });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_PENDING_PROCESSES_INVALID,
                            data: [],
                            error: response.data.errorMessage,
                            response: true
                        });
                        return response;
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request

                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_PENDING_PROCESSES_ERROR, error: newError, response: response });
                return error;
            });
    };
}
// GET
export function continueProcess(originationNumber, documentNumber) {
    return (dispatch) => {
        //Begin Request
        dispatch({ type: ODC_EXPRESS_CONTINUE_PROCESS_REQUEST });
        return axios
            .get(`${URL_ODC}/Originacion/SolicitudRetomar?cod_solicitud_completa=${originationNumber}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_CONTINUE_PROCESS_SUCCESS, data: response.data.solicitud_Retomar });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_CONTINUE_PROCESS_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request

                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_CONTINUE_PROCESS_ERROR, error: newError, response: response });
                return error;
            });
    };
}
// GET
export function pendingProcessesActivation(data) {
    return (dispatch) => {
        //Begin Request
        dispatch({ type: ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_REQUEST });
        return axios
            .get(`${URL_ODC}/Originacion/SolicitudesPorAutorizar`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_SUCCESS, data: response.data.solicitudesEnRevision });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_INVALID,
                            data: [],
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_PENDING_PROCESSES_ACTIVATION_ERROR, error: newError, response: response });
                return error;
            });
    };
}

// GET
export function pendingProcessesCancellation(data) {

    return (dispatch) => {
        //Begin Request
        dispatch({ type: ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_REQUEST });
        return axios
            .get(`${URL_ODC}/Cancelaciones/SolicitudesAptasParaCancelacion?des_nro_documento=${data.des_nro_documento}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_SUCCESS, data: response.data.solicitudesPendienteActivacionSae });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_INVALID,
                            data: [],
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_PENDING_PROCESSES_CANCELLATION_ERROR, error: newError, response: response });
                return error;
            });
    };
}
// POST
export function cancelProcess(data, extra = null, next = false) {
    return (dispatch) => {
        // Begin Request
        dispatch({ type: ODC_EXPRESS_CANCEL_PROCESS_REQUEST });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_solicitud_cancelacion: 0,
            des_comentario: data.des_observacion,
            flg_visible: true,
            flg_eliminado: true,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_act: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_agencia: decryptData.agency ? decryptData.agency.ide_agencia : 0,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            des_usu_act: decryptData.username,
            des_ter_act: device.ip
        });
        return axios
            .post(`${URL_ODC}/Cancelaciones/AgregarCancelacion`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_EXPRESS_CANCEL_PROCESS_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                            next: next
                        });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_CANCEL_PROCESS_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_CANCEL_PROCESS_ERROR, error: newError, response: response });
                return error;
            });
    }
}
// GET
export function UpdateClientQuery(documentTypeId, documentNumber, clientName) {
    return (dispatch) => {
        dispatch({ type: ODC_EXPRESS_UPDATE_CLIENT_REQUEST });
        return axios
            .get(`${URL_ODC}/Requerimiento/ActualizarCliente/BuscarCliente?cod_tipo_documento=${documentTypeId}&des_nro_documento=${documentNumber}&apellidos_nombres=${clientName}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {

                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_UPDATE_CLIENT_SUCCESS, data: response.data.buscarClienteActualizar });
                    } else {
                        dispatch({
                            type: ODC_EXPRESS_UPDATE_CLIENT_INVALID,
                            data: [],
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_UPDATE_CLIENT_ERROR, error: newError, response: response });
                return error;
            });
    };
}
export function UpdateClientQueryData(documentTypeId, documentNumber) {
    return (dispatch) => {
        dispatch({ type: ODC_EXPRESS_UPDATE_CLIENT_DATA_REQUEST });

        return axios
            .get(`${URL_ODC}/Requerimiento/ActualizarCliente/SeleccionarCliente?cod_tipo_documento=${documentTypeId}&des_nro_documento=${documentNumber}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {

                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_UPDATE_CLIENT_DATA_SUCCESS, data: response.data.SeleccionarCliente });
                    } else {
                        dispatch({
                            type: ODC_EXPRESS_UPDATE_CLIENT_DATA_INVALID,
                            data: [],
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_UPDATE_CLIENT_DATA_ERROR, error: newError, response: response });
                return error;
            });
    };
}
// POST
export function authorizeActivationProcess(data, extra = null, next = false) {
    return (dispatch) => {
        // Begin Request
        dispatch({ type: ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_REQUEST });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_requerimiento: data.cod_requerimiento,
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            nom_actividad: data.nom_actividad,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip
        });
        return axios
            .post(`${URL_ODC}/Originacion/Express/RegistrarActividadGenericoSimple`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                            next: next
                        });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_AUTHORIZE_ACTIVATION_PROCESS_ERROR, error: newError, response: response });
                return error;
            });
    }
}
// POST
export function rejectProcess(data, extra = null, next = false) {
    return (dispatch) => {
        // Begin Request
        dispatch({ type: ODC_EXPRESS_REJECT_PROCESS_REQUEST });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            nom_actividad: data.nom_actividad,
            des_observacion: data.des_observacion,
            cod_agencia: decryptData.agency ? decryptData.agency.ide_agencia : 0,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip

        });
        return axios
            .post(`${URL_ODC}/Originacion/RegistrarActividadRechazar`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_EXPRESS_REJECT_PROCESS_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                            next: next
                        });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_REJECT_PROCESS_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_REJECT_PROCESS_ERROR, error: newError, response: response });
                return error;
            });
    }
}
// POST
export function offerSummary(data, extra = null) {
    return (dispatch) => {
        //Begin Request
        dispatch({ type: ODC_EXPRESS_OFFER_SUMMARY_REQUEST });
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        let sendData = JSON.stringify({
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            nom_actividad: data.nom_actividad,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            cod_valor_color: data.cod_valor_color,
            disp_efec_porcentaje: data.disp_efec_porcentaje,
            cod_valor_marca: data.cod_valor_marca,
            linea_credito_oferta: data.linea_credito_oferta,
            linea_credito_final: data.linea_credito_final
        });
        return axios
            .post(`${URL_ODC}/Originacion/Express/RegistrarActividadResumen`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_EXPRESS_OFFER_SUMMARY_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_OFFER_SUMMARY_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_OFFER_SUMMARY_ERROR, error: newError, response: response });
                return error;
            });
    };
}
// POST
export function commercialOffer(data, extra = null) {
    return (dispatch) => {
        //Begin Request
        dispatch({ type: ODC_EXPRESS_COMMERCIAL_OFFER_REQUEST });
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        let sendData = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_valor_color: data.cod_valor_color,
            disp_efec_porcentaje: data.disp_efec_porcentaje,
            disp_efec_monto: data.disp_efec_monto,
            cod_producto: data.cod_producto,
            cod_valor_marca: data.cod_valor_marca,
            linea_credito_oferta: data.linea_credito_oferta,
            linea_credito_final: data.linea_credito_final,
            linea_credito_global: data.linea_credito_global,
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_cliente: data.cod_cliente,

            cod_agencia: decryptData.agency ? decryptData.agency.ide_agencia : 0,
            fec_act_base_sae: data.fec_act_base_sae ? data.fec_act_base_sae : "",
            flg_bq2: data.flg_bq2 ? data.flg_bq2 : "",

            linea_sae: data.linea_sae ? data.linea_sae : 0,
            linea_sae_final: data.linea_sae_final ? data.linea_sae_final : 0,
            monto_max: data.monto_max ? data.monto_max : 0,
            monto_max_final: data.monto_max_final ? data.monto_max_final : 0,
            pct_sae_1: data.pct_sae_1 ? data.pct_sae_1 : "",
            pct_sae_2: data.pct_sae_2 ? data.pct_sae_2 : "",
            pct_sae_final: data.pct_sae_final ? data.pct_sae_final : "",
            des_cci: data.des_cci ? data.des_cci : "",
            cod_tipo_desembolso: data.cod_tipo_desembolso ? data.cod_tipo_desembolso : 0,
            des_descripcion: data.des_descripcion ? data.des_descripcion : "",
            numero_cuotas: data.numero_cuotas ? data.numero_cuotas : '',
            monto_cuota: data.monto_cuota ? data.monto_cuota : '',
            tcea: data.tcea ? data.tcea : '',
            monto_cci: data.monto_cci ? data.monto_cci : 0,
            monto_efectivo: data.monto_efectivo ? data.monto_efectivo : 0
        });

        return axios
            .post(`${URL_ODC}/Pmp/OfertaComercial`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_COMMERCIAL_OFFER_SUCCESS, data: response.data.actividad });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_COMMERCIAL_OFFER_INVALID,
                            data: null,
                            error: response.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_COMMERCIAL_OFFER_ERROR, error: newError, response: response });
                return error;
            });
    };
}
// POST
export function pendingEmbossing(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({ type: ODC_EXPRESS_PENDING_EMBOSSING_REQUEST });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_requerimiento: data.cod_requerimiento,
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            nom_actividad: data.nom_actividad,
            cod_motivo_emboce: data.cod_motivo_emboce,
            des_motivo_comentario_emboce: data.des_motivo_comentario_emboce,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip
        });
        return axios
            .post(`${URL_ODC}/Originacion/Express/RegistrarActividadPendienteEmboce`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_EXPRESS_PENDING_EMBOSSING_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_PENDING_EMBOSSING_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_PENDING_EMBOSSING_ERROR, error: newError, response: response });
                return error;
            });
    }
}
// GET
export function pendingActivateSAE(data) {
    return (dispatch) => {
        //Begin Request
        dispatch({ type: ODC_EXPRESS_PENDING_ACTIVATE_SAE_REQUEST });
        return axios
            .get(`${URL_ODC}/Originacion/SolicitudesPedienteActivarSAE`, {
                //.get(`${URL_ODC}/Originacion/SolicitudesPedienteActivarSAEPrototy`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_PENDING_ACTIVATE_SAE_SUCCESS, data: response.data.solicitudesPendienteActivacionSae });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_PENDING_ACTIVATE_SAE_INVALID,
                            data: [],
                            error: response.data.errorMessage,
                            response: true
                        });
                        return response;
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request

                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_PENDING_ACTIVATE_SAE_ERROR, error: newError, response: response });
                return error;
            });
    };
}
////////////////////////////////////////
export function pendingActivateSAEPrototype(data) {
    return (dispatch) => {
        //Begin Request
        dispatch({ type: ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_REQUEST });
        return axios
            .get(`${URL_ODC}/Originacion/SolicitudesPedienteActivarSAEPrototy`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_SUCCESS, data: response.data.solicitudesPendienteActivacionSae });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_INVALID,
                            data: [],
                            error: response.data.errorMessage,
                            response: true
                        });
                        return response;
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request

                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_PENDING_ACTIVATE_SAE_PROTOTYPE_ERROR, error: newError, response: response });
                return error;
            });
    };
}
//////////////////////////7
// GET 
export function loadClient(documentTypeId, documentNumber) {
    return (dispatch) => {
        //Begin Request
        dispatch({ type: ODC_EXPRESS_LOAD_CLIENT_REQUEST });
        return axios
            .get(`${URL_ODC}/Cliente/PrecargaCliente?cod_tipo_documento=${documentTypeId}&des_nro_documento=${documentNumber}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_LOAD_CLIENT_SUCCESS, data: response.data.precargaCliente });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_LOAD_CLIENT_INVALID,
                            data: [],
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request

                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_LOAD_CLIENT_ERROR, error: newError, response: response });
                return error;
            });
    };
}
// GET
export function SearchBusiness(rucNumber, businessName) {
    return (dispatch) => {
        dispatch({ type: ODC_EXPRESS_SEARCH_BUSINESS_REQUEST });

        return axios
            .get(`${URL_ODC}/Empresa/ListaEmpresas?ruc_emp=${rucNumber}&nom_emp=${businessName}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {

                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_SEARCH_BUSINESS_SUCCESS, data: response.data.empresas });
                    } else {
                        dispatch({
                            type: ODC_EXPRESS_SEARCH_BUSINESS_INVALID,
                            data: [],
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_SEARCH_BUSINESS_ERROR, error: newError, response: response });
                return error;
            });
    };
}
// POST
export function createBusiness(data) {
    return (dispatch) => {
        // Begin Request
        dispatch({ type: ODC_EXPRESS_CREATE_BUSINESS_REQUEST });
        const dataEncrypt = sessionStorage.getItem('data');
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        let ODCcreateBusinessData = JSON.stringify({
            empresa: {
                cod_emp: data.cod_emp,
                nom_emp: data.nom_emp,
                ruc_emp: data.ruc_emp,
                des_observaciones: data.des_observaciones,
                flg_visible: data.flg_visible,
                flg_eliminado: !data.flg_visible,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg: decryptData.username,
                des_ter_reg: device.ip,
                fec_act: null,
                des_usu_act: "",
                des_ter_act: ""
            }
        })
        return new Promise((resolve, reject) => {
            axios
                .post(`${URL_ODC}/Empresa/CrearEmpresa`, ODCcreateBusinessData, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    }
                })
                .then(response => {
                    // Launch Dispatch
                    if (response.data) {
                        if (response.data.success) {
                            dispatch({ type: ODC_EXPRESS_CREATE_BUSINESS_SUCCESS, data: response.data });
                        }
                        else {
                            dispatch({
                                type: ODC_EXPRESS_CREATE_BUSINESS_INVALID,
                                data: null,
                                error: response.data.errorMessage,
                                response: true
                            });
                        }
                    }
                    resolve(response);
                })
                .catch(error => {
                    // Error Request
                    let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                    let response = error.response ? true : false;
                    dispatch({ type: ODC_EXPRESS_CREATE_BUSINESS_ERROR, error: newError, response: response });
                    reject(error);
                });
        })
    }

}
// POST
export function deleteBusiness(data) {
    return (dispatch) => {
        // Begin Request
        dispatch({ type: ODC_EXPRESS_DELETE_BUSINESS_REQUEST });
        const dataEncrypt = sessionStorage.getItem('data');
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        let ODCdeleteClientData = JSON.stringify({
            empresa: {
                cod_emp: data.cod_emp,
                nom_emp: "",
                ruc_emp: "",
                des_observaciones: "",

                flg_visible: false,
                flg_eliminado: true,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_reg: decryptData.username,
                des_ter_reg: device.ip,
                fec_act: null,
                des_usu_act: "",
                des_ter_act: ""
            }
        })
        return axios
            .post(`${URL_ODC}/Empresa/ActualizarEmpresa`, ODCdeleteClientData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_DELETE_BUSINESS_SUCCESS, data: response.data });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_DELETE_BUSINESS_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_DELETE_BUSINESS_ERROR, error: newError, response: response });
                return error;
            });
    }

}
//POST
export function createComment(data) {
    return (dispatch) => {
        //Begin Request
        dispatch({ type: ODC_EXPRESS_CREATE_COMMENT_REQUEST });
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        let sendData = JSON.stringify({
            Comentario: {
                cod_comentario: data.cod_comentario,
                cod_solicitud: data.cod_solicitud,
                cod_solicitud_completa: data.cod_solicitud_completa,
                des_comentario: data.des_comentario,
                cod_agencia: decryptData.agency ? decryptData.agency.ide_agencia : 0,
                flg_visible: true,
                flg_eliminado: false,
                fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_perfil: decryptData.profiles.name,
                cod_perfil: decryptData.profiles.id,
                des_usu_reg: decryptData.username,
                des_ter_reg: device.ip,
                fec_act: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_usu_act: decryptData.username,
                des_ter_act: ""
            }
        });

        return axios
            .post(`${URL_ODC}/Comentario/CrearComentario`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_CREATE_COMMENT_SUCCESS, data: response.data.crearComentario });

                        loadComment(data.cod_solicitud_completa);
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_CREATE_COMMENT_INVALID,
                            data: null,
                            error: response.errorMessage,
                            response: true
                        });
                    }
                }
                return response;

            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_CREATE_COMMENT_ERROR, error: newError, response: response });
                return error;

            });
    };
}
// GET
export function loadComment(fullNumber) {
    return (dispatch) => {
        //Begin Request
        dispatch({ type: ODC_EXPRESS_LOAD_COMMENT_REQUEST });
        return axios
            .get(`${URL_ODC}/Comentario/ListaComentarios?cod_solicitud_completa=${fullNumber}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({ type: ODC_EXPRESS_LOAD_COMMENT_SUCCESS, data: response.data.comentarios });
                    }
                    else {
                        dispatch({
                            type: ODC_EXPRESS_LOAD_COMMENT_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({ type: ODC_EXPRESS_LOAD_COMMENT_ERROR, error: newError, response: response });
                return error;
            });
    };
}

export function verifySolicitudeIsBlocked(data) {
    const dataEncrypt = sessionStorage.getItem('data')
    const decryptData = decrypt(dataEncrypt)
    const sendData = {
        ConsultaClienteEnProceso: {
            cod_solicitud: data.solicitudeCode || 0,
            cod_solicitud_completa: data.completeSolicitudeCode || '',
            cod_cliente: data.clientCode || 0,
            cod_tipo_documento: data.documentTypeId || 0,
            des_nro_documento: data.documentNumber || '',
            des_usu_sesion: decryptData.username
        }
    }
    const request = {
        url: `${URL_ODC}/api/ClienteEnProceso/ConsultaClienteEnProceso`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_REQUEST,
        success: ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_SUCCESS,
        invalid: ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_INVALID,
        error: ODC_EXPRESS_VERIFY_SOLICITUDE_IS_BLOCKED_ERROR
    }
    return callApi(request, actions)
}

export function blockSolicitude(data) {
    const dataEncrypt = sessionStorage.getItem('data')
    const decryptData = decrypt(dataEncrypt)
    const device = getDeviceData()
    const sendData = {
        RegistrarClienteEnProceso: {
            cod_solicitud: data.solicitudeCode || 0,
            cod_solicitud_completa: data.completeSolicitudeCode || '',
            cod_cliente: data.clientCode || 0,
            cod_tipo_documento: data.documentTypeId || 0,
            des_nro_documento: data.documentNumber || '',
            linea_ingresada: data.lineAvailable || 0,
            linea_global: data.globalLineAvailable || 0,
            des_usu_sesion: decryptData.username,
            flg_sol_enproceso: true,
            fec_reg_sol_enproceso: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            flg_visible: true,
            flg_eliminado: false,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            fec_act: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_act: decryptData.username,
            des_ter_act: device.ip
        }
    }
    const request = {
        url: `${URL_ODC}/api/ClienteEnProceso/RegistrarClienteEnProceso`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXPRESS_BLOCK_SOLICITUDE_REQUEST,
        success: ODC_EXPRESS_BLOCK_SOLICITUDE_SUCCESS,
        invalid: ODC_EXPRESS_BLOCK_SOLICITUDE_INVALID,
        error: ODC_EXPRESS_BLOCK_SOLICITUDE_ERROR
    }
    return callApi(request, actions)
}

export function unlockSolicitude(data) {
    const dataEncrypt = sessionStorage.getItem('data')
    const decryptData = decrypt(dataEncrypt)
    const device = getDeviceData()
    const sendData = {
        ActualizarClienteEnProceso: {
            cod_cliente_enproceso: 0,
            cod_solicitud: data.solicitudeCode || 0,
            cod_solicitud_completa: data.completeSolicitudeCode || '',
            cod_cliente: data.clientCode || 0,
            cod_tipo_documento: data.documentTypeId || 0,
            des_nro_documento: data.documentNumber || '',
            flg_sol_liberada: true,
            fec_reg_sol_liberada: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            flg_visible: true,
            flg_eliminado: false,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            fec_act: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_act: decryptData.username,
            des_ter_act: device.ip
        }
    }
    const request = {
        url: `${URL_ODC}/api/ClienteEnProceso/ActualizarClienteEnProceso`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXPRESS_UNLOCK_SOLICITUDE_REQUEST,
        success: ODC_EXPRESS_UNLOCK_SOLICITUDE_SUCCESS,
        invalid: ODC_EXPRESS_UNLOCK_SOLICITUDE_INVALID,
        error: ODC_EXPRESS_UNLOCK_SOLICITUDE_ERROR
    }
    return callApi(request, actions)
}