export const URL_SECURITY = process.env.REACT_APP_URL_SRA;
export const URL_ODC = process.env.REACT_APP_URL_ODC;
export const URL_ODC_REGULAR = process.env.REACT_APP_URL_ODC_REGULAR;
export const URL_ODC_EXISTING_CLIENT = process.env.REACT_APP_URL_ODC_EXISTING_CLIENT;
export const URL_ZYTRUST = process.env.REACT_APP_URL_ZYTRUST;
export const URL_VALIDATE_ZYTRUST = process.env.REACT_APP_URL_VALIDATE_ZYTRUST;

export const URL_BASE_VALUE_LIST = process.env.REACT_APP_URL_BASE_VALUE_LIST;
export const URL_BASE =  process.env.REACT_APP_URL_BASE;

// Server Error Message 
export const DOCUMENT_NOT_FOUND_MESSAGE = process.env.REACT_APP_DOCUMENT_NOT_FOUND_MESSAGE; //'No existe documento o no se ha generado';
export const SERVER_ERROR_MESSAGE = process.env.REACT_APP_SERVER_ERROR_MESSAGE;  // Error de connectividad al servicio. Something went wrong, please try again please try again;

export const REACT_APP_VERSION_ODC = process.env.REACT_APP_VERSION_ODC;  // ODC - Version
