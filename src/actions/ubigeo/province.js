import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';
// Actions 
export const PROVINCE_REQUEST = 'PROVINCE_REQUEST'; 
export const PROVINCE_SUCCESS = 'PROVINCE_SUCCESS'; 
export const PROVINCE_INVALID = 'PROVINCE_INVALID'; 
export const PROVINCE_ERROR = 'PROVINCE_ERROR'; 

export const HOME_ADDRESS_PROVINCE_REQUEST = 'HOME_ADDRESS_PROVINCE_REQUEST';
export const HOME_ADDRESS_PROVINCE_SUCCESS = 'HOME_ADDRESS_PROVINCE_SUCCESS';
export const HOME_ADDRESS_PROVINCE_INVALID = 'HOME_ADDRESS_PROVINCE_INVALID';
export const HOME_ADDRESS_PROVINCE_ERROR = 'HOME_ADDRESS_PROVINCE_ERROR';

export const WORK_ADDRESS_PROVINCE_REQUEST = 'WORK_ADDRESS_PROVINCE_REQUEST';
export const WORK_ADDRESS_PROVINCE_SUCCESS = 'WORK_ADDRESS_PROVINCE_SUCCESS';
export const WORK_ADDRESS_PROVINCE_INVALID = 'WORK_ADDRESS_PROVINCE_INVALID';
export const WORK_ADDRESS_PROVINCE_ERROR = 'WORK_ADDRESS_PROVINCE_ERROR';

export function getProvince(data) {
    return (dispatch) => {
        //Begin Request
        dispatch({type: PROVINCE_REQUEST});
        let department = JSON.stringify({
            cod_tipo:data.type,
            cod_det_ubi:data.value
        })
        return axios
            .post(`${URL_ODC}/Ubigeo`, department, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: PROVINCE_SUCCESS, data: response.data.ubigeos });
                    }
                    else{
                        dispatch({ 
                            type: PROVINCE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: PROVINCE_ERROR, error:newError, response: response});
                return error;
            });
    };
}
export function getHomeAddressProvince(data){
    return (dispatch) => {
        //Begin Request
        dispatch({type: HOME_ADDRESS_PROVINCE_REQUEST});
        let department = JSON.stringify({
            cod_tipo:data.type,
            cod_det_ubi:data.value
        })
        return axios
            .post(`${URL_ODC}/Ubigeo`, department, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: HOME_ADDRESS_PROVINCE_SUCCESS, data: response.data.ubigeos });
                    }
                    else{
                        dispatch({ 
                            type: HOME_ADDRESS_PROVINCE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                        return response;
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: HOME_ADDRESS_PROVINCE_ERROR, error:newError, response: response});
                return error;
            });
    };
}
export function getWorkAddressProvince(data){
    return (dispatch) => {
        //Begin Request
        dispatch({type: WORK_ADDRESS_PROVINCE_REQUEST});
        let department = JSON.stringify({
            cod_tipo:data.type,
            cod_det_ubi:data.value
        })
        return axios
            .post(`${URL_ODC}/Ubigeo`, department, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: WORK_ADDRESS_PROVINCE_SUCCESS, data: response.data.ubigeos });
                    }
                    else{
                        dispatch({ 
                            type: WORK_ADDRESS_PROVINCE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                        return response;
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: WORK_ADDRESS_PROVINCE_ERROR, error:newError, response: response});
                return error;
            });
    };
}