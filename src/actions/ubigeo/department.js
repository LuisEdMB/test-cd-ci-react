import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';

// Actions 
export const DEPARTMENT_REQUEST = 'DEPARTMENT_REQUEST'; 
export const DEPARTMENT_SUCCESS = 'DEPARTMENT_SUCCESS'; 
export const DEPARTMENT_INVALID = 'DEPARTMENT_INVALID'; 
export const DEPARTMENT_ERROR = 'DEPARTMENT_ERROR'; 

export function getDepartment() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: DEPARTMENT_REQUEST});
        let data = JSON.stringify({
            cod_tipo: "DE",
            cod_ubigeo: 0
        })
        return axios
            .post(`${URL_ODC}/Ubigeo`, data, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: DEPARTMENT_SUCCESS, data: response.data.ubigeos });
                    }
                    else{
                        dispatch({
                            type:DEPARTMENT_INVALID, 
                            data:null, 
                            error:response.data.errorMessage, 
                            response:true,
                        });
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: DEPARTMENT_ERROR, error:newError, response: response});
                return error;
            });
    };
}
