import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';

// Actions 
export const DISTRICT_REQUEST = 'DISTRICT_REQUEST'; 
export const DISTRICT_SUCCESS = 'DISTRICT_SUCCESS'; 
export const DISTRICT_INVALID = 'DISTRICT_INVALID'; 
export const DISTRICT_ERROR = 'DISTRICT_ERROR'; 

export const HOME_ADDRESS_DISTRICT_REQUEST = 'HOME_ADDRESS_DISTRICT_REQUEST';
export const HOME_ADDRESS_DISTRICT_SUCCESS = 'HOME_ADDRESS_DISTRICT_SUCCESS';
export const HOME_ADDRESS_DISTRICT_INVALID = 'HOME_ADDRESS_DISTRICT_INVALID';
export const HOME_ADDRESS_DISTRICT_ERROR = 'HOME_ADDRESS_DISTRICT_ERROR';

export const WORK_ADDRESS_DISTRICT_REQUEST = 'WORK_ADDRESS_DISTRICT_REQUEST';
export const WORK_ADDRESS_DISTRICT_SUCCESS = 'WORK_ADDRESS_DISTRICT_SUCCESS';
export const WORK_ADDRESS_DISTRICT_INVALID = 'WORK_ADDRESS_DISTRICT_INVALID';
export const WORK_ADDRESS_DISTRICT_ERROR = 'WORK_ADDRESS_DISTRICT_ERROR';

export function getDistrict(data) {
    return (dispatch) => {
        //Begin Request
        dispatch({type: DISTRICT_REQUEST});
        let province = JSON.stringify({
            cod_tipo:data.type,
            cod_det_ubi:data.value
        })
        return axios
            .post(`${URL_ODC}/Ubigeo`, province, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: DISTRICT_SUCCESS, data: response.data.ubigeos });
                    }
                    else{
                        dispatch({
                            type:DISTRICT_INVALID, 
                            data:null, 
                            error:response.data.errorMessage, 
                            response:true,
                        });
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: DISTRICT_ERROR, error:newError, response: response});
                return error;
            });
    };
}
export function getHomeAddressDistrict(data){
    return (dispatch) => {
        //Begin Request
        dispatch({type: HOME_ADDRESS_DISTRICT_REQUEST});
        let province = JSON.stringify({
            cod_tipo:data.type,
            cod_det_ubi:data.value
        })
        return axios
            .post(`${URL_ODC}/Ubigeo`, province, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: HOME_ADDRESS_DISTRICT_SUCCESS, data: response.data.ubigeos });
                    }
                    else{
                        dispatch({
                            type:HOME_ADDRESS_DISTRICT_INVALID, 
                            data:null, 
                            error:response.data.errorMessage, 
                            response:true,
                        });
                        return response;
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: HOME_ADDRESS_DISTRICT_ERROR, error:newError, response: response});
                return error;
            });
    };
}
export function getWorkAddressDistrict(data){
    return (dispatch) => {
        //Begin Request
        dispatch({type: WORK_ADDRESS_DISTRICT_REQUEST});
        let province = JSON.stringify({
            cod_tipo:data.type,
            cod_det_ubi:data.value
        })
        return axios
            .post(`${URL_ODC}/Ubigeo`, province, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: WORK_ADDRESS_DISTRICT_SUCCESS, data: response.data.ubigeos });
                    }
                    else{
                        dispatch({
                            type:WORK_ADDRESS_DISTRICT_INVALID, 
                            data:null, 
                            error:response.data.errorMessage, 
                            response:true,
                        });
                        return response;
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: WORK_ADDRESS_DISTRICT_ERROR, error:newError, response: response});
                return error;
            });
    };
}
