// URL APIs
import { URL_ODC_EXISTING_CLIENT } from '../config'

// Generic API
import { callApi } from '../generic/api'

// Utils
import * as moment from 'moment';
import * as Utils from '../../utils/Utils'

// Consult Offer
export const ODC_EXISTING_CLIENT_CONSULT_OFFER_REQUEST = 'ODC_EXISTING_CLIENT_CONSULT_OFFER_REQUEST'
export const ODC_EXISTING_CLIENT_CONSULT_OFFER_SUCCESS = 'ODC_EXISTING_CLIENT_CONSULT_OFFER_SUCCESS'
export const ODC_EXISTING_CLIENT_CONSULT_OFFER_INVALID = 'ODC_EXISTING_CLIENT_CONSULT_OFFER_INVALID'
export const ODC_EXISTING_CLIENT_CONSULT_OFFER_ERROR = 'ODC_EXISTING_CLIENT_CONSULT_OFFER_ERROR'

// Get Client Information
export const ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_REQUEST = 'ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_REQUEST'
export const ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_SUCCESS = 'ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_SUCCESS'
export const ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_INVALID = 'ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_INVALID'
export const ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_ERROR = 'ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_ERROR'

// Update Client Information
export const ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_REQUEST = 'ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_REQUEST'
export const ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_SUCCESS = 'ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_SUCCESS'
export const ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_INVALID = 'ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_INVALID'
export const ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_ERROR = 'ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_ERROR'

// Register Commercial Offer
export const ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_REQUEST = 'ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_REQUEST'
export const ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_SUCCESS = 'ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_SUCCESS'
export const ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_INVALID = 'ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_INVALID'
export const ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_ERROR = 'ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_ERROR'

// Register Account & Card
export const ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_REQUEST = 'ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_REQUEST'
export const ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_SUCCESS = 'ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_SUCCESS'
export const ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_INVALID = 'ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_INVALID'
export const ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_ERROR = 'ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_ERROR'

// Continue Process
export const ODC_EXISTING_CLIENT_CONTINUE_PROCESS_REQUEST = 'ODC_EXISTING_CLIENT_CONTINUE_PROCESS_REQUEST'
export const ODC_EXISTING_CLIENT_CONTINUE_PROCESS_SUCCESS = 'ODC_EXISTING_CLIENT_CONTINUE_PROCESS_SUCCESS'
export const ODC_EXISTING_CLIENT_CONTINUE_PROCESS_INVALID = 'ODC_EXISTING_CLIENT_CONTINUE_PROCESS_INVALID'
export const ODC_EXISTING_CLIENT_CONTINUE_PROCESS_ERROR = 'ODC_EXISTING_CLIENT_CONTINUE_PROCESS_ERROR'

// Cancel Origination
export const ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_REQUEST = 'ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_REQUEST'
export const ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_SUCCESS = 'ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_SUCCESS'
export const ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_INVALID = 'ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_INVALID'
export const ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_ERROR = 'ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_ERROR'

// Finish Call Origination
export const ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_REQUEST = 'ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_REQUEST'
export const ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_SUCCESS = 'ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_SUCCESS'
export const ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_INVALID = 'ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_INVALID'
export const ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_ERROR = 'ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_ERROR'

// Register Activity
export const ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_REQUEST = 'ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_REQUEST'
export const ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_SUCCESS = 'ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_SUCCESS'
export const ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_INVALID = 'ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_INVALID'
export const ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_ERROR = 'ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_ERROR'

// Register Summary Activity
export const ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_REQUEST = 'ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_REQUEST'
export const ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_SUCCESS = 'ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_SUCCESS'
export const ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_INVALID = 'ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_INVALID'
export const ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_ERROR = 'ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_ERROR'

// Register Reject Activity
export const ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_REQUEST = 'ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_REQUEST'
export const ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_SUCCESS = 'ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_SUCCESS'
export const ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_INVALID = 'ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_INVALID' 
export const ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_ERROR = 'ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_ERROR'

// Cancel Activity
export const ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_REQUEST = 'ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_REQUEST'
export const ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_SUCCESS = 'ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_SUCCESS'
export const ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_INVALID = 'ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_INVALID'
export const ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_ERROR = 'ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_ERROR'

export function consultOffer(data) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    const sendData = {
        tipo_doc: data.documentType,
        nro_doc: data.documentNumber,
        fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip,
        tipo_doc_letra: data.documentTypeLetter,
        terminalName: 'WEB',
        cod_flujo_fase_estado_actual: data.currentlyPhaseCode,
        cod_flujo_fase_estado_anterior: data.previousPhaseCode,
        fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        cod_solicitud: 0,
        cod_solicitud_completa: '',
        cod_cliente: 0,
        cod_tipo_solicitud: data.typeSolicitudeCode,
        cod_tipo_relacion: data.relationTypeId,
        cod_tipo_solicitud_requerimiento: 0
    }
    const request = {
        url: `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Cliente/ConsultaOferta`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_CONSULT_OFFER_REQUEST,
        success: ODC_EXISTING_CLIENT_CONSULT_OFFER_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_CONSULT_OFFER_INVALID,
        error: ODC_EXISTING_CLIENT_CONSULT_OFFER_ERROR
    }
    return callApi(request, actions)
}

export function getClientInformation(data) {
    const request = {
        url: `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Cliente/PrecargaCliente?` +
            `cod_tipo_documento=${data.documentType}&des_nro_documento=${data.documentNumber}`,
        method: 'GET',
        data: null
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_REQUEST,
        success: ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_INVALID,
        error: ODC_EXISTING_CLIENT_GET_CLIENT_INFORMATION_ERROR
    }
    return callApi(request, actions)
}

export function updateClientInformation(data) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    const sendData = {
        tipo_doc: data.documentType,
        nro_doc: data.documentNumber,
        fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip,
        tipo_doc_letra: data.documentTypeLetter,
        terminalName: 'WEB',
        tcType: 'VISA',
        maritalStatus: data.maritalStatus,
        dateJoinedCompany: data.jobInformation.laborIncomeDate,
        birthDate: data.birthday,
        nationality: data.nationality,
        studiesLevel: data.academicDegree,
        occupation: data.jobInformation.jobTitle,
        employerRUC: data.jobInformation.ruc,
        gender: data.gender,
        laborSituation: data.jobInformation.jobSituation,
        cellPhone: data.cellphone,
        homePhone: data.telephone,
        workPhone: data.jobInformation.telephone,
        housingType: data.personalAddress.housingType,
        cod_cliente: data.clientCode,
        cod_tipo_documento: data.documentTypeId,
        des_nro_documento: data.documentNumber,
        des_primer_nom: data.firstName,
        des_segundo_nom: data.secondName,
        des_ape_paterno: data.firstLastName,
        des_ape_materno: data.secondLastName,
        fec_nacimiento: data.birthday,
        cod_genero: data.genderId,
        cod_estado_civil: data.maritalStatusId,
        cod_nacionalidad: data.nationalityId,
        cod_pais_residencia: data.nationalityId,
        cod_gradoacademico: data.academicDegreeId,
        des_correo: data.email,
        des_telef_celular: data.cellphone,
        des_telef_fijo: data.telephone,
        cod_tipo_cliente: data.typeClientId,
        cod_situacion_laboral: data.jobInformation.jobSituationId,
        cod_cargo_profesion: data.jobInformation.jobTitleId,
        cod_actividad_economica: data.jobInformation.economicActivityId,
        fec_ingreso_laboral: data.jobInformation.laborIncomeDate,
        des_ruc: data.jobInformation.ruc,
        des_razon_social: data.jobInformation.businessName,
        num_ingreso_bruto: data.jobInformation.grossIncome,
        des_telefono_laboral: data.jobInformation.telephone,
        des_anexo_laboral: data.jobInformation.telephoneExtension,
        flg_autoriza_datos_per: data.benefits.authorizePersonalData,
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_fecha_pago: data.benefits.paymentDateId,
        cod_envio_correspondencia: data.benefits.sendCorrespondenceId,
        cod_estado_cuenta: data.benefits.accountStatusId,
        flg_retiro_efectivo: data.benefits.effectiveDisposition,
        flg_consumo_internet: false,
        flg_consumo_extranjero: false,
        flg_retirar_efectivo: false,
        flg_sobregirar_credito: false,
        flg_notificacion_email: data.questions.mailAlerts === '1',
        num_monto_minimo: data.questions.minimumForAlerts,
        cod_resp_promotor: data.benefits.promoterCode,
        cod_resp_registro: 0,
        cod_resp_actualiza: 0,
        cod_agencia: decryptData.agency?.ide_agencia || 0,
        cod_flujo_fase_estado_actual: data.currentlyPhaseCode,
        cod_flujo_fase_estado_anterior: data.previousPhaseCode,
        fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        cod_valor_color: 0,
        disp_efec_porcentaje: 0,
        disp_efec_monto: 0,
        cod_valor_marca: 0,
        linea_credito_oferta: 0,
        linea_credito_final: 0,
        list_direccion: [
            {
                cod_direccion: data.personalAddress.addressCode,
                cod_cliente: data.clientCode,
                cod_tipo_direccion: 260001,
                des_tipo_direccion: 'Dirección personal',
                cod_ubigeo: data.personalAddress.districtId,
                cod_via: data.personalAddress.viaId,
                des_nombre_via: data.personalAddress.viaDescription,
                des_nro: data.personalAddress.number,
                des_departamento: data.personalAddress.building,
                des_interior: data.personalAddress.insideBuilding,
                des_manzana: data.personalAddress.mz,
                des_lote: data.personalAddress.lot,
                cod_zona: data.personalAddress.zoneId,
                des_nombre_zona: data.personalAddress.zoneDescription,
                cod_tipo_residencia: 0,
                cod_tipo_vivienda: data.personalAddress.housingTypeId,
                des_referencia_domicilio: data.personalAddress.reference
            },
            {
                cod_direccion: data.jobAddress.addressCode,
                cod_cliente: data.clientCode,
                cod_tipo_direccion: 260002,
                des_tipo_direccion: 'Dirección laboral',
                cod_ubigeo: data.jobAddress.districtId,
                cod_via: data.jobAddress.viaId,
                des_nombre_via: data.jobAddress.viaDescription,
                des_nro: data.jobAddress.number,
                des_departamento: data.jobAddress.building,
                des_interior: data.jobAddress.insideBuilding,
                des_manzana: data.jobAddress.mz,
                des_lote: data.jobAddress.lot,
                cod_zona: data.jobAddress.zoneId,
                des_nombre_zona: data.jobAddress.zoneDescription,
                cod_tipo_residencia: 0,
                cod_tipo_vivienda: 0,
                des_referencia_domicilio: data.jobAddress.reference
            }
        ],
        cod_producto: 0,
        cod_motivo_emboce: 0,
        des_motivo_comentario_emboce: '',
        cod_laboral: 0,
        flg_biometria: false,
        cod_tipo_relacion: data.relationTypeId,
        cod_tipo_solicitud: data.typeSolicitudeCode,
        cod_tipo_solicitud_requerimiento: 0
    }
    const request = {
        url: `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Oferta/ActualizarClienteDireccion`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_REQUEST,
        success: ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_INVALID,
        error: ODC_EXISTING_CLIENT_UPDATE_CLIENT_INFORMATION_ERROR
    }
    return callApi(request, actions)
}

export function registerCommercialOffer(data) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    const sendData = {
        tipo_doc: data.documentType,
        nro_doc: data.documentNumber,
        fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip,
        tipo_doc_letra: data.documentTypeLetter,
        terminalName: 'WEB',
        nom_actividad: data.activityName,
        des_usu_responsable: decryptData.username,
        cod_flujo_fase_estado_actual: data.currentlyPhaseCode,
        cod_flujo_fase_estado_anterior: data.previousPhaseCode,
        fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        cod_valor_color: 250005,
        disp_efec_porcentaje: 0,
        disp_efec_monto: 0,
        cod_producto: 10009,
        cod_valor_marca: 200003,
        linea_credito_oferta: data.lineAvailable,
        linea_credito_final: data.commercialOffer.lineAvailable.value,
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_cliente: data.clientCode,
        cod_agencia: decryptData.agency?.ide_agencia || 0,
        fec_act_base_ec: moment().format('YYYYMMDD'),
        flg_bq2: '',
        linea_ec: data.lineAvailable,
        linea_ec_final: data.commercialOffer.lineAvailable.value,
        monto_max: data.lineAvailable,
        monto_max_final: data.lineAvailable,
        pct_ec_1: data.pct,
        pct_ec_2: data.pct,
        pct_ec_final: data.pct,
        des_cci: data.commercialOffer.cci,
        cod_tipo_desembolso: data.commercialOffer.disbursementTypeId,
        des_descripcion: '',
        numero_cuotas: data.commercialOffer.numberFees,
        monto_cuota: data.commercialOffer.feeAmount,
        tcea: data.commercialOffer.tcea,
        monto_cci: data.commercialOffer.lineAvailable.value,
        monto_efectivo: 0
    }
    const request = {
        url: `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Oferta/OfertaComercial`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_REQUEST,
        success: ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_INVALID,
        error: ODC_EXISTING_CLIENT_REGISTER_COMMERCIAL_OFFER_ERROR
    }
    return callApi(request, actions)
}

export function registerAccountCard(data) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_cliente: data.clientCode,
        num_cliente_pmp: data.pmpClientNumber,
        num_relacion_pmp: data.pmpRelationNumber,
        num_cuenta_pmp: data.pmpAccountNumber,
        num_tarjeta_pmp: data.pmpCardNumber,
        cod_tipo_relacion: data.relationTypeId,
        cod_tipo_relacion_pmp: 'P',
        cod_bloqueo_cuenta: '370025',
        cod_bloqueo_cuenta_pmp: 'A',
        cod_bloqueo_tarjeta: '370025',
        cod_bloqueo_tarjeta_pmp: 'A',
        cod_flujo_fase_estado_actual: data.currentlyPhaseCode,
        cod_flujo_fase_estado_anterior: data.previousPhaseCode,
        nom_actividad: data.activityName,
        flg_visible: 1,
        flg_eliminado: 0,
        fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip,
    }
    const request = {
        url: `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Originacion/RegistrarCuentaTarjeta`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_REQUEST,
        success: ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_INVALID,
        error: ODC_EXISTING_CLIENT_REGISTER_ACCOUNT_CARD_ERROR
    }
    return callApi(request, actions)
}

export function continueProcess(completeSolicitudeCode) {
    const request = {
        url: `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Originacion/SolicitudRetomar?cod_solicitud_completa=${completeSolicitudeCode}`,
        method: 'GET',
        data: null
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_CONTINUE_PROCESS_REQUEST,
        success: ODC_EXISTING_CLIENT_CONTINUE_PROCESS_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_CONTINUE_PROCESS_INVALID,
        error: ODC_EXISTING_CLIENT_CONTINUE_PROCESS_ERROR
    }
    return callApi(request, actions)
}

export function cancelOrigination(data) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_flujo_fase_estado_actual: data.currentlyPhaseCode,
        cod_flujo_fase_estado_anterior: data.previousPhaseCode,
        nom_actividad: data.activityName,
        des_observacion: data.observation,
        cod_agencia: decryptData.agency?.ide_agencia || 0,
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip
    }
    const request = {
        url: `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Originacion/CancelarEfectivoCencosud`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_REQUEST,
        success: ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_INVALID,
        error: ODC_EXISTING_CLIENT_CANCEL_ORIGINATION_ERROR
    }
    return callApi(request, actions)
}

export function finishCallOrigination(data) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_flujo_fase_estado_actual: data.currentlyPhaseCode,
        cod_flujo_fase_estado_anterior: data.previousPhaseCode,
        nom_actividad: data.activityName,
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip
    }
    const request = {
        url: `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Originacion/FinalizarOriginacionCall`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_REQUEST,
        success: ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_INVALID,
        error: ODC_EXISTING_CLIENT_FINISH_CALL_ORIGINATION_ERROR
    }
    return callApi(request, actions)
}

export function registerActivity(data, simple = false) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_flujo_fase_estado_actual: data.currentlyPhaseCode,
        cod_flujo_fase_estado_anterior: data.previousPhaseCode,
        nom_actividad: data.activityName,
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip
    }
    const request = {
        url: simple 
            ? `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Originacion/RegistrarActividadGenericoSimple`
            : `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Originacion/RegistrarActividadGenerico`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_REQUEST,
        success: ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_INVALID,
        error: ODC_EXISTING_CLIENT_REGISTER_ACTIVITY_ERROR
    }
    return callApi(request, actions)
}

export function registerSummaryActivity(data) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_flujo_fase_estado_actual: data.currentlyPhaseCode,
        cod_flujo_fase_estado_anterior: data.previousPhaseCode,
        nom_actividad: data.activityName,
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip,
        cod_valor_color: 250005,
        disp_efec_porcentaje: 0,
        cod_valor_marca: 200003,
        linea_credito_oferta: data.lineAvailable,
        linea_credito_final: data.commercialOffer.lineAvailable.value
    }
    const request = {
        url: `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Originacion/RegistrarActividadResumen`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_REQUEST,
        success: ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_INVALID,
        error: ODC_EXISTING_CLIENT_REGISTER_SUMMARY_ACTIVITY_ERROR
    }
    return callApi(request, actions)
}

export function registerRejectedActivity(data) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_flujo_fase_estado_actual: data.currentlyPhaseCode,
        cod_flujo_fase_estado_anterior: data.previousPhaseCode,
        nom_actividad: data.activityName,
        des_observacion: data.observation,
        cod_agencia: decryptData.agency?.ide_agencia || 0,
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip
    }
    const request = {
        url: `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Originacion/RegistrarActividadRechazar`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_REQUEST,
        success: ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_INVALID,
        error: ODC_EXISTING_CLIENT_REGISTER_REJECTED_ACTIVITY_ERROR
    }
    return callApi(request, actions)
}

export function cancelActivity(data) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_flujo_fase_estado_actual: data.currentlyPhaseCode,
        cod_flujo_fase_estado_anterior: data.previousPhaseCode,
        nom_actividad: data.activityName,
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip
    }
    const request = {
        url: `${URL_ODC_EXISTING_CLIENT}/api/ClientesExistentes/Originacion/RegistrarActividadGenericoSimple`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_REQUEST,
        success: ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_SUCCESS,
        invalid: ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_INVALID,
        error: ODC_EXISTING_CLIENT_CANCEL_ACTIVITY_ERROR
    }
    return callApi(request, actions)
}