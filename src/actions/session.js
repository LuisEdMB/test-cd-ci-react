import axios from 'axios';
import { encrypt, decrypt, getDeviceData, getIP, getUrlWebServer } from '../utils/Utils';

// URL - SERVER
import { URL_SECURITY, SERVER_ERROR_MESSAGE, REACT_APP_VERSION_ODC } from './config';
// Actions - Login
export const AUTH_USER_LOGIN_REQUEST = 'AUTH_USER_LOGIN_REQUEST'; 
export const AUTH_USER_LOGIN_SUCCESS = 'AUTH_USER_LOGIN_SUCCESS'; 
export const AUTH_USER_LOGIN_INVALID = 'AUTH_USER_LOGIN_INVALID'; 
export const AUTH_USER_LOGIN_ERROR = 'AUTH_USER_LOGIN_ERROR';

export const AUTH_USER_LOGOUT_REQUEST = 'AUTH_USER_LOGOUT_REQUEST'; 
export const AUTH_USER_LOGOUT_SUCCESS = 'AUTH_USER_LOGOUT_SUCCESS'; 
export const AUTH_USER_LOGOUT_INVALID = 'AUTH_USER_LOGOUT_INVALID';
export const AUTH_USER_LOGOUT_ERROR = 'AUTH_USER_LOGOUT_ERROR';

export function login (data) {
    return (dispatch) => {
        // Begin Request
        dispatch({type: AUTH_USER_LOGIN_REQUEST});
        let device = getDeviceData();
        // Launch Dispatch - Server
        let user = JSON.stringify({
            des_usuario: data.des_usuario, 
            des_clave: data.des_clave, 
            des_ter_reg: device.ip, 
            flg_active_directory:data.flg_active_directory
        });
        console.log("ODC version: ", REACT_APP_VERSION_ODC)
        return axios
            .post(`${URL_SECURITY}/Login/Autenticarse`, user, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                // Set data
                if(response.data.success){
                    let activeDirectory = response.data.usuario_ActiveDirectory;
                    let permissions = response.data.perfil.tb_permisoList.map((permission, index) => {
                        return {
                            status: permission.cod_estado,
                            action: permission.des_accion, 
                            option: {
                                id: permission.tb_opcion.cod_opcion,
                                name: permission.tb_opcion.des_opcion,
                                fatherOptionId: permission.tb_opcion.cod_opcion_padre,
                                levelId: permission.tb_opcion.cod_nivel,
                                image:permission.tb_opcion.des_ruta_imagen,
                                order: permission.tb_opcion.cod_orden, 
                                url: permission.tb_opcion.des_url_opcion, 
                                icon: permission.tb_opcion.des_icono,
                                visible: permission.tb_opcion.flg_visible,
                                active: permission.tb_opcion.flg_eliminado,
                                full_url: permission.tb_opcion.des_url_opcion_completa? permission.tb_opcion.des_url_opcion_completa: "/express/nuevo",
                                system:{
                                    id: permission.tb_opcion.tb_sistema.cod_sistema, 
                                    name: permission.tb_opcion.tb_sistema.des_nom_sistema, 
                                    system: permission.tb_opcion.tb_sistema.des_sistema, 
                                    fatherSystemId: permission.tb_opcion.tb_sistema.cod_sistema_padre, 
                                    active: permission.tb_opcion.tb_sistema.flg_eliminado,
                                    visible: permission.tb_opcion.tb_sistema.flg_visible
                                }
                            }
                        }
                    })
                    
                    // Get Data User - Profile - Permission - Option - System
                    let profiles = {
                        id: response.data.perfil.cod_perfil,
                        membership: response.data.perfil.des_membresia,
                        name: response.data.perfil.des_nom_perfil,
                        description: response.data.perfil.des_perfil,
                        permissions: permissions,
                    };
                    let user = {
                        agency: response.data.membresias_Sede? response.data.membresias_Sede: null,
                        name: response.data.usuario.des_nombres,
                        username: response.data.usuario.des_usuario, 
                        email: response.data.usuario.des_correo, 
                        profiles: profiles,
                        sessionCode: response.data.cod_aud_login,
                        activeDirectory:{
                            ...activeDirectory
                        }
                    };

                    let dataEncrypt = encrypt(JSON.stringify(user));
                    // Set data in Session Storage
                    sessionStorage.setItem("token", "01234567890");
                    sessionStorage.setItem("data", dataEncrypt);
                    const device = sessionStorage.getItem('_device');

                    if(!device){
                        // Set Ip
                        getIP(ip => {
                            sessionStorage.setItem("_device", JSON.stringify({
                                ip:ip
                            }));
                        });
                    }

                    // Launch Dispatch - AUTH USER OK 
                    dispatch({ type: AUTH_USER_LOGIN_SUCCESS, data: user }); 
                    return response;
                }
                else{
                    dispatch({ 
                        type: AUTH_USER_LOGIN_INVALID, 
                        data: null, 
                        error: response.data.errorMessage, 
                        response: true });
                    return response;
                }
            })
            .catch(error => {
               // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: AUTH_USER_LOGIN_ERROR, error:newError, response:response});
                return error;
            });
    }
}

export function logout (){ 
    const device = getDeviceData();
    return (dispatch) => {
        //Begin Request
        dispatch({type: AUTH_USER_LOGOUT_REQUEST});
        if(!sessionStorage.getItem("data")){
            dispatch({type: AUTH_USER_LOGOUT_SUCCESS});
        }
        else{
            let dataODCEncrypt = sessionStorage.getItem("data");
            let dataODC = decrypt(dataODCEncrypt, "Cencosud");

            let data = {
                cod_aud_login: dataODC.sessionCode,
                des_usuario:  dataODC.username,
                des_ter_act: device.ip
            };
            return axios
                .post(`${URL_SECURITY}/Login/CerrarSesion`, data, {
                    headers: {
                        'Accept':'application/json',
                        'Content-Type':'application/json'
                    }
                })  
                .then(response => {
                    if(!response.data.success){
                        dispatch({type: AUTH_USER_LOGOUT_INVALID, error: response.data.errorMessage}); 
                        return response;
                    }
                    sessionStorage.clear();
                    dispatch({type: AUTH_USER_LOGOUT_SUCCESS}); 
                    return response;
                })
                .catch(error => {
                    //Error Request
                    let newError = error.response? error.response.data.errorMessage? error.response.data.errorMessage: error.response.data.error: SERVER_ERROR_MESSAGE; 
                    let response = error.response? true : false;
                    dispatch({type: AUTH_USER_LOGOUT_ERROR, error:newError, response:response});
                    return error;
                });
        }
    }
}

export function setIpAddress() {
    const urlWebServer = getUrlWebServer()
    axios.get(`${urlWebServer}/iniciar-sesion`).then(response => {
        const ipAddress = response.headers['client-ipaddress'] || '127.0.0.1'
        sessionStorage.removeItem('_device')
        sessionStorage.setItem('_device', JSON.stringify({
            ip: ipAddress
        }))
    })
}