import axios from 'axios';
// URL - SERVER
import { URL_ODC, DOCUMENT_NOT_FOUND_MESSAGE, SERVER_ERROR_MESSAGE } from '../config';
// Generic API
import { callApi } from '../generic/api'
import {decrypt, getDeviceData} from "../../utils/Utils";
// Actions 
export const ODC_DOWNLOAD_DOCUMENT_REQUEST = 'ODC_DOWNLOAD_DOCUMENT_REQUEST';
export const ODC_DOWNLOAD_DOCUMENT_SUCCESS = 'ODC_DOWNLOAD_DOCUMENT_SUCCESS';
export const ODC_DOWNLOAD_DOCUMENT_INVALID = 'ODC_DOWNLOAD_DOCUMENT_INVALID';
export const ODC_DOWNLOAD_DOCUMENT_ERROR = 'ODC_DOWNLOAD_DOCUMENT_ERROR';

export const ODC_EXPRESS_DOCUMENTS_REQUEST = 'ODC_EXPRESS_DOCUMENTS_REQUEST';
export const ODC_EXPRESS_DOCUMENTS_SUCCESS = 'ODC_EXPRESS_DOCUMENTS_SUCCESS';
export const ODC_EXPRESS_DOCUMENTS_INVALID = 'ODC_EXPRESS_DOCUMENTS_INVALID';
export const ODC_EXPRESS_DOCUMENTS_ERROR = 'ODC_EXPRESS_DOCUMENTS_ERROR';

export const ODC_GET_DOCUMENTS_REQUEST = 'ODC_GET_DOCUMENTS_REQUEST';
export const ODC_GET_DOCUMENTS_SUCCESS = 'ODC_GET_DOCUMENTS_SUCCESS';
export const ODC_GET_DOCUMENTS_INVALID = 'ODC_GET_DOCUMENTS_INVALID';
export const ODC_GET_DOCUMENTS_ERROR = 'ODC_GET_DOCUMENTS_ERROR';

export const ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_REQUEST = 'ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_REQUEST';
export const ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_SUCCCESS = 'ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_SUCCCESS';
export const ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_INVALID = 'ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_INVALID';
export const ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_ERROR = 'ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_ERROR';

export const ODC_REGISTER_PENDING_DOCUMENTS_REQUEST = 'ODC_REGISTER_PENDING_DOCUMENTS_REQUEST';
export const ODC_REGISTER_PENDING_DOCUMENTS_SUCCESS = 'ODC_REGISTER_PENDING_DOCUMENTS_SUCCESS';
export const ODC_REGISTER_PENDING_DOCUMENTS_INVALID = 'ODC_REGISTER_PENDING_DOCUMENTS_INVALID';
export const ODC_REGISTER_PENDING_DOCUMENTS_ERROR = 'ODC_REGISTER_PENDING_DOCUMENTS_ERROR';

export const ODC_REGISTER_ACTIVITY_DOCUMENTS_REQUEST = 'ODC_REGISTER_ACTIVITY_DOCUMENTS_REQUEST';
export const ODC_REGISTER_ACTIVITY_DOCUMENTS_SUCCESS = 'ODC_REGISTER_ACTIVITY_DOCUMENTS_SUCCESS';
export const ODC_REGISTER_ACTIVITY_DOCUMENTS_INVALID = 'ODC_REGISTER_ACTIVITY_DOCUMENTS_INVALID';
export const ODC_REGISTER_ACTIVITY_DOCUMENTS_ERROR = 'ODC_REGISTER_ACTIVITY_DOCUMENTS_ERROR';

export function expressDocuments(data){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_EXPRESS_DOCUMENTS_REQUEST});
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_tipo_relacion: data.cod_tipo_relacion
        });

        return axios
            .post(`${URL_ODC}/Documento/ODC/Solicitudes`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    if(response.data.success){
                        dispatch({type: ODC_EXPRESS_DOCUMENTS_SUCCESS, data:response.data.documento });
                    }
                    else{
                        dispatch({
                            type: ODC_EXPRESS_DOCUMENTS_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            next:false,
                            response: true,
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                 // Error Request
                 let newError = error.response? error.response.data.error:SERVER_ERROR_MESSAGE; 
                 let response = error.response? true : false;
                 dispatch({type: ODC_EXPRESS_DOCUMENTS_ERROR, error:newError, response: response, next:false});
                 return error;
            });
    }
}
export function getDocuments(data){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_GET_DOCUMENTS_REQUEST});
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_tipo_relacion: data.cod_tipo_relacion
        });
        return axios
            .post(`${URL_ODC}/Documento/ODC/Solicitudes`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    if(response.data.success){
                        
                        dispatch({type: ODC_GET_DOCUMENTS_SUCCESS, data:response.data.documento });
                    }
                    else{
                        dispatch({
                            type: ODC_GET_DOCUMENTS_INVALID, 
                            data: null, 
                            error: response.data.error, 
                            next:false,
                            response: true,
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                 // Error Request
                 let newError = error.response? error.response.data.errorMessage: SERVER_ERROR_MESSAGE; 
                 let response = error.response? true : false;
                 dispatch({type: ODC_GET_DOCUMENTS_ERROR, error:newError, response: response, next:false});
                 return error;
            });
    }
}
export function downloadDocument(data){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_DOWNLOAD_DOCUMENT_REQUEST});
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_control: data.cod_control,
        });
        return axios
            .post(`${URL_ODC}/Documento/ODC/Descargar`, sendData, {
                responseType: 'blob',
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    dispatch({type: ODC_DOWNLOAD_DOCUMENT_SUCCESS, data: response.data });
                }
                else{
                    dispatch({
                        type: ODC_DOWNLOAD_DOCUMENT_INVALID, 
                        data: null, 
                        error: response.data.errorMessage, 
                        response: true,
                    });
                }
 
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error? DOCUMENT_NOT_FOUND_MESSAGE: SERVER_ERROR_MESSAGE; 
                let response = error? true : false;
                dispatch({type: ODC_DOWNLOAD_DOCUMENT_ERROR, error: newError, response: response, next:false});
                return error;
            });
    }
}
export function downloadReprintAttachedDocument(data){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_REQUEST});
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_solicitud_completa: data.cod_solicitud_completa,
        });
        return axios
            .post(`${URL_ODC}/Documento/Reimpresion/DescargarDocumentoAdjunto`, sendData, {
                responseType: 'blob',
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    dispatch({type: ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_SUCCCESS, data: response.data });
                }
                else{
                    dispatch({
                        type: ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_INVALID, 
                        data: null, 
                        error: response.data.errorMessage, 
                        response: true,
                    });
                }
 
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error? DOCUMENT_NOT_FOUND_MESSAGE: SERVER_ERROR_MESSAGE; 
                let response = error? true : false;
                dispatch({type: ODC_REPRINT_DOWNLOAD_REPRINT_ATTACHED_DOCUMENT_ERROR, error: newError, response: response, next:false});
                return error;
            });
    }
}

export function registerPendingDocuments(data) {
    const request = {
        url: `${URL_ODC}/Documento/RegistroPendienteGeneracionDocumento`,
        method: 'POST',
        data: {
            documentoPendiente: {
                ...data
            }
        }
    }
    const actions = {
        request: ODC_REGISTER_PENDING_DOCUMENTS_REQUEST,
        success: ODC_REGISTER_PENDING_DOCUMENTS_SUCCESS,
        invalid: ODC_REGISTER_PENDING_DOCUMENTS_INVALID,
        error: ODC_REGISTER_PENDING_DOCUMENTS_ERROR
    }
    return callApi(request, actions)
}

export function registerActivityDocuments(data) {
    const dataEncrypt = sessionStorage.getItem('data');
    const decryptData = decrypt(dataEncrypt);
    const device = getDeviceData();
    const request = {
        url: `${URL_ODC}/Documento/RegistroActividadGeneracionDocumento`,
        method: 'POST',
        data: {
            actividadDocumento: {
                ...data,
                des_usu_reg: decryptData.username,
                des_ter_reg: device.ip,
            }
        }
    }
    const actions = {
        request: ODC_REGISTER_ACTIVITY_DOCUMENTS_REQUEST,
        success: ODC_REGISTER_ACTIVITY_DOCUMENTS_SUCCESS,
        invalid: ODC_REGISTER_ACTIVITY_DOCUMENTS_INVALID,
        error: ODC_REGISTER_ACTIVITY_DOCUMENTS_ERROR
    }
    return callApi(request, actions)
}