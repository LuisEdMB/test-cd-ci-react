// Config
import { URL_ODC } from '../config'

// Generic API
import { callApi } from '../generic/api'

// Utils
import * as Utils from '../../utils/Utils'

// Consult Client
export const ODC_REPRINT_CONSULT_CLIENT_REQUEST = 'ODC_REPRINT_CONSULT_CLIENT_REQUEST'
export const ODC_REPRINT_CONSULT_CLIENT_SUCCESS = 'ODC_REPRINT_CONSULT_CLIENT_SUCCESS'
export const ODC_REPRINT_CONSULT_CLIENT_INVALID = 'ODC_REPRINT_CONSULT_CLIENT_INVALID'
export const ODC_REPRINT_CONSULT_CLIENT_ERROR = 'ODC_REPRINT_CONSULT_CLIENT_ERROR'

// Upload Document
export const ODC_REPRINT_UPLOAD_DOCUMENT_REQUEST = 'ODC_REPRINT_UPLOAD_DOCUMENT_REQUEST'
export const ODC_REPRINT_UPLOAD_DOCUMENT_SUCCESS = 'ODC_REPRINT_UPLOAD_DOCUMENT_SUCCESS'
export const ODC_REPRINT_UPLOAD_DOCUMENT_INVALID = 'ODC_REPRINT_UPLOAD_DOCUMENT_INVALID'
export const ODC_REPRINT_UPLOAD_DOCUMENT_ERROR = 'ODC_REPRINT_UPLOAD_DOCUMENT_ERROR'

// Register Reprint Reason
export const ODC_REPRINT_REGISTER_REPRINT_REASON_REQUEST = 'ODC_REPRINT_REGISTER_REPRINT_REASON_REQUEST'
export const ODC_REPRINT_REGISTER_REPRINT_REASON_SUCCESS = 'ODC_REPRINT_REGISTER_REPRINT_REASON_SUCCESS'
export const ODC_REPRINT_REGISTER_REPRINT_REASON_INVALID = 'ODC_REPRINT_REGISTER_REPRINT_REASON_INVALID'
export const ODC_REPRINT_REGISTER_REPRINT_REASON_ERROR = 'ODC_REPRINT_REGISTER_REPRINT_REASON_ERROR'

// Register Reprint Detail
export const ODC_REPRINT_REGISTER_REPRINT_DETAIL_REQUEST = 'ODC_REPRINT_REGISTER_REPRINT_DETAIL_REQUEST'
export const ODC_REPRINT_REGISTER_REPRINT_DETAIL_SUCCESS = 'ODC_REPRINT_REGISTER_REPRINT_DETAIL_SUCCESS'
export const ODC_REPRINT_REGISTER_REPRINT_DETAIL_INVALID = 'ODC_REPRINT_REGISTER_REPRINT_DETAIL_INVALID'
export const ODC_REPRINT_REGISTER_REPRINT_DETAIL_ERROR = 'ODC_REPRINT_REGISTER_REPRINT_DETAIL_ERROR'

// Register Pending Emboss
export const ODC_REPRINT_REGISTER_PENDING_EMBOSS_REQUEST = 'ODC_REPRINT_REGISTER_PENDING_EMBOSS_REQUEST'
export const ODC_REPRINT_REGISTER_PENDING_EMBOSS_SUCCESS = 'ODC_REPRINT_REGISTER_PENDING_EMBOSS_SUCCESS'
export const ODC_REPRINT_REGISTER_PENDING_EMBOSS_INVALID = 'ODC_REPRINT_REGISTER_PENDING_EMBOSS_INVALID'
export const ODC_REPRINT_REGISTER_PENDING_EMBOSS_ERROR = 'ODC_REPRINT_REGISTER_PENDING_EMBOSS_ERROR'

// Register Contingency Process
export const ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_REQUEST = 'ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_REQUEST'
export const ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_SUCCESS = 'ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_SUCCESS'
export const ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_INVALID = 'ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_INVALID'
export const ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_ERROR = 'ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_ERROR'

// Register Activity
export const ODC_REPRINT_REGISTER_ACTIVITY_REQUEST = 'ODC_REPRINT_REGISTER_ACTIVITY_REQUEST'
export const ODC_REPRINT_REGISTER_ACTIVITY_SUCCESS = 'ODC_REPRINT_REGISTER_ACTIVITY_SUCCESS'
export const ODC_REPRINT_REGISTER_ACTIVITY_INVALID = 'ODC_REPRINT_REGISTER_ACTIVITY_INVALID'
export const ODC_REPRINT_REGISTER_ACTIVITY_ERROR = 'ODC_REPRINT_REGISTER_ACTIVITY_ERROR'

// Cancel Activity
export const ODC_REPRINT_CANCEL_ACTIVITY_REQUEST = 'ODC_REPRINT_CANCEL_ACTIVITY_REQUEST'
export const ODC_REPRINT_CANCEL_ACTIVITY_SUCCESS = 'ODC_REPRINT_CANCEL_ACTIVITY_SUCCESS'
export const ODC_REPRINT_CANCEL_ACTIVITY_INVALID = 'ODC_REPRINT_CANCEL_ACTIVITY_INVALID'
export const ODC_REPRINT_CANCEL_ACTIVITY_ERROR = 'ODC_REPRINT_CANCEL_ACTIVITY_ERROR'

// Continue Process
export const ODC_REPRINT_CONTINUE_PROCESS_REQUEST = 'ODC_REPRINT_CONTINUE_PROCESS_REQUEST'
export const ODC_REPRINT_CONTINUE_PROCESS_SUCCESS = 'ODC_REPRINT_CONTINUE_PROCESS_SUCCESS'
export const ODC_REPRINT_CONTINUE_PROCESS_INVALID = 'ODC_REPRINT_CONTINUE_PROCESS_INVALID'
export const ODC_REPRINT_CONTINUE_PROCESS_ERROR = 'ODC_REPRINT_CONTINUE_PROCESS_ERROR'

// Register Continue Process Activity
export const ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_REQUEST = 'ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_REQUEST'
export const ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_SUCCESS = 'ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_SUCCESS'
export const ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_INVALID = 'ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_INVALID'
export const ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_ERROR = 'ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_ERROR'

// Get Pending Processes Activation
export const ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_REQUEST = 'ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_REQUEST'
export const ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_SUCCESS = 'ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_SUCCESS'
export const ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_INVALID = 'ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_INVALID'
export const ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_ERROR = 'ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_ERROR'

// Authorize Activation Process
export const ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_REQUEST = 'ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_REQUEST'
export const ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS = 'ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS'
export const ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_INVALID = 'ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_INVALID'
export const ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_ERROR = 'ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_ERROR'

// Reject Activation Process
export const ODC_REPRINT_REJECT_ACTIVATION_PROCESS_REQUEST = 'ODC_REPRINT_REJECT_ACTIVATION_PROCESS_REQUEST'
export const ODC_REPRINT_REJECT_ACTIVATION_PROCESS_SUCCESS = 'ODC_REPRINT_REJECT_ACTIVATION_PROCESS_SUCCESS'
export const ODC_REPRINT_REJECT_ACTIVATION_PROCESS_INVALID = 'ODC_REPRINT_REJECT_ACTIVATION_PROCESS_INVALID'
export const ODC_REPRINT_REJECT_ACTIVATION_PROCESS_ERROR = 'ODC_REPRINT_REJECT_ACTIVATION_PROCESS_ERROR'

// Get Pending Processes
export const ODC_REPRINT_GET_PENDING_PROCESSES_REQUEST = 'ODC_REPRINT_GET_PENDING_PROCESSES_REQUEST'
export const ODC_REPRINT_GET_PENDING_PROCESSES_SUCCESS = 'ODC_REPRINT_GET_PENDING_PROCESSES_SUCCESS'
export const ODC_REPRINT_GET_PENDING_PROCESSES_INVALID = 'ODC_REPRINT_GET_PENDING_PROCESSES_INVALID'
export const ODC_REPRINT_GET_PENDING_PROCESSES_ERROR = 'ODC_REPRINT_GET_PENDING_PROCESSES_ERROR'

// Register Observation Validated
export const ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_REQUEST = 'ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_REQUEST'
export const ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_SUCCESS = 'ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_SUCCESS'
export const ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_INVALID = 'ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_INVALID'
export const ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_ERROR = 'ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_ERROR'

// Get Reprints For Searching
export const ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_REQUEST = 'ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_REQUEST'
export const ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_SUCCESS = 'ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_SUCCESS'
export const ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_INVALID = 'ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_INVALID'
export const ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_ERROR = 'ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_ERROR'

export function consultClient(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        terminalName: 'WEB',
        cod_agencia: userData.agencyCode,
        flg_visible: 1,
        flg_eliminado: 0,
        fec_actividad_ini: userData.currentlyDate,
        fec_actividad_fin: userData.currentlyDate,
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_REPRINT_CONSULT_CLIENT_REQUEST,
        success: ODC_REPRINT_CONSULT_CLIENT_SUCCESS,
        invalid: ODC_REPRINT_CONSULT_CLIENT_INVALID,
        error: ODC_REPRINT_CONSULT_CLIENT_ERROR
    }
    return callApi(request, actions)
}

export function uploadDocument(data) {
    const userData = Utils.getDataUserSession()
    let formData = data.object.data
    formData.append('fec_reg', userData.currentlyDate)
    formData.append('des_usu_reg', userData.username)
    formData.append('des_nombre_archivo', 'Reimpresiones')
    formData.append('des_ter_reg', 'WEB')
    formData.append('terminalName', userData.ip)
    const request = {
        url: data.object.url,
        method: data.object.method,
        data: formData,
        contentType: 'multipart/form-data',
        extra: {
            onUploadProgress: data.onUploadProgress
        }
    }
    const actions = {
        request: ODC_REPRINT_UPLOAD_DOCUMENT_REQUEST,
        success: ODC_REPRINT_UPLOAD_DOCUMENT_SUCCESS,
        invalid: ODC_REPRINT_UPLOAD_DOCUMENT_INVALID,
        error: ODC_REPRINT_UPLOAD_DOCUMENT_ERROR
    }
    return callApi(request, actions, false)
}

export function registerReprintReason(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        cod_agencia: userData.agencyCode,
        flg_visible: 1,
        flg_eliminado: 0,
        ...data.data,
        motivoReimpresion: {
            ...data.data.motivoReimpresion,
            cod_agencia: userData.agencyCode
        }
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_REPRINT_REGISTER_REPRINT_REASON_REQUEST,
        success: ODC_REPRINT_REGISTER_REPRINT_REASON_SUCCESS,
        invalid: ODC_REPRINT_REGISTER_REPRINT_REASON_INVALID,
        error: ODC_REPRINT_REGISTER_REPRINT_REASON_ERROR
    }
    return callApi(request, actions)
}

export function registerReprintDetail(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        flg_visible: 1,
        flg_eliminado: 0,
        ...data.data,
        motivoReimpresion: {
            ...data.data.motivoReimpresion,
            cod_agencia: userData.agencyCode
        }
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_REPRINT_REGISTER_REPRINT_DETAIL_REQUEST,
        success: ODC_REPRINT_REGISTER_REPRINT_DETAIL_SUCCESS,
        invalid: ODC_REPRINT_REGISTER_REPRINT_DETAIL_INVALID,
        error: ODC_REPRINT_REGISTER_REPRINT_DETAIL_ERROR
    }
    return callApi(request, actions)
}

export function registerPendingEmboss(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_REPRINT_REGISTER_PENDING_EMBOSS_REQUEST,
        success: ODC_REPRINT_REGISTER_PENDING_EMBOSS_SUCCESS,
        invalid: ODC_REPRINT_REGISTER_PENDING_EMBOSS_INVALID,
        error: ODC_REPRINT_REGISTER_PENDING_EMBOSS_ERROR
    }
    return callApi(request, actions)
}

export function registerContingencyProcess(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_REQUEST,
        success: ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_SUCCESS,
        invalid: ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_INVALID,
        error: ODC_REPRINT_REGISTER_CONTINGENCY_PROCESS_ERROR
    }
    return callApi(request, actions)
}

export function registerActivity(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_REPRINT_REGISTER_ACTIVITY_REQUEST,
        success: ODC_REPRINT_REGISTER_ACTIVITY_SUCCESS,
        invalid: ODC_REPRINT_REGISTER_ACTIVITY_INVALID,
        error: ODC_REPRINT_REGISTER_ACTIVITY_ERROR
    }
    return callApi(request, actions)
}

export function cancelActivity(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_REPRINT_CANCEL_ACTIVITY_REQUEST,
        success: ODC_REPRINT_CANCEL_ACTIVITY_SUCCESS,
        invalid: ODC_REPRINT_CANCEL_ACTIVITY_INVALID,
        error: ODC_REPRINT_CANCEL_ACTIVITY_ERROR
    }
    return callApi(request, actions)
}

export function continueProcess(data) {
    const request = {
        url: data.url,
        method: data.method,
        data: data.data
    }
    const actions = {
        request: ODC_REPRINT_CONTINUE_PROCESS_REQUEST,
        success: ODC_REPRINT_CONTINUE_PROCESS_SUCCESS,
        invalid: ODC_REPRINT_CONTINUE_PROCESS_INVALID,
        error: ODC_REPRINT_CONTINUE_PROCESS_ERROR
    }
    return callApi(request, actions)
}

export function registerContinueProcessActivity(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_REQUEST,
        success: ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_SUCCESS,
        invalid: ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_INVALID,
        error: ODC_REPRINT_REGISTER_CONTINUE_PROCESS_ACTIVITY_ERROR
    }
    return callApi(request, actions)
}

export function getPendingProcessesActivation() {
    const request = {
        url: `${ URL_ODC }/Requerimiento/Reimpresiones/SolicitudesPorAutorizar`,
        method: 'GET',
        data: null
    }
    const actions = {
        request: ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_REQUEST,
        success: ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_SUCCESS,
        invalid: ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_INVALID,
        error: ODC_REPRINT_GET_PENDING_PROCESSES_ACTIVATION_ERROR
    }
    return callApi(request, actions)
}

export function authorizeActivationProcess(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        ...data
    }
    const request = {
        url: `${ URL_ODC }/Requerimiento/RegistrarActividadGenericoSimple`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_REQUEST,
        success: ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS,
        invalid: ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_INVALID,
        error: ODC_REPRINT_AUTHORIZE_ACTIVATION_PROCESS_ERROR
    }
    return callApi(request, actions)
}

export function rejectActivationProcess(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        cod_agencia: userData.agencyCode,
        ...data
    }
    const request = {
        url: `${ URL_ODC }/Originacion/RegistrarActividadRechazar`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_REPRINT_REJECT_ACTIVATION_PROCESS_REQUEST,
        success: ODC_REPRINT_REJECT_ACTIVATION_PROCESS_SUCCESS,
        invalid: ODC_REPRINT_REJECT_ACTIVATION_PROCESS_INVALID,
        error: ODC_REPRINT_REJECT_ACTIVATION_PROCESS_ERROR
    }
    return callApi(request, actions)
}

export function getPendingProcesses() {
    const request = {
        url: `${ URL_ODC }/Requerimiento/Reimpresiones/Solicitudes`,
        method: 'GET',
        data: null
    }
    const actions = {
        request: ODC_REPRINT_GET_PENDING_PROCESSES_REQUEST,
        success: ODC_REPRINT_GET_PENDING_PROCESSES_SUCCESS,
        invalid: ODC_REPRINT_GET_PENDING_PROCESSES_INVALID,
        error: ODC_REPRINT_GET_PENDING_PROCESSES_ERROR
    }
    return callApi(request, actions)
}

export function registerObservationValidated(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        ...data
    }
    const request = {
        url: `${ URL_ODC }/Requerimiento/RegistrarActividadGenericoSimple`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_REQUEST,
        success: ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_SUCCESS,
        invalid: ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_INVALID,
        error: ODC_REPRINT_REGISTER_OBSERVATION_VALIDATED_ERROR
    }
    return callApi(request, actions)
}

export function getReprintsForSearching(data) {
    const request = {
        url: `${ URL_ODC }/Requerimiento/Reimpresiones/SolicitudesConsulta?` + 
                `cod_solicitud_completa=${ data.cod_solicitud_completa }&des_nro_documento=${ data.des_nro_documento }&` + 
                `cod_agencia=${ data.cod_agencia }&fec_inicial=${ data.fec_inicial }&fec_final=${ data.fec_final }`,
        method: 'GET',
        data: null
    }
    const actions = {
        request: ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_REQUEST,
        success: ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_SUCCESS,
        invalid: ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_INVALID,
        error: ODC_REPRINT_GET_REPRINTS_FOR_SEARCHING_ERROR
    }
    return callApi(request, actions)
}