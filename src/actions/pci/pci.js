// URL APIs
import { URL_ODC } from '../config'

// Generic API
import { callApi } from '../generic/api'

// Utils
import * as moment from 'moment';
import * as Utils from '../../utils/Utils'

// Encrypt Cardnumber
export const PCI_ENCRYPT_CARDNUMBER_REQUEST = 'PCI_ENCRYPT_CARDNUMBER_REQUEST'
export const PCI_ENCRYPT_CARDNUMBER_SUCCESS = 'PCI_ENCRYPT_CARDNUMBER_SUCCESS'
export const PCI_ENCRYPT_CARDNUMBER_INVALID = 'PCI_ENCRYPT_CARDNUMBER_INVALID'
export const PCI_ENCRYPT_CARDNUMBER_ERROR = 'PCI_ENCRYPT_CARDNUMBER_ERROR'

// Decrypt Cardnumber
export const PCI_DECRYPT_CARDNUMBER_REQUEST = 'PCI_DECRYPT_CARDNUMBER_REQUEST'
export const PCI_DECRYPT_CARDNUMBER_SUCCESS = 'PCI_DECRYPT_CARDNUMBER_SUCCESS'
export const PCI_DECRYPT_CARDNUMBER_INVALID = 'PCI_DECRYPT_CARDNUMBER_INVALID'
export const PCI_DECRYPT_CARDNUMBER_ERROR = 'PCI_DECRYPT_CARDNUMBER_ERROR'

export function encryptCardnumber(data) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_flujo_etapa: data.pciStage,
        fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip
    }

    const request = {
        url: `${ URL_ODC }/Pci/PciEncriptar`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: PCI_ENCRYPT_CARDNUMBER_REQUEST,
        success: PCI_ENCRYPT_CARDNUMBER_SUCCESS,
        invalid: PCI_ENCRYPT_CARDNUMBER_INVALID,
        error: PCI_ENCRYPT_CARDNUMBER_ERROR
    }
    return callApi(request, actions)
}

export function decryptCardnumber(data) {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = Utils.decrypt(dataEncrypt)
    const device = Utils.getDeviceData()
    
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        token: data.token,
        cod_etapa_maestro: data.masterStageCode,
        fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        des_usu_reg: decryptData.username,
        des_ter_reg: device.ip
    }

    const request = {
        url: `${ URL_ODC }/Pci/PciDesencriptar`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: PCI_DECRYPT_CARDNUMBER_REQUEST,
        success: PCI_DECRYPT_CARDNUMBER_SUCCESS,
        invalid: PCI_DECRYPT_CARDNUMBER_INVALID,
        error: PCI_DECRYPT_CARDNUMBER_ERROR
    }
    return callApi(request, actions)
}