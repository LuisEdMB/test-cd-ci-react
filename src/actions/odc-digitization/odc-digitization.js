import axios from 'axios';
import * as moment from 'moment';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';
import { decrypt, getDeviceData } from '../../utils/Utils';
// Actions 
export const ODC_DIGITIZATION_INBOX_SEND_REQUEST = 'ODC_DIGITIZATION_INBOX_SEND_REQUEST';
export const ODC_DIGITIZATION_INBOX_SEND_SUCCESS = 'ODC_DIGITIZATION_INBOX_SEND_SUCCESS';
export const ODC_DIGITIZATION_INBOX_SEND_INVALID = 'ODC_DIGITIZATION_INBOX_SEND_INVALID';
export const ODC_DIGITIZATION_INBOX_SEND_ERROR = 'ODC_DIGITIZATION_INBOX_SEND_ERROR';

export const ODC_DIGITIZATION_INBOX_RECEIVED_REQUEST = 'ODC_DIGITIZATION_INBOX_RECEIVED_REQUEST';
export const ODC_DIGITIZATION_INBOX_RECEIVED_SUCCESS = 'ODC_DIGITIZATION_INBOX_RECEIVED_SUCCESS';
export const ODC_DIGITIZATION_INBOX_RECEIVED_INVALID = 'ODC_DIGITIZATION_INBOX_RECEIVED_INVALID';
export const ODC_DIGITIZATION_INBOX_RECEIVED_ERROR = 'ODC_DIGITIZATION_INBOX_RECEIVED_ERROR';

export const ODC_DIGITIZATION_INBOX_MONITORING_REQUEST = 'ODC_DIGITIZATION_INBOX_MONITORING_REQUEST';
export const ODC_DIGITIZATION_INBOX_MONITORING_SUCCESS = 'ODC_DIGITIZATION_INBOX_MONITORING_SUCCESS';
export const ODC_DIGITIZATION_INBOX_MONITORING_INVALID = 'ODC_DIGITIZATION_INBOX_MONITORING_INVALID';
export const ODC_DIGITIZATION_INBOX_MONITORING_ERROR = 'ODC_DIGITIZATION_INBOX_MONITORING_ERROR';

export const ODC_DIGITIZATION_SEND_LEGAJO_REQUEST = 'ODC_DIGITIZATION_SEND_LEGAJO_REQUEST';
export const ODC_DIGITIZATION_SEND_LEGAJO_SUCCESS = 'ODC_DIGITIZATION_SEND_LEGAJO_SUCCESS';
export const ODC_DIGITIZATION_SEND_LEGAJO_INVALID = 'ODC_DIGITIZATION_SEND_LEGAJO_INVALID';
export const ODC_DIGITIZATION_SEND_LEGAJO_ERROR = 'ODC_DIGITIZATION_SEND_LEGAJO_ERROR';

export const ODC_DIGITIZATION_REGULARIZE_LEGAJO_REQUEST = 'ODC_DIGITIZATION_REGULARIZE_LEGAJO_REQUEST';
export const ODC_DIGITIZATION_REGULARIZE_LEGAJO_SUCCESS = 'ODC_DIGITIZATION_REGULARIZE_LEGAJO_SUCCESS';
export const ODC_DIGITIZATION_REGULARIZE_LEGAJO_INVALID = 'ODC_DIGITIZATION_REGULARIZE_LEGAJO_INVALID';
export const ODC_DIGITIZATION_REGULARIZE_LEGAJO_ERROR = 'ODC_DIGITIZATION_REGULARIZE_LEGAJO_ERROR';

export const ODC_DIGITIZATION_OBSERVE_LEGAJO_REQUEST = 'ODC_DIGITIZATION_OBSERVE_LEGAJO_REQUEST';
export const ODC_DIGITIZATION_OBSERVE_LEGAJO_SUCCESS = 'ODC_DIGITIZATION_OBSERVE_LEGAJO_SUCCESS';
export const ODC_DIGITIZATION_OBSERVE_LEGAJO_INVALID = 'ODC_DIGITIZATION_OBSERVE_LEGAJO_INVALID';
export const ODC_DIGITIZATION_OBSERVE_LEGAJO_ERROR = 'ODC_DIGITIZATION_OBSERVE_LEGAJO_ERROR';

export const ODC_DIGITIZATION_RECEIVE_LEGAJO_REQUEST = 'ODC_DIGITIZATION_RECEIVE_LEGAJO_REQUEST';
export const ODC_DIGITIZATION_RECEIVE_LEGAJO_SUCCESS = 'ODC_DIGITIZATION_RECEIVE_LEGAJO_SUCCESS';
export const ODC_DIGITIZATION_RECEIVE_LEGAJO_INVALID = 'ODC_DIGITIZATION_RECEIVE_LEGAJO_INVALID';
export const ODC_DIGITIZATION_RECEIVE_LEGAJO_ERROR = 'ODC_DIGITIZATION_RECEIVE_LEGAJO_ERROR';

// GET 
export function inboxSend(cod_solicitud_completa, cod_tipo_documento, des_nro_documento){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_DIGITIZATION_INBOX_SEND_REQUEST});
        const dataEncrypt = sessionStorage.getItem('data');
        const decryptData = decrypt(dataEncrypt);
        const username = "";
        const agencyId = decryptData.agency? decryptData.agency.ide_agencia: 0;
        // Launch Dispatch - Server
        return axios
            .get(`${URL_ODC}/Digitalizacion/SolicitudesEnviarLegajo?cod_solicitud_completa=${cod_solicitud_completa}&cod_tipo_documento=${cod_tipo_documento}&des_nro_documento${des_nro_documento}&cod_agencia=${agencyId}&des_usuario=${username}`, {   
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: ODC_DIGITIZATION_INBOX_SEND_SUCCESS, data: response.data });
                    }
                    else{
                        dispatch({ 
                            type: ODC_DIGITIZATION_INBOX_SEND_INVALID, 
                            data: [], 
                            error: response.data.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_DIGITIZATION_INBOX_SEND_ERROR, error:newError, response: response});
                return error;
            });
    }
}
// GET
export function inboxReceived(cod_solicitud_completa="", cod_tipo_documento = 0, des_nro_documento =""){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_DIGITIZATION_INBOX_RECEIVED_REQUEST});
        // Launch Dispatch - Server
        return axios
            .get(`${URL_ODC}/Digitalizacion/SolicitudesRecepcionarLegajo`, {   
            headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                // Launch Dispatch 
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: ODC_DIGITIZATION_INBOX_RECEIVED_SUCCESS, data: response.data });
                    }
                    else{
                        dispatch({ 
                            type: ODC_DIGITIZATION_INBOX_RECEIVED_INVALID, 
                            data: [], 
                            error: response.data.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_DIGITIZATION_INBOX_RECEIVED_ERROR, error:newError, response: response});
                return error;
            });
    }
}
// GET
export function inboxMonitoring(cod_solicitud_completa="", cod_tipo_documento = 0, des_nro_documento =""){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_DIGITIZATION_INBOX_MONITORING_REQUEST});
        // Launch Dispatch - Server
        return axios
            .get(`${URL_ODC}/Digitalizacion/SolicitudesMonitoreo`, {   
            headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                // Launch Dispatch 
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: ODC_DIGITIZATION_INBOX_MONITORING_SUCCESS, data: response.data });
                    }
                    else{
                        dispatch({ 
                            type: ODC_DIGITIZATION_INBOX_MONITORING_INVALID, 
                            data: [], 
                            error: response.data.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_DIGITIZATION_INBOX_MONITORING_ERROR, error:newError, response: response});
                return error;
            });
    }
}
// POST
export function sendLegajo(data, extra=null){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_DIGITIZATION_SEND_LEGAJO_REQUEST});
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_cliente: data.cod_cliente,
            cod_solicitud_digitalizacion: data.cod_solicitud_digitalizacion,
            digitalizacionActividad: {
                nom_actividad: data.digitalizacionActividad.nom_actividad,
                des_usu_responsable: decryptData.username,
                cod_flujo_fase_estado_actual: data.digitalizacionActividad.cod_flujo_fase_estado_actual,
                cod_flujo_fase_estado_anterior: data.digitalizacionActividad.cod_flujo_fase_estado_anterior,
                fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_observacion_legajo: data.digitalizacionActividad.des_observacion_legajo,
                flg_visible: true,
                flg_eliminado: false
            }
        });
        return axios
            .post(`${URL_ODC}/Digitalizacion/Registro`, sendData, {   
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    if(response.data.success){
                        dispatch({type: ODC_DIGITIZATION_SEND_LEGAJO_SUCCESS, data:response.data, extra:extra });
                    }
                    else{
                        dispatch({
                            type: ODC_DIGITIZATION_SEND_LEGAJO_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true,
                            extra: extra
                        });
                    }
                }   
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data? error.response.data.errorMessage: SERVER_ERROR_MESSAGE:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_DIGITIZATION_SEND_LEGAJO_ERROR, error:newError, response: response,  extra: extra});
            });
    }
}
// POST
export function regularizeLegajo(data, extra=null){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_DIGITIZATION_REGULARIZE_LEGAJO_REQUEST});
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_cliente: data.cod_cliente,
            cod_solicitud_digitalizacion: data.cod_solicitud_digitalizacion,
            digitalizacionActividad: {
                nom_actividad: data.digitalizacionActividad.nom_actividad,
                des_usu_responsable: decryptData.username,
                cod_flujo_fase_estado_actual: data.digitalizacionActividad.cod_flujo_fase_estado_actual,
                cod_flujo_fase_estado_anterior: data.digitalizacionActividad.cod_flujo_fase_estado_anterior,
                fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_observacion_legajo: data.digitalizacionActividad.des_observacion_legajo,
                flg_visible: true,
                flg_eliminado: false
            }
        });
        return axios
            .post(`${URL_ODC}/Digitalizacion/Registro`, sendData, {   
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    if(response.data.success){
                        dispatch({type: ODC_DIGITIZATION_REGULARIZE_LEGAJO_SUCCESS, data:response.data, extra:extra });
                    }
                    else{
                        dispatch({
                            type: ODC_DIGITIZATION_REGULARIZE_LEGAJO_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true,
                            extra: extra
                        });
                    }
                }   
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data? error.response.data.errorMessage: SERVER_ERROR_MESSAGE:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_DIGITIZATION_REGULARIZE_LEGAJO_ERROR, error:newError, response: response,  extra: extra});
            });
    }
}
// POST
export function observeLegajo(data, extra=null){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_DIGITIZATION_OBSERVE_LEGAJO_REQUEST});
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_cliente: data.cod_cliente,
            cod_solicitud_digitalizacion: data.cod_solicitud_digitalizacion,
            digitalizacionActividad: {
                nom_actividad: data.digitalizacionActividad.nom_actividad,
                des_usu_responsable: decryptData.username,
                cod_flujo_fase_estado_actual: data.digitalizacionActividad.cod_flujo_fase_estado_actual,
                cod_flujo_fase_estado_anterior: data.digitalizacionActividad.cod_flujo_fase_estado_anterior,
                fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_observacion_legajo: data.digitalizacionActividad.des_observacion_legajo,
                flg_visible: true,
                flg_eliminado: false
            }
        });
        return axios
            .post(`${URL_ODC}/Digitalizacion/Registro`, sendData, {   
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    if(response.data.success){
                        dispatch({type: ODC_DIGITIZATION_OBSERVE_LEGAJO_SUCCESS, data:response.data, extra:extra });
                    }
                    else{
                        dispatch({
                            type: ODC_DIGITIZATION_OBSERVE_LEGAJO_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true,
                            extra: extra
                        });
                    }
                }   
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data? error.response.data.errorMessage: SERVER_ERROR_MESSAGE:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_DIGITIZATION_OBSERVE_LEGAJO_ERROR, error:newError, response: response,  extra: extra});
            });
    }
}
// POST
export function receiveLegajo(data, extra=null){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_DIGITIZATION_RECEIVE_LEGAJO_REQUEST});
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_cliente: data.cod_cliente,
            cod_solicitud_digitalizacion: data.cod_solicitud_digitalizacion,
            digitalizacionActividad: {
                nom_actividad: data.digitalizacionActividad.nom_actividad,
                des_usu_responsable: decryptData.username,
                cod_flujo_fase_estado_actual: data.digitalizacionActividad.cod_flujo_fase_estado_actual,
                cod_flujo_fase_estado_anterior: data.digitalizacionActividad.cod_flujo_fase_estado_anterior,
                fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                des_observacion_legajo: data.digitalizacionActividad.des_observacion_legajo,
                flg_visible: true,
                flg_eliminado: false
            }
        });
        return axios
            .post(`${URL_ODC}/Digitalizacion/Registro`, sendData, {   
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                if(response.data){
                    if(response.data.success){

                        dispatch({type: ODC_DIGITIZATION_RECEIVE_LEGAJO_SUCCESS, data: response.data, extra:extra });
                    }
                    else{
                        dispatch({
                            type: ODC_DIGITIZATION_RECEIVE_LEGAJO_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true,
                            extra: extra
                        });
                    }
                }   
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data? error.response.data.errorMessage: SERVER_ERROR_MESSAGE:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_DIGITIZATION_RECEIVE_LEGAJO_ERROR, error:newError, response: response,  extra: extra});
            });
    }
}