// Config
import { URL_ODC } from '../config'

// Generic API
import { callApi } from '../generic/api'

// Utils
import * as Utils from '../../utils/Utils'

// Consult Client
export const ODC_ADDITIONAL_CONSULT_CLIENT_REQUEST = 'ODC_ADDITIONAL_CONSULT_CLIENT_REQUEST'
export const ODC_ADDITIONAL_CONSULT_CLIENT_SUCCESS = 'ODC_ADDITIONAL_CONSULT_CLIENT_SUCCESS'
export const ODC_ADDITIONAL_CONSULT_CLIENT_INVALID = 'ODC_ADDITIONAL_CONSULT_CLIENT_INVALID'
export const ODC_ADDITIONAL_CONSULT_CLIENT_ERROR = 'ODC_ADDITIONAL_CONSULT_CLIENT_ERROR'

// Validate Additional Client Already Belongs To Holder Client
export const ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_REQUEST = 'ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_REQUEST'
export const ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_SUCCESS = 'ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_SUCCESS'
export const ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_INVALID = 'ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_INVALID'
export const ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_ERROR = 'ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_ERROR'

// Register Additional Client
export const ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_REQUEST = 'ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_REQUEST'
export const ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_SUCCESS = 'ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_SUCCESS'
export const ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_INVALID = 'ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_INVALID'
export const ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_ERROR = 'ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_ERROR'

// Register Additional Detail
export const ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_REQUEST = 'ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_REQUEST'
export const ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_SUCCESS = 'ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_SUCCESS'
export const ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_INVALID = 'ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_INVALID'
export const ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_ERROR = 'ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_ERROR'

// Register Pending Emboss
export const ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_REQUEST = 'ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_REQUEST'
export const ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_SUCCESS = 'ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_SUCCESS'
export const ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_INVALID = 'ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_INVALID'
export const ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_ERROR = 'ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_ERROR'

// Register Contingency Process
export const ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_REQUEST = 'ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_REQUEST'
export const ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_SUCCESS = 'ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_SUCCESS'
export const ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_INVALID = 'ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_INVALID'
export const ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_ERROR = 'ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_ERROR'

// Register Activity
export const ODC_ADDITIONAL_REGISTER_ACTIVITY_REQUEST = 'ODC_ADDITIONAL_REGISTER_ACTIVITY_REQUEST'
export const ODC_ADDITIONAL_REGISTER_ACTIVITY_SUCCESS = 'ODC_ADDITIONAL_REGISTER_ACTIVITY_SUCCESS'
export const ODC_ADDITIONAL_REGISTER_ACTIVITY_INVALID = 'ODC_ADDITIONAL_REGISTER_ACTIVITY_INVALID'
export const ODC_ADDITIONAL_REGISTER_ACTIVITY_ERROR = 'ODC_ADDITIONAL_REGISTER_ACTIVITY_ERROR'

// Cancel Activity
export const ODC_ADDITIONAL_CANCEL_ACTIVITY_REQUEST = 'ODC_ADDITIONAL_CANCEL_ACTIVITY_REQUEST'
export const ODC_ADDITIONAL_CANCEL_ACTIVITY_SUCCESS = 'ODC_ADDITIONAL_CANCEL_ACTIVITY_SUCCESS'
export const ODC_ADDITIONAL_CANCEL_ACTIVITY_INVALID = 'ODC_ADDITIONAL_CANCEL_ACTIVITY_INVALID'
export const ODC_ADDITIONAL_CANCEL_ACTIVITY_ERROR = 'ODC_ADDITIONAL_CANCEL_ACTIVITY_ERROR'

// Continue Process
export const ODC_ADDITIONAL_CONTINUE_PROCESS_REQUEST = 'ODC_ADDITIONAL_CONTINUE_PROCESS_REQUEST'
export const ODC_ADDITIONAL_CONTINUE_PROCESS_SUCCESS = 'ODC_ADDITIONAL_CONTINUE_PROCESS_SUCCESS'
export const ODC_ADDITIONAL_CONTINUE_PROCESS_INVALID = 'ODC_ADDITIONAL_CONTINUE_PROCESS_INVALID'
export const ODC_ADDITIONAL_CONTINUE_PROCESS_ERROR = 'ODC_ADDITIONAL_CONTINUE_PROCESS_ERROR'

// Register Continue Process Activity
export const ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_REQUEST = 'ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_REQUEST'
export const ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_SUCCESS = 'ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_SUCCESS'
export const ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_INVALID = 'ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_INVALID'
export const ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_ERROR = 'ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_ERROR'

// Get Pending Processes Activation
export const ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_REQUEST = 'ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_REQUEST'
export const ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_SUCCESS = 'ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_SUCCESS'
export const ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_INVALID = 'ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_INVALID'
export const ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_ERROR = 'ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_ERROR'

// Authorize Activation Process
export const ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_REQUEST = 'ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_REQUEST'
export const ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS = 'ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS'
export const ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_INVALID = 'ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_INVALID'
export const ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_ERROR = 'ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_ERROR'

// Reject Activation Process
export const ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_REQUEST = 'ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_REQUEST'
export const ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_SUCCESS = 'ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_SUCCESS'
export const ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_INVALID = 'ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_INVALID'
export const ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_ERROR = 'ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_ERROR'

// Get Pending Processes
export const ODC_ADDITIONAL_GET_PENDING_PROCESSES_REQUEST = 'ODC_ADDITIONAL_GET_PENDING_PROCESSES_REQUEST'
export const ODC_ADDITIONAL_GET_PENDING_PROCESSES_SUCCESS = 'ODC_ADDITIONAL_GET_PENDING_PROCESSES_SUCCESS'
export const ODC_ADDITIONAL_GET_PENDING_PROCESSES_INVALID = 'ODC_ADDITIONAL_GET_PENDING_PROCESSES_INVALID'
export const ODC_ADDITIONAL_GET_PENDING_PROCESSES_ERROR = 'ODC_ADDITIONAL_GET_PENDING_PROCESSES_ERROR'

// Register Observation Validated
export const ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_REQUEST = 'ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_REQUEST'
export const ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_SUCCESS = 'ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_SUCCESS'
export const ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_INVALID = 'ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_INVALID'
export const ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_ERROR = 'ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_ERROR'

export function consultClient(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        flg_visible: 1,
        flg_eliminado: 0,
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_CONSULT_CLIENT_REQUEST,
        success: ODC_ADDITIONAL_CONSULT_CLIENT_SUCCESS,
        invalid: ODC_ADDITIONAL_CONSULT_CLIENT_INVALID,
        error: ODC_ADDITIONAL_CONSULT_CLIENT_ERROR
    }
    return callApi(request, actions)
}

export function validateAdditionalClientAlreadyBelongsHolderClient(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_REQUEST,
        success: ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_SUCCESS,
        invalid: ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_INVALID,
        error: ODC_ADDITIONAL_VALIDATE_ADDITIONAL_CLIENT_ALREADY_BELONGS_TO_HOLDER_CLIENT_ERROR
    }
    return callApi(request, actions)
}

export function registerAdditionalClient(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        cod_agencia: userData.agencyCode,
        flg_visible: 1,
        flg_eliminado: 0,
        terminalName: 'WEB',
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_REQUEST,
        success: ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_SUCCESS,
        invalid: ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_INVALID,
        error: ODC_ADDITIONAL_REGISTER_ADDITIONAL_CLIENT_ERROR
    }
    return callApi(request, actions)
}

export function registerAdditionalDetail(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        terminalName: 'WEB',
        flg_visible: 1,
        flg_eliminado: 0,
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_REQUEST,
        success: ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_SUCCESS,
        invalid: ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_INVALID,
        error: ODC_ADDITIONAL_REGISTER_ADDITIONAL_DETAIL_ERROR
    }
    return callApi(request, actions)
}

export function registerPendingEmboss(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        terminalName: 'WEB',
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_REQUEST,
        success: ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_SUCCESS,
        invalid: ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_INVALID,
        error: ODC_ADDITIONAL_REGISTER_PENDING_EMBOSS_ERROR
    }
    return callApi(request, actions)
}

export function registerContingencyProcess(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        terminalName: 'WEB',
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_REQUEST,
        success: ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_SUCCESS,
        invalid: ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_INVALID,
        error: ODC_ADDITIONAL_REGISTER_CONTINGENCY_PROCESS_ERROR
    }
    return callApi(request, actions)
}

export function registerActivity(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        terminalName: 'WEB',
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_REGISTER_ACTIVITY_REQUEST,
        success: ODC_ADDITIONAL_REGISTER_ACTIVITY_SUCCESS,
        invalid: ODC_ADDITIONAL_REGISTER_ACTIVITY_INVALID,
        error: ODC_ADDITIONAL_REGISTER_ACTIVITY_ERROR
    }
    return callApi(request, actions)
}

export function cancelActivity(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        terminalName: 'WEB',
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_CANCEL_ACTIVITY_REQUEST,
        success: ODC_ADDITIONAL_CANCEL_ACTIVITY_SUCCESS,
        invalid: ODC_ADDITIONAL_CANCEL_ACTIVITY_INVALID,
        error: ODC_ADDITIONAL_CANCEL_ACTIVITY_ERROR
    }
    return callApi(request, actions)
}

export function continueProcess(data) {
    const request = {
        url: data.url,
        method: data.method,
        data: data.data
    }
    const actions = {
        request: ODC_ADDITIONAL_CONTINUE_PROCESS_REQUEST,
        success: ODC_ADDITIONAL_CONTINUE_PROCESS_SUCCESS,
        invalid: ODC_ADDITIONAL_CONTINUE_PROCESS_INVALID,
        error: ODC_ADDITIONAL_CONTINUE_PROCESS_ERROR
    }
    return callApi(request, actions)
}

export function registerContinueProcessActivity(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        ...data.data
    }
    const request = {
        url: data.url,
        method: data.method,
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_REQUEST,
        success: ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_SUCCESS,
        invalid: ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_INVALID,
        error: ODC_ADDITIONAL_REGISTER_CONTINUE_PROCESS_ACTIVITY_ERROR
    }
    return callApi(request, actions)
}

export function getPendingProcessesActivation() {
    const request = {
        url: `${ URL_ODC }/Adicionales/SolicitudesPorAutorizar`,
        method: 'GET',
        data: null
    }
    const actions = {
        request: ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_REQUEST,
        success: ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_SUCCESS,
        invalid: ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_INVALID,
        error: ODC_ADDITIONAL_GET_PENDING_PROCESSES_ACTIVATION_ERROR
    }
    return callApi(request, actions)
}

export function authorizeActivationProcess(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        ...data,
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        terminalName: 'WEB',
        actividadGenericoSimpleModelRequest: {
            ...data.actividadGenericoSimpleModelRequest,
            des_usu_reg: userData.username,
            des_ter_reg: userData.ip
        }
    }
    const request = {
        url: `${ URL_ODC }/Adicionales/RegistrarActividadGenericoSimple`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_REQUEST,
        success: ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS,
        invalid: ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_INVALID,
        error: ODC_ADDITIONAL_AUTHORIZE_ACTIVATION_PROCESS_ERROR
    }
    return callApi(request, actions)
}

export function rejectActivationProcess(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        cod_agencia: userData.agencyCode,
        ...data
    }
    const request = {
        url: `${ URL_ODC }/Originacion/RegistrarActividadRechazar`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_REQUEST,
        success: ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_SUCCESS,
        invalid: ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_INVALID,
        error: ODC_ADDITIONAL_REJECT_ACTIVATION_PROCESS_ERROR
    }
    return callApi(request, actions)
}

export function getPendingProcesses() {
    const request = {
        url: `${ URL_ODC }/Adicionales/Solicitudes`,
        method: 'GET',
        data: null
    }
    const actions = {
        request: ODC_ADDITIONAL_GET_PENDING_PROCESSES_REQUEST,
        success: ODC_ADDITIONAL_GET_PENDING_PROCESSES_SUCCESS,
        invalid: ODC_ADDITIONAL_GET_PENDING_PROCESSES_INVALID,
        error: ODC_ADDITIONAL_GET_PENDING_PROCESSES_ERROR
    }
    return callApi(request, actions)
}


export function registerObservationValidated(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        ...data,
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip,
        terminalName: 'WEB',
        actividadGenericoSimpleModelRequest: {
            ...data.actividadGenericoSimpleModelRequest,
            des_usu_reg: userData.username,
            des_ter_reg: userData.ip
        }
    }
    const request = {
        url: `${ URL_ODC }/Adicionales/RegistrarActividadGenericoSimple`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_REQUEST,
        success: ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_SUCCESS,
        invalid: ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_INVALID,
        error: ODC_ADDITIONAL_REGISTER_OBSERVATION_VALIDATED_ERROR
    }
    return callApi(request, actions)
}