import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';
import { decrypt } from '../../utils/Utils';
// Actions 
export const REPORT_ORIGINATED_CARDS_REQUEST = 'REPORT_ORIGINATED_CARDS_REQUEST'; 
export const REPORT_ORIGINATED_CARDS_SUCCESS = 'REPORT_ORIGINATED_CARDS_SUCCESS'; 
export const REPORT_ORIGINATED_CARDS_INVALID = 'REPORT_ORIGINATED_CARDS_INVALID'; 
export const REPORT_ORIGINATED_CARDS_ERROR = 'REPORT_ORIGINATED_CARDS_ERROR';

export const REPORT_ACTIVATED_CARDS_REQUEST = 'REPORT_ACTIVATED_CARDS_REQUEST'; 
export const REPORT_ACTIVATED_CARDS_SUCCESS = 'REPORT_ACTIVATED_CARDS_SUCCESS'; 
export const REPORT_ACTIVATED_CARDS_INVALID = 'REPORT_ACTIVATED_CARDS_INVALID'; 
export const REPORT_ACTIVATED_CARDS_ERROR = 'REPORT_ACTIVATED_CARDS_ERROR';

export const REPORT_ACTIVATED_CARDS_EXPORT_EXCEL_REQUEST = 'REPORT_ACTIVATED_CARDS_EXPORT_EXCEL_REQUEST'; 
export const REPORT_ACTIVATED_CARDS_EXPORT_EXCEL_SUCCESS = 'REPORT_ACTIVATED_CARDS_EXPORT_EXCEL_SUCCESS'; 
export const REPORT_ACTIVATED_CARDS_EXPORT_EXCEL_INVALID = 'REPORT_ACTIVATED_CARDS_EXPORT_EXCEL_INVALID'; 
export const REPORT_ACTIVATED_CARDS_EXPORT_EXCEL_ERROR = 'REPORT_ACTIVATED_CARDS_EXPORT_EXCEL_ERROR';

export const REPORT_ORIGINATED_CARDS_INDICATORS_REQUEST = 'REPORT_ORIGINATED_CARDS_INDICATORS_REQUEST'; 
export const REPORT_ORIGINATED_CARDS_INDICATORS_SUCCESS = 'REPORT_ORIGINATED_CARDS_INDICATORS_SUCCESS'; 
export const REPORT_ORIGINATED_CARDS_INDICATORS_INVALID = 'REPORT_ORIGINATED_CARDS_INDICATORS_INVALID'; 
export const REPORT_ORIGINATED_CARDS_INDICATORS_ERROR = 'REPORT_ORIGINATED_CARDS_INDICATORS_ERROR';

export const REPORT_ORIGINATION_OPERATIONS_REQUEST = 'REPORT_ORIGINATION_OPERATIONS_REQUEST'; 
export const REPORT_ORIGINATION_OPERATIONS_SUCCESS = 'REPORT_ORIGINATION_OPERATIONS_SUCCESS'; 
export const REPORT_ORIGINATION_OPERATIONS_INVALID = 'REPORT_ORIGINATION_OPERATIONS_INVALID'; 
export const REPORT_ORIGINATION_OPERATIONS_ERROR = 'REPORT_ORIGINATION_OPERATIONS_ERROR';

export const REPORT_ORIGINATION_OPERATIONS_SAE_REQUEST = 'REPORT_ORIGINATION_OPERATIONS_SAE_REQUEST'; 
export const REPORT_ORIGINATION_OPERATIONS_SAE_SUCCESS = 'REPORT_ORIGINATION_OPERATIONS_SAE_SUCCESS'; 
export const REPORT_ORIGINATION_OPERATIONS_SAE_INVALID = 'REPORT_ORIGINATION_OPERATIONS_SAE_INVALID'; 
export const REPORT_ORIGINATION_OPERATIONS_SAE_ERROR = 'REPORT_ORIGINATION_OPERATIONS_SAE_ERROR'; 

export const REPORT_ORIGINATIONS_OPERATIONS_SAE_REQUEST = 'REPORT_ORIGINATIONS_OPERATIONS_SAE_REQUEST'; 
export const REPORT_ORIGINATIONS_OPERATIONS_SAE_SUCCESS = 'REPORT_ORIGINATIONS_OPERATIONS_SAE_SUCCESS'; 
export const REPORT_ORIGINATIONS_OPERATIONS_SAE_INVALID = 'REPORT_ORIGINATIONS_OPERATIONS_SAE_INVALID'; 
export const REPORT_ORIGINATIONS_OPERATIONS_SAE_ERROR = 'REPORT_ORIGINATIONS_OPERATIONS_SAE_ERROR'; 

export const REPORT_ORIGINATION_OPERATIONS_INDICATORS_REQUEST = 'REPORT_ORIGINATION_OPERATIONS_INDICATORS_REQUEST'; 
export const REPORT_ORIGINATION_OPERATIONS_INDICATORS_SUCCESS = 'REPORT_ORIGINATION_OPERATIONS_INDICATORS_SUCCESS'; 
export const REPORT_ORIGINATION_OPERATIONS_INDICATORS_INVALID = 'REPORT_ORIGINATION_OPERATIONS_INDICATORS_INVALID'; 
export const REPORT_ORIGINATION_OPERATIONS_INDICATORS_ERROR = 'REPORT_ORIGINATION_OPERATIONS_INDICATORS_ERROR'; 

export const REPORT_CLIENT_CONSULT_REQUEST = 'REPORT_CLIENT_CONSULT_REQUEST';
export const REPORT_CLIENT_CONSULT_SUCCESS = 'REPORT_CLIENT_CONSULT_SUCCESS'; 
export const REPORT_CLIENT_CONSULT_INVALID = 'REPORT_CLIENT_CONSULT_INVALID'; 
export const REPORT_CLIENT_CONSULT_ERROR = 'REPORT_CLIENT_CONSULT_ERROR';

export const REPORT_DAILY_DOCUMENT_REQUEST = ' REPORT_DAILY_DOCUMENT_REQUEST';
export const REPORT_DAILY_DOCUMENT_SUCCESS = ' REPORT_DAILY_DOCUMENT_SUCCESS';
export const REPORT_DAILY_DOCUMENT_INVALID = ' REPORT_DAILY_DOCUMENT_INVALID';
export const REPORT_DAILY_DOCUMENT_ERROR = ' REPORT_DAILY_DOCUMENT_ERROR';


export function getReportOriginationOperationsSae(data, extra=null){
    return (dispatch) => {
        let sendData = JSON.stringify({
            //solicitudesReporteActivacionSae:{
                fec_inicial: data.originacionOperacionesSae.fec_inicial,
                fec_final: data.originacionOperacionesSae.fec_final,
                cod_agencia: data.originacionOperacionesSae.cod_agencia? data.originacionOperacionesSae.cod_agencia: 0,
                cod_tipo_desembolso: 0
            //}
        });

        //Begin Request
        dispatch({type: REPORT_ORIGINATION_OPERATIONS_SAE_REQUEST});
        return axios
            .post(`${URL_ODC}/Originacion/SolicitudesReporteActivarSAE`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REPORT_ORIGINATION_OPERATIONS_SAE_SUCCESS, data: response.data });
                    }
                    else{
                        dispatch({ 
                            type: REPORT_ORIGINATION_OPERATIONS_SAE_INVALID, 
                            data: [], 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response;
                
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: REPORT_ORIGINATION_OPERATIONS_SAE_ERROR, error:newError, response: response});
                return error;
            });
    };
}

export function getReportOriginationsOperationsSae(data, extra=null){
    return (dispatch) => {
        let sendData = JSON.stringify({
                cod_estado_sae: data.cod_estado_sae,
                fec_inicial: data.fec_inicial,
                hora_inicial: data.hora_inicial,
                fec_final: data.fec_final,
                hora_final: data.hora_final,
                cod_agencia: data.cod_agencia? data.cod_agencia: 0,
                cod_tipo_desembolso: 0
        });
        //Begin Request
        dispatch({type: REPORT_ORIGINATIONS_OPERATIONS_SAE_REQUEST});
        return axios
            .post(`${URL_ODC}/Originacion/SolicitudesReporteActivacionesSAE`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REPORT_ORIGINATIONS_OPERATIONS_SAE_SUCCESS, data: response.data });
                    }
                    else{
                        dispatch({ 
                            type: REPORT_ORIGINATIONS_OPERATIONS_SAE_INVALID, 
                            data: [], 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response;
                
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: REPORT_ORIGINATIONS_OPERATIONS_SAE_ERROR, error:newError, response: response});
                return error;
            });
    };
}


export function getReportOriginatedCards(data, extra=null) {
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);

        let sendData = JSON.stringify({
            tarjetasOriginadas:{
                cod_agencia: decryptData.agency? decryptData.agency.ide_agencia:0,
                cod_tipo_documento: data.tarjetasOriginadas.cod_tipo_documento,
                des_nro_documento: data.tarjetasOriginadas.des_nro_documento,
                cod_producto: data.tarjetasOriginadas.cod_producto? data.tarjetasOriginadas.cod_producto : 0,                
                cod_proceso: data.tarjetasOriginadas.cod_proceso? data.tarjetasOriginadas.cod_proceso: 0,
                cod_subproceso: data.tarjetasOriginadas.cod_subproceso? data.tarjetasOriginadas.cod_subproceso: 0,
                cod_flujo_fase_estado: data.tarjetasOriginadas.cod_flujo_fase_estado? data.tarjetasOriginadas.cod_flujo_fase_estado: 0,
                des_siglas_flujo_fase_estado: data.tarjetasOriginadas.des_siglas_flujo_fase_estado,
                des_usu_reg: data.tarjetasOriginadas.des_usu_reg,
                fec_inicial: data.tarjetasOriginadas.fec_inicial,
                fec_final: data.tarjetasOriginadas.fec_final,
                
            }
        });

        //Begin Request
        dispatch({type: REPORT_ORIGINATED_CARDS_REQUEST});
        return axios
       
            .post(`${URL_ODC}/Reportes/TarjetasOriginadas`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REPORT_ORIGINATED_CARDS_SUCCESS, data: response.data, extra: extra });
                    }
                    else{
                        dispatch({ 
                            type: REPORT_ORIGINATED_CARDS_INVALID, 
                            data: [], 
                            error: response.errorMessage,
                            response: true 
                        });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: REPORT_ORIGINATED_CARDS_ERROR, error:newError, response: response});
                return error;
            });
    };
}

export function getReportActivatedCards(data, extra=null) {
    return (dispatch) => {
        let sendData = JSON.stringify({
            tarjetasActivadas:{
                cod_agencia: data.tarjetasActivadas.cod_agencia? data.tarjetasActivadas.cod_agencia: 0, 
                cod_proceso: data.tarjetasActivadas.cod_proceso? data.tarjetasActivadas.cod_proceso: 0,
                cod_subproceso: data.tarjetasActivadas.cod_subproceso? data.tarjetasActivadas.cod_subproceso: 0,
                fec_inicial: data.tarjetasActivadas.fec_inicial,
                fec_final: data.tarjetasActivadas.fec_final,
            }
        });

        // console.warn(sendData)
        //Begin Request
        dispatch({type: REPORT_ACTIVATED_CARDS_REQUEST});
        return axios
       
            .post(`${URL_ODC}/Reportes/TarjetasActivadas`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REPORT_ACTIVATED_CARDS_SUCCESS, data: response.data, extra: extra });
                    }
                    else{
                        dispatch({ 
                            type: REPORT_ACTIVATED_CARDS_INVALID, 
                            data: [], 
                            error: response.errorMessage,
                            response: true 
                        });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: REPORT_ACTIVATED_CARDS_ERROR, error:newError, response: response});
                return error;
            });
    };
}

export function getReportActivatedCardsPaginated(data, extra=null) {
    return (dispatch) => {
        let sendData = JSON.stringify({
            tarjetasActivadasPaginado:{
                cod_agencia: data.tarjetasActivadas.cod_agencia? data.tarjetasActivadas.cod_agencia: 0, 
                cod_proceso: data.tarjetasActivadas.cod_proceso? data.tarjetasActivadas.cod_proceso: 0,
                cod_subproceso: data.tarjetasActivadas.cod_subproceso? data.tarjetasActivadas.cod_subproceso: 0,
                fec_inicial: data.tarjetasActivadas.fec_inicial,
                fec_final: data.tarjetasActivadas.fec_final,
                num_pagina_actual: data.tarjetasActivadas.num_pagina_actual,
                num_filas_por_pagina: data.tarjetasActivadas.num_filas_por_pagina
            }
        });

        // console.warn(sendData)
        //Begin Request
        dispatch({type: REPORT_ACTIVATED_CARDS_REQUEST});
        return axios
       
            .post(`${URL_ODC}/Reportes/TarjetasActivadasPaginado`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REPORT_ACTIVATED_CARDS_SUCCESS, data: response.data, extra: extra });
                    }
                    else{
                        dispatch({ 
                            type: REPORT_ACTIVATED_CARDS_INVALID, 
                            data: [], 
                            error: response.errorMessage,
                            response: true 
                        });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: REPORT_ACTIVATED_CARDS_ERROR, error:newError, response: response});
                return error;
            });
    };
}

export function getReportActivatedCardsExcel(data, extra=null) {
        let sendData = JSON.stringify({
            exportarReporteTarjetasActivadas:{
                cod_agencia: data.exportarReporteTarjetasActivadas.cod_agencia? data.exportarReporteTarjetasActivadas.cod_agencia: 0, 
                cod_proceso: data.exportarReporteTarjetasActivadas.cod_proceso? data.exportarReporteTarjetasActivadas.cod_proceso: 0,
                cod_subproceso: data.exportarReporteTarjetasActivadas.cod_subproceso? data.exportarReporteTarjetasActivadas.cod_subproceso: 0,
                fec_inicial: data.exportarReporteTarjetasActivadas.fec_inicial,
                fec_final: data.exportarReporteTarjetasActivadas.fec_final,
            }
        });
        let nameFile = data.exportarReporteTarjetasActivadas.nameFile
        //Begin Request
        // dispatch({type: REPORT_ACTIVATED_CARDS_EXPORT_EXCEL_REQUEST});
        return axios
       
            .post(`${URL_ODC}/Exportar/TarjetasActivadasExportarExcel`, sendData, {
                responseType: 'blob',
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.status===200){
                    const url = window.URL.createObjectURL(new Blob([response.data]));
                    let link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', `${nameFile}.xlsx`);
                    link.click();
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                // let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                // let response = error.response? true : false;
                // dispatch({type: REPORT_ACTIVATED_CARDS_ERROR, error:newError, response: response});
                return error;
            });
}

export function getReportOriginatedCardsIndicators(data, extra=null){
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);

        let sendData = JSON.stringify({
            tarjetasOriginadasIndicadores:{
                fec_inicial: data.tarjetasOriginadasIndicadores.fec_inicial,
                fec_final: data.tarjetasOriginadasIndicadores.fec_final,
                des_usu_sesion: decryptData.username
            }
        });
        // console.warn(sendData);
        //Begin Request
        dispatch({type: REPORT_ORIGINATED_CARDS_INDICATORS_REQUEST});
        return axios
            .post(`${URL_ODC}/Reportes/TarjetasOriginadasIndicadores`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REPORT_ORIGINATED_CARDS_INDICATORS_SUCCESS, data: response.data.tarjetasOriginadasIndicadores, extra: extra });
                    }
                    else{
                        dispatch({ 
                            type: REPORT_ORIGINATED_CARDS_INDICATORS_INVALID, 
                            data: [], 
                            error: response.errorMessage, 
                            response: true 
                        });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: REPORT_ORIGINATED_CARDS_INDICATORS_ERROR, error:newError, response: response});
                return error;
            });
    };
}

export function getReportOriginationOperations(data, extra=null){
    return (dispatch) => {
        let sendData = JSON.stringify({
            originacionOperaciones:{
                cod_tipo_documento: data.originacionOperaciones.cod_tipo_documento,
                des_nro_documento: data.originacionOperaciones.des_nro_documento,
                cod_proceso: data.originacionOperaciones.cod_proceso,
                cod_subproceso: data.originacionOperaciones.cod_subproceso,
                cod_flujo_fase_estado: data.originacionOperaciones.cod_flujo_fase_estado,
                cod_agencia: data.originacionOperaciones.cod_agencia,
                cod_agencia_emboce: data.originacionOperaciones.cod_agencia_emboce,
                fec_inicial: data.originacionOperaciones.fec_inicial,
                fec_final: data.originacionOperaciones.fec_final,
            }, 
        });

        //Begin Request
        dispatch({type: REPORT_ORIGINATION_OPERATIONS_REQUEST});
        return axios
            .post(`${URL_ODC}/Reportes/OriginacionOperaciones`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REPORT_ORIGINATION_OPERATIONS_SUCCESS, data: response.data });
                    }
                    else{
                        dispatch({ 
                            type: REPORT_ORIGINATION_OPERATIONS_INVALID, 
                            data: [], 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
                
                
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: REPORT_ORIGINATION_OPERATIONS_ERROR, error:newError, response: response});
                return error;
            });
    };
}

export function getReportOriginationOperationsIndicators(data, extra=null){
    return (dispatch) => {

        let sendData = JSON.stringify({
            originacionOperacionesIndicadores:{
                fec_inicial: data.originacionOperacionesIndicadores.fec_inicial,
                fec_final: data.originacionOperacionesIndicadores.fec_final,
            }
        });

        //Begin Request
        dispatch({type: REPORT_ORIGINATION_OPERATIONS_INDICATORS_REQUEST});
        return axios
            .post(`${URL_ODC}/Reportes/OriginacionOperacionesIndicadores`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REPORT_ORIGINATION_OPERATIONS_INDICATORS_SUCCESS, data: response.data.originacionOperacionesIndicadores, extra: extra });
                    }
                    else{
                        dispatch({ 
                            type: REPORT_ORIGINATION_OPERATIONS_INDICATORS_INVALID, 
                            data: [], 
                            error: response.errorMessage, 
                            response: true 
                        });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: REPORT_ORIGINATION_OPERATIONS_INDICATORS_ERROR, error:newError, response: response});
                return error;
            });
    };
}

export function getReportClienteConsult(data, extra=null){
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);

        let sendData = JSON.stringify({
            tarjetasOriginadas:{
                cod_tipo_documento: data.tarjetasOriginadas.cod_tipo_documento,
                des_nro_documento: data.tarjetasOriginadas.des_nro_documento,
                cod_producto: data.tarjetasOriginadas.cod_producto? data.tarjetasOriginadas.cod_producto : 0,
                des_siglas_flujo_fase_estado: data.tarjetasOriginadas.des_siglas_flujo_fase_estado,
                des_usu_reg: data.tarjetasOriginadas.des_usu_reg,
                fec_inicial: data.tarjetasOriginadas.fec_inicial,
                fec_final: data.tarjetasOriginadas.fec_final,
                cod_agencia: decryptData.agency? decryptData.agency.ide_agencia:0,
            }
        });

        // console.warn(sendData)
        //Begin Request
        dispatch({type: REPORT_CLIENT_CONSULT_REQUEST});
        return axios
            .post(`${URL_ODC}/Reportes/TarjetasOriginadas`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REPORT_CLIENT_CONSULT_SUCCESS, data: response.data, extra: extra });
                    }
                    else{
                        dispatch({ 
                            type: REPORT_CLIENT_CONSULT_INVALID, 
                            data: [], 
                            error: response.errorMessage, 
                            response: true 
                        });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: REPORT_CLIENT_CONSULT_ERROR, error:newError, response: response});
                return error;
            });
    };
}

export function getReportDailyDocument(data, extra=null){
    return (dispatch) => {
        let sendData = JSON.stringify({
            originacionCuadreDocDigitales:{
                fec_inicial: data.originacionCuadreDocDigitales.fec_inicial,
                fec_final: data.originacionCuadreDocDigitales.fec_final,
            }
        });

        //Begin Request
        dispatch({type: REPORT_DAILY_DOCUMENT_REQUEST});
        return axios
            .post(`${URL_ODC}/Reportes/OriginacionCuadreDocDigitales`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REPORT_DAILY_DOCUMENT_SUCCESS, data: response.data, extra: extra });
                    }
                    else{
                        dispatch({
                            type: REPORT_DAILY_DOCUMENT_INVALID,
                            data: [],
                            error: response.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE;
                let response = error.response? true : false;
                dispatch({type: REPORT_DAILY_DOCUMENT_ERROR, error:newError, response: response});
                return error;
            });
    };
}

