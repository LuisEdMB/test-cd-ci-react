import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const JOB_TITLE_REQUEST = 'JOB_TITLE_REQUEST'; 
export const JOB_TITLE_SUCCESS = 'JOB_TITLE_SUCCESS'; 
export const JOB_TITLE_INVALID = 'JOB_TITLE_INVALID'; 
export const JOB_TITLE_ERROR = 'JOB_TITLE_ERROR'; 

export function getJobTitle() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: JOB_TITLE_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=170000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: JOB_TITLE_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: JOB_TITLE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: JOB_TITLE_ERROR, error:newError, response: response});
                return error;
            });
    };
}
