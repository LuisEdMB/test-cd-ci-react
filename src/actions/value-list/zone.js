import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';
// Actions 
export const ZONE_REQUEST = 'ZONE_REQUEST'; 
export const ZONE_SUCCESS = 'ZONE_SUCCESS'; 
export const ZONE_INVALID = 'ZONE_INVALID';
export const ZONE_ERROR = 'ZONE_ERROR'; 

export function getZone() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: ZONE_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=140000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: ZONE_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: ZONE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ZONE_ERROR, error:newError, response: response});
                return error;
            });
    };
}
