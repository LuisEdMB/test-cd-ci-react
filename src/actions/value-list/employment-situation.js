import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const EMPLOYMENT_SITUATION_REQUEST = 'EMPLOYMENT_SITUATION_REQUEST'; 
export const EMPLOYMENT_SITUATION_SUCCESS = 'EMPLOYMENT_SITUATION_SUCCESS'; 
export const EMPLOYMENT_SITUATION_INVALID = 'EMPLOYMENT_SITUATION_INVALID'; 
export const EMPLOYMENT_SITUATION_ERROR = 'EMPLOYMENT_SITUATION_ERROR'; 

export function getEmploymentSituation() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: EMPLOYMENT_SITUATION_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=160000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: EMPLOYMENT_SITUATION_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({
                            type:EMPLOYMENT_SITUATION_INVALID, 
                            data:null, 
                            error:response.data.errorMessage, 
                            response:true,
                        });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: EMPLOYMENT_SITUATION_ERROR, error:newError, response: response});
                return error;
            });
    };
}
