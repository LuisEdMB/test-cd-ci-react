import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';

// Actions 
export const CREDIT_CARD_BIN_REQUEST = 'CREDIT_CARD_BIN_REQUEST'; 
export const CREDIT_CARD_BIN_SUCCESS = 'CREDIT_CARD_BIN_SUCCESS'; 
export const CREDIT_CARD_BIN_INVALID = 'CREDIT_CARD_BIN_INVALID'; 
export const CREDIT_CARD_BIN_ERROR = 'CREDIT_CARD_BIN_ERROR'; 

export function getCreditCardBin() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: CREDIT_CARD_BIN_REQUEST});
        return axios
            .post(`${URL_ODC}/Valores/ProductoBines`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: CREDIT_CARD_BIN_SUCCESS, data: response.data.productosBines });
                    }
                    else{
                        dispatch({ 
                            type: CREDIT_CARD_BIN_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: CREDIT_CARD_BIN_ERROR, error:newError, response: response});
                return error;
            });
    };
}