import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const OPERATION_TYPE_REQUEST = 'OPERATION_TYPE_REQUEST'; 
export const OPERATION_TYPE_SUCCESS = 'OPERATION_TYPE_SUCCESS'; 
export const OPERATION_TYPE_INVALID = 'OPERATION_TYPE_INVALID'; 
export const OPERATION_TYPE_ERROR = 'OPERATION_TYPE_ERROR'; 

export function getOperationType() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: OPERATION_TYPE_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=340000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: OPERATION_TYPE_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: OPERATION_TYPE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: OPERATION_TYPE_ERROR, error:newError, response: response});
                return error;
            });
    };
}
