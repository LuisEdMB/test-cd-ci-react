import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';
// Actions 
export const ACCOUNT_STATUS_REQUEST = 'ACCOUNT_STATUS_REQUEST'; 
export const ACCOUNT_STATUS_SUCCESS = 'ACCOUNT_STATUS_SUCCESS'; 
export const ACCOUNT_STATUS_INVALID = 'ACCOUNT_STATUS_INVALID'; 
export const ACCOUNT_STATUS_ERROR = 'ACCOUNT_STATUS_ERROR'; 

export function getAccountStatus() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: ACCOUNT_STATUS_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=280000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: ACCOUNT_STATUS_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: ACCOUNT_STATUS_SUCCESS, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ACCOUNT_STATUS_ERROR, error:newError, response: response});
                return error;
            });
    };
}