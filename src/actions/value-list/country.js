import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const COUNTRY_REQUEST = 'COUNTRY_REQUEST'; 
export const COUNTRY_SUCCESS = 'COUNTRY_SUCCESS'; 
export const COUNTRY_INVALID = 'COUNTRY_INVALID';
export const COUNTRY_ERROR = 'COUNTRY_ERROR'; 

export function getCountry() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: COUNTRY_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=230000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: COUNTRY_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: COUNTRY_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: COUNTRY_ERROR, error:newError, response: response});
                return error;
            });
    };
}
