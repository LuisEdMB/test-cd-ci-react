import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const ECONOMIC_ACTIVITY_REQUEST = 'ECONOMIC_ACTIVITY_REQUEST'; 
export const ECONOMIC_ACTIVITY_SUCCESS = 'ECONOMIC_ACTIVITY_SUCCESS'; 
export const ECONOMIC_ACTIVITY_INVALID = 'ECONOMIC_ACTIVITY_INVALID'; 
export const ECONOMIC_ACTIVITY_ERROR = 'ECONOMIC_ACTIVITY_ERROR'; 

export function getEconomicActivity() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: ECONOMIC_ACTIVITY_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=180000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: ECONOMIC_ACTIVITY_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({
                            type:ECONOMIC_ACTIVITY_INVALID, 
                            data:null, 
                            error:response.data.errorMessage, 
                            response:true,
                        });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ECONOMIC_ACTIVITY_ERROR, error:newError, response: response});
                return error;
            });
    };
}