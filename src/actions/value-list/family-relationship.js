import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const FAMILY_RELATIONSHIP_REQUEST = 'FAMILY_RELATIONSHIP_REQUEST'; 
export const FAMILY_RELATIONSHIP_SUCCESS = 'FAMILY_RELATIONSHIP_SUCCESS'; 
export const FAMILY_RELATIONSHIP_INVALID = 'FAMILY_RELATIONSHIP_INVALID'; 
export const FAMILY_RELATIONSHIP_ERROR = 'FAMILY_RELATIONSHIP_ERROR'; 

export function getFamilyRelationship() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: FAMILY_RELATIONSHIP_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=320000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: FAMILY_RELATIONSHIP_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({
                            type:FAMILY_RELATIONSHIP_INVALID, 
                            data:null, 
                            error:response.data.errorMessage, 
                            response:true,
                        });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: FAMILY_RELATIONSHIP_ERROR, error:newError, response: response});
                return error;
            });
    };
}
