import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const IDENTIFICATION_DOCUMENT_TYPE_REQUEST = 'IDENTIFICATION_DOCUMENT_TYPE_REQUEST'; 
export const IDENTIFICATION_DOCUMENT_TYPE_SUCCESS = 'IDENTIFICATION_DOCUMENT_TYPE_SUCCESS'; 
export const IDENTIFICATION_DOCUMENT_TYPE_INVALID = 'IDENTIFICATION_DOCUMENT_TYPE_INVALID'; 
export const IDENTIFICATION_DOCUMENT_TYPE_ERROR = 'IDENTIFICATION_DOCUMENT_TYPE_ERROR'; 

export function getIdentificationDocumentType() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: IDENTIFICATION_DOCUMENT_TYPE_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=100000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: IDENTIFICATION_DOCUMENT_TYPE_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: IDENTIFICATION_DOCUMENT_TYPE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: IDENTIFICATION_DOCUMENT_TYPE_ERROR, error:newError, response: response});
                return error;
            });
    };
}
