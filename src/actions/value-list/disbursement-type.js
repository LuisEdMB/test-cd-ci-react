import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const DISBURSEMENT_REQUEST = 'DISBURSEMENT_REQUEST'; 
export const DISBURSEMENT_SUCCESS = 'DISBURSEMENT_SUCCESS'; 
export const DISBURSEMENT_INVALID = 'DISBURSEMENT_INVALID'; 
export const DISBURSEMENT_ERROR = 'DISBURSEMENT_ERROR'; 

export function getDisbursementType() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: DISBURSEMENT_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=380000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: DISBURSEMENT_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: DISBURSEMENT_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage: SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: DISBURSEMENT_ERROR, error:newError, response: response});
                return error;
            });
    };
}


export function getDisbursementTypeCallCenter() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: DISBURSEMENT_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=380000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        let {lista_valores} = response.data
                        let lista_valores_call_center = lista_valores.filter(item => item.cod_valor !== 380002)
                        dispatch({ type: DISBURSEMENT_SUCCESS, data: lista_valores_call_center });
                    }
                    else{
                        dispatch({ 
                            type: DISBURSEMENT_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage: SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: DISBURSEMENT_ERROR, error:newError, response: response});
                return error;
            });
    };
}
