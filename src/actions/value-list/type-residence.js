import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';
// Actions 
export const TYPE_RESIDENCE_REQUEST = 'TYPE_RESIDENCE_REQUEST'; 
export const TYPE_RESIDENCE_SUCCESS = 'TYPE_RESIDENCE_SUCCESS'; 
export const TYPE_RESIDENCE_INVALID = 'TYPE_RESIDENCE_INVALID';
export const TYPE_RESIDENCE_ERROR = 'TYPE_RESIDENCE_ERROR'; 

export function getTypeResidence() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: TYPE_RESIDENCE_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=150000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: TYPE_RESIDENCE_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: TYPE_RESIDENCE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: TYPE_RESIDENCE_ERROR, error:newError, response: response});
                return error;
            });
    };
}
