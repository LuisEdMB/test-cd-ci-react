import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';
// Actions 
export const ODC_PHASE_FLOW_STATE_REQUEST = 'ODC_PHASE_FLOW_STATE_REQUEST'; 
export const ODC_PHASE_FLOW_STATE_SUCCESS = 'ODC_PHASE_FLOW_STATE_SUCCESS'; 
export const ODC_PHASE_FLOW_STATE_INVALID = 'ODC_PHASE_FLOW_STATE_INVALID'; 
export const ODC_PHASE_FLOW_STATE_ERROR = 'ODC_PHASE_FLOW_STATE_ERROR'; 

export function phaseFlowState(data){
    return (dispatch) => {
        //Begin Request
        dispatch({type: ODC_PHASE_FLOW_STATE_REQUEST});
        return new Promise((resolve, error) => {
            resolve();
        })
        .then(()=>{
            setTimeout(()=>{
                dispatch({ type: ODC_PHASE_FLOW_STATE_SUCCESS, data: data });
                return data;
            }, 2000)
        })
        .catch(error => {
            // Error Request
            let newError = error.response? error.response.data.error:SERVER_ERROR_MESSAGE; 
            let response = error.response? true : false;
            dispatch({type: ODC_PHASE_FLOW_STATE_ERROR, error:newError, response: response});
            return error;
        });
    };
};

export function phaseFlowStateOk(data) {
    return (dispatch) => {
        //Begin Request
        dispatch({type: ODC_PHASE_FLOW_STATE_REQUEST});
        return axios
            .post(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=170000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: ODC_PHASE_FLOW_STATE_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: ODC_PHASE_FLOW_STATE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_PHASE_FLOW_STATE_ERROR, error:newError, response: response});
                return error;
            });
    };
}
