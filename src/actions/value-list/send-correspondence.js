import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const SEND_CORRESPONDENCE_REQUEST = 'SEND_CORRESPONDENCE_REQUEST'; 
export const SEND_CORRESPONDENCE_SUCCESS = 'SEND_CORRESPONDENCE_SUCCESS'; 
export const SEND_CORRESPONDENCE_INVALID = 'SEND_CORRESPONDENCE_INVALID'; 
export const SEND_CORRESPONDENCE_ERROR = 'SEND_CORRESPONDENCE_ERROR'; 

export function getSendCorrespondence() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: SEND_CORRESPONDENCE_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=270000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: SEND_CORRESPONDENCE_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: SEND_CORRESPONDENCE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: SEND_CORRESPONDENCE_ERROR, error:newError, response: response});
                return error;
            });
    };
}