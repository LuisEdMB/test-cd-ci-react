import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const VIA_REQUEST = 'VIA_REQUEST'; 
export const VIA_SUCCESS = 'VIA_SUCCESS'; 
export const VIA_INVALID = 'VIA_INVALID'; 
export const VIA_ERROR = 'VIA_ERROR'; 

export function getVia() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: VIA_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=130000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: VIA_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: VIA_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: VIA_ERROR, error:newError, response: response});
                return error;
            });
    };
}
