import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';
// Actions 
export const CREDIT_CARD_COLOR_REQUEST = 'CREDIT_CARD_COLOR_REQUEST'; 
export const CREDIT_CARD_COLOR_SUCCESS = 'CREDIT_CARD_COLOR_SUCCESS'; 
export const CREDIT_CARD_COLOR_INVALID = 'CREDIT_CARD_COLOR_INVALID'; 
export const CREDIT_CARD_COLOR_ERROR = 'CREDIT_CARD_COLOR_ERROR'; 

export function getCreditCardColor() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: CREDIT_CARD_COLOR_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=250000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: CREDIT_CARD_COLOR_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({
                            type:CREDIT_CARD_COLOR_INVALID, 
                            data:null, 
                            error:response.data.errorMessage, 
                            response:true,
                        });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: CREDIT_CARD_COLOR_ERROR, error:newError, response: response});
                return error;
            });
    };
}