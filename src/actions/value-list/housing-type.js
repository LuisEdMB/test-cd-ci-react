import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';
// Actions 
export const HOUSING_TYPE_REQUEST = 'HOUSING_TYPE_REQUEST'; 
export const HOUSING_TYPE_SUCCESS = 'HOUSING_TYPE_SUCCESS'; 
export const HOUSING_TYPE_INVALID = 'HOUSING_TYPE_INVALID';
export const HOUSING_TYPE_ERROR = 'HOUSING_TYPE_ERROR'; 

export function getHousingType() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: HOUSING_TYPE_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=150000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: HOUSING_TYPE_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: HOUSING_TYPE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: HOUSING_TYPE_ERROR, error:newError, response: response});
                return error;
            });
    };
}