import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const ACADEMIC_DEGREE_REQUEST = 'ACADEMIC_DEGREE_REQUEST'; 
export const ACADEMIC_DEGREE_SUCCESS = 'ACADEMIC_DEGREE_SUCCESS'; 
export const ACADEMIC_DEGREE_ERROR = 'ACADEMIC_DEGREE_ERROR'; 
export const ACADEMIC_DEGREE_INVALID = 'ACADEMIC_DEGREE_INVALID';

export function getAcademicDegree() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: ACADEMIC_DEGREE_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=120000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: ACADEMIC_DEGREE_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: ACADEMIC_DEGREE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ACADEMIC_DEGREE_ERROR, error:newError, response: response});
                return error;
            });
    };
}