import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const REASON_EMBOSSING_REQUEST = 'REASON_EMBOSSING_REQUEST'; 
export const REASON_EMBOSSING_SUCCESS = 'REASON_EMBOSSING_SUCCESS'; 
export const REASON_EMBOSSING_INVALID = 'REASON_EMBOSSING_INVALID'; 
export const REASON_EMBOSSING_ERROR = 'REASON_EMBOSSING_ERROR'; 

export function getReasonEmbossing() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: REASON_EMBOSSING_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=240000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REASON_EMBOSSING_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: REASON_EMBOSSING_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: REASON_EMBOSSING_ERROR, error:newError, response: response});
                return error;
            });
    };
}
