import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const PAYMENT_DATE_REQUEST = 'PAYMENT_DATE_REQUEST'; 
export const PAYMENT_DATE_SUCCESS = 'PAYMENT_DATE_SUCCESS'; 
export const PAYMENT_DATE_INVALID = 'PAYMENT_DATE_INVALID'; 
export const PAYMENT_DATE_ERROR = 'PAYMENT_DATE_ERROR'; 

export function getPaymentDate() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: PAYMENT_DATE_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=190000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: PAYMENT_DATE_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: PAYMENT_DATE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: PAYMENT_DATE_ERROR, error:newError, response: response});
                return error;
            });
    };
}
