// Config
import { URL_ODC } from '../config'

// Generic API
import { callApi } from '../generic/api'

// Get All Product Colors
export const ODC_GET_ALL_PRODUCT_COLORS_REQUEST = 'ODC_GET_ALL_PRODUCT_COLORS_REQUEST'
export const ODC_GET_ALL_PRODUCT_COLORS_SUCCESS = 'ODC_GET_ALL_PRODUCT_COLORS_SUCCESS'
export const ODC_GET_ALL_PRODUCT_COLORS_INVALID = 'ODC_GET_ALL_PRODUCT_COLORS_INVALID'
export const ODC_GET_ALL_PRODUCT_COLORS_ERROR = 'ODC_GET_ALL_PRODUCT_COLORS_ERROR'

export function getAllProductColors() {
    const request = {
        url: `${ URL_ODC }/Valores/productos/colores`,
        method: 'GET'
    }
    const actions = {
        request: ODC_GET_ALL_PRODUCT_COLORS_REQUEST,
        success: ODC_GET_ALL_PRODUCT_COLORS_SUCCESS,
        invalid: ODC_GET_ALL_PRODUCT_COLORS_INVALID,
        error: ODC_GET_ALL_PRODUCT_COLORS_ERROR
    }
    return callApi(request, actions)
}