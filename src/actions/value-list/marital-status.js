import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const MARITAL_STATUS_REQUEST = 'MARITAL_STATUS_REQUEST'; 
export const MARITAL_STATUS_SUCCESS = 'MARITAL_STATUS_SUCCESS'; 
export const MARITAL_STATUS_INVALID = 'MARITAL_STATUS_INVALID'; 
export const MARITAL_STATUS_ERROR = 'MARITAL_STATUS_ERROR'; 

export function getMaritalStatus() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: MARITAL_STATUS_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=110000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: MARITAL_STATUS_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: MARITAL_STATUS_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: MARITAL_STATUS_ERROR, error:newError, response: response});
                return error;
            });
    };
}
