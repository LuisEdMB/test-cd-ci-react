import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const REASON_REPRINT_REQUEST = 'REASON_REPRINT_REQUEST'; 
export const REASON_REPRINT_SUCCESS = 'REASON_REPRINT_SUCCESS'; 
export const REASON_REPRINT_INVALID = 'REASON_REPRINT_INVALID'; 
export const REASON_REPRINT_ERROR = 'REASON_REPRINT_ERROR'; 

export function getReasonReprint() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: REASON_REPRINT_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=310000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: REASON_REPRINT_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: REASON_REPRINT_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: REASON_REPRINT_ERROR, error:newError, response: response});
                return error;
            });
    };
}
