import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const GENDER_REQUEST = 'GENDER_REQUEST'; 
export const GENDER_SUCCESS = 'GENDER_SUCCESS'; 
export const GENDER_INVALID = 'GENDER_INVALID'; 
export const GENDER_ERROR = 'GENDER_ERROR'; 

export function getGender() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: GENDER_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=220000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: GENDER_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({
                            type:GENDER_INVALID, 
                            data:null, 
                            error:response.data.errorMessage, 
                            response:true,
                        });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: GENDER_ERROR, error:newError, response: response});
                return error;
            });
    };
}
