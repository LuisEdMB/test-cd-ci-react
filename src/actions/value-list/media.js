import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE, URL_BASE_VALUE_LIST } from '../config';

// Actions 
export const MEDIA_REQUEST = 'MEDIA_REQUEST'; 
export const MEDIA_SUCCESS = 'MEDIA_SUCCESS'; 
export const MEDIA_INVALID = 'MEDIA_INVALID'; 
export const MEDIA_ERROR = 'MEDIA_ERROR'; 

export function getMedia() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: MEDIA_REQUEST});
        return axios
            .get(`${URL_ODC}/${URL_BASE_VALUE_LIST}?valor=0&grupo=300000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: MEDIA_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: MEDIA_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: MEDIA_ERROR, error:newError, response: response});
                return error;
            });
    };
}
