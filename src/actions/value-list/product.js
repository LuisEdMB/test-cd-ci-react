import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';

// Actions 
export const GET_PRODUCT_REQUEST = 'GET_PRODUCT_REQUEST'; 
export const GET_PRODUCT_SUCCESS = 'GET_PRODUCT_SUCCESS'; 
export const GET_PRODUCT_INVALID = 'GET_PRODUCT_INVALID'; 
export const GET_PRODUCT_ERROR = 'GET_PRODUCT_ERROR'; 

export function getProduct() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: GET_PRODUCT_REQUEST});
        return axios
            .post(`${URL_ODC}/Valores/ProductoBines`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: GET_PRODUCT_SUCCESS, data: response.data.productosBines });
                    }
                    else{
                        dispatch({ 
                            type: GET_PRODUCT_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: GET_PRODUCT_ERROR, error:newError, response: response});
                return error;
            });
    };
}