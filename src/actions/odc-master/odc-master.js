// Config
import { URL_ODC } from '../config'

// Generic API
import { callApi } from '../generic/api'

// Utils
import * as Utils from '../../utils/Utils'

// Preload Client
export const ODC_MASTER_PRELOAD_CLIENT_REQUEST = 'ODC_MASTER_PRELOAD_CLIENT_REQUEST'
export const ODC_MASTER_PRELOAD_CLIENT_SUCCESS = 'ODC_MASTER_PRELOAD_CLIENT_SUCCESS'
export const ODC_MASTER_PRELOAD_CLIENT_INVALID = 'ODC_MASTER_PRELOAD_CLIENT_INVALID'
export const ODC_MASTER_PRELOAD_CLIENT_ERROR = 'ODC_MASTER_PRELOAD_CLIENT_ERROR'

// Preload Additional Client
export const ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_REQUEST = 'ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_REQUEST'
export const ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_SUCCESS = 'ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_SUCCESS'
export const ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_INVALID = 'ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_INVALID'
export const ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_ERROR = 'ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_ERROR'

// Register - Update Client
export const ODC_MASTER_REGISTER_UPDATE_CLIENT_REQUEST = 'ODC_MASTER_REGISTER_UPDATE_CLIENT_REQUEST'
export const ODC_MASTER_REGISTER_UPDATE_CLIENT_SUCCESS = 'ODC_MASTER_REGISTER_UPDATE_CLIENT_SUCCESS'
export const ODC_MASTER_REGISTER_UPDATE_CLIENT_INVALID = 'ODC_MASTER_REGISTER_UPDATE_CLIENT_INVALID'
export const ODC_MASTER_REGISTER_UPDATE_CLIENT_ERROR = 'ODC_MASTER_REGISTER_UPDATE_CLIENT_ERROR'

// Register Account
export const ODC_MASTER_REGISTER_ACCOUNT_REQUEST = 'ODC_MASTER_REGISTER_ACCOUNT_REQUEST'
export const ODC_MASTER_REGISTER_ACCOUNT_SUCCESS = 'ODC_MASTER_REGISTER_ACCOUNT_SUCCESS'
export const ODC_MASTER_REGISTER_ACCOUNT_INVALID = 'ODC_MASTER_REGISTER_ACCOUNT_INVALID'
export const ODC_MASTER_REGISTER_ACCOUNT_ERROR = 'ODC_MASTER_REGISTER_ACCOUNT_ERROR'

// Register Credit Card
export const ODC_MASTER_REGISTER_CREDIT_CARD_REQUEST = 'ODC_MASTER_REGISTER_CREDIT_CARD_REQUEST'
export const ODC_MASTER_REGISTER_CREDIT_CARD_SUCCESS = 'ODC_MASTER_REGISTER_CREDIT_CARD_SUCCESS'
export const ODC_MASTER_REGISTER_CREDIT_CARD_INVALID = 'ODC_MASTER_REGISTER_CREDIT_CARD_INVALID'
export const ODC_MASTER_REGISTER_CREDIT_CARD_ERROR = 'ODC_MASTER_REGISTER_CREDIT_CARD_ERROR'

// Register Credit Card for External
export const ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_REQUEST = 'ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_REQUEST'
export const ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_SUCCESS = 'ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_SUCCESS'
export const ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_INVALID = 'ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_INVALID'
export const ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_ERROR = 'ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_ERROR'

// Update Blocking Code Credit Card
export const ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_REQUEST = 'ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_REQUEST'
export const ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_SUCCESS = 'ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_SUCCESS'
export const ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_INVALID = 'ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_INVALID'
export const ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_ERROR = 'ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_ERROR'

// Update Blocking Code Credit Card for External
export const ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_REQUEST = 'ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_REQUEST'
export const ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_SUCCESS = 'ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_SUCCESS'
export const ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_INVALID = 'ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_INVALID'
export const ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_ERROR = 'ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_ERROR'

export function preloadClient(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_tipo_documento: data.documentType,
        des_nro_documento: data.documentNumber,
        nom_actividad: data.activityName,
        cod_flujo_fase_estado: data.phaseCode,
        cod_etapa_maestro: data.masterStageCode,
        cod_tipo_cliente: data.clientTypeCode,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip
    }
    const request = {
        url: `${ URL_ODC }/api/Maestro/Cliente/PrecargaCliente`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_MASTER_PRELOAD_CLIENT_REQUEST,
        success: ODC_MASTER_PRELOAD_CLIENT_SUCCESS,
        invalid: ODC_MASTER_PRELOAD_CLIENT_INVALID,
        error: ODC_MASTER_PRELOAD_CLIENT_ERROR
    }
    return callApi(request, actions)
}

export function preloadAdditionalClient(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cod_tipo_documento_adicional: data.additionalDocumentTypeAux,
        des_nro_documento_adicional: data.additionalDocumentNumber,
        cod_tipo_documento_titular: data.holderDocumentTypeAux,
        des_nro_documento_titular: data.holderDocumentNumber,
        cod_tipo_cliente: data.clientTypeCode,
        cod_etapa_maestro: data.masterStageCode,
        fec_reg: userData.currentlyDate,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip
    }
    const request = {
        url: `${ URL_ODC }/api/Maestro/Cliente/Adicional/PrecargaCliente`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_REQUEST,
        success: ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_SUCCESS,
        invalid: ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_INVALID,
        error: ODC_MASTER_PRELOAD_ADDITIONAL_CLIENT_ERROR
    }
    return callApi(request, actions)
}

export function registerUpdateClient(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        nom_actividad: data.activityName,
        cod_flujo_fase_estado: data.phaseCode,
        cod_etapa_maestro: data.masterStageCode,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip
    }
    const request = {
        url: `${ URL_ODC }/api/Maestro/Cliente/RegistraActualiza`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_MASTER_REGISTER_UPDATE_CLIENT_REQUEST,
        success: ODC_MASTER_REGISTER_UPDATE_CLIENT_SUCCESS,
        invalid: ODC_MASTER_REGISTER_UPDATE_CLIENT_INVALID,
        error: ODC_MASTER_REGISTER_UPDATE_CLIENT_ERROR
    }
    return callApi(request, actions)
}

export function registerAccount(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        nom_actividad: data.activityName,
        cod_flujo_fase_estado: data.phaseCode,
        cod_etapa_maestro: data.masterStageCode,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip
    }
    const request = {
        url: `${ URL_ODC }/api/Maestro/Cuenta/RegistrarCuenta`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_MASTER_REGISTER_ACCOUNT_REQUEST,
        success: ODC_MASTER_REGISTER_ACCOUNT_SUCCESS,
        invalid: ODC_MASTER_REGISTER_ACCOUNT_INVALID,
        error: ODC_MASTER_REGISTER_ACCOUNT_ERROR
    }
    return callApi(request, actions)
}

export function registerCreditCard(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        nom_actividad: data.activityName,
        cod_flujo_fase_estado: data.phaseCode,
        cod_etapa_maestro: data.masterStageCode,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip
    }
    const request = {
        url: `${ URL_ODC }/api/Maestro/Cuenta/RegistrarTarjeta`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_MASTER_REGISTER_CREDIT_CARD_REQUEST,
        success: ODC_MASTER_REGISTER_CREDIT_CARD_SUCCESS,
        invalid: ODC_MASTER_REGISTER_CREDIT_CARD_INVALID,
        error: ODC_MASTER_REGISTER_CREDIT_CARD_ERROR
    }
    return callApi(request, actions)
}

export function registerCreditCardForExternal(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        nom_actividad: data.activityName,
        cod_flujo_fase_estado: data.phaseCode,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip
    }
    const request = {
        url: `${ URL_ODC }/api/Maestro/Cuenta/RegistrarTarjetaExt`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_REQUEST,
        success: ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_SUCCESS,
        invalid: ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_INVALID,
        error: ODC_MASTER_REGISTER_CREDIT_CARD_FOR_EXTERNAL_ERROR
    }
    return callApi(request, actions)
}

export function updateBlockingCodeCreditCard(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cuenta: data.accountNumber,
        token: data.token,
        bloqueo: data.blockingCode,
        nom_actividad: data.activityName,
        cod_flujo_fase_estado: data.phaseCode,
        cod_etapa_maestro: data.masterStageCode,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip
    }
    const request = {
        url: `${ URL_ODC }/api/Maestro/Cuenta/BloquearTarjeta`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_REQUEST,
        success: ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_SUCCESS,
        invalid: ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_INVALID,
        error: ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_ERROR
    }
    return callApi(request, actions)
}

export function updateBlockingCodeCreditCardForExternal(data) {
    const userData = Utils.getDataUserSession()
    const sendData = {
        cod_solicitud: data.solicitudeCode,
        cod_solicitud_completa: data.completeSolicitudeCode,
        cuenta: data.accountNumber,
        token: data.token,
        bloqueo: data.blockingCode,
        nom_actividad: data.activityName,
        cod_flujo_fase_estado: data.phaseCode,
        des_usu_reg: userData.username,
        des_ter_reg: userData.ip
    }
    const request = {
        url: `${ URL_ODC }/api/Maestro/Cuenta/BloquearTarjetaExt`,
        method: 'POST',
        data: sendData
    }
    const actions = {
        request: ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_REQUEST,
        success: ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_SUCCESS,
        invalid: ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_INVALID,
        error: ODC_MASTER_UPDATE_BLOCKING_CODE_CREDIT_CARD_FOR_EXTERNAL_ERROR
    }
    return callApi(request, actions)
}