import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';

// Actions 
export const BUSINESS_REQUEST = 'BUSINESS_REQUEST'; 
export const BUSINESS_SUCCESS = 'BUSINESS_SUCCESS'; 
export const BUSINESS_INVALID = 'BUSINESS_INVALID'; 
export const BUSINESS_ERROR = 'BUSINESS_ERROR'; 

export const BUSINESS_FILTER_REQUEST = 'BUSINESS_FILTER_REQUEST'; 
export const BUSINESS_FILTER_SUCCESS = 'BUSINESS_FILTER_SUCCESS'; 
export const BUSINESS_FILTER_INVALID = 'BUSINESS_FILTER_INVALID'; 
export const BUSINESS_FILTER_ERROR = 'BUSINESS_FILTER_ERROR'; 

export function getBusiness() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: BUSINESS_REQUEST});
        return axios
            .get(`${URL_ODC}/Valores/0/210000`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: BUSINESS_SUCCESS, data: response.data.lista_valores });
                    }
                    else{
                        dispatch({ 
                            type: BUSINESS_INVALID, 
                            data: [], 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: BUSINESS_ERROR, error:newError, response: response});
                return error;
            });
    };
}

export function getFilterByNameBusinessOrRuc(nameBusiness="", ruc="") {
    return (dispatch) => {
        //Begin Request
        dispatch({type: BUSINESS_FILTER_REQUEST});
        return axios
            .post(`${URL_ODC}/Valores/Empresas?nomEmpresa=${nameBusiness}&rucEmp=${ruc}`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: BUSINESS_FILTER_SUCCESS, data: response.data.empresas });
                    }
                    else{
                        dispatch({ 
                            type: BUSINESS_FILTER_INVALID, 
                            data: [], 
                            error: response.errorMessage, 
                            response: true });
                        return response;
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: BUSINESS_FILTER_ERROR, error:newError, response: response});
                return error;
            });
    };
}
