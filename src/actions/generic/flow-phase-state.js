import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';
// Actions 
export const GET_REPORT_FLOW_PHASE_STATE_REQUEST = 'GET_REPORT_FLOW_PHASE_STATE_REQUEST'; 
export const GET_REPORT_FLOW_PHASE_STATE_SUCCESS = 'GET_REPORT_FLOW_PHASE_STATE_SUCCESS'; 
export const GET_REPORT_FLOW_PHASE_STATE_INVALID = 'GET_REPORT_FLOW_PHASE_STATE_INVALID';
export const GET_REPORT_FLOW_PHASE_STATE_ERROR = 'GET_REPORT_FLOW_PHASE_STATE_ERROR'; 

export function getFlowPhaseState(cod_reporte=0) {
    return (dispatch) => {
        //Begin Request
        dispatch({type: GET_REPORT_FLOW_PHASE_STATE_REQUEST});
        return axios
            .get(`${URL_ODC}/Valores/Reporte/FlujoFaseEstado?cod_reporte=${cod_reporte}`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: GET_REPORT_FLOW_PHASE_STATE_SUCCESS, data: response.data.flujoFaseEstado });
                    }
                    else{
                        dispatch({ 
                            type: GET_REPORT_FLOW_PHASE_STATE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage: SERVER_ERROR_MESSAGE; 
                let response = error.response? true: false;
                dispatch({type: GET_REPORT_FLOW_PHASE_STATE_ERROR, error:newError, response: response});
                return error;
            });
    };
}
