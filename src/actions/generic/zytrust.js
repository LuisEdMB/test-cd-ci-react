import axios from 'axios'
import {
    SERVER_ERROR_MESSAGE,
    URL_ZYTRUST,
    URL_VALIDATE_ZYTRUST
} from '../config'
import {
    decrypt
} from '../../utils/Utils'
// import { de } from 'date-fns/locale'
// Actions - Service
export const BIOMETRIC_ZYTRUST_SERVICE_REQUEST = 'BIOMETRIC_ZYTRUST_SERVICE_REQUEST'
export const BIOMETRIC_ZYTRUST_SERVICE_SUCCESS = 'BIOMETRIC_ZYTRUST_SERVICE_SUCCESS'
export const BIOMETRIC_ZYTRUST_SERVICE_INVALID = 'BIOMETRIC_ZYTRUST_SERVICE_INVALID'
export const BIOMETRIC_ZYTRUST_SERVICE_ERROR = 'BIOMETRIC_ZYTRUST_SERVICE_ERROR'
// Actions - Validate
export const VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_REQUEST = 'VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_REQUEST'
export const VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_SUCCESS = 'VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_SUCCESS'
export const VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_INVALID = 'VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_INVALID'
export const VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_ERROR = 'VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_ERROR'

const httpClient = axios.create();
//httpClient.defaults.timeout = 30000;

export function biometricZytrustService(data) {
    return (dispatch) => {
        //Begin Request - Demo

        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const {
            activeDirectory
        } = decryptData;

        const {
            nuDocCliente,
            tiDocCliente
        } = data;
        const nuDocUsuario = activeDirectory ? activeDirectory.employeeID ? activeDirectory.employeeID : "" : "";
        const tiDocUsuario = "1";

        let xmlToSend = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wsi='http://wsi.client.match.bio.zy.com/'>";
        xmlToSend += "  <soapenv:Header/>";
        xmlToSend += "  <soapenv:Body>";
        xmlToSend += "   <wsi:autenticacionBiometrica>";
        xmlToSend += "     <inputBean>";
        xmlToSend += "        <coProceso>1234</coProceso>";
        xmlToSend += "        <ipConsulta>192.168.54.128</ipConsulta>";
        xmlToSend += "        <nuDocCliente>" + nuDocCliente + "</nuDocCliente>";
        xmlToSend += "        <nuDocUsuario>" + nuDocUsuario + "</nuDocUsuario>";
        xmlToSend += "        <tiDocCliente>" + tiDocCliente + "</tiDocCliente>";
        xmlToSend += "        <tiDocUsuario>" + tiDocUsuario + "</tiDocUsuario>";
        xmlToSend += "      </inputBean>";
        xmlToSend += "   </wsi:autenticacionBiometrica>";
        xmlToSend += "  </soapenv:Body>";
        xmlToSend += "</soapenv:Envelope>";

        dispatch({
            type: BIOMETRIC_ZYTRUST_SERVICE_REQUEST
        });
        return httpClient
            .post(`${URL_ZYTRUST}`, xmlToSend, {
                headers: {
                    'Content-Type': 'text/xml'
                }
            })
            .then(response => {
                // XML - Object                
                let xml = response.data;
                let parser = new DOMParser();
                let xmlDoc = parser.parseFromString(xml, "text/xml");
                // Set object Data
                let messageCode = xmlDoc.getElementsByTagName("coMensaje").length > 0 ?
                    xmlDoc.getElementsByTagName("coMensaje")[0].childNodes[0] ? xmlDoc.getElementsByTagName("coMensaje")[0].childNodes[0].nodeValue : 0 : 0;

                let message = xmlDoc.getElementsByTagName("deMensaje").length > 0 ?
                    xmlDoc.getElementsByTagName("deMensaje")[0].childNodes[0] ? xmlDoc.getElementsByTagName("deMensaje")[0].childNodes[0].nodeValue : "" : "";

                let resultCode = xmlDoc.getElementsByTagName("coResultado").length > 0 ?
                    xmlDoc.getElementsByTagName("coResultado")[0].childNodes[0] ? xmlDoc.getElementsByTagName("coResultado")[0].childNodes[0].nodeValue : 0 : 0;

                let result = xmlDoc.getElementsByTagName("deResultado").length > 0 ?
                    xmlDoc.getElementsByTagName("deResultado")[0].childNodes[0] ? xmlDoc.getElementsByTagName("deResultado")[0].childNodes[0].nodeValue : "" : "";

                let restriccion = xmlDoc.getElementsByTagName("restriccion").length > 0 ?
                    xmlDoc.getElementsByTagName("restriccion")[0].childNodes[0] ? xmlDoc.getElementsByTagName("restriccion")[0].childNodes[0].nodeValue : "" : "";

                let huella = ""
                if(xmlDoc.getElementsByTagName("imagen").length > 0 && xmlDoc.getElementsByTagName("imagen")[0].childNodes[0]){
                    huella = xmlDoc.getElementsByTagName("imagen")[0].childNodes[0].nodeValue
                    xmlDoc.getElementsByTagName("imagen")[0].childNodes[0].textContent = ""
                }

                let serializer = new XMLSerializer();
                let validateXML = serializer.serializeToString(xmlDoc);
                
                let newMessage = `${result} ${message}`; // Merge Message

                let zytrustResponse = {
                    messageCode,
                    message: newMessage,
                    resultCode,
                    result: newMessage,
                    restriccion: restriccion,
                    validateXML,
                    inputXML: xmlToSend,
                    outputXML: xml,
                    huella,
                }

                if (response.status === 200) {
                    dispatch({
                        type: BIOMETRIC_ZYTRUST_SERVICE_SUCCESS,
                        data: zytrustResponse,
                        error: null,
                        response: true
                    });
                } else {
                    dispatch({
                        type: BIOMETRIC_ZYTRUST_SERVICE_INVALID,
                        data: null,
                        error: zytrustResponse,
                        response: true
                    });
                }
                return response;
            }).catch(error => {
                // Error Request
                let newError = error.response ? error.response.data : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;

                let zytrustError = {
                    message: newError,
                    inputXML: xmlToSend ? xmlToSend : "",
                }

                dispatch({
                    type: BIOMETRIC_ZYTRUST_SERVICE_ERROR,
                    error: zytrustError,
                    response: response
                });
                return error;
            });
    }
}

export function validateBiometricZytrustService() {
    return (dispatch) => {
        dispatch({
            type: VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_REQUEST
        });
        return httpClient
            .get(`${URL_VALIDATE_ZYTRUST}`, {
                headers: {
                    'Content-Type': 'text/xml'
                }
            })
            .then(response => {
                if (response.status === 200) {
                    dispatch({
                        type: VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_SUCCESS,
                        data: {
                            online: true,
                            message: "BioMatchClient activo."
                        },
                        error: null,
                        response: true
                    });
                } else {
                    dispatch({
                        type: VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_INVALID,
                        data: {
                            online: false,
                            message: "BioMatchClient inactivo o acceso denegado."
                        },
                        error: null,
                        response: true
                    });
                }
                return response;
            }).catch(error => {
                // Error Request
                let newError = error.response ? error.response.data ? error.response.data : SERVER_ERROR_MESSAGE : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: VALIDATE_BIOMETRIC_ZYTRUST_SERVICE_ERROR,
                    data: {
                        online: false,
                        message: "BioMatchClient inactivo o acceso denegado."
                    },
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}
