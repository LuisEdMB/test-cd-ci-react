import axios from 'axios';
import * as moment from 'moment';
import {
    URL_ODC,
    SERVER_ERROR_MESSAGE
} from '../config';
import {
    decrypt,
    getDeviceData
} from '../../utils/Utils';
// Actions 
export const ODC_BIOMETRIC_ACTIVITY_REQUEST = 'ODC_BIOMETRIC_ACTIVITY_REQUEST';
export const ODC_BIOMETRIC_ACTIVITY_SUCCESS = 'ODC_BIOMETRIC_ACTIVITY_SUCCESS';
export const ODC_BIOMETRIC_ACTIVITY_INVALID = 'ODC_BIOMETRIC_ACTIVITY_INVALID';
export const ODC_BIOMETRIC_ACTIVITY_ERROR = 'ODC_BIOMETRIC_ACTIVITY_ERROR';

export const ODC_BIOMETRIC_ACTIVITY_ADITIONAL_REQUEST = 'ODC_BIOMETRIC_ACTIVITY_ADITIONAL_REQUEST';
export const ODC_BIOMETRIC_ACTIVITY_ADITIONAL_SUCCESS = 'ODC_BIOMETRIC_ACTIVITY_ADITIONAL_SUCCESS';
export const ODC_BIOMETRIC_ACTIVITY_ADITIONAL_INVALID = 'ODC_BIOMETRIC_ACTIVITY_ADITIONAL_INVALID';
export const ODC_BIOMETRIC_ACTIVITY_ADITIONAL_ERROR = 'ODC_BIOMETRIC_ACTIVITY_ADITIONAL_ERROR';

export const ODC_BIOMETRIC_VALIDATE_REQUEST = 'ODC_BIOMETRIC_VALIDATE_REQUEST';
export const ODC_BIOMETRIC_VALIDATE_SUCCESS = 'ODC_BIOMETRIC_VALIDATE_SUCCESS';
export const ODC_BIOMETRIC_VALIDATE_INVALID = 'ODC_BIOMETRIC_VALIDATE_INVALID';
export const ODC_BIOMETRIC_VALIDATE_ERROR = 'ODC_BIOMETRIC_VALIDATE_ERROR';

export const ODC_BIOMETRIC_VALIDATE_ADITIONAL_REQUEST = 'ODC_BIOMETRIC_VALIDATE_ADITIONAL_REQUEST';
export const ODC_BIOMETRIC_VALIDATE_ADITIONAL_SUCCESS = 'ODC_BIOMETRIC_VALIDATE_ADITIONAL_SUCCESS';
export const ODC_BIOMETRIC_VALIDATE_ADITIONAL_INVALID = 'ODC_BIOMETRIC_VALIDATE_ADITIONAL_INVALID';
export const ODC_BIOMETRIC_VALIDATE_ADITIONAL_ERROR = 'ODC_BIOMETRIC_VALIDATE_ADITIONAL_ERROR';

export function biometricActivity(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: ODC_BIOMETRIC_ACTIVITY_REQUEST
        });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            inputXml: data.inputXml,
            outputXml: data.outputXml,
            errorCodeBiometrico: data.errorCodeBiometrico,
            errorMessageBiometrico: data.errorMessageBiometrico,
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_cliente: data.cod_cliente
        });
        return axios
            .post(`${URL_ODC}/Biometrico/RegistrarActividad`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_BIOMETRIC_ACTIVITY_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    } else {
                        dispatch({
                            type: ODC_BIOMETRIC_ACTIVITY_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true,
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: ODC_BIOMETRIC_ACTIVITY_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}

function biometricActivityPromise(data) {
    return new Promise((resolve, reject) => {
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        let sendData = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            inputXml: data.inputXml,
            outputXml: data.outputXml,
            errorCodeBiometrico: data.errorCodeBiometrico,
            errorMessageBiometrico: data.errorMessageBiometrico,
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_cliente: data.cod_cliente
        });
        return axios
            .post(`${URL_ODC}/Biometrico/RegistrarActividad`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                return resolve(response);
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                reject({
                    error: newError,
                    response: response
                });
            });
    })
}

export function biometricActivityAditionals(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: ODC_BIOMETRIC_ACTIVITY_ADITIONAL_REQUEST
        });
        // Set Data
        let biometricActivitesPromises = data.map(item => biometricActivityPromise(item))

        Promise.all(biometricActivitesPromises).then(responses => {
            const errorEncontrado = responses.find(response => response.data === null || !response.data.success)
            if (errorEncontrado) {
                dispatch({
                    type: ODC_BIOMETRIC_ACTIVITY_ADITIONAL_INVALID,
                    data: null,
                    error: errorEncontrado.data.errorMessage,
                    response: true,
                });
            } else {
                dispatch({
                    type: ODC_BIOMETRIC_ACTIVITY_ADITIONAL_SUCCESS,
                    data: responses[0].data,
                    error: responses[0].data.errorMessage,
                    extra: extra,
                });
            }
            return responses
        }).catch(error => {
            // Error Request
            let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
            let response = error.response ? true : false;
            dispatch({
                type: ODC_BIOMETRIC_ACTIVITY_ADITIONAL_ERROR,
                error: newError,
                response: response
            });
            return error;
        })
    }
}

export function validateBiometric(data, extra = null) {
    return (dispatch) => {
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Begin Request
        dispatch({
            type: ODC_BIOMETRIC_VALIDATE_REQUEST
        });
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_solicitud: data.solicitudeCode,
            cod_solicitud_completa: data.completeSolicitudeCode,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            cod_error: data.codeError,
            cod_resultado: data.resultCode,
            intentos: data.intentos,
            mensaje_alerta: data.message,
            des_restrincion: data.restriccion,
            xml: data.validateXML
        });
        return axios
            .post(`${URL_ODC}/Biometrico/ValidarBiometrico`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        let data = response.data.validacion_Biometrica
                        dispatch({
                            type: ODC_BIOMETRIC_VALIDATE_SUCCESS,
                            data: data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    } else {
                        dispatch({
                            type: ODC_BIOMETRIC_VALIDATE_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true,
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: ODC_BIOMETRIC_VALIDATE_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}

function validateBiometricAditionalPromise(data) {
    return new Promise((resolve, reject) => {
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        let sendData = JSON.stringify({
            cod_solicitud: data.solicitudeCode,
            cod_solicitud_completa: data.completeSolicitudeCode,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            cod_error: data.codeError,
            cod_resultado: data.resultCode,
            intentos: data.intentos,
            mensaje_alerta: data.message,
            des_restrincion: data.restriccion,
            xml: data.validateXML
        });
        return axios
            .post(`${URL_ODC}/Biometrico/ValidarBiometrico`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                if (response.data) {
                    response.data = {
                        ...response.data,
                        ...response.data.validacion_Biometrica
                    }
                }
                return resolve(response);
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                reject({
                    error: newError,
                    response: response
                });
            });
    })
}

export function validateBiometricAditional(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: ODC_BIOMETRIC_VALIDATE_ADITIONAL_REQUEST
        });
        // Launch Dispatch - Server
        let validateActivitesPromises = data.map(item => validateBiometricAditionalPromise(item))

        Promise.all(validateActivitesPromises).then(responses => {
            const errorEncontrado = responses.find(response => response.data === null || !response.data.success)
            
            if (errorEncontrado) {
                dispatch({
                    type: ODC_BIOMETRIC_VALIDATE_ADITIONAL_INVALID,
                    data: null,
                    error: errorEncontrado.data.errorMessage,
                    response: true,
                });
            } else {
                dispatch({
                    type: ODC_BIOMETRIC_VALIDATE_ADITIONAL_SUCCESS,
                    data: responses[0].data,
                    error: responses[0].data.errorMessage,
                    extra: extra,
                });
            }
            return responses
        }).catch(error => {
            // Error Request
            let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
            let response = error.response ? true : false;
            dispatch({
                type: ODC_BIOMETRIC_VALIDATE_ADITIONAL_ERROR,
                error: newError,
                response: response
            });
            return error;
        })
    }
}