import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';

// Actions 
export const ODC_CONSTANT_REQUEST = 'ODC_CONSTANT_REQUEST'; 
export const ODC_CONSTANT_SUCCESS = 'ODC_CONSTANT_SUCCESS'; 
export const ODC_CONSTANT_INVALID = 'ODC_CONSTANT_INVALID';
export const ODC_CONSTANT_ERROR = 'ODC_CONSTANT_ERROR'; 

export function getConstantODC() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: ODC_CONSTANT_REQUEST});
        return axios
            .post(`${URL_ODC}/Valores/Constantes`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: ODC_CONSTANT_SUCCESS, data: response.data.constantes });
                    }
                    else{
                        dispatch({ 
                            type: ODC_CONSTANT_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_CONSTANT_ERROR, error:newError, response: response});
                return error;
            });
    };
}