import axios from 'axios';
import { SERVER_ERROR_MESSAGE } from '../config';

export function callApi(request, actions, dataToJson = true) {
    return (dispatch) => {
        dispatch({ type: actions.request })
        return axios({
            url: request.url,
            method: request.method,
            data: dataToJson ? JSON.stringify(request.data) : request.data,
            headers: {
                'Accept': 'application/json',
                'Content-Type': request.contentType || 'application/json'
            },
            ...request.extra
        })
        .then(response => {
            if (response?.data) {
                if (response.data?.success) dispatch({ type: actions.success, data: response.data })
                else dispatch({ 
                    type: actions.invalid,
                    error: response.data?.errorMessage || '',
                    errorCode: response.data?.errorCode || '',
                    data: response.data
                })
            }
            return response?.data
        })
        .catch(error => {
            let newError = error.response?.data?.errorMessage || SERVER_ERROR_MESSAGE
            let newErrorCode = error.response?.data?.errorCode || ''
            let newSuccess = error.response?.data?.success || false
            let response = error.response?.data ? true : false
            dispatch({ type: actions.error, error: newError, errorCode: newErrorCode, success: newSuccess,
                response: response, data: error.response?.data })
            return response?.data
        })
    }
}