import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';
// Actions 
export const GET_PROCESS_REQUEST = 'GET_PROCESS_REQUEST';
export const GET_PROCESS_SUCCESS = 'GET_PROCESS_SUCCESS';
export const GET_PROCESS_INVALID = 'GET_PROCESS_INVALID';
export const GET_PROCESS_ERROR = 'GET_PROCESS_ERROR';

export const GET_SUBPROCESS_REQUEST = 'GET_SUBPROCESS_REQUEST'; 
export const GET_SUBPROCESS_SUCCESS = 'GET_SUBPROCESS_SUCCESS'; 
export const GET_SUBPROCESS_INVALID = 'GET_SUBPROCESS_INVALID';
export const GET_SUBPROCESS_ERROR = 'GET_SUBPROCESS_ERROR'; 

export const GET_PROCESS_FLOW_PHASE_STATE_REQUEST = 'GET_PROCESS_FLOW_PHASE_STATE_REQUEST'; 
export const GET_PROCESS_FLOW_PHASE_STATE_SUCCESS = 'GET_PROCESS_FLOW_PHASE_STATE_SUCCESS'; 
export const GET_PROCESS_FLOW_PHASE_STATE_INVALID = 'GET_PROCESS_FLOW_PHASE_STATE_INVALID';
export const GET_PROCESS_FLOW_PHASE_STATE_ERROR = 'GET_PROCESS_FLOW_PHASE_STATE_ERROR'; 

export function getProcess(cod_tipo=0, cod_valor=0) {
    return (dispatch) => {
        //Begin Request
        dispatch({type: GET_PROCESS_REQUEST});
        return axios
            .get(`${URL_ODC}/Valores/Procesos?cod_tipo=${cod_tipo}&cod_valor=${cod_valor}`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: GET_PROCESS_SUCCESS, data: response.data.procesos });
                    }
                    else{
                        dispatch({ 
                            type: GET_PROCESS_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data? error.response.data.errorMessage? error.response.data.errorMessage: SERVER_ERROR_MESSAGE: SERVER_ERROR_MESSAGE: SERVER_ERROR_MESSAGE; 
                let response = error.response? true: false;
                dispatch({type: GET_PROCESS_ERROR, error: newError, response: response});
                return error;
            });
    };
}
export function getSubProcess(cod_tipo=0, cod_valor=0) {
    return (dispatch) => {
        //Begin Request
        dispatch({type: GET_SUBPROCESS_REQUEST});
        return axios
            .get(`${URL_ODC}/Valores/Procesos?cod_tipo=${cod_tipo}&cod_valor=${cod_valor}`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: GET_SUBPROCESS_SUCCESS, data: response.data.procesos });
                    }
                    else{
                        dispatch({ 
                            type: GET_SUBPROCESS_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data? error.response.data.errorMessage? error.response.data.errorMessage: SERVER_ERROR_MESSAGE: SERVER_ERROR_MESSAGE: SERVER_ERROR_MESSAGE; 
                let response = error.response? true: false;
                dispatch({type: GET_SUBPROCESS_ERROR, error: newError, response: response});
                return error;
            });
    };
}
export function getProcessFlowPhaseState(cod_tipo=0, cod_valor=0){
    return (dispatch) => {
        //Begin Request
        dispatch({type: GET_PROCESS_FLOW_PHASE_STATE_REQUEST});
        return axios
            .get(`${URL_ODC}/Valores/Procesos?cod_tipo=${cod_tipo}&cod_valor=${cod_valor}`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: GET_PROCESS_FLOW_PHASE_STATE_SUCCESS, data: response.data.procesos });
                    }
                    else{
                        dispatch({ 
                            type: GET_PROCESS_FLOW_PHASE_STATE_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data? error.response.data.errorMessage? error.response.data.errorMessage: SERVER_ERROR_MESSAGE: SERVER_ERROR_MESSAGE: SERVER_ERROR_MESSAGE; 
                let response = error.response? true: false;
                dispatch({type: GET_PROCESS_FLOW_PHASE_STATE_ERROR, error: newError, response: response});
                return error;
            });
    };
}
