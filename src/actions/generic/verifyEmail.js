import axios from 'axios';
import * as moment from 'moment';
import {
    URL_ODC,
    SERVER_ERROR_MESSAGE
} from '../config';
import {
    decrypt,
    getDeviceData
} from '../../utils/Utils';

// Actions 
export const SEND_MAIL_CODE_REQUEST = 'SEND_MAIL_CODE_REQUEST'; 
export const SEND_MAIL_CODE_SUCCESS = 'SEND_MAIL_CODE_SUCCESS'; 
export const SEND_MAIL_CODE_INVALID = 'SEND_MAIL_CODE_INVALID'; 
export const SEND_MAIL_CODE_ERROR   = 'SEND_MAIL_CODE_ERROR'; 

export const VERIFY_MAIL_CODE_REQUEST = 'VERIFY_MAIL_CODE_REQUEST'; 
export const VERIFY_MAIL_CODE_SUCCESS = 'VERIFY_MAIL_CODE_SUCCESS'; 
export const VERIFY_MAIL_CODE_INVALID = 'VERIFY_MAIL_CODE_INVALID'; 
export const VERIFY_MAIL_CODE_ERROR   = 'VERIFY_MAIL_CODE_ERROR'; 

export const UPDATE_CLIENT_MAIL_REQUEST = 'UPDATE_CLIENT_MAIL_REQUEST'; 
export const UPDATE_CLIENT_MAIL_SUCCESS = 'UPDATE_CLIENT_MAIL_SUCCESS'; 
export const UPDATE_CLIENT_MAIL_INVALID = 'UPDATE_CLIENT_MAIL_INVALID'; 
export const UPDATE_CLIENT_MAIL_ERROR = 'UPDATE_CLIENT_MAIL_ERROR'; 

export function sendMailCode(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: SEND_MAIL_CODE_REQUEST
        });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_solicitud_completa: data.cod_solicitud_completa,
            des_nro_documento: data.des_nro_documento,
            des_to: data.des_to,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
        });
        return axios
            .post(`${URL_ODC}/Email/EnviarConfirmacionEmail`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: SEND_MAIL_CODE_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    } else {
                        dispatch({
                            type: SEND_MAIL_CODE_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true,
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: SEND_MAIL_CODE_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}

export function verifyMailCode(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: VERIFY_MAIL_CODE_REQUEST
        });
        // Launch Dispatch - Server
        return axios
            .post(`${URL_ODC}/Email/ValidarConfirmacionEmailPorSolicitud?cod_solicitud_completa=${data.cod_solicitud_completa}&des_nro_documento=${data.des_nro_documento}&code=${data.code}`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })
            .then(response => {
                // Launch Dispatch 
                        if (response.data) {
                        if (response.data.success) {
                        dispatch({
                            type: VERIFY_MAIL_CODE_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    } else {
                        dispatch({
                            type: VERIFY_MAIL_CODE_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true,
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: VERIFY_MAIL_CODE_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}

export function updateClientMail(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: UPDATE_CLIENT_MAIL_REQUEST
        });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_cliente: data.cod_cliente,
            des_nro_documento: data.des_nro_documento,
            des_to: data.des_to,
            fec_act: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_act: decryptData.username,
            des_ter_act: device.ip,
        });
        return axios
            .post(`${URL_ODC}/Email/ActualizarCorreo`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: UPDATE_CLIENT_MAIL_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    } else {
                        dispatch({
                            type: UPDATE_CLIENT_MAIL_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true,
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: UPDATE_CLIENT_MAIL_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}