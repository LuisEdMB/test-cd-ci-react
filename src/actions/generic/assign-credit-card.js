import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';
// Actions 
export const ODC_CREDIT_CARD_ASSIGNMENT_REQUEST = 'ODC_CREDIT_CARD_ASSIGNMENT_REQUEST'; 
export const ODC_CREDIT_CARD_ASSIGNMENT_SUCCESS = 'ODC_CREDIT_CARD_ASSIGNMENT_SUCCESS'; 
export const ODC_CREDIT_CARD_ASSIGNMENT_INVALID = 'ODC_CREDIT_CARD_ASSIGNMENT_INVALID';
export const ODC_CREDIT_CARD_ASSIGNMENT_ERROR = 'ODC_CREDIT_CARD_ASSIGNMENT_ERROR'; 

export function getCreditCardAssignment() {
    return (dispatch) => {
        //Begin Request
        dispatch({type: ODC_CREDIT_CARD_ASSIGNMENT_REQUEST});
        return axios
            .get(`${URL_ODC}/Valores/AsignacionTarjetaCredito`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: ODC_CREDIT_CARD_ASSIGNMENT_SUCCESS, data: response.data.asignacionTC });
                    }
                    else{
                        dispatch({ 
                            type: ODC_CREDIT_CARD_ASSIGNMENT_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_CREDIT_CARD_ASSIGNMENT_ERROR, error:newError, response: response});
                return error;
            });
    };
}
