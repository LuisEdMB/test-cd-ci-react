import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';
// Actions 
export const GET_PCT_REQUEST = 'GET_PCT_REQUEST'; 
export const GET_PCT_SUCCESS = 'GET_PCT_SUCCESS'; 
export const GET_PCT_INVALID = 'GET_PCT_INVALID';
export const GET_PCT_ERROR = 'GET_PCT_ERROR'; 

export function getPCT(pct_linea_final, pct_porcentaje, pct_segmento) {
    let sendData = JSON.stringify({
        // des_nom_prod:des_nom_prod, 
        // pct_porcentaje:pct_porcentaje, 
        // pct_segmento:pct_segmento
        pct_linea_final: pct_linea_final,
        pct_porcentaje: pct_porcentaje,
        pct_segmento: pct_segmento,
    });
    return (dispatch) => {
        //Begin Request
        dispatch({type: GET_PCT_REQUEST});
        return axios
            .post(`${URL_ODC}/Valores/Pct`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: GET_PCT_SUCCESS, data: response.data.pct });
                    }
                    else{
                        dispatch({ 
                            type: GET_PCT_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: GET_PCT_ERROR, error:newError, response: response});
                return error;
            });
    };
}

export function getPCTCommercialOffer(num_pct, pct_segmento, pct_linea_final){
    let sendData = JSON.stringify({
        num_pct,
        pct_segmento,
        pct_linea_final
    });
    return (dispatch) => {
        dispatch({type: GET_PCT_REQUEST});
        return axios
            .post(`${URL_ODC}/Valores/PctTEA`, sendData, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: GET_PCT_SUCCESS, data: response.data.pct });
                    }
                    else{
                        dispatch({
                            type: GET_PCT_INVALID,
                            data: null,
                            error: response.errorMessage,
                            response: true });
                    }
                }
                return response;
            }).catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE;
                let response = error.response? true : false;
                dispatch({type: GET_PCT_ERROR, error:newError, response: response});
                return error;
            });
    }
}