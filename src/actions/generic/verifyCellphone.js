import axios from 'axios';
import * as moment from 'moment';
import {
    URL_ODC_REGULAR,
    SERVER_ERROR_MESSAGE
} from '../config';
import {
    decrypt,
    getDeviceData
} from '../../utils/Utils';

// Actions 
export const SEND_SMS_CODE_REQUEST = 'SEND_SMS_CODE_REQUEST'; 
export const SEND_SMS_CODE_SUCCESS = 'SEND_SMS_CODE_SUCCESS'; 
export const SEND_SMS_CODE_INVALID = 'SEND_SMS_CODE_INVALID'; 
export const SEND_SMS_CODE_ERROR   = 'SEND_SMS_CODE_ERROR'; 

export const VERIFY_SMS_CODE_REQUEST = 'VERIFY_SMS_CODE_REQUEST'; 
export const VERIFY_SMS_CODE_SUCCESS = 'VERIFY_SMS_CODE_SUCCESS'; 
export const VERIFY_SMS_CODE_INVALID = 'VERIFY_SMS_CODE_INVALID'; 
export const VERIFY_SMS_CODE_ERROR   = 'VERIFY_SMS_CODE_ERROR'; 

export const UPDATE_CLIENT_CELLPHONE_REQUEST = 'UPDATE_CLIENT_CELLPHONE_REQUEST'; 
export const UPDATE_CLIENT_CELLPHONE_SUCCESS = 'UPDATE_CLIENT_CELLPHONE_SUCCESS'; 
export const UPDATE_CLIENT_CELLPHONE_INVALID = 'UPDATE_CLIENT_CELLPHONE_INVALID'; 
export const UPDATE_CLIENT_CELLPHONE_ERROR = 'UPDATE_CLIENT_CELLPHONE_ERROR'; 

export function sendSMSCode(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: SEND_SMS_CODE_REQUEST
        });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_solicitud_completa: data.cod_solicitud_completa,
            des_nro_documento: data.des_nro_documento,
            des_telefono: data.des_telefono,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            mensajeSMS: "",
            cod_validador_sms: 0
        });
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/SMS/EnviarConfirmacionSMS`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: SEND_SMS_CODE_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    } else {
                        dispatch({
                            type: SEND_SMS_CODE_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true,
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: SEND_SMS_CODE_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}

export function verifySMSCode(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: VERIFY_SMS_CODE_REQUEST
        });
        // Launch Dispatch - Server
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/SMS/ValidarConfirmacionSMSPorSolicitud?cod_solicitud_completa=${data.cod_solicitud_completa}&des_telf=${data.des_telefono}&code=${data.code}`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })
            .then(response => {
                // Launch Dispatch 
                        if (response.data) {
                        if (response.data.success) {
                        dispatch({
                            type: VERIFY_SMS_CODE_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    } else {
                        dispatch({
                            type: VERIFY_SMS_CODE_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true,
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: VERIFY_SMS_CODE_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}

export function updateClientCellphone(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: UPDATE_CLIENT_CELLPHONE_REQUEST
        });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_cliente: data.cod_cliente,
            des_nro_documento: data.des_nro_documento,
            des_telefono: data.des_telefono,
            fec_act: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_act: decryptData.username,
            des_ter_act: device.ip,
        });
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/SMS/ActualizarTelefono`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: UPDATE_CLIENT_CELLPHONE_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    } else {
                        dispatch({
                            type: UPDATE_CLIENT_CELLPHONE_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true,
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: UPDATE_CLIENT_CELLPHONE_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}