import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';
// Actions 
export const GET_AGENCY_REQUEST = 'GET_AGENCY_REQUEST'; 
export const GET_AGENCY_SUCCESS = 'GET_AGENCY_SUCCESS'; 
export const GET_AGENCY_INVALID = 'GET_AGENCY_INVALID';
export const GET_AGENCY_ERROR = 'GET_AGENCY_ERROR'; 

export function getAgency(cod_agencia="", des_agencia="", des_abreviatura="", cod_interno="") {
    return (dispatch) => {
        //Begin Request
        dispatch({type: GET_AGENCY_REQUEST});
        return axios
            .get(`${URL_ODC}/Valores/Agencias?cod_agencia=${cod_agencia}&des_agencia=${des_agencia}&cod_interno=${des_abreviatura}&des_abreviatura=${cod_interno}`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: GET_AGENCY_SUCCESS, data: response.data.agencias });
                    }
                    else{
                        dispatch({ 
                            type: GET_AGENCY_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage: SERVER_ERROR_MESSAGE; 
                let response = error.response? true: false;
                dispatch({type: GET_AGENCY_ERROR, error:newError, response: response});
                return error;
            });
    };
}
