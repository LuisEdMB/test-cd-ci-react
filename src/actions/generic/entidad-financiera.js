import axios from 'axios'
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';

export const GET_ENTIDAD_FINANCIERA_REQUEST = 'GET_ENTIDAD_FINANCIERA_REQUEST';
export const GET_ENTIDAD_FINANCIERA_SUCCESS = 'GET_ENTIDAD_FINANCIERA_SUCCESS';
export const GET_ENTIDAD_FINANCIERA_INVALID = 'GET_ENTIDAD_FINANCIERA_INVALID';
export const GET_ENTIDAD_FINANCIERA_ERROR = 'GET_ENTIDAD_FINANCIERA_ERROR';

export function getEntidadFinanciera(cci) {
    let sendData = JSON.stringify({
        cci
    });
    return (dispatch) => {
        dispatch({type:GET_ENTIDAD_FINANCIERA_REQUEST});
        return axios
            .post(`${URL_ODC}/Valores/EntidadFinanciera`, sendData,{
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })
            .then(response => {
                if(response.data){
                    if(response.data.success){
                        dispatch({
                            type: GET_ENTIDAD_FINANCIERA_SUCCESS,
                            data: response.data.entidadFinanciera
                        })
                    }
                    else {
                        dispatch({
                            type: GET_ENTIDAD_FINANCIERA_INVALID,
                            data: null,
                            error: response.errorMessage,
                            response: true
                        })
                    }
                }
                return response
            }).catch(error => {
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE;
                let response = error.response? true : false;
                dispatch({type: GET_ENTIDAD_FINANCIERA_ERROR, error:newError, response: response});
                return error;
            })
    }
}