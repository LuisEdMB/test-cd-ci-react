import axios from 'axios';
import { URL_ODC, SERVER_ERROR_MESSAGE } from '../config';
// Actions 
export const GET_PROMOTER_REQUEST = 'GET_PROMOTER_REQUEST'; 
export const GET_PROMOTER_SUCCESS = 'GET_PROMOTER_SUCCESS'; 
export const GET_PROMOTER_INVALID = 'GET_PROMOTER_INVALID'; 
export const GET_PROMOTER_ERROR = 'GET_PROMOTER_ERROR'; 

export function getPromoter(search) {
    return (dispatch) => {
        //Begin Request
        dispatch({type: GET_PROMOTER_REQUEST});
        return axios
            .get(`${URL_ODC}/Originacion/Promotor?buscar=${String(search).trim()}`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: GET_PROMOTER_SUCCESS, data: response.data.promotor });
                        return response;
                    }
                    else{
                        dispatch({ 
                            type: GET_PROMOTER_INVALID, 
                            data: null, 
                            error: response.errorMessage, 
                            response: true });
                        return response;
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: GET_PROMOTER_ERROR, error:newError, response: response});
                return error;
            });
    };
}
