import axios from 'axios';
import * as moment from 'moment';
import {
    decrypt,
    getDeviceData
} from '../../utils/Utils';
// URL - SERVER
import {
    URL_ODC,
    URL_ODC_REGULAR,
    SERVER_ERROR_MESSAGE
} from '../config';

//import { osName, osVersion, browserName, browserVersion } from 'react-device-detect';
// Actions - First Call
export const ODC_REGULAR_CONSULT_CLIENT_REQUEST = 'ODC_REGULAR_CONSULT_CLIENT_REQUEST';
export const ODC_REGULAR_CONSULT_CLIENT_SUCCESS = 'ODC_REGULAR_CONSULT_CLIENT_SUCCESS';
export const ODC_REGULAR_CONSULT_CLIENT_INVALID = 'ODC_REGULAR_CONSULT_CLIENT_INVALID';
export const ODC_REGULAR_CONSULT_CLIENT_ERROR = 'ODC_REGULAR_CONSULT_CLIENT_ERROR';
// Actions - Second Call gg
export const ODC_REGULAR_SECOND_CALL_REQUEST = 'ODC_REGULAR_SECOND_CALL_REQUEST';
export const ODC_REGULAR_SECOND_CALL_SUCCESS = 'ODC_REGULAR_SECOND_CALL_SUCCESS';
export const ODC_REGULAR_SECOND_CALL_INVALID = 'ODC_REGULAR_SECOND_CALL_INVALID';
export const ODC_REGULAR_SECOND_CALL_ERROR = 'ODC_REGULAR_SECOND_CALL_ERROR';
// Actions - Summary
export const ODC_REGULAR_OFFER_SUMMARY_REQUEST = 'ODC_REGULAR_OFFER_SUMMARY_REQUEST';
export const ODC_REGULAR_OFFER_SUMMARY_SUCCESS = 'ODC_REGULAR_OFFER_SUMMARY_SUCCESS';
export const ODC_REGULAR_OFFER_SUMMARY_INVALID = 'ODC_REGULAR_OFFER_SUMMARY_INVALID';
export const ODC_REGULAR_OFFER_SUMMARY_ERROR = 'ODC_REGULAR_OFFER_SUMMARY_ERROR';
// Actions - Commercial Offer
export const ODC_REGULAR_COMMERCIAL_OFFER_REQUEST = 'ODC_REGULAR_COMMERCIAL_OFFER_REQUEST';
export const ODC_REGULAR_COMMERCIAL_OFFER_SUCCESS = 'ODC_REGULAR_COMMERCIAL_OFFER_SUCCESS';
export const ODC_REGULAR_COMMERCIAL_OFFER_INVALID = 'ODC_REGULAR_COMMERCIAL_OFFER_INVALID';
export const ODC_REGULAR_COMMERCIAL_OFFER_ERROR = 'ODC_REGULAR_COMMERCIAL_OFFER_ERROR';
// Actions - Embossing
export const ODC_REGULAR_PENDING_EMBOSSING_REQUEST = 'ODC_REGULAR_PENDING_EMBOSSING_REQUEST';
export const ODC_REGULAR_PENDING_EMBOSSING_SUCCESS = 'ODC_REGULAR_PENDING_EMBOSSING_SUCCESS';
export const ODC_REGULAR_PENDING_EMBOSSING_INVALID = 'ODC_REGULAR_PENDING_EMBOSSING_INVALID';
export const ODC_REGULAR_PENDING_EMBOSSING_ERROR = 'ODC_REGULAR_PENDING_EMBOSSING_ERROR';
// Actions - Pending Processes gg
export const ODC_REGULAR_PENDING_PROCESSES_REQUEST = 'ODC_REGULAR_PENDING_PROCESSES_REQUEST';
export const ODC_REGULAR_PENDING_PROCESSES_SUCCESS = 'ODC_REGULAR_PENDING_PROCESSES_SUCCESS';
export const ODC_REGULAR_PENDING_PROCESSES_INVALID = 'ODC_REGULAR_PENDING_PROCESSES_INVALID';
export const ODC_REGULAR_PENDING_PROCESSES_ERROR = 'ODC_REGULAR_PENDING_PROCESSES_ERROR';
// Actions - Continue Process
export const ODC_REGULAR_CONTINUE_PROCESS_REQUEST = 'ODC_REGULAR_CONTINUE_PROCESS_REQUEST';
export const ODC_REGULAR_CONTINUE_PROCESS_SUCCESS = 'ODC_REGULAR_CONTINUE_PROCESS_SUCCESS';
export const ODC_REGULAR_CONTINUE_PROCESS_INVALID = 'ODC_REGULAR_CONTINUE_PROCESS_INVALID';
export const ODC_REGULAR_CONTINUE_PROCESS_ERROR = 'ODC_REGULAR_CONTINUE_PROCESS_ERROR';
// Actions - Pending Processes Activation gg
export const ODC_REGULAR_PENDING_PROCESSES_ACTIVATION_REQUEST = 'ODC_REGULAR_PENDING_PROCESSES_ACTIVATION_REQUEST';
export const ODC_REGULAR_PENDING_PROCESSES_ACTIVATION_INVALID = 'ODC_REGULAR_PENDING_PROCESSES_ACTIVATION_INVALID';
export const ODC_REGULAR_PENDING_PROCESSES_ACTIVATION_SUCCESS = 'ODC_REGULAR_PENDING_PROCESSES_ACTIVATION_SUCCESS';
export const ODC_REGULAR_PENDING_PROCESSES_ACTIVATION_ERROR = 'ODC_REGULAR_PENDING_PROCESSES_ACTIVATION_ERROR';
// Actions - authorize Activation Process
export const ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_REQUEST = 'ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_REQUEST';
export const ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS = 'ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS';
export const ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_INVALID = 'ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_INVALID';
export const ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_ERROR = 'ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_ERROR';
// Actions - Reject Process
export const ODC_REGULAR_REJECT_PROCESS_REQUEST = 'ODC_REGULAR_REJECT_PROCESS_REQUEST'
export const ODC_REGULAR_REJECT_PROCESS_INVALID = 'ODC_REGULAR_REJECT_PROCESS_INVALID'
export const ODC_REGULAR_REJECT_PROCESS_SUCCESS = 'ODC_REGULAR_REJECT_PROCESS_SUCCESS'
export const ODC_REGULAR_REJECT_PROCESS_ERROR = 'ODC_REGULAR_REJECT_PROCESS_ERROR'
// PreloaderClient
export const ODC_REGULAR_LOAD_CLIENT_REQUEST = 'ODC_REGULAR_LOAD_CLIENT_REQUEST';
export const ODC_REGULAR_LOAD_CLIENT_SUCCESS = 'ODC_REGULAR_LOAD_CLIENT_SUCCESS';
export const ODC_REGULAR_LOAD_CLIENT_INVALID = 'ODC_REGULAR_LOAD_CLIENT_INVALID';
export const ODC_REGULAR_LOAD_CLIENT_ERROR = 'ODC_REGULAR_LOAD_CLIENT_ERROR';
// Actions- Update-Client-Module-load-client
export const ODC_REGULAR_UPDATE_CLIENT_REQUEST = 'ODC_REGULAR_UPDATE_CLIENT_REQUEST';
export const ODC_REGULAR_UPDATE_CLIENT_SUCCESS = 'ODC_REGULAR_UPDATE_CLIENT_SUCCESS';
export const ODC_REGULAR_UPDATE_CLIENT_INVALID = 'ODC_REGULAR_UPDATE_CLIENT_INVALID';
export const ODC_REGULAR_UPDATE_CLIENT_ERROR = 'ODC_REGULAR_UPDATE_CLIENT_ERROR';
// Actions- Update-Client-Module-load-client-data
export const ODC_REGULAR_UPDATE_CLIENT_DATA_REQUEST = 'ODC_REGULAR_UPDATE_CLIENT_DATA_REQUEST';
export const ODC_REGULAR_UPDATE_CLIENT_DATA_SUCCESS = 'ODC_REGULAR_UPDATE_CLIENT_DATA_SUCCESS';
export const ODC_REGULAR_UPDATE_CLIENT_DATA_INVALID = 'ODC_REGULAR_UPDATE_CLIENT_DATA_INVALID';
export const ODC_REGULAR_UPDATE_CLIENT_DATA_ERROR = 'ODC_REGULAR_UPDATE_CLIENT_DATA_ERROR';
// Actions - Update-Client-Data-Save
export const ODC_REGULAR_REGISTER_CLIENT_REQUEST = 'ODC_REGULAR_REGISTER_CLIENT_REQUEST';
export const ODC_REGULAR_REGISTER_CLIENT_SUCCESS = 'ODC_REGULAR_REGISTER_CLIENT_SUCCESS';
export const ODC_REGULAR_REGISTER_CLIENT_INVALID = 'ODC_REGULAR_REGISTER_CLIENT_INVALID';
export const ODC_REGULAR_REGISTER_CLIENT_ERROR = 'ODC_REGULAR_REGISTER_CLIENT_ERROR';
// Actions-Create-Business
export const ODC_REGULAR_CREATE_BUSINESS_REQUEST = 'ODC_REGULAR_CREATE_BUSINESS_REQUEST';
export const ODC_REGULAR_CREATE_BUSINESS_SUCCESS = 'ODC_REGULAR_CREATE_BUSINESS_SUCCESS';
export const ODC_REGULAR_CREATE_BUSINESS_INVALID = 'ODC_REGULAR_CREATE_BUSINESS_INVALID';
export const ODC_REGULAR_CREATE_BUSINESS_ERROR = 'ODC_REGULAR_CREATE_BUSINESS_ERROR';
// Actions-Search-Business
export const ODC_REGULAR_SEARCH_BUSINESS_REQUEST = 'ODC_REGULAR_SEARCH_BUSINESS_REQUEST';
export const ODC_REGULAR_SEARCH_BUSINESS_SUCCESS = 'ODC_REGULAR_SEARCH_BUSINESS_SUCCESS';
export const ODC_REGULAR_SEARCH_BUSINESS_INVALID = 'ODC_REGULAR_SEARCH_BUSINESS_INVALID';
export const ODC_REGULAR_SEARCH_BUSINESS_ERROR = 'ODC_REGULAR_SEARCH_BUSINESS_ERROR';
// Actions-Update-Business
export const ODC_REGULAR_UPDATE_BUSINESS_REQUEST = 'ODC_REGULAR_UPDATE_BUSINESS_REQUEST';
export const ODC_REGULAR_UPDATE_BUSINESS_SUCCESS = 'ODC_REGULAR_UPDATE_BUSINESS_SUCCESS';
export const ODC_REGULAR_UPDATE_BUSINESS_INVALID = 'ODC_REGULAR_UPDATE_BUSINESS_INVALID';
export const ODC_REGULAR_UPDATE_BUSINESS_ERROR = 'ODC_REGULAR_UPDATE_BUSINESS_ERROR';
// Actions-Delete-Business
export const ODC_REGULAR_DELETE_BUSINESS_REQUEST = 'ODC_REGULAR_DELETE_BUSINESS_REQUEST';
export const ODC_REGULAR_DELETE_BUSINESS_SUCCESS = 'ODC_REGULAR_DELETE_BUSINESS_SUCCESS';
export const ODC_REGULAR_DELETE_BUSINESS_INVALID = 'ODC_REGULAR_DELETE_BUSINESS_INVALID';
export const ODC_REGULAR_DELETE_BUSINESS_ERROR = 'ODC_REGULAR_DELETE_BUSINESS_ERROR';
// Actions-Create-comment
export const ODC_REGULAR_CREATE_COMMENT_REQUEST = 'ODC_REGULAR_CREATE_COMMENT_REQUEST';
export const ODC_REGULAR_CREATE_COMMENT_SUCCESS = 'ODC_REGULAR_CREATE_COMMENT_SUCCESS';
export const ODC_REGULAR_CREATE_COMMENT_INVALID = 'ODC_REGULAR_CREATE_COMMENT_INVALID';
export const ODC_REGULAR_CREATE_COMMENT_ERROR = 'ODC_REGULAR_CREATE_COMMENT_ERROR';
// Actions-Client-DNI
export const ODC_REGULAR_VALIDATION_CLIENT_DNI_REQUEST = 'ODC_REGULAR_VALIDATION_CLIENT_DNI_REQUEST';
export const ODC_REGULAR_VALIDATION_CLIENT_DNI_SUCCESS = 'ODC_REGULAR_VALIDATION_CLIENT_DNI_SUCCESS';
export const ODC_REGULAR_VALIDATION_CLIENT_DNI_INVALID = 'ODC_REGULAR_VALIDATION_CLIENT_DNI_INVALID';
export const ODC_REGULAR_VALIDATION_CLIENT_DNI_ERROR = 'ODC_REGULAR_VALIDATION_CLIENT_DNI_ERROR';
// Actions-Valida-Cuenta
export const PMP_REGULAR_CLIENTE_VALIDA_CUENTA_REQUEST = 'PMP_REGULAR_CLIENTE_VALIDA_CUENTA_REQUEST';
export const PMP_REGULAR_CLIENTE_VALIDA_CUENTA_INVALID = 'PMP_REGULAR_CLIENTE_VALIDA_CUENTA_INVALID';
export const PMP_REGULAR_CLIENTE_VALIDA_CUENTA_SUCCESS = 'PMP_REGULAR_CLIENTE_VALIDA_CUENTA_SUCCESS';
export const PMP_REGULAR_CLIENTE_VALIDA_CUENTA_ERROR = 'PMP_REGULAR_CLIENTE_VALIDA_CUENTA_ERROR';

// GET
export function continueProcess(originationNumber, documentNumber){
    return (dispatch) => {
        //Begin Request
        dispatch({type: ODC_REGULAR_CONTINUE_PROCESS_REQUEST});
        return axios
            .get(`${URL_ODC}/Originacion/SolicitudRetomar?cod_solicitud_completa=${originationNumber}`, {
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })  
            .then((response)=>{
                if(response.data){
                    if(response.data.success){
                        dispatch({ type: ODC_REGULAR_CONTINUE_PROCESS_SUCCESS, data: response.data.solicitud_Retomar });
                    }
                    else{
                        dispatch({ 
                            type: ODC_REGULAR_CONTINUE_PROCESS_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true });
                    }
                }
                return response; 
            })
            .catch(error => {
                // Error Request

                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_REGULAR_CONTINUE_PROCESS_ERROR, error:newError, response: response});
                return error;
            });
    };
}

// POST
export function firstRegularCall(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: ODC_REGULAR_CONSULT_CLIENT_REQUEST
        });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        // Launch Dispatch - Server
        let ODCFirstCallData = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),

            TCType: "VISA",
            maritalStatus: data.maritalStatus,
            dateJoinedCompany: data.dateJoinedCompany,
            birthDate: data.birthDate,
            "nationality": "",
            studiesLevel: data.studiesLevel,
            "occupation": "",
            "employerRUC": "",
            "gender": "",
            laborSituation: data.laborSituation,
            "cellPhone": "",
            "homePhone": "",
            "workPhone": "",
            "housingType": "",

            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_cliente: data.cod_cliente,
            cod_tipo_solicitud: data.cod_tipo_solicitud,
            cod_tipo_relacion: data.cod_tipo_relacion,
            cod_tipo_solicitud_requerimiento: data.cod_tipo_solicitud_requerimiento,
            cod_tipo_solicitud_originacion: data.cod_tipo_solicitud_originacion,
            cod_etapa_maestro: data.cod_etapa_maestro
        });
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Cliente/ConsultaCliente`, ODCFirstCallData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {

                    if (response.data.success) {
                        let {
                            sae
                        } = response.data;

                        let {
                            primeraLlamadaCda,
                            segundaLlamadaCda,
                            fraude,
                            solicitud_EnProceso_Siebel,
                            adn,
                            scoring,
                            validacionesTotales,
                            cod_cliente,
                            cod_solicitud,
                            cod_solicitud_actividad,
                            cod_solicitud_completa,
                        } = response.data;

                        // Origination 
                        let origination = {
                            number: cod_solicitud ? cod_solicitud : 0,
                            activityNumber: cod_solicitud_actividad ? cod_solicitud_actividad : 0,
                            fullNumber: cod_solicitud_completa ? cod_solicitud_completa : "",
                        }

                        // Client
                        let client = {
                            id: cod_cliente ? cod_cliente : 0,
                            documentTypeId: extra.documentTypeId,
                            documentType: data.tipo_doc_letra,
                            documentNumber: data.nro_doc,
                            documentTypeInternalValue: extra.documentTypeInternalValue
                        };

                        if (scoring) {
                            client = {
                                ...client,
                                fullName: scoring.row.nombres,
                            }
                        }

                        // ADN
                        if (adn) {
                            client = {
                                ...client,
                                email: adn.email !== null ? adn.email : ""
                            }
                            adn = {
                                lic: adn.lic === "N" || adn.lic === null ? 1 : 0,
                                pep: adn.pep === "N" || adn.pep === null ? 1 : 0,
                                error: adn.responseDesc
                            }
                        }

                        if (sae) {
                            sae = {
                                lineSAE: sae.linea_sae ? sae.linea_sae : 0,
                                lineAvailable: sae.linea_tc ? sae.linea_tc : 0,
                                maxAmount: sae.monto_max ? sae.monto_max : 0,
                                pctSAE1: sae.pct_sae_1 ? sae.pct_sae_1 : "",
                                pctSAE2: sae.pct_sae_2 ? sae.pct_sae_2 : "",
                                baseDate: sae.fecha_act ? sae.fecha_act : "",
                                block: sae.flg_bq2 ? sae.flg_bq2 : ""
                            };
                        }

                        let dataFC = {
                            origination: {
                                ...origination
                            },
                            firstCallCda: {
                                ...primeraLlamadaCda,
                            },
                            secondCallCda: {
                                ...segundaLlamadaCda,
                            },
                            client: {
                                ...client,
                            },
                            adn: {
                                ...adn
                            },
                            fraudPrevention: fraude ? fraude : null,
                            siebel: {
                                ...solicitud_EnProceso_Siebel
                            },
                            validations: validacionesTotales,
                            sae: {
                                ...sae
                            }
                        }
                        dispatch({
                            type: ODC_REGULAR_CONSULT_CLIENT_SUCCESS,
                            data: dataFC,
                            error: response.data.errorMessage,
                            extra: extra
                        });
                    } else {
                        dispatch({
                            type: ODC_REGULAR_CONSULT_CLIENT_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });

                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: ODC_REGULAR_CONSULT_CLIENT_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}
// POST
export function registerClient(data) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: ODC_REGULAR_REGISTER_CLIENT_REQUEST
        });
        const dataEncrypt = sessionStorage.getItem('data');
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        let ODCRegisterClient = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            //Register
            cod_cliente: data.cod_cliente,
            cod_tipo_documento: data.cod_tipo_documento,
            des_nro_documento: data.des_nro_documento,
            cod_cliente_documento: data.cod_cliente_documento,
            des_primer_nom: data.des_primer_nom,
            des_segundo_nom: data.des_segundo_nom,
            des_ape_paterno: data.des_ape_paterno,
            des_ape_materno: data.des_ape_materno,
            fec_nacimiento: data.fec_nacimiento,
            cod_genero: data.cod_genero,
            cod_estado_civil: data.cod_estado_civil, // Change 
            cod_nacionalidad: data.cod_nacionalidad,
            cod_pais_residencia: data.cod_pais_residencia,
            cod_gradoacademico: data.cod_gradoacademico,
            des_correo: data.des_correo,
            des_telef_celular: data.des_telef_celular,
            des_telef_fijo: data.des_telef_fijo,
            cod_tipo_cliente: data.cod_tipo_cliente,
            cod_situacion_laboral: data.cod_situacion_laboral,
            cod_cargo_profesion: data.cod_cargo_profesion,
            cod_actividad_economica: data.cod_actividad_economica,
            fec_ingreso_laboral: data.fec_ingreso_laboral,
            des_ruc: data.des_ruc,
            des_razon_social: data.des_razon_social,
            num_ingreso_bruto: data.num_ingreso_bruto,
            des_telefono_laboral: data.des_telefono_laboral,
            des_anexo_laboral: data.des_anexo_laboral,
            flg_autoriza_datos_per: data.flg_autoriza_datos_per,
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_fecha_pago: data.cod_fecha_pago,
            cod_envio_correspondencia: data.cod_envio_correspondencia,
            cod_estado_cuenta: data.cod_estado_cuenta,
            // flg_retiro_efectivo:data.flg_retiro_efectivo, 
            // flg_alerta_uso_tar:data.flg_alerta_uso_tar, 
            // cod_envio_comunicacion:data.cod_envio_comunicacion,
            // flg_envio_comunicacion:data.flg_envio_comunicacion,
            flg_consumo_internet: data.flg_consumo_internet,
            flg_consumo_extranjero: data.flg_consumo_extranjero,
            flg_retirar_efectivo: data.flg_retirar_efectivo,
            flg_sobregirar_credito: data.flg_sobregirar_credito,
            flg_notificacion_email: data.flg_notificacion_email,
            num_monto_minimo: data.num_monto_minimo,
            cod_resp_promotor: data.cod_resp_promotor,
            cod_resp_registro: data.cod_resp_registro,
            cod_resp_actualiza: data.cod_resp_actualiza,
            cod_agencia: decryptData.agency ? decryptData.agency.ide_agencia : 0,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_valor_color: data.cod_valor_color,
            disp_efec_porcentaje: data.disp_efec_porcentaje, // Default
            disp_efec_monto: data.disp_efec_monto, // Default
            cod_valor_marca: data.cod_valor_marca, // Default
            linea_credito_oferta: data.linea_credito_oferta, // Default
            linea_credito_final: data.linea_credito_final, // Default
            list_direccion: data.list_direccion,
            cod_producto: data.cod_producto,
            cod_motivo_emboce: data.cod_motivo_emboce,
            des_motivo_comentario_emboce: data.des_motivo_comentario_emboce,
            cod_laboral: data.cod_laboral,

            flg_biometria: data.flg_biometria,
            cod_tipo_relacion: data.cod_tipo_relacion,
            cod_tipo_solicitud: data.cod_tipo_solicitud,
            cod_tipo_solicitud_requerimiento: data.cod_tipo_solicitud_requerimiento,

            des_segmento: data.des_segmento,
        })
        return new Promise((resolve, reject) => {
            axios
                .post(`${URL_ODC_REGULAR}/api/Regular/Cliente/RegistraCliente`, ODCRegisterClient, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    }
                })
                .then(response => {
                    // Launch Dispatch
                    if (response.data) {
                        if (response.data.success) {
                            dispatch({
                                type: ODC_REGULAR_REGISTER_CLIENT_SUCCESS,
                                data: response.data
                            });
                        } else {
                            dispatch({
                                type: ODC_REGULAR_REGISTER_CLIENT_INVALID,
                                data: null,
                                error: response.data.errorMessage,
                                response: true
                            });
                        }
                    }
                    resolve(response);
                })
                .catch(error => {
                    // Error Request
                    let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                    let response = error.response ? true : false;
                    dispatch({
                        type: ODC_REGULAR_REGISTER_CLIENT_ERROR,
                        error: newError,
                        response: response
                    });
                    reject(error);
                });
        })
    }

}

// POST
// export function clienteValidaCuentaTC(data) {
//     return (dispatch) => {
//         dispatch({
//             type: PMP_REGULAR_CLIENTE_VALIDA_CUENTA_REQUEST
//         });
//         return axios
//             .post(`${URL_ODC_REGULAR}/api/Regular/Cliente/ValidacionCuentaTC`, data, {
//                 headers: {
//                     'Accept': 'application/json',
//                     'Content-Type': 'application/json',
//                 }
//             })
//             .then(response => {
//                 if (response.data) {
//                     // Launch Dispatch 
//                     if (response.data.success) {
//                         dispatch({
//                             type: PMP_REGULAR_CLIENTE_VALIDA_CUENTA_SUCCESS,
//                             data: response.data,
//                             extra: null
//                         });
//                     } else {
//                         dispatch({
//                             type: PMP_REGULAR_CLIENTE_VALIDA_CUENTA_INVALID,
//                             data: null,
//                             error: response.data.errorMessage,
//                             response: true
//                         });
//                     }
//                 }
//                 return response;
//             })
//             .catch(error => {
//                 // Error Request
//                 let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
//                 let response = error.response ? true : false;
//                 dispatch({
//                     type: PMP_REGULAR_CLIENTE_VALIDA_CUENTA_ERROR,
//                     error: newError,
//                     response: response
//                 });
//                 return error;
//             });
//     }
// }


// GET 
export function loadClient(documentTypeId, documentNumber) {
    return (dispatch) => {
        //Begin Request
        dispatch({
            type: ODC_REGULAR_LOAD_CLIENT_REQUEST
        });
        return axios
            .get(`${URL_ODC_REGULAR}/api/Regular/Cliente/PrecargaCliente/${documentTypeId}/${documentNumber}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_REGULAR_LOAD_CLIENT_SUCCESS,
                            data: response.data.precargaCliente
                        });
                    } else {
                        dispatch({
                            type: ODC_REGULAR_LOAD_CLIENT_INVALID,
                            data: [],
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request

                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: ODC_REGULAR_LOAD_CLIENT_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    };
}

// POST
export function commercialOffer(data, extra = null) {
    return (dispatch) => {
        //Begin Request
        dispatch({
            type: ODC_REGULAR_COMMERCIAL_OFFER_REQUEST
        });
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        let sendData = JSON.stringify({
            tipo_doc: data.tipo_doc,
            nro_doc: data.nro_doc,
            fec_reg: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            tipo_doc_letra: data.tipo_doc_letra,
            terminalName: "WEB",
            nom_actividad: data.nom_actividad,
            des_usu_responsable: decryptData.username,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            fec_actividad_ini: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            fec_actividad_fin: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
            cod_valor_color: data.cod_valor_color,
            disp_efec_porcentaje: data.disp_efec_porcentaje,
            disp_efec_monto: data.disp_efec_monto,
            cod_producto: data.cod_producto,
            cod_valor_marca: data.cod_valor_marca,
            linea_credito_oferta: data.linea_credito_oferta,
            linea_credito_final: data.linea_credito_final,
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_cliente: data.cod_cliente,

            cod_agencia: decryptData.agency ? decryptData.agency.ide_agencia : 0,
            fec_act_base_sae: data.fec_act_base_sae ? data.fec_act_base_sae : "",
            flg_bq2: data.flg_bq2 ? data.flg_bq2 : "",

            linea_sae: data.linea_sae ? data.linea_sae : 0,
            linea_sae_final: data.linea_sae_final ? data.linea_sae_final : 0,
            monto_max: data.monto_max ? data.monto_max : 0,
            monto_max_final: data.monto_max_final ? data.monto_max_final : 0,
            pct_sae_1: data.pct_sae_1 ? data.pct_sae_1 : "",
            pct_sae_2: data.pct_sae_2 ? data.pct_sae_2 : "",
            pct_sae_final: data.pct_sae_final ? data.pct_sae_final : "",
            des_cci: data.des_cci ? data.des_cci : "",
            cod_tipo_desembolso: data.cod_tipo_desembolso ? data.cod_tipo_desembolso : 0,
            des_descripcion: data.des_descripcion ? data.des_descripcion : "",
            numero_cuotas: data.numero_cuotas ? data.numero_cuotas : '',
            monto_cuota: data.monto_cuota ? data.monto_cuota : '',
            tcea: data.tcea ? data.tcea : '',
            monto_cci: data.monto_cci ? data.monto_cci : 0,
            monto_efectivo: data.monto_efectivo ? data.monto_efectivo : 0
        });
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Originacion/RegistraOfertaComercial`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_REGULAR_COMMERCIAL_OFFER_SUCCESS,
                            data: response.data.actividad
                        });
                    } else {
                        dispatch({
                            type: ODC_REGULAR_COMMERCIAL_OFFER_INVALID,
                            data: null,
                            error: response.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: ODC_REGULAR_COMMERCIAL_OFFER_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    };
}

// POST
// export function ValidacionClienteDNI(data, extra = null) {
//     return (dispatch) => {
//         //Begin Request
//         dispatch({
//             type: ODC_REGULAR_VALIDATION_CLIENT_DNI_REQUEST
//         });
//         return axios
//             .get(`${URL_ODC_REGULAR}/api/Regular/Cliente/ValidacionClienteDNI`, data, {
//                 headers: {
//                     'Accept': 'application/json',
//                     'Content-Type': 'application/json'
//                 }
//             })
//             .then((response) => {
//                 if (response.data) {
//                     if (response.data.success) {
//                         dispatch({
//                             type: ODC_REGULAR_VALIDATION_CLIENT_DNI_SUCCESS,
//                             data: response.data.comentarios
//                         });
//                     } else {
//                         dispatch({
//                             type: ODC_REGULAR_VALIDATION_CLIENT_DNI_INVALID,
//                             data: null,
//                             error: response.data.errorMessage,
//                             response: true
//                         });
//                     }
//                 }
//                 return response;
//             })
//             .catch(error => {
//                 // Error Request
//                 let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
//                 let response = error.response ? true : false;
//                 dispatch({
//                     type: ODC_REGULAR_VALIDATION_CLIENT_DNI_ERROR,
//                     error: newError,
//                     response: response
//                 });
//                 return error;
//             });
//     };
// }

// POST
export function offerSummary(data, extra = null) {
    return (dispatch) => {
        //Begin Request
        dispatch({
            type: ODC_REGULAR_OFFER_SUMMARY_REQUEST
        });
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();

        let sendData = JSON.stringify({
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            nom_actividad: data.nom_actividad,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip,
            cod_valor_color: data.cod_valor_color,
            disp_efec_porcentaje: data.disp_efec_porcentaje,
            cod_valor_marca: data.cod_valor_marca,
            linea_credito_oferta: data.linea_credito_oferta,
            linea_credito_final: data.linea_credito_final
        });
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Originacion/ResumenOfertaComercial`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_REGULAR_OFFER_SUMMARY_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    } else {
                        dispatch({
                            type: ODC_REGULAR_OFFER_SUMMARY_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: ODC_REGULAR_OFFER_SUMMARY_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    };
}


// POST
export function pendingEmbossing(data, extra=null){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_REGULAR_PENDING_EMBOSSING_REQUEST});
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_requerimiento: data.cod_requerimiento,
            cod_solicitud: data.cod_solicitud, 
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual, 
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior, 
            nom_actividad: data.nom_actividad,
            cod_motivo_emboce: data.cod_motivo_emboce,
            des_motivo_comentario_emboce: data.des_motivo_comentario_emboce,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip
        });
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Originacion/RegistraPendienteEmboce`, sendData, {   
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                // Launch Dispatch 
                if(response.data){
                    if(response.data.success){
                        dispatch({ 
                            type: ODC_REGULAR_PENDING_EMBOSSING_SUCCESS, 
                            data: response.data, 
                            error: response.data.errorMessage, 
                            extra: extra,
                        }); 
                    }
                    else{
                        dispatch({ 
                            type: ODC_REGULAR_PENDING_EMBOSSING_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_REGULAR_PENDING_EMBOSSING_ERROR, error:newError, response: response});
                return error;
            });
    }
}

// POST
export function authorizeActivationProcess(data, extra=null, next=false){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_REQUEST});
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_requerimiento: data.cod_requerimiento,
            cod_solicitud: data.cod_solicitud, 
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual, 
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior, 
            nom_actividad: data.nom_actividad,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip
        });
        return axios
            .post(`${URL_ODC}/Originacion/Express/RegistrarActividadGenericoSimple`, sendData, {   
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })  
            .then(response => {
                // Launch Dispatch 
                if(response.data){
                    if(response.data.success){
                        dispatch({ 
                            type: ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_SUCCESS, 
                            data: response.data, 
                            error: response.data.errorMessage, 
                            extra: extra,
                            next: next
                        }); 
                    }
                    else{
                        dispatch({ 
                            type: ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_REGULAR_AUTHORIZE_ACTIVATION_PROCESS_ERROR, error:newError, response: response});
                return error;
            });
    }
}
// POST
export function rejectProcess(data, extra=null, next=false){
    return (dispatch) => {
        // Begin Request
        dispatch({type: ODC_REGULAR_REJECT_PROCESS_REQUEST});
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data') 
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_solicitud: data.cod_solicitud, 
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual, 
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior, 
            nom_actividad: data.nom_actividad,
            des_observacion: data.des_observacion,
            cod_agencia: decryptData.agency? decryptData.agency.ide_agencia:0,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip

        });
        return axios
            .post(`${URL_ODC}/Originacion/RegistrarActividadRechazar`, sendData, {   
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if(response.data){
                    if(response.data.success){
                        dispatch({ 
                            type: ODC_REGULAR_REJECT_PROCESS_SUCCESS, 
                            data: response.data, 
                            error: response.data.errorMessage, 
                            extra: extra,
                            next: next
                        }); 
                    }
                    else{
                        dispatch({ 
                            type: ODC_REGULAR_REJECT_PROCESS_INVALID, 
                            data: null, 
                            error: response.data.errorMessage, 
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response? error.response.data.errorMessage:SERVER_ERROR_MESSAGE; 
                let response = error.response? true : false;
                dispatch({type: ODC_REGULAR_REJECT_PROCESS_ERROR, error:newError, response: response});
                return error;
            });
    }
}
