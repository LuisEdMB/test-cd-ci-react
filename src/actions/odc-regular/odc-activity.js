import axios from 'axios';
import {
    URL_ODC_REGULAR,
    SERVER_ERROR_MESSAGE
} from '../config';
import {
    decrypt,
    getDeviceData
} from '../../utils/Utils';

// Activity - Cancel
export const ODC_REGULAR_CANCEL_ACTIVITY_REQUEST = 'ODC_REGULAR_CANCEL_ACTIVITY_REQUEST';
export const ODC_REGULAR_CANCEL_ACTIVITY_SUCCESS = 'ODC_REGULAR_CANCEL_ACTIVITY_SUCCESS';
export const ODC_REGULAR_CANCEL_ACTIVITY_INVALID = 'ODC_REGULAR_CANCEL_ACTIVITY_INVALID';
export const ODC_REGULAR_CANCEL_ACTIVITY_ERROR = 'ODC_REGULAR_CANCEL_ACTIVITY_ERROR';
// Activity - Generic
export const ODC_REGULAR_GENERIC_ACTIVITY_REQUEST = 'ODC_REGULAR_GENERIC_ACTIVITY_REQUEST';
export const ODC_REGULAR_GENERIC_ACTIVITY_SUCCESS = 'ODC_REGULAR_GENERIC_ACTIVITY_SUCCESS';
export const ODC_REGULAR_GENERIC_ACTIVITY_INVALID = 'ODC_REGULAR_GENERIC_ACTIVITY_INVALID';
export const ODC_REGULAR_GENERIC_ACTIVITY_ERROR = 'ODC_REGULAR_GENERIC_ACTIVITY_ERROR';
// Activity - Generic - Simple
export const ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_REQUEST = 'ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_REQUEST';
export const ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_SUCCESS = 'ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_SUCCESS';
export const ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_INVALID = 'ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_INVALID';
export const ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_ERROR = 'ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_ERROR';

export function cancelActivity(data, extra = null) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: ODC_REGULAR_CANCEL_ACTIVITY_REQUEST
        });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_requerimiento: data.cod_requerimiento,
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            nom_actividad: data.nom_actividad,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip
        });
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Originacion/RegistrarActividadGenericoSimple`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_REGULAR_CANCEL_ACTIVITY_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                        });
                    } else {
                        dispatch({
                            type: ODC_REGULAR_CANCEL_ACTIVITY_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: ODC_REGULAR_CANCEL_ACTIVITY_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}
export function genericActivity(data, extra = null, next = false) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: ODC_REGULAR_GENERIC_ACTIVITY_REQUEST
        });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_requerimiento: data.cod_requerimiento,
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            nom_actividad: data.nom_actividad,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip
        });
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Originacion/RegistrarActividadGenerico`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_REGULAR_GENERIC_ACTIVITY_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                            next: next
                        });
                    } else {
                        dispatch({
                            type: ODC_REGULAR_GENERIC_ACTIVITY_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true,
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: ODC_REGULAR_GENERIC_ACTIVITY_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}
export function simpleGenericActivity(data, extra = null, next = false) {
    return (dispatch) => {
        // Begin Request
        dispatch({
            type: ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_REQUEST
        });
        // Set Data
        const dataEncrypt = sessionStorage.getItem('data')
        const decryptData = decrypt(dataEncrypt);
        const device = getDeviceData();
        // Launch Dispatch - Server
        let sendData = JSON.stringify({
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            nom_actividad: data.nom_actividad,
            des_usu_reg: decryptData.username,
            des_ter_reg: device.ip
        });
        return axios
            .post(`${URL_ODC_REGULAR}/api/Regular/Originacion/RegistrarActividadGenericoSimple`, sendData, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                // Launch Dispatch 
                if (response.data) {
                    if (response.data.success) {
                        dispatch({
                            type: ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_SUCCESS,
                            data: response.data,
                            error: response.data.errorMessage,
                            extra: extra,
                            next: next
                        });
                    } else {
                        dispatch({
                            type: ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_INVALID,
                            data: null,
                            error: response.data.errorMessage,
                            response: true
                        });
                    }
                }
                return response;
            })
            .catch(error => {
                // Error Request
                let newError = error.response ? error.response.data.errorMessage : SERVER_ERROR_MESSAGE;
                let response = error.response ? true : false;
                dispatch({
                    type: ODC_REGULAR_SIMPLE_GENERIC_ACTIVITY_ERROR,
                    error: newError,
                    response: response
                });
                return error;
            });
    }
}
