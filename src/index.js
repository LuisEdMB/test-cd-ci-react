// React
import React from 'react';
import ReactDOM from 'react-dom';
//React Router Dom
import { BrowserRouter as Router/*, Redirect */} from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { Provider } from 'react-redux'
import rootStore from './stores/root-store';
// CSS Custom 
import './assets/css/index.css';
// Material UI 
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

// Page
import App from './pages/App';
import * as serviceWorker from './serviceWorker';

// Utils 
import {decrypt, getIP} from './utils/Utils';

import { customPalette, customTypography, customOverrides } from './theme';

//Session
import * as Session from './actions/session'

//Config Materail UI Color 
const theme = createMuiTheme({
    typography: customTypography,
    palette: customPalette, 
    overrides: customOverrides
});

const { createHistory } = createBrowserHistory();

if (process.env.NODE_ENV === 'production')
    Session.setIpAddress()

const token = sessionStorage.getItem('token');
const data = sessionStorage.getItem('data');
const device = sessionStorage.getItem('_device');

if(!device){
    // Set Ip
    getIP(ip => {
        sessionStorage.setItem("_device", JSON.stringify({
            ip:ip
        }));
    });
}

//window.onbeforeunload = function (e) {
//    var confirmationMessage = "Warning: Leaving this page will result in any unsaved data being lost. Are you sure you wish to continue?";
//    (e || window.event).returnValue = confirmationMessage; //Gecko + IE
//    return confirmationMessage; //Webkit, Safari, Chrome etc.
//};

if (token && data) {    
    const decryptData = decrypt(data);
    rootStore.dispatch({ type: 'AUTH_USER_LOGIN_SUCCESS', data: decryptData });

    const { profiles } = decryptData;
    const { permissions } = profiles;
    const URL = window.location.href? window.location.href.split(process.env.REACT_APP_URL_BASE).pop(): "";

    //Mapear campo "option.full_url" en API
    if(URL !== "" && URL !== "/")
    {
        const obj = permissions.find(item => item.option.full_url === URL);
        if(!obj){ //Si no tiene permisos lo saca del app

            //Implementar modal de alerta
            
            //Termina la session del usuario
            rootStore.dispatch({ type: 'AUTH_USER_LOGOUT_SUCCESS' });
            sessionStorage.clear();
        }
    }
}
else
{
    rootStore.dispatch({ type: 'AUTH_USER_LOGOUT_SUCCESS' })
    sessionStorage.clear();
}

export const history = createHistory

ReactDOM.render(
    <Provider store={rootStore}>
        <Router >
            <React.Fragment>
                <CssBaseline />
                {/* The rest of your application */}
                <MuiThemeProvider theme={theme}>
                    <App history={history} />
                </MuiThemeProvider>
            </React.Fragment>
        </Router>
    </Provider>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
