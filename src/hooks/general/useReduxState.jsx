// React
import { useState, useEffect } from "react"

// Notify
import * as Notistack from '../../utils/Notistack'

// Utils
import * as Utils from '../../utils/Utils'

function useReduxState({ props, prevProps, notify, options = { setDataWhenErrorOcurred: false } }) {
    const { setDataWhenErrorOcurred, nameState } = options

    const [ state, setState ] = useState({})
    const [ isLoading, setIsLoading ] = useState(false)
    const [ isSuccess, setIsSuccess ] = useState(false)
    const [ isError, setIsError ] = useState(false)

    useEffect(_ => {
        Utils.resolveStateRedux(prevProps, props,
            _ => changeState({ state: null, isLoading: true, isSuccess: false, isError: false }),
            _ => changeState({ state: props.data, isLoading: false, isSuccess: true, isError: false }),
            warningMessage => {
                Notistack.getNotistack(`${ nameState ? nameState + ': ' : '' }${ warningMessage }`, notify, 'warning')
                changeState({ state: setDataWhenErrorOcurred ? props.data : null, isLoading: false, isSuccess: false, isError: true })
            },
            errorMessage => {
                Notistack.getNotistack(`${ nameState ? nameState + ': ' : '' }${ errorMessage }`, notify, 'error')
                changeState({ state: setDataWhenErrorOcurred ? props.data : null, isLoading: false, isSuccess: false, isError: true })
            }
        )
    }, [props])

    function changeState({ state, isLoading, isSuccess, isError }) {
        setState(state)
        setIsLoading(isLoading)
        setIsSuccess(isSuccess)
        setIsError(isError)
    }

    return [ state, isLoading, isSuccess, isError ]
}

export default useReduxState