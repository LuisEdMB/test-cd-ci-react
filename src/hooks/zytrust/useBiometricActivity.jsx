// React
import { useState, useEffect } from "react"

// Notify
import * as Notistack from '../../utils/Notistack'

// Utils
import * as Utils from '../../utils/Utils'

function useBiometricActivity({ props, prevProps, notify }) {
    const [ configButton, setConfigButton ] = useState({
        validateIdentity: {
            disabled: false,
            loading: false
        }
    })
    const [ isSuccess, setIsSuccess ] = useState(false)
    const [ isError, setIsError ] = useState(false)

    useEffect(_ => {
        Utils.resolveStateRedux(prevProps, props, 
            _ => changeState({ configButton: null, isSuccess: false, isError: false }),
            _ => changeState({ configButton: null, isSuccess: true, isError: false }),
            warningMessage => {
                Notistack.getNotistack(`Biometría: ${ warningMessage }`, notify, 'warning')
                changeState({ configButton: { validateIdentity: { disabled: false, loading: false } }, isSuccess: false, isError: true })
            }, 
            errorMessage => {
                Notistack.getNotistack(`Biometría: ${ errorMessage }`, notify, 'error')
                changeState({ configButton: { validateIdentity: { disabled: false, loading: false } }, isSuccess: false, isError: true })
            }
        )
    }, [props])

    function changeState({ configButton, isSuccess, isError }) {
        setConfigButton(configButton)
        setIsSuccess(isSuccess)
        setIsError(isError)
    }

    return [ configButton, isSuccess, isError ]
}

export default useBiometricActivity