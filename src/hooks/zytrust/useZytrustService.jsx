// React
import { useState, useEffect } from "react"

// Utils
import * as Utils from '../../utils/Utils'

function useZytrustService({ props, prevProps }) {
    const [ state, setState ] = useState({})
    const [ configButton, setConfigButton ] = useState({
        contingency: {
            disabled: true
        },
        validateIdentity: {
            disabled: false,
            loading: false
        }
    })
    const [ isLoading, setIsLoading ] = useState(false)
    const [ isSuccess, setIsSuccess ] = useState(false)
    const [ isError, setIsError ] = useState(false)

    useEffect(_ => {
        Utils.resolveStateRedux(prevProps, props,
            _ => changeState({ state: null, configButton: { contingency: { disabled: true }, validateIdentity: { disabled: true, loading: true }},
                isLoading: true, isSuccess: false, isError: false }),
            _ => changeState({ state: props, configButton: null, isLoading: false, isSuccess: true, isError: false }),
            _ => changeState({ state: props, configButton: null, isLoading: false, isSuccess: false, isError: true }),
            _ => changeState({ state: props, configButton: null, isLoading: false, isSuccess: false, isError: true })
        )
    }, [props])

    function changeState({ state, configButton, isLoading, isSuccess, isError }) {
        setState(state)
        setConfigButton(configButton)
        setIsLoading(isLoading)
        setIsSuccess(isSuccess)
        setIsError(isError)
    }

    return [state, configButton, isLoading, isSuccess, isError]
}

export default useZytrustService