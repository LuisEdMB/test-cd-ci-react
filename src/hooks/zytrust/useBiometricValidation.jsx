// React
import { useState, useEffect } from "react"

// Notify
import * as Notistack from '../../utils/Notistack'

// Utils
import * as Utils from '../../utils/Utils'

function useBiometricValidation({ props, prevProps, notify }) {
    const [ state, setState ] = useState({})
    const [ configButton, setConfigButton ] = useState({
        contingency: {
            disabled: true
        },
        validateIdentity: {
            disabled: false,
            loading: false
        }
    })
    const [ isLoading, setIsLoading ] = useState(false)
    const [ isSuccess, setIsSuccess ] = useState(false)
    const [ isError, setIsError ] = useState(false)

    useEffect(_ => {
        Utils.resolveStateRedux(prevProps, props, 
            _ => changeState({ state: null, configButton: { contingency: { disabled: true }, validateIdentity: { disabled: true, loading: true } },
                    isLoading: true, isSuccess: false, isError: false }),
            _ => changeState({ state: props.data, configButton: null, isLoading: false, isSuccess: true, isError: false }),
            warningMessage => {
                Notistack.getNotistack(`Biometría: ${ warningMessage }`, notify, 'warning')
                changeState({ state: null, configButton: { validateIdentity: { disabled: false, loading: false } }, isLoading: false, isSuccess: false, isError: true })
            }, 
            errorMessage => {
                Notistack.getNotistack(`Biometría: ${ errorMessage }`, notify, 'error')
                changeState({ state: null, configButton: { validateIdentity: { disabled: false, loading: false } }, isLoading: false, isSuccess: false, isError: true })
            }
        )
    }, [props])

    function changeState({ state, configButton, isLoading, isSuccess, isError }) {
        setState(state)
        setConfigButton(configButton)
        setIsLoading(isLoading)
        setIsSuccess(isSuccess)
        setIsError(isError)
    }

    return [ state, configButton, isLoading, isSuccess, isError ]
}

export default useBiometricValidation