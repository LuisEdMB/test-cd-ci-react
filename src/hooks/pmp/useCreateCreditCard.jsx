// React
import { useState, useEffect } from 'react'

// Notify
import * as Notistack from '../../utils/Notistack'

// Utils
import * as Utils from '../../utils/Utils'

function useCreateCreditCard({ props, prevProps, notify, options }) {
    const { flowName } = options

    const [ state, setState ] = useState({})
    const [ isLoading, setIsLoading ] = useState(false)
    const [ isSuccess, setIsSuccess ] = useState(false)
    const [ isError, setIsError ] = useState(false)

    useEffect(_ => {
        Utils.resolveStateRedux(prevProps, props,
            _ => changeState({ state: null, isLoading: true, isSuccess: false, isError: false }),
            _ => {
                Notistack.getNotistack(`Tarjeta ${ flowName }: PMP Crear Tarjeta Ok!`, notify, 'success')
                changeState({ state: props.data, isLoading: false, isSuccess: true, isError: false })
            },
            warningMessage => {
                Notistack.getNotistack(warningMessage, notify, 'error')
                changeState({ state: null, isLoading: false, isSuccess: false, isError: true })
            },
            errorMessage => {
                Notistack.getNotistack(errorMessage, notify, 'error')
                changeState({ state: null, isLoading: false, isSuccess: false, isError: true })
            }
        )
    }, [props])

    function changeState({ state, isLoading, isSuccess, isError }) {
        setState(state)
        setIsLoading(isLoading)
        setIsSuccess(isSuccess)
        setIsError(isError)
    }

    return [ state, isLoading, isSuccess, isError ]
}

export default useCreateCreditCard