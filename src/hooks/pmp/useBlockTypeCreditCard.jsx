// React
import { useState, useEffect } from "react"

// Notify
import * as Notistack from '../../utils/Notistack'

// Utils
import * as Utils from '../../utils/Utils'

function useBlockTypeCreditCard({ props, prevProps, notify, options }) {
    const { typeBlockName, flowName } = options
    
    const [ isLoading, setIsLoading ] = useState(false)
    const [ isSuccess, setIsSuccess ] = useState(false)
    const [ isError, setIsError ] = useState(false)

    useEffect(_ => {
        Utils.resolveStateRedux(prevProps, props,
            _ => changeState({ isLoading: true, isSuccess: false, isError: false }),
            _ => {
                Notistack.getNotistack(`${ typeBlockName } ${ flowName }: PMP ${ typeBlockName } TC Ok!`, notify, 'success')
                changeState({ isLoading: false, isSuccess: true, isError: false })
            },
            warningMessage => {
                Notistack.getNotistack(warningMessage, notify, 'error')
                changeState({ isLoading: false, isSuccess: false, isError: true })
            },
            errorMessage => {
                Notistack.getNotistack(errorMessage, notify, 'error')
                changeState({ isLoading: false, isSuccess: false, isError: true })
            }
        )
    }, [props])

    function changeState({ isLoading, isSuccess, isError }) {
        setIsLoading(isLoading)
        setIsSuccess(isSuccess)
        setIsError(isError)
    }

    return [ isLoading, isSuccess, isError ]
}

export default useBlockTypeCreditCard