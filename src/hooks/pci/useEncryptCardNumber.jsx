// React
import { useState, useEffect } from 'react'

// Notify
import * as Notistack from '../../utils/Notistack'

// Utils
import * as Utils from '../../utils/Utils'

function useEncryptCardNumber({ props, prevProps, notify, options }) {
    const { flowName } = options

    const [ state, setState ] = useState({})
    const [ isLoading, setIsLoading ] = useState(false)
    const [ isSuccess, setIsSuccess ] = useState(false)
    const [ isError, setIsError ] = useState(false)
    const [ errorCode, setErrorCode ] = useState(null)

    useEffect(_ => {
        Utils.resolveStateRedux(prevProps, props,
            _ => changeState({ state: null, isLoading: true, isSuccess: false, isError: false, errorCode: null }),
            _ => {
                Notistack.getNotistack(`Tarjeta ${ flowName }: PCI Tarjeta Ok!`, notify, 'success')
                changeState({ state: props.data, isLoading: false, isSuccess: true, isError: false, errorCode: null })
            },
            (warningMessage, errorCode) => {
                Notistack.getNotistack(`Tarjeta ${ flowName }: PCI - ${ warningMessage }`, notify, 'warning')
                changeState({ state: null, isLoading: false, isSuccess: false, isError: true, errorCode: errorCode })
            },
            (errorMessage, errorCode) => {
                Notistack.getNotistack(`Tarjeta ${ flowName }: PCI - ${ errorMessage }`, notify, 'error')
                changeState({ state: null, isLoading: false, isSuccess: false, isError: true, errorCode: errorCode })
            }
        )
    }, [props])

    function changeState({ state, isLoading, isSuccess, isError, errorCode }) {
        setState(state)
        setIsLoading(isLoading)
        setIsSuccess(isSuccess)
        setIsError(isError)
        setErrorCode(errorCode)
    }

    return [ state, isLoading, isSuccess, isError, errorCode ]
}

export default useEncryptCardNumber