// React
import React, { useEffect, useState } from 'react'

// Material UI
import { Grid, List, ListItem, ListItemIcon, ListItemText, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import InfoIcon from '@material-ui/icons/Info'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'

// Effects
import Bounce from 'react-reveal/Bounce'

// Components
import Modal from '../Modal'
import ActionButton from '../ActionButton'

const styles = {
    buttonContainer: {
        justifyContent: 'center',
        alignContent: 'center'
    },
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function NotificationSolicitudeBlockedModal(props) {
    const { open = false, onClickOk, wait = true, classes } = props
    const textSplit = props.text?.split('|') || []

    const [ buttonOkVisible, setButtonOkVisible ] = useState(false)

    useEffect(_ => {
        if (open) {
            setButtonOkVisible(false)
            if (wait) setTimeout(_ => setButtonOkVisible(true), 6000)
            else setButtonOkVisible(true)
        }
    }, [open])

    const handleResetSolicitude = _ => {
        onClickOk()
    }

    return (
        <Modal
            title='Originación Bloqueada'
            size='sm'
            body= { 
                textSplit.length > 0
                    ?   <List component='nav' aria-label='info'>
                            {
                                textSplit.map((value, index) => {
                                    if (index === 0)
                                        return  <React.Fragment
                                                    key={ index }>
                                                        <Typography
                                                            key={ index }
                                                            align='justify'>
                                                                { value }
                                                        </Typography>
                                                    <br/>
                                                </React.Fragment>
                                    return (
                                        <ListItem
                                            key={ index }>
                                                <ListItemIcon>
                                                    <InfoIcon 
                                                        fontSize='small'/>
                                                </ListItemIcon>
                                                <ListItemText
                                                    secondary={ value } />
                                        </ListItem>
                                    )
                                })
                            }
                        </List>
                    :   <Typography
                            align='justify'>
                                La originación ha sido bloqueada, y está en proceso de originación por otro usuario.
                        </Typography>
            }
            open={ open } 
            actions={ 
                <Grid
                    container
                    className={ classes.buttonContainer }
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                {
                                    buttonOkVisible &&  <Bounce>
                                                            <div className={ classes.buttonProgressWrapper }>
                                                                <ActionButton 
                                                                    text='Aceptar'
                                                                    type='primary'
                                                                    handleAction={ _ => handleResetSolicitude() }
                                                                    icon={ 
                                                                        <CheckCircleIcon 
                                                                            fontSize='small' 
                                                                            className='ml-2'/>
                                                                    }/>
                                                            </div>
                                                        </Bounce>
                                }
                        </Grid>
                </Grid>
        }/>
    )
}

export default React.memo(withStyles(styles)(NotificationSolicitudeBlockedModal))