import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter }  from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// Components
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DrawerUserPanel from './DrawerUserPanel';
import DrawerListMenu from './DrawerListMenu';
import CencosudLogo from '../assets/media/images/png/Cencosud-1.png';
// Icons 
import LogOutIcon from '@material-ui/icons/PowerSettingsNew';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MenuIcon from '@material-ui/icons/Menu';
// Actions
import { logout } from '../../actions/session';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: drawerWidth,
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: "0",
    margin: 0,
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  title: {
    display: 'none',
    fontSize: 16,
    [theme.breakpoints.up('sm')]: {
        display: 'block',
    },
  },
  content: {
    flexGrow: 1,
    padding: "0 .5em",
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4,
  }, 
  link:{ 
    color:"white", 
    transform:".3s",
  }
});

const mapStateToProps = (state, props) => {
    return {
        
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        logout: bindActionCreators(logout, dispatch)
    };
    return actions;
}

class PersistentDrawerLeft extends Component {

    state = {
        open: false,
        openList: true, 
        selectedListItemIndex: 1,
        anchorEl: null,
        offSetWidthHeader: 0,
    }

    handleDrawerOpen = () => {
        this.setState({ open: true });
    }

    handleDrawerClose = () => {
        this.setState({ open: false });
    }

    handledLogout = (e) => {
        e.preventDefault();
        this.props.logout();  
    }

    handleClickListItemMenu = (e) => {
        this.setState(state => ({ openList: !state.openList }));
    }
    handleListItemClick = (e, index) => {
        this.setState({ selectedListItemIndex: index });
    }

    handleClickUserProfile = e => {
        this.setState({ anchorEl: e.currentTarget });
    }

    handleCloseUserProfile = () => {
        this.setState({ anchorEl: null });
    }
   
    render() {
        const { open } = this.state;
        const { classes, theme,  session } = this.props;
        const { authenticated } = session;

        return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={classNames(classes.appBar, "border-bottom border-white", {
                    [classes.appBarShift]: open
                })}>
                <Toolbar disableGutters={!open}>
                    {/* Button Menu - Show | Hide */}   
                    <Typography variant="h6" color="inherit" noWrap>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerOpen}
                            className={classNames(classes.menuButton, open && classes.hide)}>
                            <MenuIcon />
                        </IconButton>
                    </Typography>

                    { 
                        /* Header Main */
                        authenticated && <React.Fragment>
                            
                              <Typography className={classes.title} variant="h6" color="inherit" noWrap>
                                  <Link to="/" className={classes.link}> 
                                    <Typography component="span" color="inherit" className="text-shadow-black text-uppercase">
                                        Plataforma de Originación de Clientes 
                                    </Typography> 
                                  </Link>
                              </Typography>
                           
                            <div style={{flexGrow:1}}></div>
                            <DrawerUserPanel user={session.data} />
                            {/* Button Menu Items */}   
                            <Tooltip title="Click para salir del sistema.">
                                <IconButton
                                    className="mx-2"
                                    aria-label="More"
                                    color="inherit"
                                    aria-haspopup="true"
                                    onClick={this.handledLogout}>
                                    <LogOutIcon />
                                </IconButton>
                            </Tooltip>      
                        </React.Fragment>
                    }
                </Toolbar>
            </AppBar>
            {/* Items - Aside */}
            
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}>
                {/* Panel - Menu Head */}
                <div className={classes.drawerHeader}>
                    {/* Panel - Menu Head Avatar */}
                    <Link
                      to="/"
                      >
                        <img 
                            // height="50"
                            // width="160"
                            src={CencosudLogo}
                            alt="Cencosud"/>   
                    </Link>
                    {/* Panel - Menu Head Icon */}
                    <IconButton onClick={this.handleDrawerClose} className="mx-2">
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                        
                    </IconButton>
                </div>
                <Divider />
                {/* Panel - Menu Option */}
                {
                    authenticated === true &&(
                        <DrawerListMenu
                            data={session.data}
                        />
                    ) 
                }
            </Drawer>
            {/* Section - Main */}
            <main
                id="mainChildren"
                className={classNames(classes.content, { [classes.contentShift]: open,})}>
                <div className={classNames(classes.drawerHeader, "mb-4")} style={{minHeight:"45px"}}/>
                { this.props.children }
            </main>
        </div>
        );
    }
}

PersistentDrawerLeft.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(PersistentDrawerLeft)));