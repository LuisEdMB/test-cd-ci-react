
import React from "react";
// React Router Dom 
import { withStyles } from '@material-ui/core/styles';
// React Router Dom 
import Tooltip from "@material-ui/core/Tooltip";
import CircularProgress from '@material-ui/core/CircularProgress';
import orange from '@material-ui/core/colors/orange';
import IconButton from '@material-ui/core/IconButton';
import LogOutIcon from '@material-ui/icons/PowerSettingsNew';

const styles = theme => ({
    logoutButtonWrapper:{
		position: 'relative',
		margin: "0 .5em 0 1em",
	},
	logoutButtonProgress:{
		color: orange[700],
		position: 'absolute',
		top:-3,
		left:-3,
		zIndex: -10,
	}, 
	icon:{
		color:"white"
	}
});

const DrawerOptionButton = ({ handledLogout, classes, session, ...props }) => (
        <div>
            <Tooltip title="Click para salir del sistema.">
                <div className={classes.logoutButtonWrapper}>
                    <IconButton
                        disabled={session.loading}
                        aria-label="LogOut"
                        color="inherit"
                        aria-haspopup="true"
                        size="small"
                        onClick={handledLogout}>
                        <LogOutIcon className={classes.icon}/>
                    </IconButton>
                    {// className={classes.logoutButtonWrapper}
                        session.loading && <CircularProgress size={54} className={classes.logoutButtonProgress} />
                    }
                </div>
            </Tooltip>    
        </div>
);

export default withStyles(styles)(DrawerOptionButton);
