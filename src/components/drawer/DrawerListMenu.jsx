import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Link }  from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import Icon from '@material-ui/core/Icon';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { URL_BASE } from '../../actions/config';

const styles = theme => ({
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    }
})
class DrawerMenu extends Component{
    state = {
        openList: true,
        selectedListItemIndex:0, 
        index:-1
    }
    
    handleClickListItemMenu = (e) => {        
        this.setState(state => ({ openList: !state.openList }));
    }

    handleListItemClick = (e, index) => {
        //alert(index)
        this.setState({ selectedListItemIndex: index });


    }

    handleClick = (e, index) => {
        if(this.state.index === index){
            this.setState({ index: -1 });
        }
        else{
            this.setState({ index: index });
        }
    }

    render(){
        let { data, classes } = this.props;
        let menuId = -1;
        let submenuId = -1;
        var menuFather = [];
        // Start Logic
        data.profiles.permissions.sort((a,b) => (a.option.levelId || b.option.levelId) > 1 && 
            ((a.option.order > b.option.order) ? 1 : ((b.option.order > a.option.order) ? -1 : 0)))
        .filter(permission => permission.option.visible && !permission.option.active)
        .map((permission, index) => {
            if (menuId !== permission.option.id){
                if (permission.option.fatherOptionId === 0){
                    let children = data.profiles.permissions.filter((element) => {
                        return element.option.fatherOptionId === permission.option.id;
                    })
                    menuFather.push( {
                        id:1, 
                        component:<React.Fragment key={index}>
                            <Link
                                onClick={ (e) => children.length > 0  && e.preventDefault() } 
                                to={`/${URL_BASE}/${permission.option.url !== "/"? permission.option.url:""}`}>
                                <ListItem button onClick={(e) => this.handleClick(e, index)}>
                                    <ListItemIcon>
                                        <Icon>
                                            { permission.option.icon }
                                        </Icon>
                                    </ListItemIcon>
                                    <ListItemText inset primary={permission.option.name} />
                                    {
                                        children.length > 0 ? 
                                            this.state.index === index? 
                                            <ExpandLessIcon className="text-gray" /> :
                                            <ExpandMoreIcon className="text-gray" /> : ""
                                    }
                                </ListItem> 
                            </Link>
                            <Collapse in={this.state.index === index} timeout="auto" unmountOnExit>
                            {
                                data.profiles.permissions
                                .filter(permission => permission.option.visible && !permission.option.active)
                                .map((child, index) => {
                                    if (child.option.fatherOptionId === permission.option.id){
                                        if (submenuId !== child.option.id){
                                            submenuId = child.option.id;
                                            return (
                                                <List component="div" disablePadding key={index}>
                                                    <Link 
                                                        to={permission.option.url !== "/"?
                                                            `/${URL_BASE}/${permission.option.url}/${child.option.url}`
                                                            :`/${URL_BASE}/${child.option.url}`
                                                        } 
                                                        onClick={this.props.toggleDrawer}
                                                        className="d-flex w-100"> 
                                                        {/* Option Child */}
                                                        <ListItem 
                                                            color="primary"
                                                            button className={classes.nested}
                                                            selected={this.state.selectedListItemIndex === index}
                                                            onClick={e => this.handleListItemClick(e, index)}>
                                                            {/* Option - Icon */}
                                                            <ListItemIcon>
                                                                <Icon>
                                                                    {child.option.icon}
                                                                </Icon>
                                                            {/* Option - Name */}
                                                            </ListItemIcon>
                                                            <ListItemText inset primary={child.option.name} />
                                                        </ListItem>
                                                    </Link>
                                                </List>
                                            )
                                        }
                                    }
                                    return null;
                                })
                            }
                        </Collapse>
                    </React.Fragment>
                    })
                }
                menuId = permission.option.id
            }
            return null;
        }); 
        return (
            <List component="nav">
             {
                 menuFather.map((item, index) => (
                     item.component
                 ))
             }
            </List>
        );
    }
}



export default withStyles(styles)(DrawerMenu);