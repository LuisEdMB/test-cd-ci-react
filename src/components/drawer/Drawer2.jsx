import React,{Component} from 'react';
import classNames from 'classnames';
import { Link }  from 'react-router-dom';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import DrawerUserPanel from './DrawerUserPanel';
import DrawerListMenu from './DrawerListMenu';
import orange from '@material-ui/core/colors/orange';
// Icons 
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MenuIcon from '@material-ui/icons/Menu';
// Images 
import CencosudLogo from '../../assets/media/images/png/Cencosud-1.png';
import DrawerOptionButton from './DrawerOptionButton';
//URL 
import { URL_BASE } from '../../actions/config';

const styles = theme => ({
	root: {
		width: '100%',
	},
	grow: {
		flexGrow: 1,
	},
	menuButton: {
		marginRight: 20,
	},
	drawerHeader: {
		display: 'flex',
		alignItems: 'center',
		padding: "0",
		margin: 1,
		...theme.mixins.toolbar,
		justifyContent: 'center',
	},
	drawer:{
		minWidth:250
	},
	title: {
		display: 'none',
		fontSize: 16,
		color:'white',
		textTransform: "uppercase",
		textShadow:"1px 1px 6px black",
		[theme.breakpoints.up('md')]: {
		    display: 'block',
		}
  	},
	search: {
		position: 'relative',
		borderRadius: theme.shape.borderRadius,
		backgroundColor: fade(theme.palette.common.white, 0.15),
		'&:hover': {
			backgroundColor: fade(theme.palette.common.white, 0.25),
		},
		marginLeft: 0,
		width: '100%',
		[theme.breakpoints.up('sm')]: {
		marginLeft: theme.spacing.unit,
		width: 'auto',
		},
	},
	searchIcon: {
		width: theme.spacing.unit * 9,
		height: '100%',
		position: 'absolute',
		pointerEvents: 'none',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	inputRoot: {
		color: 'inherit',
		width: '100%',
	},
	inputInput: {
		paddingTop: theme.spacing.unit,
		paddingRight: theme.spacing.unit,
		paddingBottom: theme.spacing.unit,
		paddingLeft: theme.spacing.unit * 10,
		transition: theme.transitions.create('width'),
		width: '100%',
		[theme.breakpoints.up('sm')]: {
		width: 120,
		'&:focus': {
			width: 200,
		},
		},
	},
	logoutButtonWrapper:{
		position: 'relative',
		margin: "0 .5em 0 1em",
	},
	logoutButton:{
		width:45,
		height:45
	},
	logoutButtonProgress:{
		color: orange[700],
		position: 'absolute',
		top:-3,
		left:-3,
		zIndex: -10,
	}, 
	icon:{
		color:"white"
	}
});

class SearchAppBar extends Component {
	state = {
		left: false,
	};
	toggleDrawer = (side, open) => () => {
		this.setState({
		  [side]: open,
		});
	};
	handledLogout = e => {
		e.preventDefault();
		this.props.handleLogout();
	}
	render () {
		const { classes, theme, session } = this.props;
		const { authenticated } = session;
		return (
			<div className={classes.root}>
				<AppBar position="fixed">
					<Toolbar>
						{/* Button Menu - Show | Hide */}   
						<Typography variant="h6" color="inherit" noWrap>
							<IconButton 
								onClick={this.toggleDrawer('left', true)}
								className={classes.menuButton} color="inherit" aria-label="Open drawer">
								<MenuIcon />
							</IconButton>
						</Typography>
						{ 
							/* Header Main */
							authenticated && <React.Fragment>
								<Link to={`/${URL_BASE}`}>
									<Typography className={classes.title} variant="h6" component="span" color="inherit" noWrap>
										Plataforma de Originación de Clientes                             
									</Typography>
								</Link>
								<div style={{flexGrow:1}}></div>
								{/* Button User Panel */}   
								<DrawerUserPanel user={session.data} />
								{/* Button Option Menu */}   
								<DrawerOptionButton handledLogout={this.handledLogout} session={session}/>
							</React.Fragment>
            }
						
					</Toolbar>
				</AppBar>
				<Drawer 
					anchor="left"
					open={this.state.left} onClose={this.toggleDrawer('left', false)}>
					<div
						className={classes.drawer}
						tabIndex={0}
						role="button"
						onKeyDown={this.toggleDrawer('left', false)}
					>
						{/* Panel - Menu Head */}
						<div className={classes.drawerHeader}>
							{/* Panel - Menu Head Avatar */}
							<Link to={`/${URL_BASE}`}>
								<img 
									width="140"
									src={CencosudLogo}
									alt="Cencosud"/>   
							</Link>
							{/* Panel - Menu Head Icon */}
							<IconButton onClick={this.toggleDrawer('left', false)} className="mx-2 d-none">
								{theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
							</IconButton>
						</div>
						<Divider />
 						{/* Panel - Menu Option */}
 						{
							authenticated === true &&(
								<DrawerListMenu
									data={session.data}
									toggleDrawer={this.toggleDrawer('left', false)}
								/>
							) 
						}
						
					</div>
				</Drawer>
				{/* Section - Main */}
					<main>
							<div className={classNames(classes.drawerHeader, "mb-4")} style={{minHeight:"50px"}}/>
							{ this.props.children }
					</main>
			</div>
		);
	}
}

SearchAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(SearchAppBar);
