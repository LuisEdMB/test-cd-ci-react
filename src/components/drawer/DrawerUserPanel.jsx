import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import UserDefault from '../../assets/media/images/png/UserDefault-1.png';
import { withStyles } from '@material-ui/core/styles';

import { Typography } from '@material-ui/core';
import Fade from 'react-reveal/Fade';

const styles = theme => ({
    panelUser: {
        fontSize: 12
    }, 
    fontSize:{
        fontSize:10, 
        [theme.breakpoints.up('sm')]: {
            fontSize:11, 
        },
        [theme.breakpoints.up('md')]: {
            fontSize:12, 
        }
    }
});

const DrawerUserPanel = (props) => {
    let {user, classes} = props;
    return (
        <div className="d-flex align-items-center">
            <div className="d-flex flex-column flex-md-row">
                <Fade top>
                    <Typography 
                        className={classNames("text-shadow-black text-left small border-left border-white ml-2 pl-2" , classes.fontSize)}
                        color="inherit">
                        @{ 
                            user.username? user.username.toUpperCase(): props.userDefault.username
                        }
                    </Typography>
                </Fade>
                <Fade bottom>
                    <Typography className={classNames("text-shadow-black text-left small border-left border-white ml-2 pl-2" , classes.fontSize)}
                        color="inherit">
                        { 
                            user.profiles? user.profiles.name.toString().toUpperCase(): props.userDefault.profile
                        }
                    </Typography>
                </Fade>
                <Fade top>
                    <Typography 
                        className={classNames("text-shadow-black text-left small border-left border-white ml-2 pl-2" , classes.fontSize)}
                        color="inherit">
                        {
                            user.agency? user.agency.nom_age.toUpperCase() : "Sede Principal".toUpperCase()
                        }
                    </Typography>
                </Fade>
            </div>
        </div>
    );
}

const { object } = PropTypes;

DrawerUserPanel.propTypes = {
    user: object.isRequired
};

DrawerUserPanel.defaultProps = {
    userDefault: {
            username:'username',
            url: UserDefault,
            profile: "Sin Perfil", 
            headquarter: "Agencia Wong Benavides"
        }
};

export default withStyles(styles)(DrawerUserPanel);