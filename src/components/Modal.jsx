// React
import React from 'react'

// Material UI
import { Dialog, DialogActions, DialogContent, DialogTitle, Grid, Typography } from "@material-ui/core"

function Modal(props) {
    const {
        title,
        body,
        open,
        actions,
        size = 'xs' } = props

    return (
        <Dialog
            fullWidth
            open={ open }
            maxWidth={ size }
            aria-labelledby='form-dialog'>
                <DialogTitle
                    id='form-dialog'
                    className='bg-metal-blue'>
                        <Typography
                            align='center'
                            component='span'
                            variant='h6'
                            className='text-white text-shadow-black'>
                                { title }
                        </Typography>
                </DialogTitle>
                <DialogContent>
                    <Grid 
                        container 
                        spacing={ 8 }>
                            <Grid
                                item
                                xs={ 12 }>
                                    <br />
                                    { body }
                            </Grid>       
                    </Grid>
                </DialogContent>
                <DialogActions>
                    { actions }
                </DialogActions>
        </Dialog>
    )
}

export default React.memo(Modal)