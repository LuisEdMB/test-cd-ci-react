import React from "react";

import { IconButton } from "@material-ui/core";

import CsvIcon from "../../assets/media/images/svg/csv_icon";

const ObjectToCSV = (data) => {
  let dataCsv = [];
  if (data.length > 0) {
    let header = [
      "                                                  Trama de venta AE / SAE / SAO",
    ];

    let column = Object.keys(data.length > 0 && data[0]);
    column.push('')
    header = header.concat(Array(column.length-1).fill());
    
    dataCsv.push(header.join(";"));
    dataCsv.push(column.join(";"));

    for (let i in data) {
      let d = [];
      let row = data[i];
      for (let key in row) {
        d.push(row[key]);
      }
      d.push('')
      dataCsv.push(d.join(";"));
    }
  }
  return dataCsv.join("\r\n");
};

const downloadFileCSV = ({ nameFile, data }) => {
  const dataCSV = ObjectToCSV(data);
  const blob = new Blob([dataCSV], { type: "text/csv;charset=utf-8;" });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.setAttribute("hidden", "");
  a.setAttribute("href", url);
  a.setAttribute("download", nameFile + ".csv");
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
};

const ExportCSV = ({ nameFile, data }) => (
  <div
    onClick={() => {
      downloadFileCSV({ nameFile, data });
    }}
  >
    <IconButton fontSize="small" style={{ color: "#FFFFFF" }}>
      <CsvIcon />
    </IconButton>
  </div>
);

ExportCSV.defaultProps = {
  data: [],
  nameFile: "Csv Default",
};

export default ExportCSV
