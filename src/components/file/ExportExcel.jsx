import React from 'react';
import Workbook from 'react-excel-workbook';

import { 
    IconButton, 
    Icon
} from '@material-ui/core';



const ExportExcel = ({nameFile, workBook}) => (
    <Workbook filename={`${nameFile}.xlsx`} element={<IconButton fontSize="small"><Icon className="icon-microsoftexcel"> </Icon></IconButton>}>
        {
            workBook.map((item, index)=>(
                <Workbook.Sheet key={index} data={item.sheet.data} name={item.sheet.nameSheet}>
                    {
                        item.sheet.columns.map((column, index) => (
                            <Workbook.Column key={index} label={column.label} value={column.value}/>
                        ))
                    }
                </Workbook.Sheet>
            ))
        }
    </Workbook>
)


ExportExcel.defaultProps = {
    workBook: [
        {   sheet:{
                data: [],
                columns: [{label:"", value:""}],
                nameSheet: "Sheet Default"
            }
        }
    ],
    nameFile: "Excel Default"
};


export default ExportExcel;