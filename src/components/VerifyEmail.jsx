import React, { Component } from "react";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";
// React Router
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// Icons
import EmailIcon from "@material-ui/icons/Email";
import CloseIcon from "@material-ui/icons/Close";
import SendIcon from "@material-ui/icons/Send";
import CheckIcon from "@material-ui/icons/Check";
import ErrorIcon from "@material-ui/icons/Error";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
// Components
import {
  IconButton,
  Grid,
  TextField,
  InputAdornment,
  Button,
  Tooltip,
  Zoom,
} from "@material-ui/core";
import Bounce from "react-reveal/Bounce";
// Utils
import { defaultEmailLengthInput, checkEmailInputRegex } from "../utils/Utils";
// Colors
import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";
// Actions
import {
  sendMailCode,
  verifyMailCode,
  updateClientMail,
} from "../actions/generic/verifyEmail";

const styles = (theme) => ({
  fontSize: {
    fontSize: 16,
    margin: "0 5px",
  },
  verifyEmailContainer: {
    position: "fixed",
    width: 290,
    top: "46.6vh",
    boxShadow: "1px 1px 4px black",
    transition: ".3s",
    height: 180,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "rgba(0,0,0,0.23)",
    borderRadius: ".2em",
    zIndex: 950,
    backgroundColor: "white",
    padding: "0 10px",
  },
  isEnabled: {
    backgroundColor: green[200], //"rgba(0,0,0,0.10)",
    "&:hover": {
      backgroundColor: green[300],
      transform: "rotate(180deg) scale(1.05)",
    },
  },
  isDisabled: {
    backgroundColor: green[50],
  },
  isOnVerifyEmail: {
    right: 0,
  },
  isOffVerifyEmail: {
    right: -290,
  },
  IconButton: {
    top: -10, //37
    left: -50, //37
    position: "absolute",
    zIndex: 950,
    color: "white",
    transition: ".3s",
  },
  validateIcon: {
    color: green,
  },
  errorIcon: {
    color: red,
  },
  icon: {
    fontSize: 20,
  },
});

const mapStateToProps = (state, props) => {
  return {
    verifyEmail: state.verifyEmailReducer,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const actions = {
    sendMailCode: bindActionCreators(sendMailCode, dispatch),
    updateClientMail: bindActionCreators(updateClientMail, dispatch),
    verifyMailCode: bindActionCreators(verifyMailCode, dispatch),
  };
  return actions;
};

class VerifyEmail extends Component {
  state = {
    email: "",
    emailError: false,
    codigo: "",
    codigoError: false,
    loading: false,
    emailConfirmed: false,
  };

  getNotistack(message, variant = "default", duration = 6000) {
    let select = "default";
    switch (variant) {
      case "error":
        select = variant;
        break;
      case "success":
        select = variant;
        break;
      case "warning":
        select = variant;
        break;
      case "info":
        select = variant;
        break;
      default:
        select = variant;
        break;
    }
    // Notistack
    this.props.enqueueSnackbar(message, {
      variant: select,
      autoHideDuration: duration,
      action: (
        <IconButton>
          <CloseIcon size="small" className="text-white" color="inherit" />
        </IconButton>
      ),
    });
  }

  componentDidMount = () => {
    const { email, emailConfirmed } = this.props.client;

    if (!emailConfirmed) {
      let data = {
        cod_solicitud_completa: this.props.origination.fullNumber,
        des_nro_documento: this.props.client.documentNumber,
        des_to: email,
      };

      this.props.sendMailCode(data);
    }

    this.setState({
      email: email,
      emailConfirmed: emailConfirmed ? true : false,
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (
      prevProps.verifyEmail.sendMailCodeService !==
      this.props.verifyEmail.sendMailCodeService
    ) {
      this.setState({
        loading: this.props.verifyEmail.sendMailCodeService.loading,
      });

      // Send Mail Code Service - Success Service
      if (
        !this.props.verifyEmail.sendMailCodeService.loading &&
        this.props.verifyEmail.sendMailCodeService.response &&
        this.props.verifyEmail.sendMailCodeService.success
      ) {
        this.getNotistack(`Enviar Codigo: Codigo enviado con exito`, "success");
      }
      // Send Mail Code Service - Error Service
      else if (
        !this.props.verifyEmail.sendMailCodeService.loading &&
        this.props.verifyEmail.sendMailCodeService.response &&
        !this.props.verifyEmail.sendMailCodeService.success
      ) {
        this.getNotistack(
          `Enviar Codigo: ${this.props.verifyEmail.sendMailCodeService.error}`,
          "error"
        );
      }
      // Send Mail Code Service - Error Service Connectivity
      else if (
        !this.props.verifyEmail.sendMailCodeService.loading &&
        !this.props.verifyEmail.sendMailCodeService.response &&
        !this.props.verifyEmail.sendMailCodeService.success
      ) {
        this.getNotistack(
          `Enviar Codigo: ${this.props.verifyEmail.sendMailCodeService.error}`,
          "error"
        );
      }
    }

    if (
      prevProps.verifyEmail.verifyMailCodeService !==
      this.props.verifyEmail.verifyMailCodeService
    ) {
      this.setState({
        loading: this.props.verifyEmail.verifyMailCodeService.loading,
      });

      // Verify Mail Service - Success Service
      if (
        !this.props.verifyEmail.verifyMailCodeService.loading &&
        this.props.verifyEmail.verifyMailCodeService.response &&
        this.props.verifyEmail.verifyMailCodeService.success
      ) {
        this.setState({ emailConfirmed: true, codigoError: false });
        this.getNotistack(`Verificar Correo: Correo confirmado`, "success");
        let data = {
          cod_cliente: this.props.client.id,
          des_nro_documento: this.props.client.documentNumber,
          des_to: this.state.email,
        };
        this.props.updateClientMail(data);
      }
      // Verify Mail Service - Error Service
      else if (
        !this.props.verifyEmail.verifyMailCodeService.loading &&
        this.props.verifyEmail.verifyMailCodeService.response &&
        !this.props.verifyEmail.verifyMailCodeService.success
      ) {
        this.setState({ emailConfirmed: false, codigoError: true });
        this.getNotistack(
          `Verificar Correo: ${this.props.verifyEmail.verifyMailCodeService.error}`,
          "error"
        );
      }
      // Verify Mail Service - Error Service Connectivity
      else if (
        !this.props.verifyEmail.verifyMailCodeService.loading &&
        !this.props.verifyEmail.verifyMailCodeService.response &&
        !this.props.verifyEmail.verifyMailCodeService.success
      ) {
        this.setState({ emailConfirmed: false, codigoError: true });
        this.getNotistack(
          `Verificar Correo: ${this.props.verifyEmail.verifyMailCodeService.error}`,
          "error"
        );
      }
    }

    if (
      prevProps.verifyEmail.updateClientMailService !==
      this.props.verifyEmail.updateClientMailService
    ) {
      // Update Mail Service - Success Service
      if (
        !this.props.verifyEmail.updateClientMailService.loading &&
        this.props.verifyEmail.updateClientMailService.response &&
        this.props.verifyEmail.updateClientMailService.success
      ) {
        this.getNotistack(`Actualizar Correo: Correo actualizado`, "success");
      }
      // Verify Mail Service - Error Service
      else if (
        !this.props.verifyEmail.updateClientMailService.loading &&
        this.props.verifyEmail.updateClientMailService.response &&
        !this.props.verifyEmail.updateClientMailService.success
      ) {
        this.setState({ emailConfirmed: false, codigoError: true });
        this.getNotistack(
          `Actualizar Correo: ${this.props.verifyEmail.updateClientMailService.error}`,
          "error"
        );
      }
      // Verify Mail Service - Error Service Connectivity
      else if (
        !this.props.verifyEmail.updateClientMailService.loading &&
        !this.props.verifyEmail.updateClientMailService.response &&
        !this.props.verifyEmail.updateClientMailService.success
      ) {
        this.setState({ emailConfirmed: false, codigoError: true });
        this.getNotistack(
          `Actualizar Correo: ${this.props.verifyEmail.updateClientMailService.error}`,
          "error"
        );
      }
    }
  };

  // Check Email Input
  handleBlurTextFieldCheckEmailInput = (e) => {
    e.persist();
    let { value } = e.target;
    this.setState({
      emailError: !checkEmailInputRegex(value),
    });
  };

  onChange = (nameObject) => (e) => {
    e.preventDefault();
    let { value } = e.target;
    this.setState({ [nameObject]: value });
  };

  handleSendCode = (e) => {
    e.preventDefault();
    let data = {
      cod_solicitud_completa: this.props.origination.fullNumber,
      des_nro_documento: this.props.client.documentNumber,
      des_to: this.state.email,
    };

    this.props.sendMailCode(data);
  };

  handleValidateMail = (e) => {
    e.preventDefault();
    let data = {
      cod_solicitud_completa: this.props.origination.fullNumber,
      des_nro_documento: this.props.client.documentNumber,
      code: this.state.codigo,
    };
    this.props.verifyMailCode(data);
  };

  render() {
    const { classes } = this.props;
    const { showVerifyEmail, onClick } = this.props;
    const {
      email,
      emailError,
      codigoError,
      codigo,
      loading,
      emailConfirmed,
    } = this.state;
    return (
      <div
        className={classNames(classes.verifyEmailContainer, {
          [classes.isOnVerifyEmail]: showVerifyEmail,
          [classes.isOffVerifyEmail]: !showVerifyEmail,
        })}
      >
        <div>
          <IconButton
            disabled={emailConfirmed}
            onClick={onClick}
            className={classNames(classes.IconButton, {
              [classes.isDisabled]: emailConfirmed,
              [classes.isEnabled]: !emailConfirmed,
            })}
          >
            {showVerifyEmail ? (
              <KeyboardArrowLeftIcon className={classNames(classes.icon)} />
            ) : (
              <KeyboardArrowRightIcon className={classNames(classes.icon)} />
            )}
          </IconButton>
        </div>

        <form
          method="post"
          onSubmit={this.handleValidateMail}
          autoComplete="off"
        >
          <Grid container spacing={8} className="mb-2">
            <Grid item xs={12}>
              <Grid container spacing={8}>
                {/* Client - E-Mail */}
                <Grid item xs={12}>
                  <TextField
                    required
                    error={emailError}
                    fullWidth={true}
                    label="Correo Electrónico"
                    type="email"
                    margin="normal"
                    color="default"
                    name="email"
                    value={email}
                    onChange={this.onChange("email")}
                    onBlur={this.handleBlurTextFieldCheckEmailInput}
                    InputProps={{
                      inputProps: {
                        readOnly: emailConfirmed,
                        maxLength: defaultEmailLengthInput,
                      },
                      endAdornment: (
                        <InputAdornment position="end">
                          <EmailIcon
                            color={emailError ? "secondary" : "inherit"}
                            fontSize="small"
                          />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
                {/* Client - Email Code */}
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    required
                    error={codigoError}
                    fullWidth={true}
                    label="Código"
                    type="text"
                    margin="normal"
                    color="default"
                    name="codigo"
                    value={codigo}
                    onChange={this.onChange("codigo")}
                    InputProps={{
                      inputProps: {
                        readOnly: emailConfirmed,
                        maxLength: 5,
                      },
                      endAdornment: (
                        <InputAdornment position="end">
                          {codigoError ? (
                            <ErrorIcon
                              className={classNames(classes.errorIcon)}
                              // color={emailError ? "secondary" : "inherit"}
                              fontSize="small"
                            />
                          ) : (
                            <CheckIcon
                              className={classNames(classes.validateIcon)}
                              // color={emailError ? "secondary" : "inherit"}
                              fontSize="small"
                            />
                          )}
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
                {/* Button - Resend */}
                <Grid item xs={6}>
                  <Tooltip
                    TransitionComponent={Zoom}
                    title="Reenviar Código al Correo."
                  >
                    <div>
                      <Bounce>
                        <Button
                          loading={loading.toString()}
                          disabled={emailConfirmed}
                          className={classes.button}
                          type="button"
                          variant="contained"
                          size="small"
                          margin="normal"
                          onClick={this.handleSendCode}
                          fullWidth={true}
                        >
                          Reenviar
                          <EmailIcon fontSize="small" className="ml-2" />
                        </Button>
                      </Bounce>
                    </div>
                  </Tooltip>
                </Grid>
                {/* Button - Validate */}
                <Grid item xs={6}>
                  <Tooltip
                    TransitionComponent={Zoom}
                    title="Validar Codigo del Correo"
                  >
                    <div>
                      <Bounce>
                        <div className={classNames(classes.wrapper)}>
                          <Button
                            loading={loading.toString()}
                            disabled={emailConfirmed}
                            variant="contained"
                            type="submit"
                            className={classNames(classes.button)}
                            color="primary"
                            size="small"
                            fullWidth={true}
                          >
                            Validar
                            <SendIcon fontSize="small" className="ml-2" />
                          </Button>
                        </div>
                      </Bounce>
                    </div>
                  </Tooltip>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </form>
      </div>
    );
  }
}

export default withStyles(styles)(
  withSnackbar(connect(mapStateToProps, mapDispatchToProps)(VerifyEmail))
);
