import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import {
    Paper
} from '@material-ui/core';
import LabelIcon from '@material-ui/icons/Label';

const styles = theme =>({
    root:{
        position:"relative",
        border:"1px solid #80808033",
        borderRadius:".5em",
        maxHeight: 130,
        margin: ".5em 0",
        padding: ".5em .5em 0 .5em",
    },
    iconWrapper:{
        position:"absolute",
        top:-14,
        left:14,
        borderRadius:5,
        width: 60,
        height: 60,
        overflow: "unset",
        color:"white",
        boxShadow:"1px 1px 9px #0000005e",
        display:"flex",
        alignItems:"center",
        justifyContent:"center", 
       
    },
    description:{
        borderTop: "1px solid #eee",
        marginTop: 15,
        padding: ".5em 0",
    },
    title:{
        color:"#999"
    },
    total:{
        color:"#3C4858"
    }
});
const CardSummary = ({
    title,
    classes,
    color,
    icon,
    description,
    total
}) => {
  
    return(
        <div>
            <Paper className={classes.root}>
                <div>
                    <Typography className={classes.iconWrapper} style={{background:color}}>
                       <Typography align="center" color="inherit" component="span" style={{lineHeight: 0}} >
                            {icon}
                       </Typography>
                    </Typography>
                    
                    <Typography align="right">
                        <Typography 
                            className={classes.title} 
                            component="span">
                            {title}
                        </Typography>
                        <Typography 
                            className={classes.total}
                            variant="h5"
                            component="span">
                            {total}
                        </Typography>
                        
                    </Typography>
                </div>
                <div>
                    <Typography className={classes.description}>
                        {description}
                    </Typography>
                </div>
            </Paper>
        </div>
    );
}

CardSummary.defaultProps = {
    title:"Total",
    color:"gray",
    icon:<LabelIcon color="inherit" style={{fontSize:36}}/>,
    total:100, 
    description:"Pendientes"
};

export default withStyles(styles)(CardSummary);


