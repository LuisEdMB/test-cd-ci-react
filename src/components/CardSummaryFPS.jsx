import React from 'react';
import { 
    Paper,
    Typography,
    withStyles,
    Tooltip, 
    Badge,
    IconButton
} from '@material-ui/core';
import {Zoom} from 'react-reveal';
import LabelIcon from '@material-ui/icons/Label';
import ErrorIcon from '@material-ui/icons/Error'
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline'
import HistoryIcon from '@material-ui/icons/History';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled'

const styles = theme =>({
    root:{
        position:"relative",
        border:"1px solid #80808033",
        borderRadius:".5em",
        maxHeight: 130,
        margin: ".5em 0",
        padding: ".5em .5em 0 .5em",
    },
    iconWrapper:{
        position:"absolute",
        top:-14,
        left:14,
        borderRadius:5,
        width: 60,
        height: 60,
        overflow: "unset",
        color:"white",
        boxShadow:"1px 1px 9px #0000005e",
        display:"flex",
        alignItems:"center",
        justifyContent:"center", 
       
    },
    description:{
        borderTop: "1px solid #eee",
        marginTop: 15,
        padding: ".5em 0",
    },
    title:{
        color:"#999"
    },
    total:{
        color:"#3C4858"
    }
});
const CardSummary = ({
    title,
    classes,
    color,
    icon,
    description,
    max=9999, 
    data=[]
}) => {
    const selectIcon = (index) => {
        switch(index) {
            case 0 : return {icon: <CheckCircleOutlineIcon style={{fontSize:36}} />, color: "green"};
            case 1 : return {icon: <PlayCircleFilledIcon style={{fontSize:36}} />, color: "#36A2EB"};
            case 2 : return {icon: <HistoryIcon style={{fontSize:36}} />, color: "orange"};
            case 3 : return {icon: <ErrorIcon style={{fontSize:36}} />, color: "red"};
            default : return {icon: <CheckCircleOutlineIcon style={{fontSize:36}} />, color: "green"};
        }
    }
    return(
        <div>
            <Paper className={classes.root}>
                <div>
                    <Typography className={classes.iconWrapper} style={{background:color}}>
                       <Typography align="center" color="inherit" component="span" style={{lineHeight: 0}} >
                            {icon}
                       </Typography>
                    </Typography>
                    <div className="d-flex justify-content-center">
                        {
                            data.map((item, index) => {
                                return  <Tooltip TransitionComponent={Zoom} title={item.des_indicador} key={index}>
                                            <div className="pl-2">
                                                <IconButton aria-label={`${item.des_indicador}`}>
                                                    <Badge badgeContent={item.contador} max={max} style={{color:selectIcon(index).color} }>
                                                      {selectIcon(index).icon}
                                                    </Badge>
                                                </IconButton>
                                            </div>
                                        </Tooltip>
                            })
                        }
                    </div>
                 
                </div>
                <div>
                    <Typography className="d-flex justify-content-between w-100">
                        <Typography   
                            align="left" 
                            component="span">
                            {description}
                        </Typography>
                    </Typography>
                </div>
            </Paper>
        </div>
    );
}

CardSummary.defaultProps = {
    title:"Total",
    color:"gray",
    icon:<LabelIcon color="inherit" style={{fontSize:36}}/>,
    total:100, 
    description:"Pendientes"
};

export default withStyles(styles)(CardSummary);


