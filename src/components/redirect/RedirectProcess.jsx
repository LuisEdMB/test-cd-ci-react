import React, { Component } from "react";
//import classNames from "classnames";

// Components
import {
    Button,
    //Dialog,
    //DialogTitle,
    //DialogContent,
    //DialogActions,
    //IconButton,
    //InputAdornment,
    //Grid,
    //TextField,
    Tooltip,
    //Typography,
    withStyles,
    Zoom,
} from "@material-ui/core";
import Bounce from "react-reveal/Bounce";
// Icons 
import SendIcon from '@material-ui/icons/Send';

const styles = theme => ({
    fontSize: {
        fontSize: 12,
    },
    root: {
    },
    container: {
    },
    button: {
        textTransform: 'none',
    },
    buttonProgressWrapper: {
        position: 'relative',
    },
    //   IconButton: {
    //     top: -10, //37
    //     left: -50, //37
    //     position: "absolute",
    //     zIndex: 950,
    //     backgroundColor: orange[200], //"rgba(0,0,0,0.10)",
    //     color: "white",
    //     transition: ".3s",
    //     "&:hover": {
    //       backgroundColor: orange[300],
    //       transform: "rotate(180deg) scale(1.05)",
    //     },
    //   },
    //   icon: {
    //     fontSize: 20,
    //   },
    //   creditCardColor: {
    //     fontSize: 11.5,
    //   },

});

class RegularRedirect extends Component {

    state = {
        openModal: false,
    }

    // Open Modal Cancel
    handleOpenModal = () => {
        // this.setState(state => ({
        //     ...state,
        //     openModal: true
        // }));
        this.props.history.push(this.props.link)

    }
    // Close Modal Cancel
    handleCloseModal = () => {
        this.setState(state => ({
            ...state,
            openModal: false
        }));
    }

    handleSubmitRedirect = (e) => {
        e.preventDefault()
        const {// classes,
            history} = this.props
        history.push(this.props.link)
        this.handleCloseModal()
    }

    // // Cancel Express
    // handleCancelExpress = () => {
    //     this.handleCloseModalCancelExpress();
    //     this.handleReset();
    // }

    getProcess = (typeProcess) => {
        switch (typeProcess) {
            case "R": return "Regular";
            case "E": return "Express";
            default: return "Sin Nombre";
        }
    }

    getMessage = () => `Originación ${this.getProcess(this.props.typeProcessInitial)} a Originación ${this.getProcess(this.props.typeProcessFinal)}`

    render() {
        const { classes, process } = this.props;

        return (
            <div className={classes.root}>
                    <Tooltip
                        TransitionComponent={Zoom}
                        title="Cambiar Proceso de Originación"
                    >
                        <div>
                            <Bounce>
                                <Button
                                    type="button"
                                    variant="contained"
                                    size="small"
                                    margin="normal"
                                    onClick={this.handleOpenModal}
                                    fullWidth={true}
                                >
                                    Continuar Flujo {process}
                                    <SendIcon fontSize="small" className="ml-2" />

                                </Button>
                            </Bounce>
                        </div>
                    </Tooltip>

                {/* <Dialog
                    onClose={this.handleCloseModal}
                    open={this.state.openModal}
                    aria-labelledby="form-dialog">
                    <DialogTitle
                        id="form-dialog"
                        className="bg-metal-blue">
                        <Typography align="center" component="span" variant="h6" className="text-white text-shadow-black">
                            Cambiar Proceso de Originación
                        </Typography>
                    </DialogTitle>

                    <form onSubmit={this.handleSubmitRedirect} className={classes.modalRoot} autoComplete="off">
                        <DialogContent>
                            <Typography align="center">
                                ¿Esta seguro de cambiar el proceso de {this.getMessage()}?
                            </Typography>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div className={classes.buttonProgressWrapper}>
                                        <Button
                                            // disabled={odcActivity.cancelActivity.loading}
                                            // className={classes.button}
                                            fullWidth={true}
                                            color="secondary"
                                            size="small"
                                            onClick={this.handleCloseModal}
                                            margin="normal">
                                            No
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classes.buttonProgressWrapper}>
                                        <Button
                                            // disabled={odcActivity.cancelActivity.loading}
                                            // className={classes.button}
                                            type="submit"
                                            margin="normal"
                                            color="primary"
                                            size="small"
                                            fullWidth={true}>
                                            Si
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                                // odcActivity.cancelActivity.loading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog> */}

            </div>
        );
    }
}

RegularRedirect.defaultProps = {
    typeProcessInitial: "E",
    typeProcessFinal: "R",
    process: "Regular",
    link: "/odc/regular",
};

export default withStyles(styles)(RegularRedirect);