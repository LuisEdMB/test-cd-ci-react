import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
//Effects 
import LanguageIcon from '@material-ui/icons/Language';

const styles = theme => ({
    root: {
        width: '100%',
        position: "relative"
    },
    fontSize:{
        fontSize:10, 
        [theme.breakpoints.up('sm')]: {
            fontSize:11, 
        },
        [theme.breakpoints.up('md')]: {
            fontSize:12, 
        }
    }
});

const Footer = ({footer, classes, ...props}) => {
    return (
        <footer
            className='w-100 footer d-flex flex-column flex-sm-row justify-content-between align-items-center p-1 px-2' 
            style={props.style}>
            <div>
                <Typography className={classNames("d-flex white-text font-italic text-white text-shadow-black", classes.fontSize)} >
                    <Typography component="span" color="secondary" className={classNames("font-weight-bold", classes.fontSize)}> 
                        © {footer.year} Copyright: 
                        &nbsp;
                    </Typography>
                    <Typography component="span" color="inherit" className={classes.fontSize}>
                        Todos los derechos reservados - Versión Piloto {footer.version}
                    </Typography>
                </Typography>
            </div>
            <div>
                <a 
                    className='d-none d-md-flex align-items-center text-center font-weight-bold small' 
                    target='_blank' 
                    rel="noopener noreferrer" 
                    href={footer.url}> 
                    <LanguageIcon className="pr-1"/>
                    <Typography component="span" color="inherit" className={classes.fontSize}>
                        {footer.website} 
                    </Typography>
                </a>
            </div>
        </footer>
    );
}

const date =  new Date();
const year = date.getFullYear();
const { object } = PropTypes;

Footer.propTypes = {
    footer: object.isRequired
};

Footer.defaultProps = {
    footer: {
        name:'Banco Cencosud',
        year: year, 
        url: 'https://www.tarjetacencosud.pe',
        website: "www.tarjetacencosud.pe", 
        version: "v1.0 [Certificación]"
    }
};

export default withStyles(styles)(Footer);
