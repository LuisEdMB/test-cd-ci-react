// React
import React, { Component } from 'react';
import classNames from 'classnames';
import { SnackbarProvider, withSnackbar } from 'notistack';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
    Grid,
    Typography,
    withStyles,
} from '@material-ui/core';

// Components
import Confetti from 'react-confetti'
// Images
import TravelBackground1 from '../../assets/media/images/png/Travel-1.png';
import TravelBackground2 from '../../assets/media/images/jpg/Travel-1.jpg';
import TravelBackground3 from '../../assets/media/images/jpg/Travel-2.jpg';
import CencosudScotiabank from '../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg';
import Plane from '../../assets/media/images/png/Plane-1.png';
// Components
import Particles from 'react-particles-js';
// Effects
import Roll from 'react-reveal/Roll';
import Zoom from 'react-reveal/Zoom';
import Fade from 'react-reveal/Fade';
import Bounce from 'react-reveal/Bounce';
import makeCarousel from 'react-reveal/makeCarousel';
import Slide from 'react-reveal/Slide';

import DocumentPanel from './DocumentPanel';

// Actions
import { registerPendingDocuments, registerActivityDocuments } from '../../actions/odc-document/odc-document';

// Utils
import * as Utils from '../../utils/Utils'
import * as Notistack from '../../utils/Notistack';
import PreLoaderImage from '../PreLoaderImage';

const styles = theme => ({
    root: {
        position: "relative",
        width: "100%",
        height: "80vh",
        borderRadius: ".5em",
    },
    button: {
        textTransform: 'none',
    },

    sliderImage: {
        width: "100%",
        height: "80vh",
    },
    particle: {
        position: "absolute",
        top: 0,
        right: 0,
        left: 0,
        zIndex: 100,
        height: "80vh",
        width: "100%",
        overflow: "hidden"
    },
    headerContainer: {
        position: "absolute",
        top: 0,
        right: 0,
        left: 0,
        zIndex: 850,
        height: "80vh",
        width: "100%",
    },
    header: {
        width: 360,
        position: "relative",
        [theme.breakpoints.down('xs')]: {
            width: '80vw'
        },
        [theme.breakpoints.up('sm')]: {
            width: 480
        },
        [theme.breakpoints.up('md')]: {
            width: 600
        }
    },
    title: {
        fontSize: "1.1em",
        [theme.breakpoints.down('sm')]: {

        },
        [theme.breakpoints.up('md')]: {
            fontSize: "1.7em",
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: "1.9em",
        },
    },
    cencosudContainer: {
        width: "100%",
        position: "absolute",
        top: -20,
        left: 0,
        right: 0,
        [theme.breakpoints.down('sm')]: {

        },
        [theme.breakpoints.up('md')]: {
            top: -40,
        },
        [theme.breakpoints.up('lg')]: {

        },
    },
    cencosud: {
        width: 100,
        height: 40,
        [theme.breakpoints.down('sm')]: {

        },
        [theme.breakpoints.up('md')]: {
            width: 220,
            height: 80,
        },
        [theme.breakpoints.up('lg')]: {

        },
    },
    cencosudScotiabank: {
        height: 90,
        width: 130
    }
});

const mapStateToProps = (state, props) => {
    return {
        odcDocument: state.odcDocumentReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        registerPendingDocuments: bindActionCreators(registerPendingDocuments, dispatch),
        registerActivityDocuments: bindActionCreators(registerActivityDocuments, dispatch)
    }
    return actions
}

class BackgroundFinish extends Component {
    state = {
        items: [
            {
                url: TravelBackground1,
                alt: "Travel 1"
            },
            {
                url: TravelBackground2,
                alt: "Travel 2"
            },
            {
                url: TravelBackground3,
                alt: "Travel 3"
            }
        ],
        isFirstCallApiRegisterPendingDocuments: true,
        jsonSent: ''
    }

    componentWillMount() {
        if (this.props.callCenter) return
        this.handleRegisterPendingDocuments()
    }

    componentDidUpdate = prevProps => {
        Utils.resolveStateRedux(prevProps.odcDocument.registerPendingDocuments, this.props.odcDocument.registerPendingDocuments,
            null, _ => {
                Notistack.getNotistack("Documentos pendientes registrados correctamente!", this.props.enqueueSnackbar, "success")
                this.handleRegisterActivityDocument()
            },
            warningMessage => {
                Notistack.getNotistack(warningMessage, this.props.enqueueSnackbar, "error")
                this.handleRegisterActivityDocument()
            },
            errorMessage => {
                Notistack.getNotistack(errorMessage, this.props.enqueueSnackbar, "error")
                this.handleRegisterActivityDocument()
            }
        )
    }

    handleRegisterActivityDocument = _ => {
        const process = (this.props.data || []).map(({ process }) => process)[0];
        const sendData = {
            cod_solicitud: process?.number || 0,
            cod_solicitud_completa: process?.fullNumber || '',
            des_json_doc_input: this.state.jsonSent,
            des_json_doc_output: JSON.stringify(this.props.odcDocument.registerPendingDocuments || '')
        };
        this.props.registerActivityDocuments(sendData);
    }

    handleRegisterPendingDocuments = (isRetry = false) => {
        if (isRetry)
            Notistack.getNotistack("Intentando registrar los documentos pendientes a generar ...",
                this.props.enqueueSnackbar, "info");
        const client = (this.props.data || []).map(({ client }) => client)[0]
        const process = (this.props.data || []).map(({ process }) => process)[0]
        const sendData = {
            cod_solicitud: process?.number || 0,
            cod_solicitud_completa: process?.fullNumber || '',
            des_grupo: Utils.getGroupDescription(process?.typeSolicitudeCode, client?.relationTypeId,
                process?.typeSolicitudeOriginationCodeInitial || process?.typeSolicitudeOriginationCode,
                this.props.isSAEProcess, this.props.isClientExistingProcess),
            cod_tipo_relacion: client?.relationTypeId || 0,
            cod_tipo_solicitud: process?.typeSolicitudeCode || 0,
            cod_tipo_solicitud_originacion: process?.typeSolicitudeOriginationCode || 0,
            cod_tipo_solicitud_originacion_inicial: process?.typeSolicitudeOriginationCodeInitial || 0,
            cod_tipo_solicitud_requerimiento: process?.typeSolicitudeReqCode || 0,
            cod_motivo_reimpresion: process?.rePrintMotiveCode || 0
        }
        this.props.registerPendingDocuments(sendData).then(_ => {
            if (this.state.isFirstCallApiRegisterPendingDocuments)
                this.setState({ isFirstCallApiRegisterPendingDocuments: false })
        })
        this.setState({ jsonSent: JSON.stringify({ documentoPendiente: sendData }) })
    }

    render() {
        const { classes, data = [], callCenter } = this.props;
        const { isFirstCallApiRegisterPendingDocuments } = this.state;
        const CarouselUI = ({ children }) => <div style={{
            position: "absolute",
            top: 0,
            right: 0,
            left: 0,
            overflow: "hidden",
            minHeight: "80vh",
            width: "100%"
        }}>
            {children}
        </div>;

        const Carousel = makeCarousel(CarouselUI);
        if (!isFirstCallApiRegisterPendingDocuments || callCenter)
            return (
                <div className={classNames(classes.root, "bg-blue-transparent rounded")}>
                    <Confetti
                        style={{ zIndex: 100, height: 450, width: '100%' }}
                        className="w-100" />
                    <div>
                        <Carousel defaultWait={3500}>
                            {
                                this.state.items.map((item, index) => {
                                    return (
                                        <Slide key={index} className={classNames(classes.slider, "bg-black-transparent")}>
                                            <Fade>
                                                <li>
                                                    <img
                                                        className={classes.sliderImage}
                                                        src={item.url}
                                                        alt={item.alt} />

                                                </li>
                                            </Fade>
                                        </Slide>
                                    )
                                })
                            }

                        </Carousel>
                    </div>
                    {/* Header */}
                    <div className={classNames(classes.headerContainer, "d-flex justify-content-center align-items-center")}>
                        <Roll top>
                            <Grid
                                container
                                spacing={8}
                                className={classNames(classes.header, "bg-white-transparent rounded p-3")}>
                                <Grid item xs={12} className="mb-2">
                                    <Zoom top>
                                        <Typography
                                            className={classNames("text-white text-shadow-black", classes.title)}
                                            align="center"
                                            variant="h4" color="inherit" component="title">
                                            {this.props.title}
                                        </Typography>
                                    </Zoom>
                                </Grid>
                                {/* Logo -  Cencosud Scotiabank */}
                                <Grid item xs={12}>
                                    <Bounce>
                                        <figure className="d-flex justify-content-center">
                                            <img
                                                className={classes.cencosudScotiabank}
                                                alt={"Cencosud - Scotiabank"}
                                                src={CencosudScotiabank}
                                            />
                                        </figure>
                                    </Bounce>
                                </Grid>
                                {/* Form - Button - Files */}
                                {!callCenter && (<Grid item xs={12}>
                                    <SnackbarProvider max={3} >
                                        <DocumentPanel
                                            data={data}
                                            handleRegisterPendingDocuments={this.handleRegisterPendingDocuments} />
                                    </SnackbarProvider>
                                </Grid>
                                )}
                            </Grid>
                        </Roll>
                    </div>
                    <Particles
                        className={classes.particle}
                        params={{
                            "particles": {
                                "number": {
                                    "value": 8,
                                    "density": {
                                        "enable": true,
                                        "value_area": 800
                                    }
                                },
                                "line_linked": {
                                    "enable": false
                                },
                                "move": {
                                    "speed": 5,
                                    "out_mode": "out"
                                },
                                "shape": {
                                    "type": [
                                        "images",
                                        "circle"
                                    ],
                                    "images": [
                                        {
                                            "src": Plane,
                                            "height": 50,
                                            "width": 50
                                        },
                                        {
                                            "src": Plane,
                                            "height": 50,
                                            "width": 50
                                        },

                                        {
                                            "src": Plane,
                                            "height": 50,
                                            "width": 50
                                        },

                                        {
                                            "src": Plane,
                                            "height": 50,
                                            "width": 50
                                        },
                                    ]
                                },
                                "color": {
                                    "value": "#CCC"
                                },
                                "size": {
                                    "value": 70,
                                    "random": true,
                                    "anim": {
                                        "enable": true,
                                        "speed": 6,
                                        "size_min": 10,
                                        "sync": false
                                    }
                                }
                            },
                            "retina_detect": false
                        }} />
                </div>
            )
        else return <PreLoaderImage text='Registrando documentos pendientes a generar, espere por favor ...' />
    }
}

BackgroundFinish.defaultProps = {
    title: "¡Felicitaciones, ha terminado el proceso de Originación Correctamente!",
    callCenter: false,
    isSAEProcess: false,
    isClientExistingProcess: false
};

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(BackgroundFinish)));