import React, { Component } from "react"
import classNames from 'classnames';
import { withSnackbar } from 'notistack';

import {
    Grid,
    Typography,
    withStyles,
    IconButton,
    CircularProgress,
    Button,
    Divider
} from '@material-ui/core';
import { green } from '@material-ui/core/colors';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Effects
import { Bounce, Rotate, Fade } from 'react-reveal';
// Icons
import RefreshIcon from '@material-ui/icons/Refresh';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import ChromeReaderModeIcon from '@material-ui/icons/ChromeReaderMode';

import { getDocuments, downloadDocument } from '../../actions/odc-document/odc-document';

import { getNotistack } from '../../utils/Notistack';

const styles = theme => ({
    button: {
        textTransform: 'none',
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    contracts: {
        backgroundColor: '#FFFFFF',
    },
    documentsRoot: {
        padding: 15,
        maxHeight: 400,
        overflowY: 'auto'
    },
    documentTitle:{
        textAlign: 'left',
        verticalAlign: "center",
        margin: 'auto 0'
    }
});

const mapStateToProps = (state, props) => {
    return {
        odcDocument: state.odcDocumentReducer,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getDocuments: bindActionCreators(getDocuments, dispatch),
        downloadDocument: bindActionCreators(downloadDocument, dispatch),
    };
    return actions;
}


class DocumentPanel extends Component {

    state = {
        fileModal: false,
        documentsFile: [],
        data: [],
        refreshButtonDisabled: false,
        refreshButtonLoading: false,

        cancelRefreshButtonDisabled: false,
        cancelRefreshButtonLoading: false,
    }

    componentDidMount = () => {
        this.getDocumentsForDocumentPanel()
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.odcDocument !== this.props.odcDocument) {
            // Documents
            if (prevProps.odcDocument.documents !== this.props.odcDocument.documents) {
                if (this.props.odcDocument.documents.loading) {
                    this.setState(state => ({
                        ...state,
                        refreshButtonDisabled: true,
                        cancelRefreshButtonDisabled: true
                    }));
                }
                if (!this.props.odcDocument.documents.loading && this.props.odcDocument.documents.response && this.props.odcDocument.documents.success) {
                    this.setState(state => ({
                        ...state,
                        refreshButtonDisabled: false,
                        cancelRefreshButtonDisabled: false
                    }));
                }
                if (!this.props.odcDocument.documents.loading && this.props.odcDocument.documents.response && !this.props.odcDocument.documents.success) {
                    // Notistack
                    this.setState(state => ({
                        ...state,
                        refreshButtonDisabled: false,
                        cancelRefreshButtonDisabled: false
                    }));
                }
                else if (!this.props.odcDocument.documents.loading && !this.props.odcDocument.documents.response && !this.props.odcDocument.documents.success) {
                    // Notistack
                    getNotistack(this.props.odcDocument.documents.error, this.props.enqueueSnackbar, "error");
                    this.setState(state => ({
                        ...state,
                        refreshButtonDisabled: false,
                        cancelRefreshButtonDisabled: false
                    }));
                }
            }
            // Downloading
            if (prevProps.odcDocument.downloadDocument !== this.props.odcDocument.downloadDocument) {
                if (!this.props.odcDocument.downloadDocument.loading && this.props.odcDocument.downloadDocument.response && !this.props.odcDocument.downloadDocument.success) {
                    // Notistack
                    getNotistack(this.props.odcDocument.downloadDocument.error, this.props.enqueueSnackbar, "error");
                }
                else if (!this.props.odcDocument.downloadDocument.loading && !this.props.odcDocument.downloadDocument.response && !this.props.odcDocument.downloadDocument.success) {
                    // Notistack
                    getNotistack(this.props.odcDocument.downloadDocument.error, this.props.enqueueSnackbar, "error");
                }
            }
            if (prevProps.odcDocument.registerPendingDocuments !== this.props.odcDocument.registerPendingDocuments) {
                this.setState(state => ({
                    ...state,
                    refreshButtonDisabled: this.props.odcDocument.registerPendingDocuments.loading,
                    cancelRefreshButtonDisabled: this.props.odcDocument.registerPendingDocuments.loading
                }));
                if (this.props.odcDocument.registerPendingDocuments.success)
                    this.getDocumentsForDocumentPanel()
            }
        }
    }
    handleClickOpenModalFile = (e) => {
        this.handleClickRefreshFile();
        this.setState(state => ({
            fileModal: true
        }));
    }
    handleClickCloseModalFile = (e) => {
        this.setState(state => ({
            fileModal: false
        }));
    }

    handleSubmitDownloadFiles = (e) => {
        e.preventDefault();
    }

    handleClickRefreshFile = (e) => {
        if (!this.props.odcDocument.registerPendingDocuments.success)
            this.props.handleRegisterPendingDocuments(true)
        else
            this.getDocumentsForDocumentPanel()
    }

    getDocumentsForDocumentPanel() {
        const { data } = this.props;
        this.setState({ refreshButtonDisabled: true, documentsFile: [] })
        if (data.length > 0) {
            data.map((item, index) => {
                let sendData = {
                    cod_solicitud_completa: item.process.fullNumber,
                }
                this.props.getDocuments(sendData)
                    .then(response => {
                        if (response.data && response.data.success)
                            this.setState(state => ({
                                ...state,
                                documentsFile: [{ id: index, client: item.client, document: response.data.documento }],
                                refreshButtonDisabled: false
                            }));
                    });
                return null;
            });
        }
    }

    // Download Document
    handleDownloadDocument = item => e => {
        if (item) {
            let sendData = {
                cod_control: item.cod_control
            }
            this.props.downloadDocument(sendData)
                .then(response => {
                    if (response) {
                        if (response.data) {
                            const url = window.URL.createObjectURL(new Blob([response.data]));
                            let link = document.createElement('a');
                            link.href = url;
                            link.setAttribute('download', `${item.nom_documento}.pdf`);
                            link.click();
                        }
                    }
                });
        }
    }

    render = () => {
        const { classes } = this.props;
        const { //fileModal, 
            documentsFile,
            //refreshButtonLoading, 
            refreshButtonDisabled,
            //cancelRefreshButtonDisabled
        } = this.state;

        return (
            <>
                {/* <Bounce>
                    <div className="d-flex flex-column justify-content-center align-items-center">
                        <div className=" m-2">
                            <Typography className="font-weight-bolder">
                                Descargar Contrato(s)
                            </Typography>
                        </div>
                        <div className=" m-2">
                            <Fab 
                                color="secondary" 
                                onClick={this.handleClickOpenModalFile}
                                variant="extended">
                                <SaveAltIcon  
                                    className="text-white"
                                    fontSize="small" 
                                />
                            </Fab>
                        </div>
                    </div>
                </Bounce> */}

                {/* Modal Files */}
                <div
                    // onClose={this.handleClickCloseModalFile}
                    // open={fileModal}
                    className={classes.contracts}
                    aria-labelledby="form-dialog-document">
                    <div
                        id="form-dialog-document"
                        className="bg-metal-blue">
                        <Typography align="center" component="span" variant="h6" className="text-white text-shadow-black">
                            Documentos del Contrato
                        </Typography>
                    </div>

                    <form onSubmit={this.handleSubmitDownloadFiles} className={classes.modalRoot}>
                        <Grid container className={classes.documentsRoot}>
                            {
                                documentsFile.length > 0 ?
                                    documentsFile.map((item, index) => (
                                        <Grid item xs={12} key={index}>
                                            <div className="d-flex justify-content-between">
                                                <div>
                                                    <Rotate top left>
                                                        <Typography component="span" className="font-weight-bold">
                                                            {
                                                                (item.client.firstName && item.client.lastName) ?
                                                                    `Cliente: ${item.client.firstName} ${item.client.lastName}`.toLocaleUpperCase()
                                                                    : `Cliente: ${item.client.fullName}`.toLocaleUpperCase()
                                                            }
                                                        </Typography>
                                                    </Rotate>
                                                </div>
                                                <div>
                                                    <Rotate top right>
                                                        <ChromeReaderModeIcon />
                                                    </Rotate>
                                                </div>
                                            </div>


                                            {
                                                item.document.map((document, index) => (
                                                    <Grid item xs={12} key={index}>
                                                        <Divider className="my-2" />
                                                        <div className={`d-flex justify-content-between ${classes.documentRow}`}>
                                                            <Fade>
                                                                <Typography component="a" className={classes.documentTitle}>
                                                                    {document.nom_documento}
                                                                </Typography>
                                                            </Fade>
                                                            <Bounce>
                                                                <IconButton onClick={this.handleDownloadDocument(document)} disabled={document.flg_estado === "4" ? false : true} >
                                                                    <SaveAltIcon
                                                                        disabled={document.flg_estado === "4" ? false : true}
                                                                        color={document.flg_estado === "4" ? "secondary" : "inherit"}
                                                                        fontSize="small" />
                                                                </IconButton>
                                                            </Bounce>
                                                        </div>
                                                    </Grid>
                                                ))
                                            }
                                        </Grid>
                                    )) :
                                    <Typography>
                                        Los documentos se estan cargando o se encuentran en proceso de generación, espere por favor.
                                    </Typography>
                            }
                        </Grid>
                        <Grid container spacing={8}>
                            <Grid item xs={12}>
                                <div>
                                    {
                                        refreshButtonDisabled ?
                                            <CircularProgress size={24} className={classes.button} disabled={true} />
                                            :
                                            <Button
                                                disabled={refreshButtonDisabled}
                                                className={classNames(classes.button)}
                                                fullWidth={true}
                                                color="primary"
                                                size="small"
                                                onClick={this.handleClickRefreshFile}
                                                margin="normal"
                                                variant="contained">
                                                Actualizar
                                            <RefreshIcon fontSize="small" className="ml-2" />
                                            </Button>
                                    }
                                </div>
                            </Grid>
                            {/* <Grid item xs={12}>
                                    <div>
                                        <Button 
                                            disabled={cancelRefreshButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth={true} 
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickCloseModalFile}
                                            margin="normal">
                                            Cancelar
                                        </Button>
                                    </div>
                                </Grid> */}
                        </Grid>
                    </form>
                </div>
            </>
        )
    }
}

DocumentPanel.defaultProps = {
    data: []
};

// const DocumentPanelWrapper = () => <SnackbarProvider max={3} > <DocumentPanel {...props}/> </SnackbarProvider>

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(DocumentPanel)));
