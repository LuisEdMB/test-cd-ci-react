import React from 'react';
import { 
    Paper,
    withStyles,
    Tooltip
} from '@material-ui/core';
import { Zoom, Bounce, Fade } from 'react-reveal';
import CencosudScotiaBank from '../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg';
// Icons
import ExportExcel from '../file/ExportExcel';
import Title from '../title/Title';

import { 
    IconButton, 
    Icon
} from '@material-ui/core';

const styles = theme =>({
    avatar:{
        [theme.breakpoints.down('md')]: {
            width: 60, 
            height: 32
        },
        width:80, 
        height:48
    },
    root:{
        display: "flex", 
        justifyContent: "space-between", 
        alignItems: "center",
       
    },
    actionRoot:{
        display: "flex", 
        justifyContent: "center", 
        alignItems: "center"
    }, 
    green:{
        background: `radial-gradient(circle at center, red 0, blue, green 100%)`
    },
    title:{
        [theme.breakpoints.down('md')]: {
            fontSize:14, 
        }
    }
});

const HeaderReportDownloadExcel = ({title, dataExcel, columnExcel, nameSheet, nameFile, classes, downloadDocument}) => (
    <Paper className="p-1" color="primary">
        <figure className={classes.root}>
            <Bounce>
                <img
                    className={classes.avatar}
                    src={CencosudScotiaBank}
                    alt="Cencosud ScotiaBank" 
                    />
            </Bounce>
            <figcaption className="w-100">
                <Fade>
                    <Title 
                        align="center"
                        title={title}
                    />
                </Fade>
            </figcaption>
            <div className={classes.actionRoot}>
                <Bounce>
                    <div>
                        <Tooltip TransitionComponent={Zoom} title="Exportar a Excel. " onClick={downloadDocument}>
                            <div className="border-right pr-2">
                                {/* <ExportExcel nameFile={nameFile} workBook={
                                        [
                                            {   sheet:{
                                                    data: dataExcel,
                                                    columns: columnExcel,
                                                    nameSheet: nameSheet
                                                },
                                                nameFile: nameFile
                                            }
                                        ]
                                    }
                                /> */}
                                <IconButton fontSize="small" ><Icon className="icon-microsoftexcel" > </Icon></IconButton>
                            </div>
                        </Tooltip>
                    </div>
                </Bounce>
            </div>
        </figure>
    </Paper>
)

ExportExcel.defaultProps = {
    title: "",
    dataExcel: [],
    columnExcel: [],
    nameSheet: "Excel",
    nameFile: "Excel Default"
};

export default withStyles(styles)(HeaderReportDownloadExcel);