// React
import React from 'react'

// Material UI
import { Grid, withStyles } from '@material-ui/core'

//Utils
import classNames from 'classnames'

const styles = {
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: '100%'
    }
}

function Image(props) {
    const { src, alt, minHeight = 250, classes, className, ...rest } = props
    return (
        <Grid
            container
            item
            { ...rest }
            className='justify-content-center'>
            <Grid
                item
                className={ classNames(classes.root, 'px-3 py-4 rounded') }>
                    <img
                        className={ classNames(classes.image, className) }
                        style={{ height: minHeight }}
                        src={ src }
                        alt={ alt }/>
            </Grid>
        </Grid>
    )
}

export default React.memo(withStyles(styles)(Image))