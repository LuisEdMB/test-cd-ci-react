import React, { Component } from "react";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";
// React Router
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// Icons
import PhoneAndroid from "@material-ui/icons/PhoneAndroid";
import Message from "@material-ui/icons/Message";
import CloseIcon from "@material-ui/icons/Close";
import SendIcon from "@material-ui/icons/Send";
import CheckIcon from "@material-ui/icons/Check";
import ErrorIcon from "@material-ui/icons/Error";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
// Components
import {
  IconButton,
  Grid,
  TextField,
  InputAdornment,
  Button,
  Tooltip,
  Zoom, CircularProgress,
} from "@material-ui/core";
import Bounce from "react-reveal/Bounce";
// Utils
import { defaultCellphoneLengthInput } from "../utils/Utils";
// Colors
import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";
// Actions
import {
  sendSMSCode,
  verifySMSCode,
  updateClientCellphone,
} from "../actions/generic/verifyCellphone";

const styles = (theme) => ({
  fontSize: {
    fontSize: 16,
    margin: "0 5px",
  },
  verifyCellphoneContainer: {
    position: "fixed",
    width: 290,
    top: "60vh",
    boxShadow: "1px 1px 4px black",
    transition: "all .3s",
    height: 180,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "rgba(0,0,0,0.23)",
    borderRadius: ".2em",
    zIndex: 950,
    backgroundColor: "white",
    padding: "0 10px",
  },
  isEnabled: {
    backgroundColor: green[200], //"rgba(0,0,0,0.10)",
    "&:hover": {
      backgroundColor: green[300],
      transform: "rotate(180deg) scale(1.05)",
    },
  },
  isDisabled: {
    backgroundColor: green[50],
  },
  isOnVerifyCellphone: {
    right: 0,
  },
  isOffVerifyCellphone: {
    right: -290,
  },
  IconButton: {
    top: -10, //37
    left: -50, //37
    position: "absolute",
    zIndex: 950,
    color: "white",
    transition: "all .3s",
  },
  validateIcon: {
    color: green,
  },
  errorIcon: {
    color: red,
  },
  icon: {
    fontSize: 20,
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
    textTransform: "none",
  },
});

const mapStateToProps = (state, props) => {
  return {
    verifyCellphone: state.verifyCellphoneReducer,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const actions = {
    sendSMSCode: bindActionCreators(sendSMSCode, dispatch),
    updateClientCellphone: bindActionCreators(updateClientCellphone, dispatch),
    verifySMSCode: bindActionCreators(verifySMSCode, dispatch),
  };
  return actions;
};

class VerifyCellphone extends Component {
  state = {
    cellphone: "",
    cellphoneError: false,
    codigo: "",
    codigoError: false,
    loading: false,
    cellphoneConfirmed: false,
  };

  getNotistack(message, variant = "default", duration = 6000) {
    let select = "default";
    switch (variant) {
      case "error":
        select = variant;
        break;
      case "success":
        select = variant;
        break;
      case "warning":
        select = variant;
        break;
      case "info":
        select = variant;
        break;
      default:
        select = variant;
        break;
    }
    // Notistack
    this.props.enqueueSnackbar(message, {
      variant: select,
      autoHideDuration: duration,
      action: (
        <IconButton>
          <CloseIcon size="small" className="text-white" color="inherit" />
        </IconButton>
      ),
    });
  }

  componentDidMount = () => {
    const { cellphone, cellphoneConfirmed } = this.props.client;

    if (!cellphoneConfirmed) {
      //let data = {
      //  cod_solicitud_completa: this.props.origination.fullNumber,
      //  des_nro_documento: this.props.client.documentNumber,
      //  des_to: cellphone,
      //};

      // this.props.sendSMSCode(data);
    }

    this.setState({
      cellphone: cellphone,
      cellphoneConfirmed: cellphoneConfirmed,
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (
      prevProps.verifyCellphone.sendSMSCodeService !==
      this.props.verifyCellphone.sendSMSCodeService
    ) {
      this.setState({
        loading: this.props.verifyCellphone.sendSMSCodeService.loading,
      });

      // Send SMS Code Service - Success Service
      if (
        !this.props.verifyCellphone.sendSMSCodeService.loading &&
        this.props.verifyCellphone.sendSMSCodeService.response &&
        this.props.verifyCellphone.sendSMSCodeService.success
      ) {
        this.getNotistack(
          `Enviar Código SMS: Codigo enviado con exito`,
          "success"
        );
      }
      // Send SMS Code Service - Error Service
      else if (
        !this.props.verifyCellphone.sendSMSCodeService.loading &&
        this.props.verifyCellphone.sendSMSCodeService.response &&
        !this.props.verifyCellphone.sendSMSCodeService.success
      ) {
        this.getNotistack(
          `Enviar Código SMS: ${this.props.verifyCellphone.sendSMSCodeService.error}`,
          "error"
        );
      }
      // Send SMS Code Service - Error Service Connectivity
      else if (
        !this.props.verifyCellphone.sendSMSCodeService.loading &&
        !this.props.verifyCellphone.sendSMSCodeService.response &&
        !this.props.verifyCellphone.sendSMSCodeService.success
      ) {
        this.getNotistack(
          `Enviar Código SMS: ${this.props.verifyCellphone.sendSMSCodeService.error}`,
          "error"
        );
      }
    }

    if (
      prevProps.verifyCellphone.verifySMSCodeService !==
      this.props.verifyCellphone.verifySMSCodeService
    ) {
      this.setState({
        loading: this.props.verifyCellphone.verifySMSCodeService.loading,
      });

      // Verify Cellphone Service - Success Service
      if (
        !this.props.verifyCellphone.verifySMSCodeService.loading &&
        this.props.verifyCellphone.verifySMSCodeService.response &&
        this.props.verifyCellphone.verifySMSCodeService.success
      ) {
        this.setState({ cellphoneConfirmed: true, codigoError: false });
        this.getNotistack(`Verificar Celular: Celular confirmado`, "success");
//        let data = {
//          cod_cliente: this.props.client.id,
//          des_nro_documento: this.props.client.documentNumber,
//          des_telefono: this.state.cellphone,
//        };
        // this.props.updateClientCellphone(data);
      }
      // Verify Cellphone Service - Error Service
      else if (
        !this.props.verifyCellphone.verifySMSCodeService.loading &&
        this.props.verifyCellphone.verifySMSCodeService.response &&
        !this.props.verifyCellphone.verifySMSCodeService.success
      ) {
        this.setState({ cellphoneConfirmed: false, codigoError: true });
        this.getNotistack(
          `Verificar Celular: ${this.props.verifyCellphone.verifySMSCodeService.error}`,
          "error"
        );
      }
      // Verify Cellphone Service - Error Service Connectivity
      else if (
        !this.props.verifyCellphone.verifySMSCodeService.loading &&
        !this.props.verifyCellphone.verifySMSCodeService.response &&
        !this.props.verifyCellphone.verifySMSCodeService.success
      ) {
        this.setState({ cellphoneConfirmed: false, codigoError: true });
        this.getNotistack(
          `Verificar Celular: ${this.props.verifyCellphone.verifySMSCodeService.error}`,
          "error"
        );
      }
    }

    if (
      prevProps.verifyCellphone.updateClientSMSService !==
      this.props.verifyCellphone.updateClientSMSService
    ) {
      // Update Cellphone Service - Success Service
      if (
        !this.props.verifyCellphone.updateClientSMSService.loading &&
        this.props.verifyCellphone.updateClientSMSService.response &&
        this.props.verifyCellphone.updateClientSMSService.success
      ) {
        this.getNotistack(`Actualizar Celular: Celular actualizado`, "success");
      }
      // Verify Cellphone Service - Error Service
      else if (
        !this.props.verifyCellphone.updateClientSMSService.loading &&
        this.props.verifyCellphone.updateClientSMSService.response &&
        !this.props.verifyCellphone.updateClientSMSService.success
      ) {
        this.setState({ cellphoneConfirmed: false, codigoError: true });
        this.getNotistack(
          `Actualizar Celular: ${this.props.verifyCellphone.updateClientSMSService.error}`,
          "error"
        );
      }
      // Verify Cellphone Service - Error Service Connectivity
      else if (
        !this.props.verifyCellphone.updateClientSMSService.loading &&
        !this.props.verifyCellphone.updateClientSMSService.response &&
        !this.props.verifyCellphone.updateClientSMSService.success
      ) {
        this.setState({ cellphoneConfirmed: false, codigoError: true });
        this.getNotistack(
          `Actualizar Celular: ${this.props.verifyCellphone.updateClientSMSService.error}`,
          "error"
        );
      }
    }
  };

  onChange = (nameObject) => (e) => {
    e.preventDefault();
    let { value } = e.target;
    this.setState({ [nameObject]: value });
  };

  handleSendCode = (e) => {
    e.preventDefault();
    const {client, origination} = this.props;
    if(client.cellphone === "" || client.cellphone.length !== 9){
      this.getNotistack(
          `Enviar Código: Verificar número de celular`,
          "warning"
      );
    } else {
      let data = {
        cod_solicitud_completa: origination.fullNumber,
        des_nro_documento: client.documentNumber,
        des_telefono: client.cellphone,
      };
      this.props.sendSMSCode(data);
    }
  };

  handleValidateSMSCode = (e) => {
    e.preventDefault();
    let data = {
      cod_solicitud_completa: this.props.origination.fullNumber,
      des_telefono: this.props.client.cellphone,
      code: this.state.codigo,
    };
    this.props.verifySMSCode(data);
  };

  render() {
    const { classes } = this.props;
    const { showVerifyCellphone, onClick } = this.props;
    const {
//      cellphone,
      cellphoneError,
      codigoError,
      codigo,
      loading,
      cellphoneConfirmed,
    } = this.state;
    return (
      <div
        className={classNames(classes.verifyCellphoneContainer, {
          [classes.isOnVerifyCellphone]: showVerifyCellphone,
          [classes.isOffVerifyCellphone]: !showVerifyCellphone,
        })}
      >
        <div>
          <IconButton
            disabled={cellphoneConfirmed}
            onClick={onClick}
            className={classNames(classes.IconButton, {
              [classes.isDisabled]: cellphoneConfirmed,
              [classes.isEnabled]: !cellphoneConfirmed,
            })}
          >
            {showVerifyCellphone ? (
              <KeyboardArrowLeftIcon className={classNames(classes.icon)} />
            ) : (
              <KeyboardArrowRightIcon className={classNames(classes.icon)} />
            )}
          </IconButton>
        </div>

        <form
          method="post"
          onSubmit={this.handleValidateSMSCode}
          autoComplete="off"
        >
          <Grid container spacing={8} className="mb-2">
            <Grid item xs={12}>
              <Grid container spacing={8}>
                {/* Client - Cellphone */}
                <Grid item xs={12}>
                  <TextField
                    required
                    error={this.props.client.cellphoneError}
                    fullWidth={true}
                    label="Celular"
                    type="text"
                    margin="normal"
                    color="default"
                    name="cellphone"
                    value={this.props.client.cellphone}
                    onChange={this.props.onChange}
                    onBlur={this.props.onBlur}
                    onKeyPress={this.props.onKeyPress}
                    InputProps={{
                      inputProps: {
                        readOnly: cellphoneConfirmed,
                        maxLength: defaultCellphoneLengthInput,
                      },
                      endAdornment: (
                        <InputAdornment position="end">
                          <PhoneAndroid
                            color={cellphoneError ? "secondary" : "inherit"}
                            fontSize="small"
                          />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
                {/* Client - Cellphone */}
                
                {/* Client - Email Code */}
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    required
                    error={codigoError}
                    fullWidth={true}
                    label="Código"
                    type="text"
                    margin="normal"
                    color="default"
                    name="codigo"
                    value={codigo}
                    onChange={this.onChange("codigo")}
                    InputProps={{
                      inputProps: {
                        readOnly: cellphoneConfirmed,
                        maxLength: 5,
                      },
                      endAdornment: (
                        <InputAdornment position="end">
                          {codigoError ? (
                            <ErrorIcon
                              className={classNames(classes.errorIcon)}
                              // color={cellphoneError ? "secondary" : "inherit"}
                              fontSize="small"
                            />
                          ) : (
                            <CheckIcon
                              className={classNames(classes.validateIcon)}
                              // color={cellphoneError ? "secondary" : "inherit"}
                              fontSize="small"
                            />
                          )}
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
                {/* Button - Resend */}
                <Grid item xs={6}>
                  <Tooltip
                    TransitionComponent={Zoom}
                    title="Reenviar Código al Celular."
                  >
                    <div className={classNames(classes.buttonProgressWrapper)}>
                      <Bounce>
                        <Button
                          disabled={loading}
                          className={classes.button}
                          type="button"
                          variant="contained"
                          size="small"
                          margin="normal"
                          onClick={this.handleSendCode}
                          fullWidth={true}
                        >
                          Reenviar
                          <Message fontSize="small" className="ml-2" />
                          {loading && (
                              <CircularProgress
                                  size={24}
                                  className={classes.buttonProgress}
                              />
                          )}
                        </Button>
                      </Bounce>
                    </div>
                  </Tooltip>
                </Grid>
                {/* Button - Validate */}
                <Grid item xs={6}>
                  <Tooltip
                    TransitionComponent={Zoom}
                    title="Validar Codigo del Celular"
                  >
                    <div>
                      <Bounce>
                        <div className={classNames(classes.wrapper)}>
                          <Button
                            disabled={loading}
                            variant="contained"
                            type="submit"
                            className={classNames(classes.button)}
                            color="primary"
                            size="small"
                            fullWidth={true}
                          >
                            Validar
                            <SendIcon fontSize="small" className="ml-2" />
                            {loading && (
                                <CircularProgress
                                    size={24}
                                    className={classes.buttonProgress}
                                />
                            )}
                          </Button>
                        </div>
                      </Bounce>
                    </div>
                  </Tooltip>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </form>
      </div>
    );
  }
}

export default withStyles(styles)(
  withSnackbar(connect(mapStateToProps, mapDispatchToProps)(VerifyCellphone))
);
