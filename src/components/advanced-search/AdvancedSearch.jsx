import React, { Component }from 'react';
// Element
import { 
    Grid,
    Divider,
    IconButton,
    Typography,
    Tooltip
} from '@material-ui/core';
// Custom Element
import SearchElement from './SearchElement';
import SearchPreview from './SearchPreview';
// Effects 
import { Fade, Zoom, Bounce }from 'react-reveal';
// Icons
import AddIcon from '@material-ui/icons/Add';
import FilterListIcon from '@material-ui/icons/FilterList';

class AdvancedSearch extends Component {
    /*
        Struct Props -> columns [
            {value:"documento", label:"Documento", type:"text"},
            {value:"fecha", label:"Fecha", type:"date"},
            {value:"Numero", label:"Numero", type:"number"}
        ]    
    */
    state = {
        search:{
            connectId: "",
            connect: "",
            connectError: false,
    
            column:"", 
            columnId:"", 
            columnType: "text",
            columnError: false,
    
            operatorId: "",
            operator:"", 
            operatorError: false,
    
            valueId: "",
            value: "",
            valueError: false,     
        },
        valueSearch: []
    }
    // Add Element Search
    handleClickAddElementSearch = e => {
        const { valueSearch, search } = this.state;
        const { connect, column, operator, value } = search;
        // Validate Values
        let connectError = false;
        if(valueSearch.length > 0 ){
            connectError =  connect !== ""? false:true;
        }
        let operatorError =  operator !== ""? false:true;
        let columnError =  column !== ""? false:true;
        let valueError =  value !== ""? false:true;

        if(!connectError && !columnError && !operatorError && !valueError){
            // Get Data
            let newData = valueSearch.concat(search);
            this.setState(state => ({
                ...state,
                valueSearch: newData,
                search:{
                    ...state.search, 
                    connectId: "",
                    connect: "",
                    connectError: false,
                    column:"", 
                    columnId:"", 
                    columnError: false,
                    operatorId: "",
                    operator:"", 
                    operatorError: false,
                    valueId: "",
                    value: "",
                    valueError: false,     
                }
            }));
            this.props.handleFilter(newData);
        }
        else{
            this.setState(state => ({
                search:{
                    ...state.search,
                    connectError: connectError,
                    columnError: columnError,
                    operatorError: operatorError,
                    valueError: valueError,
                }
            }));
        }
    }
    // Delete Element Search
    handleClickDeleteElement = index => e => {
        const { valueSearch } =  this.state;
        const newData = valueSearch.filter((item, i) => {
            return i !== index
        });
        this.setState(state => ({
            ...state,
            valueSearch: newData
        }));
        this.props.handleFilter(newData);
    }
    // TextField Default - Event Change
    handleChangeTextFieldRequired = (objectName, subObjectName, name) => e => {
        e.persist();
        let { value } = e.target;
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        if(subObjectName){
            this.setState(state => ({
                ...state,
                [objectName]:{
                    ...state[objectName], 
                    [subObjectName]:{
                        ...state[objectName][subObjectName],
                        [name]: value,
                        [nameId]: value,
                        [nameError]: value !== "" ? false: true,
                    }
                }
            }));
        }
        else{
            this.setState(state => ({
                ...state,
                [objectName]:{
                    ...state[objectName], 
                    [name]: value,
                    [nameId]: value,
                    [nameError]: value !== "" ? false : true
                }
            }));
        }
    }
    // Select Default - Event Change
    handleChangeSelectFieldRequired = (objectName, subObjectName, name, data=[]) => e => {
        let nameId = `${name}Id`;
        let nameError = `${name}Error`;
        let {value} = e.target;
        let nameType = `${name}Type`;
        let obj = data.find(item => item.value === value);
        this.setState(state => ({
            ...state,
            search:{
                ...state.search,
                [name]: value,
                [nameId]: value,
                [nameError]: value !== "" ? false: true,
                [nameType]: obj? obj.type : "text"
            }
        }));
    }
   
    render = () => {
        let { columns = [] } = this.props;
        return (
            <React.Fragment>
                <Grid container spacing={8} className="mt-2">
                    <Grid item xs={12}>
                        <Fade>
                            <Typography color="textSecondary"  className="d-flex align-items-center">
                                <FilterListIcon fontSize="small" className="mr-2"/>
                                Seleccionar filtros:
                            </Typography>
                        </Fade>
                    </Grid>
                </Grid>
                <Grid container spacing={8}>
                    <SearchElement 
                        connectDisabled={this.state.valueSearch.length === 0? true : false }
                        handleChangeSelectColumn={this.handleChangeSelectFieldRequired("search", null, "column", columns)}
                        handleChangeSelectOperator={this.handleChangeSelectFieldRequired("search", null, "operator")}
                        handleChangeSelectValue={this.handleChangeTextFieldRequired("search", null, "value")}
                        handleChangeSelectConnect={this.handleChangeSelectFieldRequired("search", null, "connect")}
                        columns={columns}
                        state={this.state.search} 
                    />
                    <Divider />
                    <Grid item xs={12} sm={12} md={1} lg={1} className="d-flex justify-content-center align-items-center">
                        <Bounce>
                            <div>
                                <Tooltip TransitionComponent={Zoom} title="Agregar filtro.">
                                    <IconButton 
                                        onClick={this.handleClickAddElementSearch}
                                        margin="normal" color="primary" size="small">
                                        <AddIcon />
                                    </IconButton>
                                </Tooltip>
                            </div>
                        </Bounce>
                    </Grid>
                </Grid>
                {
                    this.state.valueSearch.length > 0 &&
                        <Grid container spacing={8}>
                            <Grid item xs={12}>
                                <Fade left>
                                    <Typography color="textSecondary">
                                        Solicitudes que cumplan la(s) siguiente(s) coindicion(es):
                                    </Typography>
                                </Fade>
                            </Grid>
                        </Grid>
                }
                {
                    this.state.valueSearch.map((item, index) => (
                        <Fade key={index}>
                            <SearchPreview  
                                handleClickDeleteElement={this.handleClickDeleteElement(index)}
                                connect={item.connect}
                                column={item.column}
                                operator={item.operator}
                                value={item.value}
                            />
                        </Fade>
                    ))
                }
            </React.Fragment>
        )
    }
}

export default AdvancedSearch;