import React from 'react';
import * as moment from 'moment';

import{ 
    Grid, 
    TextField,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    InputAdornment
} from '@material-ui/core';

import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
// Utils 
import { onlyTextKeyCode } from '../../utils/Utils'

const SearchElement = ({ 
        columns=[], connectDisabled = false, state, 
        handleChangeSelectColumn, 
        handleChangeSelectOperator,
        handleChangeSelectValue,
        handleChangeSelectConnect
    }) => (
    <React.Fragment>
        {/* Connect */}
        <Grid item xs={12} sm={6} md={2} lg={2}>
            <FormControl 
                error={state.connectError}
                disabled={connectDisabled}
                required
                margin="normal"
                fullWidth>
                <InputLabel htmlFor="connect">Conector</InputLabel>
                <Select
                    value={state.connectId}
                    onChange={handleChangeSelectConnect}
                    className="connect"
                    inputProps={{
                        name: "connect",
                        id: "connectId",
                    }}
                    >
                    <MenuItem value=""></MenuItem>
                    <MenuItem value="Y">Y</MenuItem>
                    <MenuItem value="O">O</MenuItem>
                </Select>
            </FormControl>                     
        </Grid>
        {/* Column */}
        <Grid item xs={12} sm={6} md={4} lg={4}>
            <FormControl 
                error={state.columnError}
                className="column"
                required
                margin="normal"
                fullWidth>
                <InputLabel htmlFor="column">Columna</InputLabel>
                <Select 
                    value={state.columnId}
                    onChange={handleChangeSelectColumn}
                    className="column"
                    inputProps={{
                        name: "column",
                        id: "column",
                    }}>
                    <MenuItem value=""></MenuItem>
                    {
                        columns.map((item, index) => (
                            <MenuItem key={index} value={item.value}> {item.label}</MenuItem>
                        ))
                    }
                </Select>
            </FormControl>                     
        </Grid>
        {/* Operator */}
        <Grid item xs={12} sm={6} md={2} lg={2}>
            <FormControl 
                error={state.operatorError}
                className="operator"
                required
                margin="normal"
                fullWidth>
                <InputLabel htmlFor="operator">Comparación</InputLabel>
                <Select
                    value={state.operatorId}
                    onChange={handleChangeSelectOperator}
                    inputProps={{
                        name: "operator",
                        id: "operator",
                    }}
                >
                    <MenuItem value=""></MenuItem>
                    <MenuItem value="Igual">Igual</MenuItem>
                    <MenuItem value="Mayor">Mayor a</MenuItem>
                    <MenuItem value="Menor">Menor a</MenuItem>
                    <MenuItem value="Contiene">Contiene</MenuItem>
                </Select>
            </FormControl>                     
        </Grid>
        {/* Value */}
        <Grid item xs={12} sm={6} md={3} lg={3}>
            {
                state.columnType !== "date"? 
                    <TextField
                        error={state.valueError}
                        className="value"
                        fullWidth
                        required
                        label="Valor"
                        type={state.columnType}
                        margin="normal"
                        color="default"
                        id="value"
                        name="value"
                        value={state.valueId}
                        autoComplete='off'
                        onChange={handleChangeSelectValue}
                        onKeyPress={(e) => {
                            let code = (e.which) ? e.which : e.keyCode;
                            if(!onlyTextKeyCode(code)){
                                e.preventDefault();
                            }     
                        }}
                        InputProps={{
                            inputProps:{
                                maxLength: 50
                            },
                        }}
                    />:
                    <TextField
                        error={state.valueError}
                        className="value"
                        fullWidth
                        required
                        label="Valor"
                        type={state.columnType}
                        margin="normal"
                        color="default"
                        id="value"
                        name="value"
                        value={state.valueId}
                        onChange={handleChangeSelectValue}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{
                            inputProps:{
                                max: moment().format('YYYY-MM-DD')
                            },
                            endAdornment: (
                                <InputAdornment position="end">
                                    <CalendarTodayIcon color={state.valueError? "secondary": "inherit"} fontSize="small" />
                                </InputAdornment>
                            )
                        }}
                    />
            }
        </Grid>
    </React.Fragment>
)
export default SearchElement;