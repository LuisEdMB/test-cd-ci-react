import React from 'react';
import{ 
    Tooltip,
    Grid, 
    TextField,
    IconButton,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
//Effects 
import { Bounce, Zoom } from 'react-reveal';

const SeachPreview = ({ connect, column, operator, value, readOnly = true, handleClickDeleteElement}) => (
    <Grid container spacing={8}>
        <Grid item xs={12} sm={6} md={2} lg={2}>
            <TextField
                className="connect"
                fullWidth
                label="Conector"
                type="text"
                margin="normal"
                color="default"
                id="connect"
                name="connect"
                value={connect}
                InputProps={{
                    readOnly: readOnly,
                }}
            />            
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={4}>
            <TextField
                className="column"
                fullWidth
                label="Columna"
                type="text"
                margin="normal"
                color="default"
                id="column"
                name="column"
                value={column}
                InputProps={{
                    readOnly: readOnly,
                }}
            />            
        </Grid>
        <Grid item xs={12} sm={6} md={2} lg={2}>
            <TextField
                className="operador"
                fullWidth
                label="Valor"
                type="text"
                margin="normal"
                color="default"
                id="operator"
                name="operator"
                value={operator}
                InputProps={{
                    readOnly: readOnly,
                }}
            />             
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg={3}>
            <TextField
                className="value"
                fullWidth
                label="Valor"
                type="text"
                margin="normal"
                color="default"
                id="value"
                name="value"
                value={value}
                InputProps={{
                    readOnly: readOnly,
                }}
            />
        </Grid>
        <Grid item xs={12} sm={12} md={1} lg={1} className="d-flex justify-content-center align-items-center">
            <Bounce>
                <div>
                    <Tooltip TransitionComponent={Zoom} title="Eliminar filtro.">
                        <IconButton margin="normal" size="small" onClick={handleClickDeleteElement}>
                            <DeleteIcon color="secondary"/>
                        </IconButton>
                    </Tooltip>
                </div>
            </Bounce>
        </Grid>
    </Grid>
)
export default SeachPreview;