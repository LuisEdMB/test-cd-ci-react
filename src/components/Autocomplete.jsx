import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Select from 'react-select';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import CancelIcon from '@material-ui/icons/Cancel';
import { emphasize } from '@material-ui/core/styles/colorManipulator';
import NoSsr from '@material-ui/core/NoSsr';

const styles = theme => ({
  root: {
    flexGrow: 1,
    minWidth: 290,
    //height: 29.5,
  },
  input: {
    //height: 29.5,
    display: 'flex',
    margin: 0,
    padding: ".67em 0 0 0",
    fontSize: 14,
    maxHeight: 36,
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden',
    //height: 29.5,
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    ),
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  singleValue: {
    fontSize: 14,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  divider: {
    height: theme.spacing.unit * 2,
  },
});

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}
function ControlError(props) {
  return (
    <TextField
      disabled={props.isDisabled}
      fullWidth
      error
      margin="normal"
      value={props.value}
      InputProps={{
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps,
        },
      }}
      {...props.selectProps.textFieldProps}
    />
  );
}

function Control(props) {
  return (
    <TextField
      disabled={props.isDisabled}
      fullWidth
      margin="normal"
      value={props.value}
      InputLabelProps={{
        shrink: true,
      }}
      InputProps={{
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps,
        },
      }}
      {...props.selectProps.textFieldProps}
    />
  );
}

function Option(props) {
  return (
    <MenuItem
      disabled={props.isDisabled}
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 100,
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

function Placeholder(props) {
  return (
    <Typography
      noWrap
      color="textSecondary"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function SingleValue(props) {
  return (
    <Typography 
      noWrap
      color={props.children ? "inherit": "textSecondary"}
      className={props.selectProps.classes.singleValue} 
      {...props.innerProps}>
      {props.children}
    </Typography>
  );
}

function ValueContainer(props) {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

function MultiValue(props) {
  return (
    <Chip
      tabIndex={-1}
      label={props.children}
      className={classNames(props.selectProps.classes.chip, {
        [props.selectProps.classes.chipFocused]: props.isFocused,
      })}
      onDelete={props.removeProps.onClick}
      deleteIcon={<CancelIcon {...props.removeProps} />}
    />
  );
}

function Menu(props) {
  return (
    <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
      {props.children}
    </Paper>
  );
}



class IntegrationReactSelect extends React.Component {

  render() {
    const { 
            classes, 
            theme,
            data, 
          } = this.props; 

    const components = {
      Control: !this.props.error? Control:ControlError,
      Menu,
      MultiValue,
      NoOptionsMessage,
      Option,
      Placeholder,
      SingleValue,
      ValueContainer,
    };

    const selectStyles = {
      input: base => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit',
         
        },
      }),
    };
    return (
      <NoSsr>
        <Select
          autoFocus={this.props.focus}
          isLoading={this.props.loading}
          isRtl={false}
          isSearchable={this.props.search}
          isDisabled={this.props.disabled}
          margin="normal"
          classes={classes}
          styles={selectStyles}
          options={data}
          placeholder={this.props.placeholder}
          isClearable
          searchText={this.props.searchText}
          components={components}
          noOptionsMessage={() => null}
          value={this.props.data.filter(option => option.value === this.props.value)}
          selectValue={this.props.selectValue}
          onChange={this.props.onChange}
          ref={ this.props.inputRef }
          textFieldProps={{
            label: this.props.label,
            InputLabelProps: {
              shrink: this.props.label && true,
            }
          }}
        />
      </NoSsr>
    );
  }
}

IntegrationReactSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  onChange: PropTypes.func
};

IntegrationReactSelect.defaultProps = {
  dataDefault:[]
};

export default React.memo(withStyles(styles, { withTheme: true })(IntegrationReactSelect));