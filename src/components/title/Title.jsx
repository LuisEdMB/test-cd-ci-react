import React from 'react';
import { 
    withStyles, 
    Typography 
} from '@material-ui/core';

const styles = theme => ({
    title: {
        [theme.breakpoints.down('sm')]: {
           fontSize: "1.2em"
        }
    }
});

const Title = ({ classes, title, align, color, ...props}) => {
    return (
        <Typography variant="h5" color={color} align={align} className={classes.title}>
            <strong> {title} </strong>
        </Typography>
    )
}

Title.defaultProps = {
    title: "",
    color: "textSecondary",
    align: "left"
};

export default withStyles(styles)(Title);