import React from "react";
import { Table,} from '@devexpress/dx-react-grid-material-ui';
import { withStyles } from '@material-ui/core/styles';
import { customPalette } from '../../theme';

const styles = theme => ({
    header:  {
        backgroundColor: customPalette.table.main, //"#007ab8"//"#f7f7f7",
    }
});

const HeadComponent = ({classes, ...props}) => {
    return <Table.TableHead {...props} className={classes.header} />;
}

export default withStyles(styles, { name: 'TableHeadComponent' })(HeadComponent);
