import React from "react";
import { Table } from '@devexpress/dx-react-grid-material-ui';

const CellComponent = ({...props}) => {
    return <Table.Cell {...props} />
}

export default CellComponent;
