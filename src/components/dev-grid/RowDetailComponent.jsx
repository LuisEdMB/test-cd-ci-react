import React from "react";
import { withStyles, Typography } from "@material-ui/core";

const styles = theme => ({
    fontSizeDefault: {
        fontSize:12
    },
});

const RowDetailComponent = ({ row, classes, ...props }) => (
    <ul>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Data:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                No disponibles
            </Typography>
        </li>
    </ul>
);

export default withStyles(styles, { name: 'RowDetailComponent' })(RowDetailComponent);