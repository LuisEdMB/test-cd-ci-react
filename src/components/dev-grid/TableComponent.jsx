import React from "react";
import classNames from "classnames";
import { withStyles } from '@material-ui/core/styles';
import { Table } from '@devexpress/dx-react-grid-material-ui';
import { customPalette } from '../../theme';
const styles = theme => ({
    tableStriped: {
        transition: ".3s",
        '& tbody tr:nth-of-type(odd)': {
            backgroundColor: customPalette.table.striped,// "#007ab814"//"#eeeeee50",
        },
        '& tbody tr:hover': {
            backgroundColor: customPalette.table.hover,// "#007ab814"//"#eeeeee75",
        },
        '& tbody tr:hover td': {
            color: customPalette.table.contrastText,
        }
    }
});

const TableComponent = ({ classes, ...props }) => (
    <Table.Table {...props} className={classNames(classes.tableStriped, classes.hover )} />
);

export default withStyles(styles, { name: 'TableComponent' })(TableComponent);