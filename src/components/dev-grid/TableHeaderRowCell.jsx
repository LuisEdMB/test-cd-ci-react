import React from "react";
import classNames from "classnames";
import { TableHeaderRow } from '@devexpress/dx-react-grid-material-ui';
import { withStyles } from '@material-ui/core/styles';
import { customPalette } from '../../theme';

const styles = theme => ({
    root:{
        color: "white",
        "&:hover span": {
            color: customPalette.table.contrastText,
        },
        "& span":{
            color: customPalette.table.contrastText,
        },
        "& svg":{
            color: customPalette.table.contrastText
        }
    },

});

const TableHeaderRowCell = ({classes, ...props}) => {
    return <TableHeaderRow.Cell {...props} className={classNames(classes.root, classes.MuiTableSortLabel)} />;
}


export default withStyles(styles, { name: 'TableHeaderRowCell' })(TableHeaderRowCell);