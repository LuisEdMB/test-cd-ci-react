import React from 'react';
import { Paper, withStyles } from '@material-ui/core';
import { 
    RowDetailState,
    IntegratedFiltering,
    SortingState,
    IntegratedSorting, 
    PagingState,
    IntegratedPaging,
    SearchState,

    SelectionState,
    IntegratedSelection,
} from '@devexpress/dx-react-grid';
import {
  Grid as DevGrid,
  Table,
  TableColumnResizing,
  TableHeaderRow,
  TableRowDetail,
  TableColumnVisibility,
  PagingPanel, 
  SearchPanel,
  Toolbar,
  TableSelection
} from '@devexpress/dx-react-grid-material-ui';

// Default 
import TableComponent from './TableComponent'
import RowDetailComponent from './RowDetailComponent';
import CellComponent from './CellComponent';
import HeadComponent from './HeadComponent';
import TableHeaderRowCell from './TableHeaderRowCell';

const styles = theme => ({ 


});

const TableCustom = ({
        rows, 
        columns, 
        defaultSorting, 
        defaultCurrentPage, 
        defaultPageSize, 
        columnExtensions,
        defaultHiddenColumnNames,
        defaultColumnWidths,
        getRowId,
        TableHeaderRowCellComponent,
        HeadComponent,
        TableComponent, 
        CellComponent,
        RowDetailComponent,
        pageSizes,
        selection, 
        onSelectionChange,
        width,
        search,
        check,
        pagination,
        handleGridChangePage,
        handleGridChangeSizePage,
        noData,
        ...props
    }) => {

    if(defaultColumnWidths.length === 0 && columns.length>0 && rows.length > 0){
        return <li>Definir tamaño del ancho de las columnas en la tabla</li>
    }

    return(
        <Paper style={{ position: 'relative' }}>
            <DevGrid 
                getRowId={getRowId}
                rows={rows}
                columns={columns}>
                {
                    search &&  <SearchState />
                }
               
                <SortingState defaultSorting={defaultSorting} />
                <PagingState defaultCurrentPage={defaultCurrentPage} defaultPageSize={pagination ? defaultPageSize : 1000}
                    onCurrentPageChange={handleGridChangePage}
                    onPageSizeChange={handleGridChangeSizePage}
                />
                {
                    check && <SelectionState selection={selection} onSelectionChange={onSelectionChange}/>
                }
                {
                    check && <IntegratedSelection />
                }
                <IntegratedFiltering />
                <IntegratedSorting />
                <IntegratedPaging />
                {
                    (width === "md" || width === "lg" || width === "xl") ? "" : <RowDetailState />
                }
               <Table 
                    height="auto"
                    messages={{noData: noData}}
                    columnExtensions={columnExtensions}
                    
                    headComponent={HeadComponent}
                    tableComponent={TableComponent} 
                    cellComponent={CellComponent}/>
                <TableColumnVisibility defaultHiddenColumnNames={defaultHiddenColumnNames}/>
                <TableColumnResizing defaultColumnWidths={defaultColumnWidths} />
                <TableHeaderRow showSortingControls  cellComponent={TableHeaderRowCell} />
                {
                    (width === "md" || width === "lg" || width === "xl") ? "" : <TableRowDetail contentComponent={RowDetailComponent} />
                }
                {
                    check &&  <TableSelection highlightRow={true} />
                }
                {
                    pagination && <PagingPanel
                        messages={{
                            rowsPerPage: "Filas por Página:",
                            showAll: "Todos",
                            info: '{from} | {to} - ({count}) solicitude(s)',
                        }}
                        pageSizes={pageSizes}/>
                }
                {
                    search &&  <Toolbar /> 
                }
                {
                    search && <SearchPanel />
                }
            </DevGrid>
        </Paper>
    )
}

TableCustom.defaultProps = {
    rows:[{ "columna" : "value"}],
    columns: [{ name: 'columna', title: 'Solicitud' },],
    defaultSorting: [],
    defaultCurrentPage: 0,
    defaultPageSize: 10,
    columnExtensions: [],
    defaultHiddenColumnNames:[],
    defaultColumnWidths: [ { columnName: 'columna', width: 125 },],
    HeadComponent: HeadComponent,
    TableComponent: TableComponent,
    CellComponent: CellComponent,
    RowDetailComponent: RowDetailComponent,
    TableHeaderRowCell: TableHeaderRowCell,
    pageSizes: [25, 50, 100],
    width: "lg",
    search: false,
    check: false,
    pagination: true,
    noData: 'Solicitudes no disponibles'
};

export default withStyles(styles)(TableCustom);
