import React from 'react';
import classNames from 'classnames';

import {
    FormLabel,
    Typography,
    FormHelperText,
    withStyles
} from '@material-ui/core';
import AttachFileIcon from '@material-ui/icons/AttachFile';

const styles = theme => ({
    upload:{
        display: 'none',
    },
    dropzone:{
        height: 80,
        border: "1px solid #bdbdbd", 
        display:"flex", 
        justifyContent:"center", 
        alignItems:"center",
        textTransform: 'none',
        margin:".5em 0",
        borderRadius:5
    },
    dropzoneHelperWrapper:{
        display:"flex", 
        justifyContent:"space-between", 
        alignItems:"center"
    },
});
const InputFileCustom = ({
    label,
    title,
    max,
    name,
    id,
    fileName,
    message,
    size,
    error,
    accept,
    handleUploadFile, 
    classes
}) => (
    <React.Fragment>
        <FormLabel>
            {label}:
        </FormLabel>
        <input
            name={name}
            onChange={handleUploadFile}
            className={classNames(classes.upload)}
            accept={accept}
            id={id}
            type="file"
        />
        <FormLabel htmlFor="file" style={{width:"100%"}}>
            <div 
                id={`${id}-zone`}
                className={classes.dropzone}
                margin="normal"
                variant="outlined" 
                component="span">
                <AttachFileIcon />
                <Typography component="span" variant="inherit" align="center">
                    <Typography component="span" variant="inherit" className="mb-1">
                        <Typography component="span" variant="inherit" >
                            {title}
                        </Typography>
                    </Typography>
                    <Typography component="span" variant="inherit">
                        Limite {max}MB Max.
                    </Typography>
                </Typography>
            </div>
        </FormLabel>
        <FormHelperText className={classNames(classes.dropzoneHelperWrapper , "mt-2")}>
            <Typography component="span" noWrap variant="inherit">
                <Typography 
                    className={classes.textSuccess} 
                    component="span" noWrap variant="inherit">
                    {fileName}
                </Typography>
                <Typography 
                    color={error? "secondary":"default"}
                    component="span" noWrap variant="inherit">
                    {message}
                </Typography>
            </Typography>
            
            <Typography component="span" variant="inherit" className="ml-2">
                {Number(size/1e+6).toFixed(2)}MB.
            </Typography>
        </FormHelperText>
    </React.Fragment>
)

InputFileCustom.defaultProps = {
    label:"",
    title:"Adjuntar documento",
    max:3,
    name:"",
    id:"",
    fileName:"",
    message:"",
    size:0,
    error:false,
    accept:[".pdf"],
};

export default withStyles(styles)(InputFileCustom);