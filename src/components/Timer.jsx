import React, { Component } from "react";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";
// React Router
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// Icons
import ScheduleIcon from "@material-ui/icons/Schedule";
import CloseIcon from "@material-ui/icons/Close";
// Components
import { Typography, IconButton } from "@material-ui/core";
// Colors
import orange from "@material-ui/core/colors/orange";
import blue from "@material-ui/core/colors/blue";
// Actions
import { getConstantODC } from "../actions/generic/constant";

const pad = (num) => ("0" + num).slice(-2);

const mmss = (secs) => {
  var minutes = Math.floor(secs / 60);
  secs = secs % 60;
  var hours = Math.floor(minutes / 60);
  minutes = minutes % 60;
  return `${pad(hours)}:${pad(minutes)}:${pad(secs)}`;
};

const styles = (theme) => ({
  fontSize: {
    fontSize: 16,
    margin: "0 5px",
  },
  timerContainer: {
    position: "fixed",
    width: 240,
    right: ".5em",
    boxShadow: "1px 1px 4px black",
    height: 32,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "rgba(0,0,0,0.23)",
    borderRadius: ".2em",
    zIndex: 900,
  },
  backgroundWarning0: {
    backgroundColor: blue[300],
  },
  backgroundWarning1: {
    backgroundColor: orange[300],
  },
  backgroundWarning2: {
    backgroundColor: orange[400],
  },
  container: {
    height: 20,
  },
});

const mapStateToProps = (state, props) => {
  return {
    constantODC: state.constantODCReducer,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const actions = {
    getConstantODC: bindActionCreators(getConstantODC, dispatch),
  };
  return actions;
};

class Timer extends Component {
  state = {
    timeRemaining: 0,
  };

  getNotistack(message, variant = "default", duration = 6000) {
    let select = "default";
    switch (variant) {
      case "error":
        select = variant;
        break;
      case "success":
        select = variant;
        break;
      case "warning":
        select = variant;
        break;
      case "info":
        select = variant;
        break;
      default:
        select = variant;
        break;
    }
    // Notistack
    this.props.enqueueSnackbar(message, {
      variant: select,
      autoHideDuration: duration,
      action: (
        <IconButton>
          <CloseIcon size="small" className="text-white" color="inherit" />
        </IconButton>
      ),
    });
  }
  render() {
    const { classes } = this.props;
    const { timeRemaining } = this.state;
    return (
      <div
        className={classNames(classes.timerContainer, {
          [classes.backgroundWarning0]: timeRemaining > 300,
          [classes.backgroundWarning1]:
            timeRemaining > 60 && timeRemaining <= 300,
          [classes.backgroundWarning2]: timeRemaining <= 60,
        })}
      >
        <Typography
          color="default"
          align="center"
          component="span"
          className={classNames(
            "w-100 d-flex align-items-center justify-content-center text-white py-1"
          )}
        >
          <ScheduleIcon className={"ml-2"} />
          <Typography
            component="span"
            color="inherit"
            className={classes.fontSize}
          >
            Tiempo Restante {mmss(timeRemaining)}
          </Typography>
        </Typography>
      </div>
    );
  }

  componentDidMount() {
    this.props.getConstantODC();
    this.doIntervalChange();
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.constantODC !== this.props.constantODC) {
      // Credit Card Constant - Error Service
      if (
        !this.props.constantODC.loading &&
        this.props.constantODC.response &&
        this.props.constantODC.success
      ) {
        let { data } = this.props.constantODC;
        let tiempoRestante = data.find(
          (item) => item.des_abv_constante === "ODC_T_RESTANTE"
        );
        this.setState({
          timeRemaining: tiempoRestante.valor_numerico * 60,
        });
      }
      // Credit Card Constant - Error Service
      else if (
        !this.props.constantODC.loading &&
        this.props.constantODC.response &&
        !this.props.constantODC.success
      ) {
        this.getNotistack(
          `Constante: ${this.props.constantODC.error}`,
          "error"
        );
      }
      // Credit Card Constant - Error Service Connectivity
      else if (
        !this.props.constantODC.loading &&
        !this.props.constantODC.response &&
        !this.props.constantODC.success
      ) {
        this.getNotistack(
          `Constante: ${this.props.constantODC.error}`,
          "error"
        );
      }
    }
  };

  doIntervalChange = () => {
    this.myInterval = setInterval(() => {
      this.setState(
        (prevState) => ({
          timeRemaining:
            prevState.timeRemaining > 0
              ? prevState.timeRemaining - 1
              : prevState.timeRemaining,
        }),
        () => {
          if (this.state.timeRemaining === 300) {
            this.getNotistack("Quedan cinco (5) minutos restantes!", "warning");
          }
          if (this.state.timeRemaining === 60) {
            this.getNotistack("Queda un (1) minuto restante!", "warning");
          }
        }
      );
    }, 1000);
  };

  componentWillUnmount() {
    clearInterval(this.myInterval);
  }
}

export default withStyles(styles)(
  withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Timer))
);
