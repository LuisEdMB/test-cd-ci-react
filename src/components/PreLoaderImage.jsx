import React, { Component } from 'react';
// import ImageLoader from 'react-load-image';
import { withStyles } from '@material-ui/core/styles';
//import CencosudLogo1 from '../assets/media/images/\png/Cencosud-1.png';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import Zoom from 'react-reveal/Zoom'

const styles = theme => ({
    root: {
        width: '100%',
        minHeight:400,
        display: 'flex', 
        flexDirection: "column",
        justifyContent: 'center', 
        alignItems: 'center'
    },
    image:{
        width:320, 
        minHeight:100
    }
});


class PreLoaderImage extends Component{

    render(){
        let {classes } = this.props;
        // const Preloader = () => {
        //     return <img src={CencosudLogo1} alt="Cencosud Logo"/>;
        // }
        return (
            <div className={classes.root}>
                <Zoom top>
                    <div>
                        {
                            /**
                                <ImageLoader src={CencosudLogo1} >
                                    { 
                                        <img className={classes.image} alt="Cencosud Image" src={CencosudLogo1}/> 
                                    }
                                    <div>Error!</div>
                                    <Preloader />
                                </ImageLoader> 

                             */
                        }
                     
                        <br />
                        <div>
                            <Typography align="center" component="h3" variant="h6">
                                { this.props.text }
                            </Typography>
                            <LinearProgress />
                        </div>
                    </div>
                </Zoom>
            </div>
        );
    }
}

PreLoaderImage.defaultProps = {
    text: 'Espere por favor, Cargando...'
};

export default withStyles(styles)(PreLoaderImage);
