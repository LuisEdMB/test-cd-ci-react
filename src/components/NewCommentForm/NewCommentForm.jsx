import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components 
import { 
  Button,
  TextField,
  AppBar,
  Fab,
  Tooltip, 
  Typography,
  Divider,
  IconButton,
  Grid,
  LinearProgress,
  FormHelperText
} from "@material-ui/core";
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close'; 
import DoneIcon from '@material-ui/icons/Description';
import SendIcon from '@material-ui/icons/Send';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress';
// Actions 
import { createComment, loadComment } from '../../actions/odc-express/odc'
// Utils
import { onlyTextKeyCode, checkInputKeyCodePunComSla } from '../../utils/Utils';

const styles = theme => ({
  modalRoot:{
      minWidth:280, 
      width:450,
      maxWidth:680,
  },
  fab:{
    minHeight:0, 
    height:24,
    width:24, 
    color:"white",
    transition:".3s",
    textTransform: 'none'
  }, 
  fabFixed :{
    height:44,
    width:44, 
  },
  icon:{
      minHeight: 0, 
      height: 12, 
      width: 12
  }, 
  iconFixed:{
    minHeight: 0, 
    height: 20, 
    width: 20
  }, 
  button:{
      textTransform: "none"
  },
  appBar: {
      position: 'relative',
      color: theme.palette.grey[50],
  },
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  chip: {
    marginRight: theme.spacing.unit,
  },
  section1: {
    margin: `${theme.spacing.unit * 3}px ${theme.spacing.unit * 2}px`,
  },
  section2: {
    margin: theme.spacing.unit * 2,
  },
  section3: {
    margin: `${theme.spacing.unit * 6}px ${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
  },
  dialog:{
  // zIndex: 950,
  },
  DialogTitle2: {
    color: theme.palette.grey[50],
  },
  DialogTitulos: {   
    fontWeight: 600,
    color:"black",
    transition:".3s",
    textTransform: 'none',
  },
  DialogSubTitulos: {   
    fontWeight: 500,
    color: "darkgray",
    transition:".3s",
    textTransform: 'none',
  },
});

const DialogTitle = withStyles(theme => ({
  root: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit * 2,
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing.unit,
    top: theme.spacing.unit,
    color: theme.palette.grey[50],
  },
}))(props => {
  const { children, classes } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <Typography variant="h6">{children}</Typography>
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing.unit * 2,
  },
}))(MuiDialogContent);


const mapStateToProps = (state) => {
  return {
    odc: state.odcReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  const actions = {
    createComment: bindActionCreators(createComment, dispatch),
    loadComment: bindActionCreators(loadComment, dispatch)
  };
  return actions;
}

class NewCommentForm extends Component {
  state = {
    open: false,
    comentariosButtonDisabled: true,
    cancelarButtonDisabled: true,
    loading: true,
    listComments: [],
    comment: "",
    codSolicitud: 0,
    codSolicitudCompleta: ''
  };
  componentDidMount = () => {
    const { row, codSolicitud, codSolicitudCompleta } = this.props;
    this.setState({
        codSolicitud: row ? row.cod_solicitud : codSolicitud ,
        codSolicitudCompleta : row ? row.cod_solicitud_completa : codSolicitudCompleta
    })
  }

  componentDidUpdate = (prevProps, prevState) => {
    if(prevProps.odc !== this.props.odc){
        if(prevProps.odc.createComment !== this.props.odc.createComment){
            if(this.props.odc.createComment.loading){
              this.setState(state => ({
                ...state
              }));
            }
            if(!this.props.odc.createComment.loading && 
              this.props.odc.createComment.response && 
              this.props.odc.createComment.success){
                this.setState(state => ({
                  ...state
                }));              
            }
            else if(!this.props.odc.createComment.loading && 
                    this.props.odc.createComment.response && 
                    !this.props.odc.createComment.success){
              this.setState(state => ({
                ...state
              }));              
            }
            else if(!this.props.odc.createComment.loading && 
                    !this.props.odc.createComment.response && 
                    !this.props.odc.createComment.success){
              this.setState(state => ({
                ...state
              }));             
            }
        }

        if(prevProps.odc.loadComment !== this.props.odc.loadComment){
          if(this.props.odc.loadComment.loading){
            this.setState(state => ({
              ...state
            }));
          }
          if(!this.props.odc.loadComment.loading && 
            this.props.odc.loadComment.response && 
            this.props.odc.loadComment.success){
              this.setState(state => ({
                ...state
              }));
          }
          else if(!this.props.odc.loadComment.loading && 
                  this.props.odc.loadComment.response && 
                  !this.props.odc.loadComment.success){
            this.setState(state => ({
                ...state
            }));
          }
          else if(!this.props.odc.loadComment.loading && 
                  !this.props.odc.loadComment.response && 
                  !this.props.odc.loadComment.success){
            this.setState(state => ({
                ...state
            }));
          }
        }
        
    }
  }

  handleClickOpen = () => {
    this.setState( state => ({
      ...state,
      open: true,
      comentariosButtonDisabled: false,
      cancelarButtonDisabled: false,
      loading: true
    }));
    let { row, codSolicitudCompleta } = this.props;

   codSolicitudCompleta = row ? row.cod_solicitud_completa : codSolicitudCompleta;
    
    this.props.loadComment(codSolicitudCompleta)
    .then(response => {
      if(response){
        if(response.data){
          if(response.data.success){
            this.setState(state => ({
                ...state, 
                listComments: response.data.comentarios,
                loading: false
            }));
          }
        }
      }
    })

  };

  handleClose = () => {
    this.setState( state => ({
      ...state,
      open: false,
      listComments: [],
      comentariosButtonDisabled: true,
      cancelarButtonDisabled: true,
      comment: "",
      loading: true
    }));
  };

  handleAddComment = (e) => {
    const { comment } = this.state;
    let commentError = false;
    let { row, codSolicitud, codSolicitudCompleta } = this.props;
    codSolicitud = row ? row.cod_solicitud : codSolicitud ;
    codSolicitudCompleta = row ? row.cod_solicitud_completa : codSolicitudCompleta;
    if(comment.trim().length === 0 || codSolicitudCompleta === "") {
      commentError = true;
    }

    const sendData = {
        cod_solicitud_completa: codSolicitudCompleta,
        cod_solicitud: codSolicitud,
        des_comentario: comment,
        cod_agencia:'',
        flg_visible: '',
        flg_eliminado: '',
        fec_reg: '',
        des_usu_reg: '',
        des_ter_reg: '',
        fec_act: '',
        des_usu_act: '',
        des_ter_act: ''
    }

    if(!commentError)
    {
      this.setState(state => ({
          ...state, 
          comentariosButtonDisabled: true,
          cancelarButtonDisabled: true,
          loading: true
      }));
      this.props.createComment(sendData)      
      .then(response => {
          if(response){
              if(response.data){
                  if(response.data.success){                      
                      this.getNotistack("Comentario guardado correctamente.", "success");
                      //Cargar comentarios
                      this.props.loadComment(response.data.crearComentario.cod_solicitud_completa)
                      .then(response => {
                        if(response){
                          if(response.data){
                            if(response.data.success){
                              this.setState(state => ({
                                  ...state, 
                                  comment: "",
                                  comentariosButtonDisabled: false,
                                  cancelarButtonDisabled: false,
                                  listComments: response.data.comentarios,
                                  loading: false
                              }));
                              return response;
                            }
                          }
                        }
                      })                    
                  }
                  else
                  {
                    this.setState(state => ({
                      ...state, 
                      comentariosButtonDisabled: false,
                      cancelarButtonDisabled: false,
                      loading: false
                    }));
                    // Notistack
                    this.getNotistack(this.props.odc.createComment.error, "error");
                  }
              }
              else
              {
                this.setState(state => ({
                  ...state, 
                  comentariosButtonDisabled: false,
                  cancelarButtonDisabled: false,
                  loading: false
                }));
                // Notistack
                this.getNotistack(this.props.odc.createComment.error, "error");
              }
          }
          else
          {
            this.setState(state => ({
              ...state, 
              comentariosButtonDisabled: false,
              cancelarButtonDisabled: false,
              loading: false
            }));
            // Notistack
            this.getNotistack(this.props.odc.createComment.error, "error");
          }
          return null;
      })

    }
    else
    {
      if(commentError){
        this.getNotistack("El comentario no puede estar vacio.", "warning");
        //this.comment.focus(); 
      }
    }

  }

  // Client - TextField - Event Change - Required    
  handleChangeTextFieldRequired = name => e => {
      e.persist();
      let { value } = e.target;
      this.setState(state => ({
          ...state,
          [name]: value, 
      }));
  }
  // Only Text
  handleKeyPressTextFieldOnlyText = name => e =>{
      let code = (e.which) ? e.which : e.keyCode;
      if(!onlyTextKeyCode(code)){
          e.preventDefault();
      }       
  }
  // Check Input
  handleKeyPressTextFieldCheckPunComSlaInput = name => e => {
      let code = (e.which) ? e.which : e.keyCode;
      if(!checkInputKeyCodePunComSla(code)){
          e.preventDefault();
      }   
  }

  // Notistack 
  getNotistack(message, variant="default", duration = 6000){
    let select = "default";
    switch(variant){
        case "error": 
            select = variant; break;
        case "success":
            select = variant; break;
        case "warning":
            select = variant; break;
        case "info":
            select = variant; break;
        default: 
            select = variant; break;
    }
    // Notistack
    this.props.enqueueSnackbar(message, {
        variant: select,
        autoHideDuration: duration,
        action: (
            <IconButton>
                <CloseIcon size="small" className="text-white" color="inherit"/>
            </IconButton>
        ),
    });
  } 

  render = () => {
    const { classes, fixed } = this.props;
    let { comment, loading, listComments, comentariosButtonDisabled, cancelarButtonDisabled } = this.state;
    let { row, codSolicitudCompleta } = this.props;
    codSolicitudCompleta = row ? row.cod_solicitud_completa : codSolicitudCompleta;
    return (
      <React.Fragment>
        <Tooltip title="Click para ver comentario">
           <div>
            <Fab 
              className={classNames( classes.fab, {[classes.fabFixed] : fixed})}
              fontSize="small"
              color="primary" 
              disabled={codSolicitudCompleta.length === 0}
              onClick={this.handleClickOpen}>
                <DoneIcon className={ classNames( classes.icon, {[classes.iconFixed] : fixed})} fontSize="small"/>
            </Fab>
           </div>
        </Tooltip>
               
        <Dialog
        className={classes.dialog}
          fullWidth
          maxWidth={"md"}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <AppBar className={classes.appBar}>
            <DialogTitle id="customized-dialog-title" onClose={this.handleClose} className={DialogTitle}>
              <Typography align="center" component="span" variant="h6" className={classes.DialogTitle2}  >
                    Agregar / Visualizar Comentario - Nro. {codSolicitudCompleta}
              </Typography>           
            </DialogTitle>
          </AppBar>
          <DialogContent>
            
            <div className={classes.root}>

              <div className={classes.section2}>

                <Grid item xs={12}>            
                  <TextField    
                    required
                    autoFocus
                    fullWidth
                    multiline
                    rows="3"
                    label="Nuevo Comentario"
                    id="comment" 
                    value={comment} 
                    variant="outlined"
                    name="comment"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    InputProps={{
                        inputProps:{
                            ref: this.commentInput,
                            maxLength: 600,
                        },
                    }}
                    onKeyPress={this.handleKeyPressTextFieldCheckPunComSlaInput("comment")}
                    onChange={this.handleChangeTextFieldRequired("comment")}
                  />
                  <FormHelperText className="d-flex justify-content-end">
                      {comment.toString().length}/600
                  </FormHelperText>                
                </Grid>
                <Grid container spacing={8}>
                  <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Button variant="contained" 
                    margin="normal"
                    color="primary" 
                    fullWidth
                    size="small"
                    onClick={this.handleAddComment}
                    disabled={comentariosButtonDisabled}
                  >
                    Agregar
                    <SendIcon fontSize="small" className="ml-2" />
                  </Button>
                  </Grid>
                  <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Button variant="contained" 
                    margin="normal"
                    color="secondary"
                    fullWidth      
                    size="small"            
                    onClick={this.handleClose} 
                    disabled={cancelarButtonDisabled}
                  >
                    Cancelar
                    <CancelIcon fontSize="small" className="ml-2" />
                  </Button>
                  </Grid>
                </Grid>

              </div>
              
              {
                loading ? <div>
                  <Typography align="center" component={'span'}> 
                    <CircularProgress /> Cargando comentarios ... 
                  </Typography>
                  <LinearProgress />                  
                </div>
                :
                listComments?.map((comentario, index) => { 
                const { des_comentario, des_usu_reg, fec_reg_texto, des_perfil, nom_agencia } = comentario; 

                  return( 
                    <div key={index}>
                    {/* // <Card key={index} style={{margin: "5px 0", backgroundColor: "whitesmoke"}}> */}
                      <div className={classes.section1} key={index}>
                        <Grid container alignItems="center" key={index}>
                          <Grid item xs>
                            <Typography className={classes.DialogTitulos} gutterBottom>
                            {`${des_usu_reg} ${ des_perfil ? '- ' + des_perfil: '' }`} <span className={classes.DialogSubTitulos}>- {nom_agencia}</span>
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography className={classes.DialogTitulos} gutterBottom>
                            {fec_reg_texto}
                            </Typography>
                          </Grid>
                        </Grid>
                        <Typography color="textSecondary">
                          {des_comentario}
                        </Typography>
                      </div>
                      <Divider variant="middle" />
                    {/* // </Card>                 */}
                    </div>
                  );

                })
              }             
            </div>            
            
          </DialogContent>
        </Dialog>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(NewCommentForm)));


