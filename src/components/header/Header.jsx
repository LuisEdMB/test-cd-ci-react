import React from 'react';
import { 
    Paper,
    withStyles,
    Tooltip
} from '@material-ui/core';
import { Zoom, Bounce, Fade } from 'react-reveal';
import CencosudScotiaBank from '../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg';
// Icons
import ExportExcel from '../file/ExportExcel';
import Title from '../title/Title';

const styles = theme =>({
    avatar:{
        [theme.breakpoints.down('md')]: {
            width: 60, 
            height: 32
        },
        width:80, 
        height:48
    },
    root:{
        display: "flex", 
        justifyContent: "space-between", 
        alignItems: "center",
       
    },
    actionRoot:{
        display: "flex", 
        justifyContent: "center", 
        alignItems: "center"
    }, 
    green:{
        background: `radial-gradient(circle at center, red 0, blue, green 100%)`
    },
    title:{
        [theme.breakpoints.down('md')]: {
            fontSize:14, 
        }
    }
});

const Header = ({title, classes}) => (
    <Paper className="p-1" color="primary">
        <figure className={classes.root}>
            <Bounce>
                <img
                    className={classes.avatar}
                    src={CencosudScotiaBank}
                    alt="Cencosud ScotiaBank" 
                    />
            </Bounce>
            <figcaption className="w-100">
                <Fade>
                    <Title 
                        align="center"
                        title={title}
                    />
                </Fade>
            </figcaption>
        </figure>
    </Paper>
)

Header.defaultProps = {
    title: "",
};

export default withStyles(styles)(Header);