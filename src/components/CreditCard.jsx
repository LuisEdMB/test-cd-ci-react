import React, { Component } from 'react';
import classNames from 'classnames';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Visa from './../assets/media/images/png/Visa-3.png';
import MasterCard from './../assets/media/images/png/MasterCard-2.png';
import Chip from './../assets/media/images/png/Chip-1.png';
import Cencosud from './../assets/media/images/png/Cencosud-1.png';
// Effects
import Fade from 'react-reveal/Fade';
// Utils
import * as Utils from '../utils/Utils'

const styles = theme => ({
    root: {
        [theme.breakpoints.down('xs')]: {
            minWidth:280, 
        },
          [theme.breakpoints.up('sm')]: {
            minWidth:290, 
        },
          [theme.breakpoints.up('md')]: {
            minWidth:330, 
            minHeight:200
        },
        maxWidth:350, 
        position: "relative", 
        overflow: "hidden"
    }, 
    backgroundLogo:{
        position: "absolute", 
        bottom: 0, 
    },
    text:{
        [theme.breakpoints.down('xs')]: {
            fontSize:12, 
            fontWeight: 600, 
        },
          [theme.breakpoints.up('sm')]: {
            fontSize:14, 
        },
        fontWeight: 550, 
    }, 
    button: {
        textTransform: 'none',
    },
    bgSilverWhite:{
        backgroundImage: "linear-gradient(to right, silver, white)"
    },
    bgGoldWhite:{
        backgroundImage: "linear-gradient(to right, #b69f4f , white)"
    }, 
    bgBlackWhite:{
        backgroundImage: "linear-gradient(to right, rgba(0, 0, 0, 0.72) , white)"
    }
});


class CreditCard extends Component {    
    selectColor = (color) => {
        let { classes } = this.props;
        switch(color){
            case 0: 
                return classes.bgSilverWhite;
            case 1:
                return classes.bgGoldWhite;
            case 2: 
                return classes.bgBlackWhite;
            default:
                return classes.bgSilverWhite;
        }
    }

    render() {
        let { type, brand, color=-1, customColor, bin, client, untilEnd, cardNumber, dataDefault, classes, image } = this.props;
        let colorStyle = this.selectColor( color ); 
        let background = "";
        if(customColor !== "" && customColor !== null){
            background = `linear-gradient(to right, ${customColor}, rgba(255,255,255,0.1))`;
        }
        return (
            <div 
                className={classNames(classes.root, "px-3 py-4 rounded border-bottom border-light-gray box-shadow-black", colorStyle )}
                style={{background:background}}>
                <Grid container className="mb-5">
                    {/* Image - Chip */}
                    <Grid item xs={6}>
                        <figure className="d-flex flex-column">
                            <figcaption>
                                <Typography>
                                    <em className="pl-1 font-weight-bold">
                                        {type? type : dataDefault.type} 
                                    </em>
                                </Typography>
                            </figcaption>
                            <img 
                                width={42}
                                height={42}
                                src={Chip} 
                                alt="Chip" />
                        </figure>
                    </Grid>
                    {/* Logo - Cencosud */}
                    {image && <Grid item xs={6}>
                        <figure className="d-flex justify-content-end">
                            <img 
                                width={130}
                                height={90}
                                src={Cencosud} 
                                alt="Cencosud" />
                        </figure>
                    </Grid>
                    }
                </Grid>
                <Grid container className="mb-1">
                    <Grid item xs={7}>
                        <Typography className={classes.text}>
                            {
                                cardNumber? Utils.maskCreditCard(cardNumber):
                                bin? 
                                `${bin.toString().substring(0, 4)} ${bin.toString().substring(4, 6)}** **** ****`:
                                `**** **** **** ****`
                            }
                        </Typography>
                    </Grid>
                    <Grid item xs={5} className="d-flex justify-content-start">
                        {
                            untilEnd && 
                            <Typography className="d-flex align-items-center">
                                <span style={{fontSize:8}}>
                                    Vence <br/>
                                    fin de 
                                </span>
                                <span style={{fontSize:10}}>
                                    {untilEnd}
                                </span>
                            </Typography>
                        }
                    </Grid>
                </Grid>
                <Grid container className="d-flex align-items-center">
                    
                    <Grid item xs={9} className="d-flex align-items-center">
                        <Typography className={classes.text} noWrap={true}>
                            { client ? client.toUpperCase().trim() : dataDefault.client }
                        </Typography>
                    </Grid>
                    {/* Brand  */}
                    <Grid item xs={3}>
                        {
                            type !== 0 &&
                            <figure className="d-flex justify-content-end">
                                <Fade>
                                    <img 
                                        width={50}
                                        height={30}
                                        src={ brand === 0 ? MasterCard:Visa } 
                                        alt={ brand === 0 ? "MasterCard":"Visa" }  />
                                </Fade>
                            </figure>
                        }
                    </Grid>
                </Grid>
            </div>
        );
    }
}


CreditCard.propTypes = {
    //type: PropTypes.number.isRequired, 
};

CreditCard.defaultProps = {
    dataDefault:{
        type:"Clásica",
        client:"CARBONELL ROBERTO "
    }
};


export default withStyles(styles)(CreditCard);

