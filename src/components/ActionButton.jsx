// React
import React from 'react'

// Material UI
import { Button, CircularProgress, withStyles } from '@material-ui/core'

// Metarial UI - 
import { green } from '@material-ui/core/colors'

// Utils
import classNames from 'classnames'

const styles = {
    button: {
        textTransform: 'none'
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none'
    }
}

function ActionButton(props) {
    const { 
        text,
        loading = false,
        type = 'default',
        handleAction,
        icon = null,
        className = '',
        showLoading = false,
        variant = 'contained',
        classes } = props
    return (
        <Button
            variant={ variant }
            disabled={ loading }
            className={ classNames(classes.button, className) }
            fullWidth
            color={ type }
            size='small'
            onClick={ handleAction }
            margin='normal'>
                { text }
                { icon }
                { (showLoading && loading) && 
                    <CircularProgress 
                        size={ 24 } 
                        className={ classes.buttonProgress }/> }
        </Button>
    )
}

export default React.memo(withStyles(styles)(ActionButton))