import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
// Components 
import Grid from '@material-ui/core/Grid';
// Icons 
import ItemInfo from './ItemInfo';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import PlaceIcon from '@material-ui/icons/Place';
import MapIcon from '@material-ui/icons/Map';

const styles = theme => ({
    root: {
        flexGrow: 1,
        width: '100%',
    },
    text:{
        fontSize:12,
        display:"flex", 
        alignItems:"center"
    },
    icon:{
        marginRight:2,
    },
    tab:{
        width: '100%',
        maxWidth:650,
        [theme.breakpoints.down('sm')]: {
                
        },
        [theme.breakpoints.up('md')]: {
        
        },
        [theme.breakpoints.up('lg')]: {

        },
    }
});

const AddressInfo = (props) => {
    let { classes, data } = props;
    let ubigeo = `${data.department} - ${data.province} - ${data.district}`;
    let complementaryVia = `${data.number && `Nro. ${data.number}`} ${data.building && `Dpto. ${data.building}`} ${data.inside && `Int. ${data.inside}`} ${data.mz && `Mz. ${data.mz}`} ${data.lot && `Lt. ${data.lot}`}`
    let via = `${data.via} ${data.nameVia} ${complementaryVia}`;
    let zone = `${data.zone} ${data.nameZone}`;
    return (
        <div className={classes.root}>
            <Grid container spacing={8} className="mt-2"> 
                <Grid item xs={12} md={6}>
                    <ItemInfo 
                        data={{
                            icon:<LocationCityIcon fontSize="small" className={classes.icon}/>, 
                            name: "Ubigeo",
                            value: ubigeo
                        }}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <ItemInfo 
                        data={{
                            icon:<PlaceIcon fontSize="small" className={classes.icon}/>, 
                            name:"Via",
                            value: via
                        }}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <ItemInfo 
                        data={{
                            icon:<MapIcon fontSize="small" className={classes.icon}/>, 
                            name:"Zona",
                            value: zone
                        }}
                    />
                </Grid>
                
                <Grid item xs={12} md={6}>
                    <ItemInfo 
                        data={{
                            icon:<LocalLibraryIcon fontSize="small" className={classes.icon}/>, 
                            name:"Ref",
                            value: data.reference
                        }}
                    />
                </Grid>
            </Grid>
        </div>
    );
}

AddressInfo.propTypes = {
  classes: PropTypes.object.isRequired,
};

AddressInfo.defaultProps = {
    data: {
        department:"",
        province:"",
        district:"", 
        via:"", 
        nameVia:"", 
        number:"",
        building:"",
        inside: "", 
        mz:"",
        lot:"",
        zone:"",
        nameZone:"", 
        housingType:"",
        reference:""
    }
};

export default withStyles(styles)((withWidth()(AddressInfo)));