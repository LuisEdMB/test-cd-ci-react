import React from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
// Components 
import Typography  from '@material-ui/core/Typography';

// Effects
import Fade from 'react-reveal/Fade';
//Icons 
import LabelIcon from '@material-ui/icons/Label';

const styles = theme => ({
    item:{
        display:"flex", 
        alignItems:"between"
    },
    name:{
        display:"flex", 
        alignItems:"start"
    },
    fontSize:{
        fontSize:12
    }
});

const ItemInfo = ({ data, noWrap=true, classes, style, ...props }) => (
    <Typography className={classes.item} component="span" noWrap={noWrap}>
        <Fade>
            <Typography component="span" className={classes.name}>
                { data.icon }
                <Typography component="span" noWrap={noWrap} className={classes.fontSize}>{ data.name }:&nbsp;</Typography>
            </Typography>
        </Fade>
        <Fade>
            <Typography component="span" noWrap={noWrap} className={classes.fontSize}>
                { data.value }
            </Typography>
        </Fade>
    </Typography>
);

  
ItemInfo.propTypes = {
    data: PropTypes.object.isRequired,
};

ItemInfo.defaultProps = {
    data: {
        icon: <LabelIcon fontSize="small"/>,
        name: "",
        value: "", 
    },
    noWrap: true
};

export default withStyles(styles)((ItemInfo));
