import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
// Components 
import {
    Grid,
    Divider, 
    Typography
} from '@material-ui/core';
// Custom 
import AddressInfo from './AddressInfo';
import PersonalInfo from './PersonalInfo';

const styles = theme => ({
    root: {
        flexGrow: 1,
        width: '100%',
    },
    text:{
        fontSize:12,
        display:"flex", 
        alignItems:"center"
    },
    icon:{
        marginRight:2,
    },
    tab:{
        width: '100%',
        maxWidth:650,
        [theme.breakpoints.down('sm')]: {
                
        },
        [theme.breakpoints.up('md')]: {
        
        },
        [theme.breakpoints.up('lg')]: {

        },
    }
});

const InfoPreview = (props) => {
    let { data, } = props;

    let { homeAddress, workAddress } = data;

    return (
        <Grid container spacing={8}>
            <Grid item xs={12} className="mb-2">
                <Typography>
                    Infomación Personal
                </Typography>
                <Divider />
                <PersonalInfo data={data}/>
            </Grid>
            {
                homeAddress &&   <Grid item xs={12} className="mb-2">
                                    <Typography>
                                        Dirección Personal
                                    </Typography>
                                    <Divider />
                                    <AddressInfo data={homeAddress}/>
                                </Grid>
            }
            {
                workAddress && <Grid item xs={12}>
                                    <Typography>
                                        Dirección Laboral
                                    </Typography>
                                    <Divider />
                                    <AddressInfo data={workAddress}/>
                                </Grid>
            }
        </Grid>
    );
}

InfoPreview.propTypes = {
  classes: PropTypes.object.isRequired,
};

InfoPreview.defaultProps = {
    data: {
            documentType: "", 
            documentNumber: "",
            firstName: "",
            sencodName: "",
            lastName: "", 
            motherLastName: "",

            birthday: "",
            gender: "", 
            maritalStatus: "",
            nationality: "", 

            academicDegree: "",
            email: "",
            telephone: "",
            cellphone: "",

            homeAddress:{
                department:"",
                province:"",
                district:"", 
                via:"", 
                nameVia:"", 
                number:"",
                building:"",
                inside: "", 
                mz:"",
                lot:"",
                zone:"",
                nameZone:"", 
                housingType:"",
                reference:""
            },
            workAddress:{
                department:"",
                province:"",
                district:"", 
                via:"", 
                nameVia:"", 
                number:"",
                building:"",
                inside: "", 
                mz:"",
                lot:"",
                zone:"",
                nameZone:"", 
                housingType:"",
                reference:""
            }
        }
};

export default withStyles(styles)((withWidth()(InfoPreview)));