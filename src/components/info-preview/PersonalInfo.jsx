import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
// Components 
import Grid from '@material-ui/core/Grid';
import ItemInfo from './ItemInfo';
// Icons 
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import WCIcon from '@material-ui/icons/Wc';
import CakeIcon from '@material-ui/icons/Cake';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FlagIcon from '@material-ui/icons/Flag';
import SchoolIcon from '@material-ui/icons/School';
import EmailIcon from '@material-ui/icons/Email';
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import PhoneIcon from '@material-ui/icons/Phone';

const styles = theme => ({
    root: {
        flexGrow: 1,
        width: '100%',
    },
    text:{
        fontSize:12,
        display:"flex", 
        alignItems:"center"
    },
    icon:{
        marginRight:2,
    },
    tab:{
        width: '100%',
        maxWidth:650,
        [theme.breakpoints.down('sm')]: {
                
        },
        [theme.breakpoints.up('md')]: {
        
        },
        [theme.breakpoints.up('lg')]: {

        },
    }
});

const PersonalInfo = (props) => {
    let { classes, data } = props;
    let fullName = data.fullName? data.fullName.trim(): `${data.lastName? data.lastName: ""} ${data.motherLastName? data.motherLastName: ""} ${data.firstName? data.firstName: ""} ${data.secondName? data.secondName: ""}`.trim();
    let document = `${data.documentType? data.documentType: ""} ${data.documentNumber? data.documentNumber: ""}`.trim();
    return (
        <div className={classes.root}>
            <Grid container spacing={8} className="mt-2">
                <Grid item xs={12} sm={6}>
                    <ItemInfo 
                        data={{
                            icon:<PermIdentityIcon fontSize="small" className={classes.icon}/>, 
                            name:"",
                            value: fullName 
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <ItemInfo 
                        data={{
                            icon:<PermIdentityIcon fontSize="small" className={classes.icon}/>, 
                            name:"",
                            value: document
                        }}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={8}>
                <Grid item xs={12} sm={6} md={3} lg={3}>
                    <ItemInfo
                        data={{
                            icon:<CakeIcon fontSize="small" className={classes.icon}/>, 
                            name:"",
                            value:data.birthday? data.birthday.split("T")[0] : ""
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={3}>
                    <ItemInfo 
                        data={{
                            icon:<WCIcon fontSize="small" className={classes.icon}/>, 
                            name:"",
                            value:data.gender
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={3}>
                    <ItemInfo 
                        data={{
                            icon:<FavoriteIcon fontSize="small" className={classes.icon}/>, 
                            name:"",
                            value: data.maritalStatus
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={3}>
                    <ItemInfo 
                        data={{
                            icon:<FlagIcon fontSize="small" className={classes.icon}/>, 
                            name:"",
                            value:data.nationality
                        }}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={8}>
                <Grid item xs={12} sm={6} md={3} lg={3}>
                    <ItemInfo 
                        data={{
                            icon:<SchoolIcon fontSize="small" className={classes.icon}/>, 
                            name:"",
                            value: data.academicDegree 
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={3}>
                    <ItemInfo 
                        data={{
                            icon:<EmailIcon fontSize="small" className={classes.icon}/>, 
                            name:"",
                            value: data.email
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={3}>
                    <ItemInfo 
                        data={{
                            icon:<PhoneIcon fontSize="small" className={classes.icon}/>, 
                            name:"",
                            value:data.telephone
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={3}>
                    <ItemInfo 
                        data={{
                            icon:<PhoneIphoneIcon fontSize="small" className={classes.icon}/>, 
                            name:"",
                            value:data.cellphone
                        }}
                    />
                </Grid>
            </Grid>
        </div>
    );
}

PersonalInfo.propTypes = {
  classes: PropTypes.object.isRequired,
};

PersonalInfo.defaultProps = {
    data: {
        documentType: "DNI", 
        documentNumber: "",
        firstName: '',
        secondName: '',
        lastName: "", 
        motherLastName: "",

        birthday: "",
        gender: "", 
        maritalStatus: "",
        nationality: "", 

        academicDegree: "",
        email: "",
        telephone: "",
        cellphone: ""
    }
};

export default withStyles(styles)((withWidth()(PersonalInfo)));