import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

const mapStateToProps = (state, props) => {
    return {
        session: state.sessionReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {

    };
    return actions;
}


const PrivateRoute = ({ component, exact = false, path, role, props, session, authenticated }) => {
    let isSessionCorrect = session && session.authenticated && session.data && session.data.activeDirectory && session.data.agency && session.data.name && session.data.profiles && session.data.sessionCode && session.data.username
    return <Route
        {...props}
        exact={exact}
        path={path}
        render={props => (
            isSessionCorrect ? (
                React.createElement(component, { ...props })
            ) : (
                    <Redirect to='/iniciar-sesion' />
                )
        )}
    />
}

const { object, bool, string, func } = PropTypes;

PrivateRoute.propTypes = {
    component: func.isRequired,
    exact: bool,
    path: string.isRequired,
    authenticated: bool.isRequired,
    location: object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PrivateRoute));