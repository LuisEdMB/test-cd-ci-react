// Redirect State
export const redirectStepClientExisting = (status) => {
    switch(status){
        case 100401:
        case 100501:
        case 100502:
            return 4
        case 100601:
            return 5
        case 100701:
        case 100702:
        case 100704:
        case 100705:
            return 6
        default:
            return 0
    }
}