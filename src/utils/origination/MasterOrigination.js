export const preloadClientActivity = {
    activityName: 'Precarga',
    phaseCode: 111101
}

export const registerUpdateClientActivity = {
    activityName: 'Crear Cliente',
    phaseCode: 111102
}

export const registerAccountActivity = {
    activityName: 'Crear Cuenta',
    phaseCode: 111103
}

export const registerCreditCardActivity = {
    activityName: 'Crear Tarjeta',
    phaseCode: 111104
}

export const updateBlockingCodeCreditCardActivity = {
    activityName: 'Cambio Bloqueo Tarjeta',
    phaseCode: 111105
}