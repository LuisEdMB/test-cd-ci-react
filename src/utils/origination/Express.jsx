// Redirect State
export const redirectStep = (status) => {
    switch(status){
        // Proccess Origination
        case 10501:
        case 10509:
        case 10510:
        case 10512:
        case 10515:
        case 10518:
        case 10520:
        case 10521:
        case 90501:
        case 90502:
        case 90503:
        case 90504:
        case 90505:
        case 90506: return {
            step:4,
        };
        // Offer Summary Complete
        case 10601:
        case 90601: return {
            step:5,
        }
        case 10701:
        case 10702:
        case 10703:
        case 90701:
        case 90702:
        case 90704:
        case 90705:
        case 90706:
        case 90707:
        case 90801: return {
            step:6,
        };
        // Activate CreditCard
        case 10801: return {
            step:7,
        };
        case 10802: return {
            step:7,
        };
        // Activate CreditCard - Auth
        case 10804: return {
            step:7,
        };
        // Activate CreditCard
        case 10805: return {
            step:7,
        };
        // Activate CreditCard
        case 10806: return {
            step:7,
        };
        // Activate CreditCard
        case 10807: return {
            step:7,
        };
        default: return {
            step:0,
        };
    }
}

export const sendDataSimpleGenericActivityStepClientRegister = (origination) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 10301,
        cod_flujo_fase_estado_anterior: 10204,
        nom_actividad: "Oferta: En Proceso",
    }
}
export const sendDataCommercialOffer = (data, lineAvailable, initialLineAvailable, origination, client ) => {
    return {
        tipo_doc: data.client.documentTypeId === 100001? "1":"2",
        nro_doc: data.client.documentNumber,
        tipo_doc_letra: data.client.documentType,
        nom_actividad: "Oferta: Aceptado",
        cod_flujo_fase_estado_actual: 10302,
        cod_flujo_fase_estado_anterior: 10301,
        cod_valor_color: data.creditCard.colorId,
        disp_efec_porcentaje: data.finalEffectiveProvision,
        disp_efec_monto: data.finalEffectiveProvision*data.finalLineAvailable/100,
        cod_valor_marca: data.creditCard.brandId,
        cod_producto:data.creditCard.productId,
        linea_credito_oferta: lineAvailable.value,
        linea_credito_final: data.finalLineAvailable,
        linea_credito_global: initialLineAvailable.globalLineaAvailable,
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_cliente: client.id,

        cod_agencia: 0,
        fec_act_base_sae: "",
        flg_bq2: "",
        linea_sae: 0,
        linea_sae_final: 0,
        monto_max: 0,
        monto_max_final: 0,
        pct_sae_1: "",
        pct_sae_2: "",
        pct_sae_final: "",
        des_cci: "",
        cod_tipo_desembolso: 0,
        monto_cci: 0,
        monto_efectivo: 0,
        numero_cuotas: '',
        monto_cuota: '',
        tcea: '',
    };
}

export const sendDataSimpleGenericActivityStepProductDetail = (origination) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 10402,
        cod_flujo_fase_estado_anterior: 10401,
        nom_actividad: "Preventa: Aceptado",
    }
}

export const sendDataOfferSummary = (origination, creditCard, finalEffectiveProvision, lineAvailable, finalLineAvailable) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 10602,
        cod_flujo_fase_estado_anterior: 10601,
        nom_actividad: "Resumen: Aceptado",
        cod_valor_color: creditCard.colorId,
        disp_efec_porcentaje: finalEffectiveProvision,
        cod_valor_marca: creditCard.brandId,
        linea_credito_oferta: lineAvailable.value,
        linea_credito_final: finalLineAvailable
    }
}

export const sendDatagenericActivityStepEmbossing = (origination) => {
    let previousStatePhaseFlow = 10701;
    if(origination.previousStatePhaseFlow === 10702){
        previousStatePhaseFlow = 10702
    }
    return {
        cod_requerimiento: origination.requestNumber,
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 10703,
        cod_flujo_fase_estado_anterior: previousStatePhaseFlow,
        nom_actividad: "Embozado: Aceptado",
    }
}

export const sendDataPendingEmbossing = (origination, reason) => {
    return {
        cod_requerimiento: origination.requestNumber,
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 10702,
        cod_flujo_fase_estado_anterior: 10701,
        cod_motivo_emboce: reason.id,
        des_motivo_comentario_emboce: reason.description,
        nom_actividad: "Embozado: Pendiente",
    }
}

export const sendDataActivationCreditCard = (organization, comodinPMP, origination, client, pmp, creditCard) => {
    let previousStatePhaseFlow = 10802;
    if(origination.previousStatePhaseFlow === 10804){
        previousStatePhaseFlow = 10804 // Authorizate State
    }
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 10805,
        cod_flujo_fase_estado_anterior: previousStatePhaseFlow,
        nom_actividad: "Activación Tarjeta: PMP Activar TC",
        blockCode: comodinPMP, //A : Activado  //K : Dejar pasar comodin
        process: origination,
        client: client,
        pmp: pmp,
        creditCard: creditCard,
        isSAE: false
    }
}

export const sendDataGenericActivityActivateCard = (origination) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 10806,
        cod_flujo_fase_estado_anterior: 10805,
        nom_actividad: "Activación Tarjeta: Aceptado",
    }
}

export const sendDataGenericActivityServingCardAccepted = (origination) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 10902,
        cod_flujo_fase_estado_anterior: 10901,
        nom_actividad: "Entrega Tarjeta: Aceptado",
    }
}

export const sendDataSimpleGenericActivityPendingActivationCredit = (origination) => {
    return {
        cod_requerimiento: origination.requestNumber,
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 10803,
        cod_flujo_fase_estado_anterior: 10802,
        nom_actividad: "Activación Tarjeta: Pendiente",
    }
}

// Reintent Process PMP
export const setReintentProcessPMP = data => {
    const { status, errorMessage } = data
    const fix = errorMessage ? 0 : 1
    switch(status){
        case 10501: return 0;
        case 10509: return 1; // Create Client PMP
        case 10510: return 1.5 + (fix ? 0.5 : 0); // Update Client PMP
        case 10512: return 2 + fix; // Create Relationship PMP
        case 10515: return 3 + fix; // Create Account PMP
        case 10518: return 5 + fix; // Create Card PMP
        case 10519: return 8;
        default: return 0;
    }
}

// Reintent Process PMP Regular
export const setReintentProcessPMPRegular = data => {
    const { status, errorMessage } = data
    const fix = errorMessage ? 0 : 1
    switch (status) {
        case 20501: return 0;
        case 20502: return 1; // Create Client PMP
        case 20503: return 1.5 + (fix ? 0.5 : 0); // Update Client PMP
        case 20504: return 2 + fix; // Create Relationship PMP
        case 20505: return 3 + fix; // Create Account PMP
        case 20506: return 5 + fix; // Create Card PMP
        case 20507: return 8;
        default: return 0;
    }
}

// Reintnt Process PMP Credit Card Blocking
export const setReintentProcessPMPBlocking = data => {
    const { status, errorMessage } = data
    const fix = errorMessage ? 0 : 1
    switch (status) {
        case 10805: return 0 + (fix ? 2 : 0); // Blocking Credit Card PMP
        case 10806:
        case 10901:
        case 10902: return 3;
        default: return 0;
    }
}

// Reintnt Process PMP Credit Card Blocking Regular
export const setReintentProcessPMPBlockingRegular = data => {
    const { status, errorMessage } = data
    const fix = errorMessage ? 0 : 1
    switch (status) {
        case 20805: return 0 + (fix ? 2 : 0); // Blocking Credit Card PMP
        case 20806:
        case 20901:
        case 20902: return 3;
        default: return 0;
    }
}

export const setReintentBlockSolicitude = (status) => {
    switch (status) {
        case 10501: case 10509: case 10510: case 10512: case 10515: case 10518: case 10519: // Express
        case 90501: case 90502: case 90503: case 90504: case 90505: case 90506: case 90507: // CEC
        case 20501: case 20502: case 20503: case 20504: case 20505: case 20506: case 20520: case 20507: // Regular
        case 30501: case 30601: case 30602: case 30603: // Reprint
            return 1
        default:
            return 0
    }
}

export const sendDataCreateClientPMP = (organization, origination, client) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 10509,
        cod_flujo_fase_estado_anterior: 10501,
        nom_actividad: "Originación Express: PMP Crear Cliente",
        process: origination,
        client: client,
    }
}

export const sendDataUpdateClientePMP = (organization, origination, client) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 10510,
        cod_flujo_fase_estado_anterior: 10501,
        nom_actividad: "Originación Express: PMP Actualizar Cliente",
        process: origination,
        client: client,
    }
}

export const sendDataCreateRelationshipPMP = ({organization, origination, client, pmp, createCustomer, updateCustomer, finalLineAvailable}) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 10512,
        cod_flujo_fase_estado_anterior: updateCustomer ? 10510 : 10509,
        nom_actividad: "Originación Express: PMP Crear Relación",
        process: origination,
        client: client,
        pmp: {
            ...pmp,
            clientNumber: createCustomer? createCustomer.customerNumber : updateCustomer? updateCustomer.customerNumber : pmp.clientNumber
        },
        lineAvailable: finalLineAvailable
    }
}

export const sendDataCreateAccountPMP = ({organization, origination, client, creditCard, pmp, createCustomerRelationship, pct, finalLineAvailable}) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 10515,
        cod_flujo_fase_estado_anterior: 10512,
        nom_actividad: "Originación Express: PMP Crear Cuenta",
        process: origination,
        client: client,
        creditCard: creditCard,
        pmp: {
            ...pmp,
            relationshipNumber: createCustomerRelationship ? createCustomerRelationship.relationshipNumber : pmp.relationshipNumber
        },
        pct: pct,
        lineAvailable: finalLineAvailable
    }
}

export const sendDataCreateCreditCard = ({organization, origination, client, creditCard, pmp, createCustomerAccount, pct, finalLineAvailable}) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 10518,
        cod_flujo_fase_estado_anterior: 10515,
        nom_actividad: "Originación Express: PMP Crear Tarjeta",
        process: origination,
        client: client,
        creditCard: creditCard,
        pmp: {
            ...pmp,
            accountNumber: createCustomerAccount ? createCustomerAccount.accountNumber : pmp.accountNumber
        },
        pct: pct,
        lineAvailable: finalLineAvailable
    }
}

export const sendDataCreateSAE = (organization, origination, client, pmp, creditCard, pctSAE, finalLineSAE) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 10520,
        cod_flujo_fase_estado_anterior: 10518,
        nom_actividad: "Originación Express: PMP Crear SAE",
        process: origination,
        client: client,
        creditCard: creditCard,
        pmp: {
            ...pmp,
        },
        sae:{
            pctSAE: pctSAE,
            lineSAE: finalLineSAE,
            initialDate: "00000000",
            finalDate: "00000000"
        }
    }
}

export const sendDataAcceptedOrigination = (origination, sae = false) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 10519,
        cod_flujo_fase_estado_anterior: sae ? 10520 : 10518,
        nom_actividad: "Originación Express: Aceptado",
    }
}