// Constants
import * as Constants from '../Constants'

export function getFlowTypeForCancellation({ cod_tipo_solicitud_originacion, flg_cec, cod_tipo_relacion, cod_motivo_reimpresion }) {
    if (cod_motivo_reimpresion) return Constants.CancelOriginationFlowTypes.reprint
    if (flg_cec) return Constants.CancelOriginationFlowTypes.cec
    if (cod_tipo_relacion === Constants.ClientRelationTypes.additionalClientCode) return Constants.CancelOriginationFlowTypes.additional
    if (cod_tipo_solicitud_originacion === '390001') return Constants.CancelOriginationFlowTypes.express
    if (cod_tipo_solicitud_originacion === '390002') return Constants.CancelOriginationFlowTypes.regular
}

export function getDataForCancelCreditCardByFlowOrigination(flowType, options = { }) {
    switch (flowType) {
        case Constants.CancelOriginationFlowTypes.express:
            return {
                cod_flujo_fase_estado_actual: 11103,
                cod_flujo_fase_estado_anterior: 11101,
                nom_actividad: 'Rechazar Originación Express: PMP Cancelar TC',
                blockCode: Constants.BlockCodePmpTypes.blockTypeCreditCardCanceledByEnterprise
            }
        case Constants.CancelOriginationFlowTypes.cec:
            return {
                cod_flujo_fase_estado_actual: 91003,
                cod_flujo_fase_estado_anterior: 91001,
                nom_actividad: 'Rechazar Originación CEC: PMP Cancelar TC',
                blockCode: Constants.BlockCodePmpTypes.blockTypeCreditCardCanceledByEnterprise
            }
        case Constants.CancelOriginationFlowTypes.regular:
            return {
                cod_flujo_fase_estado_actual: 21103,
                cod_flujo_fase_estado_anterior: 21101,
                nom_actividad: 'Rechazar Originación Regular: PMP Cancelar TC',
                blockCode: Constants.BlockCodePmpTypes.blockTypeCreditCardCanceledByEnterprise
            }
        case Constants.CancelOriginationFlowTypes.reprint:
            const isEmbossError = options.cod_motivo_reimpresion === Constants.ReprintReasonType.embossError
            const isHolderClient = options.cod_tipo_relacion === Constants.ClientRelationTypes.holderClientCode
            const blockCode = isEmbossError
                ? (isHolderClient
                    ? Constants.BlockCodePmpTypes.blockTypeCreditCardCanceledByEnterprise
                    : Constants.BlockCodePmpTypes.blockTypeCreditCardDesactivated)
                : Constants.BlockCodePmpTypes.blockTypeCreditCardTemporalBlockByOperation
            return {
                cod_flujo_fase_estado_actual: 31203,
                cod_flujo_fase_estado_anterior: 31201,
                nom_actividad: 'Rechazar Reimpresión: PMP Cancelar TC',
                blockCode: blockCode
            }
        case Constants.CancelOriginationFlowTypes.additional:
            return {
                cod_flujo_fase_estado_actual: 41003,
                cod_flujo_fase_estado_anterior: 41001,
                nom_actividad: 'Rechazar Adicional: PMP Cancelar TC',
                blockCode: Constants.BlockCodePmpTypes.blockTypeCreditCardDesactivated
            }
        default:
            return { }
    }
}