export const sendSimpleGenericActivityStepClientRegisterCE = (origination) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 90301,
        cod_flujo_fase_estado_anterior: 90204,
        nom_actividad: "Oferta: En Proceso",
    }
}

export const sendDataCommercialOfferCE = (data, lineAvailable, initialLineAvailable, origination, client, sae ) => {
    return {
        tipo_doc: data.client.documentTypeId === 100001? "1":"2",
        nro_doc: data.client.documentNumber,
        tipo_doc_letra: data.client.documentType,
        nom_actividad: "Oferta: Aceptado",
        cod_flujo_fase_estado_actual: 90302,
        cod_flujo_fase_estado_anterior: 90301,
        cod_valor_color: data.creditCard.colorId,
        disp_efec_porcentaje: data.finalEffectiveProvision,
        disp_efec_monto: data.finalEffectiveProvision*data.finalLineAvailable/100,
        cod_valor_marca: data.creditCard.brandId,
        cod_producto:data.creditCard.productId,
        linea_credito_oferta: lineAvailable.value,
        linea_credito_final: data.finalLineAvailable,
        linea_credito_global: initialLineAvailable.globalLineaAvailable,
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_cliente: client.id,

        cod_agencia: 0,
        fec_act_base_sae: sae.baseDate? sae.baseDate: "",
        flg_bq2: sae.block? sae.block: "",
        linea_sae: sae.lineSAE? sae.lineSAE: 0,
        linea_sae_final: data.finalLineSAE? data.finalLineSAE: 0,
        monto_max: sae.maxAmount? sae.maxAmount: 0,
        monto_max_final: data.finalMaxAmount? data.finalMaxAmount: 0,
        pct_sae_1: sae.pctSAE1? sae.pctSAE1: "",
        pct_sae_2: sae.pctSAE2? sae.pctSAE2: "",
        pct_sae_final: data.pctSAE? data.pctSAE: "",
        des_cci: data.CCI? data.CCI: "",
        numero_cuotas: data.numberFees? data.numberFees : '',
        monto_cuota: data.feeAmount ? data.feeAmount: '',
        tcea: data.tcea,
        cod_tipo_desembolso: data.disbursementTypeId? data.disbursementTypeId: 0,
        des_descripcion: data.description? data.description: "",

        monto_cci: data.transferAmount? data.transferAmount: 0,
        monto_efectivo: data.effectiveAmount? data.effectiveAmount: 0,
    };
}

export const sendDataSimpleGenericActivityStepProductDetailCE = (origination) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 90402,
        cod_flujo_fase_estado_anterior: 90401,
        nom_actividad: "Preventa: Aceptado",
    }
}

export const sendDataCreateCreditCardCE = ({organization, origination, client, creditCard, pmp, createCustomerAccount, pct, finalLineAvailable}) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 90506,
        cod_flujo_fase_estado_anterior: 90505,
        nom_actividad: "Originación Efectivo: PMP Crear CEC",
        process: origination,
        client: client,
        creditCard: creditCard,
        pmp: {
            ...pmp,
            accountNumber: createCustomerAccount ? createCustomerAccount.accountNumber : pmp.accountNumber
        },
        pct: pct,
        lineAvailable: finalLineAvailable
    }
}

export const sendDataOfferSummaryCE = (origination, creditCard, finalEffectiveProvision, lineAvailable, finalLineAvailable) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 90602,
        cod_flujo_fase_estado_anterior: 90601,
        nom_actividad: "Resumen: Aceptado",
        cod_valor_color: creditCard.colorId,
        disp_efec_porcentaje: finalEffectiveProvision,
        cod_valor_marca: creditCard.brandId,
        linea_credito_oferta: lineAvailable.value,
        linea_credito_final: finalLineAvailable
    }
}

export const sendDataGenericActivityActivateCardCE = (origination) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 90706,
        cod_flujo_fase_estado_anterior: 90705,
        nom_actividad: "Activación Efectivo: Aceptado",
    }
}

export const sendDataGenericActivityServingCardAcceptedCE = (origination) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 90802,
        cod_flujo_fase_estado_anterior: 90801,
        nom_actividad: "Entrega Efectivo: Aceptado",
    }
}

export const sendDataGenericActivityFinishProcessCE = (origination) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 90901,
        cod_flujo_fase_estado_anterior: 90802,
        nom_actividad: "Fin Proceso CEC",
    }
}

export const sendDataGenericActivityPendingProcessCE = (origination) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 90910,
        cod_flujo_fase_estado_anterior: 90801,
        nom_actividad: "Pendiente Atención CEC",
    }
}

// Reintent Process PMP
export const setReintentProcessPMPCE = data => {
    const { status, errorMessage } = data
    const fix = errorMessage ? 0 : 1
    switch (status){
        case 90501: return 0;
        case 90502: return 1; // Create Client PMP
        case 90503: return 1.5 + (fix ? 0.5 : 0); // Update Client PMP
        case 90504: return 2 + fix; // Create Relationship PMP
        case 90505: return 3 + fix; // Create Account PMP
        case 90506: return 5 + fix; // Create CEC PMP
        case 90507: return 8;
        default: return 0;
    }
}

// Reintent Process PMP Credit Card Blocking
export const setReintentProcessPMPCEBlocking = data => {
    const { status, errorMessage } = data
    const fix = errorMessage ? 0 : 1
    switch (status) {
        case 90705: return 0 + (fix ? 2 : 0); // Blocking Credit Card PMP
        case 90706:
        case 90800:
        case 90801:
        case 90802:
        case 90900:
        case 90910: return 3;
        default: return 0;
    }
}

export const sendDataCreateClientPMPCE = (organization, origination, client) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 90502,
        cod_flujo_fase_estado_anterior: 90501,
        nom_actividad: "Originación Efectivo: PMP Crear Cliente",
        process: origination,
        client: client,
    }
}


export const sendDataUpdateClientePMPCE = (organization, origination, client) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 90503,
        cod_flujo_fase_estado_anterior: 90501,
        nom_actividad: "Originación Efectivo: PMP Actualizar Cliente",
        process: origination,
        client: client,
    }
}

export const sendDataCreateRelationshipPMPCE = ({organization, origination, client, pmp, createCustomer, updateCustomer, finalLineAvailable}) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 90504,
        cod_flujo_fase_estado_anterior: updateCustomer ? 90503 : 90502,
        nom_actividad: "Originación Efectivo: PMP Crear Relación",
        process: origination,
        client: client,
        pmp: {
            ...pmp,
            clientNumber: createCustomer? createCustomer.customerNumber : updateCustomer? updateCustomer.customerNumber : pmp.clientNumber
        },
        lineAvailable: finalLineAvailable
    }
}

export const sendDataCreateAccountPMPCE = ({organization, origination, client, creditCard, pmp, createCustomerRelationship, pct, finalLineAvailable}) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 90505,
        cod_flujo_fase_estado_anterior: 90504,
        nom_actividad: "Originación Efectivo: PMP Crear Cuenta",
        process: origination,
        client: client,
        creditCard: creditCard,
        pmp: {
            ...pmp,
            relationshipNumber: createCustomerRelationship ? createCustomerRelationship.relationshipNumber : pmp.relationshipNumber
        },
        pct: pct,
        lineAvailable: finalLineAvailable
    }
}

export const sendDataCreateSAECE = (organization, origination, client, pmp, creditCard, pctSAE, finalLineSAE) => {
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 90506,
        cod_flujo_fase_estado_anterior: 90505,
        nom_actividad: "Originación Efectivo: PMP Crear SAE",
        process: origination,
        client: client,
        creditCard: creditCard,
        pmp: {
            ...pmp,
        },
        sae:{
            pctSAE: pctSAE,
            lineSAE: finalLineSAE,
            initialDate: "00000000",
            finalDate: "00000000"
        }
    }
}

export const sendDataAcceptedOriginationCE = (origination) => {
    return {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 90507,
        cod_flujo_fase_estado_anterior: 90506,
        nom_actividad: "Originación Efectivo: Aceptado",
    }
}


export const sendDataActivationCreditCardCE = (organization, comodinPMP, origination, client, pmp, creditCard) => {
    let previousStatePhaseFlow = 90702;
    if(origination.previousStatePhaseFlow === 90704){
        previousStatePhaseFlow = 90704 // Authorizate State
    }
    return {
        organization: organization,
        cod_flujo_fase_estado_actual: 90705,
        cod_flujo_fase_estado_anterior: previousStatePhaseFlow,
        nom_actividad: "Activación Efectivo: PMP Activar CEC",
        blockCode: comodinPMP, //A : Activado  //K : Dejar pasar comodin
        process: origination,
        client: client,
        pmp: pmp,
        creditCard: creditCard,
        isSAE: false
    }
}

export const sendDataSimpleGenericActivityPendingActivationCreditCE = (origination) => {
    return {
        cod_requerimiento: origination.requestNumber,
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 90703,
        cod_flujo_fase_estado_anterior: 90702,
        nom_actividad: "Activación Efectivo: Pendiente",
    }
}