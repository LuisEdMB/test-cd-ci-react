export const sendDataSecondCall = (data, origination, clientId, isSAEProcess) => {
    let { client } = data;
    const telephone = client.telephone;
    const workTelephone = client.workTelephone;
    return {
        tipo_doc: client.documentTypeAux,
        nro_doc: client.documentNumber,
        tipo_doc_letra: client.documentType.toUpperCase(),
        maritalStatus: client.maritalStatusInternalValue,
        dateJoinedCompany: client.laborIncomeDate.toUpperCase(),
        birthDate: client.birthday,
        employerRUC: client.ruc,
        nationality: client.nationality.toUpperCase(),
        studiesLevel: client.academicDegree.toUpperCase(),
        occupation: client.jobTitle ? client.jobTitle.toUpperCase() : "Desconocido",
        gender: client.gender.toUpperCase(),
        laborSituation: client.employmentSituation.toUpperCase(),
        cellPhone: `${client.cellphone}`,
        homePhone: telephone !== "" ? `${telephone}`.substring(0, 9) : `0510000000`,
        workPhone: workTelephone !== "" ? `${workTelephone}`.substring(0, 9) : `0510000000`,
        housingType: client.homeAddress.housingType.toUpperCase(),
        // Register
        cod_cliente: clientId,
        cod_tipo_documento: client.documentTypeId,
        des_nro_documento: client.documentNumber,
        des_primer_nom: client.firstName,
        des_segundo_nom: client.secondName,
        des_ape_paterno: client.lastName,
        des_ape_materno: client.motherLastName,
        fec_nacimiento: client.birthday,
        cod_genero: client.genderId.toString(),
        cod_estado_civil: client.maritalStatusId.toString(), // Change
        cod_nacionalidad: client.nationalityId.toString(),
        cod_pais_residencia: 230183,
        cod_gradoacademico: client.academicDegreeId.toString(),
        des_correo: client.email,
        des_telef_celular: client.cellphone,
        des_telef_fijo: telephone !== "" ? `${telephone}`.substring(0, 12).replace(/\s/g, "").trim() : `0510000000`,
        cod_tipo_cliente: 290002, // Observation
        cod_situacion_laboral: client.employmentSituationId,
        cod_cargo_profesion: client.jobTitleId ? client.jobTitleId : 0,
        cod_actividad_economica: client.economicActivityId ? client.economicActivityId : 0,
        fec_ingreso_laboral: client.laborIncomeDate,
        des_ruc: client.ruc,
        des_razon_social: client.businessName,
        num_ingreso_bruto: client.grossIncome,
        des_telefono_laboral: workTelephone !== "" ? `${workTelephone}`.substring(0, 12).replace(/\s/g, "").trim() : `0510000000`,
        des_anexo_laboral: client.workTelephoneExtension,
        flg_autoriza_datos_per: client.extra.term === "1" || client.extra.term ? true : false,
        // Additional
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_fecha_pago: client.extra.paymentDateId,
        cod_envio_correspondencia: client.extra.sendCorrespondenceId ? client.extra.sendCorrespondenceId : 0,
        cod_estado_cuenta: client.extra.accountStatusId,
        // flg_retiro_efectivo: client.extra.effectiveWithdrawal === "1",

        // flg_alerta_uso_tar: client.extra.alert === "1"? true:false,
        // cod_envio_comunicacion: client.extra.mediaId ? client.extra.mediaId:0,
        // flg_envio_comunicacion: client.extra.sendCommonPromotion === "1" ? true:false,

        flg_consumo_internet: client.extra.internetConsumeAdvice === "1",
        flg_consumo_extranjero: client.extra.foreignConsumeAdvice === "1",
        flg_retirar_efectivo: client.extra.effectiveDisposition === "1",
        flg_sobregirar_credito: client.extra.overflipping === "1",
        flg_notificacion_email: client.extra.mailAlerts === "1",
        num_monto_minimo: client.extra.minimumForAlerts ? client.extra.minimumForAlerts : 0,

        cod_resp_promotor: client.extra.promoterId,
        cod_resp_registro: 0,
        cod_resp_actualiza: 0,
        cod_flujo_fase_estado_actual: isSAEProcess ? 90204 : 10204,
        cod_flujo_fase_estado_anterior: isSAEProcess ? 90201 : 10201,
        nom_actividad: isSAEProcess ? 'Efectivo: Registrado' : 'Preevaluado: Registrado',
        cod_agencia: 0,
        cod_flujo_fase_estado: 0,
        cod_valor_color: 0,
        disp_efec_porcentaje: 0, // Default
        disp_efec_monto: 0, // Default
        cod_valor_marca: 0, // Default
        linea_credito_oferta: 0, // Default
        linea_credito_final: 0, // Default
        cod_laboral: 0,
        cod_producto: 0,
        cod_motivo_emboce: 0,
        des_motivo_comentario_emboce: "",
        flg_biometria: false,
        cod_tipo_relacion: 330001,
        cod_tipo_solicitud: 340001, //340001,
        cod_tipo_solicitud_requerimiento: 0,
        // Address
        list_direccion: [
            {
                cod_direccion: client.homeAddress.id ? client.homeAddress.id : 0,
                cod_cliente: clientId,
                cod_tipo_direccion: 260001, // Home
                des_tipo_direccion: "Dirección personal",
                cod_ubigeo: client.homeAddress.districtId,
                cod_via: client.homeAddress.viaId,
                des_nombre_via: client.homeAddress.nameVia,
                des_nro: client.homeAddress.number,
                des_departamento: client.homeAddress.building,
                des_interior: client.homeAddress.inside,
                des_manzana: client.homeAddress.mz,
                des_lote: client.homeAddress.lot,
                cod_zona: client.homeAddress.zoneId ? client.homeAddress.zoneId : 0,
                des_nombre_zona: client.homeAddress.nameZone,
                cod_tipo_residencia: 0,
                cod_tipo_vivienda: client.homeAddress.housingTypeId,
                des_referencia_domicilio: client.homeAddress.reference
            },
            {
                cod_direccion: client.workAddress.id ? client.workAddress.id : 0,
                cod_cliente: clientId,
                cod_tipo_direccion: 260002,
                des_tipo_direccion: "Dirección laboral",
                cod_ubigeo: client.workAddress.districtId,
                cod_via: client.workAddress.viaId,
                des_nombre_via: client.workAddress.nameVia,
                des_nro: client.workAddress.number,
                des_departamento: client.workAddress.building,
                des_interior: client.workAddress.inside,
                des_manzana: client.workAddress.mz,
                des_lote: client.workAddress.lot,
                cod_zona: client.workAddress.zoneId ? client.workAddress.zoneId : 0,
                des_nombre_zona: client.workAddress.nameZone,
                cod_tipo_residencia: "",
                cod_tipo_vivienda: "",
                des_referencia_domicilio: client.workAddress.reference
            }
        ]
    }
}

// Set Data Create Client PMP
export const setDataCreateClientPMP = data => {
    let { client, process } = data;
    if (client && process) {
        let telephone = client.telephone !== "" ? client.telephone.substring(0, 11) : "0000000000";
        let workTelephone = client.workTelephone !== "" ? client.workTelephone.substring(0, 11) : "0000000000";
        return {
            tipo_doc_letra: client.documentType,//
            nom_actividad: data.nom_actividad,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            cod_solicitud: process.number,
            cod_solicitud_completa: process.fullNumber,
            cod_cliente: null,
            cod_cliente_titular: client ? client.id : 0,

            terceraLlamadaPmpCrearCliente: {
                tipo_doc: client.documentTypeAux,
                nro_doc: client.documentNumber,
                organization: data.organization,
                identityDocumentType: client.documentTypeInternalValue,
                identityDocumentNumber: client.documentNumber,
                nameLine1Cliente: `${client.shortName ? client.shortName : `${client.lastName ? client.lastName : ""} ${client.firstName ? client.firstName : ""}`}`,
                statusCliente: "A",
                nameLine2Cliente: client.lastName,
                nameLine3Cliente: client.firstName,
                stmtMsgIndicCliente: "",
                addrLine1Cliente: `${client.homeAddress.nameVia} ${client.homeAddress.number} ${client.homeAddress.department}`,
                mailyngListCliente: "Y",
                addrLine2Cliente: `${client.workAddress.nameVia} ${client.workAddress.number} ${client.workAddress.department}`,
                carrierRoute12Cliente: "Casa", // Casa or Trabajo
                carrierRoute34Cliente: "",
                cityCliente: client.homeAddress.department,
                stateCliente: client.nationality,
                zipCodeCliente: client.homeAddress.districtId,
                nameTypeIndNl1Cliente: "P",
                nameTypeIndNl2Cliente: "P",
                nameTypeIndNl3Cliente: "P",
                ownRentResidFlagCliente: "0",
                genderCliente: client.genderInternalValue,
                relativeNameCliente: client.email,
                numberOfDependentsCliente: "1",
                makerEmployerCliente: client.businessName !== "" ? client.businessName : "Desconocido",
                makerPhoneFlagCliente: "",
                makerEmpPhoneCliente: `${workTelephone}`,
                makerPositionCliente: client.workTelephoneExtension ? client.workTelephoneExtension : "",
                makerHomePhoneCliente: `${telephone}`,
                makerSsanFlagCliente: "0",
                makerSsanCliente: "000000000",
                dlStateCliente: "",
                dlNumberCliente: "", //1-89OLJ0
                languageIndicCliente: client.maritalStatusInternalValue,
                w9IndicCliente: "0",
                w9DateSentCliente: "00000000",
                w9DateFiledCliente: "00000000",
                coMakerNameCliente: "",
                coNameTypeIndCliente: "P",
                coMakerEmployerCliente: "",
                coMakerPhoneFlagCliente: "N",
                coMakerEmpPhoneCliente: "000000000",
                coMakerPositionCliente: "",
                coMakerHomePhoneCliente: "000000000",
                coMakerSsanFlagCliente: "N",
                coMakerSsanCliente: "00000000", //1
                userDemo1Cliente: "",
                userDemo2Cliente: client.documentNumber,
                memoLine1Cliente: "",
                memoLine2Cliente: "",
                lastMaintDateCliente: "",
                customerNumberCliente: "",
                relationshipNumberCliente: "",
                accountNumberCliente: "",
                cardNumberCliente: "",
                cardSeqCliente: "",
                celNumberCliente: client.cellphone,
                trackTypeCliente: client.homeAddress.viaInternalValue, //CamelCase OK
                trackNameCliente: client.homeAddress.nameVia,
                numberCliente: client.homeAddress.number,
                departmentNumberCliente: client.homeAddress.building,
                officeCliente: "",
                floorCliente: "",
                blockCliente: client.homeAddress.mz,
                lotCliente: client.homeAddress.lot,
                interiorCliente: client.homeAddress.inside,
                sectorCliente: "",
                kilometerCliente: "",
                zoneTypeCliente: client.homeAddress.zoneInternalValue !== "" ? client.homeAddress.zoneInternalValue : "Desconocido",
                zoneNameCliente: client.homeAddress.nameZone !== "" ? client.homeAddress.nameZone : "-",
                referenceCliente: client.homeAddress.reference,
                geoLocationCliente: client.homeAddress.districtId,
                workTypeCliente: client.workAddress.viaInternalValue,
                workTrackNameCliente: client.workAddress.nameVia,
                workNumberCliente: client.workAddress.number,
                workDepartmentNumberCliente: client.workAddress.building,
                workOfficeCliente: "",
                workFloorCliente: "",
                workBlockCliente: client.workAddress.mz,
                workLotCliente: client.workAddress.lot,
                workInteriorCliente: client.workAddress.inside,
                workSectorCliente: "",
                workKilometerCliente: "",
                workZoneTypeCliente: client.workAddress.zoneInternalValue !== "" ? client.workAddress.zoneInternalValue : "Desconocido",
                workZoneNameCliente: client.workAddress.nameZone !== "" ? client.workAddress.nameZone : "-",
                workReferenceCliente: client.workAddress.reference,
                workGeoLocationCliente: client.workAddress.districtId,
                dualCoinFlagCliente: "0",
                homeAddressModificationDateCliente: "",
                workAddressModificationDateCliente: ""
            }
        }
    }
}
// Set Data Update Client PMP
export const setDataUpdateClientePMP = data => {
    let { client, process } = data;
    if (client && process) {
        let telephone = client.telephone !== "" ? client.telephone.substring(0, 11) : "0000000000";
        let workTelephone = client.workTelephone !== "" ? client.workTelephone.substring(0, 11) : "0000000000";
        return {
            tipo_doc_letra: client.documentType,
            nom_actividad: data.nom_actividad,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            cod_solicitud: process.number,
            cod_solicitud_completa: process.fullNumber,
            cod_cliente: null,
            cod_cliente_titular: client ? client.id : 0,

            terceraLlamadaPmpActualizarCliente: {
                tipo_doc: client.documentTypeAux,
                nro_doc: client.documentNumber,
                organization: data.organization,
                identityDocumentType: client.documentTypeInternalValue,
                identityDocumentNumber: client.documentNumber,
                nameLine1Cliente: `${client.shortName ? client.shortName : `${client.lastName ? client.lastName : ""} ${client.firstName ? client.firstName : ""}`}`,
                statusCliente: "A",
                nameLine2Cliente: client.lastName,
                nameLine3Cliente: client.firstName,
                stmtMsgIndicCliente: "",
                addrLine1Cliente: `${client.homeAddress.nameVia} ${client.homeAddress.number} ${client.homeAddress.department}`,
                mailyngListCliente: "Y",
                addrLine2Cliente: `${client.workAddress.nameVia} ${client.workAddress.number} ${client.workAddress.department}`,
                carrierRoute12Cliente: "Casa", // Casa or Trabajo
                carrierRoute34Cliente: "",
                cityCliente: client.homeAddress.department,
                stateCliente: client.nationality,
                zipCodeCliente: client.homeAddress.districtId,
                nameTypeIndNl1Cliente: "P",
                nameTypeIndNl2Cliente: "P",
                nameTypeIndNl3Cliente: "P",
                ownRentResidFlagCliente: "0",
                genderCliente: client.genderInternalValue,
                relativeNameCliente: client.email,
                numberOfDependentsCliente: "1",
                makerEmployerCliente: client.businessName !== "" ? client.businessName : "Desconocido",
                makerPhoneFlagCliente: "",
                makerEmpPhoneCliente: `${workTelephone}`,
                makerPositionCliente: client.workTelephoneExtension ? client.workTelephoneExtension : "",
                makerHomePhoneCliente: `${telephone}`,
                makerSsanFlagCliente: "0",
                makerSsanCliente: "000000000",
                dlStateCliente: "",
                dlNumberCliente: "", //1-89OLJ0
                languageIndicCliente: client.maritalStatusInternalValue,
                w9IndicCliente: "0",
                w9DateSentCliente: "00000000",
                w9DateFiledCliente: "00000000",
                coMakerNameCliente: "",
                coNameTypeIndCliente: "P",
                coMakerEmployerCliente: "",
                coMakerPhoneFlagCliente: "N",
                coMakerEmpPhoneCliente: "000000000",
                coMakerPositionCliente: "",
                coMakerHomePhoneCliente: "000000000",
                coMakerSsanFlagCliente: "N",
                coMakerSsanCliente: "00000000", //1
                userDemo1Cliente: "",
                userDemo2Cliente: client.documentNumber,
                memoLine1Cliente: "",
                memoLine2Cliente: "",
                lastMaintDateCliente: "",
                customerNumberCliente: "",
                relationshipNumberCliente: "",
                accountNumberCliente: "",
                cardNumberCliente: "",
                cardSeqCliente: "",
                celNumberCliente: client.cellphone,
                trackTypeCliente: client.homeAddress.viaInternalValue, //CamelCase OK
                trackNameCliente: client.homeAddress.nameVia,
                numberCliente: client.homeAddress.number,
                departmentNumberCliente: client.homeAddress.building,
                officeCliente: "",
                floorCliente: "",
                blockCliente: client.homeAddress.mz,
                lotCliente: client.homeAddress.lot,
                interiorCliente: client.homeAddress.inside,
                sectorCliente: "",
                kilometerCliente: "",
                zoneTypeCliente: client.homeAddress.zoneInternalValue !== "" ? client.homeAddress.zoneInternalValue : "Desconocido",
                zoneNameCliente: client.homeAddress.nameZone !== "" ? client.homeAddress.nameZone : "-",
                referenceCliente: client.homeAddress.reference,
                geoLocationCliente: client.homeAddress.districtId,
                workTypeCliente: client.workAddress.viaInternalValue,
                workTrackNameCliente: client.workAddress.nameVia,
                workNumberCliente: client.workAddress.number,
                workDepartmentNumberCliente: client.workAddress.building,
                workOfficeCliente: "",
                workFloorCliente: "",
                workBlockCliente: client.workAddress.mz,
                workLotCliente: client.workAddress.lot,
                workInteriorCliente: client.workAddress.inside,
                workSectorCliente: "",
                workKilometerCliente: "",
                workZoneTypeCliente: client.workAddress.zoneInternalValue !== "" ? client.workAddress.zoneInternalValue : "Desconocido",
                workZoneNameCliente: client.workAddress.nameZone !== "" ? client.workAddress.nameZone : "-",
                workReferenceCliente: client.workAddress.reference,
                workGeoLocationCliente: client.workAddress.districtId,
                dualCoinFlagCliente: "0",
                homeAddressModificationDateCliente: "",
                workAddressModificationDateCliente: ""
            }
        }
    }
}
// Set Data Create Relationship PMP
export const setDataCreateRelationshipPMP = data => {
    let { process, client, pmp, lineAvailable } = data;
    if (process && client && pmp && lineAvailable > 0) {
        return {
            tipo_doc_letra: client.documentType,
            nro_doc: client.documentNumber,
            nom_actividad: data.nom_actividad,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            cod_solicitud: process.number,
            cod_solicitud_completa: process.fullNumber,
            cod_cliente: null,
            cod_cliente_titular: client ? client.id : 0,
            terceraLlamadaPmpCrearRelacion: {
                tipo_doc: client.documentTypeAux,
                nro_doc: client.documentNumber,
                organization: data.organization,
                identityDocumentType: client.documentTypeInternalValue,
                identityDocumentNumber: client.documentNumber,
                relationshipCreditLimitRelacion: lineAvailable,
                billingCycleRelacion: client.extra.paymentDateInternalValue.toString(),
                relationshipCustomerNameRelacion: `${client.shortName ? client.shortName : `${client.lastName ? client.lastName : ""} ${client.firstName ? client.firstName : ""}`}`,
                primaryAccountRelacion: "",
                numberOfSubAccountsRelacion: "00000",
                statusRelacionRelacion: "0",
                relationshipAvailableCreditRelacion: "",
                blockRelacion: "",
                mailRelacion: "0",
                subAccountsCreditLimitRelacion: "000000000",
                subAccountsModificationRelacion: "0",
                billingLvlModificationRelacion: "0",
                wvMembLvRelacion: "0",
                wvMembMdfyRelacion: "0",
                dualCurrencyFlagRelacion: "0",
                customerNumberRelacion: pmp.clientNumber,
                relationshipNumberRelacion: "",
                accountNumberRelacion: "",
                cardNumberRelacion: "",
                cardSequenceRelacion: ""
            }
        }
    }
}
// Set Data Create Account PMP
export const setDataCreateAccountPMP = data => {
    let { organization, process, client, pmp, pct, creditCard, lineAvailable } = data;
    if (organization && process && client && pmp && pct && creditCard && lineAvailable) {
        let { birthday } = client;
        birthday = birthday ? birthday.replace(/-/g, "").replace(/\//g, "") : "19001001";
        birthday = birthday.substring(6, 8) + birthday.substring(4, 6) + birthday.substring(0, 4);

        return {
            tipo_doc_letra: client.documentType,
            nro_doc: client.documentNumber,
            nom_actividad: data.nom_actividad,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            cod_solicitud: process.number,
            cod_solicitud_completa: process.fullNumber,
            cod_cliente: null,
            cod_cliente_titular: client ? client.id : 0,

            terceraLlamadaPmpCrearCuenta: {
                tipo_doc: client.documentTypeAux,
                nro_doc: client.documentNumber,
                organization: organization,
                identityDocumentType: client.documentTypeInternalValue,
                identityDocumentNumber: client.documentNumber,
                isoMessageTypeCuenta: "",
                transactionNumberCuenta: "",
                logCuenta: creditCard.name, // Name
                accountNumberAutoCuenta: "/",
                relationNumberCuenta: pmp.relationshipNumber,
                shortNameCuenta: `${client.shortName ? client.shortName : `${client.lastName ? client.lastName : ""} ${client.firstName ? client.firstName : ""}`}`,
                creditLimitCuenta: lineAvailable,
                relPrimAcctFlagCuenta: "P",
                pmtAchFlagCuenta: "",
                pmtAchDbTypeCuenta: "S",
                pmtAchDbNbrCuenta: "00000000000000000",
                intStatusCuenta: "N",
                altCustNbrCuenta: pmp.clientNumber && data.pmp.clientNumber,
                colatCodeCuenta: "",
                flexBillFlagCuenta: "",
                dateAppliCuenta: "00000000",
                dateOpenedCuenta: "00000000",
                relCardSchemeCuenta: "3",
                grtstCardExprDateCuenta: "00000000",
                billingCycleCuenta: client.extra.paymentDateInternalValue.toString(),
                blockCode1Cuenta: "A",
                blockCode2Cuenta: "A",
                blockDate1Cuenta: "00000000",
                ownerFlagCuenta: "",
                blockDate2Cuenta: "00000000",
                acctRestructureCuenta: "",
                cardExpDateCuenta: "00000000",
                sourceCodeCuenta: "",
                collCardCuenta: "N",
                acctDisplayCuenta: "N",
                stateOfResidCuenta: pct && pct.number ? pct.number : "056", //"056",
                stateOfIssueCuenta: pct && pct.number ? pct.number : "056",  //"056",
                pctCtlIdCuenta: "",
                pctStartDateCuenta: "00000000",
                pctExpirDateCuenta: "00000000",
                pctLevelCuenta: "L",
                pctLevelStartDateCuenta: "00000000",
                pctLevelExpirDateCuenta: "00000000",
                officerCodeCuenta: "",
                pmtSkipFlagCuenta: "Z",
                fsFlagCuenta: "Y",
                fraudReportingFlagCuenta: "Y",
                waiveCashAvailCrCuenta: 'Y',
                makersDobCuenta: birthday,//"10041949",
                comakersDobCuenta: "0",
                cashPlanNbrCuenta: "0236",
                retailPlanNbrCuenta: "0136",
                userDate1Cuenta: "00000000",
                userDate2Cuenta: "00000000",
                userDate3Cuenta: "00000000",
                userDate4Cuenta: "00000000",
                userDate5Cuenta: "00000000",
                userDate6Cuenta: "00000000",
                userAmt1Cuenta: "00000000000",
                userAmt2Cuenta: "00000000000",
                userAmt3Cuenta: "00000000000",
                userAmt4Cuenta: "00000000000",
                userAmt5Cuenta: "00000000000",
                userAmt6Cuenta: "00000000000",
                userMsg1IndCuenta: "0",
                userMsg2IndCuenta: "0",
                userMsg3IndCuenta: "0",
                origBankNbrCuenta: "555", // Code agency Loggger
                origBranchNbrCuenta: organization,
                origStoreNbrCuenta: organization,
                owingBankNbrCuenta: organization,
                owingBranchNbrCuenta: organization,
                owingStoreNbrCuenta: organization,
                pendBankNbrCuenta: "0",
                pendBranchNbrCuenta: "0",
                pendStoreNbrCuenta: "0",
                incomeCuenta: "0",
                customerNumberCuenta: pmp.clientNumber && data.pmp.clientNumber,
                relationshipNumberCuenta: pmp.relationshipNumber && data.pmp.relationshipNumber,
                accountNumberCuenta: "",
                cardNumberCuenta: "",
                cardSeqCuenta: "0",
                ambsPlanPmtOvrdFlagCuenta: "",
                ambsFixedPmtAmtPctCuenta: "00000000000"
            }
        }
    }
}
// Set Data Create Credit Card PMP
export const setDataCreateCreditCard = data => {
    let { organization, process, client, pmp, creditCard } = data;
    const cod_cliente_titular = client.fatherId || client.id || 0
    let cod_cliente = null
    let cardTypeTarjeta = 'P'
    if (client.fatherId) {
        cod_cliente = client.id === client.fatherId ?  null : client.id
        cardTypeTarjeta = client.id === client.fatherId ? 'P' : 'A'
    }
    if (organization && process && client && pmp && creditCard) {
        return {
            tipo_doc_letra: client.documentType,
            nom_actividad: data.nom_actividad,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            cod_solicitud: process.number,
            cod_solicitud_completa: process.fullNumber,
            cod_cliente: cod_cliente,
            cod_cliente_titular: cod_cliente_titular,
            terceraLlamadaPmpCrearTarjeta: {
                tipo_doc: client.documentTypeAux,
                nro_doc: client.documentNumber,
                organization: organization,
                identityDocumentType: client.documentTypeInternalValue,
                identityDocumentNumber: client.documentNumber,
                isoMessageTypeTarjeta: "0",
                transactionNumberTarjeta: "0",
                logTarjeta: creditCard.name,
                accountNumberTarjeta: pmp.accountNumber,
                accountCardNumberTarjeta: "/",
                cardTypeTarjeta: data.cardTypeTarjeta || cardTypeTarjeta,
                cardSequenceTarjeta: "0001",
                statusTarjeta: "0",
                nmbrCardsOutstTarjeta: "01",
                nmbrCardsRtnTarjeta: "00",
                cardActionTarjeta: "1",
                nbrRqtdTarjeta: "01",
                typeOfCardTarjeta: "0",
                rqtdCardTypeTarjeta: "00",
                expireDateTarjeta: "00000000",
                embrName1Tarjeta: `${client.shortName ? client.shortName : `${client.lastName ? client.lastName : ""} ${client.firstName ? client.firstName : ""}`}`, // `${client.firstName} ${client.lastName}`,
                embrName2Tarjeta: `${client.shortName ? client.shortName : `${client.lastName ? client.lastName : ""} ${client.firstName ? client.firstName : ""}`}`,
                pinOffsetTarjeta: "0000000",
                posServiceCodeTarjeta: "",
                dteWarningBulletinTarjeta: "00000000",
                addrLine1Tarjeta: `${client.homeAddress.nameVia} ${client.homeAddress.number} ${client.homeAddress.department}`,
                addrLine2Tarjeta: "",
                cityTarjeta: client.homeAddress.department,
                stateTarjeta: client.nationality,
                zipTarjeta: client.homeAddress.districtId,
                languageCodeTarjeta: "",
                requestedCardTypeTarjeta: "",
                warningBulletinTarjeta: "",
                posServiceCode2Tarjeta: "",
                blockCodeTarjeta: data.blockCode || "V",
                feeTarjeta: "0",
                user1Tarjeta: "",
                user2Tarjeta: "0000000000000000000",
                currFirstUsageFlagTarjeta: "0",
                priorFirstUsageFlagTarjeta: "0",
                dateOpenedTarjeta: "00000000",
                dateNextExprTarjeta: "",
                dateLstPlasticIssueTarjeta: "",
                dateLastExprTarjeta: "",
                dateFirstCardVerifyTarjeta: "",
                lastCardActionTarjeta: "",
                dateExceptionPurgeTarjeta: "",
                excptnRespCodeTarjeta: "",
                regionCodeTarjeta: "",
                currentCVC2Tarjeta: "",
                customerNumberTarjeta: pmp.clientNumber,
                relationshipNumberTarjeta: pmp.relationshipNumber,
                cardAccountNumberTarjeta: pmp.accountNumber,
                cardNumberTarjeta: "",
                cardSeqTarjeta: "0001",
                trackTypeTarjeta: client.homeAddress.viaInternalValue || "Desconocido",
                trackNameTarjeta: `${client.homeAddress.nameVia} ${client.homeAddress.number} ${client.homeAddress.department}`,
                numberTarjeta: client.homeAddress.number,
                departmentNumberTarjeta: client.homeAddress.building,
                officeTarjeta: "",
                floorTarjeta: "",
                blockTarjeta: client.homeAddress.mz || '',
                lotTarjeta: client.homeAddress.lot,
                insideTarjeta: client.homeAddress.inside,
                sectorTarjeta: "",
                kilometerTarjeta: "",
                zoneTypeTarjeta: client.homeAddress.zoneInternalValue || "Desconocido",
                zoneNameTarjeta: client.homeAddress.nameZone || "-",
                referenceTarjeta: client.homeAddress.reference,
                addressFlagTypeTarjeta: "1",
                originalBranchTarjeta: "0",
                mandatory1Tarjeta: "",
                mandatory2Tarjeta: "",
                actualBranchTarjeta: "0",
                promotionCodeTarjeta: "",
                geoLocationTarjeta: client.homeAddress.departmentId,
                changeReasonTarjeta: "",
                indentityDocType1Tarjeta: "",
                documentNmbr1Tarjeta: "",
                relationship1Tarjeta: "",
                indentityDocType2Tarjet: "",
                documentNmbr2Tarjeta: "",
                relationship2Tarjeta: "",
                emissionTypeTarjeta: "0",
                dualCoinFlagTarjeta: "0",
                cardLastModificationDateTarjeta: "0",
                broadcastTypeTarjeta: "1",
                adiNameTarjeta: "",
                adiLastNamesTarjeta: "",
                adiDocTypeTarjeta: "",
                adiDocNmbrTarjeta: "",
            }
        }
    }
}
// Set Data Block Type Credit Card PMP
export const setDataBlockTypeCreditCardPMP = data => {
    let { client, pmp, process, isSAE = false } = data;
    const cod_cliente_titular = client.fatherId || client.id || 0
    let cod_cliente = null
    if (client.fatherId) cod_cliente = client.id === client.fatherId ?  null : client.id
    return {
        tipo_doc_letra: client.documentType,
        nom_actividad: data.nom_actividad,
        cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
        cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
        cod_solicitud: process.number,
        cod_solicitud_completa: process.fullNumber,
        cod_solicitud_antigua: process.numberPrev || 0,
        cod_solicitud_completa_antigua: process.fullNumberPrev || '',
        cod_cliente: cod_cliente,
        cod_cliente_titular: cod_cliente_titular,
        terceraLlamadaPmpTipoBloqueTarjeta: {
            tipo_doc: client.documentTypeAux,
            nro_doc: client.documentNumber,
            organization: data.organization,
            identityDocumentType: client.documentTypeInternalValue,
            identityDocumentNumber: client.documentNumber,
            isoMessageTypeActivar: "0",
            transactionNumberActivar: "0",
            cardNumberActivar: isSAE ? pmp.parallelCardNumber : pmp.cardNumber,
            cardSequenceActivar: "0001",
            amedBlockCodeActivar: data.blockCode, // Code Block
            amedCardActionActivar: "0",
            amedRqtdCardTypeActivar: "0",
            amedCurrFirstUsageFlagActivar: "",
            amedPriorFirstUsageFlagActivar: "",
            amedMotiveBlockadeActivar: "01",
            internacionUsaRegionActivar: "",
            boletineoInterUsaDateActivar: "0",
            interCanRegionActivar: "",
            boletineoInterCanDateActivar: "0",
            internacionalCAmerRegionActivar: "",
            boletineoInterCAmerRegionActivar: "0",
            interAsiaRegionActivar: "",
            bolitineoInterAsiaDateActivar: "0",
            interRegionEuropaActivar: "",
            bolitineoInterDateEuropaActivar: "0",
            interEuropaRegionActivar: "",
            bolitineoInterEuropaDateActivar: "0",
            principalAccountActivar: "",
            documentTypeActivar: "",
            documentNumerActivar: "",
            clientNameActivar: "",
            clientAddressActivar: "",
            birthDateActivar: "",
            telephoneActivar: "0",
            cardNameActivar: "",
            cardAddressActivar: "",
            cardTypeActivar: "",
            applicantNameActivar: "",
            applicantDocumentActivar: "",
            applicantTelephoneActivar: "0",
            relationshipActivar: "",
            blockadeUserActivar: "",
            blockadeNumberActivar: "0",
            blockadeDateActivar: "0",
            blockadeTimeActivar: "0",
            userInitialsActivar: "",
            emisionTypeActivar: ""
        }
    }
}
// Set Data Create Client PMP
export const setDataCreateSAE = data => {
    let { organization, process, client, pmp, sae } = data;
    return {
        tipo_doc_letra: client.documentType,
        nom_actividad: data.nom_actividad,
        cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
        cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
        cod_solicitud: process.number,
        cod_solicitud_completa: process.fullNumber,
        cod_cliente: null,
        cod_cliente_titular: client ? client.id : 0,
        terceraLlamadaPmpCrearSAE: {
            tipo_doc: client.documentTypeAux,
            nro_doc: client.documentNumber,
            fec_reg: "",
            des_usu_reg: "",
            des_ter_reg: "",
            sid: "",
            cui: "0000000000000000",
            cid: "odc", // odc
            codemisor: organization,
            codusuario: "",
            numterminal: "",
            numreferencia: "",
            fechatxnterminal: "",
            horatxnterminal: "",
            numerocuenta: pmp.accountNumber,
            codigopct: sae.pctSAE ? sae.pctSAE : "", // 001
            lineaasignada: sae.lineSAE ? sae.lineSAE : 0,
            fechainicio: sae.initialDate ? sae.initialDate : "00000000",
            fechafin: sae.finalDate ? sae.finalDate : "00000000"
        }
    }
}