import { defaultValueReducer } from '@material-ui/lab/Slider';

export function valueReducer(rawValue, props, event) {
    const { disabled, max, min, step } = props;
  
    function roundToStep(number) {
        return Math.round(number / step) * step;
    }
  
    if (!disabled && step) {
        if (rawValue > min && rawValue < max) {
            if (rawValue === max - step) {
                // If moving the Slider using arrow keys and value is formerly an maximum edge value
                return roundToStep(rawValue + step / 2);
            }
            if (rawValue === min + step) {
                // Same for minimum edge value
                return roundToStep(rawValue - step / 2);
            }
            return roundToStep(rawValue);
        }
        return rawValue;
    }
  
    return defaultValueReducer(rawValue, props, event);
}