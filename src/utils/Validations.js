import * as Utils from './Utils'

const documentTypeIdSelector = 'documentTypeId'
const documentNumberField = 'documentNumber'
const firstNameField= 'firstName'
const firstLastNameField = 'firstLastName'
const birthdayField = 'birthday'
const genderIdSelector = 'genderId'
const maritalStatusIdSelector = 'maritalStatusId'
const nationalityIdSelector = 'nationalityId'
const academicDegreeIdSelector = 'academicDegreeId'
const emailField = 'email'
const cellphoneField = 'cellphone'
const cellphoneConfirmedField = 'cellphoneConfirmed'
const telephoneField = 'telephone'
const departmentIdSelector = 'departmentId'
const provinceIdSelector = 'provinceId'
const districtIdSelector = 'districtId'
const viaIdSelector = 'viaId'
const viaDescriptionField = 'viaDescription'
const zoneDescriptionField = 'zoneDescription'
const housingTypeIdSelector = 'housingTypeId'
const jobSituationIdSelector = 'jobSituationId'
const jobTitleIdSelector = 'jobTitleId'
const economicActivityIdSelector = 'economicActivityId'
const laborIncomeDateField = 'laborIncomeDate'
const businessOptionSelector = 'businessOption'
const businessNameField = 'businessName'
const rucField = 'ruc'
const grossIncomeField = 'grossIncome'
const paymentDateIdSelector = 'paymentDateId'
const sendCorrespondenceIdSelector = 'sendCorrespondenceId'
const accountStatusIdSelector = 'accountStatusId'
const scannerCodeField = 'scannerCode'
const nameScannerCodeField = 'nameScannerCode'
const minimumForAlertsField = 'minimumForAlerts'
const disbursementTypeIdSelector = 'disbursementTypeId'
const cciField = 'cci'
const bankNameField = 'bankName'
const numberFeesField = 'numberFees'
const feeAmountField = 'feeAmount'
const tceaField = 'tcea'
const referenceAddressField = 'reference'
const reprintReasonIdField = 'reprintReasonId'
const embossReasonIdField = 'embossReasonId'
const rejectActivationCommentField = 'rejectActivationComment'
const familyRelationshipIdField = 'familyRelationshipId'
const colorCreditCardField = 'color'

const validOk = {
    message: '',
    error: false
}

const validationDocumentTypeId = [
    {
        regex: value => value,
        message: 'Debe seleccionar un tipo de documento.',
        isRegex: false
    }
]

const validationDocumentNumber = [
    {
        regex: '^[0-9]*$',
        message: 'Solo es permitido números.',
        isRegex: true
    },
    {
        regex: value => value?.toString()?.length === 8,
        message: 'La cantidad de dígitos no son los correctos.',
        isRegex: false
    }
]

const validationFirstNameField = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    }
]

const validationFirstLastNameField = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    }
]

const validationBirthdayField = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    }
]

const validationGenderId = [
    {
        regex: value => value,
        message: 'Debe seleccionar un tipo de género.',
        isRegex: false
    }
]

const validationMaritalStatusId = [
    {
        regex: value => value,
        message: 'Debe seleccionar un estado civil.',
        isRegex: false
    }
]

const validationNationalityId = [
    {
        regex: value => value,
        message: 'Debe seleccionar una nacionalidad.',
        isRegex: false
    }
]

const validationAcademicDegreeId = [
    {
        regex: value => value,
        message: 'Debe seleccionar un grado académico.',
        isRegex: false
    }
]

const validationEmail = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    },
    {
        regex: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
        message: 'Debe ingresar un correo electrónico válido.',
        isRegex: true 
    }
]

const validationCellphone = [
    {
        regex: '^[0-9]*$',
        message: 'Solo es permitido números.',
        isRegex: true
    },
    {
        regex: value => value?.toString()?.length === Utils.defaultCellphoneLengthInput,
        message: 'La cantidad de dígitos no son los correctos.',
        isRegex: false
    },
    {
        regex: value => value?.toString()[0] === '9',
        message: 'No es un número correcto de celular.',
        isRegex: false
    }
]

const validationCellphoneConfirmed = [
    {
        regex: value => value,
        message: 'Verifique el código SMS.',
        isRegex: false
    }
]

const validationTelephone = [
    {
        regex: value => Utils.filterMaskTelephone(value).length >= 9 ||
            Utils.filterMaskTelephone(value).length === 0,
        message: 'La cantidad de dígitos no son los correctos.',
        isRegex: false
    }
]

const validationDepartmentId = [
    {
        regex: value => value,
        message: 'Debe seleccionar un departamento.',
        isRegex: false
    }
]

const validationProvinceId = [
    {
        regex: value => value,
        message: 'Debe seleccionar una provincia.',
        isRegex: false
    }
]

const validationDistrictId = [
    {
        regex: value => value,
        message: 'Debe seleccionar un distrito.',
        isRegex: false
    }
]

const validationViaId = [
    {
        regex: value => value,
        message: 'Debe seleccionar una vía.',
        isRegex: false
    }
]

const validationViaDescription = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    }
]

const validationZoneDescription = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    }
]

const validationHousingTypeIdId = [
    {
        regex: value => value,
        message: 'Debe seleccionar un tipo de vivienda.',
        isRegex: false
    }
]

const validationJobSituationId = [
    {
        regex: value => value,
        message: 'Debe seleccionar una situación laboral.',
        isRegex: false
    }
]

const validationJobTitleId = [
    {
        regex: value => value,
        message: 'Debe seleccionar un cargo profesión.',
        isRegex: false
    }
]

const validationEconomicActivityId = [
    {
        regex: value => value,
        message: 'Debe seleccionar una actividad económica.',
        isRegex: false
    }
]

const validationLaborIncomeDate = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    }
]

const validationBusinessOption = [
    {
        regex: value => value,
        message: 'Debe seleccionar un tipo de consulta de negocio.',
        isRegex: false
    }
]

const validationBusinessName = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    }
]

const validationRuc = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    },
    {
        regex: '^[0-9]*$',
        message: 'Solo es permitido números.',
        isRegex: true
    },
    {
        regex: value => value?.toString()?.length === 11,
        message: 'La cantidad de dígitos no son los correctos.',
        isRegex: false
    }
]

const validationGrossIncome = [
    {
        regex: value => parseFloat(value || 0) > 0,
        message: 'El monto debe ser mayor a cero.',
        isRegex: false
    },
    {
        regex: /^\d+(\.\d{0,2})?$/,
        message: 'Solo es permitido números y dos decimales.',
        isRegex: true
    }
]

const validationPaymentDateId = [
    {
        regex: value => value,
        message: 'Debe seleccionar una fecha de pago',
        isRegex: false
    }
]

const validationSendCorrespondenceId = [
    {
        regex: value => value,
        message: 'Debe seleccionar un tipo de envío de correspondencia.',
        isRegex: false
    }
]

const validationAccountStatusId = [
    {
        regex: value => value,
        message: 'Debe seleccionar un tipo de estado de cuenta.',
        isRegex: false
    }
]

const validationScannerCode = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    }
]

const validationNameScannerCode = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    }
]

const validationMinimumForAlerts = [
    {
        regex: value => parseFloat(value || 0) >= 0 && (value || 0) <= 9999999.99,
        message: 'El monto debe estar en el rango de S/. 0 a S/. 9 999 999.99.',
        isRegex: false
    },
    {
        regex: value => parseFloat(value || 0).toString().length <= Utils.defaultvalCuotasLength,
        message: `La cantidad máxima de dígitos es: ${ Utils.defaultvalCuotasLength }.`,
        isRegex: false
    }
]

const validationDisbursementTypeId = [
    {
        regex: value => value,
        message: 'Debe seleccionar un tipo de desembolso.',
        isRegex: false
    }
]

const validationCci = [
    {
        regex: '^[0-9]*$',
        message: 'Solo es permitido números.',
        isRegex: true
    },
    {
        regex: `[0-9]{${ Utils.defaultCCILengthInput }}`,
        message: 'La cantidad de dígitos no son los correctos.',
        isRegex: true
    }
]

const validationBankName = [
    {
        regex: value => value,
        message: 'Este campo no debe estar vacío.',
        isRegex: false
    }
]

const validationNumberFees = [
    {
        regex: '^[0-9]*$',
        message: 'Solo es permitido números.',
        isRegex: true
    }
]

const validationFeeAmount = [
    {
        regex: /^\d+(\.\d{0,2})?$/,
        message: 'Solo es permitido números y dos decimales.',
        isRegex: true
    },
    {
        regex: value => (value || 0) >= 1 && (value || 0) <= 9999999.99,
        message: 'El monto de cada cuota debe de ser como máximo S/. 9 999 999.99.',
        isRegex: false
    }
]

const validationTcea = [
    {
        regex: /^\d+(\.\d{0,2})?$/,
        message: 'Solo es permitido números y dos decimales.',
        isRegex: true
    },
    {
        regex: value => (value || 0) >= 1 && (value || 0) <= 200,
        message: 'El valor de % debe de ser mayor o igual a 1 y menor o igual a 200',
        isRegex: false
    }
]

const validationReferenceAddress = [
    {
        regex: /^[a-zA-Z0-9 ]*$/,
        message: 'Solo es permitido caracteres alfanuméricos (A-Z;0-9).',
        isRegex: true
    }
]

const validationReprintReasonId = [
    {
        regex: value => value,
        message: 'Debe seleccionar el motivo de reimpresión.',
        isRegex: false
    }
]

const validationEmbossReasonId = [
    {
        regex: value => value,
        message: 'Debe seleccionar el motivo de emboce.',
        isRegex: false
    }
]

const validationRejectActivationComment = [
    {
        regex: value => value,
        message: 'Debe ingresar alguna observación.',
        isRegex: false
    }
]

const validationFamilyRelationshipId = [
    {
        regex: value => value,
        message: 'Debe seleccionar el vínculo familiar.',
        isRegex: false
    }
]

const validationColorCreditCard = [
    {
        regex: value => value,
        message: 'Debe seleccionar un color.',
        isRegex: false
    }
]

export function validateByField(name, value, validate = true, extra = []) {
    if (validate)
        switch (name) {
            case documentTypeIdSelector:
                return verifyValidation(validationDocumentTypeId, value)
            case documentNumberField:
                return verifyValidation(validationDocumentNumber, value)
            case firstNameField:
                return verifyValidation(validationFirstNameField, value)
            case firstLastNameField:
                return verifyValidation(validationFirstLastNameField, value)
            case birthdayField:
                return verifyValidation([
                    ...validationBirthdayField,
                    ...extra
                ], value)
            case genderIdSelector:
                return verifyValidation(validationGenderId, value)
            case maritalStatusIdSelector:
                return verifyValidation(validationMaritalStatusId, value)
            case nationalityIdSelector:
                return verifyValidation(validationNationalityId, value)
            case academicDegreeIdSelector:
                return verifyValidation(validationAcademicDegreeId, value)
            case emailField:
                return verifyValidation(validationEmail, value)
            case cellphoneField:
                return verifyValidation(validationCellphone, value)
            case cellphoneConfirmedField:
                return verifyValidation(validationCellphoneConfirmed, value)
            case telephoneField:
                return verifyValidation(validationTelephone, value)
            case departmentIdSelector:
                return verifyValidation(validationDepartmentId, value)
            case provinceIdSelector:
                return verifyValidation(validationProvinceId, value)
            case districtIdSelector:
                return verifyValidation(validationDistrictId, value)
            case viaIdSelector:
                return verifyValidation(validationViaId, value)
            case viaDescriptionField:
                return verifyValidation(validationViaDescription, value)
            case zoneDescriptionField:
                return verifyValidation(validationZoneDescription, value)
            case housingTypeIdSelector:
                return verifyValidation(validationHousingTypeIdId, value)
            case jobSituationIdSelector:
                return verifyValidation(validationJobSituationId, value)
            case jobTitleIdSelector:
                return verifyValidation(validationJobTitleId, value)
            case economicActivityIdSelector:
                return verifyValidation(validationEconomicActivityId, value)
            case laborIncomeDateField:
                return verifyValidation([
                    ...validationLaborIncomeDate,
                    ...extra
                ], value)
            case businessOptionSelector:
                return verifyValidation(validationBusinessOption, value)
            case businessNameField:
                return verifyValidation(validationBusinessName, value)
            case rucField:
                return verifyValidation(validationRuc, value)
            case grossIncomeField:
                return verifyValidation(validationGrossIncome, value)     
            case paymentDateIdSelector:
                return verifyValidation(validationPaymentDateId, value)
            case sendCorrespondenceIdSelector:
                return verifyValidation(validationSendCorrespondenceId, value)
            case accountStatusIdSelector:
                return verifyValidation(validationAccountStatusId, value)
            case scannerCodeField:
                return verifyValidation(validationScannerCode, value)
            case nameScannerCodeField:
                return verifyValidation(validationNameScannerCode, value)
            case minimumForAlertsField:
                return verifyValidation(validationMinimumForAlerts, value)
            case disbursementTypeIdSelector:
                return verifyValidation(validationDisbursementTypeId, value)
            case cciField:
                return verifyValidation(validationCci, value)
            case bankNameField:
                return verifyValidation(validationBankName, value)
            case numberFeesField:
                return verifyValidation([
                    ...validationNumberFees,
                    ...extra
                ], value)
            case feeAmountField:
                return verifyValidation(validationFeeAmount, value)
            case tceaField:
                return verifyValidation(validationTcea, value)
            case referenceAddressField:
                return verifyValidation(validationReferenceAddress, value)
            case reprintReasonIdField:
                return verifyValidation(validationReprintReasonId, value)
            case embossReasonIdField:
                return verifyValidation(validationEmbossReasonId, value)
            case rejectActivationCommentField:
                return verifyValidation(validationRejectActivationComment, value)
            case familyRelationshipIdField:
                return verifyValidation(validationFamilyRelationshipId, value)
            case colorCreditCardField:
                return verifyValidation(validationColorCreditCard, value)
            default:
                return validOk
        }
    else return validOk
}

export async function comprobeAllValidationsSuccess(object, handleChangeState) {
    let keys = Object.keys(object)
    return new Promise((resolve, _) => {
        keys.forEach((key, index) => {
            let value = object[key]
            if (typeof value !== 'object') {
                if (index === keys.length - 1) {
                    handleChangeState({ name: key, value: value, call: validations => {
                        const errors = Utils.findProperty(validations, 'error', ['ref', 'extra'])
                        resolve(!errors.some(error => error.value))
                    } })
                }
                else {
                    handleChangeState({ name: key, value: value })
                }
            }
        })
    })
}

function verifyValidation(array, value) {
    return array.map(validation => {
        if (validation.isRegex) {
            let regex = new RegExp(validation.regex)
            if (!regex.test(value)) {
                validation['error'] = true
                return validation
            }
        }
        else {
            if (!validation.regex(value)) {
                validation['error'] = true
                return validation
            }
        }
        return null
    }).find(validation => validation !== null) || validOk
}