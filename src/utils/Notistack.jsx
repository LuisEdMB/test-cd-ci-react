import React from 'react';
import { 
    IconButton
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

export const getNotistack = (message, callBack, variant="default", duration = 6000) => {
    let select = "default";
    switch(variant){
        case "error": 
            select = variant; break;
        case "success":
            select = variant; break;
        case "warning":
            select = variant; break;
        case "info":
            select = variant; break;
        default: 
            select = variant; break;
    }
    if(callBack){
        // Notistack
        callBack(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
}

