import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { onlyTextRegex } from './Utils';

function TextMaskOnlyText(props) {
    const { inputRef, onChange, ...other } = props;
    
    return (
      <TextField
        {...other}
        getInputRef={inputRef}
        onChange={values => {
            onChange({
                target: {
                    value: values.value,
                },
            });
        }}/>
    );
}
  
TextMaskOnlyText.propTypes = {
    inputRef: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default TextMaskOnlyText;