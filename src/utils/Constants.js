// General
export const General = {
    organization: '641',
    comodinCreditCardActivate: 'COMODIN_PMP',
    maxNumberAdditionalClient: 'NUM_MAX_ADICIONAL'
}

// Country Residences
export const CountryResidence = {
    Peru: 230183
}

// Custom Error Code
export const CustomErrorCode = {
    errorCodeSolicitudeBlocked: '-5000',
    errorCodeIndebtedness: '-5001',
    errorCodeCreditCardAlreadyPci: 'ODC-PCI-001'
}

// Stage PCI Configuration
export const PciConfigurationStage = {
    pciAdditionalStage: '600001',
    pciReprintStage: '600002',
    pciRegularStage: '600003',
    pciExpressStage: '600004'
}

// Stage Master Configuration
export const MasterConfigurationStage = {
    masterAdditionalStage: '700001',
    masterReprintStage: '700002',
    masterRegularStage: '700003',
    masterExpressStage: '700004',
    masterCECStage: '700005',
    masterExistingClientStage: '700006',
    masterTrayStage: '700007'
}

// Additional Factory
export const AdditionalFactoryTypes = {
    additional: 'ADDITIONAL',
    additionalExternal: 'ADDITIONAL_EXTERNAL'
}

// Reprint Factory
export const ReprintFactoryTypes = {
    reprint: 'REPRINT',
    reprintExternal: 'REPRINT_EXTERNAL'
}

// Block Code PMP Types
export const BlockCodePmpTypes = {
    blockTypeCreditCardDesactivated: 'L',
    blockTypeCreditCardDisabled: 'V',
    blockTypeCreditCardCanceledByEnterprise: 'H',
    blockTypeCreditCardTemporalBlockByOperation: 'P'
}

// Client Types
export const ClientTypes = {
    holderClientCode: 290002,
    additionalClientCode: 290003
}

// Client Relation Types
export const ClientRelationTypes = {
    holderClientCode: 330001,
    additionalClientCode: 330002
}

// Origination Flow Type for Cancellation
export const CancelOriginationFlowTypes = {
    express: 'EXPRESS',
    cec: 'CEC',
    regular: 'REGULAR',
    reprint: 'REPRINT',
    additional: 'ADDITIONAL'
}

// Reprint Reason Types
export const ReprintReasonType = {
    embossError: 310003
}

//  Origination Types
export const OriginationType = {
    Preevaluated: 390001
}