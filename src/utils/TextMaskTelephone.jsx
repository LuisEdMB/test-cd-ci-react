import React from 'react';
import MaskedInput from 'react-text-mask';
import PropTypes from 'prop-types';

// const codigos = ['41', '43', '83', '54', '66', '76', '84', '67', '62' ,'56', '64','44', '74', '65','82', '53', '63','73', '51', '42', '52','72','61']

// function listaCodigos(s) {

//     if(s.length >= 2 && s[1] == '1' ){
//         // cambiar formato
//     }
//     if(s.length >= 3 ){
//     }
// }

function TextMaskTelephone(props) {
  const { inputRef, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={ref => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
      placeholderChar={'\u2000'}
      showMask
    />
  );
}

TextMaskTelephone.propTypes = {
  inputRef: PropTypes.func.isRequired,
};

export default TextMaskTelephone;
