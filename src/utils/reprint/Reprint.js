// Config
import * as Config from '../../actions/config'

// Utils
import * as Utils from '../Utils'
import * as OriginationUtils from '../origination/Origination'
import * as Constants from '../Constants'

class Reprint {
    constructor() {
        this.showInfoHolderClientForCreditCardDetailSection = false
        this.showAdditionalServicesForAllReprintReasons = false
        this.isOptionalTokenForDeactivatePreviousCreditCard = true
        this.isOptionalTokenForActivateCreditCard = true
    }

    /* Continue Process */
    getDataForContinueProcessApi(completeSolicitudeCode) {
        return {
            url: `${ Config.URL_ODC }/Originacion/SolicitudRetomar?cod_solicitud_completa=${completeSolicitudeCode}`,
            method: 'GET',
            data: null
        }
    }

    getDataForRegisterContinueProcessActivityApi(data) {
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenericoSimple: {
                    cod_flujo_fase_estado_actual: 31301,
                    cod_flujo_fase_estado_anterior: 31301,
                    nom_actividad: 'Retomar Solicitud Reimpresión'
                }
            }
        }
    }

    /* Redirecting step */
    redirectStep(status) {
        switch(status) {
            case 30501:
            case 30502:
            case 30601:
            case 30602:
            case 30603: return 3
            case 30701: return 4
            case 30801:
            case 30802: return 5
            case 30901:
            case 30902:
            case 30904:
            case 30905:
            case 30906:
            case 31001: return 6
            default: return 0
        }
    }

    /* Reintent Process Reprint Confirm Section */
    getReintentProcessForReprintConfirmSection(data) {
        const { status, message } = data || { }
        if (!status) return 0
        const next = !message
        switch (status) {
            case 30501: return 0
            case 30502:
            case 30601: return 1
            case 30602: return 1 + (next ? 2 : 0)
            case 30603: return 4 + Number(next)
            default: return 0
        }
    }

    /* Reintent Process Credit Card Activation Section */
    getReintentProcessForCreditCardActivationSection(data) {
        const { status, message } = data || { }
        if (!status) return 0
        const next = !message
        switch (status) {
            case 30901:
            case 30902:
            case 30903:
            case 30904: return 0
            case 30905: return next ? 2 : 0
            case 30906:
            case 31001: return 3
            default: return 0
        }
    }

    /* Step 1 */
    getDataForConsultClientApi(data) {
        return {
            url: `${ Config.URL_ODC }/Cliente/ConsultaCliente`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                cod_flujo_fase_estado_actual: 30201,
                cod_flujo_fase_estado_anterior: 30200,
                cod_tipo_solicitud: 340002,
                cod_tipo_solicitud_requerimiento: 350019
            }
        }
    }

    /* Step 2 */
    getConfigColumnsToCreditCardDetailSection() {
        return {
            xs: [
                { name: 'des_astrq_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' }
            ],
            sm: [
                { name: 'des_astrq_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nro_documento',  title: 'Nro Documento' },
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' }
            ],
            md: [
                { name: 'des_astrq_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nombre_completo', title: 'Cliente'},
                { name: 'des_tipo_documento',  title: 'Tipo Documento' },
                { name: 'des_nro_documento',  title: 'Nro Documento' },
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' }
            ],
            lg: [
                { name: 'des_astrq_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nombre_completo', title: 'Cliente'},
                { name: 'des_tipo_documento',  title: 'Tipo Documento' },
                { name: 'des_nro_documento',  title: 'Nro Documento' },
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' }
            ],
            xl: [
                { name: 'des_astrq_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nombre_completo', title: 'Cliente'},
                { name: 'des_tipo_documento',  title: 'Tipo Documento' },
                { name: 'des_nro_documento',  title: 'Nro Documento' },
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' }
            ],
            default: [
                { columnName: 'des_astrq_tarjeta_pmp', width: 160 },
                { columnName: 'num_cuenta_pmp', width: 160 },
                { columnName: 'des_tipo_relacion', width: 100 },
                { columnName: 'des_nombre_completo', width: 220 },
                { columnName: 'des_tipo_documento', width: 130 },
                { columnName: 'des_nro_documento', width: 150 },
                { columnName: 'des_nombre_producto', width: 200 },
                { columnName: 'des_bloqueo_tarjeta_pmp', width: 120 }
            ]
        }
    }

    processDataResponseOfConsultClient(data) {
        return {
            solicitudeCode: data.cod_solicitud,
            completeSolicitudeCode: data.cod_solicitud_completa,
            reqCode: data.cod_requerimiento,
            reprints: data.reimpresiones
        }
    }

    getDataForCreditCardDetailSectionCancelApi(data) {
        const { solicitudeCode, completeSolicitudeCode, reqCode } = data
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_requerimiento: reqCode,
                cod_solicitud: solicitudeCode,
                cod_solicitud_completa: completeSolicitudeCode,
                actividadGenericoSimple: {
                    cod_flujo_fase_estado_actual: 30303,
                    cod_flujo_fase_estado_anterior: 30301,
                    nom_actividad: 'Motivo: Cancelado'
                }
            }
        }
    }

    getDataForUploadFileApi(data) {
        let formData = new FormData()
        formData.append('tipo_doc', data.aux_tipo_documento)
        formData.append('nro_doc', data.des_nro_documento)
        formData.append('tipo_doc_letra', data.des_tipo_documento)
        formData.append('cod_solicitud', data.solicitudeCode)
        formData.append('cod_solicitud_completa', data.completeSolicitudeCode)
        formData.append('archivo', data.file)
        return {
            url: `${ Config.URL_ODC }/Requerimiento/SubirArchivo`,
            method: 'POST',
            data: formData
        }
    }

    getDataForRegisterReprintReasonApi(data) {
        return {
            url: `${ Config.URL_ODC }/Requerimiento/MotivoReimpresion`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                motivoReimpresion: {
                    cod_cliente_titular : data.cod_cliente_titular,
                    cod_cliente: data.cod_cliente,
                    cod_tipo_solicitud: 340002,
                    cod_tipo_solicitud_requerimiento: 350019,
                    num_tarjeta_antigua: data.num_tarjeta_pmp,
                    cod_solicitud_antigua: data.cod_solicitud,
                    cod_solicitud_completa_antigua: data.cod_solicitud_completa,
                    cod_motivo_reimpresion: data.reprintReasonId,
                    des_archivo_adjunto_reimpresion: data.documentUploaded,
                    cod_flujo_fase_estado_actual: 30302,
                    cod_flujo_fase_estado_anterior: 30201,
                    nom_actividad: 'Motivo: Aceptado',
                    cod_tipo_relacion: data.cod_tipo_relacion,
                    des_astrq_tarjeta_pmp: data.des_astrq_tarjeta_pmp,
                    des_encrp_tarjeta_pmp: data.des_encrp_tarjeta_pmp
                }
            }
        }
    }

    getDataForCreditCardDetailSection(data) {
        const { precargaCliente, precargaAdicional } = data.clientPreloaded
        const clientToProcess = Utils.comprobeObjectIsEmpty(precargaAdicional) ?  precargaCliente : precargaAdicional
        const homeAddress = (precargaCliente?.list_tt_ori_cliente_direccionEntity || []).find(address => address.cod_tipo_direccion === 260001) || { }
        const bin = (data.bins || []).find(bin => bin.cod_bin_pro === data.cod_bin_pro) || { }
        return {
            reqCode: data.reqCode,
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            prevSolicitudCode: data.cod_solicitud,
            prevCompleteSolicitudeCode: data.cod_solicitud_completa,
            documentTypeLetter: clientToProcess?.des_tipo_documento || data.des_tipo_documento || '',
            documentNumber: clientToProcess?.des_nro_documento || data.des_nro_documento || '',
            documentTypeAux: clientToProcess?.cod_tipo_documento || data.aux_tipo_documento || '',
            documentTypeInternalValue: data.documentTypeInternalValue,
            firstName: clientToProcess?.des_primer_nom || data.des_primer_nom || '',
            secondName: clientToProcess?.des_segundo_nom || data.des_segundo_nom || '',
            firstLastName: clientToProcess?.des_ape_paterno || data.des_ape_paterno || '',
            secondLastName: clientToProcess?.des_ape_materno || data.des_ape_materno || '',
            principalClientId: precargaCliente?.cod_cliente || data.cod_cliente_titular || 0,
            clientId: clientToProcess?.cod_cliente || data.cod_cliente || data.cod_cliente_titular || 0,
            nationality: clientToProcess?.des_nacionalidad || data.des_nacionalidad,
            relationTypeId: data.cod_tipo_relacion,
            typeSolicitudeOriginationCodeInitial: data.cod_tipo_solicitud_originacion_inicial,
            reprintReasonId: data.reprintReasonId,
            homeAddress: {
                departmentId: homeAddress.cod_ubi_departamento || '',
                department: homeAddress.des_ubi_departamento || '',
                districtId: homeAddress.cod_ubi_distrito || '',
                nameVia: homeAddress.des_nombre_via || '',
                number: homeAddress.des_nro || '',
                building: homeAddress.des_departamento || '',
                inside: homeAddress.des_interior || '',
                mz: homeAddress.des_manzana || '',
                lot: homeAddress.des_lote || '',
                nameZone: homeAddress.des_nombre_zona || '',
                reference: homeAddress.des_referencia_domicilio || ''
            },
            creditCard: {
                color: data.cod_valor_color,
                colorAux: data.cod_color,
                brand: data.cod_valor_marca,
                brandDescription: data.des_valor_marca,
                productId: bin.cod_producto,
                productDescription: data.des_nombre_producto,
                productType: data.des_tipo_producto,
                cardNumber: data.num_tarjeta_pmp,
                newCardNumber: '',
                newToken: '',
                token: data.token,
                clientNumber: data.num_cliente_pmp,
                accountNumber: data.num_cuenta_pmp,
                relationNumber: data.num_relacion_pmp,
                bin: data.cod_bin_pro
            }
        }
    }

    /* Step 3 */
    getDataForRegisterReprintDetailApi(data) {
        return {
            url: `${ Config.URL_ODC }/Requerimiento/MotivoReimpresion`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                motivoReimpresion: {
                    cod_cliente_titular : data.principalClientId,
                    cod_cliente: data.clientId,
                    cod_tipo_solicitud: 340002,
                    cod_tipo_solicitud_requerimiento: 350019,
                    cod_valor_color: data.creditCard.color,
                    cod_flujo_fase_estado_actual: 30402,
                    cod_flujo_fase_estado_anterior: 30401,
                    nom_actividad: 'Detalle: Aceptado'
                },
                flg_pregunta_1: data.additionalServices?.flagInternetConsume || '0',
                flg_pregunta_2: data.additionalServices?.flagForeignConsume || '0',
                flg_pregunta_3: data.additionalServices?.flagEffectiveDisposition || '0',
                flg_pregunta_4: data.additionalServices?.flagOverdraftTC || '0'
            }
        }
    }

    getDataForReprintDetailSectionCancelApi(data) {
        const { solicitudeCode, completeSolicitudeCode, reqCode } = data
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_requerimiento: reqCode,
                cod_solicitud: solicitudeCode,
                cod_solicitud_completa: completeSolicitudeCode,
                actividadGenericoSimple: {
                    cod_flujo_fase_estado_actual: 30403,
                    cod_flujo_fase_estado_anterior: 30401,
                    nom_actividad: 'Detalle: Cancelado'
                }
            }
        }
    }

    /* Step 4 */
    getDataForRegisterActivityInitOnReprintConfirmSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 30502,
                    cod_flujo_fase_estado_anterior: 30501,
                    nom_actividad: 'Confirmar: Aceptado'
                }
            }
        }
    }

    getDataForDecryptPreviousCardnumberApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            token: data.creditCard.token
        }
    }

    getDataForBlockTypeCreditCardApi(data) {
        const dataPmp = OriginationUtils.setDataBlockTypeCreditCardPMP({
            organization: Constants.General.organization,
            cod_flujo_fase_estado_actual: 30602,
            cod_flujo_fase_estado_anterior: 30601,
            nom_actividad: 'Reimpresión: PMP Bloquear TC',
            blockCode: Constants.BlockCodePmpTypes.blockTypeCreditCardDesactivated,
            process: {
                number: data.solicitudeCode,
                fullNumber: data.completeSolicitudeCode,
                numberPrev: data.prevSolicitudCode,
                fullNumberPrev: data.prevCompleteSolicitudeCode
            },
            client: {
                fatherId: data.principalClientId,
                id: data.clientId,
                documentType: data.documentTypeLetter,
                documentTypeAux: data.documentTypeAux,
                documentNumber: data.documentNumber
            },
            pmp: {
                cardNumber: data.cardNumberToDesactivate
            }
        })
        return dataPmp
    }

    getDataForBlockTypeCreditCardIntoMasterApi(_) {
        return { next: true }
    }

    getDataForCreateCreditCardApi(data) {
        const isHolderClient = data.relationTypeId === Constants.ClientRelationTypes.holderClientCode
        const dataPmp = OriginationUtils.setDataCreateCreditCard({
            organization: Constants.General.organization,
            cod_flujo_fase_estado_actual: 30603,
            cod_flujo_fase_estado_anterior: 30602,
            nom_actividad: 'Reimpresión: PMP Crear Tarjeta',
            blockCode: Constants.BlockCodePmpTypes.blockTypeCreditCardDisabled,
            process: {
                number: data.solicitudeCode,
                fullNumber: data.completeSolicitudeCode
            },
            client: {
                fatherId: data.principalClientId,
                id: data.clientId,
                documentType: data.documentTypeLetter,
                documentTypeAux: data.documentTypeAux,
                documentTypeInternalValue: data.documentTypeInternalValue,
                documentNumber: data.documentNumber,
                shortName: `${ data.firstLastName } ${ data.firstName }`,
                nationality: data.nationality,
                homeAddress: data.homeAddress
            },
            pmp: {
                accountNumber: data.creditCard.accountNumber,
                clientNumber: data.creditCard.clientNumber,
                relationshipNumber: data.creditCard.relationNumber
            },
            creditCard: {
                name: data.creditCard.productDescription
            }
        })
        return {
            ...dataPmp,
            flg_adicional: Number(!isHolderClient)
        }
    }

    getDataForEncryptCardnumberApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            pciStage: Constants.PciConfigurationStage.pciReprintStage
        }
    }

    getDataForCreateCreditCardIntoMasterApi(_) {
        return { next: true }
    }

    getDataForRegisterActivityEndOnReprintConfirmSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 30604,
                    cod_flujo_fase_estado_anterior: 30603,
                    nom_actividad: 'Reimpresión: Aceptado'
                }
            }
        }
    }

    getDataForReprintConfirmSectionCancelApi(data) {
        const { solicitudeCode, completeSolicitudeCode, reqCode } = data
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_requerimiento: reqCode,
                cod_solicitud: solicitudeCode,
                cod_solicitud_completa: completeSolicitudeCode,
                actividadGenericoSimple: {
                    cod_flujo_fase_estado_actual: 30503,
                    cod_flujo_fase_estado_anterior: 30501,
                    nom_actividad: 'Confirmar: Cancelado'
                }
            }
        }
    }

    /* Step 5 */
    getDataForRegisterActivityOnReprintSummarySectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 30702,
                    cod_flujo_fase_estado_anterior: 30701,
                    nom_actividad: 'Resumen: Aceptado'
                }
            }
        }
    }

    /* Step 6 */
    getDataForRegisterActivityOnCreditCardEmbossSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 30803,
                    cod_flujo_fase_estado_anterior: 30801,
                    nom_actividad: 'Embozado: Aceptado'
                }
            }
        }
    }

    getDataForPendingEmbossCreditCardApi(data) {
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadPendienteEmboce`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadPendienteEmboce: {
                    cod_flujo_fase_estado_actual: 30802,
                    cod_flujo_fase_estado_anterior: 30801,
                    nom_actividad: 'Embozado: Pendiente',
                    cod_motivo_emboce: data.embossReasonId,
                    des_motivo_comentario_emboce: data.description
                }
            }
        }
    }

    /* Step 7 */
    getDataForContingencyProcessApi(data) {
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenericoSimple: {
                    cod_flujo_fase_estado_actual: 30903,
                    cod_flujo_fase_estado_anterior: 30902,
                    nom_actividad: 'Activación Tarjeta: Pendiente'
                }
            }
        }
    }

    getDataForDecryptCardnumberApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            token: data.creditCard.newToken
        }
    }

    getDataForActivateCreditCardApi(data) {
        const dataPmp = OriginationUtils.setDataBlockTypeCreditCardPMP({
            organization: Constants.General.organization,
            cod_flujo_fase_estado_actual: 30905,
            cod_flujo_fase_estado_anterior: 30904,
            nom_actividad: 'Activación Tarjeta: PMP Activar TC',
            blockCode: data.comodin,
            process: {
                number: data.solicitudeCode,
                fullNumber: data.completeSolicitudeCode,
                numberPrev: 0,
                fullNumberPrev: ''
            },
            client: {
                fatherId: data.principalClientId,
                id: data.clientId,
                documentType: data.documentTypeLetter,
                documentTypeAux: data.documentTypeAux,
                documentNumber: data.documentNumber
            },
            pmp: {
                cardNumber: data.cardNumberToActivate
            }
        })
        return dataPmp
    }

    getDataForActivateCreditCardIntoMasterApi(_) {
        return { next: true }
    }

    getDataForRegisterActivityInitOnCreditCardActivationSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 30906,
                    cod_flujo_fase_estado_anterior: 30905,
                    nom_actividad: 'Activación Tarjeta: Aceptado'
                }
            }
        }
    }

    getDataForRegisterActivityEndOnCreditCardActivationSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/Requerimiento/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 31002,
                    cod_flujo_fase_estado_anterior: 31001,
                    nom_actividad: 'Entrega Tarjeta: Aceptado'
                }
            }
        }
    }
}

export default Reprint