// Constants
import * as Constants from '../Constants'

// Reprint Types
import Reprint from './Reprint'
import ReprintExternal from './ReprintExternal'

class ReprintFactory {
    constructor(type) {
        this.type = type
    }

    createReprint() {
        switch (this.type) {
            case Constants.ReprintFactoryTypes.reprint:
                return new Reprint()
            case Constants.ReprintFactoryTypes.reprintExternal:
                return new ReprintExternal()
            default:
                return new Reprint()
        }
    }
}

export default ReprintFactory