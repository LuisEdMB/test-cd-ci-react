// Config
import * as Config from '../../actions/config'

// Utils
import * as Utils from '../Utils'
import * as OriginationUtils from '../origination/Origination'
import * as Constants from '../Constants'
import * as MasterOrigination from '../origination/MasterOrigination'

class ReprintExternal {
    constructor() {
        this.showInfoHolderClientForCreditCardDetailSection = true
        this.showAdditionalServicesForAllReprintReasons = true
        this.isOptionalTokenForDeactivatePreviousCreditCard = false
        this.isOptionalTokenForActivateCreditCard = false
    }

    /* Continue Process */
    getDataForContinueProcessApi(completeSolicitudeCode) {
        return {
            url: `${ Config.URL_ODC }/Originacion/SolicitudRetomar?cod_solicitud_completa=${completeSolicitudeCode}`,
            method: 'GET',
            data: null
        }
    }

    getDataForRegisterContinueProcessActivityApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenericoSimple: {
                    cod_flujo_fase_estado_actual: 31301,
                    cod_flujo_fase_estado_anterior: 31301,
                    nom_actividad: 'Retomar Solicitud Reimpresión'
                }
            }
        }
    }

    /* Redirecting step */
    redirectStep(status) {
        switch(status) {
            case 30501:
            case 30502:
            case 30601:
            case 30602:
            case 30603: return 3
            case 30701: return 4
            case 30801:
            case 30802: return 5
            case 30901:
            case 30902:
            case 30904:
            case 30905:
            case 30906:
            case 31001: return 6
            default: return 0
        }
    }

    /* Reintent Process Reprint Confirm Section */
    getReintentProcessForReprintConfirmSection(data) {
        const { status, message } = data || { }
        if (!status) return 0
        const next = !message
        switch (status) {
            case 30501: return 0
            case 30502:
            case 30601: return 1
            case 30602: return 1 + (next ? 2 : 0)
            case 30603: return 4 + Number(next)
            default: return 0
        }
    }

    /* Reintent Process Credit Card Activation Section */
    getReintentProcessForCreditCardActivationSection(data) {
        const { status, message } = data || { }
        if (!status) return 0
        const next = !message
        switch (status) {
            case 30901:
            case 30902:
            case 30903:
            case 30904: return 0
            case 30905: return next ? 2 : 0
            case 30906:
            case 31001: return 3
            default: return 0
        }
    }

    /* Step 1 */
    getDataForConsultClientApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/ConsultaCliente`,
            method: 'POST',
            data: {
                des_nro_documento: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                cod_tipo_documento: data.documentTypeAux,
                cod_flujo_fase_estado_actual: 30201,
                cod_flujo_fase_estado_anterior: 30200,
                nom_actividad: 'Validacion: En Proceso',
                cod_tipo_solicitud: 340002,
                cod_tipo_solicitud_requerimiento: 350019
            }
        }
    }

    /* Step 2 */
    getConfigColumnsToCreditCardDetailSection() {
        return {
            xs: [
                { name: 'num_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' },
                { name: 'fechabloqueo',  title: 'Fecha Bloqueo' }
            ],
            sm: [
                { name: 'num_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' },
                { name: 'fechabloqueo',  title: 'Fecha Bloqueo' }
            ],
            md: [
                { name: 'num_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nombre_completo', title: 'Cliente'},
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' },
                { name: 'fechabloqueo',  title: 'Fecha Bloqueo' }
            ],
            lg: [
                { name: 'num_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nombre_completo', title: 'Cliente'},
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' },
                { name: 'fechabloqueo',  title: 'Fecha Bloqueo' }
            ],
            xl: [
                { name: 'num_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nombre_completo', title: 'Cliente'},
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' },
                { name: 'fechabloqueo',  title: 'Fecha Bloqueo' }
            ],
            default: [
                { columnName: 'num_tarjeta_pmp', width: 160 },
                { columnName: 'num_cuenta_pmp', width: 160 },
                { columnName: 'des_tipo_relacion', width: 100 },
                { columnName: 'des_nombre_completo', width: 220 },
                { columnName: 'des_nombre_producto', width: 200 },
                { columnName: 'des_bloqueo_tarjeta_pmp', width: 120 },
                { columnName: 'fechabloqueo',  width: 140 }
            ]
        }
    }

    processDataResponseOfConsultClient(data) {
        return {
            solicitudeCode: data.cod_solicitud,
            completeSolicitudeCode: data.cod_solicitud_completa,
            reqCode: data.cod_requerimiento,
            cellphoneHolder: data.des_celular_titular,
            emailHolder: data.des_correo_titular,
            reprints: (data.reimpresiones || []).map((item, index) => {
                const reprint = {
                    cod_solicitud: index + 1,
                    num_cuenta_pmp: item.num_cuenta,
                    des_tipo_relacion: item.des_relacion,
                    des_nombre_completo: item.des_nombre_cliente,
                    des_primer_nombre: item.des_primer_nombre,
                    des_primer_apellido: item.des_primer_apellido,
                    des_tipo_documento: item.tipo_documento,
                    des_nro_documento: item.des_nro_documento,
                    des_nombre_producto: item.producto,
                    des_bloqueo_tarjeta_pmp: item.bloqueo,
                    cod_tipo_relacion: item.cod_tipo_cliente,
                    num_tarjeta_pmp: item.num_tarjeta,
                    token: item.token,
                    fecha_pago: item.fecha_pago,
                    num_cliente_pmp: item.customer,
                    num_relacion_pmp: item.relationship,
                    fechabloqueo: item.fechabloqueo?.split('T')[0] || ''
                }
                return reprint
            })
        }
    }

    getDataForCreditCardDetailSectionCancelApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenericoSimple: {
                    cod_flujo_fase_estado_actual: 30303,
                    cod_flujo_fase_estado_anterior: 30301,
                    nom_actividad: 'Motivo: Cancelado'
                }
            }
        }
    }

    getDataForUploadFileApi(data) {
        let formData = new FormData()
        formData.append('cod_solicitud', data.solicitudeCode)
        formData.append('cod_solicitud_completa', data.completeSolicitudeCode)
        formData.append('archivo', data.file)
        return {
            url: `${ Config.URL_ODC }/Requerimiento/SubirArchivo`,
            method: 'POST',
            data: formData
        }
    }

    getDataForRegisterReprintReasonApi(data) {
        const { precargaCliente, precargaAdicional } = data.clientPreloaded
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/MotivoReimpresion`,
            method: 'POST',
            data: {
                cod_cliente: precargaAdicional?.cod_cliente || precargaCliente?.cod_cliente,
                cod_tipo_relacion: data.cod_tipo_relacion,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                des_nro_documento: data.des_nro_documento,
                num_cuenta: data.num_cuenta_pmp,
                token: data.token,
                fecha_pago: data.fecha_pago,
                cod_motivo_reimpresion: data.reprintReasonId,
                des_archivo_adjunto_reimpresion: data.documentUploaded
            }
        }
    }

    getDataForCreditCardDetailSection(data) {
        const { precargaCliente, precargaAdicional } = data.clientPreloaded
        const clientToProcess = Utils.comprobeObjectIsEmpty(precargaAdicional) ?  precargaCliente : precargaAdicional
        const homeAddress = (precargaCliente?.list_tt_ori_cliente_direccionEntity || []).find(address => address.cod_tipo_direccion === 260001) || { }
        const isHolderClient = Constants.ClientRelationTypes.holderClientCode === data.cod_tipo_relacion
        const bin = (data.bins || []).find(bin => bin.cod_producto === data.responseOfRegisterReprintReason?.cod_producto) || { }
        return {
            reqCode: data.reqCode,
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            documentTypeLetter: clientToProcess?.des_tipo_documento || '',
            documentNumber: clientToProcess?.des_nro_documento || '',
            documentTypeAux: clientToProcess?.cod_tipo_documento || '',
            documentTypeInternalValue: data.documentTypeInternalValue || '',
            firstName: (isHolderClient ? clientToProcess?.des_primer_nom : data.des_primer_nombre) || '',
            secondName: isHolderClient ? clientToProcess?.des_segundo_nom : '',
            firstLastName: (isHolderClient ? clientToProcess?.des_ape_paterno : data.des_primer_apellido) || '',
            secondLastName: isHolderClient ? clientToProcess?.des_ape_materno : '',
            principalClientId: precargaCliente?.cod_cliente || 0,
            clientId: clientToProcess?.cod_cliente || 0,
            nationality: clientToProcess?.des_nacionalidad || '',
            relationTypeId: data.cod_tipo_relacion,
            reprintReasonId: data.reprintReasonId,
            homeAddress: {
                departmentId: homeAddress.cod_ubi_departamento || '',
                department: homeAddress.des_ubi_departamento || '',
                districtId: homeAddress.cod_ubi_distrito || '',
                nameVia: homeAddress.des_nombre_via || '',
                number: homeAddress.des_nro || '',
                building: homeAddress.des_departamento || '',
                inside: homeAddress.des_interior || '',
                mz: homeAddress.des_manzana || '',
                lot: homeAddress.des_lote || '',
                nameZone: homeAddress.des_nombre_zona || '',
                reference: homeAddress.des_referencia_domicilio || ''
            },
            creditCard: {
                color: 0,
                colorAux: bin.cod_color,
                brand: Number(data.responseOfRegisterReprintReason?.cod_valor_marca || '0'),
                brandDescription: bin.des_marca_pro || '',
                productId: bin.cod_producto,
                productDescription: bin.des_nom_prod || '',
                productType: bin.des_aux_pro,
                cardNumber: data.num_tarjeta_pmp,
                newCardNumber: '',
                newToken: '',
                token: data.token,
                clientNumber: data.num_cliente_pmp,
                accountNumber: data.num_cuenta_pmp,
                relationNumber: data.num_relacion_pmp,
                bin: bin?.cod_bin_pro || ''
            }
        }
    }

    /* Step 3 */
    getDataForRegisterReprintDetailApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/ConfirmarMotivoReimpresion`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                cod_valor_color: data.creditCard.color,
                flg_pregunta_1: data.additionalServices.flagInternetConsume,
                flg_pregunta_2: data.additionalServices.flagForeignConsume,
                flg_pregunta_3: data.additionalServices.flagEffectiveDisposition,
                flg_pregunta_4: data.additionalServices.flagOverdraftTC
            }
        }
    }

    getDataForReprintDetailSectionCancelApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenericoSimple: {
                    cod_flujo_fase_estado_actual: 30403,
                    cod_flujo_fase_estado_anterior: 30401,
                    nom_actividad: 'Detalle: Cancelado'
                }
            }
        }
    }

    /* Step 4 */
    getDataForRegisterActivityInitOnReprintConfirmSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 30502,
                    cod_flujo_fase_estado_anterior: 30501,
                    nom_actividad: 'Confirmar: Aceptado'
                }
            }
        }
    }

    getDataForDecryptPreviousCardnumberApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            token: data.creditCard.token
        }
    }

    getDataForBlockTypeCreditCardApi(data) {
        const dataPmp = OriginationUtils.setDataBlockTypeCreditCardPMP({
            organization: Constants.General.organization,
            cod_flujo_fase_estado_actual: 30602,
            cod_flujo_fase_estado_anterior: 30601,
            nom_actividad: 'Reimpresión: PMP Bloquear TC',
            blockCode: Constants.BlockCodePmpTypes.blockTypeCreditCardDesactivated,
            process: {
                number: data.solicitudeCode,
                fullNumber: data.completeSolicitudeCode
            },
            client: {
                fatherId: data.principalClientId,
                id: data.clientId,
                documentType: data.documentTypeLetter,
                documentTypeAux: data.documentTypeAux,
                documentNumber: data.documentNumber
            },
            pmp: {
                cardNumber: data.cardNumberToDesactivate
            }
        })
        return {
            ...dataPmp,
            token: data.creditCard.token,
            callPmpBlockingReprint: true
        }
    }

    getDataForBlockTypeCreditCardIntoMasterApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            accountNumber: data.creditCard.accountNumber,
            token: data.creditCard.token,
            blockingCode: Constants.BlockCodePmpTypes.blockTypeCreditCardDesactivated,
            activityName: MasterOrigination.updateBlockingCodeCreditCardActivity.activityName,
            phaseCode: MasterOrigination.updateBlockingCodeCreditCardActivity.phaseCode
        }
    }

    getDataForCreateCreditCardApi(data) {
        const isHolderClient = data.relationTypeId === Constants.ClientRelationTypes.holderClientCode
        const dataPmp = OriginationUtils.setDataCreateCreditCard({
            organization: Constants.General.organization,
            cod_flujo_fase_estado_actual: 30603,
            cod_flujo_fase_estado_anterior: 30602,
            nom_actividad: 'Reimpresión: PMP Crear Tarjeta',
            blockCode: Constants.BlockCodePmpTypes.blockTypeCreditCardDisabled,
            process: {
                number: data.solicitudeCode,
                fullNumber: data.completeSolicitudeCode
            },
            client: {
                fatherId: data.principalClientId,
                id: !isHolderClient ? 0 : data.clientId,
                documentType: data.documentTypeLetter,
                documentTypeAux: data.documentTypeAux,
                documentTypeInternalValue: data.documentTypeInternalValue,
                documentNumber: data.documentNumber,
                shortName: `${ data.firstLastName } ${ data.firstName }`,
                nationality: data.nationality,
                homeAddress: data.homeAddress
            },
            pmp: {
                accountNumber: data.creditCard.accountNumber,
                clientNumber: data.creditCard.clientNumber,
                relationshipNumber: data.creditCard.relationNumber
            },
            creditCard: {
                name: data.creditCard.productDescription
            }
        })
        return {
            ...dataPmp,
            cod_cliente: isHolderClient ? 0 : data.principalClientId,
            flg_adicional: Number(!isHolderClient)
        }
    }

    getDataForEncryptCardnumberApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            pciStage: Constants.PciConfigurationStage.pciReprintStage
        }
    }

    getDataForCreateCreditCardIntoMasterApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            activityName: MasterOrigination.registerCreditCardActivity.activityName,
            phaseCode: MasterOrigination.registerCreditCardActivity.phaseCode
        }
    }

    getDataForRegisterActivityEndOnReprintConfirmSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 30604,
                    cod_flujo_fase_estado_anterior: 30603,
                    nom_actividad: 'Reimpresión: Aceptado'
                }
            }
        }
    }

    getDataForReprintConfirmSectionCancelApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenericoSimple: {
                    cod_flujo_fase_estado_actual: 30503,
                    cod_flujo_fase_estado_anterior: 30501,
                    nom_actividad: 'Confirmar: Cancelado'
                }
            }
        }
    }

    /* Step 5 */
    getDataForRegisterActivityOnReprintSummarySectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 30702,
                    cod_flujo_fase_estado_anterior: 30701,
                    nom_actividad: 'Resumen: Aceptado'
                }
            }
        }
    }

    /* Step 6 */
    getDataForRegisterActivityOnCreditCardEmbossSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 30803,
                    cod_flujo_fase_estado_anterior: 30801,
                    nom_actividad: 'Embozado: Aceptado'
                }
            }
        }
    }

    getDataForPendingEmbossCreditCardApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadPendienteEmboceExt`,
            method: 'POST',
            data: {
                cod_requerimiento: data.reqCode,
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadPendienteEmboce: {
                    cod_flujo_fase_estado_actual: 30802,
                    cod_flujo_fase_estado_anterior: 30801,
                    nom_actividad: 'Embozado: Pendiente',
                    cod_motivo_emboce: data.embossReasonId,
                    des_motivo_comentario_emboce: data.description
                }
            }
        }
    }

    /* Step 7 */
    getDataForContingencyProcessApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenericoSimple: {
                    cod_flujo_fase_estado_actual: 30903,
                    cod_flujo_fase_estado_anterior: 30902,
                    nom_actividad: 'Activación Tarjeta: Pendiente'
                }
            }
        }
    }

    getDataForDecryptCardnumberApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            token: data.creditCard.newToken
        }
    }

    getDataForActivateCreditCardApi(data) {
        const dataPmp = OriginationUtils.setDataBlockTypeCreditCardPMP({
            organization: Constants.General.organization,
            cod_flujo_fase_estado_actual: 30905,
            cod_flujo_fase_estado_anterior: 30904,
            nom_actividad: 'Activación Tarjeta: PMP Activar TC',
            blockCode: data.comodin,
            process: {
                number: data.solicitudeCode,
                fullNumber: data.completeSolicitudeCode,
                numberPrev: 0,
                fullNumberPrev: ''
            },
            client: {
                fatherId: data.principalClientId,
                id: data.clientId,
                documentType: data.documentTypeLetter,
                documentTypeAux: data.documentTypeAux,
                documentNumber: data.documentNumber
            },
            pmp: {
                cardNumber: data.cardNumberToActivate
            }
        })
        return {
            ...dataPmp,
            callPmpActivateReprint: true
        }
    }

    getDataForActivateCreditCardIntoMasterApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            accountNumber: data.creditCard.accountNumber,
            token: data.creditCard.newToken,
            blockingCode: data.comodin,
            activityName: MasterOrigination.updateBlockingCodeCreditCardActivity.activityName,
            phaseCod: MasterOrigination.updateBlockingCodeCreditCardActivity.phaseCode
        }
    }

    getDataForRegisterActivityInitOnCreditCardActivationSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 30906,
                    cod_flujo_fase_estado_anterior: 30905,
                    nom_actividad: 'Activación Tarjeta: Aceptado'
                }
            }
        }
    }

    getDataForRegisterActivityEndOnCreditCardActivationSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/RequerimientoExt/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                actividadGenerico: {
                    cod_flujo_fase_estado_actual: 31002,
                    cod_flujo_fase_estado_anterior: 31001,
                    nom_actividad: 'Entrega Tarjeta: Aceptado'
                }
            }
        }
    }
}

export default ReprintExternal