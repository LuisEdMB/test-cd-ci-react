// Constants
import * as Constants from '../Constants'

// Additional Types
import Additional from './Additional'
import AdditionalExternal from './AdditionalExternal'

class AdditionalFactory {
    constructor(type) {
        this.type = type
    }

    createAdditional() {
        switch (this.type) {
            case Constants.AdditionalFactoryTypes.additional:
                return new Additional()
            case Constants.AdditionalFactoryTypes.additionalExternal:
                return new AdditionalExternal()
            default:
                return new Additional()
        }
    }
}

export default AdditionalFactory