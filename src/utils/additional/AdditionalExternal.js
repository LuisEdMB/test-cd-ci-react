// Config
import * as Config from '../../actions/config'

// Utils
import * as Utils from '../../utils/Utils'
import * as OriginationUtils from '../../utils/origination/Origination'
import * as MasterOrigination from '../../utils/origination/MasterOrigination'

// Constants
import * as Constants from '../../utils/Constants'

class AdditionalExternal {
    constructor(){
        this.showAdditionalDetailTableIntoHolderClientInformationSection = true
        this.isOptionalTokenForActivateCreditCard = false
    }

    /* Continue Process */
    getDataForContinueProcessApi(completeSolicitudeCode) {
        return {
            url: `${ Config.URL_ODC }/Originacion/SolicitudRetomar?cod_solicitud_completa=${completeSolicitudeCode}`,
            method: 'GET',
            data: null
        }
    }

    getDataForRegisterContinueProcessActivityApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                cod_flujo_fase_estado_actual: 41101,
                cod_flujo_fase_estado_anterior: 41101,
                nom_actividad: 'Retomar Solicitud Adicional',
            }
        }
    }

    /* Redirecting step */
    redirectStep(status) {
        switch(status) {
            case 40402: return 4
            case 40501: return 5
            case 40601:
            case 40602: return 6
            case 40701:
            case 40702:
            case 40704:
            case 40705: 
            case 40706:
            case 40801: return 7
            default: return 0
        }
    }

    /* Reintent Process Credit Card Confirm Section */
    getReintentProcessForCreditCardConfirmSection(data) {
        const { status, message } = data || { }
        if (!status) return 0
        const next = !message
        switch (status) {
            case 40402: return 0 + Number(next)
            default: return 0
        }
    }

    /* Reintent Process Credit Card Activation Section */
    getReintentProcessForCreditCardActivationSection(data) {
        const { status, message } = data || { }
        if (!status) return 0
        const next = !message
        switch (status) {
            case 40701:
            case 40702:
            case 40703:
            case 40704: return 0
            case 40705: return next ? 2 : 0
            case 40706:
            case 40801: return 3
            default: return 0
        }
    }

    /* Step 1 */
    getDataForConsultClientApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/ConsultaCliente`,
            method: 'POST',
            data: {
                cod_tipo_documento: data.documentTypeAux,
                tipo_doc_letra: data.documentTypeLetter,
                des_nro_documento: data.documentNumber
            }
        }
    }

    /* Step 2 */
    getColumnsAdditionalDetailTableForHolderClientInformationSection() {
        return {
            xs: [
                { name: 'num_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' }
            ],
            sm: [
                { name: 'num_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' }
            ],
            md: [
                { name: 'num_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nombre_completo', title: 'Cliente'},
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' }
            ],
            lg: [
                { name: 'num_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nombre_completo', title: 'Cliente'},
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' }
            ],
            xl: [
                { name: 'num_tarjeta_pmp',  title: 'Nro. Tarjeta' },
                { name: 'num_cuenta_pmp',  title: 'Nro. Cuenta' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_nombre_completo', title: 'Cliente'},
                { name: 'des_nombre_producto',  title: 'Producto' },
                { name: 'des_bloqueo_tarjeta_pmp',  title: 'Estado' }
            ],
            default: [
                { columnName: 'num_tarjeta_pmp', width: 160 },
                { columnName: 'num_cuenta_pmp', width: 160 },
                { columnName: 'des_tipo_relacion', width: 100 },
                { columnName: 'des_nombre_completo', width: 220 },
                { columnName: 'des_nombre_producto', width: 200 },
                { columnName: 'des_bloqueo_tarjeta_pmp', width: 120 }
            ]
        }
    }

    processDataResponseOfConsultClient(data) {
        const bin = (data.bins || []).find(bin => bin.cod_producto === data.cod_producto_titular) || { }
        return {
            solicitudeCode: data.cod_solicitud,
            completeSolicitudeCode: data.cod_solicitud_completa,
            paymentDateId: data.cod_fecha_pago_titular,
            creditCard: {
                colorAux: bin.cod_color || '',
                brand: data.cod_valor_marca_titular,
                brandDescription: bin.des_marca_pro || '',
                productId: data.cod_producto_titular,
                product: bin.des_aux_pro || '',
                productDescription: bin.des_nom_prod || '',
                productType: bin.des_aux_pro || '',
                accountNumber: data.num_cuenta || '',
                clientNumber: data.customer || '',
                relationshipNumber: data.relationship || '',
                bin: bin.cod_bin_pro || 0
            },
            additionalNumber: data.num_adicionales || 0,
            additionals: (data.adicionales || []).map((item, index) => ({
                cod_solicitud: index + 1,
                num_cuenta_pmp: item.num_cuenta,
                des_tipo_relacion: item.des_relacion,
                des_nombre_completo: item.des_nombre_cliente,
                des_tipo_documento: item.tipo_documento,
                des_nro_documento: item.des_nro_documento,
                des_nombre_producto: item.producto,
                des_bloqueo_tarjeta_pmp: item.bloqueo,
                num_tarjeta_pmp: item.num_tarjeta,
                token: item.token
            })),
            codeSolicitudeOriginationType: Constants.OriginationType.Preevaluated
        }
    }

    /* Step 3 */
    getDataForValidateAdditionalClientAlreadyBelongsHolderClientApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/ValidarClienteAdicional`,
            method: 'POST',
            data: {
                cod_solicitud: data.additionalClient.solicitudeCode,
                cod_solicitud_completa: data.additionalClient.completeSolicitudeCode,
                cod_tipo_documento_adicional: data.additionalClient.documentTypeAux,
                des_nro_documento_adicional: data.additionalClient.documentNumber,
                des_nro_documento_titular: data.principalClient.documentNumber
            }
        }
    }

    getDataForAdditionalClientPreloadApi(data) {
        return {
            solicitudeCode: data.additionalClient.solicitudeCode,
            completeSolicitudeCode: data.additionalClient.completeSolicitudeCode,
            additionalDocumentTypeAux: data.additionalClient.documentTypeAux,
            additionalDocumentNumber: data.additionalClient.documentNumber,
            holderDocumentTypeAux: data.principalClient.documentTypeAux,
            holderDocumentNumber: data.principalClient.documentNumber,
            clientTypeCode: Constants.ClientTypes.additionalClientCode,
            masterStageCode: Constants.MasterConfigurationStage.masterAdditionalStage
        }
    }

    processDataResponseOfPreloadAdditionalClient(data) {
        const additional = data?.precargaAdicional || { }
        const holder = data?.precargaCliente || { }
        const homeAddress = (holder.list_tt_ori_cliente_direccionEntity || []).find(address => address.cod_tipo_direccion === 260001) || { }
        return {
            clientId: additional.cod_cliente || 0,
            holderClientId: holder.cod_cliente || 0,
            documentNumber: additional.des_nro_documento || '',
            firstName: additional.des_primer_nom || '',
            secondName: additional.des_segundo_nom || '',
            firstLastName: additional.des_ape_paterno || '',
            secondLastName: additional.des_ape_materno || '',
            birthday: additional.fec_nacimiento?.split('T')[0] || '',
            genderId: additional.cod_genero || 0,
            gender: additional.des_genero || '',
            nationalityId: additional.cod_nacionalidad || 0,
            nationality: additional.des_nacionalidad || '',
            familyRelationshipId: additional.cod_vinculo_adicional || 0,
            familyRelationship: additional.des_vinculo_adicional || '',
            cellphone: additional.des_telef_celular || '',
            holderEmail: holder.des_correo || '',
            homeAddress: {
                departmentId: homeAddress.cod_ubi_departamento || '',
                department: homeAddress.des_ubi_departamento || '',
                districtId: homeAddress.cod_ubi_distrito || '',
                nameVia: homeAddress.des_nombre_via || '',
                viaInternalValue: homeAddress.des_via_valor_interno || '',
                number: homeAddress.des_nro || '',
                building: homeAddress.des_departamento || '',
                inside: homeAddress.des_interior || '',
                mz: homeAddress.des_manzana || '',
                lot: homeAddress.des_lote || '',
                nameZone: homeAddress.des_zona || '',
                zoneInternalValue: homeAddress.des_zona_valor_interno || '',
                reference: homeAddress.des_referencia_domicilio || ''
            }
        }
    }

    getDataForRegisterAdditionalClientIntoMasterApi(data) {
        const { additionalClient } = data
        return {
            solicitudeCode: additionalClient.solicitudeCode,
            completeSolicitudeCode: additionalClient.completeSolicitudeCode,
            activityName: MasterOrigination.registerUpdateClientActivity.activityName,
            phaseCode: MasterOrigination.registerUpdateClientActivity.phaseCode,
            masterStageCode: Constants.MasterConfigurationStage.masterAdditionalStage
        }
    }

    getDataForRegisterAdditionalClientApi(data) {
        const { principalClient, additionalClient } = data
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/RegistroAdicional`,
            method: 'POST',
            data: {
                cod_solicitud: additionalClient.solicitudeCode,
                cod_solicitud_completa: additionalClient.completeSolicitudeCode,
                cod_resp_promotor: additionalClient.promoterCode,
                titular: {
                    cod_cliente_titular: principalClient.clientId,
                    des_correo_titular: principalClient.email,
                    cod_producto_titular: principalClient.creditCard.productId,
                    cod_valor_marca_titular: principalClient.creditCard.brand,
                    cod_fecha_pago_titular: principalClient.paymentDateId
                },
                adicional: {
                    cod_cliente_adicional: additionalClient.clientId,
                    cod_tipo_documento: additionalClient.documentTypeAux,
                    des_nro_documento: additionalClient.documentNumber,
                    des_primer_nom: additionalClient.firstName,
                    des_segundo_nom: additionalClient.secondName,
                    des_ape_paterno: additionalClient.firstLastName,
                    des_ape_materno: additionalClient.secondLastName,
                    fec_nacimiento: additionalClient.birthday,
                    cod_genero: additionalClient.genderId,
                    cod_nacionalidad: additionalClient.nationalityId,
                    cod_pais_residencia: Constants.CountryResidence.Peru,
                    cod_vinculo: additionalClient.familyRelationshipId,
                    des_telef_celular: additionalClient.cellphone
                },
                servicios_adicionales: {
                    flg_consumo_internet: additionalClient.flagInternetConsume,
                    flg_consumo_extranjero: additionalClient.flagForeignConsume,
                    flg_retirar_efectivo: additionalClient.flagEffectiveDisposition,
                    flg_sobregirar_credito: additionalClient.flagOverdraftCreditCard
                }
            }
        }
    }

    processDataResponseOfRegisterAdditionalClient(data) {
        return {
            clientId: data?.cod_cliente_adicional || 0
        }
    }

    /* Step 4 */
    getDataForRegisterAdditionalDetailApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/RegistrarColorTarjetaAdicional`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                cod_valor_color: data.creditCard.color
            }
        }
    }

    getDataForAdditionalDetailSectionCancelApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                cod_flujo_fase_estado_actual: 40303,
                cod_flujo_fase_estado_anterior: 40301,
                nom_actividad: 'Detalle Adicional: Cancelado',
            }
        }
    }

    /* Step 5 */
    getDataForCreateCreditCardApi(data) {
        const { principalClient, additionalClient } = data
        const dataPmp = OriginationUtils.setDataCreateCreditCard({
            organization: Constants.General.organization,
            cod_flujo_fase_estado_actual: 40402,
            cod_flujo_fase_estado_anterior: 40401,
            nom_actividad: 'Confirmar Adicional: PMP Crear Tarjeta',
            blockCode: Constants.BlockCodePmpTypes.blockTypeCreditCardDisabled,
            process: {
                number: additionalClient.solicitudeCode,
                fullNumber: additionalClient.completeSolicitudeCode
            },
            client: {
                fatherId: principalClient.clientId,
                id: additionalClient.clientId,
                documentType: additionalClient.documentTypeLetter,
                documentTypeAux: additionalClient.documentTypeAux,
                documentTypeInternalValue: additionalClient.documentTypeInternalValue,
                documentNumber: additionalClient.documentNumber,
                shortName: `${ additionalClient.firstLastName } ${ additionalClient.firstName }`,
                nationality: additionalClient.nationality,
                homeAddress: principalClient.homeAddress
            },
            pmp: {
                accountNumber: additionalClient.creditCard.accountNumber,
                clientNumber: additionalClient.creditCard.clientNumber,
                relationshipNumber: additionalClient.creditCard.relationshipNumber
            },
            creditCard: {
                name: additionalClient.creditCard.productDescription
            }
        })
        return dataPmp
    }

    getDataForEncryptCardnumberApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            pciStage: Constants.PciConfigurationStage.pciAdditionalStage
        }
    }

    getDataForCreateCreditCardIntoMasterApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            activityName: MasterOrigination.registerCreditCardActivity.activityName,
            phaseCode: MasterOrigination.registerCreditCardActivity.phaseCode
        }
    }

    getDataForRegisterActivityOnAdditionalConfirmSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                cod_flujo_fase_estado_actual: 40403,
                cod_flujo_fase_estado_anterior: 40402,
                nom_actividad: 'Confirmar Adicional: Aceptado'
            }

        }
    }

    getDataForAdditionalConfirmSectionCancelApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                cod_flujo_fase_estado_actual: 40403,
                cod_flujo_fase_estado_anterior: 40401,
                nom_actividad: 'Confirmar Adicional: Cancelado',
            }
        }
    }

    /* Step 6 */
    getDataForRegisterActivityOnAdditionalSummarySectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                cod_flujo_fase_estado_actual: 40502,
                cod_flujo_fase_estado_anterior: 40501,
                nom_actividad: 'Resumen: Aceptado'
            }
        }
    }

    /* Step 7 */
    getDataForRegisterActivityOnAdditionalEmbossSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                cod_flujo_fase_estado_actual: 40603,
                cod_flujo_fase_estado_anterior: 40601,
                nom_actividad: 'Embozado: Aceptado'
            }
        }
    }

    getDataForPendingEmbossCreditCardApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadPendienteEmboce`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                actividadPendienteEmboceModelRequest: {
                    cod_solicitud: data.solicitudeCode,
                    cod_solicitud_completa: data.completeSolicitudeCode,
                    cod_flujo_fase_estado_actual: 40602,
                    cod_flujo_fase_estado_anterior: 40601,
                    nom_actividad: 'Embozado: Pendiente',
                    cod_motivo_emboce: data.embossReasonId,
                    des_motivo_comentario_emboce: data.description,
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }

    /* Step 8 */
    getDataForDecryptCreditCardNumber(data) {
        const { solicitudeCode, completeSolicitudeCode, creditCard } = data
        return {
            solicitudeCode: solicitudeCode,
            completeSolicitudeCode: completeSolicitudeCode,
            token: creditCard.token
        }
    }

    getDataForActivateCreditCardApi(data) {
        const { principalClient, additionalClient, cardNumberToActivate, comodin } = data
        const dataPmp = OriginationUtils.setDataBlockTypeCreditCardPMP({
            organization: Constants.General.organization,
            cod_flujo_fase_estado_actual: 40705,
            cod_flujo_fase_estado_anterior: 40702,
            nom_actividad: 'Activación Tarjeta: PMP Activar TC',
            blockCode: comodin,
            process: {
                number: additionalClient.solicitudeCode,
                fullNumber: additionalClient.completeSolicitudeCode
            },
            client: {
                fatherId: principalClient.clientId,
                id: additionalClient.clientId,
                documentType: additionalClient.documentTypeLetter,
                documentTypeAux: additionalClient.documentTypeAux,
                documentNumber: additionalClient.documentNumber
            },
            pmp: {
                cardNumber: cardNumberToActivate || ''
            }
        })
        return dataPmp
    }

    getDataForActivateCreditCardIntoMasterApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            accountNumber: data.creditCard.accountNumber,
            token: data.creditCard.token,
            blockingCode: data.comodin,
            activityName: MasterOrigination.updateBlockingCodeCreditCardActivity.activityName,
            phaseCode: MasterOrigination.updateBlockingCodeCreditCardActivity.phaseCode
        }
    }

    getDataForRegisterActivityInitOnCreditCardActivationSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                cod_flujo_fase_estado_actual: 40706,
                cod_flujo_fase_estado_anterior: 40705,
                nom_actividad: 'Activación Tarjeta: Aceptado'
            }
        }
    }

    getDataForRegisterActivityEndOnCreditCardActivationSectionApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                cod_flujo_fase_estado_actual: 40802,
                cod_flujo_fase_estado_anterior: 40801,
                nom_actividad: 'Entrega Tarjeta: Aceptado'
            }
        }
    }

    getDataForContingencyProcessApi(data) {
        return {
            url: `${ Config.URL_ODC }/api/AdicionalesExt/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                cod_solicitud: data.solicitudeCode,
                cod_solicitud_completa: data.completeSolicitudeCode,
                cod_flujo_fase_estado_actual: 40703,
                cod_flujo_fase_estado_anterior: 40702,
                nom_actividad: 'Activación Tarjeta: Pendiente',
            }
        }
    }
}

export default AdditionalExternal