// Config
import * as Config from '../../actions/config'

// Utils
import * as Utils from '../../utils/Utils'
import * as OriginationUtils from '../../utils/origination/Origination'
import * as MasterOrigination from '../../utils/origination/MasterOrigination'

// Constants
import * as Constants from '../../utils/Constants'

class Additional {
    constructor(){
        this.showAdditionalDetailTableIntoHolderClientInformationSection = false
        this.isOptionalTokenForActivateCreditCard = true
    }

    /* Continue Process */
    getDataForContinueProcessApi(completeSolicitudeCode) {
        return {
            url: `${ Config.URL_ODC }/Originacion/SolicitudRetomar?cod_solicitud_completa=${completeSolicitudeCode}`,
            method: 'GET',
            data: null
        }
    }

    getDataForRegisterContinueProcessActivityApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                tipo_doc: data.cod_tipo_documento || 0,
                nro_doc: data.des_nro_documento || '',
                tipo_doc_letra: data.des_tipo_documento || '',
                actividadGenericoSimpleModelRequest: {
                    cod_solicitud: data.cod_solicitud || 0,
                    cod_solicitud_completa: data.cod_solicitud_completa || '',
                    cod_flujo_fase_estado_actual: 41101,
                    cod_flujo_fase_estado_anterior: 41101,
                    nom_actividad: 'Retomar Solicitud Adicional',
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }

    /* Redirecting step */
    redirectStep(status) {
        switch(status) {
            case 40402: return 4
            case 40501: return 5
            case 40601:
            case 40602: return 6
            case 40701:
            case 40702:
            case 40704:
            case 40705: 
            case 40706:
            case 40801: return 7
            default: return 0
        }
    }

    /* Reintent Process Credit Card Confirm Section */
    getReintentProcessForCreditCardConfirmSection(data) {
        const { status, message } = data || { }
        if (!status) return 0
        const next = !message
        switch (status) {
            case 40402: return 0 + Number(next)
            default: return 0
        }
    }

    /* Reintent Process Credit Card Activation Section */
    getReintentProcessForCreditCardActivationSection(data) {
        const { status, message } = data || { }
        if (!status) return 0
        const next = !message
        switch (status) {
            case 40701:
            case 40702:
            case 40703:
            case 40704: return 0
            case 40705: return next ? 2 : 0
            case 40706:
            case 40801: return 3
            default: return 0
        }
    }

    /* Step 1 */
    getDataForConsultClientApi(data) {
        return {
            url: `${ Config.URL_ODC }/Adicionales/ConsultaCliente`,
            method: 'POST',
            data: {
                cod_tipo_documento: data.documentTypeAux,
                des_nro_documento: data.documentNumber
            }
        }
    }

    /* Step 2 */
    processDataResponseOfConsultClient(data) {
        const client = data?.cliente || { }
        return {
            solicitudeCode: data.cod_solicitud,
            completeSolicitudeCode: data.cod_solicitud_completa,
            clientId: client.cod_cliente || 0,
            documentTypeAux: client.cod_tipo_documento || '',
            documentTypeId: client.cod_tipo_documento_2 || '',
            documentTypeLetter: client.des_tipo_documento || '',
            documentNumber: client.des_nro_documento || '',
            firstName: client.des_primer_nom || '',
            secondName: client.des_segundo_nom || '',
            firstLastName: client.des_ape_paterno || '',
            secondLastName: client.des_ape_materno || '',
            additionalNumber: client.num_cliente_adicional || 0,
            creditCard: {
                color: client.cod_valor_color || 0,
                colorAux: client.cod_color || '',
                brand: client.cod_valor_marca || 0,
                brandDescription: client.des_valor_marca || '',
                productId: client.cod_producto || 0,
                product: client.des_tipo_producto || '',
                productDescription: client.des_nombre_producto || '',
                productType: client.des_tipo_producto || '',
                cardNumber: client.num_tarjeta || '',
                accountNumber: client.num_cuenta || '',
                clientNumber: client.num_cliente || '',
                relationshipNumber: client.num_relacion || '',
                bin: client.cod_bin_pro || 0
            },
            codeSolicitudeType: client.cod_tipo_solicitud,
            codeSolicitudeOriginationType: client.cod_tipo_solicitud_originacion
        }
    }

    /* Step 3 */
    getDataForValidateAdditionalClientAlreadyBelongsHolderClientApi(data) {
        return {
            url: `${ Config.URL_ODC }/Adicionales/ValidarClienteAdicional?` + 
                `cod_tipo_documento_adicional=${ data.additionalClient.documentTypeAux }&` + 
                `des_nro_documento_adicional=${ data.additionalClient.documentNumber }&` + 
                `cod_tipo_documento_titular=${ data.principalClient.documentTypeAux }&` + 
                `des_nro_documento_titular=${ data.principalClient.documentNumber }`,
            method: 'GET',
            data: null
        }
    }

    getDataForAdditionalClientPreloadApi(data) {
        return {
            solicitudeCode: data.additionalClient.solicitudeCode,
            completeSolicitudeCode: data.additionalClient.completeSolicitudeCode,
            additionalDocumentTypeAux: data.additionalClient.documentTypeAux,
            additionalDocumentNumber: data.additionalClient.documentNumber,
            holderDocumentTypeAux: data.principalClient.documentTypeAux,
            holderDocumentNumber: data.principalClient.documentNumber,
            clientTypeCode: Constants.ClientTypes.additionalClientCode,
            masterStageCode: Constants.MasterConfigurationStage.masterAdditionalStage
        }
    }

    processDataResponseOfPreloadAdditionalClient(data) {
        const additional = data?.precargaAdicional || { }
        const holder = data?.precargaCliente || { }
        const homeAddress = (holder.list_tt_ori_cliente_direccionEntity || []).find(address => address.cod_tipo_direccion === 260001) || { }
        return {
            clientId: additional.cod_cliente || 0,
            documentNumber: additional.des_nro_documento || '',
            firstName: additional.des_primer_nom || '',
            secondName: additional.des_segundo_nom || '',
            firstLastName: additional.des_ape_paterno || '',
            secondLastName: additional.des_ape_materno || '',
            birthday: additional.fec_nacimiento?.split('T')[0] || '',
            genderId: additional.cod_genero || 0,
            gender: additional.des_genero || '',
            nationalityId: additional.cod_nacionalidad || 0,
            nationality: additional.des_nacionalidad || '',
            familyRelationshipId: additional.cod_vinculo_adicional || 0,
            familyRelationship: additional.des_vinculo_adicional || '',
            cellphone: additional.des_telef_celular || '',
            homeAddress: {
                departmentId: homeAddress.cod_ubi_departamento || '',
                department: homeAddress.des_ubi_departamento || '',
                districtId: homeAddress.cod_ubi_distrito || '',
                nameVia: homeAddress.des_nombre_via || '',
                viaInternalValue: homeAddress.des_via_valor_interno || '',
                number: homeAddress.des_nro || '',
                building: homeAddress.des_departamento || '',
                inside: homeAddress.des_interior || '',
                mz: homeAddress.des_manzana || '',
                lot: homeAddress.des_lote || '',
                nameZone: homeAddress.des_zona || '',
                zoneInternalValue: homeAddress.des_zona_valor_interno || '',
                reference: homeAddress.des_referencia_domicilio || ''
            }
        }
    }

    getDataForRegisterAdditionalClientIntoMasterApi(data) {
        const { additionalClient } = data
        return {
            solicitudeCode: additionalClient.solicitudeCode,
            completeSolicitudeCode: additionalClient.completeSolicitudeCode,
            activityName: MasterOrigination.registerUpdateClientActivity.activityName,
            phaseCode: MasterOrigination.registerUpdateClientActivity.phaseCode,
            masterStageCode: Constants.MasterConfigurationStage.masterAdditionalStage
        }
    }

    getDataForRegisterAdditionalClientApi(data) {
        const { principalClient, additionalClient } = data
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistroAdicional`,
            method: 'POST',
            data: {
                tipo_doc: additionalClient.documentTypeAux,
                nro_doc: additionalClient.documentNumber,
                tipo_doc_letra: additionalClient.documentTypeLetter,
                cliente_adicional: {
                    cod_cliente: additionalClient.clientId,
                    cod_cliente_titular: principalClient.clientId,
                    cod_tipo_documento: additionalClient.documentTypeAux,
                    des_nro_documento: additionalClient.documentNumber,
                    cod_cliente_documento: `${additionalClient.documentTypeAux}${additionalClient.documentNumber}`,
                    des_primer_nom: additionalClient.firstName,
                    des_segundo_nom: additionalClient.secondName,
                    des_ape_paterno: additionalClient.firstLastName,
                    des_ape_materno: additionalClient.secondLastName,
                    cod_vinculo: additionalClient.familyRelationshipId,
                    fec_nacimiento: additionalClient.birthday,
                    cod_genero: additionalClient.genderId,
                    cod_nacionalidad: additionalClient.nationalityId,
                    des_telef_celular: additionalClient.cellphone,
                    cod_tipo_cliente: 290003,
                    flg_visible: true,
                    flg_eliminado: false,
                    des_ter_act: userData.ip,
                    des_ter_reg: userData.ip,
                    des_usu_act: userData.username,
                    des_usu_reg: userData.username,
                    fec_act: userData.currentlyDate,
                    fec_reg: userData.currentlyDate
                },
                solicitud: {
                    cod_solicitud: additionalClient.solicitudeCode,
                    cod_solicitud_completa: additionalClient.completeSolicitudeCode,
                    cod_cliente: additionalClient.clientId,
                    cod_tipo_solicitud: principalClient.codeSolicitudeType,
                    cod_tipo_relacion: 330002,
                    cod_tipo_solicitud_originacion: principalClient.codeSolicitudeOriginationType,
                    flg_consumo_internet: additionalClient.flagInternetConsume === '1',
                    flg_consumo_extranjero: additionalClient.flagForeignConsume === '1',
                    flg_retirar_efectivo: additionalClient.flagEffectiveDisposition === '1',
                    flg_sobregirar_credito: additionalClient.flagOverdraftCreditCard === '1',
                    cod_resp_promotor: additionalClient.promoterCode,
                    cod_flujo_fase_estado: 40202,
                    cod_producto: principalClient.creditCard.productId,
                    cod_valor_marca: principalClient.creditCard.brand,
                    fec_act: userData.currentlyDate,
                    fec_reg: userData.currentlyDate,
                    list_ori_solicitud_actividad: [{
                        nom_actividad: 'Validación Titular: En Proceso',
                        cod_flujo_fase_estado_actual: 40101,
                        cod_flujo_fase_estado_anterior: 40100,
                        fec_actividad_ini: userData.currentlyDate,
                        fec_reg: userData.currentlyDate
                    }, {
                        cod_flujo_fase_estado_actual: 40102,
                        cod_flujo_fase_estado_anterior: 40101,
                        fec_act: userData.currentlyDate,
                        fec_actividad_fin: userData.currentlyDate,
                        fec_actividad_ini: userData.currentlyDate,
                        fec_reg: userData.currentlyDate,
                        nom_actividad: 'Validación Titular: Aceptado'
                    },{
                        nom_actividad: 'Registrar Cliente Adicional: En Proceso',
                        cod_flujo_fase_estado_actual: 40201,
                        cod_flujo_fase_estado_anterior: 40102,
                        fec_actividad_ini: userData.currentlyDate,
                        fec_actividad_fin: userData.currentlyDate,
                        fec_reg: userData.currentlyDate,
                        fec_act: userData.currentlyDate
                    }, {
                        nom_actividad: 'Registrar Cliente Adicional: Aceptado',
                        cod_flujo_fase_estado_actual: 40202,
                        cod_flujo_fase_estado_anterior: 40201,
                        fec_actividad_ini: userData.currentlyDate,
                        fec_actividad_fin: userData.currentlyDate,
                        fec_reg: userData.currentlyDate,
                        fec_act: userData.currentlyDate
                    }, {
                        nom_actividad: 'Detalle Adicional: En Proceso',
                        cod_flujo_fase_estado_actual: 40301,
                        cod_flujo_fase_estado_anterior: 40202,
                        fec_actividad_ini: userData.currentlyDate,
                        fec_actividad_fin: userData.currentlyDate,
                        fec_reg: userData.currentlyDate,
                        fec_act: userData.currentlyDate
                    }]
               }
            }
        }
    }

    processDataResponseOfRegisterAdditionalClient(data) {
        return {
            clientId: data?.cod_cliente || 0
        }
    }

    /* Step 4 */
    getDataForRegisterAdditionalDetailApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadSeleccionarColorTarjeta`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                actividadSelecionarColorTarjetaModelRequest: {
                    cod_solicitud: data.solicitudeCode,
                    cod_solicitud_completa: data.completeSolicitudeCode,
                    cod_flujo_fase_estado_actual: 40302,
                    cod_flujo_fase_estado_anterior: 40301,
                    cod_valor_color: data.creditCard.color,
                    nom_actividad: 'Detalle Adicional: Aceptado',
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }

    getDataForAdditionalDetailSectionCancelApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                actividadGenericoSimpleModelRequest: {
                    cod_solicitud: data.solicitudeCode,
                    cod_solicitud_completa: data.completeSolicitudeCode,
                    cod_flujo_fase_estado_actual: 40303,
                    cod_flujo_fase_estado_anterior: 40301,
                    nom_actividad: 'Detalle Adicional: Cancelado',
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }

    /* Step 5 */
    getDataForCreateCreditCardApi(data) {
        const { principalClient, additionalClient } = data
        const dataPmp = OriginationUtils.setDataCreateCreditCard({
            organization: Constants.General.organization,
            cod_flujo_fase_estado_actual: 40402,
            cod_flujo_fase_estado_anterior: 40401,
            nom_actividad: 'Confirmar Adicional: PMP Crear Tarjeta',
            blockCode: Constants.BlockCodePmpTypes.blockTypeCreditCardDisabled,
            process: {
                number: additionalClient.solicitudeCode,
                fullNumber: additionalClient.completeSolicitudeCode
            },
            client: {
                fatherId: principalClient.clientId,
                id: additionalClient.clientId,
                documentType: additionalClient.documentTypeLetter,
                documentTypeAux: additionalClient.documentTypeAux,
                documentTypeInternalValue: additionalClient.documentTypeInternalValue,
                documentNumber: additionalClient.documentNumber,
                shortName: `${ additionalClient.firstLastName } ${ additionalClient.firstName }`,
                nationality: additionalClient.nationality,
                homeAddress: principalClient.homeAddress
            },
            pmp: {
                accountNumber: additionalClient.creditCard.accountNumber,
                clientNumber: additionalClient.creditCard.clientNumber,
                relationshipNumber: additionalClient.creditCard.relationshipNumber
            },
            creditCard: {
                name: additionalClient.creditCard.productDescription
            }
        })
        return dataPmp
    }

    getDataForEncryptCardnumberApi(data) {
        return {
            solicitudeCode: data.solicitudeCode,
            completeSolicitudeCode: data.completeSolicitudeCode,
            pciStage: Constants.PciConfigurationStage.pciAdditionalStage
        }
    }

    getDataForCreateCreditCardIntoMasterApi(_) {
        return { next: true }
    }

    getDataForRegisterActivityOnAdditionalConfirmSectionApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                actividadGenericoModelRequest: {
                    cod_solicitud: data.solicitudeCode,
                    cod_solicitud_completa: data.completeSolicitudeCode,
                    cod_flujo_fase_estado_actual: 40403,
                    cod_flujo_fase_estado_anterior: 40402,
                    nom_actividad: 'Confirmar Adicional: Aceptado',
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }

    getDataForAdditionalConfirmSectionCancelApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                actividadGenericoSimpleModelRequest: {
                    cod_solicitud: data.solicitudeCode,
                    cod_solicitud_completa: data.completeSolicitudeCode,
                    cod_flujo_fase_estado_actual: 40403,
                    cod_flujo_fase_estado_anterior: 40401,
                    nom_actividad: 'Confirmar Adicional: Cancelado',
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }

    /* Step 6 */
    getDataForRegisterActivityOnAdditionalSummarySectionApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                actividadGenericoModelRequest: {
                    cod_solicitud: data.solicitudeCode,
                    cod_solicitud_completa: data.completeSolicitudeCode,
                    cod_flujo_fase_estado_actual: 40502,
                    cod_flujo_fase_estado_anterior: 40501,
                    nom_actividad: 'Resumen: Aceptado',
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }

    /* Step 7 */
    getDataForRegisterActivityOnAdditionalEmbossSectionApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                actividadGenericoModelRequest: {
                    cod_solicitud: data.solicitudeCode,
                    cod_solicitud_completa: data.completeSolicitudeCode,
                    cod_flujo_fase_estado_actual: 40603,
                    cod_flujo_fase_estado_anterior: 40601,
                    nom_actividad: 'Embozado: Aceptado',
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }

    getDataForPendingEmbossCreditCardApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadPendienteEmboce`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                actividadPendienteEmboceModelRequest: {
                    cod_solicitud: data.solicitudeCode,
                    cod_solicitud_completa: data.completeSolicitudeCode,
                    cod_flujo_fase_estado_actual: 40602,
                    cod_flujo_fase_estado_anterior: 40601,
                    nom_actividad: 'Embozado: Pendiente',
                    cod_motivo_emboce: data.embossReasonId,
                    des_motivo_comentario_emboce: data.description,
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }

    /* Step 8 */
    getDataForDecryptCreditCardNumber(data) {
        const { solicitudeCode, completeSolicitudeCode, creditCard } = data
        return {
            solicitudeCode: solicitudeCode,
            completeSolicitudeCode: completeSolicitudeCode,
            token: creditCard.token
        }
    }

    getDataForActivateCreditCardApi(data) {
        const { principalClient, additionalClient, cardNumberToActivate, comodin } = data
        const dataPmp = OriginationUtils.setDataBlockTypeCreditCardPMP({
            organization: Constants.General.organization,
            cod_flujo_fase_estado_actual: 40705,
            cod_flujo_fase_estado_anterior: 40702,
            nom_actividad: 'Activación Tarjeta: PMP Activar TC',
            blockCode: comodin,
            process: {
                number: additionalClient.solicitudeCode,
                fullNumber: additionalClient.completeSolicitudeCode
            },
            client: {
                fatherId: principalClient.clientId,
                id: additionalClient.clientId,
                documentType: additionalClient.documentTypeLetter,
                documentTypeAux: additionalClient.documentTypeAux,
                documentNumber: additionalClient.documentNumber
            },
            pmp: {
                cardNumber: cardNumberToActivate || ''
            }
        })
        return dataPmp
    }

    getDataForActivateCreditCardIntoMasterApi(_) {
        return { next: true }
    }

    getDataForRegisterActivityInitOnCreditCardActivationSectionApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                actividadGenericoModelRequest: {
                    cod_solicitud: data.solicitudeCode,
                    cod_solicitud_completa: data.completeSolicitudeCode,
                    cod_flujo_fase_estado_actual: 40706,
                    cod_flujo_fase_estado_anterior: 40705,
                    nom_actividad: 'Activación Tarjeta: Aceptado',
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }

    getDataForRegisterActivityEndOnCreditCardActivationSectionApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadGenerico`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                actividadGenericoModelRequest: {
                    cod_solicitud: data.solicitudeCode,
                    cod_solicitud_completa: data.completeSolicitudeCode,
                    cod_flujo_fase_estado_actual: 40802,
                    cod_flujo_fase_estado_anterior: 40801,
                    nom_actividad: 'Entrega Tarjeta: Aceptado',
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }

    getDataForContingencyProcessApi(data) {
        const userData = Utils.getDataUserSession()
        return {
            url: `${ Config.URL_ODC }/Adicionales/RegistrarActividadGenericoSimple`,
            method: 'POST',
            data: {
                tipo_doc: data.documentTypeAux,
                nro_doc: data.documentNumber,
                tipo_doc_letra: data.documentTypeLetter,
                actividadGenericoSimpleModelRequest: {
                    cod_solicitud: data.solicitudeCode,
                    cod_solicitud_completa: data.completeSolicitudeCode,
                    cod_flujo_fase_estado_actual: 40703,
                    cod_flujo_fase_estado_anterior: 40702,
                    nom_actividad: 'Activación Tarjeta: Pendiente',
                    des_usu_reg: userData.username,
                    des_ter_reg: userData.ip
                }
            }
        }
    }
}

export default Additional