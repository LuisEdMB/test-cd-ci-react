// Config
import * as Config from '../actions/config'

import Crypto from 'crypto-js';
import * as moment from 'moment';

export const textLabelsMaterialDataTable = {
    body: {
        noMatch: "No hay registros.",
        toolTip: "Ordenar",
    },
    pagination: {
        next: "Siguiente Página",
        previous: "Página Anterior",
        rowsPerPage: "Filas por página:",
        displayRows: "de",
    },
    toolbar: {
        search: "Buscar",
        downloadCsv: "Descargar CSV",
        print: "Imprimir",
        viewColumns: "Ver Columnas",
        filterTable: "Filtrar Tabla",
    },
    filter: {
        all: "Todos",
        title: "FILTROS",
        reset: "RESET",
    },
    viewColumns: {
        title: "Ver Columnas",
        titleAria: "Mostrar/Ocultar Columnas de la Tabla",
    },
    selectedRows: {
        text: "Fila(s) seleccionada(s)",
        delete: "Eliminar",
        deleteAria: "Eliminar Filas Seleccionadas",
    },
}
export function getDateConstraint(year = 2000, month = 1, day = 1) {
    let newDate = new Date(year, month, day);
    let dd = newDate.getDate();
    let mm = newDate.getMonth() + 1;
    let yyyy = newDate.getFullYear();
    if (dd < 10) {
        dd = `0${dd}`;
    }
    if (mm < 10) {
        mm = `0${mm}`;
    }
    return `${yyyy}-${mm}-${dd}`;
}
export function getDateCurrent(format = 1, short = false, description = false) {

    var monthNames = [
        "Enero", "Febreo", "Marzo",
        "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre",
        "Noviembre", "Diciembre"
    ];

    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    let seconds = today.getSeconds();
    let minutes = today.getMinutes();
    let hours = today.getHours();

    if (dd < 10) {
        dd = `0${dd}`;
    }
    if (mm < 10) {
        mm = `0${mm}`;
    }
    if (hours < 10) {
        hours = `0${hours}`
    }
    if (minutes < 10) {
        minutes = `0${minutes}`
    }
    if (seconds < 10) {
        seconds = `0${seconds}`
    }

    if (short === true) {
        switch (format) {
            case 1: today = `${dd}-${mm}-${yyyy}`; break;
            case 2: today = `${mm}-${dd}-${yyyy}`; break;
            case 3: today = `${yyyy}-${mm}-${dd}`; break;
            default: today = `${mm}-${dd}-${yyyy}`;
        }
    }
    else {
        switch (format) {
            case 1: today = `${dd}-${mm}-${yyyy}T${hours}:${minutes}:${seconds}`; break;
            case 2: today = `${mm}-${dd}-${yyyy}T${hours}:${minutes}:${seconds}`; break;
            case 3: today = `${yyyy}-${mm}-${dd}T${hours}:${minutes}:${seconds}`; break;
            default: today = `${mm}-${dd}-${yyyy}T${hours}:${minutes}:${seconds}`;
        }
    }

    if (description === true) {
        switch (format) {
            case 1: today = `${dd} ${monthNames[new Date().getMonth()].substring(0, 3)} ${yyyy},T${hours}:${minutes}:${seconds}`; break;
            case 2: today = `${monthNames[new Date().getMonth()].substring(0, 3)} ${dd} ${yyyy},T${hours}:${minutes}:${seconds}`; break;
            case 3: today = `${yyyy} ${monthNames[new Date().getMonth()].substring(0, 3)} ${dd},T${hours}:${minutes}:${seconds}`; break;
            default: today = `${monthNames[new Date().getMonth()].substring(0, 3)} ${dd} ${yyyy},T${hours}:${minutes}:${seconds}`;
        }
    }

    return today;
}
export function deleteDuplicateObjects(arr, prop) {
    var newArray = [];
    var lookup = {};

    for (var i in arr) {
        lookup[arr[i][prop]] = arr[i];
    }

    for (i in lookup) {
        newArray.push(lookup[i]);
    }
    return newArray;
}

export function verifyValueRequired(value) {
    if (value !== "" || value.length !== 0 || value !== null || value.length !== 0 || value !== undefined) {
        return true
    }
    return false;
}
export function onlyTextKeyCode(keyCode) {
    var inp = String.fromCharCode(keyCode);
    //if (/^[\u00F1A-Za-z _]*[\u00F1A-Za-z _][\u00F1A-Za-z _]*$/.test(inp)){ ///^[A-Za-z ]*$/
    if (/[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+/.test(inp)) { ///^[A-Za-z ]*$/
        return true;
    }
    return false;
}

export function onlyNumberKeyCode(keyCode) {
    var inp = String.fromCharCode(keyCode);
    if (/^\d+$/.test(inp)) {
        return true;
    }
    return false;
}
export function checkInputKeyCode(keyCode) {
    var inp = String.fromCharCode(keyCode);
    if (/^[a-zA-Z0-9 ]*$/.test(inp)) {
        return true;
    }
    return false;
}
export function checkInputKeyCodePunComSla(keyCode) {
    var inp = String.fromCharCode(keyCode);
    if (/^[a-zA-Z0-9.,#'"%@/ ]*$/.test(inp)) {
        return true;
    }
    return false;
}
export function onlyNumberRegex(value) {
    let reg = /^\d+$/;
    return reg.test(value !== "" ? value : 0) ? true : false;
}
export function onlyNumberWithDotRegex(value) {
    var rgx = /^\d+(\.\d{0,2})?$/;
    return value.toString().match(rgx);
}
export function onlyTextRegex(value) {
    let reg = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/;
    return reg.test(value) ? true : false;
}
export function checkInputRegex(value) {
    let reg = /^[a-zA-Z0-9 ]*$/;
    if (reg.test(value)) {
        return true
    }
    return false;
}

export function checkEmailInputRegex(value) {
    //if(/^\w+@[a-zA-Z_]+?\.^\[a-zA-Z]{2,3}$/.test(value)){
    //if(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value)){
    if (/^(([^<>(),;:\s@"]+(\.[^<>(),;:\s@"]+)*)|(".+"))@(([^<>()[\],;:\s@"]+\.)+[^<>()[\],;:\s@"]{2,})$/i.test(value)) {
        return true;
    }
    return false;
}

export function getYearOld(value) {
    let birthday = new Date(value);
    let today = new Date();
    let diff = today.getTime() - birthday.getTime();
    let yearOld = Math.floor(diff / (1000 * 60 * 60 * 24) / 365.25);
    return yearOld;
}

export function getDeviceData() {
    let device = sessionStorage.getItem("_device");
    let deviceData = device ? JSON.parse(device) : { ip: "127.0.0.1" };
    return deviceData;
}

export function getUrlWebServer() {
    let url = window.location.href.split('/')
    const protocol = url[0] || ''
    const domain = url[2] || ''
    return `${protocol}//${domain}`
}

export function encrypt(text, secreyKey = "Cencosud") {
    let encryptData = Crypto.AES.encrypt(text, secreyKey)
    return encryptData;
}
export function decrypt(textEncrypt, secretKey = "Cencosud") {
    let bytes = Crypto.AES.decrypt(textEncrypt.toString(), secretKey);
    let decryptedData = JSON.parse(bytes.toString(Crypto.enc.Utf8));
    // console.log(decryptedData)
    return decryptedData;
}
export function getMoneyFormat(value = 0) {
    value = parseFloat(value === "" ? "0" : value.toString());
    return value.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
export function maskCreditCard(value) {
    if (!value || value.toString().length < 16) return '**** **** **** ****'
    let newValue = value.toString()
    if (newValue.length >= 19 && newValue.includes('*')) return newValue
    newValue = newValue.length >= 19 ? newValue.substring(3, 20) : newValue
    if (newValue.includes('*'))
        return `${newValue.substring(0, 4)} ${newValue.substring(4, 8)} ${newValue.substring(8, 12)} ${newValue.substring(12, 16)}`
    return `${newValue.substring(0, 4)} ${newValue.substring(4, 6)}** **** ${newValue.substring(12, 16)}`
}

/**
 * Get the user IP throught the webkitRTCPeerConnection
 * @param onNewIP {Function} listener function to expose the IP locally
 * @return
 */
export function getIP(onNewIP) { //  onNewIp - your listener function for new IPs
    //compatibility for firefox and chrome
    var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    var pc = new myPeerConnection({
        iceServers: []
    }),
        noop = function () { },
        localIPs = {},
        ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g
    //key;

    function iterateIP(ip) {
        if (!localIPs[ip]) onNewIP(ip);
        localIPs[ip] = true;
    }

    //create a bogus data channel
    pc
        .createDataChannel("");
    // create offer and set local description
    pc
        .createOffer()
        .then(function (sdp) {
            sdp.sdp.split('\n').forEach(function (line) {
                if (line.indexOf('candidate') < 0) return;
                line.match(ipRegex).forEach(iterateIP);
            });

            pc.setLocalDescription(sdp, noop, noop);
        })
        .catch(function (reason) {
            // An error occurred, so handle the failure to connect
        });

    //listen for candidate events
    pc
        .onicecandidate = function (ice) {
            if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
            ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
        };
}
export function validateLengthDocumentType(value) {
    switch (value) {
        case 8: return true;
        case 9: return true;
        case 11: return true;
        default: return false;
    }
}

export function validateLengthFullNumber(value) {
    switch (value) {
        case 12: return true;
        default: return false;
    }
}

export function removeSpecialCharacters(value) {
    try {
        let s = value.match(/[a-zñ0-9.\s\p{L}]+/sgui).join('').replace(/\s+/gi, ' ')
        return s
    } catch (e) {
        return ''
    }
}

// Const Variable Input
export const defaultMaxAdditionalClientNumber = 3;
export const defaultInitialDate = moment().subtract(1, "days").format('YYYY-MM-DD');
export const defaultFinalDate = moment().format('YYYY-MM-DD');
export const defaultLengthFullNumber = 12;
export const defaultLengthInput = 30;
export const defaultCellphoneLengthInput = 9;
export const defaultEmailLengthInput = 100;
export const defaultMaxLengthInput = 55;
export const defaultFullMaxLengthInput = 300;
export const defaultReferenceLengthInput = 100;
export const defaultViaLengthInput = 50;

export const defaultMaxDate = moment().format('YYYY-MM-DD');
export const defaultMinDate = moment().subtract(50, "years").format('YYYY-MM-DD');


export const defaultMinBirthday = moment().subtract(65, "years").format('YYYY-MM-DD');
export const defaultMaxBirthday = moment().subtract(21, "years").format('YYYY-MM-DD');

export const defaultMinLaborIncomeDate = moment().subtract(21, "years").format('YYYY-MM-DD');
export const defaultMaxLaborIncomeDate = moment().format('YYYY-MM-DD');


export const defaultComplementaryLengthInput = 6;
export const defaultTelephoneExtensionLengthInput = 4;
export const defaultColorCode = "#007ab8";
export const defaultDurationNotification = 6000;
export const defaultPageSizes = [25, 50, 100];
export const defaultCCILengthInput = 20;
export const defaultvalCuotasLength = 12;

export const defaultEstadosSAE = [
    { label: 'Pendiente Atención', value: '1' },
    { label: 'Registrado - Conforme', value: '2' },
    { label: 'Registrado - No Conforme', value: '3' }
]

export const cencosudEffectiveBin = 902064

export function filterMaskTelephone(phone) {
    return phone.replace("(", "").replace(")", "").replace("-", "").replace("+", "").replace(/\s/g, "").trim()
}

export function HtmlEncode(s) {
    var div = document.createElement("div");
    div.innerText = div.textContent = s;
    s = div.innerHTML;
    return s;
}

export function SmoothScroll(step) {
    window.scrollTo({
        top: (45 + 32) * (step + 1) - (64 + 12),
        behavior: 'smooth',
    })
}

export function evaluatePrevaluated({ cda, adn, validations, sae }) {
    let isPrevailed = 1,
        PEP = 1,
        PIB = 1,
        isProcessSiebel = 0,
        isProcessODC = 0,
        isProcessSAE = 0,
        isProcessTcMaster = 0,
        isProcessCecMaster = 0,
        offer = {
            name: "",
            value: 0
        },
        effectiveProvision = 0;

    if (adn) {
        if (!adn && Object.keys(adn).length === 0) {
            PEP = -1;
        }
    }

    if (cda) {
        if (!cda && Object.keys(cda).length === 0) {
            PIB = -1;
        }
    }

    if (validations) {
        validations.map(item => {
            if (item.des_solicitud_actividad_log === "Validación cliente pendiente en odc.") {
                if (item.des_error_message_servicio) {
                    isProcessODC = 1;
                }
            } 
            if (item.des_solicitud_actividad_log === "Validación cliente pendiente en CE.") {
                if (item.des_error_message_servicio) {
                    isProcessSAE = 1;
                }
            }
            else if (item.des_solicitud_actividad_log === "Consulta servicio cda primera llamada.") {
                if (item.des_error_message_servicio) {
                    isPrevailed = 0;
                }
            } else if (item.des_solicitud_actividad_log === "Consulta servicio Pep.") {
                if (item.des_error_message_servicio) {
                    PEP = 0;
                }
            } else if (item.des_solicitud_actividad_log === "Consulta servicio Lic y Fraudes.") {
                if (item.des_error_message_servicio) {
                    PIB = 0;
                }
            } else if (item.des_solicitud_actividad_log === "Validación cliente pendiente en Siebel.") {
                if (item.des_error_message_servicio) {
                    isProcessSiebel = 1;
                }
            } else if (item.des_solicitud_actividad_log === "Validación tarjeta pendiente en maestro.") {
                if (item.des_error_message_servicio) {
                    isProcessTcMaster = 1;
                }
            } else if (item.des_solicitud_actividad_log === "Validación efectivo cencosud pendiente en maestro.") {
                if (item.des_error_message_servicio) {
                    isProcessCecMaster = 1;
                }
            }
            return null;
        });
    }

    if (sae === null || Object.keys(sae || { }).length === 0) {
        isProcessSAE = 1;
    }

    if (cda && Object.keys(cda).length !== 0) {
        // CDA
        let {
            solicitante,
            salidaCDA
        } = cda.solicitud;
        let {
            porcentajeDisponibleEfectivo,
            oferta,
            ofertaFinal
        } = salidaCDA;
        let {
            experienciaCrediticea,
        } = solicitante;

        offer = {
            name: "Flujo Regular",
            value: oferta ? oferta : 0
        }
        if (experienciaCrediticea === "11" || experienciaCrediticea === "13") {
            offer = {
                ...offer,
                name: "Flujo Express",
                value: ofertaFinal ? ofertaFinal : 0
            }
            isPrevailed = 1;
        }

        effectiveProvision = porcentajeDisponibleEfectivo
    }

    return {
        isPrevailed,
        PEP,
        PIB,
        isProcessSiebel,
        isProcessODC,
        isProcessSAE,
        isProcessTcMaster,
        isProcessCecMaster,
        offer,
        effectiveProvision
    }
}

export function verifyCardNumberContainsBin(cardNumber, bin) {
    return cardNumber.split('').slice(3, 9).join('').includes(bin.toString())
}

export function addNewObjectIntoArrayByPosition(array, object, position) {   
    let startArray = array.slice(0, position)
    const endArray = array.slice(position, array.length)
    startArray.push(object)
    startArray.push(...endArray)
    return startArray
}

export function addNewPropertyIntoObjectByPosition(object, value, position) {
    let newObject = { }
    Object.keys(object).map((key, index) => {
        if (index === position) {
            Object.assign(newObject, value)
        }
        Object.assign(newObject, { [key]: object[key] })
        return undefined
    })
    return newObject || object
}

export function getGroupDescription(typeSolicitudeCode, relationTypeId, typeSolicitudeOriginationCode, 
    isSAEProcess, isClientExistingProcess) {
    if (isSAEProcess) return 'CEC_CLI_NUE'
    if (isClientExistingProcess) return 'CEC_CLI_EXI'
    let groupDescription = ''
    if (typeSolicitudeCode && relationTypeId && typeSolicitudeOriginationCode) {
        groupDescription += typeSolicitudeOriginationCode === 390001 ? 'PRE' : 'REG'
        groupDescription += typeSolicitudeCode === 340002 ? '_REI' : ''
        groupDescription += relationTypeId === 330002 ? '_ADI' :
            relationTypeId === 330001 && typeSolicitudeCode === 340002 ? '_ORI' : ''
    }
    return groupDescription
}

export function getLengthDocumentByType(documentTypeId = 100001) {
    return documentTypeId === 100001 ? 8 : 9
}

export function resolveStateRedux(prev, props, callbackLoading = null, callbackSuccess = null,
    callbackWarning = null, callbackError = null) {
    if (prev !== props) {
        const error = props.error || Config.SERVER_ERROR_MESSAGE
        if (props.loading) callbackLoading && callbackLoading()
        if (!props.loading &&
            props.response &&
            props.success) callbackSuccess && callbackSuccess()
        if (!props.loading &&
            props.response &&
            !props.success &&
            error) callbackWarning && callbackWarning(error || '', props.errorCode || '')
        if (!props.loading &&
            !props.response &&
            !props.success &&
            error) callbackError && callbackError(error || '', props.errorCode || '')
    }
}

export function findProperty(object, property, excludeProperties = []) {
    let propertiesFound = []
    recursiveFindProperty(object, property, propertiesFound, excludeProperties)
    return propertiesFound
}

export function isNullOrUndefined(value) {
    return value === null || value === undefined
}

export function isArrayWithNotProperties(array) {
    return array && array.length > 0 && Object.keys(array).length > 0
}

function recursiveFindProperty(object, property, propertiesFound, excludeProperties) {
    Object.keys(object).forEach(key => {
        const propertyToExclude = excludeProperties.find(item => item === key)
        if (propertyToExclude) return
        let value = object[key]
        if (typeof value === 'object') {
            recursiveFindProperty(value, property, propertiesFound, excludeProperties)
        }
        if (key === property) {
            propertiesFound.push({ key, value })
        }
    })
}

export function handleEnterKeyPress(e, callback) {
    const code = e.which || e.keyCode
    if (code === 13) callback()
}

export function handleKeyPressTextFieldCheckInput(e) {
    let code = (e.which) ? e.which : e.keyCode;
    if (!checkInputKeyCode(code)) {
        e.preventDefault();
    }
}

export function verifyValueIntoArray(array, key, value) {
    let valueFounded = getInitialValueFromType(typeof value)
    if ((array || []).length > 0) {
        const object = array.find(item => item[key] == value)
        if (object) valueFounded = value
    }
    return valueFounded
}

function getInitialValueFromType(type) {
    switch (type) {
        case 'number':
            return 0
        default:
            return ''
    }
}

export function getDataUserSession() {
    const dataEncrypt = sessionStorage.getItem('data') 
    const decryptData = decrypt(dataEncrypt)
    const device = getDeviceData()
    return { 
        username: decryptData.username,
        ip: device.ip,
        currentlyDate: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
        agencyCode: decryptData.agency?.ide_agencia || 0
    }
}

export function comprobeFileProperties({ file, maxSize, extensions }) {
    let message = ''
    let fileVerified = null
    if (!file)
        message = 'No ha ingresado ningún archivo.'
    else if (!extensions.find(extension => file?.name?.split('.')?.pop() === extension))
        message = 'Solo puede adjuntar archivo .pdf'
    else if (file?.size > maxSize)
        message = 'El archivo excedió el tamaño límite.'
    else {
        fileVerified = file
    }
    return { message, file: fileVerified }
}

export function calculatePercentUploadFile(loaded, total) {
    return Math.round((loaded * 100) / total)
}

export function comprobeObjectIsEmpty(object) {
    return Object.keys(object || {}).length <= 0
}