import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// Actions
import { login } from '../../actions/session'; 
// Component 
import LoginForm from './components/LoginForm';
import Footer from '../../components/Footer';
// Effect
import Fade from 'react-reveal/Fade';
import makeCarousel from 'react-reveal/makeCarousel';
import Slide from 'react-reveal/Slide';
import { URL_BASE } from '../../actions/config';
// Images 
import Slider1 from "../../../src/assets/media/images/jpg/Slider-1.jpg";
import Slider2 from "../../../src/assets/media/images/jpg/Slider-2.jpg";
import Slider3 from "../../../src/assets/media/images/png/Slider-1.png";

const styles = theme => ({
    page:{
        position: "relative",
        minHeight:"100vh",
        width:"100%"
    },
    sliderImage: {
        minHeight:"100vh",
        width: "100%", 
    }, 
    slider:{
        position:"absolute",
        top:0, 
        height: "350px",
        width: "100%", 
    },
    loginContainerForm: {
        position:"absolute",
        top:0, 
        height:"100vh",
        maxHeight: "100vh",
        width: "100%", 
        zIndex:"1500"
    }, 
    copyright:{
        position:"absolute",
        bottom:0, 
        zIndex:"1500"
    }
});

const mapStateToProps = (state, props) => {
    return {
        session: state.sessionReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        login: bindActionCreators(login, dispatch)
    };
    return actions;
}

class Login extends Component {
    state = {
        items: [{url: Slider1, alt:"Slider 1" }, {url: Slider2, alt:"Slider 2" }, {url: Slider3, alt:"Slider 3" }], 
    }

    // Login - Button Event Submit
    handleSubmitLoginForm = (data) => {
        let sendData = {
            des_usuario: data.username,
            des_clave: data.password,
            flg_active_directory: this.props.bd,
            des_ter_reg: "127.0.0.1"
        }
        this.props.login(sendData);
    }
    render() {
        let {session, classes, bd } = this.props;

        let isSessionCorrect = session && session.authenticated && session.data && session.data.activeDirectory && session.data.agency && session.data.name && session.data.profiles && session.data.sessionCode && session.data.username
        // Redirect 
        if (isSessionCorrect) {
            return <Redirect to={`/${URL_BASE}`} />
        }

        const CarouselUI = ({ children }) => <div style={{
            position: "absolute",
            top:0, 
            right:0, 
            left:0,
            overflow: "hidden",
            minHeight:"100vh",
            width:"100%"
        }}>
            {children}
        </div>;

        const Carousel = makeCarousel(CarouselUI);
        return (
            <Slide >
                <div className={classes.page}>
                    <div className={classNames(classes.loginContainerForm, "d-flex justify-content-center align-items-center px-2")}>
                        <LoginForm 
                            bd={bd}
                            handleSubmitLoginForm={this.handleSubmitLoginForm} 
                            session={session}/>
                    </div>
                    <Carousel defaultWait={3500} className={classNames(classes.slider, "bg-black-transparent")}>
                        {
                            this.state.items.map((item, index)=> {
                                return (
                                    <Slide key={index} >
                                        <li>
                                            <Fade>
                                                <img className={classes.sliderImage} src={item.url} alt={item.alt} /> 
                                            </Fade>
                                        </li>
                                    </Slide>
                                )
                            })
                        }
                    </Carousel>

                    <div className={classNames(classes.copyright, "w-100 d-flex justify-content-center bg-black-transparent")}>
                        <Footer />
                    </div>
                </div>
            </Slide>
        )
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Login)));
