import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
//Components
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import CencosudLogo from '../../../assets/media/images/png/Cencosud-1.png';
//Icons 
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import SendIcon from '@material-ui/icons/Send';
//Effects
import Zoom from 'react-reveal/Zoom';
import Fade from 'react-reveal/Fade';
// Utils 
import { onlyTextKeyCode, checkInputKeyCode, defaultLengthInput } from '../../../utils/Utils';

const styles = theme => ({
    loginContainer: {
        width: "100%", 
        maxWidth: 450, 
        minWidth: 300,
        maxHeight: 600 
    }, 
    login:{
        backgroundColor: "rgba(255,255,255,0.85)"
    }, 
    avatarLoginForm:{
        position:"absolute", 
        top:-130
    },
    button: {
        textTransform: 'none',
    },
    leftIcon: {
        marginLeft: theme.spacing.unit
    },
    rightIcon: {
        marginRight: theme.spacing.unit
    },
});

class LoginForm extends Component {

    state = {
        showPassword: false,
        user: {
            username: "", 
            usernameError: false,
            password: "", 
            passwordError: false
        }
    }

    // User - Password - IconButton Event Click
    handleClickShowPassword = e => {
        this.setState(state => ({ 
            showPassword: !state.showPassword 
        }));
    }

    // User - TextField - Event Change - Required    
    handleChangeTextFieldRequired = (objectName, subObjectName, name) => e => {
        e.persist();
        let { value } = e.target;
        let nameError = `${name}Error`;
        if(subObjectName){
            this.setState(state => ({
                ...state,
                [objectName]:{
                    ...state[objectName], 
                    [subObjectName]:{
                        ...state[objectName][subObjectName],
                        [name]: value,
                        [nameError]: value !== "" ? false : true
                    }
                }
            }));
        }else{
            this.setState(state => ({
                ...state,
                [objectName]:{
                    ...state[objectName], 
                    [name]: value, 
                    [nameError]: value !== "" ? false : true
                }
            }));
        }
    }
    // User - TextField - Event Blur 
    handleBlurTextFieldRequired = (objectName, subObjectName, name) => e => {
        e.persist();
        let nameError = `${name}Error`;
        let { value } = e.target;
        if(subObjectName){
            this.setState(state => ({
                ...state,
                [objectName]:{
                    ...state[objectName], 
                    [subObjectName]:{
                        ...state[objectName][subObjectName],
                        [nameError]: value !== "" ? false : true,
                    }
                }
            }));
        }
        else{
            this.setState(state => ({
                ...state,
                [objectName]:{
                    ...state[objectName], 
                    [nameError]: value !== "" ? false : true,
                }
            }));
        }
    }
    // Check Input
    handleKeyPressTextFieldCheckInput = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!checkInputKeyCode(code)){
            e.preventDefault();
        }   
    }
    // Only Text
    handleKeyPressTextFieldOnlyText = name => e =>{
        let code = (e.which) ? e.which : e.keyCode;
        if(!onlyTextKeyCode(code)){
            e.preventDefault();
        }       
    }

    // Login Form
    handleSubmit = e =>{
        e.preventDefault(); 
        let { user } = this.state;
        let data = {
            username: user.username, 
            password: user.password, 
        }
        this.props.handleSubmitLoginForm(data);
    }
    render() {
        let { classes, session } = this.props;
        let { user } = this.state;
        return (
            <Fade>
                <div className={classNames(classes.loginContainer, "bg-black-transparent box-shadow-black rounded p-3 px-2")} >
                    {/* Form -> Login */}
                    <form 
                        autoComplete="off"
                        method="post" 
                        className={classNames(classes.login, "rounded mt-5 pb-4 px-3 d-flex justify-content-center align-items-center")}
                        onSubmit={this.handleSubmit}>
                        <Grid container spacing={8} className="w-100 rounded">
                            {/* Form - Avatar */}
                            <Grid item xs={12} className="mt-5">
                                <div style={{position:"relative"}} className="my-2 pb-4">

                                        <figure className={classNames(classes.avatarLoginForm, "d-flex justify-content-center w-100")} >
                                            <img 
                                                alt="Cencosud"
                                                height={140}
                                                src={CencosudLogo}/>
                                        </figure>

                                    <Fade top>
                                        <Typography align="center" variant="h6" className="font-weight-bold mt-3">
                                            Plataforma de Originación de Clientes
                                        </Typography>
                                    </Fade>
                                </div>
                            </Grid>
                            {/* Form - Username */}
                            <Grid item xs={12}>
                                <Fade top>
                                    {/* Username */} 
                                    <TextField 
                                        error={user.usernameError}
                                        disabled={session.loading}
                                        fullWidth={true} 
                                        required
                                        autoFocus
                                        title="Ingresar nombre de usuario de windows"
                                        id="username"
                                        name="username"
                                        label="Nombre de usuario (Windows)"
                                        type="text"
                                        margin="normal"
                                        value={user.username}
                                        onChange={this.handleChangeTextFieldRequired("user", null, "username")}
                                        onKeyPress={this.handleKeyPressTextFieldCheckInput("user", null, "username")}
                                        onBlur={this.handleBlurTextFieldRequired("user", null, "username")}
                                        InputProps={{
                                            inputProps:{
                                                maxLength: defaultLengthInput,
                                            },
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <IconButton disabled>
                                                        <AccountBoxIcon />
                                                    </IconButton>
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                </Fade>
                            </Grid>
                            {/* Form -> Password */}
                            <Grid item xs={12} className="mb-3">
                                <Fade top>
                                    {/* Password */} 
                                    <TextField 
                                        error={user.passwordError}
                                        disabled={session.loading}
                                        fullWidth={true} 
                                        required
                                        title="Ingresar contraseña del usuario de windows"
                                        id="password"
                                        name="password"
                                        label="Contraseña"
                                        margin="normal"
                                        autoComplete={this.state.showPassword ? 'off' : 'new-password'} 
                                        value={user.password}
                                       
                                        onChange={this.handleChangeTextFieldRequired("user", null, "password")}
                                        //onKeyPress={this.handleKeyPressTextFieldCheckInput("user", null, "password")}
                                        onBlur={this.handleBlurTextFieldRequired("user", null, "password")}

                                        type={this.state.showPassword ? 'text' : 'password'}
                                        InputProps={{
                                            inputProps:{
                                                maxLength: defaultLengthInput,
                                            },
                                            endAdornment: (
                                                <Tooltip 
                                                TransitionComponent={Zoom} 
                                                    title={this.state.showPassword ? "Hacer click para ocultar contraseña." : "Hacer click para visualizar contraseña."}> 
                                                    <InputAdornment position="end">
                                                        <IconButton
                                                            aria-label="Toggle password visibility"
                                                            onClick={this.handleClickShowPassword}
                                                        >
                                                        {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                                                        </IconButton>
                                                    </InputAdornment>
                                                </Tooltip>
                                            ),
                                        }}
                                    />
                                </Fade>
                            </Grid>
                            {/* Form - Submit */}
                            <Grid item xs={12} className="mb-2">
                                {/* Submit */}   
                                <Zoom>
                                    <Tooltip 
                                        TransitionComponent={Zoom} 
                                        title="Hacer click para ingresar a la Plataforma.">      
                                        <div>
                                            <Button 
                                                disabled={session.loading}
                                                className={classes.button}
                                                fullWidth={true} 
                                                type="submit"
                                                variant="contained" 
                                                color="primary"
                                                size="small" 
                                                margin="normal">
                                                Iniciar sesión
                                                <SendIcon 
                                                    fontSize="small"
                                                    className={classNames(classes.leftIcon)} 
                                                />
                                            </Button>
                                        </div>
                                    </Tooltip>
                                </Zoom>     
                            </Grid>
                            {/* Form - Linear Progress */}
                            <Grid item xs={12} className="mb-2">
                                <div>
                                    {
                                        session.loading && <LinearProgress/> 
                                    }
                                </div>
                            </Grid>
                            {/* Form - Message */}
                            <Grid item xs={12}>
                                <Fade>
                                    <Typography align="center" className="font-italic text-red">
                                        <span className="small font-weight-bold">
                                            { session.error ? session.error : ""}
                                        </span>
                                    </Typography>
                                </Fade>
                            </Grid>
                        </Grid>
                    </form>
                </div>

            </Fade>
        );
    } 
}


const { func } = PropTypes;

LoginForm.propTypes = {
    handleSubmitLoginForm: func.isRequired
};

LoginForm.defaultProps = {
    
};

export default withStyles(styles)(LoginForm);