import React, { Component } from 'react'
import Typography from '@material-ui/core/Typography';

class NotFound extends Component{
    render(){
        return(
            <div>
                <Typography 
                    component="h3" 
                    variant="h5">
                    404 Página no encontrada
                </Typography>
                <Typography>
                    Lo sentimos pero la página que busca no existe o se encuentra en mantenimiento.
                </Typography>
            </div>
        )
    }
}

export default NotFound;