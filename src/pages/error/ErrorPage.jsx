import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Fade from 'react-reveal/Fade';
import Error500 from '../../assets/media/images/png/Error-500.png';
import { Button } from '@material-ui/core';
import { Redirect } from "react-router-dom";
import { URL_BASE } from './../../actions/config';
const styles = theme => ({

    root: {
        width: '100%',
        minHeight: 450,
        background: "#08c6ff1a",
        display:"flex", 
        justifyContent:"center", 
        alignItems:"center", 
        [theme.breakpoints.up('sm')]: {
   
        },
        [theme.breakpoints.up('md')]: {
            minHeight: 600,        }
    },
    imageWrapper:{
        display:"flex", 
        justifyContent:"center", 
        alignItems:"center", 
        flexDirection:"column",
        width:"100%", 
    },
    image:{
        width:"100%", 
        minHeight:280,
        [theme.breakpoints.up('sm')]: {
        
        },
        [theme.breakpoints.up('md')]: {
            width:"100%", 
            minHeight:"100%", 
        }
    }, 
    title:{
        fontSize:14, 
        textTransform: 'none',
        margin:"2em 0",
        [theme.breakpoints.up('sm')]: {
            fontSize:16, 
        },
        [theme.breakpoints.up('md')]: {
            fontSize:18, 
        }
    }
});


class ErrorPage extends Component{
    state = {
        redirectMenu:false
    }
    handleRedirect = e => {
        e.preventDefault();
        this.setState({
            redirectMenu : true
        });
    }
    render(){
        let {classes } = this.props;
        const { redirectMenu } = this.state;
        if(redirectMenu){
            return <Redirect to={{ pathname: `/${URL_BASE}`, }} />
        }
        return (
            <Grid container>
                <Grid item xs={12}  >
                    <Grid container className={classes.root}>
                        <Grid item xs={12} md={6} >
                            <figure className={classes.imageWrapper}>
                                <Fade>
                                    <img className={classes.image} src={Error500} alt="Cencosud"/>
                                </Fade>
                                <figcaption>
                                    <Fade>
                                        <Button
                                            onClick={this.handleRedirect} 
                                            color="primary"  variant="contained" className={classes.title}>
                                            Click! para regresar al Menu Principal
                                        </Button>
                                    </Fade>
                                </figcaption>
                            </figure>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(ErrorPage);
