import React, { useEffect } from 'react';
//React Router 
import { Route, Redirect, Switch, withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import PrivateRoute from '../utils/PrivateRoute';
//Pages
import OriginationPageRoute from './origination/OriginationPageRoute';
import Login from './login/Login';
import { URL_BASE } from '../actions/config';
//Socket.io
import socketIOClient from 'socket.io-client'
//Utils
import * as Utils from '../utils/Utils'

const mapStateToProps = (state, props) => {
    return {
        session: state.sessionReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {

    };
    return actions;
}

const App = (props) => {
    let { session, history } = props;

    useEffect(() => {
        if (process.env.NODE_ENV === '') {
            sessionStorage.removeItem('connectedSocket')
            const urlWebServer = Utils.getUrlWebServer()
            const socket = socketIOClient(urlWebServer, {
                transports: ['websocket'],
                upgrade: false
            })
            socket.on('disconnect', _ => window.location.reload(true))
        }
    }, [])

    return (
        <main>
            <Switch>
                <Redirect exact from='/' to='/iniciar-sesion'/>
                <Route path="/iniciar-sesion-bd" render={(props) => <Login bd={0} {...props}/>}/> 
                <Route path="/iniciar-sesion" render={(props) => <Login bd={1} {...props}/>}/> 
                <PrivateRoute 
                        path={`/${URL_BASE}`} 
                        component={OriginationPageRoute} 
                        authenticated={session.authenticated}
                        history={history}
                />
            </Switch>
        </main>
    );
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
