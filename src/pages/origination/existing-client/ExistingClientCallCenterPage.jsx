// React
import React, { useCallback, useEffect, useState } from 'react'

// Material UI
import { Button, Paper, Step, StepContent, StepLabel, Stepper, Typography, withStyles } from '@material-ui/core'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionConstantOdc from '../../../actions/generic/constant'

// Shorcurt
import Hotkeys from 'react-hot-keys'

// Effects
import Fade from 'react-reveal/Fade'

// Notify
import { SnackbarProvider } from 'notistack'

// Utils
import classNames from 'classnames'

// Components
import ClientConsult from './components/client-consult/ClientConsult'
import ClientValidated from './components/client-validated/ClientValidated'
import ClientInformation from './components/client-information/ClientInformation'
import CommercialOffer from './components/commercial-offer/CommercialOffer'
import ProductDetail from './components/product-detail/ProductDetail'
import ModalCancelOrigination from './components/extra-components/ModalCancelOrigination'
import BackgroundFinish from '../../../components/background-finish/BackgroundFinish'
import HistoryPanel from '../express/components/history-panel/HistoryPanel'
import NewCommentForm from '../../../components/NewCommentForm/NewCommentForm'

const styles = theme => ({
    root: {
        width: '100%',
        position: 'relative'
    },
    stepLabelOk:{
        background: '#2196f31c',
        padding: '.5em',
        borderRadius: '.5em',
        boxShadow: '0px 1.2px 1px #007ab8',
        opacity: '1 !important'
    },
    button: {
        textTransform: 'none'
    },
    resetContainer: {
        padding: theme.spacing.unit * 3,
        textAlign: 'center'
    },
    commentFormButton: {
        position: 'fixed',
        top: '20vh',
        right: '5px',
        zIndex: 900,
    }
})

const steps = [
    { id: 1, title: 'Originación Efectivo Cencosud - Consulta cliente' },
    { id: 2, title: 'Validación Cliente' },
    { id: 3, title: 'Actualización del Cliente' },
    { id: 4, title: 'Oferta Comercial' },
    { id: 5, title: 'Detalle Producto' }
]

function ExistingClientCallCenterPage(props) {
    const { classes, getConstantOdc } = props

    const [ activeStep, setActiveStep ] = useState(0)
    const [ modalCancelActivity, setModalCancelActivity ] = useState(false)
    const [ dataCancelActivity, setDataCancelActivity ] = useState({})
    const [ stateStepClientConsult, setStateStepClientConsult ] = useState({})
    const [ stateStepClientValidated, setStateStepClientValidated ] = useState({})
    const [ stateStepClientInformation, setStateStepClientInformation ] = useState({})
    const [ stateStepCommercialOffer, setStateStepCommercialOffer ] = useState({})
    const [ showHistoryPanel, setShowHistoryPanel ] = useState(false)

    useEffect(_ => {
        handleReset()
        getConstantOdc()
        window.addEventListener('beforeunload', handleReloadPage)
        return _ => window.removeEventListener('beforeunload', handleReloadPage)
    }, [])

    const handleReloadPage = e => {
        (e || window.event).returnValue = 'o/'
        return 'o/'
    }

    const handleNextStep = useCallback(_ => {
        setActiveStep(activeStep + 1)
    }, [activeStep])

    const handleReset = useCallback(_ => {
        setActiveStep(0)
        setModalCancelActivity(false)
        setDataCancelActivity({})
        setStateStepClientConsult({})
        setStateStepClientValidated({})
        setStateStepClientInformation({})
        setStateStepCommercialOffer({})
        setShowHistoryPanel(false)
    }, [])

    const showModalCancelActivity = useCallback(data => {
        setDataCancelActivity(data)
        setModalCancelActivity(true)
    }, [])

    /*States of steps*/
    const handleSaveStateStepClientConsult = useCallback(data => {
        setStateStepClientConsult(data)
    }, [setStateStepClientConsult])

    const handleSaveStateStepClientValidated = useCallback(data => {
        setStateStepClientValidated(data)
    }, [setStateStepClientValidated])

    const handleSaveStateStepClientInformation = useCallback(data => {
        setStateStepClientInformation(data)
    }, [setStateStepClientInformation])

    const handleSaveStateStepCommercialOffer = useCallback(data => {
        setStateStepCommercialOffer(data)
    }, [setStateStepCommercialOffer])
    /**/

    return (
        <SnackbarProvider
            maxSnack={ 3 }>
                <Hotkeys
                    keyName='shift+i, ctrl+i'
                    onKeyUp={ _ => setShowHistoryPanel(!showHistoryPanel) }>
                        <div 
                            className={ classes.root }>
                                <HistoryPanel 
                                    onClick={ _ => setShowHistoryPanel(!showHistoryPanel) }
                                    showHistory={ showHistoryPanel }
                                    effectiveProvision={ 0 }
                                    finalEffectiveProvision={ 0 }
                                    lineAvailable={ stateStepClientValidated?.lineAvailable || 0 }
                                    finalLineAvailable={ -1 }
                                    client={{
                                        firstName: stateStepClientInformation?.personalInformation?.firstName || '',
                                        secondName: stateStepClientInformation?.personalInformation?.secondName || '',
                                        lastName: stateStepClientInformation?.personalInformation?.firstLastname || '',
                                        motherLastName: stateStepClientInformation?.personalInformation?.secondLastname || '',
                                        documentType: stateStepClientConsult?.documentTypeLetter || '',
                                        documentNumber: stateStepClientConsult?.documentNumber || '',
                                        email: stateStepClientInformation?.personalInformation?.email || '',
                                        birthday: stateStepClientInformation?.personalInformation?.birthday || ''
                                    }}
                                    origination={{
                                        fullNumber: stateStepClientValidated?.completeSolicitudeCode || ''
                                    }}
                                    numero_cuotas={ stateStepCommercialOffer?.numberFees || 0 }
                                    monto_cuota={ stateStepCommercialOffer?.feeAmount || 0 }
                                    tcea={ stateStepCommercialOffer?.tcea || 0 }
                                    lineSAE={ stateStepCommercialOffer?.lineAvailable?.value || -1 }
                                    isSAEProcess
                                />
                                <div 
                                    className={ classes.commentFormButton }>
                                        <NewCommentForm
                                            fixed={ true }
                                            codSolicitud={ stateStepClientValidated?.solicitudeCode || 0 }
                                            codSolicitudCompleta={ stateStepClientValidated?.completeSolicitudeCode || '' }
                                        />
                                </div>
                                <Stepper 
                                    activeStep={ activeStep } 
                                    orientation='vertical'>
                                        {
                                            steps.map(step => 
                                                <Step 
                                                    key={ step.id }>
                                                        <StepLabel>
                                                            <Fade>
                                                                <Typography
                                                                    color={ 'primary' }
                                                                    className={ classNames(classes.stepLabelOk, 'text-uppercase') }>
                                                                    { step.title }
                                                                </Typography>
                                                            </Fade>
                                                        </StepLabel>
                                                        <StepContent>
                                                            <div 
                                                                className='py-4'>
                                                                    {
                                                                        step.id === 1
                                                                            ? <ClientConsult
                                                                                handleNextStep={ handleNextStep }
                                                                                handleSaveStateStepClientConsult={ handleSaveStateStepClientConsult }/> :
                                                                        step.id === 2
                                                                            ? <ClientValidated
                                                                                handleNextStep={ handleNextStep }
                                                                                stateStepClientConsult={ stateStepClientConsult }
                                                                                handleSaveStateStepClientValidated={ handleSaveStateStepClientValidated }
                                                                                showModalCancelActivity={ showModalCancelActivity } /> :
                                                                        step.id === 3
                                                                            ? <ClientInformation
                                                                                handleNextStep={ handleNextStep }
                                                                                stateStepClientConsult={ stateStepClientConsult }
                                                                                stateStepClientValidated={ stateStepClientValidated }
                                                                                handleSaveStateStepClientInformation={ handleSaveStateStepClientInformation }
                                                                                showModalCancelActivity={ showModalCancelActivity }
                                                                                callCenter /> :
                                                                        step.id === 4
                                                                            ? <CommercialOffer
                                                                                handleNextStep={ handleNextStep }
                                                                                stateStepClientConsult={ stateStepClientConsult }
                                                                                stateStepClientValidated={ stateStepClientValidated }
                                                                                stateStepClientInformation={ stateStepClientInformation }
                                                                                handleSaveStateStepCommercialOffer={ handleSaveStateStepCommercialOffer }
                                                                                showModalCancelActivity={ showModalCancelActivity }
                                                                                callCenter /> :
                                                                        step.id === 5
                                                                            ? <ProductDetail
                                                                                handleNextStep={ handleNextStep }
                                                                                stateStepClientConsult={ stateStepClientConsult }
                                                                                stateStepClientValidated={ stateStepClientValidated }
                                                                                stateStepClientInformation={ stateStepClientInformation }
                                                                                stateStepCommercialOffer={ stateStepCommercialOffer }
                                                                                showModalCancelActivity={ showModalCancelActivity }
                                                                                callCenter />
                                                                            : null
                                                                    }
                                                            </div>
                                                        </StepContent>
                                                </Step>
                                            )
                                        }
                                </Stepper>
                                {
                                    activeStep === steps.length && (
                                        <Paper
                                            square
                                            elevation={ 0 }
                                            className={ classes.resetContainer }
                                            style={{ textAlign: 'center' }}>
                                                <BackgroundFinish
                                                    callCenter />
                                                <br />
                                                <Button
                                                    variant='contained'
                                                    color='primary'
                                                    className={ classes.button }
                                                    onClick={ handleReset }>
                                                    Nueva Originación
                                                </Button>
                                        </Paper>
                                    )
                                }
                                <ModalCancelOrigination
                                    open={ modalCancelActivity }
                                    close={ _ => setModalCancelActivity(false) }
                                    data={ dataCancelActivity }
                                    handleReset={ handleReset } />
                        </div>
                </Hotkeys>
        </SnackbarProvider>
    )
}

function mapDispatchToProps(dispatch) {
    return {
        getConstantOdc: bindActionCreators(ActionConstantOdc.getConstantODC, dispatch)
    }
}

export default withStyles(styles)(connect(null, mapDispatchToProps)(ExistingClientCallCenterPage))