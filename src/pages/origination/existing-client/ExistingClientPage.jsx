// React
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { Redirect, withRouter } from 'react-router'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcExistingClient from '../../../actions/odc-existing-client/odc-existing-client'
import * as ActionConstantOdc from '../../../actions/generic/constant'

// Material UI
import { Button, Paper, Step, StepContent, StepLabel, Stepper, Typography, withStyles } from '@material-ui/core'

// Shorcurt
import Hotkeys from 'react-hot-keys'

// Effects
import Fade from 'react-reveal/Fade'

// Notify
import { SnackbarProvider } from 'notistack'

// Utils
import classNames from 'classnames'
import * as Utils from '../../../utils/Utils'
import * as Config from '../../../actions/config'
import * as ClientExisting from '../../../utils/origination/Existentes'

// Components
import ClientConsult from './components/client-consult/ClientConsult'
import ClientValidated from './components/client-validated/ClientValidated'
import ClientInformation from './components/client-information/ClientInformation'
import CommercialOffer from './components/commercial-offer/CommercialOffer'
import ProductDetail from './components/product-detail/ProductDetail'
import SummaryOffer from './components/summary-offer/SummaryOffer'
import ActivationOrigination from './components/activation-origination/ActivationOrigination'
import ModalCancelOrigination from './components/extra-components/ModalCancelOrigination'
import BackgroundFinish from '../../../components/background-finish/BackgroundFinish'
import VerifyEmail from '../../../components/VerifyEmail'
import HistoryPanel from '../express/components/history-panel/HistoryPanel'
import NewCommentForm from '../../../components/NewCommentForm/NewCommentForm'
import PreLoaderImage from '../../../components/PreLoaderImage'

const styles = theme => ({
    root: {
        width: '100%',
        position: 'relative'
    },
    stepLabelOk:{
        background: '#2196f31c',
        padding: '.5em',
        borderRadius: '.5em',
        boxShadow: '0px 1.2px 1px #007ab8',
        opacity: '1 !important'
    },
    button: {
        textTransform: 'none'
    },
    resetContainer: {
        padding: theme.spacing.unit * 3,
        textAlign: 'center'
    },
    commentFormButton: {
        position: 'fixed',
        top: '20vh',
        right: '5px',
        zIndex: 900,
    }
})

const steps = [
    { id: 1, title: 'Originación Efectivo Cencosud - Consulta cliente' },
    { id: 2, title: 'Validación Cliente' },
    { id: 3, title: 'Actualización del Cliente' },
    { id: 4, title: 'Oferta Comercial' },
    { id: 5, title: 'Detalle Producto' },
    { id: 6, title: 'Resumen Oferta' },
    { id: 7, title: 'Validación de Identidad' }
]

function ExistingClientPage(props) {
    const { classes, verifyEmail, location, odcExistingClient, continueProcess, registerActivity, getConstantOdc } = props

    const prevVerifyEmail = useRef(verifyEmail)
    const prevOdcExistingClient = useRef(odcExistingClient)

    const [ activeStep, setActiveStep ] = useState(0)
    const [ modalCancelActivity, setModalCancelActivity ] = useState(false)
    const [ dataCancelActivity, setDataCancelActivity ] = useState({})
    const [ stateStepClientConsult, setStateStepClientConsult ] = useState({})
    const [ stateStepClientValidated, setStateStepClientValidated ] = useState({})
    const [ stateStepClientInformation, setStateStepClientInformation ] = useState({})
    const [ stateStepCommercialOffer, setStateStepCommercialOffer ] = useState({})
    const [ emailConfirmed, setEmailConfirmed ] = useState({
        confirmed: false,
        show: true
    })
    const [ isBiometricOk, setIsBiometricOk ] = useState(false)
    const [ showHistoryPanel, setShowHistoryPanel ] = useState(false)
    const [ loadingContinueProcess, setLoadingContinueProcess ] = useState(false)
    const [ errorContinueProcess, setErrorContinueProcess ] = useState(false)
    const [ phaseCodeFromContinueProcess, setPhaseCodeFromContinueProcess ] = useState(0)

    useEffect(_ => {
        handleReset()
        getConstantOdc()
        const { cod_solicitud_completa } = location.state?.dataUpdate || {}
        if (cod_solicitud_completa) {
            setLoadingContinueProcess(true)
            continueProcess(cod_solicitud_completa)
        }
        window.addEventListener('beforeunload', handleReloadPage)
        return _ => window.removeEventListener('beforeunload', handleReloadPage)
    }, [])

    useEffect(_ => {
        Utils.resolveStateRedux(prevVerifyEmail.current.updateClientMailService, verifyEmail.updateClientMailService,
            null, _ => {
                setEmailConfirmed({ confirmed: true, show: false })
            }, null, null)
    }, [verifyEmail.updateClientMailService])

    useEffect(_ => {
        Utils.resolveStateRedux(prevOdcExistingClient.current.process, odcExistingClient.process, null, 
            _ => {
                const data = odcExistingClient.process.data?.solicitud_Retomar || { }
                setStateStepClientConsult({
                    documentTypeId: data.cod_tipo_documento_2 || 0,
                    documentNumber: data.des_nro_documento || '',
                    documentType: data.cod_tipo_documento || '',
                    documentTypeLetter: data.des_tipo_documento || '',
                    typeSolicitudeCode: data.cod_tipo_solicitud || 0,
                    typeSolicitudeOriginationCode: data.cod_tipo_solicitud_originacion || 0,
                    relationTypeId: data.cod_tipo_relacion || 0
                })
                setStateStepClientValidated({
                    clientCode: data.cod_cliente || 0,
                    solicitudeCode: data.cod_solicitud || 0,
                    completeSolicitudeCode: data.cod_solicitud_completa || '',
                    cencosudCash: data.linea_credito_oferta || 0,
                    lineAvailable: data.linea_credito_oferta || 0,
                    pct: data.pct_sae_final || '',
                    pmpClientNumber: data.num_cliente_pmp || '',
                    pmpAccountNumber: data.num_cuenta_pmp || '',
                    pmpRelationNumber: data.num_relacion_pmp || '',
                    pmpCardNumber: data.num_tarjeta_pmp || ''
                })
                setStateStepClientInformation({
                        personalInformation: {
                            firstName: data.des_primer_nom || '',
                            secondName: data.des_segundo_nom || '',
                            firstLastName: data.des_ape_paterno || '',
                            secondLastName: data.des_ape_materno || '',
                            birthday: data.fec_nacimiento?.split('T')[0] || '',
                            email: data.des_correo || ''
                    }
                })
                setStateStepCommercialOffer({
                    disbursementTypeId: data.cod_tipo_desembolso || 0,
                    lineAvailable: {
                        value: data.linea_credito_final || 0,
                    },
                    cci: '',
                    numberFees: data.numero_cuotas || 0,
                    feeAmount: data.monto_cuota || 0,
                    tcea: data.tcea || 0
                })
                setEmailConfirmed({
                    confirmed: data.flg_confirmado || false,
                    show: !data.flg_confirmado || false
                })
                setActiveStep(ClientExisting.redirectStepClientExisting(data.cod_flujo_fase_estado || 0))
                setIsBiometricOk(data.flg_biometria || false)
                setLoadingContinueProcess(false)
                setPhaseCodeFromContinueProcess(data.cod_flujo_fase_estado || 0)
                const activity = {
                    solicitudeCode: data.cod_solicitud || 0,
                    completeSolicitudeCode: data.cod_solicitud_completa || '',
                    currentlyPhaseCode: 101001,
                    previousPhaseCode: 101001,
                    activityName: 'Retomar Solicitud Cliente Existente'
                }
                registerActivity(activity, true)
            },
            _ => setErrorContinueProcess(true),
            _ => setErrorContinueProcess(true))
    }, [odcExistingClient.process])

    const handleReloadPage = e => {
        (e || window.event).returnValue = 'o/'
        return 'o/'
    }

    const handleNextStep = useCallback(_ => {
        setActiveStep(activeStep + 1)
    }, [activeStep])

    const handleReset = useCallback(_ => {
        setActiveStep(0)
        setModalCancelActivity(false)
        setDataCancelActivity({})
        setStateStepClientConsult({})
        setStateStepClientValidated({})
        setStateStepClientInformation({})
        setStateStepCommercialOffer({})
        setEmailConfirmed({
            confirmed: false,
            show: true
        })
        setIsBiometricOk(false)
        setShowHistoryPanel(false)
        setLoadingContinueProcess(false)
        setErrorContinueProcess(false)
        setPhaseCodeFromContinueProcess(0)
    }, [])

    const showModalCancelActivity = useCallback(data => {
        setDataCancelActivity(data)
        setModalCancelActivity(true)
    }, [])

    /*States of steps*/
    const handleSaveStateStepClientConsult = useCallback(data => {
        setStateStepClientConsult(data)
    }, [setStateStepClientConsult])

    const handleSaveStateStepClientValidated = useCallback(data => {
        setStateStepClientValidated(data)
    }, [setStateStepClientValidated])

    const handleSaveStateStepClientInformation = useCallback(data => {
        setStateStepClientInformation(data)
    }, [setStateStepClientInformation])

    const handleSaveStateStepCommercialOffer = useCallback(data => {
        setStateStepCommercialOffer(data)
    }, [setStateStepCommercialOffer])
    /**/

    if (errorContinueProcess) return <Redirect to={{ pathname: `/${ Config.URL_BASE }/error`, }} />
    if (loadingContinueProcess) return <PreLoaderImage text='Retomando proceso, espere por favor ...' />
    return (
        <SnackbarProvider
            maxSnack={ 3 }>
                <Hotkeys
                    keyName='shift+i, ctrl+i'
                    onKeyUp={ _ => setShowHistoryPanel(!showHistoryPanel) }>
                        <div 
                            className={ classes.root }>
                                {
                                    (activeStep === 5 || activeStep === 6) && 
                                        <VerifyEmail
                                            client={{
                                                email: stateStepClientInformation?.personalInformation?.email || '',
                                                emailConfirmed: emailConfirmed.confirmed,
                                                documentNumber: stateStepClientConsult?.documentNumber || '',
                                                id: stateStepClientValidated?.clientCode || 0
                                            }}
                                            origination={{
                                                fullNumber: stateStepClientValidated?.completeSolicitudeCode || ''
                                            }}
                                            onClick={ _ => setEmailConfirmed(state => ({ ...state, show: !state.show })) }
                                            showVerifyEmail={ emailConfirmed.show }
                                        />
                                }
                                <HistoryPanel 
                                    onClick={ _ => setShowHistoryPanel(!showHistoryPanel) }
                                    showHistory={ showHistoryPanel }
                                    effectiveProvision={ 0 }
                                    finalEffectiveProvision={ 0 }
                                    lineAvailable={ stateStepClientValidated?.lineAvailable || 0 }
                                    finalLineAvailable={ -1 }
                                    client={{
                                        firstName: stateStepClientInformation?.personalInformation?.firstName || '',
                                        secondName: stateStepClientInformation?.personalInformation?.secondName || '',
                                        lastName: stateStepClientInformation?.personalInformation?.firstLastname || '',
                                        motherLastName: stateStepClientInformation?.personalInformation?.secondLastname || '',
                                        documentType: stateStepClientConsult?.documentTypeLetter || '',
                                        documentNumber: stateStepClientConsult?.documentNumber || '',
                                        email: stateStepClientInformation?.personalInformation?.email || '',
                                        birthday: stateStepClientInformation?.personalInformation?.birthday || ''
                                    }}
                                    origination={{
                                        fullNumber: stateStepClientValidated?.completeSolicitudeCode || ''
                                    }}
                                    numero_cuotas={ stateStepCommercialOffer?.numberFees || 0 }
                                    monto_cuota={ stateStepCommercialOffer?.feeAmount || 0 }
                                    tcea={ stateStepCommercialOffer?.tcea || 0 }
                                    lineSAE={ stateStepCommercialOffer?.lineAvailable?.value || -1 }
                                    isSAEProcess
                                />
                                <div 
                                    className={ classes.commentFormButton }>
                                        <NewCommentForm
                                            fixed={ true }
                                            codSolicitud={ stateStepClientValidated?.solicitudeCode || 0 }
                                            codSolicitudCompleta={ stateStepClientValidated?.completeSolicitudeCode || '' }
                                        />
                                </div>
                                <Stepper 
                                    activeStep={ activeStep } 
                                    orientation='vertical'>
                                        {
                                            steps.map(step => 
                                                <Step 
                                                    key={ step.id }>
                                                        <StepLabel>
                                                            <Fade>
                                                                <Typography
                                                                    color={ 'primary' }
                                                                    className={ classNames(classes.stepLabelOk, 'text-uppercase') }>
                                                                    { step.title }
                                                                </Typography>
                                                            </Fade>
                                                        </StepLabel>
                                                        <StepContent>
                                                            <div 
                                                                className='py-4'>
                                                                    {
                                                                        step.id === 1
                                                                            ? <ClientConsult
                                                                                handleNextStep={ handleNextStep }
                                                                                handleSaveStateStepClientConsult={ handleSaveStateStepClientConsult }/> :
                                                                        step.id === 2
                                                                            ? <ClientValidated
                                                                                handleNextStep={ handleNextStep }
                                                                                stateStepClientConsult={ stateStepClientConsult }
                                                                                handleSaveStateStepClientValidated={ handleSaveStateStepClientValidated }
                                                                                showModalCancelActivity={ showModalCancelActivity } /> :
                                                                        step.id === 3
                                                                            ? <ClientInformation
                                                                                handleNextStep={ handleNextStep }
                                                                                stateStepClientConsult={ stateStepClientConsult }
                                                                                stateStepClientValidated={ stateStepClientValidated }
                                                                                handleSaveStateStepClientInformation={ handleSaveStateStepClientInformation }
                                                                                showModalCancelActivity={ showModalCancelActivity } /> :
                                                                        step.id === 4
                                                                            ? <CommercialOffer
                                                                                handleNextStep={ handleNextStep }
                                                                                stateStepClientConsult={ stateStepClientConsult }
                                                                                stateStepClientValidated={ stateStepClientValidated }
                                                                                stateStepClientInformation={ stateStepClientInformation }
                                                                                handleSaveStateStepCommercialOffer={ handleSaveStateStepCommercialOffer }
                                                                                showModalCancelActivity={ showModalCancelActivity } /> :
                                                                        step.id === 5
                                                                            ? <ProductDetail
                                                                                handleNextStep={ handleNextStep }
                                                                                stateStepClientConsult={ stateStepClientConsult }
                                                                                stateStepClientValidated={ stateStepClientValidated }
                                                                                stateStepClientInformation={ stateStepClientInformation }
                                                                                stateStepCommercialOffer={ stateStepCommercialOffer }
                                                                                showModalCancelActivity={ showModalCancelActivity } /> :
                                                                        step.id === 6
                                                                            ? <SummaryOffer
                                                                                handleNextStep={ handleNextStep }
                                                                                stateStepClientConsult={ stateStepClientConsult }
                                                                                stateStepClientValidated={ stateStepClientValidated }
                                                                                stateStepClientInformation={ stateStepClientInformation }
                                                                                stateStepCommercialOffer={ stateStepCommercialOffer } /> :
                                                                        step.id === 7
                                                                            ? <ActivationOrigination
                                                                                handleNextStep={ handleNextStep }
                                                                                stateStepClientConsult={ stateStepClientConsult }
                                                                                stateStepClientValidated={ stateStepClientValidated }
                                                                                stateStepCommercialOffer={ stateStepCommercialOffer }
                                                                                isBiometricOk={ isBiometricOk }
                                                                                phaseCodeFromContinueProcess={ phaseCodeFromContinueProcess }
                                                                                handleReset={ handleReset } /> 
                                                                            : null
                                                                    }
                                                            </div>
                                                        </StepContent>
                                                </Step>
                                            )
                                        }
                                </Stepper>
                                {
                                    activeStep === steps.length && (
                                        <Paper
                                            square
                                            elevation={ 0 }
                                            className={ classes.resetContainer }
                                            style={{ textAlign: 'center' }}>
                                                <BackgroundFinish 
                                                    data={[{
                                                        client: {
                                                            relationTypeId: stateStepClientConsult?.relationTypeId || 0,
                                                            fullName: `${ stateStepClientInformation?.personalInformation?.firstLastName || '' } ` +
                                                                `${ stateStepClientInformation?.personalInformation?.secondLastName || '' } ` + 
                                                                `${ stateStepClientInformation?.personalInformation?.firstName || '' } ` + 
                                                                `${ stateStepClientInformation?.personalInformation?.secondName || '' }`
                                                        },
                                                        process: {
                                                            number: stateStepClientValidated?.solicitudeCode || 0,
                                                            fullNumber: stateStepClientValidated?.completeSolicitudeCode || '',
                                                            typeSolicitudeCode: stateStepClientConsult?.typeSolicitudeCode || 0,
                                                            typeSolicitudeOriginationCode: stateStepClientConsult?.typeSolicitudeOriginationCode || 0
                                                        }
                                                    }]}
                                                    isClientExistingProcess />
                                                <br />
                                                <Button
                                                    variant='contained'
                                                    color='primary'
                                                    className={ classes.button }
                                                    onClick={ handleReset }>
                                                    Nueva Originación
                                                </Button>
                                        </Paper>
                                    )
                                }
                                <ModalCancelOrigination
                                    open={ modalCancelActivity }
                                    close={ _ => setModalCancelActivity(false) }
                                    data={ dataCancelActivity }
                                    handleReset={ handleReset } />
                        </div>
                </Hotkeys>
        </SnackbarProvider>
    )
}

function mapStateToProps(state) {
    return {
        verifyEmail: state.verifyEmailReducer,
        odcExistingClient: state.odcExistingClientReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        continueProcess: bindActionCreators(ActionOdcExistingClient.continueProcess, dispatch),
        registerActivity: bindActionCreators(ActionOdcExistingClient.registerActivity, dispatch),
        getConstantOdc: bindActionCreators(ActionConstantOdc.getConstantODC, dispatch)
    }
}

export default withRouter(withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(ExistingClientPage)))