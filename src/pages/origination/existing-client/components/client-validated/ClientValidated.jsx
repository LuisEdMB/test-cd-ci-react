// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, Tooltip, Typography, withStyles } from '@material-ui/core'

// Material UI - Utils
import green from '@material-ui/core/colors/green'

// Material UI - Icons
import CancelIcon from '@material-ui/icons/Cancel'
import MoneyIcon from '@material-ui/icons/Money'
import RemoveIcon from '@material-ui/icons/Remove'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcExistingClient from '../../../../../actions/odc-existing-client/odc-existing-client'

// Effects
import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import Bounce from 'react-reveal/Bounce'

// Utils
import classNames from 'classnames'
import * as Utils from '../../../../../utils/Utils'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Components
import ActionButton from '../../../../../components/ActionButton'
import ItemClientValidated from './components/ItemClientValidated'

const styles = {
    button: {
        textTransform: 'none'
    },
    buttonGreen: {
        backgroundColor: green[500],
        color: 'white',
        '&:hover': {
            backgroundColor: green[600]
        },
    },
    wrapper: {
        position: 'relative'
    },
    reason: {
        minHeight: 90,
        backgroundColor: 'white'
    }
}

function ClientValidated(props) {
    const { classes, showModalCancelActivity, stateStepClientConsult, odcExistingClient, registerActivity,
        handleSaveStateStepClientValidated, handleNextStep, enqueueSnackbar, constantOdc } = props

    const prevOdcExistingClient = useRef(odcExistingClient)

    const [ loading, setLoading ] = useState(false)
    const [ showContinueProcessButton, setShowContinueProcessButton ] = useState(false)
    const [ solicitudeOrigination, setSolicitudeOrigination ] = useState({
        clientCode: 0,
        solicitudeCode: 0,
        completeSolicitudeCode: '',
        cencosudCash: 0,
        lineAvailable: 0,
        minLineAvailableCEC: 0,
        PIB: 0,
        PEP: 0,
        pct: 0,
        paymentDateId: 0,
        pmpClientNumber: '',
        pmpAccountNumber: '',
        pmpRelationNumber: '',
        pmpCardNumber: '',
        validations: []
    })

    useEffect(_ => {
        const { cod_cliente, cod_solicitud, cod_solicitud_completa, validacionesTotales, lineaParelela, adn } = odcExistingClient.offerConsulted.data
        const { PEP, PIB } = Utils.evaluatePrevaluated({ cda: {}, adn: adn, validations: validacionesTotales })
        const minCEC = constantOdc.data.find(item => item.des_abv_constante === 'MONTO_MIN_SAE')
        setSolicitudeOrigination({
            clientCode: cod_cliente,
            solicitudeCode: cod_solicitud,
            completeSolicitudeCode: cod_solicitud_completa,
            cencosudCash: lineaParelela?.lp_lin_cred_final || 0,
            lineAvailable: lineaParelela?.lp_lin_cred_final || 0,
            minLineAvailableCEC: minCEC?.valor_numerico || 0,
            pct: lineaParelela?.lp_pct || 0,
            PIB: PIB,
            PEP: PEP,
            paymentDateId: lineaParelela?.lp_cod_fec_pago || 0,
            pmpClientNumber: lineaParelela?.lp_num_cli_pmp || '',
            pmpAccountNumber: lineaParelela?.lp_num_cue_pmp || '',
            pmpRelationNumber: lineaParelela?.lp_num_rel_pmp || '',
            pmpCardNumber: lineaParelela?.lp_num_tar_pmp || '',
            validations: validacionesTotales
        })
        setShowContinueProcessButton(validacionesTotales?.find(validation => 
            validation.des_error_message_servicio && validation.des_error_message_servicio.includes('Validación CONFORME')) &&
            (lineaParelela?.lp_lin_cred_final || -1) >= (minCEC?.valor_numerico || 0))
    }, [])

    useEffect(_ => {
        Utils.resolveStateRedux(prevOdcExistingClient.current.activityRegistered, odcExistingClient.activityRegistered, 
            _ => setLoading(true), 
            _ => {
                Notistack.getNotistack('Consulta Correcta, 2do Paso Ok!', enqueueSnackbar, 'success')
                handleSaveStateStepClientValidated(solicitudeOrigination)
                setTimeout(_ => {
                    handleNextStep()
                }, 1500)
            }, warningMessage => { 
                setLoading(false)
                Notistack.getNotistack(warningMessage, enqueueSnackbar, 'warning')
            }, errorMessage => { 
                setLoading(false)
                Notistack.getNotistack(errorMessage, enqueueSnackbar, 'error')
            }
        )
    }, [odcExistingClient.activityRegistered])

    const handleContinueProcess = _ => {
        const activity = {
            solicitudeCode: solicitudeOrigination.solicitudeCode,
            completeSolicitudeCode: solicitudeOrigination.completeSolicitudeCode,
            currentlyPhaseCode: 100102,
            previousPhaseCode: 100101,
            activityName: 'Validación: Aceptado'
        }
        registerActivity(activity)
    }

    const handleCancelProcess = _ => {
        const activity = {
            solicitudeCode: solicitudeOrigination.solicitudeCode,
            completeSolicitudeCode: solicitudeOrigination.completeSolicitudeCode,
            currentlyPhaseCode: 100103,
            previousPhaseCode: 100101,
            activityName: 'Validación: Cancelado'
        }
        showModalCancelActivity(activity)
    }

    return (
        <div 
            className='mb-3'>
                <Grid
                    container
                    spacing={ 8 }
                    className='mb-2 border border-light-gray rounded'>
                        <Grid
                            item
                            xs={ 12 }
                            className='bg-lightgray p-1'>
                                <Fade>
                                    <Typography
                                        className='p-sm-0 text-center text-sm-left text-uppercase'>
                                            <Typography
                                                component='span'
                                                align='center'
                                                color='inherit'>
                                                    Resultado de la Validación
                                            </Typography>
                                    </Typography>
                                </Fade>
                        </Grid>
                        <Grid
                            item
                            xs={ 12 }
                            className='p-2 p-md-5'>
                                <Grid
                                    container
                                    spacing={ 8 }
                                    className='d-flex justify-content-center'>
                                        <Grid
                                            item
                                            xs={ 12 }
                                            lg={ 6 }>
                                                <Grid
                                                    container
                                                    spacing={ 8 }>
                                                        <Grid
                                                            item
                                                            xs={ 12 }>
                                                                <ItemClientValidated
                                                                    text='Nro. Solicitud'
                                                                    value={ solicitudeOrigination.completeSolicitudeCode } />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }>
                                                                <ItemClientValidated
                                                                    text='Documento'
                                                                    value={ stateStepClientConsult.documentNumber } />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }>
                                                                <ItemClientValidated
                                                                    text='Políticas Internas del Banco'
                                                                    value={ solicitudeOrigination.PIB }
                                                                    isLight />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }>
                                                                <ItemClientValidated
                                                                    text='Personas Expuestas Políticamente'
                                                                    value={ solicitudeOrigination.PEP }
                                                                    isLight />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }>
                                                                <ItemClientValidated
                                                                    text={ <>Efectivo Cencosud (S/) <b><i>(Min: { Utils.getMoneyFormat(solicitudeOrigination.minLineAvailableCEC) })</i></b></> }
                                                                    value={ `S/ ${Utils.getMoneyFormat(solicitudeOrigination.cencosudCash)}` } />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }>
                                                                <ItemClientValidated
                                                                    text='Línea Disponible (S/)'
                                                                    value={ `S/ ${Utils.getMoneyFormat(solicitudeOrigination.lineAvailable)}` }/>
                                                        </Grid>
                                                </Grid>
                                        </Grid>

                                </Grid>
                                <Grid
                                    container
                                    spacing={ 8 }
                                    className='d-flex justify-content-center'>
                                        <Grid
                                            item
                                            xs={ 12 }
                                            lg={ 6 }>
                                                <Fade>
                                                    <div
                                                        className='mb-2'>
                                                            <Typography
                                                                align='left'>
                                                                    <Typography 
                                                                        component='span'>
                                                                            <strong>
                                                                                Motivo:
                                                                            </strong>
                                                                        </Typography>
                                                            </Typography>
                                                    </div>
                                                    <div
                                                        className={ classNames(classes.reason, 'p-2 border border-light-gray rounded') }>
                                                            {
                                                                solicitudeOrigination.validations.filter(validation => 
                                                                    validation.des_error_message_servicio && 
                                                                    validation.des_error_message_servicio !== 'Cliente no encontrado-').map((validation, index) => 
                                                                    <Typography
                                                                        align='left'
                                                                        color='secondary'
                                                                        key={ index }>
                                                                            <Typography  
                                                                                color={ validation.des_error_message_servicio.includes('Validación CONFORME')
                                                                                    ? 'primary'
                                                                                    : 'secondary' }
                                                                                component='span'
                                                                                className='d-flex align-items-start align-items-sm-center'>
                                                                                    <RemoveIcon
                                                                                        fontSize='small'
                                                                                        color='inherit'/>
                                                                                    {
                                                                                        validation.des_error_message_servicio
                                                                                    }
                                                                            </Typography>
                                                                    </Typography>
                                                                )
                                                            }
                                                    </div>
                                                </Fade>
                                        </Grid>
                                </Grid>
                        </Grid>
                </Grid>
                <Grid
                    container
                    spacing={ 8 }
                    className='d-flex justify-content-center align-items-center'>
                        <>
                            <Grid
                                item
                                xs={ 12 }
                                sm={ 6 }
                                md={ 6 }
                                lg={ 3 }>
                                    <Tooltip
                                        TransitionComponent={ Zoom }
                                        title='Cancelar originación'>
                                            <div>
                                                <Bounce 
                                                    left>
                                                        <div
                                                            className={ classes.wrapper }>
                                                                <ActionButton
                                                                    text='Cancelar'
                                                                    loading={ loading }
                                                                    type='secondary'
                                                                    handleAction={ () => handleCancelProcess() }
                                                                    icon={
                                                                        <CancelIcon 
                                                                            fontSize='small' 
                                                                            className='ml-2' />
                                                                    }/>
                                                        </div>
                                                </Bounce>
                                            </div>
                                    </Tooltip>
                            </Grid>
                            {
                                showContinueProcessButton && 
                                    <Grid
                                        item
                                        xs={ 12 }
                                        sm={ 6 }
                                        md={ 6 }
                                        lg={ 3 }>
                                            <Tooltip
                                                TransitionComponent={ Zoom }
                                                title='Continuar con el proceso de originación de un producto LD'>
                                                    <div>
                                                        <Bounce 
                                                            right>
                                                                <div
                                                                    className={ classes.wrapper }>
                                                                        <ActionButton
                                                                            text='Continuar Efectivo Cencosud'
                                                                            loading={ loading }
                                                                            type='secondary'
                                                                            handleAction={ () => handleContinueProcess() }
                                                                            icon={
                                                                                <MoneyIcon 
                                                                                    fontSize='small' 
                                                                                    className='ml-2' />
                                                                            }
                                                                            showLoading
                                                                            className={ classes.buttonGreen }/>
                                                                </div>
                                                        </Bounce>
                                                    </div>
                                            </Tooltip>
                                    </Grid>
                            }
                        </>
                </Grid>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        odcExistingClient: state.odcExistingClientReducer,
        constantOdc: state.constantODCReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerActivity: bindActionCreators(ActionOdcExistingClient.registerActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ClientValidated))))