// React
import React from 'react'

// Material UI
import { Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import LensIcon from '@material-ui/icons/Lens'

// Effects
import Fade from 'react-reveal/Fade'

// Utils
import classNames from 'classnames'

const styles = theme => ({
    rootItem: {
        display: 'flex', 
        flexDirection: 'column', 
        justifyContent: 'start', 
        [theme.breakpoints.down('sm')]: {
            flexDirection: 'row', 
            justifyContent: 'space-between', 
            alignItems: 'center'
        },
        [theme.breakpoints.up('md')]: {
            flexDirection: 'row', 
            justifyContent: 'space-between', 
            alignItems: 'center'
        }
    },
    valueWrapper:{
        display: 'flex', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    }
})

function ItemClientValidated(props) {
    const { text, value, isLight = false, classes } = props

    return (
        <div
            className={ classNames(classes.rootItem, 'mb-2') }>
                <Fade
                    left>
                        <Typography>
                            { text }
                        </Typography>
                </Fade>
                <Fade
                    right>
                        <Typography
                            className={ classes.valueWrapper }
                            color='textSecondary'>
                                {
                                    isLight 
                                        ? ((value === 1 && <LensIcon className="text-green" />) ||
                                            (value === 0 && <LensIcon className="text-red" />) ||
                                            (value === -1 && <LensIcon className="text-amber" />))
                                        : value
                                    
                                }
                        </Typography>        
                </Fade>
        </div>
    )
}

export default React.memo(withStyles(styles)(ItemClientValidated))