// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { FormControl, FormHelperText, FormLabel, Grid, InputLabel, MenuItem, Select, TextField, Tooltip, Typography, withStyles } from '@material-ui/core'
import Slider from '@material-ui/lab/Slider'

// Material UI - Icons
import CancelIcon from '@material-ui/icons/Cancel'
import SendIcon from '@material-ui/icons/Send'
import CreditCardIcon from '@material-ui/icons/CreditCard'
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder'

// Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Redux - Actions
import * as ActionDisbursementType from '../../../../../actions/value-list/disbursement-type'
import * as ActionFinancialEntity from '../../../../../actions/generic/entidad-financiera'
import * as ActionOdcExistingClient from '../../../../../actions/odc-existing-client/odc-existing-client'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Components
import Image from '../../../../../components/Image'
import ActionButton from '../../../../../components/ActionButton'

// Media
import LogoCencosudCash from '../../../../../assets/media/images/jpg/CEC-logo-1.jpg'

// Effects
import Zoom from 'react-reveal/Zoom'
import Fade from 'react-reveal/Fade'
import Bounce from 'react-reveal/Bounce'

// Utils
import * as Utils from '../../../../../utils/Utils'
import * as Validations from '../../../../../utils/Validations'
import { valueReducer } from '../../../../../utils/ValueReducer'
import classNames from 'classnames'

const styles = {
    root: {
        maxWidth: 900
    },
    wrapper: {
        position: 'relative'
    },
    buttonActions: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '40px'
    },
    labelError: {
        display: 'flex',
        justifyContent: 'space-between', 
        alignItems: 'center'
    }
}

function CommercialOffer(props) {
    const { classes, stateStepClientConsult, stateStepClientValidated, stateStepClientInformation, disbursementType,
        enqueueSnackbar, getDisbursementType, constantOdc, getFinancialEntity, financialEntity,
        showModalCancelActivity, registerCommercialOffer, odcExistingClient, handleSaveStateStepCommercialOffer,
        handleNextStep, callCenter } = props
    
    const prevOdcExistingClient = useRef(odcExistingClient)
    const prevFinancialEntity = useRef(financialEntity)
    const prevDisbursementType = useRef(disbursementType)
    
    const [ loading, setLoading ] = useState(false)
    const [ commercialOffer, setCommercialOffer ] = useState({
        disbursementTypeId: 0,
        lineAvailable: {
            min: 0,
            value: 0,
            max: 0
        },
        cci: '',
        bankName: '',
        numberFees: '',
        numberMinFees: 0,
        numberMaxFees: 0,
        feeAmount: '',
        tcea: ''
    })
    const [ validations, setValidations ] = useState({
        disbursementTypeId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        cci: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        bankName: {
            message: '',
            error: false,
            extraError: true,
            ref: useRef(null)
        },
        numberFees: {
            message: '',
            error: false,
            extra: null,
            ref: useRef(null)
        },
        feeAmount: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        tcea: {
            message: '',
            error: false,
            ref: useRef(null)
        }
    })

    useEffect(_ => {
        getDisbursementType()
        const minCEC = constantOdc?.data?.find(item => item.des_abv_constante === 'MONTO_MIN_SAE')
        const maxFees = constantOdc?.data?.find(item => item.des_abv_constante === 'NRO_MAX_CUOTAS')
        const minFees = constantOdc?.data?.find(item => item.des_abv_constante === 'NRO_MIN_CUOTAS')
        const valueMaxFees = maxFees?.valor_numerico || 0
        const valueMinFees = minFees?.valor_numerico || 0
        setCommercialOffer(state => ({
            ...state,
            lineAvailable: {
                min: minCEC?.valor_numerico || 0,
                value: stateStepClientValidated?.lineAvailable || 0,
                max: stateStepClientValidated?.lineAvailable || 0
            },
            numberMaxFees: valueMaxFees,
            numberMinFees: valueMinFees,
        }))
        setValidations(state => ({
            ...state,
            numberFees: {
                ...state.numberFees,
                extra: [{
                    regex: value => value >= valueMinFees && value <= valueMaxFees,
                    message: `El número de cuotas debe de ser mayor o igual a ${ valueMinFees } ` +
                        `y menor o igual a ${ valueMaxFees }.`,
                    isRegex: false
                }]
            }
        }))
    }, [])

    useEffect(_ => {
        Utils.resolveStateRedux(prevDisbursementType.current, disbursementType, _ => null, _ => { 
            const dataDisbursementType = disbursementType.data.find(item => item.val_orden === 1)
            setCommercialOffer(state => ({ ...state, disbursementTypeId: dataDisbursementType?.cod_valor || 0 }))
         }, warningMessage => Notistack.getNotistack(warningMessage, enqueueSnackbar, 'warning'), 
            errorMessage => Notistack.getNotistack(errorMessage, enqueueSnackbar, 'error')
        )
    }, [disbursementType])

    useEffect(_ => {
        Utils.resolveStateRedux(prevFinancialEntity.current, financialEntity, null, _ => {
            setCommercialOffer(state => ({
                ...state,
                bankName: `${ financialEntity.data.des_entidad_financiera_detalle || '' } ` + 
                    `${ financialEntity.data.des_abv_entidad || '' }`
            }))
            setValidations(state => ({
                ...state,
                bankName: {
                    ...state.bankName,
                    error: !financialEntity.data.flg_afiliado,
                    message: financialEntity.data.flg_afiliado 
                        ? 'Banco válido para el desembolso de EC' 
                        : 'Banco no válido para el desembolso de EC'
                }
            }))
        },
        _ => {
            setCommercialOffer(state => ({
                ...state,
                bankName: ''
            }))
            setValidations(state => ({
                ...state,
                bankName: {
                    ...state.bankName,
                    error: true,
                    message: 'Banco no válido para el desembolso de EC'
                }
            }))
        },
        _ => {
            setCommercialOffer(state => ({
                ...state,
                bankName: ''
            }))
            setValidations(state => ({
                ...state,
                bankName: {
                    ...state.bankName,
                    error: true,
                    message: 'Banco no válido para el desembolso de EC'
                }
            }))
        })
    }, [financialEntity])

    useEffect(_ => {
        Utils.resolveStateRedux(prevOdcExistingClient.current.commercialOfferRegistered, odcExistingClient.commercialOfferRegistered, 
            _ => setLoading(true), 
            _ => {
                Notistack.getNotistack('Consulta Correcta, 4to Paso Ok!', enqueueSnackbar, 'success')
                setTimeout(_ => {
                    handleNextStep()
                }, 1500)
            }, warningMessage => {
                setLoading(false)
                Notistack.getNotistack(warningMessage, enqueueSnackbar, 'warning')
            }, errorMessage => {
                setLoading(false)
                Notistack.getNotistack(errorMessage, enqueueSnackbar, 'error')
            }
        )
    }, [odcExistingClient.commercialOfferRegistered])

    const handleChangeState = e => {
        const { name, value, call = _ => null } = e.target || e
        setCommercialOffer(state => ({
            ...state,
            [name]: value
        }))
        setValidations(state => {
            let validations = { ...state }
            if (Utils.findProperty(validations, name, ['ref', 'extra']).length > 0) {
                let validationsOptional = { }
                if ((name === 'disbursementTypeId' && value === 380002) || commercialOffer.disbursementTypeId === 380002) {
                    setCommercialOffer(state => ({ ...state, cci: '', bankName: '' }))
                    validationsOptional = {
                        cci: { error: false, message: ''},
                        bankName: { error: false, message: ''}
                    }
                }
                const propertyValidated = state[name].extraError && state[name].error && state[name].message 
                    ? { } : Validations.validateByField(name, value, true, state[name].extra)
                validations = {
                    ...state,
                    [name]: {
                        ...state[name],
                        ...propertyValidated
                    },
                    ...validationsOptional,
                }
            }
            call(validations)
            return validations
        })
    }

    const handleCancelProcess = _ => {
        const activity = {
            solicitudeCode: stateStepClientValidated.solicitudeCode,
            completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
            currentlyPhaseCode: 100303,
            previousPhaseCode: 100301,
            activityName: 'Oferta: Cancelado'
        }
        showModalCancelActivity(activity)
    }

    const handleContinueProcess = async _ => {
        if (await Validations.comprobeAllValidationsSuccess(commercialOffer, handleChangeState)) {
            const data = {
                ...stateStepClientConsult,
                ...stateStepClientValidated,
                commercialOffer: commercialOffer,
                currentlyPhaseCode: 100302,
                previousPhaseCode: 100301,
                activityName: 'Oferta: Aceptado'
            }
            handleSaveStateStepCommercialOffer(commercialOffer)
            registerCommercialOffer(data)
        }
    }

    return (
        <Grid
            container
            spacing={ 8 }
            className='w-100 p-3'>
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 12 }>
                                <Grid
                                    container
                                    spacing={32}
                                    className="d-flex justify-content-center p-2 p-sm-5 mb-2 rounded">
                                        <Grid
                                            container
                                            spacing={ 32 }
                                            className={ classNames(classes.root, "m-0 m-sm-3") }>
                                                <Grid
                                                    container
                                                    spacing={ 8 }>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 12 }
                                                            md={ 3 }
                                                            lg={ 3 }>
                                                                <TextField
                                                                    fullWidth
                                                                    required
                                                                    label='Tipo Documento'
                                                                    type='text'
                                                                    margin='normal'
                                                                    color='default'
                                                                    value={ stateStepClientConsult?.documentTypeLetter || '' }
                                                                    InputProps={{
                                                                        readOnly: true
                                                                    }}
                                                                />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 12 }
                                                            md={ 3 }
                                                            lg={ 3 }>
                                                                <TextField
                                                                    fullWidth
                                                                    required
                                                                    label='Número Documento'
                                                                    type='text'
                                                                    margin='normal'
                                                                    color='default'
                                                                    value={ stateStepClientConsult?.documentNumber || '' }
                                                                    InputProps={{
                                                                        readOnly: true
                                                                    }}
                                                                />
                                                        </Grid>
                                                        <Grid 
                                                            item
                                                            xs={ 12 }
                                                            sm={ 12 }
                                                            md={ 6 }
                                                            lg={ 6 }>
                                                            <TextField
                                                                fullWidth
                                                                required
                                                                label='Nombre Completo'
                                                                type='text'
                                                                margin='normal'
                                                                color='default'
                                                                value={ `${ stateStepClientInformation?.personalInformation?.firstLastName || '' } ` +
                                                                    `${ stateStepClientInformation?.personalInformation?.secondLastName || '' } ` + 
                                                                    `${ stateStepClientInformation?.personalInformation?.firstName || '' } ` + 
                                                                    `${ stateStepClientInformation?.personalInformation?.secondName || '' }` }
                                                                InputProps={{
                                                                    readOnly: true
                                                                }}
                                                            />
                                                        </Grid>
                                                </Grid>
                                                <Grid
                                                    container
                                                    spacing={ 8 }>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 12 }
                                                            md={ 6 }
                                                            lg={ 6 }>
                                                                <Fade>
                                                                    <Image
                                                                        src={ LogoCencosudCash }
                                                                        alt='LogoCencosudCash'/>
                                                                </Fade>
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 12 }
                                                            md={ 6 }
                                                            lg={ 6 }>
                                                                <Grid
                                                                    container
                                                                    spacing={ 8 }>
                                                                        <Grid 
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 6 }>
                                                                                <FormControl
                                                                                    required
                                                                                    margin='normal'
                                                                                    disabled={ disbursementType.loading }
                                                                                    error={ validations.disbursementTypeId.error }
                                                                                    fullWidth>
                                                                                        <InputLabel>
                                                                                            Tipo Desembolso
                                                                                        </InputLabel>
                                                                                        <Select
                                                                                            name='disbursementTypeId'
                                                                                            onChange={ handleChangeState }
                                                                                            onBlur={ handleChangeState }
                                                                                            inputRef={ validations.disbursementTypeId.ref }
                                                                                            value={ commercialOffer.disbursementTypeId }>
                                                                                                {
                                                                                                    disbursementType.data
                                                                                                        .filter(item => callCenter ? item.cod_valor === 380001 : item)
                                                                                                        .map((type, index) => 
                                                                                                            <MenuItem 
                                                                                                                key={ index }
                                                                                                                value={ type.cod_valor }>
                                                                                                                    { type.des_valor }
                                                                                                            </MenuItem>)
                                                                                                }
                                                                                        </Select>
                                                                                </FormControl>
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validations.disbursementTypeId.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validations.disbursementTypeId.error &&
                                                                                                            validations.disbursementTypeId.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid 
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 6 }>
                                                                                <TextField
                                                                                    name='cci'
                                                                                    disabled={ commercialOffer.disbursementTypeId !== 380001 }
                                                                                    fullWidth
                                                                                    label='C.C.I.'
                                                                                    type='text'
                                                                                    margin='normal'
                                                                                    color='default'
                                                                                    value={ commercialOffer.cci }
                                                                                    error={ validations.cci.error  }
                                                                                    onChange={ e => {
                                                                                        const { value } = e.target
                                                                                        if (Utils.onlyNumberRegex(value)) {
                                                                                            handleChangeState(e)
                                                                                            const prevValue = commercialOffer.cci.substring(0, 3)
                                                                                            const currentlyValue = value.substring(0, 3)
                                                                                            if (prevValue !== currentlyValue && currentlyValue.length === 3)
                                                                                                getFinancialEntity(currentlyValue)
                                                                                        }
                                                                                    } }
                                                                                    inputRef={ validations.cci.ref }
                                                                                    onPaste={ e => {
                                                                                        const { value } = e.target
                                                                                        if (Utils.onlyNumberRegex(value)) {
                                                                                            handleChangeState(e)
                                                                                            const prevValue = commercialOffer.cci.substring(0, 3)
                                                                                            const currentlyValue = value.substring(0, 3)
                                                                                            if (prevValue !== currentlyValue && currentlyValue.length === 3)
                                                                                                getFinancialEntity(currentlyValue)
                                                                                        }
                                                                                    } }
                                                                                    onBlur={ e => {
                                                                                        if (Utils.onlyNumberRegex(e.target.value))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    InputProps={{
                                                                                        inputProps: {
                                                                                            maxLength: Utils.defaultCCILengthInput
                                                                                        }
                                                                                    }}
                                                                                />
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validations.cci.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validations.cci.error &&
                                                                                                            validations.cci.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid 
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 12 }
                                                                            md={ 12 }
                                                                            lg={ 12 }>
                                                                                <TextField
                                                                                    disabled
                                                                                    fullWidth
                                                                                    name='bankName'
                                                                                    label='Nombre Banco'
                                                                                    type='text'
                                                                                    margin='normal'
                                                                                    color='default'
                                                                                    error={ validations.bankName.error }
                                                                                    value={ commercialOffer.bankName }
                                                                                    InputProps={{
                                                                                        readOnly: true
                                                                                    }}
                                                                                />
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validations.bankName.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validations.bankName.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid 
                                                                            item 
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 4 }>
                                                                                <TextField
                                                                                    fullWidth
                                                                                    label='Número de Cuotas'
                                                                                    type='number'
                                                                                    margin='normal'
                                                                                    color='default'
                                                                                    name='numberFees'
                                                                                    error={ validations.numberFees.error }
                                                                                    value={ commercialOffer.numberFees }
                                                                                    onChange={ e => {
                                                                                        if (Utils.onlyNumberRegex(e.target.value))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    onBlur={ e => {
                                                                                        if (Utils.onlyNumberRegex(e.target.value))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    InputProps={{
                                                                                        inputProps: {
                                                                                            min: 0,
                                                                                            max: commercialOffer.numberMaxFees
                                                                                        }
                                                                                    }}
                                                                                />
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validations.numberFees.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validations.numberFees.error &&
                                                                                                            validations.numberFees.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 } 
                                                                            lg={ 4 }>
                                                                                <TextField
                                                                                    fullWidth
                                                                                    label='Valor Aprox. Cuotas'
                                                                                    type='number'
                                                                                    margin='normal'
                                                                                    color='default'
                                                                                    name='feeAmount'
                                                                                    value={ commercialOffer.feeAmount }
                                                                                    error={ validations.feeAmount.error }
                                                                                    onChange={ e => {
                                                                                        if (Utils.onlyNumberWithDotRegex(e.target.value || 0))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    onBlur={ e => {
                                                                                        const { value } = e.target
                                                                                        if (Utils.onlyNumberWithDotRegex(value || 0))
                                                                                            handleChangeState({ name: 'feeAmount', value: parseFloat(value || 0).toFixed(2) })
                                                                                    } }
                                                                                    InputProps={{
                                                                                        inputProps: {
                                                                                            maxLength: Utils.defaultvalCuotasLength
                                                                                        }
                                                                                    }}
                                                                                />
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validations.feeAmount.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validations.feeAmount.error &&
                                                                                                            validations.feeAmount.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid 
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 4 }>
                                                                                <Typography
                                                                                    component='h6'
                                                                                    color='textSecondary'
                                                                                    className='d-flex align-items-center'>
                                                                                        <TextField
                                                                                            fullWidth
                                                                                            label='TCEA'
                                                                                            type='number'
                                                                                            margin='normal'
                                                                                            color='default'
                                                                                            step='.01'
                                                                                            error={ validations.tcea.error }
                                                                                            name='tcea'
                                                                                            value={ commercialOffer.tcea }
                                                                                            onChange={ e => {
                                                                                                if (Utils.onlyNumberWithDotRegex(e.target.value || 0))
                                                                                                    handleChangeState(e)
                                                                                            } }
                                                                                            onBlur={ e => {
                                                                                                const { value } = e.target
                                                                                                if (Utils.onlyNumberWithDotRegex(value || 0))
                                                                                                    handleChangeState({ name: 'tcea', value: parseFloat(value || 0).toFixed(2) })
                                                                                            } }
                                                                                        />
                                                                                </Typography>
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validations.tcea.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validations.tcea.error &&
                                                                                                            validations.tcea.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                </Grid>
                                                        </Grid>
                                                </Grid>
                                                <Grid
                                                    style={{ marginTop: '20px' }}
                                                    container
                                                    spacing={ 8 }>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 12 }
                                                            md={ 12 }
                                                            lg={ 12 }>
                                                            <Grid
                                                                item
                                                                xs={ 12 }>
                                                                    <FormLabel>Línea de Crédito Efectivo Cencosud</FormLabel>
                                                                    <Slider
                                                                        className='p-2 w-100'
                                                                        style={{ overflow: 'hidden' }}
                                                                        aria-labelledby='offert'
                                                                        step={ 100 }
                                                                        valueReducer={ valueReducer }
                                                                        value={ commercialOffer.lineAvailable.value }
                                                                        min={ commercialOffer.lineAvailable.min }
                                                                        max={ commercialOffer.lineAvailable.max }
                                                                        onChange={ (_, value) => {
                                                                            setCommercialOffer(state => ({
                                                                                ...state,
                                                                                lineAvailable: {
                                                                                    ...state.lineAvailable,
                                                                                    value: value
                                                                                }
                                                                            }))
                                                                        } }
                                                                    />
                                                                    <div
                                                                        className='d-flex justify-content-between'>
                                                                            <FormHelperText
                                                                                className='font-weight-bold'>
                                                                                    S/ { Utils.getMoneyFormat(commercialOffer.lineAvailable.min) }
                                                                            </FormHelperText>
                                                                            <div
                                                                                className='d-flex justify-content-center align-items-center'>
                                                                                <Typography
                                                                                    className='mr-2'
                                                                                    fontSize='large'>
                                                                                        S/
                                                                                </Typography>
                                                                                <TextField
                                                                                    style={{ width: 90 }}
                                                                                    required
                                                                                    variant='outlined'
                                                                                    type='text'
                                                                                    margin='normal'
                                                                                    color='default'
                                                                                    name=''
                                                                                    value={ commercialOffer.lineAvailable.value }
                                                                                    onChange={ e => {
                                                                                        const { value } = e.target
                                                                                        if (Utils.onlyNumberRegex(value || 0)) {
                                                                                            const newValue = parseInt(e.target.value || 0)
                                                                                            if (newValue <= commercialOffer.lineAvailable.max)
                                                                                                setCommercialOffer(state => ({
                                                                                                    ...state,
                                                                                                    lineAvailable: {
                                                                                                        ...state.lineAvailable,
                                                                                                        value: newValue
                                                                                                    }
                                                                                                }))
                                                                                        }
                                                                                    } }
                                                                                    onBlur={ e => {
                                                                                        const { value } = e.target
                                                                                        if (Utils.onlyNumberRegex(value || 0)) {
                                                                                            const newValue = parseInt(e.target.value || 0)
                                                                                            if (newValue < commercialOffer.lineAvailable.min)
                                                                                                setCommercialOffer(state => ({
                                                                                                    ...state,
                                                                                                    lineAvailable: {
                                                                                                        ...state.lineAvailable,
                                                                                                        value: commercialOffer.lineAvailable.min
                                                                                                    }
                                                                                                }))
                                                                                        }
                                                                                    } }
                                                                                />
                                                                            </div>
                                                                            <FormHelperText
                                                                                className='font-weight-bold'>
                                                                                    S/ { Utils.getMoneyFormat(commercialOffer.lineAvailable.max) }
                                                                            </FormHelperText>
                                                                    </div>
                                                            </Grid>
                                                            <Grid
                                                                item
                                                                xs={ 12 }>
                                                                    <Typography
                                                                        component='h6'
                                                                        color='textSecondary'
                                                                        className='d-flex align-items-center'>
                                                                            <CreditCardIcon
                                                                                fontSize='small'
                                                                                className=' mr-2' />
                                                                            <Typography
                                                                                component='span'
                                                                                color='inherit'>
                                                                                    Línea de Crédito Efectivo Cencosud={' '}
                                                                                    <strong>
                                                                                    {`S/. ${ Utils.getMoneyFormat(commercialOffer.lineAvailable.value) }`}
                                                                                    </strong>
                                                                            </Typography>
                                                                    </Typography>
                                                                    <Typography
                                                                        component='h6'
                                                                        color='textSecondary'
                                                                        className='d-flex align-items-center'>
                                                                            <BookmarkBorderIcon
                                                                                fontSize='small'
                                                                                className=' mr-2' />
                                                                            <Typography
                                                                                component='span'
                                                                                color='inherit'>
                                                                                    PCT EC: <strong>{ stateStepClientValidated?.pct || '' }</strong>
                                                                            </Typography>
                                                                    </Typography>
                                                            </Grid>
                                                        </Grid>
                                                </Grid>
                                                <Grid
                                                    container
                                                    spacing={ 8 }
                                                    className={ classes.buttonActions }>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 12 }
                                                            md={ 6 }
                                                            lg={ 6 }>
                                                                <Tooltip
                                                                    TransitionComponent={ Zoom }
                                                                    title='Cancelar originación'>
                                                                        <Bounce>
                                                                            <div className={ classes.wrapper }>
                                                                                <ActionButton
                                                                                    text='Cancelar'
                                                                                    loading={ loading }
                                                                                    type='secondary'
                                                                                    handleAction={ _ => handleCancelProcess() }
                                                                                    icon={
                                                                                        <CancelIcon
                                                                                            fontSize='small'
                                                                                            className='ml-2' />
                                                                                    }
                                                                                />
                                                                            </div>
                                                                        </Bounce>
                                                                </Tooltip>
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 12 }
                                                            md={ 6 }
                                                            lg={ 6 }>
                                                                <Tooltip
                                                                    TransitionComponent={ Zoom }
                                                                    title='Continuar con el proceso de originación'>
                                                                        <Bounce>
                                                                            <div className={ classes.wrapper }>
                                                                                <ActionButton
                                                                                        text='Continuar'
                                                                                        loading={ loading }
                                                                                        type='primary'
                                                                                        handleAction={ _ => handleContinueProcess() }
                                                                                        icon={
                                                                                            <SendIcon
                                                                                                fontSize='small'
                                                                                                className='ml-2' />
                                                                                        }
                                                                                        showLoading
                                                                                />
                                                                            </div>
                                                                        </Bounce>
                                                                </Tooltip>
                                                        </Grid>
                                                </Grid>
                                        </Grid>
                                </Grid>
                        </Grid>
                </Grid>          
        </Grid>
    )
}

function mapStateToProps(state) {
    return {
        disbursementType: state.disbursementTypeReducer,
        constantOdc: state.constantODCReducer,
        financialEntity: state.financialEntityReducer,
        odcExistingClient: state.odcExistingClientReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getDisbursementType: bindActionCreators(ActionDisbursementType.getDisbursementType, dispatch),
        getFinancialEntity: bindActionCreators(ActionFinancialEntity.getEntidadFinanciera, dispatch),
        registerCommercialOffer: bindActionCreators(ActionOdcExistingClient.registerCommercialOffer, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(CommercialOffer))))