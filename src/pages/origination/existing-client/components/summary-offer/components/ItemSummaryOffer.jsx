// React
import React from 'react'

// Material UI
import { Typography, withStyles } from '@material-ui/core'

// Utils
import classNames from 'classnames'

const styles = {
    itemContainer: {
        display: 'flex', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    titleItem: {
        display:'flex', 
        alignItems:'center'
    },
    valueItem: {
        display: 'flex', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    }
}

function ItemSummaryOffer(props) {
    const { classes, title, icon, value } = props

    return (
        <li>
            <Typography
                className={ classNames(classes.itemContainer, 'mb-2') }>
                    <Typography
                        component='span' 
                        className={classes.titleItem}>
                            <Typography
                                component='span'
                                className='mt-1 mr-2'>
                                    { icon }
                            </Typography>
                            <Typography 
                                component='span' >
                                    { title }
                            </Typography>
                    </Typography>
                    <Typography
                        component='span'
                        className={ classes.valueItem }
                        color='textSecondary'>
                            { value }
                    </Typography>
            </Typography>
        </li>
    )
}

export default React.memo(withStyles(styles)(ItemSummaryOffer))