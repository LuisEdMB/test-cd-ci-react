// React
import React, { useEffect, useRef, useState } from 'react'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcExitingClient from '../../../../../actions/odc-existing-client/odc-existing-client'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Material UI
import { Divider, Grid, Tooltip, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import LabelIcon from '@material-ui/icons/Label'
import MoneyIcon from '@material-ui/icons/Money'
import SendIcon from '@material-ui/icons/Send'

// Utils
import classNames from 'classnames'
import * as Utils from '../../../../../utils/Utils'

// Effects
import Fade from 'react-reveal/Fade'
import Bounce from 'react-reveal/Bounce'
import Zoom from 'react-reveal/Zoom'
import * as Fireworks from 'fireworks-canvas'

// Components
import Image from '../../../../../components/Image'
import ActionButton from '../../../../../components/ActionButton'
import ItemSummaryOffer from './components/ItemSummaryOffer'

// Media
import LogoCencosud from '../../../../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg'

const styles = theme => ({
    root: {
        position: 'relative'
    },
    firework:{
        width: '100%',
        height: 500,
        position: 'absolute',
        top: 20
    },
    summaryWrapper:{
        display: 'flex', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    summary:{
        minWidth: 300,
        maxWidth: 700
    },
    titleContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatarTitleWrapper: {
        [theme.breakpoints.down('sm')]: {
            display: 'none'
        },
    },
    avatarTitle: {
        margin: '0 .5em',
        height: 36, 
        width: 64,
        [theme.breakpoints.down('sm')]: {

        },
        [theme.breakpoints.up('md')]: {
            height: 48, 
            width: 80
        },
        [theme.breakpoints.up('lg')]: {

        }
    },
    titleWrapper: {
        margin: '0 1em',
    },
    title: {
        [theme.breakpoints.down('sm')]: {
            fontSize: 18
        }
    },
    dateWrapper: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
    },
    headerContainer: {
        display: 'flex', 
        flexDirection: 'column',
        [theme.breakpoints.up('md')]: {
            flexDirection: 'row',
            justifyContent: 'space-between'
        }
    },
    bodyContainer: {
        padding: '.5em',
        border: '1px solid #c4c4c4',
        borderRadius: '.25rem',
        [theme.breakpoints.up('md')]: {
            padding: '1.5em',
        }
    },
    buttonActions: {
        display: 'flex', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    buttonProgressWrapper: {
        position: 'relative'
    }
})

function SummaryOffer(props) {
    const { classes, stateStepClientConsult, stateStepClientValidated, stateStepClientInformation,
        stateStepCommercialOffer, odcExistingClient, registerSummaryActivity, enqueueSnackbar,
        handleNextStep } = props

    const prevOdcExistingClient = useRef(odcExistingClient)

    const [ loading, setLoading ] = useState(false)

    useEffect(_ => {
        const container = document.getElementById('summary-container')
        const options = {
            maxRockets: 3,
            rocketSpawnInterval: 10,
            numParticles: 70,
            explosionMinHeight: 0.2,
            explosionMaxHeight: 0.9,
            explosionChance: 0.08
        }
        const fireworks = new Fireworks(container, options)
        fireworks.start()
        fireworks.fire()
    }, [])

    useEffect(_ => {
        Utils.resolveStateRedux(prevOdcExistingClient.current.summaryActivityRegistered, odcExistingClient.summaryActivityRegistered, 
            _ => setLoading(true),
            _ => {
                Notistack.getNotistack('Consulta Correcta, 6to Paso Ok!', enqueueSnackbar, 'success')
                setTimeout(_ => {
                    handleNextStep()
                }, 1500)
            }, warningMessage => {
                setLoading(false)
                Notistack.getNotistack(warningMessage, enqueueSnackbar, 'warning')
            }, errorMessage => {
                setLoading(false)
                Notistack.getNotistack(errorMessage, enqueueSnackbar, 'warning')
            })
    }, [odcExistingClient.summaryActivityRegistered])

    const handleContinueProcess = _ => {
        const activity = {
            ...stateStepClientValidated,
            commercialOffer: stateStepCommercialOffer,
            currentlyPhaseCode: 100602,
            previousPhaseCode: 100601,
            activityName: 'Resumen: Aceptado'
        }
        registerSummaryActivity(activity)
    }

    return (
        <Grid
            container
            spacing={ 8 }
            className={ classes.root }>
                <Grid
                    id='summary-container'
                    item
                    xs={ 12 }
                    className={ classes.firework } />
                <Grid 
                    item 
                    xs={ 12 }
                    className={ classes.summaryWrapper }>
                        <Grid
                            container
                            spacing={ 8 }
                            className={ classNames(classes.summary, 'mb-4') }>
                                <Grid
                                    item
                                    xs={ 12 }>
                                        <Fade>
                                            <figure
                                                className={ classNames(classes.titleContainer, 'p-3') }>
                                                    <div
                                                        className={ classes.avatarTitleWrapper }>
                                                            <Bounce>
                                                                <Image
                                                                    className={ classes.avatarTitle }
                                                                    src={ LogoCencosud }
                                                                    minHeight={ 48 }
                                                                    alt ='Cencosud' />
                                                            </Bounce>
                                                    </div>
                                                    <div
                                                        className={ classes.titleWrapper }>
                                                            <Typography 
                                                                align='center' 
                                                                component='title' 
                                                                className={ classes.title }
                                                                variant='h6'>
                                                                ¡Generación de Crédito Efectivo Cencosud Exitosa!
                                                            </Typography>
                                                    </div>
                                                    <div
                                                        className={ classes.dateWrapper }>
                                                            <Typography
                                                                component='span'
                                                                className='mt-1 mr-2'>
                                                                    <CalendarTodayIcon
                                                                        fontSize='small'/>
                                                            </Typography>
                                                            <Typography
                                                                component='span'>
                                                                { Utils.getDateCurrent(1, false, true).replace('T', ' ') }
                                                            </Typography>
                                                    </div>
                                            </figure>
                                        </Fade>
                                </Grid>
                                <Grid
                                    item
                                    xs={ 12 }
                                    className='mb-2'>
                                        <Divider />
                                </Grid>
                                <Grid
                                    item
                                    xs={ 12 }
                                    className='py-3'>
                                        <div
                                            className={ classes.headerContainer }>
                                                <Typography
                                                    className='font-weight-bold mb-2'
                                                    color='textSecondary'>
                                                        { `${ stateStepClientConsult.documentTypeLetter } - ${ stateStepClientConsult.documentNumber }`.toUpperCase() }
                                                </Typography>
                                                <Typography
                                                    className='font-weight-bold'
                                                    color='textSecondary'>
                                                        { `${ stateStepClientInformation.personalInformation.firstLastName } ` +
                                                            `${ stateStepClientInformation.personalInformation.secondLastName } ` + 
                                                            `${ stateStepClientInformation.personalInformation.firstName } ` + 
                                                            `${ stateStepClientInformation.personalInformation.secondName }` }
                                                </Typography>                                  
                                        </div>
                                </Grid>
                                <Grid
                                    item
                                    xs={ 12 }>
                                        <ul
                                            className={ classes.bodyContainer }>
                                                <ItemSummaryOffer
                                                    title='Nro. Solicitud'
                                                    icon={
                                                        <LabelIcon
                                                            fontSize='small'/>
                                                    }
                                                    value={ stateStepClientValidated.completeSolicitudeCode }
                                                />
                                                <ItemSummaryOffer
                                                    title='Línea de Crédito Efectivo Cencosud'
                                                    icon={
                                                        <MoneyIcon
                                                            fontSize='small' />
                                                    }
                                                    value={ `S/ ${ Utils.getMoneyFormat(stateStepCommercialOffer.lineAvailable.value) }` }
                                                />
                                                <ItemSummaryOffer
                                                    title='Número de Cuotas'
                                                    icon={
                                                        <MoneyIcon
                                                            fontSize='small' />
                                                    }
                                                    value={ stateStepCommercialOffer.numberFees }
                                                />
                                                <ItemSummaryOffer
                                                    title='Valor Cuota'
                                                    icon={
                                                        <MoneyIcon
                                                            fontSize='small' />
                                                    }
                                                    value={ `${ parseFloat(stateStepCommercialOffer.feeAmount).toFixed(2) } ` }
                                                />
                                                <ItemSummaryOffer
                                                    title='TCEA'
                                                    icon={
                                                        <LabelIcon
                                                            fontSize='small'/>
                                                    }
                                                    value={ `${ parseFloat(stateStepCommercialOffer.tcea).toFixed(2) }%` }
                                                />
                                        </ul>
                                </Grid>
                        </Grid>  
                </Grid>
                <Grid
                    item
                    xs={ 12 }
                    className={ classes.buttonActions }>
                        <Grid
                            container
                            spacing={ 8 }>
                                <Grid
                                    item
                                    xs={ 12 }
                                    sm={ 12 }
                                    md={ 4 }
                                    lg={ 3 }
                                    style={{ margin: '0 auto' }}>
                                        <Tooltip
                                            TransitionComponent={ Zoom }
                                            title='Click para continuar con el proceso de originación'>
                                                <Bounce>
                                                    <div className={ classes.buttonProgressWrapper }>
                                                        <ActionButton
                                                            text='Continuar'
                                                            loading={ loading }
                                                            type='primary'
                                                            handleAction={ () => handleContinueProcess() }
                                                            icon={
                                                                <SendIcon
                                                                    fontSize='small'
                                                                    className='ml-2' />
                                                            }
                                                            showLoading
                                                        />
                                                    </div>
                                                </Bounce>
                                        </Tooltip>
                                </Grid>
                        </Grid>
                </Grid>
        </Grid>
    )
}

function mapStateToProps(state) {
    return {
        odcExistingClient: state.odcExistingClientReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerSummaryActivity: bindActionCreators(ActionOdcExitingClient.registerSummaryActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(SummaryOffer))))