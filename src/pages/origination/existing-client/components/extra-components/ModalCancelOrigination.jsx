// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcExistingClient from '../../../../../actions/odc-existing-client/odc-existing-client'

// Utils
import * as Utils from '../../../../../utils/Utils'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Components
import Modal from '../../../../../components/Modal'
import ActionButton from '../../../../../components/ActionButton'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalCancelOrigination(props) {
    const { classes, open = false, close, data, odcExistingClient, cancelActivity, enqueueSnackbar, handleReset } = props

    const prevOdcExistingClient = useRef(odcExistingClient)

    const [ loading, setLoading ] = useState(false)

    useEffect(_ => {
        Utils.resolveStateRedux(prevOdcExistingClient.current.activityCanceled, odcExistingClient.activityCanceled, 
            _ => setLoading(true), 
            _ => {
                handleReset()
                setLoading(false)
            },
            warningMessage => { 
                setLoading(false)
                Notistack.getNotistack(warningMessage, enqueueSnackbar, 'warning') 
            },
            errorMessage => { 
                setLoading(false)
                Notistack.getNotistack(errorMessage, enqueueSnackbar, 'error') 
            }
        )
    }, [odcExistingClient.activityCanceled])

    const handleRegisterCancelActivity = _ => {
        cancelActivity(data)
    }

    return (
        <Modal
            title='Cancelar Proceso de Originación'
            body= { 
                <Typography
                    align='center'>
                        ¿Está seguro de cancelar la originación?
                </Typography>
            }
            open={ open } 
            actions={ 
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        loading={ loading }
                                        text='No'
                                        type='secondary'
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        loading={ loading }
                                        text='Sí'
                                        type='primary'
                                        handleAction={ _ => handleRegisterCancelActivity() }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
        }/>
    )
}

function mapStateToProps(state) {
    return {
        odcExistingClient: state.odcExistingClientReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        cancelActivity: bindActionCreators(ActionOdcExistingClient.cancelActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalCancelOrigination))))