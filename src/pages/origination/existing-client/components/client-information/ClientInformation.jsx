// React
import React, { useEffect, useRef, useState } from 'react'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Material UI
import { FormControl, Grid, InputLabel, withStyles, Select, TextField, InputAdornment, FormHelperText, Popper, Grow, Paper, ClickAwayListener, MenuList, FormLabel, FormControlLabel, Checkbox, RadioGroup, Radio, MenuItem, Typography, Tooltip } from '@material-ui/core'

// Material UI - Icons
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import PlaceIcon from '@material-ui/icons/Place'
import WorkIcon from '@material-ui/icons/Work'
import BusinessIcon from '@material-ui/icons/Business'
import LocalOfferIcon from '@material-ui/icons/LocalOffer'
import HelpIcon from '@material-ui/icons/Help'
import RecentActorsIcon from '@material-ui/icons/RecentActors'
import Filter1Icon from '@material-ui/icons/Filter1'
import Filter2Icon from '@material-ui/icons/Filter2'
import PermIdentityIcon from '@material-ui/icons/PermIdentity'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import EmailIcon from '@material-ui/icons/MailOutline'
import StayPrimaryPortraitIcon from '@material-ui/icons/StayPrimaryPortrait'
import PhoneIcon from '@material-ui/icons/Phone'
import Filter9PlusIcon from '@material-ui/icons/Filter9Plus'
import HomeIcon from '@material-ui/icons/Home'
import LastPageIcon from '@material-ui/icons/LastPage'
import CropDinIcon from '@material-ui/icons/CropDin'
import TabUnselectedIcon from '@material-ui/icons/TabUnselected'
import NearMeIcon from '@material-ui/icons/NearMe'
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary'
import MoneyIcon from '@material-ui/icons/Money'
import DialpadIcon from '@material-ui/icons/Dialpad'
import CodeIcon from '@material-ui/icons/Code'
import CancelIcon from '@material-ui/icons/Cancel'
import SendIcon from '@material-ui/icons/Send'

// Utils
import Fade from 'react-reveal/Fade'
import * as Utils from '../../../../../utils/Utils'
import * as Validations from '../../../../../utils/Validations'
import * as Moment from 'moment'
import * as MasterOrigination from '../../../../../utils/origination/MasterOrigination'
import * as Constants from '../../../../../utils/Constants'

// Redux - Actions
import * as ActionDocumentType from '../../../../../actions/value-list/identification-document-type'
import * as ActionGender from '../../../../../actions/value-list/gender'
import * as ActionMaritalStatus from '../../../../../actions/value-list/marital-status'
import * as ActionCountry from '../../../../../actions/value-list/country'
import * as ActionAcademicDegree from '../../../../../actions/value-list/academic-degree'
import * as ActionDepartment from '../../../../../actions/ubigeo/department'
import * as ActionProvince from '../../../../../actions/ubigeo/province'
import * as ActionDistrict from '../../../../../actions/ubigeo/district'
import * as ActionVia from '../../../../../actions/value-list/via'
import * as ActionZone from '../../../../../actions/value-list/zone'
import * as ActionHousingType from '../../../../../actions/value-list/housing-type'
import * as ActionEmploymentSituation from '../../../../../actions/value-list/employment-situation'
import * as ActionJobTitle from '../../../../../actions/value-list/job-title'
import * as ActionEconomicActivity from '../../../../../actions/value-list/economic-activity'
import * as ActionBusiness from '../../../../../actions/generic/business'
import * as ActionPaymentDate from '../../../../../actions/value-list/payment-date'
import * as ActionSendCorrespondence from '../../../../../actions/value-list/send-correspondence'
import * as ActionAccountStatus from '../../../../../actions/value-list/account-status'
import * as ActionPromoter from '../../../../../actions/generic/promoter'
import * as ActionVerifyCellphone from '../../../../../actions/generic/verifyCellphone'
import * as ActionOdcMaster from '../../../../../actions/odc-master/odc-master'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Effects
import Zoom from 'react-reveal/Zoom'
import Bounce from 'react-reveal/Bounce'

// Components
import Autocomplete from '../../../../../components/Autocomplete'
import RegisterModalButton from '../../../mantenimiento/empresas/components/register/RegisterModalButton'
import SectionClientInformation from './components/SectionClientInformation'
import PreLoaderImage from '../../../../../components/PreLoaderImage'
import TextMaskTelephone from '../../../../../utils/TextMaskTelephone'
import NumberMaskMoney from '../../../../../utils/NumberMaskMoney'
import ActionButton from '../../../../../components/ActionButton'
import PreviewClientInformation from './components/PreviewClientInformation'
import VerifyCellphone from '../../../../../components/VerifyCellphone'

import { extendMoment } from 'moment-range'

const moment= extendMoment(Moment)

const styles = {
    menuItem: {
        zIndex: 900
    },
    labelError: {
        display: 'flex',
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    wrapper: {
        position: 'relative'
    }
}

function ClientInformation(props) {
    const { classes, documentType, gender, maritalStatus, country, academicDegree, department, 
        province, district, via, zone, housingType, employmentSituation, jobTitle, economicActivity, 
        business, paymentDate, sendCorrespondence, accountStatus, promoter, stateStepClientConsult, 
        stateStepClientValidated, odcMaster, enqueueSnackbar, constantOdc, verifyCellphone,
        callCenter } = props

    const { getDocumentType, getGender, getMaritalStatus, getCountry, getAcademicDegree, 
        getDepartment, getHomeAddressProvince, getWorkAddressProvince, getHomeAddressDistrict, getWorkAddressDistrict, 
        getVia, getZone, getHousingType, getEmploymentSituation, getJobTitle, 
        getEconomicActivity, getFilterByNameBusinessOrRuc, getPaymentDate, getSendCorrespondence, 
        getAccountStatus, getPromoter, preloadClientMaster, handleNextStep, handleSaveStateStepClientInformation, 
        showModalCancelActivity, sendSMSCode } = props

    const prevOdcMaster = useRef(odcMaster)
    const prevPromoter = useRef(promoter)
    const prevBusiness = useRef(business)
    const prevVerifyCellphone = useRef(verifyCellphone)

    const [ loading, setLoading ] = useState(true)
    const [ showModalPreviewClientInformation, setShowModalPreviewClientInformation ] = useState(false)
    const [ dataModalPreviewClientInformation, setDataModalPreviewClientInformation ] = useState({})
    const [ openModalRuc, setOpenModalRuc ] = useState(false)
    const [ sectionPersonalInformation, setSectionPersonalInformation ] = useState(true)
    const [ sectionPersonalAddress, setSectionPersonalAddress ] = useState(false)
    const [ sectionJobInformation, setSectionJobInformation ] = useState(false)
    const [ sectionJobAddress, setSectionJobAddress ] = useState(false)
    const [ sectionBenefits, setSectionBenefits ] = useState(false)
    const [ sectionQuestions, setSectionQuestions ] = useState(false)
    const [ personalInformation, setPersonalInformation ] = useState({
        typeClientId: 0,
        documentTypeId: 100001,
        documentNumber: '',
        firstName: '',
        secondName: '',
        firstLastName: '',
        secondLastName: '',
        birthday: '',
        genderId: 0,
        gender: '',
        maritalStatusId: 0,
        maritalStatus: '',
        nationalityId: 0,
        nationality: '',
        academicDegreeId: 0,
        academicDegree: '',
        email: '',
        cellphone: '',
        cellphoneConfirmed: false,
        telephone: ''
    })
    const [ cellphoneValidationSMS, setCellphoneValidationSMS ] = useState({
        validate: true,
        confirmed: false,
        visiblePopup: false
    })
    const [ personalAddress, setPersonalAddress ] = useState({
        addressCode: 0,
        departmentId: 0,
        department: '',
        provinceId: 0,
        province: '',
        districtId: 0,
        district: '',
        viaId: 0,
        via: '',
        viaDescription: '',
        number: '',
        building: '',
        insideBuilding: '',
        mz: '',
        lot: '',
        zoneDescription: '',
        zoneId: 0,
        zone: '',
        housingTypeId: 0,
        housingType: '',
        reference: ''
    })
    const [ jobInformation, setJobInformation ] = useState({
        jobSituationId: 0,
        jobSituation: '',
        jobTitleId: 0,
        jobTitle: '',
        economicActivityId: 0,
        laborIncomeDate: '',
        businessOption: '',
        businessName: '',
        ruc: '',
        grossIncome: 0,
        telephone: '',
        telephoneExtension: ''
    })
    const [ jobAddress, setJobAddress ] = useState({
        addressCode: 0,
        departmentId: 0,
        department: '',
        provinceId: 0,
        province: '',
        districtId: 0,
        district: '',
        viaId: 0,
        via: '',
        viaDescription: '',
        number: '',
        building: '',
        insideBuilding: '',
        mz: '',
        lot: '',
        zoneDescription: '',
        zoneId: 0,
        zone: '',
        reference: ''
    })
    const [ benefits, setBenefits ] = useState({
        paymentDateId: 0,
        sendCorrespondenceId: 0,
        accountStatusId: 0,
        authorizePersonalData: false,
        effectiveDisposition: true,
        scannerCode: '',
        promoterCode: '',
        nameScannerCode: ''
    })
    const [ questions, setQuestions ] = useState({
        mailAlerts: '1',
        minimumForAlerts: 0
    })
    const businessOption = [
        { value: '0', label: 'RUC' },
        { value: '1', label: 'Razón Social' }
    ]
    const [ validationPersonalInformation, setValidationPersonalInformation ] = useState({
        documentTypeId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        documentNumber: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        firstName: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        firstLastName: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        birthday: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        genderId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        maritalStatusId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        nationalityId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        academicDegreeId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        email: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        cellphone: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        cellphoneConfirmed: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        telephone: {
            message: '',
            error: false,
            ref: useRef(null)
        }
    })
    const [ validationPersonalAddress, setValidationPersonalAddress ] = useState({
        departmentId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        provinceId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        districtId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        viaId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        viaDescription: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        zoneId: {
            message: '',
            error: false,
            propertyBind: ['zoneDescription'],
            cleanValuePropertyBind: true,
            ref: useRef(null)
        },
        zoneDescription: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        housingTypeId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        reference: {
            message: '',
            error: false,
            ref: useRef(null)
        }
    })
    const [ validationJobInformation, setValidationJobInformation ] = useState({
        jobSituationId: {
            message: '',
            error: false,
            propertyBind: ['jobTitleId', 'economicActivityId'],
            ref: useRef(null)
        },
        jobTitleId: {
            message: '',
            error: false,
            notValidateWhenDisabled: true,
            ref: useRef(null)
        },
        economicActivityId: {
            message: '',
            error: false,
            notValidateWhenDisabled: true,
            ref: useRef(null)
        },
        laborIncomeDate: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        businessOption: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        businessName: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        ruc: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        grossIncome: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        telephone: {
            message: '',
            error: false,
            ref: useRef(null)
        }
    })
    const [ validationJobAddress, setValidationJobAddress ] = useState({
        departmentId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        provinceId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        districtId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        viaId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        viaDescription: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        zoneId: {
            message: '',
            error: false,
            propertyBind: ['zoneDescription'],
            cleanValuePropertyBind: true,
            ref: useRef(null)
        },
        zoneDescription: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        reference: {
            message: '',
            error: false,
            ref: useRef(null)
        }
    })
    const [ validationBenefits, setValidationBenefits ] = useState({
        paymentDateId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        sendCorrespondenceId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        accountStatusId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        scannerCode: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        nameScannerCode: {
            message: '',
            error: false,
            ref: useRef(null)
        }
    })

    useEffect(_ => {
        Promise.all([getDocumentType(), getGender(), getMaritalStatus(), getCountry(), getAcademicDegree(),
            getDepartment(), getVia(), getZone(), getHousingType(), getEmploymentSituation(),
            getJobTitle(), getEconomicActivity(), getPaymentDate(), getSendCorrespondence(),
            getAccountStatus()
        ]).then(_ => {
            if (_) {
                const minAge = constantOdc?.data?.find(item => item.des_abv_constante === 'CDA_EDAD_MINIMA_CLIENTE_TITULAR')
                const maxAge = constantOdc?.data?.find(item => item.des_abv_constante === 'CDA_EDAD_MAXIMA_CLIENTE_TITULAR')
                const minWorkingYears = constantOdc?.data?.find(item => item.des_abv_constante === 'CDA_ANO_LABORAL_MINIMA')
                const maxWorkingYears = constantOdc?.data?.find(item => item.des_abv_constante === 'CDA_ANO_LABORAL_MAXIMA')
                setValidationPersonalInformation(state => ({
                    ...state,
                    birthday: {
                        ...state.birthday,
                        extra: [{
                            regex: value => {
                                const startDate = new Date(moment().subtract(maxAge?.valor_numerico || 0, 'years').format('YYYY-MM-DD'))
                                const endDate = new Date(moment().subtract(minAge?.valor_numerico || 0, 'years').format('YYYY-MM-DD'))
                                const date = new Date(value)
                                const range = moment().range(startDate, endDate)
                                return range.contains(date)
                            },
                            message: 'Revisar fecha de nacimiento.',
                            isRegex: false
                        }]
                    }
                }))
                setValidationJobInformation(state => ({
                    ...state,
                    laborIncomeDate: {
                        ...state.laborIncomeDate,
                        extra: [{
                            regex: value => {
                                const startDate = new Date(moment().subtract(maxWorkingYears?.valor_numerico || 0, 'years').format('YYYY-MM-DD'))
                                const endDate = new Date(moment().subtract(minWorkingYears?.valor_numerico || 0, 'years').format('YYYY-MM-DD'))
                                const date = new Date(value)
                                const range = moment().range(startDate, endDate)
                                return range.contains(date)
                            },
                            message: `CDA: Antigüedad válida es entre ${ minWorkingYears?.valor_numerico || 0 } a ${ maxWorkingYears?.valor_numerico || 0 } año(s).`,
                            isRegex: false
                        }]
                    }
                }))
                setPersonalInformation(state => ({
                    ...state,
                    documentTypeId: stateStepClientConsult.documentTypeId || 0,
                    documentNumber: stateStepClientConsult.documentNumber || '',
                    cellphoneConfirmed: callCenter
                }))
                preloadClientMaster({
                    solicitudeCode: stateStepClientValidated.solicitudeCode,
                    completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
                    documentType: stateStepClientConsult.documentType,
                    documentNumber: stateStepClientConsult.documentNumber,
                    activityName: MasterOrigination.preloadClientActivity.activityName,
                    phaseCode: MasterOrigination.preloadClientActivity.phaseCode,
                    masterStageCode: Constants.MasterConfigurationStage.masterExistingClientStage,
                    clientTypeCode: Constants.ClientTypes.holderClientCode
                })
            }
        })
    }, [])

    useEffect(_ => {
        const dataSendCorrespondence = sendCorrespondence?.data?.find(item => item.val_orden === 1) || {}
        const dataAccountStatus = accountStatus?.data?.find(item => item.val_orden === 1) || {}
        Utils.resolveStateRedux(prevOdcMaster.current.clientPreloaded, odcMaster.clientPreloaded, 
            null, _ => {
                const { precargaCliente } = odcMaster.clientPreloaded.data
                const dataPersonalAddress = precargaCliente.list_tt_ori_cliente_direccionEntity?.find(address => address.cod_tipo_direccion === 260001) || {}
                const dataJobAddress = precargaCliente.list_tt_ori_cliente_direccionEntity?.find(address => address.cod_tipo_direccion === 260002) || {}

                setPersonalInformation(state => ({
                    ...state,
                    typeClientId: precargaCliente.cod_tipo_cliente || 0,
                    documentTypeId: precargaCliente.cod_tipo_documento_2 || 0,
                    documentNumber: precargaCliente.des_nro_documento || '',
                    firstName: precargaCliente.des_primer_nom || '',
                    secondName: precargaCliente.des_segundo_nom || '',
                    firstLastName: precargaCliente.des_ape_paterno || '',
                    secondLastName: precargaCliente.des_ape_materno || '',
                    birthday: precargaCliente.fec_nacimiento?.split('T')[0] || '',
                    genderId: Utils.verifyValueIntoArray(gender.data, 'cod_valor', precargaCliente.cod_genero || 0),
                    gender: precargaCliente.des_genero || '',
                    maritalStatusId: Utils.verifyValueIntoArray(maritalStatus.data, 'cod_valor', precargaCliente.cod_estado_civil || 0),
                    maritalStatus: precargaCliente.des_estado_civil || '',
                    nationalityId: Utils.verifyValueIntoArray(country.data, 'cod_valor', precargaCliente.cod_nacionalidad || 0),
                    nationality: precargaCliente.des_nacionalidad || '',
                    academicDegreeId: Utils.verifyValueIntoArray(academicDegree.data, 'cod_valor', precargaCliente.cod_gradoacademico || 0),
                    academicDegree: precargaCliente.des_gradoacademico || '',
                    email: precargaCliente.des_correo || '',
                    cellphone: precargaCliente.des_telef_celular || '',
                    telephone: precargaCliente.des_telef_fijo || ''
                }))

                setPersonalAddress({
                    addressCode: dataPersonalAddress.cod_direccion || 0,
                    departmentId: dataPersonalAddress.cod_ubi_departamento || 0,
                    department: dataPersonalAddress.des_ubi_departamento || '',
                    provinceId: dataPersonalAddress.cod_ubi_provincia || 0,
                    province: dataPersonalAddress.des_ubi_provicia || '',
                    districtId: dataPersonalAddress.cod_ubi_distrito || 0,
                    district: dataPersonalAddress.des_ubi_distrito || '',
                    viaId: Utils.verifyValueIntoArray(via.data, 'cod_valor', dataPersonalAddress.cod_via || 0),
                    via: dataPersonalAddress.des_via || '',
                    viaDescription: dataPersonalAddress.des_nombre_via || '',
                    number: dataPersonalAddress.des_nro || '',
                    building: dataPersonalAddress.des_departamento || '',
                    insideBuilding: dataPersonalAddress.des_interior || '',
                    mz: dataPersonalAddress.des_manzana || '',
                    lot: dataPersonalAddress.des_lote || '',
                    zoneDescription: dataPersonalAddress.des_zona || '',
                    zoneId: Utils.verifyValueIntoArray(zone.data, 'cod_valor', dataPersonalAddress.cod_zona || 0),
                    zone: dataPersonalAddress.des_zona_valor_interno || '',
                    housingTypeId: Utils.verifyValueIntoArray(housingType.data, 'cod_valor', dataPersonalAddress.cod_tipo_vivienda || 0),
                    housingType: dataPersonalAddress.des_tipo_vivienda || '',
                    reference: dataPersonalAddress.des_referencia_domicilio || ''
                })

                setJobInformation({
                    jobSituationId: Utils.verifyValueIntoArray(employmentSituation.data, 'cod_valor', precargaCliente.cod_situacion_laboral || 0),
                    jobSituation: precargaCliente.des_situacion_laboral || '',
                    jobTitleId: Utils.verifyValueIntoArray(jobTitle.data, 'cod_valor', precargaCliente.cod_cargo_profesion || 0),
                    jobTitle: precargaCliente.des_cargo_profesion || '',
                    economicActivityId: Utils.verifyValueIntoArray(economicActivity.data, 'cod_valor', precargaCliente.cod_actividad_economica || 0),
                    laborIncomeDate: precargaCliente.fec_ingreso_laboral?.split('T')[0] || '',
                    businessOption: precargaCliente.des_ruc ? '0' : precargaCliente.des_razon_social ? '1' : '',
                    businessName: precargaCliente.des_razon_social || '',
                    ruc: precargaCliente.des_ruc || '',
                    grossIncome: precargaCliente.num_ingreso_bruto || 0,
                    telephone: precargaCliente.des_telefono_laboral || '',
                    telephoneExtension: precargaCliente.des_anexo_laboral || ''
                })

                setJobAddress({
                    addressCode: dataJobAddress.cod_direccion || 0,
                    departmentId: dataJobAddress.cod_ubi_departamento || 0,
                    department: dataJobAddress.des_ubi_departamento || '',
                    provinceId: dataJobAddress.cod_ubi_provincia || 0,
                    province: dataJobAddress.des_ubi_provicia || '',
                    districtId: dataJobAddress.cod_ubi_distrito || 0,
                    district: dataJobAddress.des_ubi_distrito || '',
                    viaId: Utils.verifyValueIntoArray(via.data, 'cod_valor', dataJobAddress.cod_via || 0),
                    via: dataJobAddress.des_via || '',
                    viaDescription: dataJobAddress.des_nombre_via || '',
                    number: dataJobAddress.des_nro || '',
                    building: dataJobAddress.des_departamento || '',
                    insideBuilding: dataJobAddress.des_interior || '',
                    mz: dataJobAddress.des_manzana || '',
                    lot: dataJobAddress.des_lote || '',
                    zoneDescription: dataJobAddress.des_zona || '',
                    zoneId: Utils.verifyValueIntoArray(zone.data, 'cod_valor', dataJobAddress.cod_zona || 0),
                    zone: dataJobAddress.des_zona_valor_interno || '',
                    reference: dataJobAddress.des_referencia_domicilio || ''
                })

                setBenefits({
                    paymentDateId: Utils.verifyValueIntoArray(paymentDate.data, 'cod_valor', stateStepClientValidated.paymentDateId || 0),
                    sendCorrespondenceId: Utils.verifyValueIntoArray(sendCorrespondence.data, 'cod_valor', dataSendCorrespondence.cod_valor || 0),
                    accountStatusId: Utils.verifyValueIntoArray(accountStatus.data, 'cod_valor', dataAccountStatus.cod_valor || 0),
                    authorizePersonalData: precargaCliente.flg_autoriza_datos_per || false,
                    effectiveDisposition: true,
                    scannerCode: '',
                    promoterCode: '',
                    nameScannerCode: ''
                })

                if (dataPersonalAddress.cod_ubi_departamento)
                    getHomeAddressProvince({ type: 'PR', value: dataPersonalAddress.cod_ubi_departamento || 0 })
                if (dataPersonalAddress.cod_ubi_provincia)
                    getHomeAddressDistrict({ type: 'DI', value: dataPersonalAddress.cod_ubi_provincia || 0 })
                
                if (dataJobAddress.cod_ubi_departamento)
                    getWorkAddressProvince({ type: 'PR', value: dataJobAddress.cod_ubi_departamento || 0 })
                if (dataJobAddress.cod_ubi_provincia)
                    getWorkAddressDistrict({ type: 'DI', value: dataJobAddress.cod_ubi_provincia || 0 })

                setCellphoneValidationSMS(state => ({
                    ...state,
                    visiblePopup: true,
                    validate: !callCenter
                }))

                setLoading(false)
            },
            warningMessage => {
                Notistack.getNotistack(warningMessage, enqueueSnackbar, 'warning')
                setCellphoneValidationSMS(state => ({
                    ...state,
                    visiblePopup: true,
                    validate: !callCenter
                }))
                setBenefits(state => ({
                    ...state,
                    paymentDateId: Utils.verifyValueIntoArray(paymentDate.data, 'cod_valor', stateStepClientValidated.paymentDateId || 0),
                    sendCorrespondenceId: Utils.verifyValueIntoArray(sendCorrespondence.data, 'cod_valor', dataSendCorrespondence.cod_valor || 0),
                    accountStatusId: Utils.verifyValueIntoArray(accountStatus.data, 'cod_valor', dataAccountStatus.cod_valor || 0)
                }))
                setLoading(false)
            },
            errorMessage => {
                Notistack.getNotistack(errorMessage, enqueueSnackbar, 'error')
                setCellphoneValidationSMS(state => ({
                    ...state,
                    visiblePopup: true,
                    validate: !callCenter
                }))
                setBenefits(state => ({
                    ...state,
                    paymentDateId: Utils.verifyValueIntoArray(paymentDate.data, 'cod_valor', stateStepClientValidated.paymentDateId || 0),
                    sendCorrespondenceId: Utils.verifyValueIntoArray(sendCorrespondence.data, 'cod_valor', dataSendCorrespondence.cod_valor || 0),
                    accountStatusId: Utils.verifyValueIntoArray(accountStatus.data, 'cod_valor', dataAccountStatus.cod_valor || 0)
                }))
                setLoading(false)
            }
        )
    }, [odcMaster.clientPreloaded])

    useEffect(_ => {
        Utils.resolveStateRedux(prevBusiness.current.filteredBusiness, business.filteredBusiness, 
            _ => setOpenModalRuc(false), 
            _ => setOpenModalRuc(true), _ => setOpenModalRuc(false), _ => setOpenModalRuc(false))
    }, [business.filteredBusiness])

    useEffect(_ => {
        const handle = {
            callSetState: setBenefits,
            callSetValidation: setValidationBenefits
        }
        Utils.resolveStateRedux(prevPromoter.current, promoter, _ => {
            handleChangeState(handle, { name: 'nameScannerCode', value: '' })
            handleChangeState(handle, { name: 'promoterCode', value: '' })
        }, 
        _ => {
            handleChangeState(handle, { name: 'nameScannerCode', value: promoter.data.des_nom_promotor || ' ' })
            handleChangeState(handle, { name: 'promoterCode', value: promoter.data.cod_promotor || '' })
        },
        _ => {
            handleChangeState(handle, { name: 'nameScannerCode', value: '' })
            handleChangeState(handle, { name: 'promoterCode', value: '' })
        },
        _ => {
            handleChangeState(handle, { name: 'nameScannerCode', value: '' })
            handleChangeState(handle, { name: 'promoterCode', value: '' })
        })
    }, [promoter])

    useEffect(_ => {
        Utils.resolveStateRedux(prevVerifyCellphone.current.verifySMSCodeService, verifyCellphone.verifySMSCodeService,
            null,
            _ => {
                setCellphoneValidationSMS(state => ({
                    ...state,
                    confirmed: true,
                    visiblePopup: false
                }))
                handleChangeState({
                    callSetState: setPersonalInformation,
                    callSetValidation: setValidationPersonalInformation
                    }, { name: 'cellphoneConfirmed', value: true } )
            }, null, null)
    }, [verifyCellphone.verifySMSCodeService])

    const getObjectArrayValueLabel = array => {
        return array?.data?.map(item => ({
            value: item.cod_valor || item.cod_det_ubi,
            label: item.des_valor || item.val_ubigeo
        })) || []
    }

    const handleSendSMS = e => {
        Utils.handleEnterKeyPress(e, _ =>{
            if (!validationPersonalInformation.cellphone.error &&
                cellphoneValidationSMS.validate &&
                !cellphoneValidationSMS.confirmed) {
                const smsData = {
                    cod_solicitud_completa: stateStepClientValidated.completeSolicitudeCode,
                    des_nro_documento: personalInformation.documentNumber,
                    des_telefono: personalInformation.cellphone
                }
                sendSMSCode(smsData)
            }
        })
    }

    const handleChangeState = (handle, e) => {
        const { name, value, call = _ => null, validate = true } = e.target || e
        handle.callSetState(state => ({
            ...state,
            [name]: value
        }))
        handle.callSetValidation(state => {
            let validations = { ...state }
            if (Utils.findProperty(state, name, ['ref']).length > 0) {
                const notValidate = ((state[name].ref?.current?.props?.isDisabled || false) && (state[name].notValidateWhenDisabled || false)) || !validate
                let validationOptional = { }
                const bindings = state[name].propertyBind || []
                bindings.forEach(item => {
                    const namePropertyBind = item
                    const validationBind = state[namePropertyBind]
                    if (validationBind) {
                        if (value) {
                            const valueBindProperty = handle.state[namePropertyBind]
                            validationOptional = {
                                ...validationOptional,
                                [namePropertyBind]: {
                                    ...validationBind,
                                    ...Validations.validateByField(namePropertyBind, 
                                        !Utils.isNullOrUndefined(valueBindProperty) ? valueBindProperty : '')
                                }
                            }
                        }
                        else {
                            validationOptional = {
                                ...validationOptional,
                                [namePropertyBind]: {
                                    ...validationBind,
                                    ...Validations.validateByField(namePropertyBind, '', false)
                                }
                            }
                            if (state[name].cleanValuePropertyBind) handle.callSetState(state => ({ ...state, [namePropertyBind]: '' }))
                        }
                    }
                })
                validations = {
                    ...state,
                    ...validationOptional,
                    [name]: {
                        ...state[name],
                        ...Validations.validateByField(name, value, !notValidate, state[name].extra || [])
                    }
                }
            }
            call(validations)
            return validations
        })
    }

    const handlePreviewInformation = async _ => {
        setSectionQuestions(false)
        const sections = [ 'personalInformation', 'personalAddress', 'jobInformation', 'jobAddress', 'benefits' ]
        let successCounting = 0
        sections.forEach(section => {
            let handle = { }
            switch (section) {
                case 'personalInformation':
                    handle = {
                        state: personalInformation,
                        callSetState: setPersonalInformation,
                        callSetValidation: setValidationPersonalInformation,
                        section: setSectionPersonalInformation
                    }
                    break;
                case 'personalAddress':
                    handle = {
                        state: personalAddress,
                        callSetState: setPersonalAddress,
                        callSetValidation: setValidationPersonalAddress,
                        section: setSectionPersonalAddress
                    }
                    break;
                case 'jobInformation':
                    handle = {
                        state: jobInformation,
                        callSetState: setJobInformation,
                        callSetValidation: setValidationJobInformation,
                        section: setSectionJobInformation
                    }
                    break;
                case 'jobAddress':
                    handle = {
                        state: jobAddress,
                        callSetState: setJobAddress,
                        callSetValidation: setValidationJobAddress,
                        section: setSectionJobAddress
                    }
                    break;
                case 'benefits':
                    handle = {
                        state: benefits,
                        callSetState: setBenefits,
                        callSetValidation: setValidationBenefits,
                        section: setSectionBenefits
                    }
                    break;
                default:
                    break;
            }
            Validations.comprobeAllValidationsSuccess(handle.state,
                e => handleChangeState({ callSetState: handle.callSetState, callSetValidation: handle.callSetValidation,
                    state: handle.state}, e)).then(valid => { 
                        if (valid) {
                            handle.section(false)
                            successCounting++
                            if (successCounting === 5) {
                                const data = {
                                    personalInformation: {
                                        ...personalInformation,
                                        telephone: Utils.filterMaskTelephone(personalInformation.telephone)
                                    },
                                    personalAddress: personalAddress,
                                    jobInformation: {
                                        ...jobInformation,
                                        telephone: Utils.filterMaskTelephone(jobInformation.telephone)
                                    },
                                    jobAddress: jobAddress,
                                    benefits: benefits,
                                    questions: questions
                                }
                                handleSaveStateStepClientInformation(data)
                                setDataModalPreviewClientInformation({
                                    clientConsult: stateStepClientConsult,
                                    clientValidated: stateStepClientValidated,
                                    ...data
                                })
                                setShowModalPreviewClientInformation(true)
                            }
                        } else handle.section(true)
                })
        })
    }

    const handleCancelProcess = _ => {
        const activity = {
            solicitudeCode: stateStepClientValidated.solicitudeCode,
            completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
            currentlyPhaseCode: 100203,
            previousPhaseCode: 100201,
            activityName: 'Cliente Existente: Cancelado'
        }
        showModalCancelActivity(activity)
    }

    return(
        <div 
            className='mb-2'>
                {
                    loading 
                        ?   <PreLoaderImage text='Cargando información del cliente ...' />
                        :   <Fade>
                                <Grid
                                    container
                                    spacing={ 8 }
                                    className='mb-2'>
                                        <Grid
                                            item
                                            xs={ 12 }>
                                                <SectionClientInformation
                                                    data={ personalInformation }
                                                    expanded = { sectionPersonalInformation }
                                                    onClick={ _ => setSectionPersonalInformation(!sectionPersonalInformation) }
                                                    title={
                                                        <>
                                                            <AccountCircleIcon
                                                                fontSize='small' 
                                                                className='mr-2'/>
                                                            Información Personal
                                                        </>
                                                    }>
                                                        <>
                                                            <Grid
                                                                container
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 6 }
                                                                        lg={ 3 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                required
                                                                                margin='normal'>
                                                                                    <InputLabel
                                                                                        required
                                                                                        error={ validationPersonalInformation.documentTypeId.error }>
                                                                                            Tipo Documento
                                                                                    </InputLabel>
                                                                                    <Select
                                                                                        disabled
                                                                                        error={ validationPersonalInformation.documentTypeId.error }
                                                                                        value={ personalInformation.documentTypeId }
                                                                                        inputRef={ validationPersonalInformation.documentTypeId.ref }
                                                                                        name='documentTypeId'
                                                                                        onChange={ e => handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                        }, e) }
                                                                                        onBlur={ e => handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                        }, e) }>
                                                                                        {
                                                                                            documentType.data.map((type, index) => 
                                                                                                <MenuItem
                                                                                                    key={ index }
                                                                                                    value={ type.cod_valor }>
                                                                                                        { type.des_valor_corto }
                                                                                                </MenuItem>)
                                                                                        }
                                                                                    </Select>
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.documentTypeId.error 
                                                                                                ? 'secondary' : 'default'}>
                                                                                                    {
                                                                                                        validationPersonalInformation.documentTypeId.error &&
                                                                                                        validationPersonalInformation.documentTypeId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 6 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                error={ validationPersonalInformation.documentNumber.error }
                                                                                fullWidth
                                                                                label='Número Documento'
                                                                                type='text'
                                                                                required
                                                                                margin='normal'
                                                                                color='default'
                                                                                value={ personalInformation.documentNumber }
                                                                                inputRef={ validationPersonalInformation.documentNumber.ref }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setPersonalInformation,
                                                                                    callSetValidation: setValidationPersonalInformation
                                                                                }, e) }
                                                                                onBlur={ e => handleChangeState({
                                                                                    callSetState: setPersonalInformation,
                                                                                    callSetValidation: setValidationPersonalInformation
                                                                                }, e) }
                                                                                name='documentNumber'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.getLengthDocumentByType(personalInformation.documentTypeId)
                                                                                    },
                                                                                    readOnly: true,
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <RecentActorsIcon
                                                                                                    color={ validationPersonalInformation.documentNumber.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.documentNumber.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalInformation.documentNumber.error &&
                                                                                                        validationPersonalInformation.documentNumber.message
                                                                                                    }
                                                                                        </Typography>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.documentNumber.error
                                                                                                ? 'secondary' : 'default' }>
                                                                                                {
                                                                                                    `${personalInformation.documentNumber.length}/
                                                                                                    ${Utils.getLengthDocumentByType(personalInformation.documentTypeId)}`
                                                                                                }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                            </Grid>
                                                            <Grid
                                                                container
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 6 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                error={ validationPersonalInformation.firstName.error }
                                                                                fullWidth
                                                                                required
                                                                                label='Primer Nombre'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ personalInformation.firstName }
                                                                                inputRef={ validationPersonalInformation.firstName.ref }
                                                                                onChange={ e => {
                                                                                    if (Utils.onlyTextRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                        }, e)
                                                                                } }
                                                                                onBlur={ e => {
                                                                                    if (Utils.onlyTextRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                        }, e)
                                                                                } }
                                                                                name='firstName'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <Filter1Icon
                                                                                                    color={ validationPersonalInformation.firstName.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.firstName.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalInformation.firstName.error &&
                                                                                                        validationPersonalInformation.firstName.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 6 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Segundo Nombre'
                                                                                type='text'
                                                                                color='default'
                                                                                margin='normal'
                                                                                value={ personalInformation.secondName }
                                                                                name='secondName'
                                                                                onChange={ e => {
                                                                                    if (Utils.onlyTextRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                        }, e)
                                                                                } }
                                                                                onBlur={ e => {
                                                                                    if (Utils.onlyTextRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                        }, e)
                                                                                } }
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <Filter2Icon
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 6 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                error={ validationPersonalInformation.firstLastName.error }
                                                                                fullWidth
                                                                                required
                                                                                label='Apellido Paterno'
                                                                                type='text'
                                                                                color='default'
                                                                                margin='normal'
                                                                                value={ personalInformation.firstLastName }
                                                                                inputRef={ validationPersonalInformation.firstLastName.ref }
                                                                                onChange={ e => {
                                                                                    if (Utils.onlyTextRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                        }, e)
                                                                                } }
                                                                                onBlur={ e => {
                                                                                    if (Utils.onlyTextRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                        }, e)
                                                                                } }
                                                                                name='firstLastName'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <PermIdentityIcon
                                                                                                    color={ validationPersonalInformation.firstLastName.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.firstLastName.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalInformation.firstLastName.error &&
                                                                                                        validationPersonalInformation.firstLastName.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 6 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Apellido Materno'
                                                                                type='text'
                                                                                color='default'
                                                                                margin='normal'
                                                                                value={ personalInformation.secondLastName }
                                                                                name='secondLastName'
                                                                                onChange={ e => {
                                                                                    if (Utils.onlyTextRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                        }, e)
                                                                                } }
                                                                                onBlur={ e => {
                                                                                    if (Utils.onlyTextRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                        }, e)
                                                                                } }
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <PermIdentityIcon
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                            </Grid>
                                                            <Grid
                                                                container
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 6 }
                                                                        md={ 3 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                error={ validationPersonalInformation.birthday.error }
                                                                                fullWidth
                                                                                required
                                                                                label='Fecha Nacimiento'
                                                                                type='date'
                                                                                color='default'
                                                                                margin='normal'
                                                                                format='DD-MM-YYYY'
                                                                                value={ personalInformation.birthday }
                                                                                inputRef={ validationPersonalInformation.birthday.ref }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setPersonalInformation,
                                                                                    callSetValidation: setValidationPersonalInformation
                                                                                }, e) }
                                                                                onBlur={ e => handleChangeState({
                                                                                    callSetState: setPersonalInformation,
                                                                                    callSetValidation: setValidationPersonalInformation
                                                                                }, e) }
                                                                                name='birthday'
                                                                                InputLabelProps={{
                                                                                    shrink: true
                                                                                }}
                                                                                InputProps={{
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <CalendarTodayIcon
                                                                                                    color={ validationPersonalInformation.birthday.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.birthday.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalInformation.birthday.error &&
                                                                                                        validationPersonalInformation.birthday.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 6 }
                                                                        md={ 3 }
                                                                        lg={ 3 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                required
                                                                                error={ validationPersonalInformation.genderId.error }>
                                                                                    <Autocomplete
                                                                                        placeholder='Género *'
                                                                                        value={ personalInformation.genderId }
                                                                                        inputRef={ validationPersonalInformation.genderId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalInformation,
                                                                                                callSetValidation: setValidationPersonalInformation
                                                                                            }, { name: 'genderId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalInformation,
                                                                                                callSetValidation: setValidationPersonalInformation
                                                                                            }, { name: 'gender', value: e?.label || '' })
                                                                                        }}
                                                                                        data={ getObjectArrayValueLabel(gender) }/>
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.genderId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalInformation.genderId.error &&
                                                                                                        validationPersonalInformation.genderId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 6 }
                                                                        md={ 3 }
                                                                        lg={ 3 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                required
                                                                                error={ validationPersonalInformation.maritalStatusId.error }>
                                                                                    <Autocomplete
                                                                                        placeholder='Estado Civil *'
                                                                                        value={ personalInformation.maritalStatusId }
                                                                                        inputRef={ validationPersonalInformation.maritalStatusId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalInformation,
                                                                                                callSetValidation: setValidationPersonalInformation
                                                                                            }, { name: 'maritalStatusId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalInformation,
                                                                                                callSetValidation: setValidationPersonalInformation
                                                                                            }, { name: 'maritalStatus', value: e?.label || '' })
                                                                                        }}
                                                                                        data={ getObjectArrayValueLabel(maritalStatus) }/>
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.maritalStatusId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalInformation.maritalStatusId.error &&
                                                                                                        validationPersonalInformation.maritalStatusId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 6 }
                                                                        md={ 3 }
                                                                        lg={ 3 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                required
                                                                                error={ validationPersonalInformation.nationalityId.error }>
                                                                                    <Autocomplete
                                                                                        placeholder='Nacionalidad *'
                                                                                        value={ personalInformation.nationalityId }
                                                                                        inputRef={ validationPersonalInformation.nationalityId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalInformation,
                                                                                                callSetValidation: setValidationPersonalInformation
                                                                                            }, { name: 'nationalityId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalInformation,
                                                                                                callSetValidation: setValidationPersonalInformation
                                                                                            }, { name: 'nationality', value: e?.label || '' })
                                                                                        }}
                                                                                        data={ getObjectArrayValueLabel(country) }/>
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.nationalityId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalInformation.nationalityId.error &&
                                                                                                        validationPersonalInformation.nationalityId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                            </Grid>
                                                            <Grid
                                                                container
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 6 }
                                                                        lg={ 3 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                required
                                                                                error={ validationPersonalInformation.academicDegreeId.error }>
                                                                                    <Autocomplete
                                                                                        placeholder='Grado Académico *'
                                                                                        value={ personalInformation.academicDegreeId }
                                                                                        inputRef={ validationPersonalInformation.academicDegreeId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalInformation,
                                                                                                callSetValidation: setValidationPersonalInformation
                                                                                            }, { name: 'academicDegreeId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalInformation,
                                                                                                callSetValidation: setValidationPersonalInformation
                                                                                            }, { name: 'academicDegree', value: e?.label || '' })
                                                                                        }}
                                                                                        data={ getObjectArrayValueLabel(academicDegree) }/>
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.academicDegreeId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalInformation.academicDegreeId.error &&
                                                                                                        validationPersonalInformation.academicDegreeId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 6 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                error={ validationPersonalInformation.email.error }
                                                                                required
                                                                                fullWidth
                                                                                label='Correo Electrónico'
                                                                                type='email'
                                                                                margin='normal'
                                                                                color='default'
                                                                                value={ personalInformation.email }
                                                                                inputRef={ validationPersonalInformation.email.ref }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setPersonalInformation,
                                                                                    callSetValidation: setValidationPersonalInformation
                                                                                    }, e) 
                                                                                }
                                                                                onBlur={ e => handleChangeState({
                                                                                    callSetState: setPersonalInformation,
                                                                                    callSetValidation: setValidationPersonalInformation
                                                                                    }, e)
                                                                                }
                                                                                name='email'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultEmailLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <EmailIcon
                                                                                                    color={ validationPersonalInformation.email.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.email.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalInformation.email.error &&
                                                                                                        validationPersonalInformation.email.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 6 }
                                                                        md={ 6 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                error={ validationPersonalInformation.cellphone.error || 
                                                                                    validationPersonalInformation.cellphoneConfirmed.error }
                                                                                fullWidth
                                                                                required
                                                                                label='Celular'
                                                                                type={ cellphoneValidationSMS.validate ? 'search' : 'text' }
                                                                                margin='normal'
                                                                                color='default'
                                                                                value={ personalInformation.cellphone }
                                                                                inputRef={ validationPersonalInformation.cellphone.ref }
                                                                                onKeyUp={ e => handleSendSMS(e) }
                                                                                onChange={ e => {
                                                                                    if (Utils.onlyNumberRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                            }, e) 
                                                                                } }
                                                                                onBlur={ e => {
                                                                                    if (Utils.onlyNumberRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setPersonalInformation,
                                                                                            callSetValidation: setValidationPersonalInformation
                                                                                            }, e) 
                                                                                } }
                                                                                name='cellphone'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        readOnly: cellphoneValidationSMS.validate && cellphoneValidationSMS.confirmed,
                                                                                        maxLength: Utils.defaultCellphoneLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <StayPrimaryPortraitIcon
                                                                                                    color={ validationPersonalInformation.cellphone.error ||
                                                                                                            validationPersonalInformation.cellphoneConfirmed.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small' />
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            className='text-left'
                                                                                            color={ validationPersonalInformation.cellphone.error ||
                                                                                                    validationPersonalInformation.cellphoneConfirmed.error
                                                                                                ? 'secondary' : 'default' }>
                                                                                                {
                                                                                                    cellphoneValidationSMS.validate ? 'Presionar Enter para enviar SMS | ' : ''
                                                                                                }
                                                                                                {
                                                                                                    `${personalInformation.cellphone.length}/${Utils.defaultCellphoneLengthInput}`
                                                                                                }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            className='text-left'
                                                                                            color={ validationPersonalInformation.cellphone.error ||
                                                                                                    validationPersonalInformation.cellphoneConfirmed.error
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalInformation.cellphone.error &&
                                                                                                        validationPersonalInformation.cellphone.message
                                                                                                    }
                                                                                                    {
                                                                                                        validationPersonalInformation.cellphoneConfirmed.error &&
                                                                                                        validationPersonalInformation.cellphoneConfirmed.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 6 }
                                                                        md={ 6 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                error={ validationPersonalInformation.telephone.error }
                                                                                fullWidth
                                                                                label='Teléfono Fijo'
                                                                                type='text'
                                                                                margin='normal'
                                                                                color='default'
                                                                                name='telephone'
                                                                                inputRef={ validationPersonalInformation.telephone.ref }
                                                                                value={ personalInformation.telephone }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setPersonalInformation,
                                                                                    callSetValidation: setValidationPersonalInformation
                                                                                    }, e)
                                                                                }
                                                                                onBlur={ e => handleChangeState({
                                                                                    callSetState: setPersonalInformation,
                                                                                    callSetValidation: setValidationPersonalInformation
                                                                                    }, e)
                                                                                }
                                                                                InputLabelProps={{
                                                                                    shrink: true
                                                                                }}
                                                                                InputProps={{
                                                                                    inputComponent: TextMaskTelephone,
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <PhoneIcon
                                                                                                    color={ validationPersonalInformation.telephone.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small' />
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <FormHelperText>
                                                                                (Código Región) + Número Telefónico
                                                                            </FormHelperText>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalInformation.telephone.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalInformation.telephone.error &&
                                                                                                        validationPersonalInformation.telephone.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                            </Grid>
                                                        </>
                                                </SectionClientInformation>
                                        </Grid>
                                        <Grid
                                            item
                                            xs={ 12 }>
                                                <SectionClientInformation
                                                    data={ personalAddress }
                                                    expanded = { sectionPersonalAddress }
                                                    extra={ props }
                                                    onClick={ _ => setSectionPersonalAddress(!sectionPersonalAddress) }
                                                    title={
                                                        <>
                                                            <PlaceIcon
                                                                fontSize='small'
                                                                className='mr-2'/>
                                                            Dirección Personal
                                                        </>
                                                    }>
                                                        <>
                                                            <Grid
                                                                container
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                required
                                                                                error={ validationPersonalAddress.departmentId.error }>
                                                                                    <Autocomplete
                                                                                        disabled={ !department.success }
                                                                                        data={ getObjectArrayValueLabel(department) }
                                                                                        placeholder='Departamento *'
                                                                                        value={ personalAddress.departmentId }
                                                                                        inputRef={ validationPersonalAddress.departmentId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                            }, { name: 'departmentId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                            }, { name: 'department', value: e?.label || '' })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                            }, { name: 'provinceId', value: 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                            }, { name: 'province', value: '' })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                            }, { name: 'districtId', value: 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                            }, { name: 'district', value: '' })
                                                                                            getHomeAddressProvince({ type: 'PR', value: e?.value || 0 })
                                                                                            getHomeAddressDistrict({ type: 'DI', value: 0 })
                                                                                        }} />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalAddress.departmentId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalAddress.departmentId.error &&
                                                                                                        validationPersonalAddress.departmentId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                required
                                                                                error={ validationPersonalAddress.provinceId.error }>
                                                                                    <Autocomplete
                                                                                        disabled={ !province.homeAddressProvince.success }
                                                                                        data={ getObjectArrayValueLabel(province.homeAddressProvince || []) }
                                                                                        placeholder='Provincia *'
                                                                                        value={ personalAddress.provinceId }
                                                                                        inputRef={ validationPersonalAddress.provinceId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                            }, { name: 'provinceId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                            }, { name: 'province', value: e?.label || '' })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                            }, { name: 'districtId', value: 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                            }, { name: 'district', value: '' })
                                                                                            getHomeAddressDistrict({ type: 'DI', value: e?.value || 0 })
                                                                                        }} />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalAddress.provinceId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalAddress.provinceId.error &&
                                                                                                        validationPersonalAddress.provinceId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                required
                                                                                disabled={ district.homeAddressDistrict.loading }
                                                                                error={ validationPersonalAddress.districtId.error }>
                                                                                    <Autocomplete
                                                                                        disabled={ !district.homeAddressDistrict.success }
                                                                                        data={ getObjectArrayValueLabel(district.homeAddressDistrict || []) }
                                                                                        placeholder='Distrito *'
                                                                                        value={ personalAddress.districtId }
                                                                                        inputRef={ validationPersonalAddress.districtId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                                }, { name: 'districtId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                                }, { name: 'district', value: e?.label || '' })
                                                                                        }} />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalAddress.districtId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalAddress.districtId.error &&
                                                                                                        validationPersonalAddress.districtId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                required
                                                                                error={ validationPersonalAddress.viaId.error }>
                                                                                    <Autocomplete
                                                                                        data={ getObjectArrayValueLabel(via) }
                                                                                        placeholder='Vía *'
                                                                                        value={ personalAddress.viaId }
                                                                                        inputRef={ validationPersonalAddress.viaId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                                }, { name: 'viaId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                                }, { name: 'via', value: e?.label || '' })
                                                                                        }} />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalAddress.viaId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalAddress.viaId.error &&
                                                                                                        validationPersonalAddress.viaId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 12 }
                                                                        md={ 8 }
                                                                        lg={ 6 }>
                                                                            <TextField
                                                                                error={ validationPersonalAddress.viaDescription.error }
                                                                                fullWidth
                                                                                required
                                                                                label='Nombre Vía'
                                                                                type='text'
                                                                                margin='normal'
                                                                                color='default'
                                                                                value={ personalAddress.viaDescription }
                                                                                inputRef={ validationPersonalAddress.viaDescription.ref }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setPersonalAddress,
                                                                                    callSetValidation: setValidationPersonalAddress
                                                                                    }, e) 
                                                                                }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                onBlur={ e => handleChangeState({
                                                                                    callSetState: setPersonalAddress,
                                                                                    callSetValidation: setValidationPersonalAddress
                                                                                    }, e) 
                                                                                }
                                                                                name='viaDescription'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultViaLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <PlaceIcon 
                                                                                                    color={ validationPersonalAddress.viaDescription.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalAddress.viaDescription.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalAddress.viaDescription.error &&
                                                                                                        validationPersonalAddress.viaDescription.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                            </Grid>
                                                            <Grid
                                                                container
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Número'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ personalAddress.number }
                                                                                onChange={ e => {
                                                                                    if (Utils.onlyNumberRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setPersonalAddress,
                                                                                            callSetValidation: setValidationPersonalAddress
                                                                                            }, e) 
                                                                                } }
                                                                                name='number'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultComplementaryLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <Filter9PlusIcon
                                                                                                    fontSize='small' />
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Departamento'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ personalAddress.building }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setPersonalAddress,
                                                                                    callSetValidation: setValidationPersonalAddress
                                                                                    }, e) 
                                                                                }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                name='building'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultComplementaryLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <HomeIcon
                                                                                                    fontSize='small' />
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Interior'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ personalAddress.insideBuilding }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setPersonalAddress,
                                                                                    callSetValidation: setValidationPersonalAddress
                                                                                    }, e) 
                                                                                }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                name='insideBuilding'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultComplementaryLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <LastPageIcon
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Mz'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ personalAddress.mz }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setPersonalAddress,
                                                                                    callSetValidation: setValidationPersonalAddress
                                                                                    }, e) 
                                                                                }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                name='mz'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultComplementaryLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <CropDinIcon
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Lote'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ personalAddress.lot }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setPersonalAddress,
                                                                                    callSetValidation: setValidationPersonalAddress
                                                                                    }, e) 
                                                                                }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                name='lot'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultComplementaryLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <TabUnselectedIcon
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                            </Grid>
                                                            <Grid
                                                                container
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationPersonalAddress.zoneId.error }>
                                                                                    <Autocomplete
                                                                                        data={ getObjectArrayValueLabel(zone) }
                                                                                        placeholder='Zona'
                                                                                        value={ personalAddress.zoneId }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                state: personalAddress,
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                                }, { name: 'zoneId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                state: personalAddress,
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                                }, { name: 'zone', value: e?.label || '' })
                                                                                        }} />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalAddress.zoneId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalAddress.zoneId.error &&
                                                                                                        validationPersonalAddress.zoneId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 8 }
                                                                        md={ 8 }
                                                                        lg={ 6 }>
                                                                            <TextField
                                                                                error={ validationPersonalAddress.zoneDescription.error }
                                                                                fullWidth
                                                                                label='Nombre Zona'
                                                                                type='text'
                                                                                margin='normal'
                                                                                disabled={ personalAddress.zoneId === 0 }
                                                                                color='default'
                                                                                inputRef={ validationPersonalAddress.zoneDescription.ref }
                                                                                value={ personalAddress.zoneDescription }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setPersonalAddress,
                                                                                    callSetValidation: setValidationPersonalAddress
                                                                                    }, e) 
                                                                                }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                onBlur={ e => handleChangeState({
                                                                                    callSetState: setPersonalAddress,
                                                                                    callSetValidation: setValidationPersonalAddress
                                                                                    }, e) 
                                                                                }
                                                                                name='zoneDescription'
                                                                                InputProps={{
                                                                                    inputProps:{
                                                                                        maxLength: 100
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <NearMeIcon
                                                                                                    color={ validationPersonalAddress.zoneDescription.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalAddress.zoneDescription.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalAddress.zoneDescription.error &&
                                                                                                        validationPersonalAddress.zoneDescription.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 12 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationPersonalAddress.housingTypeId.error }>
                                                                                    <Autocomplete
                                                                                        data={ getObjectArrayValueLabel(housingType) }
                                                                                        value={ personalAddress.housingTypeId }
                                                                                        inputRef={ validationPersonalAddress.housingTypeId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                                }, { name: 'housingTypeId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setPersonalAddress,
                                                                                                callSetValidation: setValidationPersonalAddress
                                                                                                }, { name: 'housingType', value: e?.label || '' })
                                                                                        }}
                                                                                        placeholder='Tipo Vivienda *'
                                                                                    />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationPersonalAddress.housingTypeId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationPersonalAddress.housingTypeId.error &&
                                                                                                        validationPersonalAddress.housingTypeId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 12 }
                                                                        md={ 8 }
                                                                        lg={ 12 }>
                                                                            <TextField
                                                                                error={ validationPersonalAddress.reference.error }
                                                                                fullWidth
                                                                                label='Referencia del Domicilio'
                                                                                multiline
                                                                                rows='1'
                                                                                rowsMax='3'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ personalAddress.reference }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setPersonalAddress,
                                                                                    callSetValidation: setValidationPersonalAddress
                                                                                    }, e) 
                                                                                }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                name='reference'
                                                                                InputProps={{
                                                                                    inputProps:{
                                                                                        maxLength: Utils.defaultReferenceLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                            <LocalLibraryIcon
                                                                                                fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ 'd-flex justify-content-between small' } >
                                                                                        <FormHelperText
                                                                                            component='span'
                                                                                            className={ validationPersonalAddress.reference.error 
                                                                                                ? 'text-red' : 'inherit' }>
                                                                                                { validationPersonalAddress.reference.message }
                                                                                        </FormHelperText>
                                                                                        <FormHelperText
                                                                                            component='span'
                                                                                            className={ validationPersonalAddress.reference.error
                                                                                                ? 'text-red': 'inherit' }>
                                                                                                {
                                                                                                    `${personalAddress.reference.length}/${Utils.defaultReferenceLengthInput}`
                                                                                                }
                                                                                        </FormHelperText>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                            </Grid>
                                                        </>
                                                </SectionClientInformation>
                                        </Grid>
                                        <Grid
                                            item
                                            xs={ 12 }>
                                                <SectionClientInformation
                                                    data={ jobInformation }
                                                    extra={ openModalRuc }
                                                    expanded = { sectionJobInformation }
                                                    onClick={ _ => setSectionJobInformation(!sectionJobInformation) }
                                                    title={
                                                        <>
                                                            <WorkIcon
                                                                fontSize='small'
                                                                className='mr-2' />
                                                            Información Laboral
                                                        </>
                                                    }>
                                                        <>
                                                            <Grid
                                                                container
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 3 }>
                                                                            <FormControl 
                                                                                fullWidth
                                                                                error={ validationJobInformation.jobSituationId.error }>
                                                                                    <Autocomplete
                                                                                        data={ getObjectArrayValueLabel(employmentSituation) }
                                                                                        placeholder='Situación Laboral *'
                                                                                        value={ jobInformation.jobSituationId }
                                                                                        inputRef={ validationJobInformation.jobSituationId.ref }
                                                                                        onChange={ e => {
                                                                                            const value = e?.value || 0
                                                                                            const label = e?.label || ''
                                                                                            handleChangeState({
                                                                                                state: jobInformation,
                                                                                                callSetState: setJobInformation,
                                                                                                callSetValidation: setValidationJobInformation
                                                                                                }, { name: 'jobSituationId', value: value })
                                                                                            handleChangeState({
                                                                                                state: jobInformation,
                                                                                                callSetState: setJobInformation,
                                                                                                callSetValidation: setValidationJobInformation
                                                                                                }, { name: 'jobSituation', value: label })
                                                                                            if (value !== 160001 && value !== 160002)
                                                                                                handleChangeState({
                                                                                                    state: jobInformation,
                                                                                                    callSetState: setJobInformation,
                                                                                                    callSetValidation: setValidationJobInformation
                                                                                                    }, { name: 'jobTitleId', value: 0, validate: false })
                                                                                            if (value !== 160003 && value !== 160004)
                                                                                                handleChangeState({
                                                                                                    state: jobInformation,
                                                                                                    callSetState: setJobInformation,
                                                                                                    callSetValidation: setValidationJobInformation
                                                                                                    }, { name: 'economicActivityId', value: 0, validate: false })
                                                                                        }} />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobInformation.jobSituationId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobInformation.jobSituationId.error &&
                                                                                                        validationJobInformation.jobSituationId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 3 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationJobInformation.jobTitleId.error }>
                                                                                    <Autocomplete
                                                                                        data={ getObjectArrayValueLabel(jobTitle) }
                                                                                        disabled={ jobInformation.jobSituationId !== 160001 && jobInformation.jobSituationId !== 160002 }
                                                                                        placeholder='Cargo Profesión *'
                                                                                        value={ jobInformation.jobTitleId }
                                                                                        inputRef={ validationJobInformation.jobTitleId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setJobInformation,
                                                                                                callSetValidation: setValidationJobInformation
                                                                                                }, { name: 'jobTitle', value: e?.label || '' })
                                                                                            handleChangeState({
                                                                                                callSetState: setJobInformation,
                                                                                                callSetValidation: setValidationJobInformation
                                                                                                }, { name: 'jobTitleId', value: e?.value || 0 })
                                                                                        }} />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobInformation.jobTitleId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobInformation.jobTitleId.error &&
                                                                                                        validationJobInformation.jobTitleId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 3 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationJobInformation.economicActivityId.error }>
                                                                                    <Autocomplete
                                                                                        data={ getObjectArrayValueLabel(economicActivity) }
                                                                                        disabled={ jobInformation.jobSituationId !== 160003 && jobInformation.jobSituationId !== 160004 }
                                                                                        placeholder='Actividad Económica *'
                                                                                        value={ jobInformation.economicActivityId }
                                                                                        inputRef={ validationJobInformation.economicActivityId.ref }
                                                                                        onChange={ e => handleChangeState({
                                                                                            callSetState: setJobInformation,
                                                                                            callSetValidation: setValidationJobInformation
                                                                                            }, { name: 'economicActivityId', value: e?.value || 0 })
                                                                                        } />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobInformation.economicActivityId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobInformation.economicActivityId.error &&
                                                                                                        validationJobInformation.economicActivityId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                error={ validationJobInformation.laborIncomeDate.error }
                                                                                required
                                                                                fullWidth
                                                                                label='Fecha de Ingreso'
                                                                                type='date'
                                                                                margin='normal'
                                                                                format='DD-MM-YYYY'
                                                                                name='laborIncomeDate'
                                                                                value={ jobInformation.laborIncomeDate }
                                                                                inputRef={ validationJobInformation.laborIncomeDate.ref }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setJobInformation,
                                                                                    callSetValidation: setValidationJobInformation
                                                                                    }, e)
                                                                                }
                                                                                onBlur={ e => handleChangeState({
                                                                                    callSetState: setJobInformation,
                                                                                    callSetValidation: setValidationJobInformation
                                                                                    }, e)
                                                                                }
                                                                                InputLabelProps={{
                                                                                    shrink: true
                                                                                }}
                                                                                InputProps={{
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <CalendarTodayIcon
                                                                                                    error={ validationJobInformation.laborIncomeDate.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small' />
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobInformation.laborIncomeDate.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobInformation.laborIncomeDate.error &&
                                                                                                        validationJobInformation.laborIncomeDate.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid 
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 3 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationJobInformation.businessOption.error }>
                                                                                    <Autocomplete
                                                                                        data={ businessOption }
                                                                                        placeholder='Seleccionar RUC | Razón Social *'
                                                                                        value={ jobInformation.businessOption }
                                                                                        inputRef={ validationJobInformation.businessOption.ref }
                                                                                        onChange={ e => {
                                                                                            const handle = {
                                                                                                callSetState: setJobInformation,
                                                                                                callSetValidation: setValidationJobInformation
                                                                                            }
                                                                                            handleChangeState(handle, { name: 'businessOption', value: e?.value || 0 })
                                                                                            if ((e?.value || 0) === 0) {
                                                                                                handleChangeState(handle, { name: 'businessName', value: '' })
                                                                                                handleChangeState(handle, { name: 'ruc', value: '' })
                                                                                            }
                                                                                        } }
                                                                                    />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobInformation.businessOption.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobInformation.businessOption.error &&
                                                                                                        validationJobInformation.businessOption.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 3 }>
                                                                            <div
                                                                                style={{ display: 'flex', position: 'relative' }}>
                                                                                    <div
                                                                                        style={{ width: 'calc(100% - 4.5em)' }}>
                                                                                            {
                                                                                                jobInformation.businessOption === '0'
                                                                                                    ?   <>
                                                                                                            <TextField
                                                                                                                error={ validationJobInformation.ruc.error  }
                                                                                                                required
                                                                                                                fullWidth
                                                                                                                label='Búsqueda RUC'
                                                                                                                type='text'
                                                                                                                margin='normal'
                                                                                                                color='default'
                                                                                                                disabled={ jobInformation.businessOption !== '0' }
                                                                                                                name='ruc'
                                                                                                                value={ jobInformation.ruc }
                                                                                                                onChange={ e => {
                                                                                                                    if (Utils.onlyNumberRegex(e.target.value))
                                                                                                                        handleChangeState({
                                                                                                                            callSetState: setJobInformation,
                                                                                                                            callSetValidation: setValidationJobInformation
                                                                                                                            }, e)
                                                                                                                } }
                                                                                                                onBlur={ e => {
                                                                                                                    if (Utils.onlyNumberRegex(e.target.value))
                                                                                                                        handleChangeState({
                                                                                                                            callSetState: setJobInformation,
                                                                                                                            callSetValidation: setValidationJobInformation
                                                                                                                            }, e)
                                                                                                                } }
                                                                                                                inputRef={ validationJobInformation.ruc.ref }
                                                                                                                onKeyUp={ e => {
                                                                                                                    getFilterByNameBusinessOrRuc('', e.target.value)
                                                                                                                }}
                                                                                                                InputProps={{
                                                                                                                    inputProps:{
                                                                                                                        maxLength: 11
                                                                                                                    },
                                                                                                                    endAdornment: (
                                                                                                                        <InputAdornment 
                                                                                                                            position='end'>
                                                                                                                                <BusinessIcon
                                                                                                                                    color={ validationJobInformation.ruc.error
                                                                                                                                        ?'secondary' : 'inherit' }
                                                                                                                                    fontSize='small'/>
                                                                                                                        </InputAdornment>
                                                                                                                    )
                                                                                                                }}
                                                                                                            />
                                                                                                            <Fade>
                                                                                                                <FormHelperText
                                                                                                                    className={ 'd-flex justify-content-between small' } >
                                                                                                                        <FormHelperText
                                                                                                                            component='span'
                                                                                                                            className={ !business.filteredBusiness.success
                                                                                                                                ? 'text-red' : 'inherit' }>
                                                                                                                                { business.filteredBusiness.error }
                                                                                                                        </FormHelperText>
                                                                                                                        <FormHelperText
                                                                                                                            component='span'
                                                                                                                            className={ validationJobInformation.ruc.error 
                                                                                                                                ? 'text-red' : 'inherit' }>
                                                                                                                                { validationJobInformation.ruc.message }
                                                                                                                        </FormHelperText>
                                                                                                                        <FormHelperText
                                                                                                                            component='span'
                                                                                                                            className={ validationJobInformation.ruc.error
                                                                                                                                ? 'text-red': 'inherit' }>
                                                                                                                                {
                                                                                                                                    `${jobInformation.ruc.length}/11`
                                                                                                                                }
                                                                                                                        </FormHelperText>
                                                                                                                </FormHelperText>
                                                                                                            </Fade>
                                                                                                        </>
                                                                                                    :   <>
                                                                                                            <TextField
                                                                                                                error={ validationJobInformation.businessName.error }
                                                                                                                required
                                                                                                                fullWidth
                                                                                                                label='Búsqueda Razón Social'
                                                                                                                type='text'
                                                                                                                margin='normal'
                                                                                                                disabled={ jobInformation.businessOption !== '1' }
                                                                                                                color='default'
                                                                                                                name='businessName'
                                                                                                                value={ jobInformation.businessName }
                                                                                                                onChange={ e => {
                                                                                                                    if (Utils.onlyTextRegex(e.target.value))
                                                                                                                        handleChangeState({
                                                                                                                            callSetState: setJobInformation,
                                                                                                                            callSetValidation: setValidationJobInformation
                                                                                                                            }, e)
                                                                                                                } }
                                                                                                                onBlur={ e => {
                                                                                                                    if (Utils.onlyTextRegex(e.target.value))
                                                                                                                        handleChangeState({
                                                                                                                            callSetState: setJobInformation,
                                                                                                                            callSetValidation: setValidationJobInformation
                                                                                                                            }, e)
                                                                                                                } }
                                                                                                                inputRef={ validationJobInformation.businessName.ref }
                                                                                                                onKeyUp={ e => {
                                                                                                                    getFilterByNameBusinessOrRuc(e.target.value, '')
                                                                                                                }}
                                                                                                                InputProps={{
                                                                                                                    inputProps:{
                                                                                                                        maxLength: 10000,
                                                                                                                    },
                                                                                                                    endAdornment: (
                                                                                                                        <InputAdornment
                                                                                                                            position='end'>
                                                                                                                                <BusinessIcon
                                                                                                                                    color={ validationJobInformation.businessName.error
                                                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                                                    fontSize='small'/>
                                                                                                                        </InputAdornment>
                                                                                                                    )
                                                                                                                }}
                                                                                                            />
                                                                                                            <Fade>
                                                                                                                <FormHelperText
                                                                                                                    className={ 'd-flex justify-content-between small' } >
                                                                                                                        <FormHelperText
                                                                                                                            component='span'
                                                                                                                            className={ !business.filteredBusiness.success
                                                                                                                                ? 'text-red' : 'inherit' }>
                                                                                                                                { business.filteredBusiness.error }
                                                                                                                        </FormHelperText>
                                                                                                                        <FormHelperText
                                                                                                                            component='span'
                                                                                                                            className={ validationJobInformation.businessName.error 
                                                                                                                                ? 'text-red' : 'inherit' }>
                                                                                                                                { validationJobInformation.businessName.message }
                                                                                                                        </FormHelperText>
                                                                                                                </FormHelperText>
                                                                                                            </Fade>
                                                                                                        </>
                                                                                            }
                                                                                    </div>
                                                                                    <div
                                                                                        style={{ width: '2em', position: 'absolute', bottom: '.5em', right: '2em' }}>
                                                                                        <RegisterModalButton
                                                                                            title={ false }/>
                                                                                    </div>
                                                                            </div>
                                                                            <Popper
                                                                                style={{
                                                                                    zIndex: 900,
                                                                                    position: 'absolute',
                                                                                    maxHeight: 48 * 4.5,
                                                                                    minWidth: 320,
                                                                                    maxWidth: 400
                                                                                }}
                                                                                anchorEl={ 
                                                                                    jobInformation.businessOption === '0' 
                                                                                        ? validationJobInformation.ruc.ref?.current?.currentTarget || null
                                                                                        : validationJobInformation.businessName.ref?.current?.currentTarget || null }
                                                                                aria-haspopup='true'
                                                                                transition
                                                                                open={ openModalRuc }
                                                                                disablePortal>
                                                                                {({ TransitionProps, placement }) => (
                                                                                    <Grow
                                                                                        {...TransitionProps}
                                                                                        id='menu-list-grow'
                                                                                        style={{
                                                                                            transformOrigin: placement === 'bottom' 
                                                                                                ? 'center top' : 'center bottom'
                                                                                        }}
                                                                                    >
                                                                                        <Paper>
                                                                                            <ClickAwayListener
                                                                                                onClickAway={ _ => setOpenModalRuc(false) }>
                                                                                                <MenuList
                                                                                                    className={ classes.menuItem }>
                                                                                                        {
                                                                                                            business.filteredBusiness.data.map((item, index) => 
                                                                                                                <MenuItem
                                                                                                                    key={ index }
                                                                                                                    onClick={ _ => {
                                                                                                                        const handle = {
                                                                                                                            callSetState: setJobInformation,
                                                                                                                            callSetValidation: setValidationJobInformation
                                                                                                                        }
                                                                                                                        handleChangeState(handle, { name: 'ruc', value: item.ruc_emp })
                                                                                                                        handleChangeState(handle, { name: 'businessName', value: item.nom_emp })
                                                                                                                        setOpenModalRuc(false)
                                                                                                                    } }>
                                                                                                                        <Typography
                                                                                                                            noWrap>
                                                                                                                                { item.nom_emp }
                                                                                                                        </Typography>
                                                                                                                </MenuItem>)
                                                                                                        }
                                                                                                </MenuList>
                                                                                            </ClickAwayListener>
                                                                                        </Paper>
                                                                                    </Grow>
                                                                                )}
                                                                            </Popper>

                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                error={ validationJobInformation.grossIncome.error }
                                                                                fullWidth
                                                                                required
                                                                                label='Ingreso Bruto (S/)'
                                                                                margin='normal'
                                                                                color='default'
                                                                                placeholder='0.00'
                                                                                value={ jobInformation.grossIncome }
                                                                                name='grossIncome'
                                                                                inputRef={ validationJobInformation.grossIncome.ref }
                                                                                onChange={ e => {
                                                                                    const grossIncome = e.target?.value?.replace('S/', '') || ''
                                                                                    if (Utils.onlyNumberWithDotRegex(grossIncome || 0))
                                                                                        handleChangeState({
                                                                                            callSetState: setJobInformation,
                                                                                            callSetValidation: setValidationJobInformation
                                                                                        }, { name: 'grossIncome', value: grossIncome })
                                                                                } }
                                                                                onBlur={ e => {
                                                                                    const grossIncome = e.target?.value?.replace('S/', '') || ''
                                                                                    if (Utils.onlyNumberWithDotRegex(grossIncome || 0))
                                                                                        handleChangeState({
                                                                                            callSetState: setJobInformation,
                                                                                            callSetValidation: setValidationJobInformation
                                                                                        }, { name: 'grossIncome', value: grossIncome })
                                                                                } }
                                                                                InputLabelProps={{
                                                                                    shrink: true
                                                                                }}
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: 10
                                                                                    },
                                                                                    inputComponent: NumberMaskMoney,
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <MoneyIcon
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobInformation.grossIncome.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobInformation.grossIncome.error &&
                                                                                                        validationJobInformation.grossIncome.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                error={ validationJobInformation.telephone.error }
                                                                                fullWidth
                                                                                label='Teléfono'
                                                                                type='text'
                                                                                margin='normal'
                                                                                color='default'
                                                                                value={ jobInformation.telephone }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setJobInformation,
                                                                                    callSetValidation: setValidationJobInformation
                                                                                    }, e)
                                                                                }
                                                                                onBlur={ e => handleChangeState({
                                                                                    callSetState: setJobInformation,
                                                                                    callSetValidation: setValidationJobInformation
                                                                                    }, e)
                                                                                }
                                                                                name='telephone'
                                                                                InputLabelProps={{
                                                                                    shrink: true
                                                                                }}
                                                                                InputProps={{
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <PhoneIcon
                                                                                                    color='inherit'
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    ),
                                                                                    inputComponent: TextMaskTelephone
                                                                                }}
                                                                            />
                                                                            <FormHelperText>
                                                                                (Código Región) + Número Telefónico
                                                                            </FormHelperText>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobInformation.telephone.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobInformation.telephone.error &&
                                                                                                        validationJobInformation.telephone.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 3 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Anexo Telefónico'
                                                                                type='text'
                                                                                margin='normal'
                                                                                color='default'
                                                                                value={ jobInformation.telephoneExtension }
                                                                                onChange={ e => {
                                                                                    if (Utils.onlyNumberRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setJobInformation,
                                                                                            callSetValidation: setValidationJobInformation
                                                                                            }, e)
                                                                                } }
                                                                                onBlur={ e => {
                                                                                    if (Utils.onlyNumberRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setJobInformation,
                                                                                            callSetValidation: setValidationJobInformation
                                                                                            }, e)
                                                                                } }
                                                                                name='telephoneExtension'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultTelephoneExtensionLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                            <DialpadIcon
                                                                                                fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                                </Grid>
                                                            </>
                                                </SectionClientInformation>
                                        </Grid>
                                        <Grid
                                            item
                                            xs={ 12 }>
                                                <SectionClientInformation
                                                    data={ jobAddress }
                                                    expanded = { sectionJobAddress }
                                                    extra={ props }
                                                    onClick={ _ => setSectionJobAddress(!sectionJobAddress) }
                                                    title={
                                                        <>
                                                            <BusinessIcon
                                                                fontSize='small'
                                                                className='mr-2'/>
                                                            Dirección Laboral
                                                        </>
                                                    }>
                                                        <>
                                                            <Grid
                                                                container
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationJobAddress.departmentId.error }>
                                                                                    <Autocomplete
                                                                                        disabled={ !department.success }
                                                                                        data={ getObjectArrayValueLabel(department) }
                                                                                        placeholder='Departamento *'
                                                                                        value={ jobAddress.departmentId }
                                                                                        inputRef={ validationJobAddress.departmentId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'departmentId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'department', value: e?.label || '' })
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'provinceId', value: 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'province', value: '' })
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'districtId', value: 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'district', value: '' })
                                                                                            getWorkAddressProvince({ type: 'PR', value: e?.value || 0 })
                                                                                            getWorkAddressDistrict({ type: 'DI', value: 0 })
                                                                                        }}
                                                                                    />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobAddress.departmentId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobAddress.departmentId.error &&
                                                                                                        validationJobAddress.departmentId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationJobAddress.provinceId.error }>
                                                                                    <Autocomplete
                                                                                        disabled={ !province.workAddressProvince.success }
                                                                                        data={ getObjectArrayValueLabel(province.workAddressProvince || []) }
                                                                                        placeholder='Provincia *'
                                                                                        value={ jobAddress.provinceId }
                                                                                        inputRef={ validationJobAddress.provinceId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'provinceId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'province', value: e?.label || '' })
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'districtId', value: 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'district', value: '' })
                                                                                            getWorkAddressDistrict({ type: 'DI', value: e?.value || 0 })
                                                                                        }}
                                                                                    />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobAddress.provinceId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobAddress.provinceId.error &&
                                                                                                        validationJobAddress.provinceId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl
                                                                                fullWidth 
                                                                                error={ validationJobAddress.districtId.error }>
                                                                                    <Autocomplete
                                                                                        disabled={ !district.workAddressDistrict.success }
                                                                                        data={ getObjectArrayValueLabel(district.workAddressDistrict || []) }
                                                                                        placeholder='Distrito *'
                                                                                        value={ jobAddress.districtId }
                                                                                        inputRef={ validationJobAddress.districtId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'districtId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'district', value: e?.label || '' })
                                                                                        }}
                                                                                    />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobAddress.districtId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobAddress.districtId.error &&
                                                                                                        validationJobAddress.districtId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 } 
                                                                        sm={ 6 } 
                                                                        md={ 4 } 
                                                                        lg={ 2 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationJobAddress.viaId.error }>
                                                                                    <Autocomplete
                                                                                        data={ getObjectArrayValueLabel(via) }
                                                                                        placeholder='Vía *'
                                                                                        value={ jobAddress.viaId }
                                                                                        inputRef={ validationJobAddress.viaId.ref }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'viaId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                            }, { name: 'via', value: e?.label || '' })
                                                                                        }}
                                                                                    />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobAddress.viaId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobAddress.viaId.error &&
                                                                                                        validationJobAddress.viaId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 12 }
                                                                        md={ 8 }
                                                                        lg={ 6 }>
                                                                            <TextField
                                                                                error={ validationJobAddress.viaDescription.error }
                                                                                fullWidth
                                                                                required
                                                                                label='Nombre Vía'
                                                                                type='text'
                                                                                margin='normal'
                                                                                color='default'
                                                                                value={ jobAddress.viaDescription }
                                                                                inputRef={ validationJobAddress.viaDescription.ref }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setJobAddress,
                                                                                    callSetValidation: setValidationJobAddress
                                                                                }, e) }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                onBlur={ e => handleChangeState({
                                                                                    callSetState: setJobAddress,
                                                                                    callSetValidation: setValidationJobAddress
                                                                                }, e) }
                                                                                name='viaDescription'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultViaLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <PlaceIcon
                                                                                                    color= { validationJobAddress.viaDescription.error
                                                                                                        ? 'secondary' : 'inherit'
                                                                                                    }
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobAddress.viaDescription.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobAddress.viaDescription.error &&
                                                                                                        validationJobAddress.viaDescription.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                            </Grid>
                                                            <Grid 
                                                                container 
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Número'
                                                                                name='number'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ jobAddress.number }
                                                                                onChange={ e => {
                                                                                    if (Utils.onlyNumberRegex(e.target.value))
                                                                                        handleChangeState({
                                                                                            callSetState: setJobAddress,
                                                                                            callSetValidation: setValidationJobAddress
                                                                                        }, e)
                                                                                } }
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultComplementaryLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <Filter9PlusIcon
                                                                                                    fontSize='small' />
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Departamento'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ jobAddress.building }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setJobAddress,
                                                                                    callSetValidation: setValidationJobAddress
                                                                                }, e) }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                name='building'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultComplementaryLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <HomeIcon
                                                                                                    fontSize='small' />
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Interior'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ jobAddress.insideBuilding }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setJobAddress,
                                                                                    callSetValidation: setValidationJobAddress
                                                                                }, e) }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                name='insideBuilding'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultComplementaryLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <LastPageIcon
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Mz'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ jobAddress.mz }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setJobAddress,
                                                                                    callSetValidation: setValidationJobAddress
                                                                                }, e) }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                name='mz'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultComplementaryLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <CropDinIcon
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 6 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <TextField
                                                                                fullWidth
                                                                                label='Lote'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ jobAddress.lot }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setJobAddress,
                                                                                    callSetValidation: setValidationJobAddress
                                                                                }, e) }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                name='lot'
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: Utils.defaultComplementaryLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <TabUnselectedIcon
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                    </Grid>
                                                            </Grid>
                                                            <Grid
                                                                container
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 4 }
                                                                        md={ 4 }
                                                                        lg={ 2 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationJobAddress.zoneId.error }>
                                                                                    <Autocomplete
                                                                                        data={ getObjectArrayValueLabel(zone) }
                                                                                        placeholder='Zona'
                                                                                        value={ jobAddress.zoneId }
                                                                                        onChange={ e => {
                                                                                            handleChangeState({
                                                                                                state: jobAddress,
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                                }, { name: 'zoneId', value: e?.value || 0 })
                                                                                            handleChangeState({
                                                                                                state: jobAddress,
                                                                                                callSetState: setJobAddress,
                                                                                                callSetValidation: setValidationJobAddress
                                                                                                }, { name: 'zone', value: e?.label || '' })
                                                                                        }} />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobAddress.zoneId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobAddress.zoneId.error &&
                                                                                                        validationJobAddress.zoneId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 8 }
                                                                        md={ 8 }
                                                                        lg={ 6 }>
                                                                            <TextField
                                                                                error={ validationJobAddress.zoneDescription.error }
                                                                                fullWidth
                                                                                label='Nombre Zona'
                                                                                type='text'
                                                                                margin='normal'
                                                                                disabled={ jobAddress.zoneId === 0 }
                                                                                color='default'
                                                                                inputRef={ validationJobAddress.zoneDescription.ref }
                                                                                value={ jobAddress.zoneDescription }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setJobAddress,
                                                                                    callSetValidation: setValidationJobAddress
                                                                                    }, e) 
                                                                                }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                onBlur={ e => handleChangeState({
                                                                                    callSetState: setJobAddress,
                                                                                    callSetValidation: setValidationJobAddress
                                                                                    }, e) 
                                                                                }
                                                                                name='zoneDescription'
                                                                                InputProps={{
                                                                                    inputProps:{
                                                                                        maxLength: Utils.defaultReferenceLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <NearMeIcon
                                                                                                    color={ validationJobAddress.zoneDescription.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationJobAddress.zoneDescription.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationJobAddress.zoneDescription.error &&
                                                                                                        validationJobAddress.zoneDescription.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 12 }
                                                                        md={ 12 }
                                                                        lg={ 12 }>
                                                                            <TextField
                                                                                error={ validationJobAddress.reference.error }
                                                                                fullWidth
                                                                                label='Referencia Dir. Laboral'
                                                                                multiline
                                                                                rows='1'
                                                                                rowsMax='3'
                                                                                type='text'
                                                                                margin='normal'
                                                                                value={ jobAddress.reference }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setJobAddress,
                                                                                    callSetValidation: setValidationJobAddress
                                                                                    }, e) 
                                                                                }
                                                                                onKeyPress={ e => Utils.handleKeyPressTextFieldCheckInput(e) }
                                                                                name='reference'
                                                                                InputProps={{
                                                                                    inputProps:{
                                                                                        maxLength: Utils.defaultReferenceLengthInput
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <LocalLibraryIcon
                                                                                                    fontSize='small' />
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ 'd-flex justify-content-between small' } >
                                                                                        <FormHelperText
                                                                                            component='span'
                                                                                            className={ validationJobAddress.reference.error 
                                                                                                ? 'text-red' : 'inherit' }>
                                                                                                { validationJobAddress.reference.message }
                                                                                        </FormHelperText>
                                                                                        <FormHelperText
                                                                                            component='span'
                                                                                            className={ validationJobAddress.reference.error
                                                                                                ? 'text-red': 'inherit' }>
                                                                                                {
                                                                                                    `${jobAddress.reference.length}/${Utils.defaultReferenceLengthInput}`
                                                                                                }
                                                                                        </FormHelperText>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>       
                                                            </Grid>
                                                        </>
                                                </SectionClientInformation>
                                        </Grid>
                                        <Grid
                                            item
                                            xs={ 12 }>
                                                <SectionClientInformation
                                                    data={ benefits }
                                                    expanded = { sectionBenefits }
                                                    onClick={ _ => setSectionBenefits(!sectionBenefits) }
                                                    title={
                                                        <>
                                                            <LocalOfferIcon
                                                                fontSize='small'
                                                                className='mr-2'/>
                                                            Beneficios y Funcionalidades
                                                        </>
                                                    }>
                                                        <>
                                                            <Grid
                                                                container
                                                                spacing={ 8 }>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationBenefits.paymentDateId.error }>
                                                                                    <Autocomplete
                                                                                        data={ getObjectArrayValueLabel(paymentDate) }
                                                                                        placeholder='Fecha de Pago *'
                                                                                        disabled
                                                                                        value={ benefits.paymentDateId } />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationBenefits.paymentDateId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationBenefits.paymentDateId.error &&
                                                                                                        validationBenefits.paymentDateId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid> 
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationBenefits.sendCorrespondenceId.error }>
                                                                                    <Autocomplete
                                                                                        data={ getObjectArrayValueLabel(sendCorrespondence) }
                                                                                        placeholder='Envío Correspondencia *'
                                                                                        value={ benefits.sendCorrespondenceId }
                                                                                        onChange={ e => handleChangeState({
                                                                                            callSetState: setBenefits,
                                                                                            callSetValidation: setValidationBenefits
                                                                                            }, { name: 'sendCorrespondenceId', value: e?.value || 0 }) 
                                                                                        } />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationBenefits.sendCorrespondenceId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationBenefits.sendCorrespondenceId.error &&
                                                                                                        validationBenefits.sendCorrespondenceId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>      
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl
                                                                                fullWidth
                                                                                error={ validationBenefits.accountStatusId.error }>
                                                                                    <Autocomplete
                                                                                        data={ getObjectArrayValueLabel(accountStatus) }
                                                                                        placeholder='Estado de EECC *'
                                                                                        value={ benefits.accountStatusId }
                                                                                        onChange={ e => handleChangeState({
                                                                                            callSetState: setBenefits,
                                                                                            callSetValidation: setValidationBenefits
                                                                                            }, { name: 'accountStatusId', value: e?.value || 0 }) 
                                                                                        } />
                                                                            </FormControl>
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ classes.labelError }>
                                                                                        <Typography
                                                                                            component='span'
                                                                                            variant='inherit'
                                                                                            color={ validationBenefits.accountStatusId.error 
                                                                                                ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        validationBenefits.accountStatusId.error &&
                                                                                                        validationBenefits.accountStatusId.message
                                                                                                    }
                                                                                        </Typography>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>     
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl 
                                                                                component='fieldset'>
                                                                                <FormLabel 
                                                                                    component='legend'>
                                                                                        Uso de Datos Personales.
                                                                                </FormLabel>
                                                                                <FormControlLabel
                                                                                    control={
                                                                                        <Checkbox
                                                                                            name='authorizePersonalData'
                                                                                            color='primary'
                                                                                            checked={ benefits.authorizePersonalData }
                                                                                            onChange={ e => handleChangeState({
                                                                                                callSetState: setBenefits,
                                                                                                callSetValidation: setValidationBenefits }, {
                                                                                                    name: 'authorizePersonalData', value: e.target.checked
                                                                                                })
                                                                                            }
                                                                                        />
                                                                                    }
                                                                                    label='Autorización del uso de datos personales.'
                                                                                />
                                                                            </FormControl>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <TextField
                                                                                error={ validationBenefits.scannerCode.error ||
                                                                                    !promoter.success }
                                                                                fullWidth
                                                                                required
                                                                                label='Código de Escáner / DNI / Usuario de Red'
                                                                                type='search'
                                                                                margin='normal'
                                                                                name='scannerCode'
                                                                                value={ benefits.scannerCode }
                                                                                onKeyDown={ e => {
                                                                                    Utils.handleEnterKeyPress(e, _ => getPromoter(e.target.value))
                                                                                } }
                                                                                onChange={ e => handleChangeState({
                                                                                    callSetState: setBenefits,
                                                                                    callSetValidation: setValidationBenefits }, e)
                                                                                }
                                                                                onBlur={ e => handleChangeState({
                                                                                    callSetState: setBenefits,
                                                                                    callSetValidation: setValidationBenefits }, e)
                                                                                }
                                                                                InputProps={{
                                                                                    inputProps:{
                                                                                        maxLength: 20
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <CodeIcon
                                                                                                    color={ validationBenefits.scannerCode.error ||
                                                                                                        !promoter.success
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small'/>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ 'd-flex justify-content-between small' } >
                                                                                        <FormHelperText
                                                                                            component='span'
                                                                                            className={ !promoter.success || validationBenefits.scannerCode.error 
                                                                                                ? 'text-red' : 'inherit' }>
                                                                                                { promoter.error || validationBenefits.scannerCode.message }
                                                                                        </FormHelperText>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <TextField
                                                                                error={ validationBenefits.nameScannerCode.error }
                                                                                fullWidth
                                                                                required
                                                                                label='Nombre Promotor'
                                                                                type='text'
                                                                                margin='normal'
                                                                                placeholder='Promotor...'
                                                                                InputLabelProps={{
                                                                                    shrink: true
                                                                                }}
                                                                                value={ benefits.nameScannerCode }
                                                                                InputProps={{
                                                                                    readOnly:true,
                                                                                    endAdornment: (
                                                                                        <InputAdornment
                                                                                            position='end'>
                                                                                                <PermIdentityIcon 
                                                                                                    color={ validationBenefits.nameScannerCode.error
                                                                                                        ? 'secondary' : 'inherit' }
                                                                                                    fontSize='small' />
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <Fade>
                                                                                <FormHelperText
                                                                                    className={ 'd-flex justify-content-between small' } >
                                                                                        <FormHelperText
                                                                                            component='span'
                                                                                            className={ validationBenefits.nameScannerCode.error 
                                                                                                ? 'text-red' : 'inherit' }>
                                                                                                { validationBenefits.nameScannerCode.message }
                                                                                        </FormHelperText>
                                                                                </FormHelperText>
                                                                            </Fade>
                                                                    </Grid>
                                                            </Grid>
                                                        </>
                                                </SectionClientInformation>
                                        </Grid>
                                        <Grid
                                            item
                                            xs={ 12 }>
                                                <SectionClientInformation
                                                    data={ questions }
                                                    expanded = { sectionQuestions }
                                                    onClick={ _ => setSectionQuestions(!sectionQuestions) }
                                                    title={
                                                        <>
                                                            <HelpIcon
                                                                fontSize='small'
                                                                className='mr-2'/>
                                                            Preguntas Adicionales Efectivo Cencosud
                                                        </>
                                                    }>
                                                        <>
                                                            <Grid 
                                                                container
                                                                spacing={8}>
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }
                                                                        sm={ 6 }
                                                                        md={ 4 }
                                                                        lg={ 4 }>
                                                                            <FormControl 
                                                                                required
                                                                                margin='normal'
                                                                                fullWidth
                                                                                color='primary'
                                                                                component='fieldset'>
                                                                                <FormLabel 
                                                                                    component='legend'>
                                                                                    ¿Desea recibir notificaciones de consumo y/o retiros en efectivo por email?
                                                                                </FormLabel>
                                                                                <RadioGroup
                                                                                    row
                                                                                    className='d-flex justify-content-between justify-content-md-start'
                                                                                    onChange={ e => setQuestions(state => ({ ...state, mailAlerts: e.target.value })) }
                                                                                    name='mailAlerts'
                                                                                    value={ questions.mailAlerts }
                                                                                    aria-label='mailAlerts'>
                                                                                        <FormControlLabel 
                                                                                            value={ '1' } 
                                                                                            control={<Radio className='d-flex' color='primary' />} 
                                                                                            label='Sí' />
                                                                                        <FormControlLabel
                                                                                            value={ '0' }
                                                                                            control={<Radio className='d-flex' color='primary' />}
                                                                                            label='No'
                                                                                        />
                                                                                </RadioGroup>
                                                                            </FormControl>  
                                                                    </Grid>
                                                            </Grid>
                                                        </>
                                                </SectionClientInformation>
                                        </Grid>
                                        <Grid
                                            container
                                            spacing={ 8 }
                                            className='d-flex justify-content-center'>
                                                <Grid
                                                    item
                                                    xs={ 12 }
                                                    sm={ 12 }
                                                    md={ 6 }
                                                    lg={ 4 }>
                                                        <Tooltip
                                                            TransitionComponent={ Zoom }
                                                            title='Cancelar Originación'>
                                                                <Bounce>
                                                                    <div
                                                                        className={ classes.wrapper }>
                                                                            <ActionButton
                                                                                text='Cancelar'
                                                                                loading={ loading }
                                                                                type='secondary'
                                                                                handleAction={ _ => handleCancelProcess() }
                                                                                icon={
                                                                                    <CancelIcon 
                                                                                        fontSize='small' 
                                                                                        className='ml-2' />
                                                                                }/>
                                                                    </div>
                                                                </Bounce>
                                                        </Tooltip>
                                                </Grid>
                                                <Grid
                                                    item
                                                    xs={ 12 }
                                                    sm={ 12 }
                                                    md={ 6 }
                                                    lg={ 4 }>
                                                        <Tooltip
                                                            TransitionComponent={ Zoom }
                                                            title='Completar el formulario, para visualizar información'>
                                                                <Bounce>
                                                                    <div
                                                                        className={ classes.wrapper }>
                                                                            <ActionButton
                                                                                text='Previsualizar Información'
                                                                                loading={ loading }
                                                                                type='primary'
                                                                                handleAction={ _ => handlePreviewInformation() }
                                                                                icon={
                                                                                    <SendIcon 
                                                                                        fontSize='small' 
                                                                                        className='ml-2' />
                                                                                }
                                                                                showLoading />
                                                                    </div>
                                                                </Bounce>
                                                        </Tooltip>
                                                </Grid>
                                        </Grid>
                                </Grid>
                            </Fade>
                }
                {
                    (cellphoneValidationSMS.validate && !callCenter) &&
                        <VerifyCellphone
                            client={ personalInformation }
                            origination={ {
                                fullNumber: stateStepClientValidated.completeSolicitudeCode
                            } }
                            onClick={ _ => setCellphoneValidationSMS(state => ({
                                ...state,
                                visiblePopup: !state.visiblePopup
                            })) }
                            onKeyPress={ e => handleSendSMS(e) }
                            onBlur={ e => {
                                if (Utils.onlyNumberRegex(e.target.value))
                                    handleChangeState({
                                        callSetState: setPersonalInformation,
                                        callSetValidation: setValidationPersonalInformation
                                        }, e) 
                            } }
                            onChange={ e => {
                                if (Utils.onlyNumberRegex(e.target.value))
                                    handleChangeState({
                                        callSetState: setPersonalInformation,
                                        callSetValidation: setValidationPersonalInformation
                                        }, e) 
                            } }
                            showVerifyCellphone={ cellphoneValidationSMS.visiblePopup } />
                }
                <PreviewClientInformation
                    open={ showModalPreviewClientInformation }
                    close={ _ => setShowModalPreviewClientInformation(false) }
                    data={ dataModalPreviewClientInformation }
                    handleNextStep={ handleNextStep } />
        </div>
    )
}

function mapStateToProps(state) {
    return {
        documentType: state.identificationDocumentTypeReducer,
        gender: state.genderReducer,
        maritalStatus: state.maritalStatusReducer,
        country: state.countryReducer,
        academicDegree: state.academicDegreeReducer,
        department: state.departmentReducer,
        province: state.provinceReducer,
        district: state.districtReducer,
        via: state.viaReducer,
        zone: state.zoneReducer,
        housingType: state.housingTypeReducer,
        employmentSituation: state.employmentSituationReducer,
        jobTitle: state.jobTitleReducer,
        economicActivity: state.economicActivityReducer,
        business: state.businessReducer,
        paymentDate: state.paymentDateReducer,
        sendCorrespondence: state.sendCorrespondenceReducer,
        accountStatus: state.accountStatusReducer,
        promoter: state.promoterReducer,
        odcMaster: state.odcMasterReducer,
        constantOdc: state.constantODCReducer,
        verifyCellphone: state.verifyCellphoneReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getDocumentType: bindActionCreators(ActionDocumentType.getIdentificationDocumentType, dispatch),
        getGender: bindActionCreators(ActionGender.getGender, dispatch),
        getMaritalStatus: bindActionCreators(ActionMaritalStatus.getMaritalStatus, dispatch),
        getCountry: bindActionCreators(ActionCountry.getCountry, dispatch),
        getAcademicDegree: bindActionCreators(ActionAcademicDegree.getAcademicDegree, dispatch),
        getDepartment: bindActionCreators(ActionDepartment.getDepartment, dispatch),
        getHomeAddressProvince: bindActionCreators(ActionProvince.getHomeAddressProvince, dispatch),
        getWorkAddressProvince: bindActionCreators(ActionProvince.getWorkAddressProvince, dispatch),
        getHomeAddressDistrict: bindActionCreators(ActionDistrict.getHomeAddressDistrict, dispatch),
        getWorkAddressDistrict: bindActionCreators(ActionDistrict.getWorkAddressDistrict, dispatch),
        getVia: bindActionCreators(ActionVia.getVia, dispatch),
        getZone: bindActionCreators(ActionZone.getZone, dispatch),
        getHousingType: bindActionCreators(ActionHousingType.getHousingType, dispatch),
        getEmploymentSituation: bindActionCreators(ActionEmploymentSituation.getEmploymentSituation, dispatch),
        getJobTitle: bindActionCreators(ActionJobTitle.getJobTitle, dispatch),
        getEconomicActivity: bindActionCreators(ActionEconomicActivity.getEconomicActivity, dispatch),
        getFilterByNameBusinessOrRuc: bindActionCreators(ActionBusiness.getFilterByNameBusinessOrRuc, dispatch),
        getPaymentDate: bindActionCreators(ActionPaymentDate.getPaymentDate, dispatch),
        getSendCorrespondence: bindActionCreators(ActionSendCorrespondence.getSendCorrespondence, dispatch),
        getAccountStatus: bindActionCreators(ActionAccountStatus.getAccountStatus, dispatch),
        getPromoter: bindActionCreators(ActionPromoter.getPromoter, dispatch),
        preloadClientMaster: bindActionCreators(ActionOdcMaster.preloadClient, dispatch),
        sendSMSCode: bindActionCreators(ActionVerifyCellphone.sendSMSCode, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ClientInformation))))