// React
import React, { useEffect, useRef, useState } from 'react'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcExistingClient from '../../../../../../actions/odc-existing-client/odc-existing-client'
import * as ActionOdcMaster from '../../../../../../actions/odc-master/odc-master'

// Hooks
import useReduxState from '../../../../../../hooks/general/useReduxState'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../../utils/Notistack'

// Material UI
import { Grid, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Utils
import * as Constants from '../../../../../../utils/Constants'
import * as MasterOrigination from '../../../../../../utils/origination/MasterOrigination'

// Components
import Modal from '../../../../../../components/Modal'
import ActionButton from '../../../../../../components/ActionButton'
import ClientPreview from '../../../../../../components/info-preview/ClientPreview'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function PreviewClientInformation(props) {
    const { classes, open = false, close, data, handleNextStep, odcExistingClient, enqueueSnackbar,
        updateClientInformation, registerActivity, odcMaster, registerClientIntoMaster } = props

    const prevOdcExistingClient = useRef(odcExistingClient)
    const prevOdcMaster = useRef(odcMaster)
    const processRetry = useRef(0)

    const [ loading, setLoading ] = useState(false)
    const [ clientPreviewData, setClientPreviewData ] = useState({})

    const [ dataUpdateClientInformation, isLoadingUpdateClientInformation, isSuccessUpdateClientInformation,
        isErrorUpdateClientInformation ] =
            useReduxState({ props: odcExistingClient.clientInformationUpdated,
                prevProps: prevOdcExistingClient.current.clientInformationUpdated, notify: enqueueSnackbar })
    const [ dataRegisterClientIntoMaster, isLoadingRegisterClientIntoMaster, isSuccessRegisterClientIntoMaster,
        isErrorRegisterClientIntoMaster ] =
            useReduxState({ props: odcMaster.clientRegistered, prevProps: prevOdcMaster.current.clientRegistered,
                notify: enqueueSnackbar })
    const [ dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity ] =
        useReduxState({ props: odcExistingClient.activityRegistered, prevProps: prevOdcExistingClient.current.activityRegistered,
            notify: enqueueSnackbar })

    useEffect(_ => {
        if (open) setLoading(false)
        processRetry.current = 0
    }, [open])

    useEffect(_ => {
        const { clientConsult, personalInformation, personalAddress, jobAddress } = data
        setClientPreviewData({
            documentType: clientConsult?.documentTypeLetter,
            documentNumber: personalInformation?.documentNumber,
            firstName: personalInformation?.firstName,
            sencodName: personalInformation?.sencodName,
            lastName: personalInformation?.firstLastName,
            motherLastName: personalInformation?.secondLastName,
            birthday: personalInformation?.birthday,
            gender: personalInformation?.gender,
            maritalStatus: personalInformation?.maritalStatus,
            nationality: personalInformation?.nationality,
            academicDegree: personalInformation?.academicDegree,
            email: personalInformation?.email,
            telephone: personalInformation?.telephone,
            cellphone: personalInformation?.cellphone,
            homeAddress:{
                department: personalAddress?.department,
                province: personalAddress?.province,
                district: personalAddress?.district,
                via: personalAddress?.via,
                nameVia: personalAddress?.viaDescription,
                number: personalAddress?.number,
                building: personalAddress?.building,
                inside: personalAddress?.insideBuilding,
                mz: personalAddress?.mz,
                lot: personalAddress?.lot,
                zone: personalAddress?.zone,
                nameZone: personalAddress?.zoneDescription,
                housingType: personalAddress?.housingType,
                reference: personalAddress?.reference
            },
            workAddress:{
                department: jobAddress?.department,
                province: jobAddress?.province,
                district: jobAddress?.district,
                via: jobAddress?.via,
                nameVia: jobAddress?.viaDescription,
                number: jobAddress?.number,
                building: jobAddress?.building,
                inside: jobAddress?.insideBuilding,
                mz: jobAddress?.mz,
                lot: jobAddress?.lot,
                zone: jobAddress?.zone,
                nameZone: jobAddress?.zoneDescription,
                housingType: '',
                reference: jobAddress?.reference
            }
        })
    }, [data])

    useEffect(_ => {
        if (isLoadingUpdateClientInformation) setLoading(true)
        if (isSuccessUpdateClientInformation) {
            processRetry.current = 1
            handleContinueProcess()
        }
        if (isErrorUpdateClientInformation) setLoading(false)
    }, [dataUpdateClientInformation, isLoadingUpdateClientInformation, isSuccessUpdateClientInformation, isErrorUpdateClientInformation])

    useEffect(_ => {
        if (isLoadingRegisterClientIntoMaster) setLoading(true)
        if (isSuccessRegisterClientIntoMaster) {
            processRetry.current = 2
            handleContinueProcess()
        }
        if (isErrorRegisterClientIntoMaster) setLoading(false)
    }, [dataRegisterClientIntoMaster, isLoadingRegisterClientIntoMaster, isSuccessRegisterClientIntoMaster, isErrorRegisterClientIntoMaster])

    useEffect(_ => {
        if (isLoadingRegisterActivity) setLoading(true)
        if (isSuccessRegisterActivity) {
            Notistack.getNotistack('Consulta Correcta, 3er Paso Ok!', enqueueSnackbar, 'success')
            setTimeout(_ => {
                handleNextStep()
            }, 1500)
        }
        if (isErrorRegisterActivity) setLoading(false)
    }, [dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity])

    const handleContinueProcess = _ => {
        const steps = getSteps()
        steps[processRetry.current].action()
    }

    const getSteps = _ => ({
        0: {
            action: _ => {
                const client = {
                    ...data.clientConsult,
                    ...data.clientValidated,
                    ...data.personalInformation,
                    currentlyPhaseCode: 100204,
                    previousPhaseCode: 100201,
                    personalAddress: { ...data.personalAddress },
                    jobInformation: { ...data.jobInformation },
                    jobAddress: { ...data.jobAddress },
                    benefits: { ...data.benefits },
                    questions: { ...data.questions }
                }
                updateClientInformation(client)
            }
        },
        1: {
            action: _ => {
                const client = {
                    solicitudeCode: data.clientValidated.solicitudeCode,
                    completeSolicitudeCode: data.clientValidated.completeSolicitudeCode,
                    activityName: MasterOrigination.registerUpdateClientActivity.activityName,
                    phaseCode: MasterOrigination.registerUpdateClientActivity.phaseCode,
                    masterStageCode: Constants.MasterConfigurationStage.masterExistingClientStage
                }
                registerClientIntoMaster(client)
            }
        },
        2: {
            action: _ => {
                const activity = {
                    solicitudeCode: data.clientValidated.solicitudeCode,
                    completeSolicitudeCode: data.clientValidated.completeSolicitudeCode,
                    currentlyPhaseCode: 100301,
                    previousPhaseCode: 100204,
                    activityName: 'Oferta: En Proceso'
                }
                registerActivity(activity, true)
            }
        }
    })

    return (
        <Modal
            title='Información Previa del Cliente'
            body={ <ClientPreview 
                data={ clientPreviewData }/> 
            }
            open={ open }
            size='md'
            actions={ 
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        loading={ loading }
                                        text='Cancelar'
                                        type='secondary'
                                        handleAction={ close }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        text='Aceptar'
                                        type='primary'
                                        loading={ loading }
                                        handleAction={ handleContinueProcess }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
        }/>
    )
}

function mapStateToProps(state) {
    return {
        odcExistingClient: state.odcExistingClientReducer,
        odcMaster: state.odcMasterReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateClientInformation: bindActionCreators(ActionOdcExistingClient.updateClientInformation, dispatch),
        registerClientIntoMaster: bindActionCreators(ActionOdcMaster.registerUpdateClient, dispatch),
        registerActivity: bindActionCreators(ActionOdcExistingClient.registerActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(PreviewClientInformation))))