// React
import React, { useEffect, useRef, useState } from 'react'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionBiometric from '../../../../../actions/generic/biometric'
import * as ActionZytrust from '../../../../../actions/generic/zytrust'
import * as Config from '../../../../../actions/config'
import * as ActionOdcExistingClient from '../../../../../actions/odc-existing-client/odc-existing-client'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Material UI
import { Fab, Grid, Tooltip, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import WarningIcon from '@material-ui/icons/Warning'
import FingerprintIcon from '@material-ui/icons/Fingerprint'
import SendIcon from '@material-ui/icons/Send'
import TouchAppIcon from '@material-ui/icons/TouchApp'

// Material UI - Utils
import yellow from '@material-ui/core/colors/yellow'
import green from '@material-ui/core/colors/green'

// Utils
import classNames from 'classnames'
import * as Utils from '../../../../../utils/Utils'

// Effects
import Bounce from 'react-reveal/Bounce'
import Zoom from 'react-reveal/Zoom'

// Components
import ActionButton from '../../../../../components/ActionButton'
import ItemListActivationOrigination from './components/ItemListActivationOrigination'
import ModalContingencyProcess from './components/ModalContingencyProcess'

const styles = theme => ({
    rootWrapper: {
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    root: {
        minWidth: 300,
        maxWidth: 700,
        margin: '0 auto'
    },
    buttonActionsOpenModal: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    contingencyFabWrapper: {
        zIndex: 900,
        [theme.breakpoints.up('md')]: {
            position: 'absolute',
            bottom: 20,
            right: 20
        }
    },
    contingencyfab: {
        backgroundColor: '#ffa000',
        transition: '.3s',
        '&:hover': {
            backgroundColor: yellow[900]
        }
    },
    diplayFlexFabCenter: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonActions: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    activationCreditCard: {
        backgroundColor: green[700],
        transition: '.3s',
        '&:hover': {
            backgroundColor: green[900]
        }
    }
})

function ActivationOrigination(props) {
    const { classes, biometricActivity, biometric, validateBiometric, validateBiometricZytrustService, odcExistingClient,
        zytrust, enqueueSnackbar, stateStepClientConsult, stateStepClientValidated, biometricZytrustService,
        registerActivity, handleNextStep, isBiometricOk, phaseCodeFromContinueProcess, stateStepCommercialOffer, handleReset } = props

    const prevZytrust = useRef(zytrust)
    const prevBiometric = useRef(biometric)
    const prevOdcExistingClient = useRef(odcExistingClient)
    const processRetry = useRef(0)
    const isFinishProcess = useRef(false)
    const isCurrentProcess = useRef(false)
    const isProcessTypeTransferency = useRef(false)

    const [ validateIdentityButton, setvalidateIdentityButton ] = useState({
        disabled: false,
        loading: false
    })
    const [ activateECButton, setActivateECButton ] = useState({
        disabled: true,
        loading: false
    })
    const [ contingencyButton, setContingencyButton ] = useState({
        disabled: true
    })
    const [ showModalContingencyProcess, setShowModalContingencyProcess ] = useState(false)
    const [ activationBiometric, setActivationBiometric ] = useState({
        countConnectionZytrust: 1,
        isBiometricOk: false
    })

    useEffect(_ => {
        if (phaseCodeFromContinueProcess === 100704 || isBiometricOk) {
            setActivateECButton(state => ({ ...state, disabled: false }))
            setvalidateIdentityButton(state => ({ ...state, disabled: true }))
        }
        setActivationBiometric(state => ({ ...state, isBiometricOk: isBiometricOk }))
        window.addEventListener('focus', validateBiometricZytrustService )
        window.addEventListener('blur', validateBiometricZytrustService )
        return _ => {
            window.removeEventListener('focus', validateBiometricZytrustService )
            window.removeEventListener('blur', validateBiometricZytrustService )
        }
    }, [])

    useEffect(_ => {
        if (prevZytrust.current.responseBioZytrustService !== zytrust.responseBioZytrustService) {
            if (zytrust.responseBioZytrustService.loading) {
                setContingencyButton({ disabled: true })
                setvalidateIdentityButton({ disabled: true, loading: true })
            } else {
                const { data, error } = zytrust.responseBioZytrustService
                const message = data || error
                const sendData = {
                    solicitudeCode: stateStepClientValidated.solicitudeCode,
                    completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
                    intentos: activationBiometric.countConnectionZytrust,
                    codeError: message.messageCode || 0,
                    resultCode: message.resultCode || 0,
                    message: message.message,
                    restriccion: message.restriccion,
                    validateXML: message.validateXML
                }
                validateBiometric(sendData)
            }
        }
    }, [zytrust.responseBioZytrustService])

    useEffect(_ => {
        Utils.resolveStateRedux(prevBiometric.current.biometricValidation, biometric.biometricValidation, 
            _ => {
                setContingencyButton({ disabled: true })
                setvalidateIdentityButton({ disabled: true, loading: true })
            },
            _ => {
                const message = zytrust.responseBioZytrustService.data || zytrust.responseBioZytrustService.error
                const isSuccess = biometric.biometricValidation.data.color === 'success'
                let sendData = {
                    tipo_doc: stateStepClientConsult.documentType,
                    nro_doc: stateStepClientConsult.documentNumber,
                    tipo_doc_letra: stateStepClientConsult.documentTypeLetter,
                    inputXml: message.inputXML,
                    outputXml: message.outputXML,
                    errorCodeBiometrico: isSuccess ? '' : message.messageCode,
                    errorMessageBiometrico: isSuccess ? '' : message.message,
                    nom_actividad: 'Activación CE: Biometría',
                    cod_flujo_fase_estado_actual: 100702,
                    cod_flujo_fase_estado_anterior: 100701,
                    cod_solicitud: stateStepClientValidated.solicitudeCode,
                    cod_solicitud_completa: stateStepClientValidated.completeSolicitudeCode,
                    cod_cliente: stateStepClientValidated.clientCode
                }
                biometricActivity(sendData)
            },
            warningMessage => {
                setvalidateIdentityButton({ disabled: false, loading: false })
                setActivationBiometric(state => ({ ...state, isBiometricOk: false }))
                Notistack.getNotistack(`Biometría: ${ warningMessage }`, enqueueSnackbar, 'warning')
            }, 
            errorMessage => {
                setvalidateIdentityButton({ disabled: false, loading: false })
                setActivationBiometric(state => ({ ...state, isBiometricOk: false }))
                Notistack.getNotistack(`Biometría: ${ errorMessage }`, enqueueSnackbar, 'error')
            })
    }, [biometric.biometricValidation])

    useEffect(_ => {
        Utils.resolveStateRedux(prevBiometric.current.biometricActivity, biometric.biometricActivity, null, _ => {
            const { des_error, color, btnActivar, btnContingencia, btnValidarIdentidad } = biometric.biometricValidation.data
            if (color === 'warning') setActivationBiometric(state => ({ ...state, countConnectionZytrust: activationBiometric.countConnectionZytrust + 1 }))
            setActivationBiometric(state => ({ ...state, isBiometricOk: btnActivar }))
            setContingencyButton({ disabled: !btnContingencia })
            setvalidateIdentityButton({ disabled: !btnValidarIdentidad, loading: false })
            setActivateECButton(state => ({ ...state, disabled: !btnActivar }))
            if (des_error) {
                const messages = des_error.split('||')
                Notistack.getNotistack(`Biometría: ${ messages[0] }`, enqueueSnackbar, color)
                if (btnContingencia && messages[1])
                    Notistack.getNotistack(messages[1], enqueueSnackbar, 'warning', 10000)
            }
        }, 
        warningMessage => Notistack.getNotistack(`Biometría: ${ warningMessage }`, enqueueSnackbar, 'warning'), 
        errorMessage => Notistack.getNotistack(`Biometría: ${ errorMessage }`, enqueueSnackbar, 'error'))
    }, [biometric.biometricActivity])

    useEffect(_ => {
        if (isCurrentProcess.current)
            Utils.resolveStateRedux(prevOdcExistingClient.current.activityRegistered, odcExistingClient.activityRegistered, 
                _ => setActivateECButton({ disabled: true, loading: true }),
                _ => {
                    if (isFinishProcess.current) {
                        Notistack.getNotistack('Consulta Correcta, 7mo Paso Ok!', enqueueSnackbar, 'success')
                        setTimeout(_ => {
                            handleNextStep()
                        }, 1500)
                    }
                    else {
                        processRetry.current += 1
                        handleContinueProcess()
                    }
                },
                warningMessage => {
                    isCurrentProcess.current = false
                    setActivateECButton({ disabled: false, loading: false })
                    Notistack.getNotistack(warningMessage, enqueueSnackbar, 'warning')
                }, 
                errorMessage => {
                    isCurrentProcess.current = false
                    setActivateECButton({ disabled: false, loading: false })
                    Notistack.getNotistack(errorMessage, enqueueSnackbar, 'error')
                })
    }, [odcExistingClient.activityRegistered])

    const handleBiometricZytrustService = _ => {
        biometricZytrustService({
            tiDocCliente: stateStepClientConsult.documentType,
            nuDocCliente: stateStepClientConsult.documentNumber
        })
    }

    const handleContinueProcess = _ => {
        isProcessTypeTransferency.current = stateStepCommercialOffer.disbursementTypeId === 380001
        isCurrentProcess.current = true
        if (processRetry.current === 0) {
            const activity = {
                solicitudeCode: stateStepClientValidated.solicitudeCode,
                completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
                currentlyPhaseCode: 100705,
                previousPhaseCode: 100704,
                activityName: 'Activación CE: Aceptado'
            }
            registerActivity(activity, true)
        } else if (processRetry.current === 1) {
            const activity = {
                solicitudeCode: stateStepClientValidated.solicitudeCode,
                completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
                currentlyPhaseCode: 100801,
                previousPhaseCode: 100705,
                activityName: 'Fin Proceso CE'
            }
            isFinishProcess.current = !isProcessTypeTransferency.current
            registerActivity(activity, true)
        }
        else if (processRetry.current === 2 && isProcessTypeTransferency.current) {
            const activity = {
                solicitudeCode: stateStepClientValidated.solicitudeCode,
                completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
                currentlyPhaseCode: 100802,
                previousPhaseCode: 100801,
                activityName: 'Pendiente Atención CE'
            }
            isFinishProcess.current = true
            registerActivity(activity, true)
        }
    }

    return (
        <>
            <Grid
                container
                spacing={ 8 }>
                    <Grid
                        item
                        xs={ 12 }>
                            <Grid
                                container
                                spacing={ 8 }
                                className={ classNames(classes.rootWrapper, 'p-sm-5') }>
                                <Grid
                                    item
                                    xs={ 12 }>
                                        <Grid
                                            container
                                            spacing={ 8 }
                                            className={ classes.root }>
                                                <Grid
                                                    item
                                                    xs={ 12 }
                                                    className='mb-2'>
                                                        <Typography
                                                            align='center'
                                                            component='h2'
                                                            variant='h6'>
                                                                Validación de Identidad
                                                        </Typography>
                                                </Grid>
                                        <Grid
                                            item
                                            xs={ 12 }
                                            className='mb-2'>
                                                <Grid
                                                    container
                                                    spacing={ 8 }>
                                                        <ItemListActivationOrigination>
                                                            Realizar el proceso de generación de clave en el Sistema
                                                            <span className='font-weight-bold'> MIG</span>.
                                                        </ItemListActivationOrigination>
                                                        <ItemListActivationOrigination>
                                                            Validación Biométrica al Cliente.
                                                        </ItemListActivationOrigination>
                                                        <ItemListActivationOrigination>
                                                            Si luego de 3 intentos el cliente no ha podido ser validado, realizar la
                                                            <span className='font-weight-bold'> Modalidad de Contingencia</span>.
                                                        </ItemListActivationOrigination>
                                                </Grid>
                                        </Grid>
                                        <Grid
                                            item
                                            xs={ 12 }
                                            className='mb-2'>
                                                <Grid
                                                    container 
                                                    spacing={ 8 }
                                                    className={ classes.buttonActionsOpenModal }>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 6 }
                                                            className={ classes.contingencyFabWrapper }>
                                                                <Bounce>
                                                                    <div
                                                                        className={ classes.diplayFlexFabCenter }>
                                                                            <Fab
                                                                                disabled={ contingencyButton.disabled }
                                                                                variant='extended'
                                                                                onClick={ _ => setShowModalContingencyProcess(true) }
                                                                                className={ classes.contingencyfab }>
                                                                                    <WarningIcon
                                                                                        className='text-white'
                                                                                        fontSize='small'
                                                                                        color='inherit' />
                                                                            </Fab>
                                                                            <Typography
                                                                                className='font-weight-bolder m-2 d-lg-none'>
                                                                                    Modalidad de Contingencia
                                                                            </Typography>
                                                                    </div>
                                                                </Bounce>
                                                        </Grid>
                                                </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid
                                container
                                spacing={ 8 }
                                className={ classes.buttonActions }>
                                    <Grid
                                        item
                                        xs={ 12 }>
                                            <Typography
                                                align='center'>
                                                    {
                                                        zytrust.validateBioZytrustService.data ? 
                                                        zytrust.validateBioZytrustService.data.online ? 
                                                            <Typography 
                                                                component='span'
                                                                className='font-weight-bolder text-green'>
                                                                    {
                                                                        zytrust.validateBioZytrustService.data.message
                                                                    }
                                                                    <TouchAppIcon 
                                                                        fontSize='small'
                                                                        className='ml-2' />
                                                            </Typography>
                                                            :
                                                            <Typography 
                                                                color='secondary'
                                                                component='span'
                                                                className='font-weight-bolder'>
                                                                    {
                                                                        zytrust.validateBioZytrustService.data.message ?
                                                                            <>
                                                                                { zytrust.validateBioZytrustService.data.message }
                                                                                <a 
                                                                                    href={ Config.URL_VALIDATE_ZYTRUST }
                                                                                    className='ml-2'
                                                                                    target='_blank'
                                                                                    rel='noopener noreferrer' >
                                                                                        Click Aqui
                                                                                </a>
                                                                            </> : null
                                                                    }
                                                            </Typography>
                                                        :
                                                        <Typography 
                                                            color='error'
                                                            component='span'
                                                            className='font-weight-bolder'>
                                                                {
                                                                    zytrust.validateBioZytrustService.data.message
                                                                }
                                                                Click Aqui
                                                        </Typography>
                                                    }
                                            </Typography>
                                    </Grid>
                                    <Grid
                                        item
                                        xs={ 12 }
                                        sm={ 12 }
                                        md={ 4 }
                                        lg={ 3 }>
                                            <Tooltip
                                                TransitionComponent={ Zoom }
                                                title='Click para validar la identidad del cliente'>
                                                <Bounce>
                                                    <ActionButton
                                                        loading={ validateIdentityButton.disabled }
                                                        text='Validar Identidad'
                                                        type='primary'
                                                        handleAction={ () => handleBiometricZytrustService() }
                                                        icon={
                                                            <FingerprintIcon
                                                                fontSize='small'
                                                                className='ml-2' />
                                                        }
                                                        showLoading={ validateIdentityButton.loading }
                                                    />
                                                </Bounce>
                                            </Tooltip>
                                    </Grid>
                                    <Grid
                                        item
                                        xs={ 12 }
                                        sm={ 12 }
                                        md={ 4 }
                                        lg={ 3 }>
                                            <Tooltip
                                                TransitionComponent={ Zoom }
                                                title='Click para finalizar la originación'>
                                                <Bounce>
                                                    <ActionButton
                                                        loading={ activateECButton.disabled }
                                                        text='Finalizar'
                                                        type='primary'
                                                        className={ classes.activationCreditCard }
                                                        handleAction={ () => handleContinueProcess() }
                                                        icon={
                                                            <SendIcon
                                                                fontSize='small'
                                                                className='ml-2' />
                                                        }
                                                        showLoading={ activateECButton.loading }
                                                    />
                                                </Bounce>
                                            </Tooltip>
                                    </Grid>
                            </Grid>
                    </Grid>
            </Grid>
            <ModalContingencyProcess
                stateStepClientValidated={ stateStepClientValidated }
                open={ showModalContingencyProcess }
                close={ _ => setShowModalContingencyProcess(!showModalContingencyProcess) }
                handleReset={ handleReset } />
        </>
    )
}

function mapStateToProps(state) {
    return {
        odcExistingClient: state.odcExistingClientReducer,
        zytrust: state.zytrustReducer,
        biometric: state.biometricReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        biometricActivity: bindActionCreators(ActionBiometric.biometricActivity, dispatch),
        validateBiometric: bindActionCreators(ActionBiometric.validateBiometric, dispatch),
        validateBiometricZytrustService: bindActionCreators(ActionZytrust.validateBiometricZytrustService, dispatch),
        biometricZytrustService: bindActionCreators(ActionZytrust.biometricZytrustService, dispatch),
        registerActivity: bindActionCreators(ActionOdcExistingClient.registerActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ActivationOrigination))))