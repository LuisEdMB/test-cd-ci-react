// React
import React, { useEffect, useRef, useState } from 'react'

// Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Redux - Actions
import * as ActionOdcExistingClient from '../../../../../../actions/odc-existing-client/odc-existing-client'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../../utils/Notistack'

// Utils
import * as Utils from '../../../../../../utils/Utils'

// Material UI
import { Grid, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Components
import Modal from '../../../../../../components/Modal'
import ActionButton from '../../../../../../components/ActionButton'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalContingencyProcess(props) {
    const { open, close, classes, odcExistingClient, registerActivity, enqueueSnackbar,
        stateStepClientValidated, handleReset } = props

    const prevOdcExistingClient = useRef(odcExistingClient)
    const isCurrentProcess = useRef(false)

    const [ loading, setLoading ] = useState(false)

    useEffect(_ => {
        if (isCurrentProcess.current)
        Utils.resolveStateRedux(prevOdcExistingClient.current.activityRegistered, odcExistingClient.activityRegistered, 
            _ => setLoading(true),
            _ => {
                Notistack.getNotistack('Pendiente de Activación de EC.', enqueueSnackbar, 'info')
                setTimeout(_ => {
                    handleReset()
                }, 1500);
            },
            warningMessage => {
                isCurrentProcess.current = false
                setLoading(false)
                Notistack.getNotistack(warningMessage, enqueueSnackbar, 'warning')
            }, 
            errorMessage => {
                isCurrentProcess.current = false
                setLoading(false)
                Notistack.getNotistack(errorMessage, enqueueSnackbar, 'error')
            })
    }, [odcExistingClient.activityRegistered])

    const handleContingencyActivation = _ => {
        isCurrentProcess.current = true
        const activity = {
            solicitudeCode: stateStepClientValidated.solicitudeCode,
            completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
            currentlyPhaseCode: 100703,
            previousPhaseCode: 100702,
            activityName: 'Activación CE: Pendiente'
        }
        registerActivity(activity, true)
    }

    return (
        <Modal
            title='Pendiente de Activación'
            body={
                <Typography
                    align='center'>
                        El Efectivo Cencosud estará en la bandeja de pendientes por activar.
                </Typography>
            }
            open={ open } 
            actions={ 
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        loading={ loading }
                                        text='Cancelar'
                                        type='secondary'
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        loading={ loading }
                                        text='Aceptar'
                                        type='primary'
                                        handleAction={ _ => handleContingencyActivation() }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
        }/>
    )
}

function mapStateToProps(state) {
    return {
        odcExistingClient: state.odcExistingClientReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerActivity: bindActionCreators(ActionOdcExistingClient.registerActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalContingencyProcess))))