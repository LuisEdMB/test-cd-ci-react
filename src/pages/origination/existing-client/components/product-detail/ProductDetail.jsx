// React
import React, { useEffect, useRef, useState } from 'react'

// Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Redux - Actions
import * as ActionOdcExistingClient from '../../../../../actions/odc-existing-client/odc-existing-client'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Material UI
import { Grid, TextField, Tooltip, withStyles } from '@material-ui/core'

// Material UI - Icons
import CancelIcon from '@material-ui/icons/Cancel'
import SendIcon from '@material-ui/icons/Send'

// Utils
import classNames from 'classnames'
import * as Utils from '../../../../../utils/Utils'

// Effects
import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import Bounce from 'react-reveal/Bounce'

// Media
import LogoCencosudCash from '../../../../../assets/media/images/jpg/CEC-logo-1.jpg'

// Components
import ActionButton from '../../../../../components/ActionButton'
import Image from '../../../../../components/Image'

const styles = {
    root: {
        maxWidth: 900
    },
    wrapper: {
        position: 'relative'
    }
}

function ProductDetail(props) {
    const { classes, stateStepClientConsult, stateStepClientValidated, stateStepClientInformation,
        stateStepCommercialOffer, showModalCancelActivity, odcExistingClient, registerActivity,
        enqueueSnackbar, handleNextStep, registerAccountCard, callCenter, finishCallOrigination } = props

    const prevOdcExistingClient = useRef(odcExistingClient)
    const isFinish = useRef(false)
    const processRetry = useRef(0)
    const isCurrentlyProcess = useRef(false)

    const [ loading, setLoading ] = useState(false)

    useEffect(_ => {
        if (isCurrentlyProcess.current)
            Utils.resolveStateRedux(prevOdcExistingClient.current.activityRegistered, odcExistingClient.activityRegistered, 
                _ => setLoading(true), _ => {
                    if (isFinish.current) {
                        Notistack.getNotistack('Originación Express: Aceptado', enqueueSnackbar, 'info');
                        setTimeout(_ => {
                            Notistack.getNotistack('Consulta Correcta, 5to Paso Ok!', enqueueSnackbar, 'success');
                        }, 1500)
                        setTimeout(_ => {
                            handleNextStep();
                        }, 2500)
                    }
                    else{
                        processRetry.current += 1
                        handleContinueProcess()
                    }
                }, warningMessage => {
                    setLoading(false)
                    isCurrentlyProcess.current = false
                    Notistack.getNotistack(warningMessage, enqueueSnackbar, 'warning')
                }, errorMessage => {
                    setLoading(false)
                    isCurrentlyProcess.current = false
                    Notistack.getNotistack(errorMessage, enqueueSnackbar, 'warning')
                }
            )
    }, [odcExistingClient.activityRegistered])

    useEffect(_ => {
        if (isCurrentlyProcess.current)
            Utils.resolveStateRedux(prevOdcExistingClient.current.accountCardRegistered, odcExistingClient.accountCardRegistered,
                _ => setLoading(true), _ => {
                    processRetry.current = 2
                    handleContinueProcess()
                }, warningMessage => {
                    setLoading(false)
                    isCurrentlyProcess.current = false
                    Notistack.getNotistack(warningMessage, enqueueSnackbar, 'warning')
                }, errorMessage => {
                    setLoading(false)
                    isCurrentlyProcess.current = false
                    Notistack.getNotistack(errorMessage, enqueueSnackbar, 'warning')
                }
            )
    }, [odcExistingClient.accountCardRegistered])

    useEffect(_ => {
        Utils.resolveStateRedux(prevOdcExistingClient.current.callOriginationFinished, odcExistingClient.callOriginationFinished, 
            _ => setLoading(true), _ => {
                Notistack.getNotistack('Originación Express: Aceptado', enqueueSnackbar, 'info');
                setTimeout(_ => {
                    Notistack.getNotistack('Consulta Correcta, 5to Paso Ok!', enqueueSnackbar, 'success');
                }, 1500)
                setTimeout(_ => {
                    handleNextStep();
                }, 2500)
            }, warningMessage => {
                setLoading(false)
                isCurrentlyProcess.current = false
                Notistack.getNotistack(warningMessage, enqueueSnackbar, 'warning')
            }, errorMessage => {
                setLoading(false)
                isCurrentlyProcess.current = false
                Notistack.getNotistack(errorMessage, enqueueSnackbar, 'warning')
            })
    }, [odcExistingClient.callOriginationFinished])

    const handleCancelProcess = _ => {
        const activity = {
            solicitudeCode: stateStepClientValidated.solicitudeCode,
            completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
            currentlyPhaseCode: 100403,
            previousPhaseCode: 100401,
            activityName: 'Detalle Producto: Cancelado'
        }
        showModalCancelActivity(activity)
    }

    const handleContinueProcess = _ => {
        isCurrentlyProcess.current = true
        if (processRetry.current === 0) {
            const activity = {
                solicitudeCode: stateStepClientValidated.solicitudeCode,
                completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
                currentlyPhaseCode: 100402,
                previousPhaseCode: 100401,
                activityName: 'Detalle Producto: Aceptado'
            }
            registerActivity(activity)
        } else if (processRetry.current === 1) {
            const data = {
                ...stateStepClientConsult,
                ...stateStepClientValidated,
                currentlyPhaseCode: 100502,
                previousPhaseCode: 100501,
                activityName: 'Originación Efectivo: Crear Cuenta y EC'
            }
            registerAccountCard(data)
        } else if (processRetry.current === 2) {
            const activity = {
                solicitudeCode: stateStepClientValidated.solicitudeCode,
                completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
                currentlyPhaseCode: 100601,
                previousPhaseCode: 100503,
                activityName: 'Resumen: En Proceso'
            }
            isFinish.current = !callCenter
            registerActivity(activity, true)
        } else if (processRetry.current === 3) {
            const activity = {
                solicitudeCode: stateStepClientValidated.solicitudeCode,
                completeSolicitudeCode: stateStepClientValidated.completeSolicitudeCode,
                currentlyPhaseCode: 100602,
                previousPhaseCode: 100601,
                activityName: 'Resumen: Aceptado'
            }
            finishCallOrigination(activity)
        }
    }

    return (
        <Grid
            container
            spacing={ 8 }
            className='w-100 p-1'>
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid 
                            item xs={12}
                            style={{ position:'relative' }} 
                            className='d-flex justify-content-center p-2 pb-4 p-sm-5 mb-2 rounded'>
                                <Grid
                                    container
                                    spacing={ 32 }
                                    className={ classNames(classes.root, 'm-0 m-sm-3') }>
                                        <Grid
                                            item
                                            xs={ 12 }
                                            sm={ 12 }
                                            md={ 6 }
                                            lg={ 6 }>
                                                <Grid
                                                    container
                                                    spacing={ 8 }>
                                                        <Grid
                                                            item
                                                            xs={ 6 }>
                                                                <TextField
                                                                    fullWidth
                                                                    required
                                                                    label='Tipo Documento'
                                                                    type='text'
                                                                    margin='normal'
                                                                    variant='outlined'
                                                                    color='default'
                                                                    value={ stateStepClientConsult?.documentTypeLetter || '' }
                                                                    InputProps={{
                                                                        readOnly: true
                                                                    }}
                                                                />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 6 }>
                                                                <TextField
                                                                    fullWidth
                                                                    required
                                                                    label='Número Documento'
                                                                    type='text'
                                                                    margin='normal'
                                                                    variant='outlined'
                                                                    color='default'
                                                                    value={ stateStepClientConsult?.documentNumber || '' }
                                                                    InputProps={{
                                                                        readOnly: true
                                                                    }}
                                                                />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }>
                                                                <TextField
                                                                    fullWidth
                                                                    required
                                                                    label='Nombre Completo'
                                                                    type='text'
                                                                    margin='normal'
                                                                    variant='outlined'
                                                                    color='default'
                                                                    value={ `${ stateStepClientInformation?.personalInformation?.firstLastName || '' } ` +
                                                                        `${ stateStepClientInformation?.personalInformation?.secondLastName || '' } ` + 
                                                                        `${ stateStepClientInformation?.personalInformation?.firstName || '' } ` + 
                                                                        `${ stateStepClientInformation?.personalInformation?.secondName || '' }` }
                                                                    InputProps={{
                                                                        readOnly: true
                                                                    }}
                                                                />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 6 }
                                                            md={ 6 }
                                                            lg={ 6 }>
                                                                <TextField
                                                                    fullWidth
                                                                    required
                                                                    label='Línea de CEC'
                                                                    type='text'
                                                                    margin='normal'
                                                                    variant='outlined'
                                                                    color='default'
                                                                    value={ `S/ ${ Utils.getMoneyFormat(stateStepCommercialOffer?.lineAvailable?.value || 0) }` }
                                                                    inputProps={{
                                                                        style: { textAlign: 'right' },
                                                                        readOnly: true
                                                                    }}
                                                                />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 6 }
                                                            md={ 6 }
                                                            lg={ 6 }>
                                                                <TextField
                                                                    fullWidth
                                                                    required
                                                                    label='PCT EC'
                                                                    type='text'
                                                                    margin='normal'
                                                                    variant='outlined'
                                                                    color='default'
                                                                    value={ stateStepClientValidated?.pct || '' }
                                                                    InputProps={{
                                                                        readOnly: true
                                                                    }}
                                                                />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 6 }
                                                            md={ 6 }
                                                            lg={ 4 }>
                                                                <TextField
                                                                    fullWidth
                                                                    required
                                                                    label='Número de Cuotas'
                                                                    type='text'
                                                                    margin='normal'
                                                                    variant='outlined'
                                                                    color='default'
                                                                    value={ stateStepCommercialOffer?.numberFees || '' }
                                                                    InputProps={{
                                                                        readOnly: true
                                                                    }}
                                                                />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 6 }
                                                            md={ 6 } 
                                                            lg={ 4 }>
                                                                <TextField
                                                                    fullWidth
                                                                    required
                                                                    label='Valor aprox. Cuotas'
                                                                    type='text'
                                                                    margin='normal'
                                                                    variant='outlined'
                                                                    color='default'
                                                                    value={ `S/ ${ Utils.getMoneyFormat(stateStepCommercialOffer?.feeAmount || 0) }` }
                                                                    InputProps={{
                                                                        readOnly: true
                                                                    }}
                                                                />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 6 }
                                                            md={ 6 }
                                                            lg={ 4 }>
                                                                <TextField
                                                                    fullWidth
                                                                    required
                                                                    label='TCEA'
                                                                    type='text'
                                                                    margin='normal'
                                                                    variant='outlined'
                                                                    color='default'
                                                                    value={ `${ parseFloat(stateStepCommercialOffer?.tcea || 0).toFixed(2) } %` }
                                                                    InputProps={{
                                                                        readOnly: true
                                                                    }}
                                                                />
                                                        </Grid>
                                                </Grid>
                                        </Grid>
                                        <Grid
                                            item
                                            xs={ 12 }
                                            sm={ 12 }
                                            md={ 6 }
                                            lg={ 6 }>
                                                <Fade>
                                                    <Image
                                                        src={ LogoCencosudCash }
                                                        alt='LogoCencosudCash'/>
                                                </Fade>
                                        </Grid>
                                </Grid>
                        </Grid>
                </Grid>
                <Grid
                    container
                    spacing={ 8 }
                    className='d-flex justify-content-center align-items-center'>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 12 }
                        md={ 6 }
                        lg={ 3 }>
                            <Tooltip
                                TransitionComponent={ Zoom }
                                title='Cancelar originación'>
                                    <Bounce>
                                        <div 
                                            className={ classes.wrapper }>
                                                <ActionButton
                                                    text='Cancelar'
                                                    loading={ loading }
                                                    type='secondary'
                                                    handleAction={ () => handleCancelProcess() }
                                                    icon={
                                                        <CancelIcon
                                                            fontSize='small'
                                                            className='ml-2' />
                                                    }
                                                />
                                        </div>
                                    </Bounce>
                            </Tooltip>
                    </Grid>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 12 }
                        md={ 6 }
                        lg={ 3 }>
                            <Tooltip
                                TransitionComponent={ Zoom }
                                title='Generar un nuevo producto'>
                                <div>
                                    <Bounce>
                                        <div
                                            className={ classes.wrapper }>
                                                <ActionButton
                                                    text='Generar Crédito Efectivo'
                                                    loading={ loading }
                                                    type='primary'
                                                    handleAction={ () => handleContinueProcess() }
                                                    icon={
                                                        <SendIcon
                                                            fontSize='small'
                                                            className='ml-2' />
                                                    }
                                                    showLoading
                                                />
                                        </div>
                                    </Bounce>
                                </div>
                            </Tooltip>
                    </Grid>
                </Grid>
        </Grid>
    )
}

function mapStateToProps(state) {
    return {
        odcExistingClient: state.odcExistingClientReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerAccountCard: bindActionCreators(ActionOdcExistingClient.registerAccountCard, dispatch),
        registerActivity: bindActionCreators(ActionOdcExistingClient.registerActivity, dispatch),
        finishCallOrigination: bindActionCreators(ActionOdcExistingClient.finishCallOrigination, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ProductDetail))))