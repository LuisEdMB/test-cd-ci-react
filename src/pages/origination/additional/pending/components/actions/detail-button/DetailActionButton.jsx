// React
import React, { useState } from 'react'

// Material UI
import { Fab, Tooltip, withStyles } from '@material-ui/core'

// Material - Colors
import { blue } from '@material-ui/core/colors'

// Material - Icons
import AccountCircleIcon from '@material-ui/icons/AccountCircle'

// Components
import ModalDetail from './components/ModalDetail'

const styles = {
    clientProfileButton: {
        minHeight: 0,
        height: 24,
        width: 24,
        margin: '0 .3em 0 0',
        backgroundColor: blue[700],
        color: 'white',
        transition: '.3s',
        '&:hover': {
            backgroundColor: blue[900]
        }
    },
    icon: {
        minHeight: 0,
        height: 12,
        width: 12
    },
    button: {
        textTransform: 'none'
    },
    appBar: {
        position: 'relative'
    }
}

function DetailActionButton(props) {
    const { classes, row, disabled = true } = props

    const [ modalDetail, setModalDetail ] = useState(false)

    return (
        <div
            key={ row.cod_solicitud }>
                <Tooltip
                    title={ !disabled ? `Hacer Click! Perfil Cliente: ${ row.des_nro_documento }` : 'Datos del cliente no registrado.' }
                    placement='left'>
                        <div>
                            <Fab
                                disabled={ disabled }
                                onClick={_ => setModalDetail(true) }
                                className={ classes.clientProfileButton }>
                                    <AccountCircleIcon
                                        className={ classes.icon }
                                        fontSize='small'/>
                            </Fab>
                        </div>
                </Tooltip>
                {
                    modalDetail && <ModalDetail
                        open={ modalDetail }
                        close={ _ => setModalDetail(false) }
                        data={ row } />
                }
        </div>
    )
}

export default React.memo(withStyles(styles)(DetailActionButton))