// React
import React from 'react'

// Material UI
import { Grid, withStyles } from '@material-ui/core'

// Material UI - Icons
import CancelIcon from '@material-ui/icons/Cancel'

// Components
import Modal from '../../../../../../../../components/Modal'
import ActionButton from '../../../../../../../../components/ActionButton'
import AdditionalClientPreview from '../../../../../../../../components/info-preview/AdditionalClientPreview'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalDetail(props) {
    const { classes, open = false, close, data } = props

    return (
        <Modal
            size='md'
            title='Información de Cliente'
            body={
                <AdditionalClientPreview 
                    data={{
                        documentType: data.des_tipo_documento || '',
                        documentNumber: data.des_nro_documento || '',
                        fullName: data.des_nombre_completo || '',
                        firstName: data.des_primer_nom || '',
                        secondName: data.des_segundo_nom || '',
                        lastName: data.des_ape_paterno || '',
                        motherLastName: data.des_ape_materno || '',
                        birthday: data.fec_nacimiento || '',
                        gender: data.des_genero || '',
                        nationality: data.des_nacionalidad || '',
                        familyRelationship: data.des_vinculo_adicional || '',
                        cellphone: data.des_telef_celular || ''
                    }} />
            }
            open={ open }
            actions={
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 12 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        text='Cancelar'
                                        type='secondary'
                                        icon={ 
                                            <CancelIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                </Grid>
            }
        />
    )
}

export default React.memo(withStyles(styles)(ModalDetail))