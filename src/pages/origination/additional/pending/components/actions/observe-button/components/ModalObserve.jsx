// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, TextField, withStyles } from '@material-ui/core'

// Material UI - Icons
import CancelIcon from '@material-ui/icons/Cancel'
import SendIcon from '@material-ui/icons/Send'

// Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Redux - Actions
import * as ActionOdcAdditional from '../../../../../../../../actions/odc-additional/odc-additional'

// Hooks
import useReduxState from '../../../../../../../../hooks/general/useReduxState'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../../../../utils/Notistack'

// Components
import Modal from '../../../../../../../../components/Modal'
import ActionButton from '../../../../../../../../components/ActionButton'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalObserve(props) {
    const { classes, open = false, close, data, odcAdditional, registerObservationValidated, enqueueSnackbar } = props

    const prevOdcAdditional = useRef(odcAdditional)

    const [ loading, setLoading ] = useState(false)

    const [ dataValidateObservation, isLoadingValidateObservation, isSuccessValidateObservation, isErrorValidateObservation ] =
        useReduxState({ props: odcAdditional.observationValidatedRegistered, prevProps: prevOdcAdditional.current.observationValidatedRegistered,
            notify: enqueueSnackbar })

    useEffect(_ => {
        if (open) setLoading(false)
    }, [open])

    useEffect(_ => {
        if (isLoadingValidateObservation) setLoading(true)
        if (isSuccessValidateObservation) {
            Notistack.getNotistack(`Adicional: Solicitud ${ data.cod_solicitud_completa } - Aceptar.`, enqueueSnackbar, 'success')
            close()
        }
        if (isErrorValidateObservation) setLoading(false)
    }, [dataValidateObservation, isLoadingValidateObservation, isSuccessValidateObservation, isErrorValidateObservation])

    const handleValidateObservation = _ => {
        const sendData = {
            actividadGenericoSimpleModelRequest: {
                cod_solicitud: data.cod_solicitud,
                cod_solicitud_completa: data.cod_solicitud_completa,
                cod_flujo_fase_estado_actual: 41004,
                cod_flujo_fase_estado_anterior: 41003,
                nom_actividad: 'Rechazar Adicional: Aceptar'
            }
        }
        registerObservationValidated(sendData)
    }

    return (
        <Modal
            title='Observaciones del Validación'
            body={
                <TextField
                    fullWidth
                    multiline
                    rows='4'
                    label='Observaciones'
                    type='text'
                    margin='normal'
                    color='default'
                    variant='outlined'
                    value={ data.des_observacion }
                    InputLabelProps={{
                        shrink: true
                    }}
                    InputProps={{
                        readOnly: true
                    }} />
            }
            open={ open }
            actions={
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        text='Cancelar'
                                        loading={ loading }
                                        type='secondary'
                                        icon={ 
                                            <CancelIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        text='Aceptar'
                                        type='primary'
                                        loading={ loading }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        handleAction={ _ => handleValidateObservation() }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
            }
        />
    )
}

function mapStateToProps(state) {
    return {
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerObservationValidated: bindActionCreators(ActionOdcAdditional.registerObservationValidated, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalObserve))))