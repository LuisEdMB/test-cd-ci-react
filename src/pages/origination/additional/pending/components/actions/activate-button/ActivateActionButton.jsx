// React
import React from 'react'
import { withRouter } from 'react-router-dom'

// Material UI
import { Fab, Tooltip, withStyles } from '@material-ui/core'

// Material UI - Colors
import green from '@material-ui/core/colors/green'

// Material UI - Icons
import DoneIcon from '@material-ui/icons/Done'

// Config
import { URL_BASE } from '../../../../../../../actions/config'

const styles = {
    fab: {
        minHeight: 0,
        height: 24,
        width: 24,
        color: 'white',
        transition: '.3s',
        backgroundColor: green[700],
        '&:hover': {
            backgroundColor: green[900]
        },
        textTransform: 'none'
    },
    icon:{
        minHeight: 0,
        height: 12,
        width: 12
    }
}


const ActivateActionButton = (props) => {
    const { row, disabled = false, classes, history } = props
    
    return (
        <Tooltip 
            title={ !disabled ? `Hacer Click! Activar Tarjeta: ${ row.cod_solicitud_completa }` : `Solicitud: ${ row.cod_solicitud_completa }` } 
            placement='left'>
                <div>
                    <Fab
                        className={ classes.fab }
                        onClick={ _ => 
                            history.push({
                                pathname: `/${URL_BASE}/adicional/${ row.flg_url_maestro ? 'nueva-solicitud-ext' : 'nueva-solicitud' }/${row.cod_solicitud_completa}`,
                                state: {dataUpdate: row} 
                            })}
                        disabled={ disabled }>
                            <DoneIcon
                                className={ classes.icon }
                                fontSize='small'/>
                    </Fab>
                </div>
        </Tooltip>
    )
}

export default React.memo(withRouter(withStyles(styles)(ActivateActionButton)))