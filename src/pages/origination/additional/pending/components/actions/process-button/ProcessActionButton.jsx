// React
import React from 'react'
import { withRouter } from 'react-router-dom'

// Material UI
import { Fab, Tooltip, withStyles } from '@material-ui/core'

// Material UI - Colors
import { orange } from '@material-ui/core/colors'

// Material UI - Icons
import PlayArrowIcon from '@material-ui/icons/PlayArrow'

// Config
import { URL_BASE } from '../../../../../../../actions/config'

const styles = {
    fab: {
        minHeight: 0,
        height: 24,
        width: 24,
        color: 'white',
        transition: '.3s',
        backgroundColor: orange[700],
        '&:hover': {
            backgroundColor: orange[900]
        },
        textTransform: 'none'
    },
    icon: {
        minHeight: 0,
        height: 12,
        width: 12
    }
}

const ProcessActionButton = (props) => {
    const { row, disabled = false, classes, history } = props

    return (
        <Tooltip
            title={ !disabled ? `Hacer Click! Retomar Proceso: ${ row.cod_solicitud_completa }` : `Solicitud: ${ row.cod_solicitud_completa }` } 
            placement='left'>
                <div>
                    <Fab
                        className={ classes.fab }
                        onClick={ _ => 
                            history.push({
                                pathname: `/${URL_BASE}/adicional/${ row.flg_url_maestro ? 'nueva-solicitud-ext' : 'nueva-solicitud' }/${row.cod_solicitud_completa}`,
                                state: {dataUpdate: row} 
                            })}
                        disabled={ disabled }>
                            <PlayArrowIcon
                                className={ classes.icon }
                                fontSize='small' />
                    </Fab>
                </div>
        </Tooltip>
    )
}        

export default React.memo(withRouter(withStyles(styles)(ProcessActionButton)))