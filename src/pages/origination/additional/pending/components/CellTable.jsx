// React
import React from 'react'

// Material UI
import { Table } from '@devexpress/dx-react-grid-material-ui'

// Utils
import * as moment from 'moment'

// Components
import ProcessActionButton from './actions/process-button/ProcessActionButton'
import ActivateActionButton from './actions/activate-button/ActivateActionButton'
import DetailActionButton from './actions/detail-button/DetailActionButton'
import ObserveActionButton from './actions/observe-button/ObserveActionButton'
import NewCommentForm from '../../../../../components/NewCommentForm/NewCommentForm'

const CellTable = (props) => {
    const { row } = props
    let disabled = true
    const rowFPS = row.cod_flujo_fase_estado || 0
    
    if (props.column.name === 'fec_reg') {
        const value = props.value? moment(props.value.replace('T', ' ')).format('DD/MM/YYYY HH:mm') : ''
        return <Table.Cell {...props} value={value} />
    }
    else if (props.column.name === 'detail') {
        if(rowFPS >= 40501) {
            disabled = false
        }
        return <Table.Cell {...props}>
            <DetailActionButton {...props} disabled={disabled} />
        </Table.Cell>
    }
    else if (props.column.name === 'process') {
        if((rowFPS >= 40402 && rowFPS <= 40702) || (rowFPS >= 40705 && rowFPS <= 40801)) {
            disabled = false
        }
        return <Table.Cell {...props}>
            <ProcessActionButton {...props}  disabled={disabled} />
        </Table.Cell>
    }
    else if (props.column.name === 'activate') {
        if(rowFPS === 40704) {
            disabled = false
        }
        return <Table.Cell {...props}>
            <ActivateActionButton {...props}  disabled={disabled} />
        </Table.Cell>
    }
    else if (props.column.name === 'observe') {
        if(rowFPS === 41003) {
            disabled = false
        }
        return <Table.Cell {...props}>
        <ObserveActionButton {...props} disabled={disabled} />
    </Table.Cell>
    }
    if (props.column.name === 'observation') {
        return <Table.Cell {...props}>
           <NewCommentForm {...props} />
        </Table.Cell>
    }
    return <Table.Cell {...props} />
}

export default React.memo(CellTable)