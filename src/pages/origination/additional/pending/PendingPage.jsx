// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, LinearProgress, withWidth } from '@material-ui/core'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcAdditional from '../../../../actions/odc-additional/odc-additional'

// Hooks
import useReduxState from '../../../../hooks/general/useReduxState'

// Notify
import { withSnackbar } from 'notistack'

// Components
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent'
import Header from './components/Header'
import RowDetailTable from './components/RowDetailTable'
import CellTable from './components/CellTable'

const columnsConfig = {
    xs: [
        { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
        { name: 'detail', title: 'Detalle' },
        { name: 'process', title: 'Retomar' },
        { name: 'observe', title: 'Conting. Obs.' },
        { name: 'activate', title: 'Activar' }
    ],
    sm: [
        { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
        { name: 'des_tipo_documento', title: 'Tipo Doc.'},
        { name: 'des_nro_documento', title: 'Nro. Doc' },
        { name: 'detail', title: 'Detalle' },
        { name: 'process', title: 'Retomar' },
        { name: 'observe', title: 'Conting. Obs.' },
        { name: 'activate', title: 'Activar' }
    ],
    md: [
        { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
        { name: 'des_tipo_documento', title: 'Tipo Doc.'},
        { name: 'des_nro_documento', title: 'Nro. Doc' },
        { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
        { name: 'des_usu_reg', title: 'Usuario Creador' },
        { name: 'des_agencia', title: 'Agencia Registro' },
        { name: 'fec_reg', title: 'Fecha Registro' },
        { name: 'detail', title: 'Detalle' },
        { name: 'process', title: 'Retomar' },
        { name: 'observe', title: 'Conting. Obs.' },
        { name: 'activate', title: 'Activar' }
    ],
    lg: [
        { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
        { name: 'des_tipo_documento', title: 'Tipo Doc.'},
        { name: 'des_nro_documento', title: 'Nro. Doc' },
        { name: 'des_nombre_completo', title: 'Cliente' },
        { name: 'des_tipo_relacion', title: 'Relación' }, 
        { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
        { name: 'des_usu_reg', title: 'Usuario Creador' },
        { name: 'des_agencia', title: 'Agencia Registro' },
        { name: 'fec_reg', title: 'Fecha Registro' },
        { name: 'detail', title: 'Detalle' },
        { name: 'process', title: 'Retomar' },
        { name: 'observe', title: 'Conting. Obs.' },
        { name: 'activate', title: 'Activar' },
        { name: 'observation', title: 'Ver Comentario' }
    ],
    xl: [
        { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
        { name: 'des_tipo_documento', title: 'Tipo Doc.'},
        { name: 'des_nro_documento', title: 'Nro. Doc' },
        { name: 'des_nombre_completo', title: 'Cliente' },
        { name: 'des_tipo_relacion', title: 'Relación' }, 
        { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
        { name: 'des_usu_reg', title: 'Usuario Creador' },
        { name: 'des_agencia', title: 'Agencia Registro' },
        { name: 'fec_reg', title: 'Fecha Registro' },
        { name: 'detail', title: 'Detalle' },
        { name: 'process', title: 'Retomar' },
        { name: 'observe', title: 'Conting. Obs.' },
        { name: 'activate', title: 'Activar' },
        { name: 'observation', title: 'Ver Comentario' }
    ],
    default: [
        { columnName: 'cod_solicitud', width: 120 },
        { columnName: 'cod_solicitud_completa', width: 125 },
        { columnName: 'des_tipo_documento', width: 100 },
        { columnName: 'des_nro_documento', width: 100 }, 
        { columnName: 'des_nombre_completo', width: 250 },
        { columnName: 'des_tipo_relacion', width: 100 }, 
        { columnName: 'des_jerarquias_flujo_fase_estado', width: 320 },
        { columnName: 'fec_reg', width: 150 },
        { columnName: 'des_usu_reg', width: 150 },
        { columnName: 'des_agencia', width: 150 },    
        { columnName: 'detail', width: 90 },
        { columnName: 'process', width: 90 },
        { columnName: 'observe', width: 120 },
        { columnName: 'activate', width: 90 },
        { columnName: 'observation', width: 150 }
    ],
    extensions: [
        { columnName: 'detail', align: 'center' },
        { columnName: 'process', align: 'center' },
        { columnName: 'observe', align: 'center' },
        { columnName: 'activate', align: 'center' },
        { columnName: 'observation', align: 'center' }
    ]
}

function PendingPage(props) {
    const { odcAdditional, width, getPendingProcesses, enqueueSnackbar } = props

    const prevWidth = useRef(width)
    const prevOdcAdditional = useRef(odcAdditional)

    const [ columns, setColumns ] = useState([])
    const [ rows, setRows ] = useState([])

    const [ dataPendingProcess, isLoadingPendingProcess, isSuccessPendingProcess, isErrorPendingProcess ] =
        useReduxState({ props: odcAdditional.pendingProcesses, prevProps: prevOdcAdditional.current.pendingProcesses,
            notify: enqueueSnackbar })
    const [ dataValidateObservation, isLoadingValidateObservation, isSuccessValidateObservation, isErrorValidateObservation ] =
        useReduxState({ props: odcAdditional.observationValidatedRegistered, prevProps: prevOdcAdditional.current.observationValidatedRegistered })

    useEffect(_ => {
        getPendingProcesses()
        setColumns(columnsConfig[width])
    }, [])

    useEffect(_ => {
        if (prevWidth.current !== width) setColumns(columnsConfig[width])
    }, [width])

    useEffect(_ => {
        if (isLoadingPendingProcess || isErrorPendingProcess) setRows([])
        if (isSuccessPendingProcess) setRows(dataPendingProcess?.solicitudesAdicionales || [])
    }, [dataPendingProcess, isLoadingPendingProcess, isSuccessPendingProcess, isErrorPendingProcess])

    useEffect(_ => {
        if (isSuccessValidateObservation) getPendingProcesses()
    }, [dataValidateObservation, isLoadingValidateObservation, isSuccessValidateObservation, isErrorValidateObservation])

    return (
        <Grid
            container
            className='p-1'>
                <Grid
                    item
                    xs={ 12 }>
                    {
                        isLoadingPendingProcess && <LinearProgress />
                    }
                </Grid>
                <Grid
                    item
                    xs={ 12 }
                    className='mb-2'>
                    <Header
                        title='Bandeja en Proceso de Adicional(es)' />
                </Grid>
                <Grid
                    item
                    xs={ 12 }>
                        <DevGridComponent
                            rows={ rows }
                            columns={ columns }
                            width={ width }
                            search
                            columnExtensions={ columnsConfig.extensions }
                            defaultColumnWidths={ columnsConfig.default }
                            RowDetailComponent={ RowDetailTable }
                            CellComponent={ CellTable }/>
                </Grid>
        </Grid>
    )
}

function mapStateToProps(state) {
    return {
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getPendingProcesses: bindActionCreators(ActionOdcAdditional.getPendingProcesses, dispatch)
    }
}

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(PendingPage)))