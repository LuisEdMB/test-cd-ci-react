// React
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { Redirect, withRouter } from 'react-router'

// Material UI
import { Button, Paper, Step, StepContent, StepLabel, Stepper, Typography, withStyles } from '@material-ui/core'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcAdditional from '../../../actions/odc-additional/odc-additional'

// Hooks
import useReduxState from '../../../hooks/general/useReduxState'

// Effects
import Fade from 'react-reveal/Fade'

// Notify
import { SnackbarProvider, withSnackbar } from 'notistack'

// Utils
import classNames from 'classnames'
import * as Constants from '../../../utils/Constants'
import * as Config from '../../../actions/config'
import AdditionalFactory from '../../../utils/additional/AdditionalFactory'

// Components
import Timer from '../../../components/Timer'
import ClientConsult from './components/client-consult/ClientConsult'
import HolderClientInformation from './components/holder-client-information/HolderClientInformation'
import AdditionalClientInformation from './components/additional-client-information/AdditionalClientInformation'
import CreditCardDetail from './components/credit-card-detail/CreditCardDetail'
import CreditCardConfirm from './components/credit-card-confirm/CreditCardConfirm'
import CreditCardSummary from './components/credit-card-summary/CreditCardSummary'
import CreditCardEmboss from './components/credit-card-emboss/CreditCardEmboss'
import CreditCardActivation from './components/credit-card-activation/CreditCardActivation'
import ModalCancelOrigination from './components/extra-components/ModalCancelOrigination'
import BackgroundFinish from '../../../components/background-finish/BackgroundFinish'
import PreLoaderImage from '../../../components/PreLoaderImage'

const styles = theme => ({
    root: {
        width: '100%',
        position: 'relative'
    },
    stepLabelOk:{
        background: '#2196f31c',
        padding: '.5em',
        borderRadius: '.5em',
        boxShadow: '0px 1.2px 1px #007ab8',
        opacity: '1 !important'
    },
    resetContainer: {
        padding: theme.spacing.unit * 3,
        textAlign: 'center'
    }
})

const steps = [
    { id: 1, title: 'Tarjeta Adicional - Consulta cliente' },
    { id: 2, title: 'Datos Titular' },
    { id: 3, title: 'Registro Cliente Adicional' },
    { id: 4, title: 'Detalle Adicional' },
    { id: 5, title: 'Confirmar Adicional' },
    { id: 6, title: 'Resumen Adicional' },
    { id: 7, title: 'Emboce de Tarjeta' },
    { id: 8, title: 'Activación de Tarjeta' }
]

function AdditionalPageExternal(props) {
    const { classes, location, continueProcess, odcAdditional, registerContinueProcessActivity } = props

    const additional = new AdditionalFactory(Constants.AdditionalFactoryTypes.additional).createAdditional()
    const prevOdcAdditional = useRef(odcAdditional)

    const [ activeStep, setActiveStep ] = useState(0)
    const [ modalCancelActivity, setModalCancelActivity ] = useState(false)
    const [ dataCancelActivity, setDataCancelActivity ] = useState({})
    const [ stateAdditional, setStateAdditional ] = useState({})
    const [ isBiometricOk, setIsBiometricOk ] = useState(false)
    const [ loadingContinueProcess, setLoadingContinueProcess ] = useState(false)
    const [ errorContinueProcess, setErrorContinueProcess ] = useState(false)
    const [ phaseCodeFromContinueProcess, setPhaseCodeFromContinueProcess ] = useState({
        status: 0,
        message: ''
    })

    const [ dataContinueProcess, isLoadingContinueProcess, isSuccessContinueProcess, isErrorContinueProcess ] =
        useReduxState({ props: odcAdditional.process, prevProps: prevOdcAdditional.current.process })

    useEffect(_ => {
        handleReset()
        const { cod_solicitud_completa } = location.state?.dataUpdate || {}
        if (cod_solicitud_completa) {
            setLoadingContinueProcess(true)
            continueProcess(additional.getDataForContinueProcessApi(cod_solicitud_completa))
        }
        window.addEventListener('beforeunload', handleReloadPage)
        return _ => window.removeEventListener('beforeunload', handleReloadPage)
    }, [])

    useEffect(_ => {
        if (isLoadingContinueProcess) setLoadingContinueProcess(true)
        if (isErrorContinueProcess) setErrorContinueProcess(true)
        if (isSuccessContinueProcess) {
            const data = dataContinueProcess?.solicitud_Retomar || { }
            const homeAddress = (data.list_ori_sel_solicitud_retomar_direccion || []).find(address => address.cod_tipo_direccion === 260001) || {}
            const creditCard = {
                color: data.cod_valor_color || 0,
                colorAux: data.cod_color_pro || '',
                brand: data.cod_valor_marca || 0,
                brandDescription: data.des_valor_marca || '',
                productId: data.cod_producto || 0,
                product: data.des_tipo_producto || '',
                productDescription: data.des_nombre_producto || '',
                productType: data.des_tipo_producto || '',
                cardNumber: data.num_tarjeta_pmp || '',
                accountNumber: data.num_cuenta_pmp || '',
                clientNumber: data.num_cliente_pmp || '',
                relationshipNumber: data.num_relacion_pmp || '',
                bin: data.cod_bin_pro || 0,
                token: data.token || ''
            }
            const principalClient = {
                clientId: data.cod_cliente_titular || 0,
                documentTypeAux: data.cod_tipo_documento_titular || '',
                documentNumber: data.des_nro_documento_titular || '',
                creditCard: creditCard,
                homeAddress: {
                    departmentId: homeAddress.cod_ubi_departamento || '',
                    department: homeAddress.des_ubi_departamento || '',
                    districtId: homeAddress.cod_ubi_distrito || '',
                    nameVia: homeAddress.des_nombre_via || '',
                    number: homeAddress.des_nro || '',
                    building: homeAddress.des_departamento || '',
                    inside: homeAddress.des_interior || '',
                    mz: homeAddress.des_manzana || '',
                    lot: homeAddress.des_lote || '',
                    nameZone: homeAddress.des_zona || '',
                    reference: homeAddress.des_referencia_domicilio || ''
                },
                codeSolicitudeType: data.cod_tipo_solicitud,
                codeSolicitudeOriginationType: data.cod_tipo_solicitud_originacion || Constants.OriginationType.Preevaluated
            }
            const additionalClient = {
                solicitudeCode: data?.cod_solicitud || 0,
                completeSolicitudeCode: data?.cod_solicitud_completa || '',
                clientId: data.cod_cliente || 0,
                documentTypeLetter: data.des_tipo_documento || '',
                documentTypeAux: data.cod_tipo_documento || '',
                documentTypeInternalValue: data.des_tipo_documento_valor_interno || '',
                documentNumber: data.des_nro_documento || '',
                firstName: data.des_primer_nom || '',
                secondName: data.des_segundo_nom || '',
                firstLastName: data.des_ape_paterno || '',
                secondLastName: data.des_ape_materno || '',
                birthday: data.fec_nacimiento?.split('T')[0] || '',
                genderId: data.cod_genero || 0,
                gender: data.des_genero || '',
                nationalityId: data.cod_nacionalidad || 0,
                nationality: data.des_nacionalidad || '',
                familyRelationshipId: data.cod_vinculo_adicional || 0,
                familyRelationship: data.des_vinculo_adicional || '',
                cellphone: data.des_telef_celular || '',
                creditCard: creditCard
            }
            
            setStateAdditional({ principalClient, additionalClient })
            setActiveStep(additional.redirectStep(data.cod_flujo_fase_estado || 0))
            setIsBiometricOk(data.flg_biometria || false)
            setPhaseCodeFromContinueProcess({
                status: data.cod_flujo_fase_estado || 0,
                message: data.des_error_message_servicio || ''
            })
            registerContinueProcessActivity(additional.getDataForRegisterContinueProcessActivityApi(data))
            setLoadingContinueProcess(false)
        }
    }, [dataContinueProcess, isLoadingContinueProcess, isSuccessContinueProcess, isErrorContinueProcess])

    const handleReloadPage = e => {
        (e || window.event).returnValue = 'o/'
        return 'o/'
    }

    const handleNextStep = useCallback(_ => {
        setActiveStep(activeStep + 1)
    }, [activeStep])

    const handleReset = useCallback(_ => {
        setActiveStep(0)
        setModalCancelActivity(false)
        setDataCancelActivity({})
        setStateAdditional({})
        setIsBiometricOk(false)
        setLoadingContinueProcess(false)
        setErrorContinueProcess(false)
        setPhaseCodeFromContinueProcess({
            status: 0,
            message: ''
        })
    }, [])

    const showModalCancelActivity = useCallback(data => {
        setDataCancelActivity(data)
        setModalCancelActivity(true)
    }, [])

    const handleSaveStateAdditional = useCallback(data => {
        setStateAdditional(state => ({
            ...state,
            ...data
        }))
    }, [setStateAdditional])

    if (errorContinueProcess) return <Redirect to={{ pathname: `/${ Config.URL_BASE }/error`, }} />
    if (loadingContinueProcess) return <PreLoaderImage text='Retomando proceso, espere por favor ...' />
    return (
        <SnackbarProvider
            maxSnack={ 3 }>
                <div
                    className={ classes.root }>
                        {
                            activeStep >= 6 && <Timer />
                        }
                        <Stepper
                            activeStep={ activeStep }
                            orientation='vertical'>
                                {
                                    steps.map(step => 
                                        <Step
                                            key={ step.id }>
                                                <StepLabel>
                                                    <Fade>
                                                        <Typography
                                                            color='primary'
                                                            className={ classNames(classes.stepLabelOk, 'text-uppercase') }>
                                                                { step.title }
                                                        </Typography>
                                                    </Fade>
                                                </StepLabel>
                                                <StepContent>
                                                    <div
                                                        className='py-4'>
                                                            {
                                                                step.id === 1
                                                                    ? <ClientConsult
                                                                        additionalType={ Constants.AdditionalFactoryTypes.additionalExternal }
                                                                        handleSaveStateAdditional={ handleSaveStateAdditional }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 2
                                                                    ? <HolderClientInformation
                                                                        additionalType={ Constants.AdditionalFactoryTypes.additionalExternal }
                                                                        stateAdditional={ stateAdditional }
                                                                        showModalCancelActivity={ showModalCancelActivity }
                                                                        handleSaveStateAdditional={ handleSaveStateAdditional }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 3
                                                                    ? <AdditionalClientInformation
                                                                        additionalType={ Constants.AdditionalFactoryTypes.additionalExternal }
                                                                        showModalCancelActivity={ showModalCancelActivity }
                                                                        stateAdditional={ stateAdditional }
                                                                        handleSaveStateAdditional={ handleSaveStateAdditional }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 4
                                                                    ? <CreditCardDetail
                                                                        additionalType={ Constants.AdditionalFactoryTypes.additionalExternal }
                                                                        showModalCancelActivity={ showModalCancelActivity }
                                                                        stateAdditional={ stateAdditional }
                                                                        handleSaveStateAdditional={ handleSaveStateAdditional }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 5
                                                                    ? <CreditCardConfirm
                                                                        additionalType={ Constants.AdditionalFactoryTypes.additionalExternal }
                                                                        showModalCancelActivity={ showModalCancelActivity }
                                                                        stateAdditional={ stateAdditional }
                                                                        phaseCodeFromContinueProcess={ phaseCodeFromContinueProcess }
                                                                        handleSaveStateAdditional={ handleSaveStateAdditional }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 6
                                                                    ? <CreditCardSummary
                                                                        additionalType={ Constants.AdditionalFactoryTypes.additionalExternal }
                                                                        stateAdditional={ stateAdditional }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 7
                                                                    ? <CreditCardEmboss
                                                                        additionalType={ Constants.AdditionalFactoryTypes.additionalExternal }
                                                                        stateAdditional={ stateAdditional }
                                                                        handleReset={ handleReset }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 8
                                                                    ? <CreditCardActivation
                                                                        additionalType={ Constants.AdditionalFactoryTypes.additionalExternal }
                                                                        stateAdditional={ stateAdditional }
                                                                        handleReset={ handleReset }
                                                                        isBiometricOk={ isBiometricOk }
                                                                        phaseCodeFromContinueProcess={ phaseCodeFromContinueProcess }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                    null
                                                            }
                                                    </div>
                                                </StepContent>
                                        </Step>
                                    )
                                }
                        </Stepper>
                        {
                            activeStep === steps.length && (
                                <Paper
                                    square
                                    elevation={ 0 }
                                    className={ classes.resetContainer }
                                    style={{ textAlign: 'center' }}>
                                        <BackgroundFinish
                                            data={[{
                                                client: {
                                                    relationTypeId: 330002,
                                                    fullName: `${ stateAdditional.additionalClient?.firstLastName || '' } ` +
                                                        `${ stateAdditional.additionalClient?.secondLastName || '' } ` + 
                                                        `${ stateAdditional.additionalClient?.firstName || '' } ` + 
                                                        `${ stateAdditional.additionalClient?.secondName || '' }`
                                                },
                                                process: {
                                                    number: stateAdditional.additionalClient?.solicitudeCode || '',
                                                    fullNumber: stateAdditional.additionalClient?.completeSolicitudeCode || '',
                                                    typeSolicitudeCode: 340001,
                                                    typeSolicitudeOriginationCode: stateAdditional.principalClient?.codeSolicitudeOriginationType || 0
                                                }
                                            }]} />
                                        <br />
                                        <Button
                                            variant='contained'
                                            color='primary'
                                            onClick={ handleReset }>
                                            Nuevo Adicional
                                        </Button>
                                </Paper>
                            )
                        }
                        <ModalCancelOrigination
                            open={ modalCancelActivity }
                            close={ _ => setModalCancelActivity(false) }
                            data={ dataCancelActivity }
                            handleReset={ handleReset } />
                </div>
        </SnackbarProvider>
    )
}

function mapStateToProps(state) {
    return {
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        continueProcess: bindActionCreators(ActionOdcAdditional.continueProcess, dispatch),
        registerContinueProcessActivity: bindActionCreators(ActionOdcAdditional.registerContinueProcessActivity, dispatch)
    }
}

export default withRouter(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(AdditionalPageExternal))))