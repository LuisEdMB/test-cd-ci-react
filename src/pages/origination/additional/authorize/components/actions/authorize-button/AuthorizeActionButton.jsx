// React
import React, { useState } from 'react'

// Material UI
import { Fab, Tooltip, withStyles } from '@material-ui/core'

// Material - Colors
import { green } from '@material-ui/core/colors'

// Material UI - Icons
import CheckCircleOutlineIcon from '@material-ui/icons/VerifiedUser'

// Notify
import { withSnackbar } from 'notistack'

// Components
import ModalAuthorize from './components/ModalAuthorize'

const styles = {
    fab: {
        minHeight: 0,
        height: 24,
        width: 24,
        color: 'white',
        transition: '.3s',
        backgroundColor: green[700],
        '&:hover': {
            backgroundColor: green[900]
        },
        textTransform: 'none'
    }, 
    icon: {
        minHeight: 0,
        height: 12,
        width: 12
    }
}

function AuthorizeActionButton(props) {
    const { classes, row } = props

    const [ modalAuthorizeTc, setModalAuthorizeTc ] = useState(false)

    return (
        <div
            key={ row.cod_solicitud }>
                <Tooltip
                    title={`Autorizar Solicitud: ${ row.cod_solicitud_completa }`}
                    placement='left'>
                        <div>
                            <Fab
                                className={ classes.fab }
                                onClick={ _ => setModalAuthorizeTc(true) }>
                                    <CheckCircleOutlineIcon
                                        className={ classes.icon }
                                        fontSize='small'/>
                            </Fab>
                        </div>
                </Tooltip>
                {
                    modalAuthorizeTc && <ModalAuthorize
                        open={ modalAuthorizeTc }
                        close={ _ => setModalAuthorizeTc(false) }
                        data={ row } />
                }
        </div>
    )
}

export default React.memo(withStyles(styles)(withSnackbar(AuthorizeActionButton)))