// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import CancelIcon from '@material-ui/icons/Cancel'
import NotInterestedIcon from '@material-ui/icons/NotInterested'
import SendIcon from '@material-ui/icons/Send'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcAdditional from '../../../../../../../../actions/odc-additional/odc-additional'

import useReduxState from '../../../../../../../../hooks/general/useReduxState'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../../../../utils/Notistack'

// Components
import Modal from '../../../../../../../../components/Modal'
import ActionButton from '../../../../../../../../components/ActionButton'
import ModalRejectAuthorize from './ModalRejectAuthorize'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalAuthorize(props) {
    const { classes, open = false, close, data, authorizeActivationProcess, odcAdditional, enqueueSnackbar } = props

    const prevOdcAdditional = useRef(odcAdditional)

    const [ loading, setLoading ] = useState(false)
    const [ modalRejectAuthorize, setModalRejectAuthorize ] = useState(false)

    const [ dataAuthorizeActivation, isLoadingAuthorizeActivation, isSuccessAuthorizeActivation, isErrorAuthorizeActivation ] =
        useReduxState({ props: odcAdditional.activationProcessAuthorized, prevProps: prevOdcAdditional.current.activationProcessAuthorized,
            notify: enqueueSnackbar })

    useEffect(_ => {
        if (open) {
            setLoading(false)
            setModalRejectAuthorize(false)
        }
    }, [open])

    useEffect(_ => {
        if (isLoadingAuthorizeActivation) setLoading(true)
        if (isSuccessAuthorizeActivation) {
            Notistack.getNotistack(`Adicional: Solicitud ${ data.cod_solicitud_completa } - Autorizado.`, enqueueSnackbar, 'success')
            close()
        }
        if (isErrorAuthorizeActivation) setLoading(false)
    }, [dataAuthorizeActivation, isLoadingAuthorizeActivation, isSuccessAuthorizeActivation, isErrorAuthorizeActivation])

    const handleAuthorize = _ => {
        const sendData = {
            tipo_doc: data.cod_tipo_documento,
            nro_doc: data.des_nro_documento,
            actividadGenericoSimpleModelRequest: {
                cod_solicitud: data.cod_solicitud,
                cod_solicitud_completa: data.cod_solicitud_completa,
                cod_flujo_fase_estado_actual: 40704,
                cod_flujo_fase_estado_anterior: 40703,
                nom_actividad: 'Activación Tarjeta: Autorizado'
            }
        }
        authorizeActivationProcess(sendData)
    }

    return (
        <>
            <Modal
                title='Autorizar Activación TC'
                body={
                    <>
                        <Typography
                            align='center'>
                                ¿Deseas autorizar la activación de la Tarjeta de Crédito para la solicitud: <strong> { data.cod_solicitud_completa }</strong>?
                        </Typography>
                        <br />
                        <Typography 
                            align='justify'>
                                Al autorizar correctamente, la solicitud volverá a la bandeja <strong>'En Proceso'</strong>, para la activación correspondiente.
                        </Typography>
                    </>
                }
                open={ open }
                actions={
                    <Grid
                        container
                        spacing={ 8 }
                        className='d-flex justify-content-center'>
                            <Grid
                                item
                                xs={ 6 }>
                                    <div className={ classes.buttonProgressWrapper }>
                                        <ActionButton 
                                            loading={ loading }
                                            text='Rechazar'
                                            type='secondary'
                                            handleAction={ _ => setModalRejectAuthorize(true) }
                                            icon={ 
                                                <NotInterestedIcon 
                                                    fontSize='small' 
                                                    className='ml-2'/>
                                            }/>
                                    </div>
                            </Grid>
                            <Grid
                                item
                                xs={ 6 }>
                                    <div className={ classes.buttonProgressWrapper }>
                                        <ActionButton 
                                            loading={ loading }
                                            text='Autorizar'
                                            type='primary'
                                            handleAction={ _ => handleAuthorize() }
                                            icon={ 
                                                <SendIcon 
                                                    fontSize='small' 
                                                    className='ml-2'/>
                                            }
                                            showLoading/>
                                    </div>
                            </Grid>
                            <Grid
                                item
                                xs={ 6 }>
                                    <div className={ classes.buttonProgressWrapper }>
                                        <ActionButton
                                            loading={ loading }
                                            variant='outlined'
                                            text='Cancelar'
                                            type='secondary'
                                            icon={ 
                                                <CancelIcon 
                                                    fontSize='small' 
                                                    className='ml-2'/>
                                            }
                                            handleAction={ _ => close() }/>
                                    </div>
                            </Grid>
                    </Grid>
                }
            />
            <ModalRejectAuthorize
                open={ modalRejectAuthorize }
                close={ _ => setModalRejectAuthorize(false) }
                closeModalParent={ _ => close() }
                data={ data } />
        </>
    )
}

function mapStateToProps(state) {
    return {
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        authorizeActivationProcess: bindActionCreators(ActionOdcAdditional.authorizeActivationProcess, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalAuthorize))))