// React
import React, { useEffect, useRef, useState } from 'react'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcAdditional from '../../../../actions/odc-additional/odc-additional'

// Hooks
import useReduxState from '../../../../hooks/general/useReduxState'

// Material UI
import { Grid, LinearProgress, withWidth } from '@material-ui/core'

// Notify
import { withSnackbar } from 'notistack'

// Components
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent'
import Header from './components/Header'
import RowDetailTable from './components/RowDetailTable'
import CellTable from './components/CellTable'

const columnsConfig = {
    xs: [
        { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
        { name: 'authorize', title: 'Autorizar' }
    ],
    sm: [
        { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
        { name: 'des_tipo_documento', title: 'Tipo Doc.'},
        { name: 'des_nro_documento', title: 'Nro. Doc' },
        { name: 'authorize', title: 'Autorizar' }
    ],
    md: [
        { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
        { name: 'des_tipo_documento', title: 'Tipo Doc.'},
        { name: 'des_nro_documento', title: 'Nro. Doc' },
        { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
        { name: 'des_usu_reg', title: 'Usuario Creador' },
        { name: 'des_agencia', title: 'Agencia Registro' },
        { name: 'fec_reg', title: 'Fecha Registro' },
        { name: 'authorize', title: 'Autorizar' },
        { name: 'observation', title: 'Ver Comentario' }
    ],
    lg: [
        { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
        { name: 'des_tipo_documento', title: 'Tipo Doc.'},
        { name: 'des_nro_documento', title: 'Nro. Doc' },
        { name: 'des_nombre_completo', title: 'Cliente' },
        { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
        { name: 'fec_reg', title: 'Fecha Registro' },
        { name: 'des_usu_reg', title: 'Usuario Creador' },
        { name: 'des_agencia', title: 'Agencia Registro' },
        { name: 'authorize', title: 'Autorizar' },
        { name: 'observation', title: 'Ver Comentario' }
    ],
    xl: [
        { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
        { name: 'des_tipo_documento', title: 'Tipo Doc.'},
        { name: 'des_nro_documento', title: 'Nro. Doc' },
        { name: 'des_nombre_completo', title: 'Cliente' },
        { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
        { name: 'des_usu_reg', title: 'Usuario Creador' },
        { name: 'des_agencia', title: 'Agencia Registro' },
        { name: 'fec_reg', title: 'Fecha Registro' },
        { name: 'authorize', title: 'Autorizar' },
        { name: 'observation', title: 'Ver Comentario' }
    ],
    default: [
        { columnName: 'cod_solicitud', width: 120 },
        { columnName: 'cod_solicitud_completa', width: 125 },
        { columnName: 'des_tipo_documento', width: 100 },
        { columnName: 'des_nro_documento', width: 100 }, 
        { columnName: 'des_nombre_completo', width: 250 },
        { columnName: 'des_jerarquias_flujo_fase_estado', width: 320 },
        { columnName: 'fec_reg', width: 150 },
        { columnName: 'des_usu_reg', width: 150 },
        { columnName: 'des_agencia', width: 150 },    
        { columnName: 'authorize', width: 120 },
        { columnName: 'observation', width: 150 }
    ],
    extensions: [
        { columnName: 'authorize', align: 'center' },
        { columnName: 'observation', align: 'center' }
    ]
}

function AuthorizePage(props) {
    const { odcAdditional, width, getPendingProcessesActivation, enqueueSnackbar, odcMaster } = props

    const prevWidth = useRef(width)
    const prevOdcAdditional = useRef(odcAdditional)
    const prevOdcMaster = useRef(odcMaster)

    const [ columns, setColumns ] = useState([])
    const [ rows, setRows ] = useState([])

    const [ dataPendingActivation, isLoadingPendingActivation, isSuccessPendingActivation, isErrorPendingActivation ] =
        useReduxState({ props: odcAdditional.pendingActivationProcesses, prevProps: prevOdcAdditional.current.pendingActivationProcesses,
            notify: enqueueSnackbar })
    const [ dataAuthorizeActivation, isLoadingAuthorizeActivation, isSuccessAuthorizeActivation, isErrorAuthorizeActivation ] =
        useReduxState({ props: odcAdditional.activationProcessAuthorized, prevProps: prevOdcAdditional.current.activationProcessAuthorized })
    const [ dataBlockTypeCreditCardMaster, isLoadingBlockTypeCreditCardMaster, isSuccessBlockTypeCreditCardMaster,
        isErrorBlockTypeCreditCardMaster ] =
            useReduxState({ props: odcMaster.blockingCodeCreditCardUpdated, prevProps: prevOdcMaster.current.blockingCodeCreditCardUpdated })

    useEffect(_ => {
        getPendingProcessesActivation()
        setColumns(columnsConfig[width])
    }, [])

    useEffect(_ => {
        if (prevWidth.current !== width) setColumns(columnsConfig[width])
    }, [width])

    useEffect(_ => {
        if (isLoadingPendingActivation || isErrorPendingActivation) setRows([])
        if (isSuccessPendingActivation) setRows(dataPendingActivation?.solicitudesAdicionalesPorAutorizar || [])
    }, [dataPendingActivation, isLoadingPendingActivation, isSuccessPendingActivation, isErrorPendingActivation])

    useEffect(_ => {
        if (isSuccessAuthorizeActivation) getPendingProcessesActivation()
    }, [dataAuthorizeActivation, isLoadingAuthorizeActivation, isSuccessAuthorizeActivation, isErrorAuthorizeActivation])

    useEffect(_ => {
        if (isSuccessBlockTypeCreditCardMaster) getPendingProcessesActivation()
    }, [dataBlockTypeCreditCardMaster, isLoadingBlockTypeCreditCardMaster, isSuccessBlockTypeCreditCardMaster, isErrorBlockTypeCreditCardMaster])

    return (
        <Grid
            container
            className='p-1'>
                <Grid
                    item
                    xs={ 12 }>
                    {
                        isLoadingPendingActivation && <LinearProgress />
                    }
                </Grid>
                <Grid
                    item
                    xs={ 12 }
                    className='mb-2'>
                    <Header
                        title='Bandeja de Aprobación de Adicional(es)' />
                </Grid>
                <Grid
                    item
                    xs={ 12 }>
                        <DevGridComponent
                            rows={ rows }
                            columns={ columns }
                            width={ width }
                            search
                            columnExtensions={ columnsConfig.extensions }
                            defaultColumnWidths={ columnsConfig.default }
                            RowDetailComponent={ RowDetailTable }
                            CellComponent={ CellTable }/>
                </Grid>
        </Grid>
    )
}

function mapStateToProps(state) {
    return {
        odcAdditional: state.odcAdditionalReducer,
        odcMaster: state.odcMasterReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getPendingProcessesActivation: bindActionCreators(ActionOdcAdditional.getPendingProcessesActivation, dispatch)
    }
}

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(AuthorizePage)))