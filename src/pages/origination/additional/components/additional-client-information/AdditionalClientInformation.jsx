// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { CircularProgress, FormControl, FormControlLabel, FormHelperText, FormLabel, Grid, InputAdornment, InputLabel, MenuItem, Radio, RadioGroup, Select, TextField, Tooltip, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import RecentActorsIcon from '@material-ui/icons/RecentActors'
import Filter1Icon from '@material-ui/icons/Filter1'
import Filter2Icon from '@material-ui/icons/Filter2'
import PermIdentityIcon from '@material-ui/icons/PermIdentity'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import StayPrimaryPortraitIcon from '@material-ui/icons/StayPrimaryPortrait'
import CodeIcon from '@material-ui/icons/Code'
import CancelIcon from '@material-ui/icons/Cancel'
import SendIcon from '@material-ui/icons/Send'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionPromoter from '../../../../../actions/generic/promoter'
import * as ActionGender from '../../../../../actions/value-list/gender'
import * as ActionCountry from '../../../../../actions/value-list/country'
import * as ActionFamilyRelationship from '../../../../../actions/value-list/family-relationship'
import * as ActionConstantOdc from '../../../../../actions/generic/constant'
import * as ActionOdcAdditional from '../../../../../actions/odc-additional/odc-additional'
import * as ActionOdcMaster from '../../../../../actions/odc-master/odc-master'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'

// Notify
import { withSnackbar } from 'notistack'

// Utils
import * as Utils from '../../../../../utils/Utils'
import * as Validations from '../../../../../utils/Validations'
import * as Moment from 'moment'
import AdditionalFactory from '../../../../../utils/additional/AdditionalFactory'

// Effects
import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import Bounce from 'react-reveal/Bounce'

// Components
import SectionClientInformation from './components/SectionClientInformation'
import Autocomplete from '../../../../../components/Autocomplete'
import PreLoaderImage from '../../../../../components/PreLoaderImage'
import ActionButton from '../../../../../components/ActionButton'
import ModalInfoPreview from './components/ModalInfoPreview'

import { extendMoment } from 'moment-range'

const moment= extendMoment(Moment)

const styles = {
    menuItem: {
        zIndex: 900
    },
    labelError: {
        display: 'flex',
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    wrapper: {
        position: 'relative'
    },
    buttonProgressWrapper: {
        position: 'relative'
    },   
    buttonProgress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -8,
        marginLeft: -12,
        textTransform: 'none'
    }
}

const initialState = {
    clientId: 0,
    documentTypeLetter: '',
    documentTypeId: 100001,
    documentTypeAux: '',
    documentTypeInternalValue: '',
    documentNumber: '',
    firstName: '',
    secondName: '',
    firstLastName: '',
    secondLastName: '',
    birthday: '',
    genderId: 0,
    gender: '',
    nationalityId: 0,
    nationality: '',
    familyRelationshipId: 0,
    familyRelationship: '',
    cellphone: '',
    scannerCode: '',
    promoterCode: '',
    nameScannerCode: '',
    flagInternetConsume: '0',
    flagForeignConsume: '0',
    flagEffectiveDisposition: '0',
    flagOverdraftCreditCard: '0'
}

function AdditionalClientInformation(props) {
    const { classes, documentType, getPromoter, promoter, gender, country, familyRelationship, getGender,
        getCountry, getFamilyRelationship, handleNextStep, additionalType, odcAdditional, odcMaster,
        preloadAdditionalClient, enqueueSnackbar, handleSaveStateAdditional, stateAdditional, showModalCancelActivity,
        validateAdditionalClientAlreadyBelongsHolderClient, constantOdc, getConstantOdc } = props
    
    const additional = new AdditionalFactory(additionalType).createAdditional()
    const prevOdcAdditional = useRef(odcAdditional)
    const prevOdcMaster = useRef(odcMaster)
    const prevPromoter = useRef(promoter)

    const [ loading, setLoading ] = useState(true)
    const [ clientFound, setClientFound ] = useState(false)
    const [ modalInfoPreview, setModalInfoPreview ] = useState(false)
    const [ personalInformation, setPersonalInformation ] = useState(initialState)
    const [ validationPersonalInformation, setValidationPersonalInformation ] = useState({
        documentTypeId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        documentNumber: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        firstName: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        firstLastName: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        birthday: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        genderId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        nationalityId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        familyRelationshipId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        cellphone: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        scannerCode: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        nameScannerCode: {
            message: '',
            error: false,
            ref: useRef(null)
        }
    })

    const [ dataValidateClientAdditional, isLoadingValidateClientAdditional, isSuccessValidateClientAdditional, isErrorValidateClientAdditional ] =
        useReduxState({ props: odcAdditional.additionalClientValidated, prevProps: prevOdcAdditional.current.additionalClientValidated,
            notify: enqueueSnackbar })
    const [ dataPreloadAdditionalClient, isLoadingPreloadAdditionalClient, isSuccessPreloadAdditionalClient, isErrorPreloadAdditionalClient ] =
        useReduxState({ props: odcMaster.additionalClientPreloaded, prevProps: prevOdcMaster.current.additionalClientPreloaded, 
            notify: enqueueSnackbar, options: { setDataWhenErrorOcurred: true } })
    const [ dataSearchPromotor, isLoadingSearchPromotor, isSuccessSearchPromotor, isErrorSearchPromotor ] =
        useReduxState({ props: promoter, prevProps: prevPromoter.current, notify: enqueueSnackbar })

    useEffect(_ => {
        Promise.all([getGender(), getCountry(), getFamilyRelationship(), getConstantOdc()]).then(_ => {
            const minAge = constantOdc?.data?.find(item => item.des_abv_constante === 'CDA_EDAD_MINIMA_CLIENTE_TITULAR')
            const maxAge = constantOdc?.data?.find(item => item.des_abv_constante === 'CDA_EDAD_MAXIMA_CLIENTE_TITULAR')
            setValidationPersonalInformation(state => ({
                ...state,
                birthday: {
                    ...state.birthday,
                    extra: [{
                        regex: value => {
                            const startDate = new Date(moment().subtract(maxAge?.valor_numerico || 0, 'years').format('YYYY-MM-DD'))
                            const endDate = new Date(moment().subtract(minAge?.valor_numerico || 0, 'years').format('YYYY-MM-DD'))
                            const date = new Date(value)
                            const range = moment().range(startDate, endDate)
                            return range.contains(date)
                        },
                        message: 'Revisar fecha de nacimiento.',
                        isRegex: false
                    }]
                }
            }))
            setLoading(false)
        })
    }, [])

    useEffect(_ => {
        if (isLoadingValidateClientAdditional) setClientFound(false)
        if (isSuccessValidateClientAdditional) {
            const document = getDocumentById()
            const sendData = additional.getDataForAdditionalClientPreloadApi({
                ...stateAdditional,
                additionalClient: {
                    ...stateAdditional?.additionalClient,
                    documentTypeAux: document.des_auxiliar,
                    documentNumber: personalInformation.documentNumber,
                }
            })
            preloadAdditionalClient(sendData)
        }
        if (isErrorValidateClientAdditional) setClientFound(false)
    }, [dataValidateClientAdditional, isLoadingValidateClientAdditional, isSuccessValidateClientAdditional, isErrorValidateClientAdditional])

    useEffect(_ => {
        if (isLoadingPreloadAdditionalClient) setClientFound(false)
        if ((isSuccessPreloadAdditionalClient || isErrorPreloadAdditionalClient) && dataPreloadAdditionalClient?.precargaCliente) {
            const response = additional.processDataResponseOfPreloadAdditionalClient(dataPreloadAdditionalClient)
            setPersonalInformation(state => ({
                ...state,
                clientId: response.clientId,
                documentNumber: response.documentNumber || personalInformation.documentNumber,
                firstName: response.firstName,
                secondName: response.secondName,
                firstLastName: response.firstLastName,
                secondLastName: response.secondLastName,
                birthday: response.birthday,
                genderId: response.genderId,
                gender: response.gender,
                nationalityId: response.nationalityId,
                nationality: response.nationality,
                familyRelationshipId: response.familyRelationshipId,
                familyRelationship: response.familyRelationship,
                cellphone: response.cellphone
            }))
            handleSaveStateAdditional({
                ...stateAdditional,
                principalClient: {
                    ...stateAdditional?.principalClient,
                    clientId: stateAdditional?.principalClient?.clientId || response.holderClientId,
                    email: stateAdditional?.principalClient?.email || response.holderEmail,
                    homeAddress: stateAdditional?.principalClient?.homeAddress || response.homeAddress
                }
            })
            setClientFound(true)
        }
    }, [dataPreloadAdditionalClient, isLoadingPreloadAdditionalClient, isSuccessPreloadAdditionalClient, isErrorPreloadAdditionalClient])

    useEffect(_ => {
        if (isLoadingSearchPromotor || isErrorSearchPromotor) {
            handleChangeState({ name: 'nameScannerCode', value: '' })
            handleChangeState({ name: 'promoterCode', value: '' })
        }
        if (isSuccessSearchPromotor) {
            handleChangeState({ name: 'nameScannerCode', value: dataSearchPromotor?.des_nom_promotor || ' ' })
            handleChangeState({ name: 'promoterCode', value: dataSearchPromotor?.cod_promotor || '' })
        }
    }, [dataSearchPromotor, isLoadingSearchPromotor, isSuccessSearchPromotor, isErrorSearchPromotor])

    const getObjectArrayValueLabel = array => {
        return array?.data?.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        })) || []
    }

    const handleChangeState = e => {
        const { name, value, call = _ => null } = e.target || e
        setPersonalInformation(state => ({
            ...state,
            [name]: value
        }))
        setValidationPersonalInformation(state => {
            let validations = { ...state }
            if (Utils.findProperty(state, name, ['ref']).length > 0) {
                validations = {
                    ...state,
                    [name]: {
                        ...state[name],
                        ...Validations.validateByField(name, value, true, state[name].extra || [])
                    }
                }
            }
            call(validations)
            return validations
        })
    }

    const getDocumentById = _ => {
        return documentType.data.find(document => document.cod_valor === personalInformation.documentTypeId)
    }

    const handleSearchClient = _ => {
        if (!validationPersonalInformation.documentNumber.error) {
            const document = getDocumentById()
            const sendData = additional.getDataForValidateAdditionalClientAlreadyBelongsHolderClientApi({
                ...stateAdditional,
                additionalClient: {
                    ...stateAdditional?.additionalClient,
                    documentTypeAux: document.des_auxiliar,
                    documentNumber: personalInformation.documentNumber
                }
            })
            validateAdditionalClientAlreadyBelongsHolderClient(sendData)
        }
    }

    const handleContinueProcess = _ => {
        Validations.comprobeAllValidationsSuccess(personalInformation, handleChangeState).then(valid => {
            if (valid) {
                const document = getDocumentById()
                setPersonalInformation(state => ({
                    ...state,
                    documentTypeLetter: document.des_valor,
                    documentTypeAux: document.des_auxiliar,
                    documentTypeInternalValue: document.valor_interno
                }))
                setModalInfoPreview(true)
            }
        })
    }

    return (
        <>
            <div 
                className='mb-2'>
                    {
                        loading 
                            ?   <PreLoaderImage text='Cargando información inicial ...' />
                            :   <Fade>
                                    <Grid
                                        container
                                        spacing={ 8 }
                                        className='mb-2'>
                                            <Grid
                                                item
                                                xs={ 12 }>
                                                    <SectionClientInformation
                                                        data={ personalInformation }
                                                        extra={ {
                                                            clientFound: clientFound,
                                                            loading: isLoadingValidateClientAdditional || isLoadingPreloadAdditionalClient
                                                        } }
                                                        expanded
                                                        title={
                                                            <>
                                                                <AccountCircleIcon
                                                                    fontSize='small' 
                                                                    className='mr-2'/>
                                                                Información Personal
                                                            </>
                                                        }>
                                                            <>
                                                                <Grid
                                                                    container
                                                                    spacing={ 8 }>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <FormControl
                                                                                    disabled={ !clientFound }
                                                                                    fullWidth
                                                                                    required
                                                                                    margin='normal'>
                                                                                        <InputLabel
                                                                                            required
                                                                                            error={ validationPersonalInformation.documentTypeId.error }>
                                                                                                Tipo Documento
                                                                                        </InputLabel>
                                                                                        <Select
                                                                                            error={ validationPersonalInformation.documentTypeId.error }
                                                                                            value={ personalInformation.documentTypeId }
                                                                                            inputRef={ validationPersonalInformation.documentTypeId.ref }
                                                                                            name='documentTypeId'
                                                                                            onChange={ e => handleChangeState(e) }
                                                                                            onBlur={ e => handleChangeState(e) }>
                                                                                            {
                                                                                                documentType.data.map((type, index) => 
                                                                                                    <MenuItem
                                                                                                        key={ index }
                                                                                                        value={ type.cod_valor }>
                                                                                                            { type.des_valor_corto }
                                                                                                    </MenuItem>)
                                                                                            }
                                                                                        </Select>
                                                                                </FormControl>
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validationPersonalInformation.documentTypeId.error 
                                                                                                    ? 'secondary' : 'default'}>
                                                                                                        {
                                                                                                            validationPersonalInformation.documentTypeId.error &&
                                                                                                            validationPersonalInformation.documentTypeId.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <div>
                                                                                    <TextField
                                                                                        error={ validationPersonalInformation.documentNumber.error }
                                                                                        fullWidth
                                                                                        label='Número Documento'
                                                                                        type='search'
                                                                                        required
                                                                                        autoFocus
                                                                                        margin='normal'
                                                                                        color='default'
                                                                                        value={ personalInformation.documentNumber }
                                                                                        inputRef={ validationPersonalInformation.documentNumber.ref }
                                                                                        onChange={ e => {
                                                                                            if (Utils.onlyNumberRegex(e.target.value)) handleChangeState(e)
                                                                                        } }
                                                                                        onBlur={ e => {
                                                                                            if (Utils.onlyNumberRegex(e.target.value)) handleChangeState(e)
                                                                                        } }
                                                                                        onKeyPress={ e => Utils.handleEnterKeyPress(e, handleSearchClient) }
                                                                                        name='documentNumber'
                                                                                        InputProps={{
                                                                                            inputProps: {
                                                                                                maxLength: Utils.getLengthDocumentByType(personalInformation.documentTypeId)
                                                                                            },
                                                                                            endAdornment: (
                                                                                                <InputAdornment
                                                                                                    position='end'>
                                                                                                        <RecentActorsIcon
                                                                                                            color={ validationPersonalInformation.documentNumber.error
                                                                                                                ? 'secondary' : 'inherit' }
                                                                                                            fontSize='small'/>
                                                                                                </InputAdornment>
                                                                                            )
                                                                                        }}
                                                                                    />
                                                                                    {
                                                                                        (isLoadingValidateClientAdditional || isLoadingPreloadAdditionalClient) &&
                                                                                            <CircularProgress
                                                                                                size={ 24 }
                                                                                                className={ classes.buttonProgress } />
                                                                                    }
                                                                                </div>
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validationPersonalInformation.documentNumber.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validationPersonalInformation.documentNumber.error 
                                                                                                                ? validationPersonalInformation.documentNumber.message
                                                                                                                : 'Presionar <Enter> para validar cliente'
                                                                                                        }
                                                                                            </Typography>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validationPersonalInformation.documentNumber.error
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        `${personalInformation.documentNumber.length}/
                                                                                                        ${Utils.getLengthDocumentByType(personalInformation.documentTypeId)}`
                                                                                                    }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                </Grid>
                                                                <Grid
                                                                    container
                                                                    spacing={ 8 }>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <TextField
                                                                                    disabled={ !clientFound }
                                                                                    error={ validationPersonalInformation.firstName.error }
                                                                                    fullWidth
                                                                                    required
                                                                                    label='Primer Nombre'
                                                                                    type='text'
                                                                                    margin='normal'
                                                                                    value={ personalInformation.firstName }
                                                                                    inputRef={ validationPersonalInformation.firstName.ref }
                                                                                    onChange={ e => {
                                                                                        if (Utils.onlyTextRegex(e.target.value))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    onBlur={ e => {
                                                                                        if (Utils.onlyTextRegex(e.target.value))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    name='firstName'
                                                                                    InputProps={{
                                                                                        inputProps: {
                                                                                            maxLength: Utils.defaultLengthInput
                                                                                        },
                                                                                        endAdornment: (
                                                                                            <InputAdornment
                                                                                                position='end'>
                                                                                                    <Filter1Icon
                                                                                                        color={ validationPersonalInformation.firstName.error
                                                                                                            ? 'secondary' : 'inherit' }
                                                                                                        fontSize='small'/>
                                                                                            </InputAdornment>
                                                                                        )
                                                                                    }}
                                                                                />
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validationPersonalInformation.firstName.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validationPersonalInformation.firstName.error &&
                                                                                                            validationPersonalInformation.firstName.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <TextField
                                                                                    disabled={ !clientFound }
                                                                                    fullWidth
                                                                                    label='Segundo Nombre'
                                                                                    type='text'
                                                                                    color='default'
                                                                                    margin='normal'
                                                                                    value={ personalInformation.secondName }
                                                                                    name='secondName'
                                                                                    onChange={ e => {
                                                                                        if (Utils.onlyTextRegex(e.target.value))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    onBlur={ e => {
                                                                                        if (Utils.onlyTextRegex(e.target.value))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    InputProps={{
                                                                                        inputProps: {
                                                                                            maxLength: Utils.defaultLengthInput
                                                                                        },
                                                                                        endAdornment: (
                                                                                            <InputAdornment
                                                                                                position='end'>
                                                                                                    <Filter2Icon
                                                                                                        fontSize='small'/>
                                                                                            </InputAdornment>
                                                                                        )
                                                                                    }}
                                                                                />
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <TextField
                                                                                    disabled={ !clientFound }
                                                                                    error={ validationPersonalInformation.firstLastName.error }
                                                                                    fullWidth
                                                                                    required
                                                                                    label='Apellido Paterno'
                                                                                    type='text'
                                                                                    color='default'
                                                                                    margin='normal'
                                                                                    value={ personalInformation.firstLastName }
                                                                                    inputRef={ validationPersonalInformation.firstLastName.ref }
                                                                                    onChange={ e => {
                                                                                        if (Utils.onlyTextRegex(e.target.value))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    onBlur={ e => {
                                                                                        if (Utils.onlyTextRegex(e.target.value))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    name='firstLastName'
                                                                                    InputProps={{
                                                                                        inputProps: {
                                                                                            maxLength: Utils.defaultLengthInput
                                                                                        },
                                                                                        endAdornment: (
                                                                                            <InputAdornment
                                                                                                position='end'>
                                                                                                    <PermIdentityIcon
                                                                                                        color={ validationPersonalInformation.firstLastName.error
                                                                                                            ? 'secondary' : 'inherit' }
                                                                                                        fontSize='small'/>
                                                                                            </InputAdornment>
                                                                                        )
                                                                                    }}
                                                                                />
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validationPersonalInformation.firstLastName.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validationPersonalInformation.firstLastName.error &&
                                                                                                            validationPersonalInformation.firstLastName.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <TextField
                                                                                    disabled={ !clientFound }
                                                                                    fullWidth
                                                                                    label='Apellido Materno'
                                                                                    type='text'
                                                                                    color='default'
                                                                                    margin='normal'
                                                                                    value={ personalInformation.secondLastName }
                                                                                    name='secondLastName'
                                                                                    onChange={ e => {
                                                                                        if (Utils.onlyTextRegex(e.target.value))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    onBlur={ e => {
                                                                                        if (Utils.onlyTextRegex(e.target.value))
                                                                                            handleChangeState(e)
                                                                                    } }
                                                                                    InputProps={{
                                                                                        inputProps: {
                                                                                            maxLength: Utils.defaultLengthInput
                                                                                        },
                                                                                        endAdornment: (
                                                                                            <InputAdornment
                                                                                                position='end'>
                                                                                                    <PermIdentityIcon
                                                                                                        fontSize='small'/>
                                                                                            </InputAdornment>
                                                                                        )
                                                                                    }}
                                                                                />
                                                                        </Grid>
                                                                </Grid>
                                                                <Grid
                                                                    container
                                                                    spacing={ 8 }>
                                                                        <Grid
                                                                            item
                                                                            xs={ 6 }
                                                                            sm={ 6 }
                                                                            md={ 3 }
                                                                            lg={ 3 }>
                                                                                <TextField
                                                                                    disabled={ !clientFound }
                                                                                    error={ validationPersonalInformation.birthday.error }
                                                                                    fullWidth
                                                                                    required
                                                                                    label='Fecha Nacimiento'
                                                                                    type='date'
                                                                                    color='default'
                                                                                    margin='normal'
                                                                                    format='DD-MM-YYYY'
                                                                                    value={ personalInformation.birthday }
                                                                                    inputRef={ validationPersonalInformation.birthday.ref }
                                                                                    onChange={ e => handleChangeState(e) }
                                                                                    onBlur={ e => handleChangeState(e) }
                                                                                    name='birthday'
                                                                                    InputLabelProps={{
                                                                                        shrink: true
                                                                                    }}
                                                                                    InputProps={{
                                                                                        endAdornment: (
                                                                                            <InputAdornment
                                                                                                position='end'>
                                                                                                    <CalendarTodayIcon
                                                                                                        color={ validationPersonalInformation.birthday.error
                                                                                                            ? 'secondary' : 'inherit' }
                                                                                                        fontSize='small'/>
                                                                                            </InputAdornment>
                                                                                        )
                                                                                    }}
                                                                                />
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validationPersonalInformation.birthday.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validationPersonalInformation.birthday.error &&
                                                                                                            validationPersonalInformation.birthday.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 6 }
                                                                            sm={ 6 }
                                                                            md={ 3 }
                                                                            lg={ 3 }>
                                                                                <FormControl
                                                                                    fullWidth
                                                                                    required
                                                                                    error={ validationPersonalInformation.genderId.error }>
                                                                                        <Autocomplete
                                                                                            disabled={ !clientFound }
                                                                                            placeholder='Género *'
                                                                                            value={ personalInformation.genderId }
                                                                                            inputRef={ validationPersonalInformation.genderId.ref }
                                                                                            onChange={ e => {
                                                                                                handleChangeState({ name: 'genderId', value: e?.value || 0 })
                                                                                                handleChangeState({ name: 'gender', value: e?.label || '' })
                                                                                            }}
                                                                                            data={ getObjectArrayValueLabel(gender) }/>
                                                                                </FormControl>
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validationPersonalInformation.genderId.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validationPersonalInformation.genderId.error &&
                                                                                                            validationPersonalInformation.genderId.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 6 }
                                                                            sm={ 6 }
                                                                            md={ 3 }
                                                                            lg={ 3 }>
                                                                                <FormControl
                                                                                    fullWidth
                                                                                    required
                                                                                    error={ validationPersonalInformation.nationalityId.error }>
                                                                                        <Autocomplete
                                                                                            disabled={ !clientFound }
                                                                                            placeholder='Nacionalidad *'
                                                                                            value={ personalInformation.nationalityId }
                                                                                            inputRef={ validationPersonalInformation.nationalityId.ref }
                                                                                            onChange={ e => {
                                                                                                handleChangeState({ name: 'nationalityId', value: e?.value || 0 })
                                                                                                handleChangeState({ name: 'nationality', value: e?.label || '' })
                                                                                            }}
                                                                                            data={ getObjectArrayValueLabel(country) }/>
                                                                                </FormControl>
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validationPersonalInformation.nationalityId.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validationPersonalInformation.nationalityId.error &&
                                                                                                            validationPersonalInformation.nationalityId.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 6 }
                                                                            sm={ 6 }
                                                                            md={ 3 }
                                                                            lg={ 3 }>
                                                                                <FormControl
                                                                                    fullWidth
                                                                                    required
                                                                                    error={ validationPersonalInformation.familyRelationshipId.error }>
                                                                                        <Autocomplete
                                                                                            disabled={ !clientFound }
                                                                                            placeholder='Vínculo *'
                                                                                            value={ personalInformation.familyRelationshipId }
                                                                                            inputRef={ validationPersonalInformation.familyRelationshipId.ref }
                                                                                            onChange={ e => {
                                                                                                handleChangeState({ name: 'familyRelationshipId', value: e?.value || 0 })
                                                                                                handleChangeState({ name: 'familyRelationship', value: e?.label || '' })
                                                                                            }}
                                                                                            data={ getObjectArrayValueLabel(familyRelationship) }/>
                                                                                </FormControl>
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validationPersonalInformation.familyRelationshipId.error 
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validationPersonalInformation.familyRelationshipId.error &&
                                                                                                            validationPersonalInformation.familyRelationshipId.message
                                                                                                        }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                </Grid>
                                                                <Grid
                                                                    container
                                                                    spacing={ 8 }>
                                                                        <Grid
                                                                            item
                                                                            xs={ 6 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <TextField
                                                                                    disabled={ !clientFound }
                                                                                    error={ validationPersonalInformation.cellphone.error }
                                                                                    fullWidth
                                                                                    required
                                                                                    label='Celular'
                                                                                    type='text'
                                                                                    margin='normal'
                                                                                    color='default'
                                                                                    value={ personalInformation.cellphone }
                                                                                    inputRef={ validationPersonalInformation.cellphone.ref }
                                                                                    onChange={ e => {
                                                                                        if (Utils.onlyNumberRegex(e.target.value))
                                                                                            handleChangeState(e) 
                                                                                    } }
                                                                                    onBlur={ e => {
                                                                                        if (Utils.onlyNumberRegex(e.target.value))
                                                                                            handleChangeState(e) 
                                                                                    } }
                                                                                    name='cellphone'
                                                                                    InputProps={{
                                                                                        inputProps: {
                                                                                            maxLength: Utils.defaultCellphoneLengthInput
                                                                                        },
                                                                                        endAdornment: (
                                                                                            <InputAdornment
                                                                                                position='end'>
                                                                                                    <StayPrimaryPortraitIcon
                                                                                                        color={ validationPersonalInformation.cellphone.error
                                                                                                            ? 'secondary' : 'inherit' }
                                                                                                        fontSize='small' />
                                                                                            </InputAdornment>
                                                                                        )
                                                                                    }}
                                                                                />
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ classes.labelError }>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validationPersonalInformation.cellphone.error
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                        {
                                                                                                            validationPersonalInformation.cellphone.error &&
                                                                                                            validationPersonalInformation.cellphone.message
                                                                                                        }
                                                                                            </Typography>
                                                                                            <Typography
                                                                                                component='span'
                                                                                                variant='inherit'
                                                                                                color={ validationPersonalInformation.cellphone.error
                                                                                                    ? 'secondary' : 'default' }>
                                                                                                    {
                                                                                                        `${personalInformation.cellphone.length}/${Utils.defaultCellphoneLengthInput}`
                                                                                                    }
                                                                                            </Typography>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <TextField
                                                                                    disabled={ !clientFound }
                                                                                    error={ validationPersonalInformation.scannerCode.error ||
                                                                                        !promoter.success }
                                                                                    fullWidth
                                                                                    required
                                                                                    label='Código de Escáner / DNI / Usuario de Red'
                                                                                    type='search'
                                                                                    margin='normal'
                                                                                    name='scannerCode'
                                                                                    value={ personalInformation.scannerCode }
                                                                                    onKeyDown={ e => {
                                                                                        Utils.handleEnterKeyPress(e, _ => getPromoter(e.target.value))
                                                                                    } }
                                                                                    onChange={ e => handleChangeState(e) }
                                                                                    onBlur={ e => handleChangeState(e) }
                                                                                    InputProps={{
                                                                                        inputProps:{
                                                                                            maxLength: 20
                                                                                        },
                                                                                        endAdornment: (
                                                                                            <InputAdornment
                                                                                                position='end'>
                                                                                                    <CodeIcon
                                                                                                        color={ validationPersonalInformation.scannerCode.error ||
                                                                                                            !promoter.success
                                                                                                            ? 'secondary' : 'inherit' }
                                                                                                        fontSize='small'/>
                                                                                            </InputAdornment>
                                                                                        )
                                                                                    }}
                                                                                />
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ 'd-flex justify-content-between small' } >
                                                                                            <FormHelperText
                                                                                                component='span'
                                                                                                className={ !promoter.success || validationPersonalInformation.scannerCode.error 
                                                                                                    ? 'text-red' : 'inherit' }>
                                                                                                    { promoter.error || validationPersonalInformation.scannerCode.message }
                                                                                            </FormHelperText>
                                                                                            <FormHelperText
                                                                                                component='span'
                                                                                                className={ !promoter.success || validationPersonalInformation.scannerCode.error 
                                                                                                    ? 'text-red' : 'inherit' }>
                                                                                                    { 'Presionar <Enter> para buscar promotor' }
                                                                                            </FormHelperText>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <TextField
                                                                                    disabled={ !clientFound }
                                                                                    error={ validationPersonalInformation.nameScannerCode.error }
                                                                                    fullWidth
                                                                                    required
                                                                                    label='Nombre Promotor'
                                                                                    type='text'
                                                                                    margin='normal'
                                                                                    placeholder='Promotor...'
                                                                                    InputLabelProps={{
                                                                                        shrink: true
                                                                                    }}
                                                                                    value={ personalInformation.nameScannerCode }
                                                                                    InputProps={{
                                                                                        readOnly:true,
                                                                                        endAdornment: (
                                                                                            <InputAdornment
                                                                                                position='end'>
                                                                                                    <PermIdentityIcon 
                                                                                                        color={ validationPersonalInformation.nameScannerCode.error
                                                                                                            ? 'secondary' : 'inherit' }
                                                                                                        fontSize='small' />
                                                                                            </InputAdornment>
                                                                                        )
                                                                                    }}
                                                                                />
                                                                                <Fade>
                                                                                    <FormHelperText
                                                                                        className={ 'd-flex justify-content-between small' } >
                                                                                            <FormHelperText
                                                                                                component='span'
                                                                                                className={ validationPersonalInformation.nameScannerCode.error 
                                                                                                    ? 'text-red' : 'inherit' }>
                                                                                                    { validationPersonalInformation.nameScannerCode.message }
                                                                                            </FormHelperText>
                                                                                    </FormHelperText>
                                                                                </Fade>
                                                                        </Grid>
                                                                </Grid>
                                                                <Grid
                                                                    container
                                                                    spacing={ 8 }>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <FormControl
                                                                                    disabled={ !clientFound }
                                                                                    required
                                                                                    margin='normal'
                                                                                    fullWidth
                                                                                    color='primary'
                                                                                    component='fieldset'>
                                                                                    <FormLabel
                                                                                        component='legend'>
                                                                                        ¿Desea habilitar la opción de consumos por internet?
                                                                                    </FormLabel>
                                                                                    <RadioGroup
                                                                                        row
                                                                                        className='d-flex justify-content-md-start'
                                                                                        onChange={ e => handleChangeState(e) }
                                                                                        name='flagInternetConsume'
                                                                                        value={ personalInformation.flagInternetConsume }
                                                                                        aria-label='flagInternetConsume'>
                                                                                            <FormControlLabel
                                                                                                value={ '1' } 
                                                                                                control={<Radio className='d-flex' color='primary' />} 
                                                                                                label='Sí' />
                                                                                            <FormControlLabel
                                                                                                value={ '0' }
                                                                                                control={<Radio className='d-flex' color='primary' />}
                                                                                                label='No'
                                                                                            />
                                                                                    </RadioGroup>
                                                                                </FormControl>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <FormControl
                                                                                    disabled={ !clientFound }
                                                                                    required
                                                                                    margin='normal'
                                                                                    fullWidth
                                                                                    color='primary'
                                                                                    component='fieldset'>
                                                                                    <FormLabel
                                                                                        component='legend'>
                                                                                        ¿Desea habilitar la opción de consumos en el extranjero?
                                                                                    </FormLabel>
                                                                                    <RadioGroup
                                                                                        row
                                                                                        className='d-flex justify-content-md-start'
                                                                                        onChange={ e => handleChangeState(e) }
                                                                                        name='flagForeignConsume'
                                                                                        value={ personalInformation.flagForeignConsume }
                                                                                        aria-label='flagForeignConsume'>
                                                                                            <FormControlLabel
                                                                                                value={ '1' } 
                                                                                                control={<Radio className='d-flex' color='primary' />} 
                                                                                                label='Sí' />
                                                                                            <FormControlLabel
                                                                                                value={ '0' }
                                                                                                control={<Radio className='d-flex' color='primary' />}
                                                                                                label='No'
                                                                                            />
                                                                                    </RadioGroup>
                                                                                </FormControl>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <FormControl
                                                                                    disabled={ !clientFound }
                                                                                    required
                                                                                    margin='normal'
                                                                                    fullWidth
                                                                                    color='primary'
                                                                                    component='fieldset'>
                                                                                    <FormLabel
                                                                                        component='legend'>
                                                                                        ¿Desea contar con la posibilidad de retirar efectivo con cargo a su Tarjeta Cencosud?
                                                                                    </FormLabel>
                                                                                    <RadioGroup
                                                                                        row
                                                                                        className='d-flex justify-content-md-start'
                                                                                        onChange={ e => handleChangeState(e) }
                                                                                        name='flagEffectiveDisposition'
                                                                                        value={ personalInformation.flagEffectiveDisposition }
                                                                                        aria-label='flagEffectiveDisposition'>
                                                                                            <FormControlLabel
                                                                                                value={ '1' } 
                                                                                                control={<Radio className='d-flex' color='primary' />} 
                                                                                                label='Sí' />
                                                                                            <FormControlLabel
                                                                                                value={ '0' }
                                                                                                control={<Radio className='d-flex' color='primary' />}
                                                                                                label='No'
                                                                                            />
                                                                                    </RadioGroup>
                                                                                </FormControl>
                                                                        </Grid>
                                                                        <Grid
                                                                            item
                                                                            xs={ 12 }
                                                                            sm={ 6 }
                                                                            md={ 6 }
                                                                            lg={ 3 }>
                                                                                <FormControl
                                                                                    disabled={ !clientFound }
                                                                                    required
                                                                                    margin='normal'
                                                                                    fullWidth
                                                                                    color='primary'
                                                                                    component='fieldset'>
                                                                                    <FormLabel
                                                                                        component='legend'>
                                                                                        ¿Desea contar con la posibilidad de sobregirar la línea de crédito de su Tarjeta Cencosud?
                                                                                    </FormLabel>
                                                                                    <RadioGroup
                                                                                        row
                                                                                        className='d-flex justify-content-md-start'
                                                                                        onChange={ e => handleChangeState(e) }
                                                                                        name='flagOverdraftCreditCard'
                                                                                        value={ personalInformation.flagOverdraftCreditCard }
                                                                                        aria-label='flagOverdraftCreditCard'>
                                                                                            <FormControlLabel
                                                                                                value={ '1' } 
                                                                                                control={<Radio className='d-flex' color='primary' />} 
                                                                                                label='Sí' />
                                                                                            <FormControlLabel
                                                                                                value={ '0' }
                                                                                                control={<Radio className='d-flex' color='primary' />}
                                                                                                label='No'
                                                                                            />
                                                                                    </RadioGroup>
                                                                                </FormControl>
                                                                        </Grid>
                                                                </Grid>
                                                            </>
                                                    </SectionClientInformation>
                                            </Grid>
                                            <Grid
                                                container
                                                spacing={ 8 }
                                                className='d-flex justify-content-center align-items-center'>
                                                    <Grid
                                                        item
                                                        xs={ 12 }
                                                        sm={ 6 }
                                                        md={ 4 }
                                                        lg={ 3 }>
                                                            <Tooltip
                                                                TransitionComponent={ Zoom }
                                                                title='Cancelar proceso adicional.'>
                                                                    <div>
                                                                        <Bounce>
                                                                            <div
                                                                                className={ classes.wrapper }>
                                                                                    <ActionButton
                                                                                        text='Cancelar'
                                                                                        loading={ loading }
                                                                                        type='secondary'
                                                                                        handleAction={ _ => showModalCancelActivity() }
                                                                                        icon={
                                                                                            <CancelIcon 
                                                                                                fontSize='small' 
                                                                                                className='ml-2' />
                                                                                        }/>
                                                                            </div>
                                                                        </Bounce>
                                                                    </div>
                                                            </Tooltip>
                                                    </Grid>
                                                    <Grid
                                                        item
                                                        xs={ 12 }
                                                        sm={ 6 }
                                                        md={ 4 }
                                                        lg={ 3 }>
                                                            <Tooltip
                                                                TransitionComponent={ Zoom }
                                                                title='Vista previa de datos.'>
                                                                    <div>
                                                                        <Bounce>
                                                                            <div
                                                                                className={ classes.wrapper }>
                                                                                    <ActionButton
                                                                                        text='Previsualizar Información'
                                                                                        loading={ loading || !clientFound }
                                                                                        type='primary'
                                                                                        handleAction={ _ => handleContinueProcess() }
                                                                                        icon={
                                                                                            <SendIcon 
                                                                                                fontSize='small' 
                                                                                                className='ml-2' />
                                                                                        }
                                                                                        showLoading={ loading } />
                                                                            </div>
                                                                        </Bounce>
                                                                    </div>
                                                            </Tooltip>
                                                    </Grid>
                                            </Grid>
                                    </Grid>
                                </Fade>
                    }
            </div>
            <ModalInfoPreview
                open={ modalInfoPreview }
                data={ {
                    principalClient: stateAdditional.principalClient,
                    additionalClient: {
                        ...stateAdditional.additionalClient,
                        ...personalInformation
                    }
                } }
                additionalType={ additionalType }
                close={ _ => setModalInfoPreview(false) }
                handleSaveStateAdditional={ handleSaveStateAdditional }
                handleNextStep={ handleNextStep } />
        </>
    )
}

function mapStateToProps(state) {
    return {
        documentType: state.identificationDocumentTypeReducer,
        promoter: state.promoterReducer,
        gender: state.genderReducer,
        country: state.countryReducer,
        familyRelationship: state.familyRelationshipReducer,
        odcAdditional: state.odcAdditionalReducer,
        constantOdc: state.constantODCReducer,
        odcMaster: state.odcMasterReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getPromoter: bindActionCreators(ActionPromoter.getPromoter, dispatch),
        getGender: bindActionCreators(ActionGender.getGender, dispatch),
        getCountry: bindActionCreators(ActionCountry.getCountry, dispatch),
        getFamilyRelationship: bindActionCreators(ActionFamilyRelationship.getFamilyRelationship, dispatch),
        getConstantOdc: bindActionCreators(ActionConstantOdc.getConstantODC, dispatch),
        validateAdditionalClientAlreadyBelongsHolderClient: bindActionCreators(ActionOdcAdditional.validateAdditionalClientAlreadyBelongsHolderClient, dispatch),
        preloadAdditionalClient: bindActionCreators(ActionOdcMaster.preloadAdditionalClient, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(AdditionalClientInformation))))