// React
import React from 'react'

// Material UI
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Grid, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

const styles = {
    root: {
        borderRadius: '.5em'
    },
    heading: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        textTransform: 'uppercase'
    }
}

function SectionClientInformation(props) {
    const { title, classes, children, expanded, onClick } = props

    return (
        <ExpansionPanel
            expanded={ expanded }
            className={ classes.root }>
                <ExpansionPanelSummary
                    onClick={ onClick }
                    expandIcon={
                        <ExpandMoreIcon
                            fontSize='small'/>
                    }>
                        <Typography
                            className={ classes.heading }>
                                { title }
                        </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Grid
                        container
                        spacing={ 8 }>
                            <Grid 
                                item 
                                xs={ 12 }>
                                    { children }
                            </Grid>
                    </Grid>
                </ExpansionPanelDetails>
        </ExpansionPanel>
    )
}

export default React.memo(withStyles(styles)(SectionClientInformation), (prev, next) =>
    {
        return prev.data === next.data && prev.expanded === next.expanded && 
        prev.extra === next.extra
    }
)