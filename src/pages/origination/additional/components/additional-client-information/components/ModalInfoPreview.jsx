// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Hooks
import useReduxState from '../../../../../../hooks/general/useReduxState'

// Redux - Actions
import * as ActionOdcAdditional from '../../../../../../actions/odc-additional/odc-additional'
import * as ActionOdcMaster from '../../../../../../actions/odc-master/odc-master'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../../utils/Notistack'

// Utils
import AdditionalFactory from '../../../../../../utils/additional/AdditionalFactory'

// Component
import Modal from '../../../../../../components/Modal'
import ActionButton from '../../../../../../components/ActionButton'
import AdditionalClientPreview from '../../../../../../components/info-preview/AdditionalClientPreview'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalInfoPreview(props) {
    const { classes, open = false, close, data, handleNextStep, odcAdditional, registerAdditionalClient,
        additionalType, enqueueSnackbar, handleSaveStateAdditional, odcMaster, registerClientIntoMaster } = props

    const additional = new AdditionalFactory(additionalType).createAdditional()
    const prevOdcAdditional = useRef(odcAdditional)
    const prevOdcMaster = useRef(odcMaster)
    const reintentProcess = useRef(0)

    const [ loading, setLoading ] = useState(false)

    const [ dataRegisterClientIntoMaster, isLoadingRegisterClientIntoMaster, isSuccessRegisterClientIntoMaster,
        isErrorRegisterClientIntoMaster ] =
            useReduxState({ props: odcMaster.clientRegistered, prevProps: prevOdcMaster.current.clientRegistered,
                notify: enqueueSnackbar })
    const [ dataRegisterAdditionalClient, isLoadingRegisterAdditionalClient, isSuccessRegisterAdditionalClient,
        isErrorRegisterAdditionalClient ] =
            useReduxState({ props: odcAdditional.additionalClientRegistered, prevProps: prevOdcAdditional.current.additionalClientRegistered,
                notify: enqueueSnackbar })
    
    useEffect(_ => {
        if (open) setLoading(false)
        reintentProcess.current = 0
    }, [open])

    useEffect(_ => {
        if (isLoadingRegisterAdditionalClient) setLoading(true)
        if (isSuccessRegisterAdditionalClient) {
            reintentProcess.current = 1
            handleContinueProcess()
        }
        if (isErrorRegisterAdditionalClient) setLoading(false)
    }, [dataRegisterAdditionalClient, isLoadingRegisterAdditionalClient, isSuccessRegisterAdditionalClient, isErrorRegisterAdditionalClient])

    useEffect(_ => {
        if (isLoadingRegisterClientIntoMaster) setLoading(true)
        if (isSuccessRegisterClientIntoMaster) {
            const response = additional.processDataResponseOfRegisterAdditionalClient(dataRegisterAdditionalClient || {})
            handleSaveStateAdditional({
                ...data,
                additionalClient: {
                    ...data.additionalClient,
                    ...response
                }
            })
            Notistack.getNotistack('Consulta Correcta, 3er Paso Ok!', enqueueSnackbar, 'success')
            setTimeout(_ => {
                handleNextStep()
            }, 2500)
        }
        if (isErrorRegisterClientIntoMaster) setLoading(false)
    }, [dataRegisterClientIntoMaster, isLoadingRegisterClientIntoMaster, isSuccessRegisterClientIntoMaster, isErrorRegisterClientIntoMaster])

    const handleContinueProcess = _ => {
        const steps = getSteps()
        steps[reintentProcess.current].action()
    }

    const getSteps = _ => ({
        0: {
            action: _ => {
                const sendData = additional.getDataForRegisterAdditionalClientApi(data)
                registerAdditionalClient(sendData)
            }
        },
        1: {
            action: _ => {
                const sendData = additional.getDataForRegisterAdditionalClientIntoMasterApi(data)
                registerClientIntoMaster(sendData)
            }
        }
    })

    return (
        <Modal
            title='Cliente Adicional'
            size='md'
            body={
                <AdditionalClientPreview
                    data={{
                        documentType: data.additionalClient?.documentTypeLetter,
                        documentNumber: data.additionalClient?.documentNumber,
                        firstName: data.additionalClient?.firstName,
                        secondName: data.additionalClient?.secondName,
                        lastName: data.additionalClient?.firstLastName,
                        motherLastName: data.additionalClient?.secondLastName,
                        gender: data.additionalClient?.gender,
                        birthday: data.additionalClient?.birthday,
                        nationality: data.additionalClient?.nationality,
                        cellphone: data.additionalClient?.cellphone,
                        familyRelationship: data.additionalClient?.familyRelationship
                    }}/>
            }
            open={ open }
            actions={
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        loading={ loading }
                                        text='Cancelar'
                                        type='secondary'
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        loading={ loading }
                                        text='Aceptar'
                                        type='primary'
                                        handleAction={ _ => handleContinueProcess() }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
            }/>
    )
}

function mapStateToProps(state) {
    return {
        odcAdditional: state.odcAdditionalReducer,
        odcMaster: state.odcMasterReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerAdditionalClient: bindActionCreators(ActionOdcAdditional.registerAdditionalClient, dispatch),
        registerClientIntoMaster: bindActionCreators(ActionOdcMaster.registerUpdateClient, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalInfoPreview))))