// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, Tooltip, withStyles } from '@material-ui/core'

// Material UI - Colors
import { yellow } from '@material-ui/core/colors'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcAdditional from '../../../../../actions/odc-additional/odc-additional'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'

// Effects
import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import Bounce from 'react-reveal/Bounce'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Utils
import AdditionalFactory from '../../../../../utils/additional/AdditionalFactory'

//Components
import CreditCard from '../../../../../components/CreditCard'
import ActionButton from '../../../../../components/ActionButton'
import ModalPendingEmboss from './components/ModalPendingEmboss'

const styles = {
    wrapper: {
        position: 'relative'
    },
    pendingEmbossButton: {
        backgroundColor: yellow[800],
        transition: '.3s',
        '&:hover': {
            backgroundColor: yellow[900],
        },
    }
}

function CreditCardEmboss(props) {
    const { classes, handleNextStep, handleReset, additionalType, stateAdditional, odcAdditional, 
        registerActivity, enqueueSnackbar } = props

    const additional = new AdditionalFactory(additionalType).createAdditional()
    const prevOdcAdditional = useRef(odcAdditional)

    const [ loading, setLoading ] = useState(false)
    const [ modalPendingEmboss, setModalPendingEmboss ] = useState(false)

    const [ dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity ] =
        useReduxState({ props: odcAdditional.activityRegistered, prevProps: prevOdcAdditional.current.activityRegistered,
            notify: enqueueSnackbar })

    useEffect(_ => {
        if (isLoadingRegisterActivity) setLoading(true)
        if (isSuccessRegisterActivity) {
            Notistack.getNotistack('Consulta Correcta, 7to Paso Ok!', enqueueSnackbar, 'success')
            setTimeout(_ => {
                handleNextStep()
            }, 2500)
        }
        if (isErrorRegisterActivity) setLoading(false)
    }, [dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity])

    const handleContinueProcess = _ => {
        const sendData = additional.getDataForRegisterActivityOnAdditionalEmbossSectionApi(stateAdditional.additionalClient)
        registerActivity(sendData)
    }

    return (
        <>
            <Grid
                container
                spacing={ 8 }
                className='d-flex justify-content-center align-items-center my-5'>
                    <Fade>
                        <CreditCard
                            brand={ (stateAdditional.additionalClient?.creditCard?.brand || 0) === 200001 ? 0 : 1 }
                            type={ stateAdditional.additionalClient?.creditCard?.productType || '' }
                            bin={ stateAdditional.additionalClient?.creditCard?.bin || 0 }
                            cardNumber={ stateAdditional.additionalClient?.creditCard?.cardNumber || '' }
                            client={ `${ stateAdditional.additionalClient?.firstLastName } ${ stateAdditional.additionalClient?.firstName }` }
                            customColor={ stateAdditional.additionalClient?.creditCard?.colorAux || '' } />
                    </Fade>
            </Grid>
            <Grid
                container
                spacing={ 8 }
                className='d-flex justify-content-center align-items-center'>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 6 }
                        md={ 4 }
                        lg={ 3 }>
                            <Tooltip
                                TransitionComponent={ Zoom }
                                title='Proceso pendiente de emboce.'>
                                    <div>
                                        <Bounce 
                                            left>
                                                <div
                                                    className={ classes.wrapper }>
                                                        <ActionButton
                                                            text='Pendiente de Emboce'
                                                            type='secondary'
                                                            loading={ loading }
                                                            className={ classes.pendingEmbossButton }
                                                            handleAction={ _ => setModalPendingEmboss(true) }/>
                                                </div>
                                        </Bounce>
                                    </div>
                            </Tooltip>
                    </Grid>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 6 }
                        md={ 4 }
                        lg={ 3 }>
                            <Tooltip
                                TransitionComponent={ Zoom }
                                title='Continuar proceso adicional.'>
                                    <div>
                                        <Bounce 
                                            right>
                                                <div
                                                    className={ classes.wrapper }>
                                                        <ActionButton
                                                            text='Tarjeta Embosada'
                                                            type='primary'
                                                            loading={ loading }
                                                            handleAction={ _ => handleContinueProcess() }
                                                            icon={
                                                                <SendIcon 
                                                                    fontSize='small' 
                                                                    className='ml-2' />
                                                            }
                                                            showLoading />
                                                </div>
                                        </Bounce>
                                    </div>
                            </Tooltip>
                    </Grid>
            </Grid>
            <ModalPendingEmboss
                open={ modalPendingEmboss }
                close={ _ => setModalPendingEmboss(false) }
                data={ stateAdditional.additionalClient }
                additionalType={ additionalType }
                handleReset={ handleReset } />
        </>
    )
}

function mapStateToProps(state) {
    return {
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerActivity: bindActionCreators(ActionOdcAdditional.registerActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(CreditCardEmboss))))