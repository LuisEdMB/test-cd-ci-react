// React
import React, { useEffect, useRef, useState } from 'react'

// Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Redux - Actions
import * as ActionEmbossReasons from '../../../../../../actions/value-list/reason-embossing'
import * as ActionOdcAdditional from '../../../../../../actions/odc-additional/odc-additional'

// Hooks
import useReduxState from '../../../../../../hooks/general/useReduxState'

// Material UI
import { FormControl, FormHelperText, Grid, TextField, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Utils
import * as Utils from '../../../../../../utils/Utils'
import * as Validations from '../../../../../../utils/Validations'
import AdditionalFactory from '../../../../../../utils/additional/AdditionalFactory'

// Effects
import Fade from 'react-reveal/Fade'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../../utils/Notistack'

// Components
import Modal from '../../../../../../components/Modal'
import ActionButton from '../../../../../../components/ActionButton'
import Autocomplete from '../../../../../../components/Autocomplete'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    },
    labelError: {
        display: 'flex',
        justifyContent: 'space-between', 
        alignItems: 'center'
    }
}

function ModalPendingEmboss(props) {
    const { classes, open = false, close, enqueueSnackbar, handleReset, embossReason, getEmbossReason,
        data, odcAdditional, registerPendingEmboss, additionalType } = props

    const additional = new AdditionalFactory(additionalType).createAdditional()
    const prevOdcAdditional = useRef(odcAdditional)
    const prevEmbossReason = useRef(embossReason)

    const [ loading, setLoading ] = useState(false)
    const [ reasons, setReasons ] = useState([])
    const [ emboss, setEmboss ] = useState({
        embossReasonId: 0,
        description: ''
    })
    const [ validationEmboss, setValidationEmboss ] = useState({
        embossReasonId: {
            error: false,
            message: '',
            ref: useRef(null)
        }
    })

    const [ dataEmbossReason, isLoadingEmbossReason, isSuccessEmbossReason ] =
        useReduxState({ props: embossReason, prevProps: prevEmbossReason.current, notify: enqueueSnackbar })
    const [ dataRegisterPendingEmboss, isLoadingRegisterPendingEmboss, isSuccessRegisterPendingEmboss, isErrorRegisterPendingEmboss ] =
        useReduxState({ props: odcAdditional.pendingEmbossRegistered,
            prevProps: prevOdcAdditional.current.pendingEmbossRegistered, notify: enqueueSnackbar })

    useEffect(_ => {
        if (open) {
            setEmboss({ embossReasonId: 0, description: '' })
            getEmbossReason()
        }
    }, [open])

    useEffect(_ => {
        if (isLoadingEmbossReason) setLoading(true)
        if (isSuccessEmbossReason) {
            const reasonTypes = dataEmbossReason?.map(item => ({
                value: item.cod_valor,
                label: item.des_valor
            })) || []
            setLoading(false)
            setReasons(reasonTypes)
        }
    }, [dataEmbossReason, isLoadingEmbossReason, isSuccessEmbossReason])

    useEffect(_ => {
        if (isLoadingRegisterPendingEmboss) setLoading(true)
        if (isSuccessRegisterPendingEmboss) {
            Notistack.getNotistack('Pendiente de Emboce', enqueueSnackbar, 'info')
            setTimeout(_ => {
                handleReset()
            }, 1500)
        }
        if (isErrorRegisterPendingEmboss) setLoading(false)
    }, [dataRegisterPendingEmboss, isLoadingRegisterPendingEmboss, isSuccessRegisterPendingEmboss, isErrorRegisterPendingEmboss])

    const handleChangeState = e => {
        const { name, value, call = _ => null } = e.target || e
        setEmboss(state => ({
            ...state,
            [name]: value
        }))
        setValidationEmboss(state => {
            let validations = { ...state }
            if (Utils.findProperty(validations, name, ['ref']).length > 0) {
                validations = {
                    ...state,
                    [name]: {
                        ...state[name],
                        ...Validations.validateByField(name, value)
                    }
                }
            }
            call(validations)
            return validations
        })
    }

    const handleContinueProcess = _ => {
        Validations.comprobeAllValidationsSuccess(emboss, handleChangeState).then(valid => {
            if (valid) {
                const sendData = additional.getDataForPendingEmbossCreditCardApi({
                    ...data,
                    ...emboss
                })
                registerPendingEmboss(sendData)
            }
        })
    }

    return (
        <Modal
            title='Pendiente de Emboce'
            body={
                <>
                    <Grid
                        item>
                            <FormControl
                                error={ validationEmboss.embossReasonId.error }
                                fullWidth>
                                    <Autocomplete
                                        error={ validationEmboss.embossReasonId.error }
                                        autoFocus
                                        onChange={ e => handleChangeState({ 
                                            name: 'embossReasonId',
                                            value: e?.value || 0 }) 
                                        }
                                        value={ emboss.embossReasonId }
                                        data={ reasons }
                                        placeholder='Seleccionar motivo' />
                            </FormControl>
                            <Fade>
                                <FormHelperText
                                    className={ classes.labelError }>
                                        <Typography
                                            component='span'
                                            variant='inherit'
                                            color={ validationEmboss.embossReasonId.error 
                                                ? 'secondary' : 'default' }>
                                                    {
                                                        validationEmboss.embossReasonId.error &&
                                                        validationEmboss.embossReasonId.message
                                                    }
                                        </Typography>
                                </FormHelperText>
                            </Fade>
                    </Grid>
                    <Grid
                        item>
                            <TextField
                                fullWidth
                                multiline
                                rows='4'
                                label='Descripción'
                                type='text'
                                margin='normal'
                                color='default'
                                name='description'
                                value={ emboss.description }
                                onChange={ e => {
                                    if (Utils.onlyTextRegex(e.target.value))
                                        handleChangeState(e)
                                }}
                                InputProps={ {
                                    inputProps:{
                                        maxLength: Utils.defaultLengthInput
                                    }
                                } }
                            />
                            <Fade>
                                <FormHelperText
                                    className='text-right'>
                                        <Typography
                                            component='span'
                                            variant='inherit'>
                                                { `${ emboss.description.length }/${ Utils.defaultLengthInput }` }
                                        </Typography>
                                </FormHelperText>
                            </Fade>
                    </Grid>
                </>
            }
            open={ open }
            actions={
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        loading={ loading }
                                        text='Cancelar'
                                        type='secondary'
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        loading={ loading }
                                        text='Aceptar'
                                        type='primary'
                                        handleAction={ _ => handleContinueProcess() }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
            }>

        </Modal>
    )
}

function mapStateToProps(state) {
    return {
        embossReason: state.reasonEmbossingReducer,
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getEmbossReason: bindActionCreators(ActionEmbossReasons.getReasonEmbossing, dispatch),
        registerPendingEmboss: bindActionCreators(ActionOdcAdditional.registerPendingEmboss, dispatch),
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalPendingEmboss))))