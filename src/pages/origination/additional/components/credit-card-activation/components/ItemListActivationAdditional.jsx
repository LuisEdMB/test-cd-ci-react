// React
import React from 'react'

// Material UI
import { Grid, Typography } from '@material-ui/core'

// Material UI - Icons
import DoneIcon from '@material-ui/icons/Done'

function ItemListActivationAdditional({ children }) {
    return (
        <Grid
            item
            xs={ 12 }>
                <Typography>
                    <DoneIcon
                        className='text-green mr-2'
                        fontSize='small' />
                    { children }
                </Typography>
        </Grid>
    )
}

export default React.memo(ItemListActivationAdditional)