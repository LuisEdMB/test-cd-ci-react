// React
import React, { useEffect, useRef, useState } from 'react'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../../utils/Notistack'

// Material UI
import { Grid, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Redux - Actions
import * as ActionOdcAdditional from '../../../../../../actions/odc-additional/odc-additional'

// Hooks
import useReduxState from '../../../../../../hooks/general/useReduxState'

// Utils
import AdditionalFactory from '../../../../../../utils/additional/AdditionalFactory'

// Components
import Modal from '../../../../../../components/Modal'
import ActionButton from '../../../../../../components/ActionButton'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalContingencyProcess(props) {
    const { open = false, close, classes, handleReset, data, additionalType,
        odcAdditional, registerContingencyProcess, enqueueSnackbar } = props

    const additional = new AdditionalFactory(additionalType).createAdditional()
    const prevOdcAdditional = useRef(odcAdditional)

    const [ loading, setLoading ] = useState(false)

    const [ dataRegisterContingencyProcess, isLoadingRegisterContingencyProcess, isSuccessRegisterContingencyProcess, isErrorRegisterContingencyProcess ] =
        useReduxState({ props: odcAdditional.contingencyProcessRegistered, prevProps: prevOdcAdditional.current.contingencyProcessRegistered, notify: enqueueSnackbar })

    useEffect(_ => {
        if (isLoadingRegisterContingencyProcess) setLoading(true)
        if (isSuccessRegisterContingencyProcess) {
            Notistack.getNotistack('Pendiente de Activación de TC.', enqueueSnackbar, 'info')
            setTimeout(_ => {
                handleReset()
            }, 1500)
        }
        if (isErrorRegisterContingencyProcess) setLoading(false)
    }, [dataRegisterContingencyProcess, isLoadingRegisterContingencyProcess, isSuccessRegisterContingencyProcess, isErrorRegisterContingencyProcess])

    const handleContinueProcess = _ => {
        const sendData = additional.getDataForContingencyProcessApi(data)
        registerContingencyProcess(sendData)
    }

    return (
        <Modal
            title='Pendiente de Activación'
            body={
                <Typography
                    align='center'>
                        La tarjeta estará en la bandeja de pendientes por activar.
                </Typography>
            }
            open={ open } 
            actions={ 
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        loading={ loading }
                                        text='Cancelar'
                                        type='secondary'
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        loading={ loading }
                                        text='Aceptar'
                                        type='primary'
                                        handleAction={ _ => handleContinueProcess() }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
        }/>
    )
}

function mapStateToProps(state) {
    return {
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerContingencyProcess: bindActionCreators(ActionOdcAdditional.registerContingencyProcess, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalContingencyProcess))))