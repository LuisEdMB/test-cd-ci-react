// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, Tooltip, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'
import CancelIcon from '@material-ui/icons/Cancel'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcAdditional from '../../../../../actions/odc-additional/odc-additional'
import * as ActionPmp from '../../../../../actions/pmp/pmp'
import * as ActionPci from '../../../../../actions/pci/pci'
import * as ActionOdcMaster from '../../../../../actions/odc-master/odc-master'

// Hooks
import useCreateCreditCard from '../../../../../hooks/pmp/useCreateCreditCard'
import useEncryptCardNumber from '../../../../../hooks/pci/useEncryptCardNumber'
import useCreateCreditCardMaster from '../../../../../hooks/master/useCreateCreditCard'
import useReduxState from '../../../../../hooks/general/useReduxState'

// Effects
import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import Bounce from 'react-reveal/Bounce'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Utils
import * as Constants from '../../../../../utils/Constants'
import AdditionalFactory from '../../../../../utils/additional/AdditionalFactory'

//Components
import CreditCard from '../../../../../components/CreditCard'
import ActionButton from '../../../../../components/ActionButton'

const styles = {
    wrapper: {
        position: 'relative'
    }
}

function CreditCardConfirm(props) {
    const { classes, handleNextStep, additionalType, odcAdditional, pmp, pci, registerActivity, 
        createCreditCard, encryptCardnumber, odcMaster, registerCreditCardIntoMaster, enqueueSnackbar,
        showModalCancelActivity, stateAdditional, handleSaveStateAdditional, phaseCodeFromContinueProcess } = props

    const additional = new AdditionalFactory(additionalType).createAdditional()
    const reintentProcess = useRef(0)
    const prevOdcAdditional = useRef(odcAdditional)
    const prevPmp = useRef(pmp)
    const prevPci = useRef(pci)
    const prevOdcMaster = useRef(odcMaster)

    const [ loading, setLoading ] = useState(false)

    const [ dataCreateCreditCard, isLoadingCreateCreditCard, isSuccessCreateCreditCard, isErrorCreateCreditCard ] =
        useCreateCreditCard({ props: pmp.createCreditCard, prevProps: prevPmp.current.createCreditCard, notify: enqueueSnackbar, 
            options: { flowName: 'Adicional' } })
    const [ dataEncryptCardNumber, isLoadingEncryptCardNumber, isSuccessEncryptCardNumber, isErrorEncryptCardNumber, codeErrorEncryptCardNumber ] =
        useEncryptCardNumber({ props: pci.cardnumberEncrypted, prevProps: prevPci.current.cardnumberEncrypted, notify: enqueueSnackbar, 
            options: { flowName: 'Adicional' } })
    const [ isLoadingCreateCreditCardMaster, isSuccessCreateCreditCardMaster, isErrorCreateCreditCardMaster ] =
        useCreateCreditCardMaster({ props: odcMaster.creditCardForExternalRegistered,
            prevProps: prevOdcMaster.current.creditCardForExternalRegistered, notify: enqueueSnackbar,
            options: { flowName: 'Adicional' } })
    const [ dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity ] =
        useReduxState({ props: odcAdditional.activityRegistered, prevProps: prevOdcAdditional.current.activityRegistered,
            notify: enqueueSnackbar })

    useEffect(_ => {
        reintentProcess.current = additional.getReintentProcessForCreditCardConfirmSection(phaseCodeFromContinueProcess)
    }, [])

    useEffect(_ => {
        if (isLoadingCreateCreditCard) setLoading(true)
        if (isSuccessCreateCreditCard) {
            reintentProcess.current = 1
            handleCreateCreditCard()
        }
        if (isErrorCreateCreditCard) setLoading(false)
    }, [dataCreateCreditCard, isLoadingCreateCreditCard, isSuccessCreateCreditCard, isErrorCreateCreditCard])

    useEffect(_ => {
        if (isLoadingEncryptCardNumber) setLoading(true)
        if (isSuccessEncryptCardNumber) {
            reintentProcess.current = 2
            handleCreateCreditCard()
        }
        if (isErrorEncryptCardNumber) {
            if (codeErrorEncryptCardNumber === Constants.CustomErrorCode.errorCodeCreditCardAlreadyPci) {
                reintentProcess.current = 2
                handleCreateCreditCard()
            } else setLoading(false)
        }
    }, [dataEncryptCardNumber, isLoadingEncryptCardNumber, isSuccessEncryptCardNumber, isErrorEncryptCardNumber, codeErrorEncryptCardNumber])

    useEffect(_ => {
        if (isLoadingCreateCreditCardMaster) setLoading(true)
        if (isSuccessCreateCreditCardMaster) {
            reintentProcess.current = 3
            handleCreateCreditCard()
        }
        if (isErrorCreateCreditCardMaster) setLoading(false)
    }, [isLoadingCreateCreditCardMaster, isSuccessCreateCreditCardMaster, isErrorCreateCreditCardMaster])

    useEffect(_ => {
        if (isLoadingRegisterActivity) setLoading(true)
        if (isSuccessRegisterActivity) {
            const { pmpCrearTarjetaCredito } = dataCreateCreditCard || { }
            const { token } = dataEncryptCardNumber || { }
            const { creditCard } = stateAdditional.additionalClient
            handleSaveStateAdditional({
                ...stateAdditional,
                additionalClient: {
                    ...stateAdditional.additionalClient,
                    creditCard: {
                        ...creditCard,
                        cardNumber: creditCard.cardNumber || pmpCrearTarjetaCredito?.createCreditCard?.cardNumber,
                        token: creditCard.cardNumber || token
                    }
                }
            })
            Notistack.getNotistack('Consulta Correcta, 5to Paso Ok!', enqueueSnackbar, 'success')
            setTimeout(_ => {
                handleNextStep()
            }, 2500)
        }
        if (isErrorRegisterActivity) setLoading(false)
    }, [dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity])

    const handleCreateCreditCard = _ => {
        const steps = getSteps()
        steps[reintentProcess.current].action()
    }

    const getSteps = _ => ({
        0: {
            action: _ => {
                const sendData = additional.getDataForCreateCreditCardApi(stateAdditional)
                createCreditCard(sendData)
            }
        },
        1: {
            action: _ => {
                const sendData = additional.getDataForEncryptCardnumberApi(stateAdditional.additionalClient)
                encryptCardnumber(sendData)
            }
        },
        2: {
            action: _ => {
                const sendData = additional.getDataForCreateCreditCardIntoMasterApi(stateAdditional.additionalClient)
                if (sendData.next) {
                    reintentProcess.current = 3
                    handleCreateCreditCard()
                } else registerCreditCardIntoMaster(sendData)
            }
        },
        3: {
            action: _ => {
                const sendData = additional.getDataForRegisterActivityOnAdditionalConfirmSectionApi(stateAdditional.additionalClient)
                registerActivity(sendData)
            }
        }
    })

    const handleCancelProcess = _ => {
        const sendData = additional.getDataForAdditionalConfirmSectionCancelApi(stateAdditional.additionalClient)
        showModalCancelActivity(sendData)
    }

    return (
        <>
            <Grid
                container
                spacing={ 8 }
                className='d-flex justify-content-center align-items-center my-5'>
                    <Fade>
                        <CreditCard
                            brand={ (stateAdditional.additionalClient?.creditCard?.brand || 0) === 200001 ? 0 : 1 }
                            type={ stateAdditional.additionalClient?.creditCard?.productType || '' }
                            bin={ stateAdditional.additionalClient?.creditCard?.bin || 0 }
                            client={ `${ stateAdditional.additionalClient?.firstLastName } ${ stateAdditional.additionalClient?.firstName }` }
                            cardNumber={ stateAdditional.additionalClient?.creditCard?.cardNumber }
                            customColor={ stateAdditional.additionalClient?.creditCard?.colorAux || '' } />
                    </Fade>
            </Grid>
            <Grid
                container
                spacing={ 8 }
                className='d-flex justify-content-center align-items-center'>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 6 }
                        md={ 4 }
                        lg={ 3 }>
                            <Tooltip
                                TransitionComponent={ Zoom }
                                title='Cancelar proceso adicional.'>
                                    <div>
                                        <Bounce 
                                            left>
                                                <div
                                                    className={ classes.wrapper }>
                                                        <ActionButton
                                                            text='Cancelar'
                                                            type='secondary'
                                                            loading={ loading }
                                                            handleAction={ _ => handleCancelProcess() }
                                                            icon={
                                                                <CancelIcon 
                                                                    fontSize='small' 
                                                                    className='ml-2' />
                                                            }/>
                                                </div>
                                        </Bounce>
                                    </div>
                            </Tooltip>
                    </Grid>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 6 }
                        md={ 4 }
                        lg={ 3 }>
                            <Tooltip
                                TransitionComponent={ Zoom }
                                title='Click para generar TC adicional.'>
                                    <div>
                                        <Bounce 
                                            right>
                                                <div
                                                    className={ classes.wrapper }>
                                                        <ActionButton
                                                            text='Generar TC Adicional'
                                                            type='primary'
                                                            loading={ loading }
                                                            handleAction={ _ => handleCreateCreditCard() }
                                                            icon={
                                                                <SendIcon 
                                                                    fontSize='small' 
                                                                    className='ml-2' />
                                                            }
                                                            showLoading />
                                                </div>
                                        </Bounce>
                                    </div>
                            </Tooltip>
                    </Grid>
            </Grid>
        </>
    )
}

function mapStateToProps(state) {
    return {
        odcAdditional: state.odcAdditionalReducer,
        pmp: state.pmpReducer,
        pci: state.pciReducer,
        odcMaster: state.odcMasterReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerActivity: bindActionCreators(ActionOdcAdditional.registerActivity, dispatch),
        createCreditCard: bindActionCreators(ActionPmp.createCreditCard, dispatch),
        encryptCardnumber: bindActionCreators(ActionPci.encryptCardnumber, dispatch),
        registerCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.registerCreditCardForExternal, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(CreditCardConfirm))))