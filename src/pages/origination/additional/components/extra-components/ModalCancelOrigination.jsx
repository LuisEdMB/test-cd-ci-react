// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Redux - Actions
import * as ActionOdcAdditional from '../../../../../actions/odc-additional/odc-additional'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'

// Notify
import { withSnackbar } from 'notistack'

// Components
import Modal from '../../../../../components/Modal'
import ActionButton from '../../../../../components/ActionButton'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalCancelOrigination(props) {
    const { classes, open = false, close, data, odcAdditional, cancelActivity, enqueueSnackbar, handleReset } = props

    const prevOdcAdditional = useRef(odcAdditional)

    const [ loading, setLoading ] = useState(false)

    const [ dataCancelActivity, isLoadingCancelActivity, isSuccessCancelActivity, isErrorCancelActivity ] = 
        useReduxState({ props: odcAdditional.activityCanceled, prevProps: prevOdcAdditional.current.activityCanceled, notify: enqueueSnackbar })

    useEffect(_ => {
        if (isLoadingCancelActivity) setLoading(true)
        if (isSuccessCancelActivity) {
            handleReset()
            setLoading(false)
        }
        if (isErrorCancelActivity) setLoading(false)
    }, [dataCancelActivity, isLoadingCancelActivity, isSuccessCancelActivity, isErrorCancelActivity])

    const handleRegisterCancelActivity = _ => {
        if (data)
            cancelActivity(data)
        else {
            handleReset()
            setLoading(false)
        }
    }

    return (
        <Modal
            title='Cancelar Proceso Adicional'
            body= { 
                <Typography
                    align='center'>
                        ¿Esta seguro de cancelar el adicional?
                </Typography>
            }
            open={ open } 
            actions={ 
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        loading={ loading }
                                        text='No'
                                        type='secondary'
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        loading={ loading }
                                        text='Sí'
                                        type='primary'
                                        handleAction={ _ => handleRegisterCancelActivity() }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
        }/>
    )
}

function mapStateToProps(state) {
    return {
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        cancelActivity: bindActionCreators(ActionOdcAdditional.cancelActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalCancelOrigination))))