// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { FormControl, Grid, InputLabel, MenuItem, OutlinedInput, Select, TextField, Tooltip, Typography, withStyles, withWidth } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'
import CancelIcon from '@material-ui/icons/Cancel'
import Label from '@material-ui/icons/Label'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionCreditCardBin from '../../../../../actions/value-list/credit-card-bin'
import * as ActionProductColors from '../../../../../actions/value-list/product-color'
import * as ActionConstantOdc from '../../../../../actions/generic/constant'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Effects
import Bounce from 'react-reveal/Bounce'
import Zoom from 'react-reveal/Zoom'
import Fade from 'react-reveal/Fade'

// Utils
import classNames from 'classnames'
import * as Utils from '../../../../../utils/Utils'
import * as Constants from '../../../../../utils/Constants'
import AdditionalFactory from '../../../../../utils/additional/AdditionalFactory'

// Components
import CreditCard from '../../../../../components/CreditCard'
import ActionButton from '../../../../../components/ActionButton'
import DevGridComponent from '../../../../../components/dev-grid/DevGridComponent'
import PreLoaderImage from '../../../../../components/PreLoaderImage'

const styles = {
    root: {
        maxWidth: 900
    },
    wrapper: {
        position: 'relative'
    },
    creditCard: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    menu: {
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center'
    }
}

function HolderClientInformation(props) {
    const { classes, width, handleNextStep, additionalType, odcAdditional,
        handleSaveStateAdditional, enqueueSnackbar, showModalCancelActivity, stateAdditional,
        creditCardBin, getCreditCardBin, productColors, getAllProductColors, constantOdc, getConstantOdc } = props

    const additional = new AdditionalFactory(additionalType).createAdditional()
    const prevCreditCardBin = useRef(creditCardBin)
    const prevProductColors = useRef(productColors)
    const prevConstantOdc = useRef(constantOdc)
    const columnsConfig = additional.showAdditionalDetailTableIntoHolderClientInformationSection
        ? additional.getColumnsAdditionalDetailTableForHolderClientInformationSection()
        : {}
    const prevWidth = useRef(width)

    const [ loading, setLoading ] = useState(true)
    const [ columns, setColumns ] = useState([])
    const [ rows, setRows ] = useState([])
    const [ allowAdditionalClient, setAllowAdditionalClient ] = useState(false)
    const [ holderClient, setHolderClient ] = useState({})
    const [ availableColors, setAvailableColors ] = useState([])

    const [ dataCreditCardBin, isLoadingCreditCardBin, isSuccessCreditCardBin, isErrorCreditCardBin ] =
        useReduxState({ props: creditCardBin, prevProps: prevCreditCardBin.current, notify: enqueueSnackbar,
            options: { stateName: 'Productos' } })
    const [ dataProductColors, isLoadingProductColors, isSuccessProductColors, isErrorProductColors ] =
        useReduxState({ props: productColors, prevProps: prevProductColors.current, notify: enqueueSnackbar,
            options: { stateName: 'Colores Productos' } })
    const [ dataConstantOdc, isLoadingConstantOdc, isSuccessConstantOdc, isErrorConstantOdc ] =
        useReduxState({ props: constantOdc, prevProps: prevConstantOdc.current, notify: enqueueSnackbar,
            options: { stateName: 'Constantes' } })

    useEffect(_ => {
        getCreditCardBin()
        getAllProductColors()
        getConstantOdc()
    }, [])

    useEffect(_ => {
        if (isLoadingCreditCardBin || isLoadingProductColors || isLoadingConstantOdc) setLoading(true)
        if (isSuccessCreditCardBin && isSuccessProductColors && isSuccessConstantOdc) {
            const maxNumberForAdditionalClient = (dataConstantOdc?.find(item => 
                item.des_abv_constante === Constants.General.maxNumberAdditionalClient) || {})?.valor_numerico || 0
            const response = additional.processDataResponseOfConsultClient({...odcAdditional.clientConsulted.data, bins: dataCreditCardBin })
            setHolderClient(response)
            setRows(response.additionals)
            setColumns(columnsConfig[width])
            setAllowAdditionalClient(response.additionalNumber < maxNumberForAdditionalClient)
            setAvailableColors(dataProductColors?.filter(item =>
                item.cod_producto === response?.creditCard?.productId && item.flg_activo) || [])
            setLoading(false)
        }
    }, [ dataCreditCardBin, isLoadingCreditCardBin, isSuccessCreditCardBin, isErrorCreditCardBin,
        dataProductColors, isLoadingProductColors, isSuccessProductColors, isErrorProductColors,
        dataConstantOdc, isLoadingConstantOdc, isSuccessConstantOdc, isErrorConstantOdc])

    useEffect(_ => {
        if (prevWidth.current !== width) setColumns(columnsConfig[width])
    }, [width])

    const handleContinueProcess = _ => {
        setLoading(true)
        handleSaveStateAdditional({
            principalClient: {
                ...stateAdditional.principalClient,
                ...holderClient
            },
            additionalClient: {
                solicitudeCode: holderClient.solicitudeCode || 0,
                completeSolicitudeCode: holderClient.completeSolicitudeCode || '',
            }
        })
        Notistack.getNotistack('Consulta Correcta, 2do Paso Ok!', enqueueSnackbar, 'success')
        setTimeout(_ => {
            handleNextStep()
        }, 1500)
    }

    return (
        <>
            {
                isLoadingCreditCardBin || isLoadingProductColors || isLoadingConstantOdc
                    ?   <PreLoaderImage text='Cargando información inicial ...' />
                    :   <>
                            <Grid
                                container
                                spacing={ 8 }
                                className='d-flex justify-content-center align-items-center'>
                            {
                                additional.showAdditionalDetailTableIntoHolderClientInformationSection 
                                    ?
                                        <Grid
                                            item
                                            xs={ 12 }
                                            sm={ 12 }
                                            md={ 10 }
                                            lg={ 8 }
                                            style={{ overflow: 'auto' }}>
                                                <DevGridComponent
                                                    noData='Sin adicionales registrados'
                                                    columns={ columns }
                                                    rows={ rows }
                                                    width={ width }
                                                    pagination={ false }
                                                    getRowId={ row => row.cod_solicitud }
                                                    defaultColumnWidths={ columnsConfig.default } />
                                        </Grid>
                                    :
                                        <Grid
                                            container
                                            spacing={ 32 }
                                            className={ classNames(classes.root, 'm-0 m-sm-3') }>
                                                <Grid
                                                    item
                                                    xs={ 12 }
                                                    sm={ 12 }
                                                    md={ 6 }
                                                    lg={ 7 }>
                                                        <Grid
                                                            container
                                                            spacing={ 8 } >
                                                                <Grid
                                                                    item
                                                                    xs={ 6 }>
                                                                        <TextField
                                                                            fullWidth
                                                                            label='Tipo Documento'
                                                                            type='text'
                                                                            margin='normal'
                                                                            value={ holderClient?.documentTypeLetter || '' }
                                                                            color='default'
                                                                            variant='outlined'
                                                                            InputProps={ {
                                                                                readOnly: true
                                                                            } } />
                                                                </Grid>
                                                                <Grid
                                                                    item
                                                                    xs={ 6 }>
                                                                        <TextField
                                                                            fullWidth
                                                                            label='Número Documento'
                                                                            type='text'
                                                                            margin='normal'
                                                                            value={ holderClient?.documentNumber || '' }
                                                                            color='default'
                                                                            variant='outlined'
                                                                            InputProps={ {
                                                                                readOnly: true
                                                                            } } />
                                                                </Grid>
                                                                <Grid
                                                                    item
                                                                    xs={ 12 }>
                                                                        <TextField
                                                                            fullWidth
                                                                            label='Nombres y Apellidos'
                                                                            type='text'
                                                                            margin='normal'
                                                                            value={ `${ holderClient.firstLastName } ${ holderClient.secondLastName } ` + 
                                                                                `${ holderClient.firstName } ${ holderClient.secondName }` }
                                                                            color='default'
                                                                            variant='outlined'
                                                                            InputProps={ {
                                                                                readOnly: true
                                                                            } } />
                                                                </Grid>
                                                                <Grid
                                                                    item
                                                                    xs={ 12 }
                                                                    sm={ 6 }>
                                                                        <TextField
                                                                            fullWidth
                                                                            label='Nro. Cuenta'
                                                                            type='text'
                                                                            margin='normal'
                                                                            value={ Utils.maskCreditCard(holderClient.creditCard?.accountNumber || '') }
                                                                            color='default'
                                                                            variant='outlined'
                                                                            InputProps={ {
                                                                                readOnly: true
                                                                            } } />
                                                                </Grid>
                                                                <Grid
                                                                    item
                                                                    xs={ 12 }
                                                                    sm={ 6 }>
                                                                        <TextField
                                                                            fullWidth
                                                                            label='Nro. Tarjeta'
                                                                            type='text'
                                                                            margin='normal'
                                                                            value={ Utils.maskCreditCard(holderClient.creditCard?.cardNumber || '') }
                                                                            color='default'
                                                                            variant='outlined'
                                                                            InputProps={ {
                                                                                readOnly: true
                                                                            } } />
                                                                </Grid>
                                                                <Grid
                                                                    item
                                                                    xs={ 12 }
                                                                    sm={ 4 }>
                                                                        <FormControl
                                                                            fullWidth
                                                                            margin='normal'
                                                                            variant='outlined'>
                                                                                <InputLabel
                                                                                    htmlFor='color'>
                                                                                        Color Tarjeta
                                                                                </InputLabel>
                                                                                <Select
                                                                                    value={ holderClient.creditCard?.color || 0 }
                                                                                    input={
                                                                                        <OutlinedInput
                                                                                            readOnly
                                                                                            labelWidth={ 95 } />
                                                                                    }
                                                                                    displayEmpty>
                                                                                        <MenuItem
                                                                                            value=''
                                                                                            disabled>
                                                                                                Seleccionar Color
                                                                                        </MenuItem>
                                                                                        {
                                                                                            availableColors.map((item, index) => (
                                                                                                <MenuItem
                                                                                                    key={ index }
                                                                                                    value={ item.cod_color }>
                                                                                                        <div
                                                                                                            className={ classes.menu }>
                                                                                                            <Label
                                                                                                                style={{ color: item.des_aux_color, fontSize: 12}}
                                                                                                                variant='inherit'/>
                                                                                                            <Typography
                                                                                                                component='span'
                                                                                                                className='px-2'
                                                                                                                variant='inherit'>
                                                                                                                    { item.des_color }
                                                                                                            </Typography>
                                                                                                        </div>
                                                                                                </MenuItem>
                                                                                            ))
                                                                                        }
                                                                                </Select>
                                                                        </FormControl>
                                                                </Grid>
                                                                <Grid
                                                                    item
                                                                    xs={ 12 }
                                                                    sm={ 8 }>
                                                                        <TextField
                                                                            fullWidth
                                                                            label='Producto'
                                                                            type='text'
                                                                            margin='normal'
                                                                            value={ holderClient.creditCard?.productDescription || '' }
                                                                            color='default'
                                                                            variant='outlined'
                                                                            InputProps={ {
                                                                                readOnly: true
                                                                            } } />
                                                                </Grid>
                                                                {
                                                                    allowAdditionalClient && 
                                                                    <Grid
                                                                        item
                                                                        xs={ 12 }>
                                                                            <Typography
                                                                                component='h6'
                                                                                className={ 'text-green' } >
                                                                                    <strong>
                                                                                        Número de adicionales actuales: { holderClient.additionalNumber }
                                                                                    </strong>
                                                                            </Typography>
                                                                    </Grid>
                                                                }
                                                        </Grid>
                                                </Grid>
                                                <Grid
                                                    item
                                                    xs={ 12 }
                                                    sm={ 12 }
                                                    md={ 6 }
                                                    lg={ 5 }>
                                                        <Grid
                                                            container
                                                            spacing={ 8 }
                                                            className='d-flex justify-content-center'>
                                                                <Grid
                                                                    item
                                                                    xs={ 12 }
                                                                    className={ classNames(classes.creditCard, 'py-5 rounded') }>
                                                                        <div
                                                                            style={{ minHeight: 230 }}>
                                                                                {
                                                                                    <Fade>
                                                                                        <CreditCard
                                                                                            client={ `${ holderClient.firstLastName } ${ holderClient.firstName }` }
                                                                                            type={ holderClient.creditCard?.productType || '' }
                                                                                            customColor={ holderClient.creditCard?.colorAux || '' }
                                                                                            bin={ holderClient.creditCard?.bin || 0 }
                                                                                            cardNumber={ holderClient.creditCard?.cardNumber || '' }
                                                                                            brand={ holderClient.creditCard?.brand === 200001 ? 0 : 1 } />
                                                                                    </Fade>
                                                                                }
                                                                        </div>
                                                                </Grid>
                                                        </Grid>
                                                </Grid>
                                        </Grid>
                            }
                            </Grid>
                            <Grid
                                container
                                spacing={ 8 }
                                className='d-flex justify-content-center align-items-center'>
                                    <>
                                        {
                                            !allowAdditionalClient &&
                                                <Grid
                                                    item
                                                    xs={ 12 }
                                                    sm={ 12 }
                                                    md={ 12 }
                                                    lg={ 12 }>
                                                        <Typography
                                                            component='h6'
                                                            align='center'
                                                            className={ 'text-red' } >
                                                                <strong>
                                                                    No puede generar más clientes adicionales: Máximo { holderClient.additionalNumber } registros
                                                                </strong>
                                                        </Typography>
                                                </Grid>
                                        }
                                        <Grid
                                            item
                                            xs={ 12 }
                                            sm={ 6 }
                                            md={ 4 }
                                            lg={ 3 }>
                                                <Tooltip
                                                    TransitionComponent={ Zoom }
                                                    title='Cancelar proceso adicional.'>
                                                        <div>
                                                            <Bounce 
                                                                left>
                                                                    <div
                                                                        className={ classes.wrapper }>
                                                                            <ActionButton
                                                                                text='Cancelar'
                                                                                type='secondary'
                                                                                loading={ loading }
                                                                                handleAction={ _ => showModalCancelActivity() }
                                                                                icon={
                                                                                    <CancelIcon 
                                                                                        fontSize='small' 
                                                                                        className='ml-2' />
                                                                                }/>
                                                                    </div>
                                                            </Bounce>
                                                        </div>
                                                </Tooltip>
                                        </Grid>
                                        {
                                            allowAdditionalClient &&
                                                <Grid
                                                    item
                                                    xs={ 12 }
                                                    sm={ 6 }
                                                    md={ 4 }
                                                    lg={ 3 }>
                                                        <Tooltip
                                                            TransitionComponent={ Zoom }
                                                            title='Continuar proceso adicional.'>
                                                                <div>
                                                                    <Bounce 
                                                                        right>
                                                                            <div
                                                                                className={ classes.wrapper }>
                                                                                    <ActionButton
                                                                                        text='Continuar'
                                                                                        type='primary'
                                                                                        loading={ loading }
                                                                                        handleAction={ _ => handleContinueProcess() }
                                                                                        icon={
                                                                                            <SendIcon 
                                                                                                fontSize='small' 
                                                                                                className='ml-2' />
                                                                                        }
                                                                                        showLoading />
                                                                            </div>
                                                                    </Bounce>
                                                                </div>
                                                        </Tooltip>
                                                </Grid>
                                        }
                                    </>
                            </Grid>
                        </>
            }
        </>
    )
}

function mapStateToProps(state) {
    return {
        creditCardBin: state.creditCardBinReducer,
        productColors: state.productColorReducer,
        constantOdc: state.constantODCReducer,
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getCreditCardBin: bindActionCreators(ActionCreditCardBin.getCreditCardBin, dispatch),
        getAllProductColors: bindActionCreators(ActionProductColors.getAllProductColors, dispatch),
        getConstantOdc: bindActionCreators(ActionConstantOdc.getConstantODC, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(HolderClientInformation)))))