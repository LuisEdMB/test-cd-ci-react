// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { FormHelperText, Grid, IconButton, Tooltip, withStyles } from '@material-ui/core'

// Material UI - Icons
import LensIcon from '@material-ui/icons/Lens'
import SendIcon from '@material-ui/icons/Send'
import CancelIcon from '@material-ui/icons/Cancel'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionProductColors from '../../../../../actions/value-list/product-color'
import * as ActionOdcAdditional from '../../../../../actions/odc-additional/odc-additional'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Effects
import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import Bounce from 'react-reveal/Bounce'

// Utils
import classNames from 'classnames'
import AdditionalFactory from '../../../../../utils/additional/AdditionalFactory'

// Components
import CreditCard from '../../../../../components/CreditCard'
import ActionButton from '../../../../../components/ActionButton'

const styles = {
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems:'center',
        flexWrap: 'wrap'
    },
    wrapper: {
        position: 'relative'
    },
    colorButtonWrapper: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    selected: {
        border: '2px solid gold',
        borderRadius: '50%'
    },
    noSelected: {
        border: '2px solid white',
        borderRadius: '50%'
    }
}

function CreditCardDetail(props) {
    const { classes, productColors, getAllProductColors, handleNextStep, additionalType,
        stateAdditional, odcAdditional, registerAdditionalDetail, enqueueSnackbar,
        handleSaveStateAdditional, showModalCancelActivity } = props

    const additional = new AdditionalFactory(additionalType).createAdditional()
    const prevOdcAdditional = useRef(odcAdditional)
    const prevProductColors = useRef(productColors)

    const [ loading, setLoading ] = useState(false)
    const [ availableColors, setAvailableColors ] = useState([])
    const [ color, setColor ] = useState({
        select: -1,
        color: 0,
        colorAux: ''
    })

    const [ dataProductColors, isLoadingProductColors, isSuccessProductColors, isErrorProductColors ] =
        useReduxState({ props: productColors, prevProps: prevProductColors.current, notify: enqueueSnackbar,
            options: { stateName: 'Colores Productos' } })
    const [ dataRegisterAdditionalDetail, isLoadingRegisterAdditionalDetail, isSuccessRegisterAdditionalDetail, isErrorRegisterAdditionalDetail ] =
        useReduxState({ props: odcAdditional.additionalDetailRegistered, prevProps: prevOdcAdditional.current.additionalDetailRegistered,
            notify: enqueueSnackbar  })

    useEffect(_ => {
        setColor(state => ({
            ...state,
             color: stateAdditional.principalClient?.creditCard?.color || 0,
             colorAux: stateAdditional.principalClient?.creditCard?.colorAux || '',
        }))
        getAllProductColors()
    }, [])

    useEffect(_ => {
        if (isLoadingProductColors) setLoading(true)
        if (isSuccessProductColors) {
            setAvailableColors(dataProductColors?.filter(item =>
                item.cod_producto === stateAdditional.principalClient?.creditCard?.productId &&
                item.flg_activo) || [])
            setLoading(false)
        }
    }, [dataProductColors, isLoadingProductColors, isSuccessProductColors, isErrorProductColors])

    useEffect(_ => {
        if (isLoadingRegisterAdditionalDetail) setLoading(true)
        if (isSuccessRegisterAdditionalDetail) {
            Notistack.getNotistack('Consulta Correcta, 4to Paso Ok!', enqueueSnackbar, 'success')
            setTimeout(_ =>
                handleNextStep(),
            2500)
        }
        if (isErrorRegisterAdditionalDetail) setLoading(false)
    }, [dataRegisterAdditionalDetail, isLoadingRegisterAdditionalDetail, isSuccessRegisterAdditionalDetail, isErrorRegisterAdditionalDetail])

    const handleChangeColor = (item, index) => {
        setColor({
            select: index,
            color: item.cod_color,
            colorAux: item.des_aux_color
        })
    }

    const handleContinueProcess = _ => {
        if (!color.color && availableColors.length) {
            Notistack.getNotistack('Seleccione el color de la tarjeta.', enqueueSnackbar, 'warning')
            return
        }
        const data = {
            ...stateAdditional.additionalClient,
            creditCard: {
                ...stateAdditional.principalClient?.creditCard,
                ...color,
                cardNumber: ''
            }
        }
        const sendData = additional.getDataForRegisterAdditionalDetailApi(data)
        handleSaveStateAdditional({
            ...stateAdditional,
            additionalClient: data
        })
        registerAdditionalDetail(sendData)
    }

    const handleCancelProcess = _ => {
        const sendData = additional.getDataForAdditionalDetailSectionCancelApi(stateAdditional.additionalClient)
        showModalCancelActivity(sendData)
    }

    return (
        <>
            <Grid
                container
                spacing={ 8 }
                className='mb-2'>
                    <Grid
                        item
                        xs={ 12 }
                        className={ classes.root }>
                            <div
                                className='mx-5 mb-3'>
                                    <Fade>
                                        <div className='mb-2 py-4'>
                                            <CreditCard
                                                brand={ (stateAdditional.principalClient?.creditCard?.brand || 0) === 200001 ? 0 : 1 }
                                                type={ stateAdditional.principalClient?.creditCard?.productType || '' }
                                                bin={ stateAdditional.principalClient?.creditCard?.bin || 0 }
                                                client={ `${ stateAdditional.additionalClient?.firstLastName } ${ stateAdditional.additionalClient?.firstName }` }
                                                customColor={ color.colorAux } />
                                        </div>
                                        <div
                                            className={ classes.colorButtonWrapper }>
                                                {
                                                    availableColors.length > 0 && 
                                                        <>
                                                            <FormHelperText>Seleccionar Color:</FormHelperText>
                                                            {
                                                                availableColors.map((item, index) => (
                                                                    <div key={ index }>
                                                                        <Tooltip
                                                                            TransitionComponent={ Zoom }
                                                                            title={ item.des_color }>
                                                                                <IconButton
                                                                                    size='small'
                                                                                    onClick={ _ => handleChangeColor(item, index) }>
                                                                                <LensIcon
                                                                                    style={{ color: item.des_aux_color }}
                                                                                    className={
                                                                                        classNames(
                                                                                            { [classes.noSelected]: index !== color.select },
                                                                                            { [classes.selected]: index === color.select }
                                                                                        )
                                                                                    }/>
                                                                            </IconButton>
                                                                        </Tooltip>
                                                                    </div>
                                                                ))
                                                            }
                                                        </>
                                                }
                                        </div>
                                    </Fade>
                            </div>
                    </Grid>
                    <Grid
                        container
                        spacing={ 8 }
                        className='d-flex justify-content-center align-items-center'>
                            <Grid
                                item
                                xs={ 12 }
                                sm={ 6 }
                                md={ 4 }
                                lg={ 3 }>
                                    <Tooltip
                                        TransitionComponent={ Zoom }
                                        title='Cancelar proceso adicional.'>
                                            <div>
                                                <Bounce>
                                                    <div
                                                        className={ classes.wrapper }>
                                                            <ActionButton
                                                                text='Cancelar'
                                                                loading={ loading }
                                                                type='secondary'
                                                                handleAction={ _ => handleCancelProcess() }
                                                                icon={
                                                                    <CancelIcon 
                                                                        fontSize='small' 
                                                                        className='ml-2' />
                                                                }/>
                                                    </div>
                                                </Bounce>
                                            </div>
                                    </Tooltip>
                            </Grid>
                            <Grid
                                item
                                xs={ 12 }
                                sm={ 6 }
                                md={ 4 }
                                lg={ 3 }>
                                    <Tooltip
                                        TransitionComponent={ Zoom }
                                        title='Continuar proceso adicional.'>
                                            <div>
                                                <Bounce>
                                                    <div
                                                        className={ classes.wrapper }>
                                                            <ActionButton
                                                                text='Continuar'
                                                                loading={ loading }
                                                                type='primary'
                                                                handleAction={ _ => handleContinueProcess() }
                                                                icon={
                                                                    <SendIcon 
                                                                        fontSize='small' 
                                                                        className='ml-2' />
                                                                }
                                                                showLoading />
                                                    </div>
                                                </Bounce>
                                            </div>
                                    </Tooltip>
                            </Grid>
                    </Grid>
            </Grid>
        </>
    )
}

function mapStateToProps(state) {
    return {
        productColors: state.productColorReducer,
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getAllProductColors: bindActionCreators(ActionProductColors.getAllProductColors, dispatch),
        registerAdditionalDetail: bindActionCreators(ActionOdcAdditional.registerAdditionalDetail, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(CreditCardDetail))))