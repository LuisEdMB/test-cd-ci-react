// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { FormControl, FormHelperText, Grid, InputAdornment, InputLabel, MenuItem, Select, TextField, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'
import RecentActorsIcon from '@material-ui/icons/RecentActors'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionDocumentType from '../../../../../actions/value-list/identification-document-type'
import * as ActionOdcAdditional from '../../../../../actions/odc-additional/odc-additional'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'

// Effects
import Fade from 'react-reveal/Fade'
import Bounce from 'react-reveal/Bounce'

// Utils
import classNames from 'classnames'
import * as Utils from '../../../../../utils/Utils'
import * as Validations from '../../../../../utils/Validations'
import AdditionalFactory from '../../../../../utils/additional/AdditionalFactory'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Components
import ActionButton from '../../../../../components/ActionButton'

const styles = theme => ({
    root: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            justifyContent: 'center'
        }
    },
    buttonWrapper: {
        display: 'flex',
        justifyContent: 'center'
    },
    button: {
        textTransform: 'none'
    },
    wrapper: {
        position: 'relative'
    },
    documentNumberFormHelperText: {
        display: 'flex',
        justifyContent: 'space-between', 
        alignItems: 'center'
    }
})

function ClientConsult(props) {
    const { classes, documentType, getDocumentType, enqueueSnackbar, handleNextStep,
        additionalType, consultClient, odcAdditional, handleSaveStateAdditional } = props

    const additional = new AdditionalFactory(additionalType).createAdditional()
    const prevOdcAdditional = useRef(odcAdditional)
    const prevDocumentType = useRef(documentType)

    const [ loading, setLoading ] = useState(true)
    const [ client, setClient ] = useState({
        documentTypeId: 100001,
        documentNumber: ''
    })
    const [ validations, setValidations ] = useState({
        documentTypeId: {
            message: '',
            error: false,
            ref: useRef(null)
        },
        documentNumber: {
            message: '',
            error: false,
            ref: useRef(null)
        }
    })

    const [ dataDocumentType, isLoadingDocumentType, isSuccessDocumentType, isErrorDocumentType ] =
        useReduxState({ props: documentType, prevProps: prevDocumentType.current, notify: enqueueSnackbar })
    const [ dataConsultClient, isLoadingConsultClient, isSuccessConsultClient, isErrorConsultClient ] =
        useReduxState({ props: odcAdditional.clientConsulted, prevProps: prevOdcAdditional.current.clientConsulted, notify: enqueueSnackbar })

    useEffect(_ => {
        getDocumentType()
    }, [])

    useEffect(_ => {
        if (isSuccessDocumentType) setLoading(false)
    }, [dataDocumentType, isLoadingDocumentType, isSuccessDocumentType, isErrorDocumentType])

    useEffect(_ => {
        if (isLoadingConsultClient) setLoading(true)
        if (isSuccessConsultClient) {
            Notistack.getNotistack('Consulta Correcta, 1er Paso Ok!', enqueueSnackbar, 'success')
            setTimeout(_ => {
                handleNextStep()
            }, 1500)
        }
        if (isErrorConsultClient) setLoading(false)
    }, [dataConsultClient, isLoadingConsultClient, isSuccessConsultClient, isErrorConsultClient])

    const handleChangeState = e => {
        const { name, value, call = _ => null } = e.target || e
        setClient(state => ({
            ...state,
            [name]: value
        }))
        setValidations(state => {
            let validations = { ...state }
            if (Utils.findProperty(validations, name, ['ref']).length > 0) {
                validations = {
                    ...state,
                    [name]: {
                        ...state[name],
                        ...Validations.validateByField(name, value)
                    }
                }
            }
            call(validations)
            return validations
        })
    }

    const handleContinueProcess = _ => {
        Validations.comprobeAllValidationsSuccess(client, handleChangeState).then(valid => {
            if (valid) {
                const document = documentType.data.find(document => document.cod_valor === client.documentTypeId)
                const data = {
                    documentTypeAux: document.des_auxiliar,
                    documentNumber: client.documentNumber,
                    documentTypeLetter: document.des_valor_corto
                }
                handleSaveStateAdditional({ principalClient: data })
                const sendData = additional.getDataForConsultClientApi(data)
                consultClient(sendData)
            }
        })
    }

    return (
        <>
            <Grid
                container
                spacing={ 8 }
                className={ classNames(classes.root, 'mb-2') }>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 6 }
                        md={ 4 }
                        lg={ 3 }>
                            <FormControl
                                margin='normal'
                                disabled={ loading }
                                fullWidth>
                                    <InputLabel
                                        required
                                        error={ validations.documentTypeId.error }>
                                            Tipo de Documento
                                    </InputLabel>
                                    <Select
                                        error={ validations.documentTypeId.error }
                                        value={ client.documentTypeId }
                                        name='documentTypeId'
                                        inputRef={ validations.documentTypeId.ref }
                                        onChange={ handleChangeState }
                                        onBlur={ handleChangeState }>
                                        {
                                            documentType.data.map((type, index) => 
                                                <MenuItem
                                                    key={ index }
                                                    value={ type.cod_valor }>
                                                        { type.des_valor_corto }
                                                </MenuItem> )
                                        }
                                    </Select>
                            </FormControl>
                            <Fade>
                                <FormHelperText
                                    className={ classes.documentNumberFormHelperText }>
                                        <Typography
                                            component='span'
                                            variant='inherit'
                                            color={ validations.documentTypeId.error 
                                                ? 'secondary' : 'default' 
                                            }>
                                                {
                                                    validations.documentTypeId.error &&
                                                    validations.documentTypeId.message
                                                }
                                        </Typography>
                                </FormHelperText>
                            </Fade>
                    </Grid>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 6 }
                        md={ 4 }
                        lg={ 3 }>
                            <TextField
                                error={ validations.documentNumber.error }
                                fullWidth
                                label='Número Documento'
                                type='text'
                                margin='normal'
                                color='default'
                                required
                                autoFocus
                                value={ client.documentNumber }
                                inputRef={ validations.documentNumber.ref }
                                onChange={ e => {
                                    if (Utils.onlyNumberRegex(e.target.value)) handleChangeState(e)
                                } }
                                onKeyPress={ e => Utils.handleEnterKeyPress(e, handleContinueProcess) }
                                name='documentNumber'
                                InputProps={{
                                    inputProps: {
                                        maxLength: Utils.getLengthDocumentByType(client.documentTypeId)
                                    },
                                    endAdornment: (
                                        <InputAdornment
                                            position='end'>
                                                <RecentActorsIcon 
                                                    color={ validations.documentNumber.error 
                                                        ? 'secondary' : 'inherit' 
                                                    }
                                                    fontSize='small'/>
                                        </InputAdornment>
                                    )
                                }}/>
                            <Fade>
                                <FormHelperText
                                    className={ classes.documentNumberFormHelperText }>
                                        <Typography
                                            component='span'
                                            variant='inherit'
                                            color={ validations.documentNumber.error 
                                                ? 'secondary' : 'default' 
                                            }>
                                                {
                                                    validations.documentNumber.error &&
                                                    validations.documentNumber.message
                                                }
                                        </Typography>
                                        <Typography
                                            component='span'
                                            variant='inherit'
                                            color={ validations.documentNumber.error 
                                                ? 'secondary' : 'default' 
                                            }>
                                                {
                                                    `${client.documentNumber.length}/${Utils.getLengthDocumentByType(client.documentTypeId)}`
                                                }
                                        </Typography>
                                </FormHelperText>
                            </Fade>
                    </Grid>
            </Grid>
            <Grid
                container
                spacing={ 8 }
                className={ classes.buttonWrapper }>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 6 }
                        md={ 4 }
                        lg={ 3 }>
                            <Bounce>
                                <div
                                    className={ classNames(classes.wrapper) }>
                                        <ActionButton
                                            text='Continuar'
                                            loading={ loading }
                                            type='primary'
                                            handleAction={ () => handleContinueProcess() }
                                            icon={ 
                                                <SendIcon 
                                                    fontSize='small' 
                                                    className='ml-2'/>
                                            }
                                            showLoading={ loading }/>
                                </div>
                            </Bounce>
                    </Grid>
            </Grid>
        </>
    )
}

function mapStateToProps(state) {
    return {
        documentType: state.identificationDocumentTypeReducer,
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getDocumentType: bindActionCreators(ActionDocumentType.getIdentificationDocumentType, dispatch),
        consultClient: bindActionCreators(ActionOdcAdditional.consultClient, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ClientConsult))))