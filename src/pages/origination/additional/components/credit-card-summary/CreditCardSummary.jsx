// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Divider, Grid, Tooltip, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import LabelIcon from '@material-ui/icons/Label'
import CreditCardIcon from '@material-ui/icons/CreditCard'
import CardMembershipIcon from '@material-ui/icons/CardMembership'
import LabelImportantIcon from '@material-ui/icons/LabelImportant'
import LensIcon from '@material-ui/icons/Lens'
import SendIcon from '@material-ui/icons/Send'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcAdditional from '../../../../../actions/odc-additional/odc-additional'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Utils
import classNames from 'classnames'
import * as Utils from '../../../../../utils/Utils'
import AdditionalFactory from '../../../../../utils/additional/AdditionalFactory'

// Effects
import Fade from 'react-reveal/Fade'
import Bounce from 'react-reveal/Bounce'
import Zoom from 'react-reveal/Zoom'
import * as Fireworks from 'fireworks-canvas'

// Components
import Image from '../../../../../components/Image'

// Media
import LogoCencosud from '../../../../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg'
import ActionButton from '../../../../../components/ActionButton'
import ItemReprintSummary from './components/ItemReprintSummary'

const styles = theme => ({
    root: {
        position: 'relative'
    },
    firework: {
        width: '100%',
        height: 500,
        position: 'absolute',
        top: 20
    },
    summaryWrapper: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    summary: {
        minWidth: 300,
        maxWidth: 700
    },
    titleContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatarTitleWrapper: {
        [theme.breakpoints.down('sm')]: {
            display: 'none'
        },
    },
    avatarTitle: {
        margin: '0 .5em',
        height: 36, 
        width: 64,
        [theme.breakpoints.down('sm')]: {

        },
        [theme.breakpoints.up('md')]: {
            height: 48, 
            width: 80
        },
        [theme.breakpoints.up('lg')]: {

        }
    },
    titleWrapper: {
        margin: '0 1em',
    },
    title: {
        [theme.breakpoints.down('sm')]: {
            fontSize: 18
        }
    },
    dateWrapper: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
    },
    headerContainer: {
        display: 'flex', 
        flexDirection: 'column',
        [theme.breakpoints.up('md')]: {
            flexDirection: 'row',
            justifyContent: 'space-between'
        }
    },
    bodyContainer: {
        padding: '.5em',
        border: '1px solid #c4c4c4',
        borderRadius: '.25rem',
        [theme.breakpoints.up('md')]: {
            padding: '1.5em',
        }
    },
    buttonActions: {
        display: 'flex', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    buttonProgressWrapper: {
        position: 'relative'
    }
})

function CreditCardSummary(props) {
    const { classes, handleNextStep, additionalType, stateAdditional, odcAdditional, registerActivity, enqueueSnackbar } = props

    const additional = new AdditionalFactory(additionalType).createAdditional()
    const prevOdcAdditional = useRef(odcAdditional)

    const [ loading, setLoading ] = useState(false)

    const [ dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity ] =
        useReduxState({ props: odcAdditional.activityRegistered, prevProps: prevOdcAdditional.current.activityRegistered, notify: enqueueSnackbar })

    useEffect(_ => {
        const container = document.getElementById('summary-container')
        const options = {
            maxRockets: 3,
            rocketSpawnInterval: 10,
            numParticles: 70,
            explosionMinHeight: 0.2,
            explosionMaxHeight: 0.9,
            explosionChance: 0.08
        }
        const fireworks = new Fireworks(container, options)
        fireworks.start()
        fireworks.fire()
    }, [])

    useEffect(_ => {
        if (isLoadingRegisterActivity) setLoading(true)
        if (isSuccessRegisterActivity) {
            Notistack.getNotistack('Consulta Correcta, 6to Paso Ok!', enqueueSnackbar, 'success')
            setTimeout(_ => {
                handleNextStep()
            }, 2500)
        }
        if (isErrorRegisterActivity) setLoading(false)
    }, [dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity])

    const handleContinueProcess = _ => {
        const sendData = additional.getDataForRegisterActivityOnAdditionalSummarySectionApi(stateAdditional.additionalClient)
        registerActivity(sendData)
    }

    return (
        <Grid
            container
            spacing={ 8 }
            className={ classes.root }>
                <Grid
                    id='summary-container'
                    item
                    xs={ 12 }
                    className={ classes.firework } />
                <Grid
                    item
                    xs={ 12 }
                    className={ classes.summaryWrapper }>
                        <Grid
                            container
                            spacing={ 8 }
                            className={ classNames(classes.summary, 'mb-4') }>
                                <Grid
                                    item
                                    xs={ 12 }>
                                        <Fade>
                                            <figure
                                                className={ classNames(classes.titleContainer, 'p-3') }>
                                                    <div
                                                        className={ classes.avatarTitleWrapper }>
                                                            <Bounce>
                                                                <Image
                                                                    className={ classes.avatarTitle }
                                                                    src={ LogoCencosud }
                                                                    minHeight={ 48 }
                                                                    alt ='Cencosud' />
                                                            </Bounce>
                                                    </div>
                                                    <div
                                                        className={ classes.titleWrapper }>
                                                            <Typography 
                                                                align='center' 
                                                                component='title' 
                                                                className={ classes.title }
                                                                variant='h6'>
                                                                ¡Generación de Tarjeta Adicional Exitosa!
                                                            </Typography>
                                                    </div>
                                                    <div
                                                        className={ classes.dateWrapper }>
                                                            <Typography
                                                                component='span'
                                                                className='mt-1 mr-2'>
                                                                    <CalendarTodayIcon
                                                                        fontSize='small'/>
                                                            </Typography>
                                                            <Typography
                                                                component='span'>
                                                                { Utils.getDateCurrent(1, false, true).replace('T', ' ') }
                                                            </Typography>
                                                    </div>
                                            </figure>
                                        </Fade>
                                </Grid>
                                <Grid
                                    item
                                    xs={ 12 }
                                    className='mb-2'>
                                        <Divider />
                                </Grid>
                                <Grid
                                    item
                                    xs={ 12 }
                                    className='py-3'>
                                        <div
                                            className={ classes.headerContainer }>
                                                <Typography
                                                    className='font-weight-bold mb-2'
                                                    color='textSecondary'>
                                                        { stateAdditional.additionalClient?.documentNumber }
                                                </Typography>
                                                <Typography
                                                    className='font-weight-bold'
                                                    color='textSecondary'>
                                                        { `${ stateAdditional.additionalClient?.firstLastName } ${ stateAdditional.additionalClient?.secondLastName } ` + 
                                                        `${ stateAdditional.additionalClient?.firstName } ${ stateAdditional.additionalClient?.secondName }` }
                                                </Typography>                                  
                                        </div>
                                </Grid>
                                <Grid
                                    item
                                    xs={ 12 }>
                                        <ul
                                            className={ classes.bodyContainer }>
                                                <ItemReprintSummary
                                                    title='Nro. Solicitud'
                                                    icon={
                                                        <LabelIcon
                                                            fontSize='small'/>
                                                    }
                                                    value={ stateAdditional.additionalClient?.completeSolicitudeCode }
                                                />
                                                <ItemReprintSummary
                                                    title='Nro. Tarjeta'
                                                    icon={
                                                        <CreditCardIcon
                                                            fontSize='small' />
                                                    }
                                                    value={ Utils.maskCreditCard(stateAdditional.additionalClient?.creditCard?.cardNumber || '') }
                                                />
                                                <ItemReprintSummary
                                                    title='Marca Tarjeta'
                                                    icon={
                                                        <CardMembershipIcon
                                                            fontSize='small' />
                                                    }
                                                    value={ stateAdditional.additionalClient?.creditCard?.brandDescription || '' }
                                                />
                                                <ItemReprintSummary
                                                    title='Tipo Tarjeta'
                                                    icon={
                                                        <LabelImportantIcon
                                                            fontSize='small' />
                                                    }
                                                    value={ stateAdditional.additionalClient?.creditCard?.productDescription || '' }
                                                />
                                                <ItemReprintSummary
                                                    title='Color Tarjeta'
                                                    icon={
                                                        <LensIcon
                                                            fontSize='small'/>
                                                    }
                                                    value={ 
                                                        stateAdditional.additionalClient?.creditCard?.colorAux
                                                            ? <LensIcon
                                                                style={{ color: stateAdditional.additionalClient?.creditCard?.colorAux }} />
                                                            : 'NO APLICA'
                                                    }
                                                />
                                        </ul>
                                </Grid>
                        </Grid>
                </Grid>
                <Grid
                    item
                    xs={ 12 }
                    className={ classes.buttonActions }>
                        <Grid
                            container
                            spacing={ 8 }>
                                <Grid
                                    item
                                    xs={ 12 }
                                    sm={ 12 }
                                    md={ 4 }
                                    lg={ 3 }
                                    style={{ margin: '0 auto' }}>
                                        <Tooltip
                                            TransitionComponent={ Zoom }
                                            title='Continuar proceso adicional.'>
                                                <div>
                                                    <Bounce>
                                                        <div
                                                            className={ classes.buttonProgressWrapper }>
                                                                <ActionButton
                                                                    text='Continuar'
                                                                    type='primary'
                                                                    loading={ loading }
                                                                    handleAction={ _ => handleContinueProcess() }
                                                                    icon={
                                                                        <SendIcon
                                                                            fontSize='small'
                                                                            className='ml-2' />
                                                                    }
                                                                    showLoading />
                                                        </div>
                                                    </Bounce>
                                                </div>
                                        </Tooltip>
                                </Grid>
                        </Grid>
                </Grid>
        </Grid>
    )
}

function mapStateToProps(state) {
    return {
        odcAdditional: state.odcAdditionalReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerActivity: bindActionCreators(ActionOdcAdditional.registerActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(CreditCardSummary))))