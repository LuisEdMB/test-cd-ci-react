﻿import React, { Component } from "react";
import classNames from "classnames";
import { withRouter } from "react-router-dom";
// Shorcurt
import Hotkeys from "react-hot-keys";
// Notistack
import { SnackbarProvider } from "notistack";
// Redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// Components
import VerifyEmail from "../../../components/VerifyEmail";
import {
  withStyles,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Button,
  Paper,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Grid,
  CircularProgress,
} from "@material-ui/core";
import Timer from "../../../components/Timer";
//Steps
import PreLoaderImage from "../../../components/PreLoaderImage";
import ConsultClient from "./components/consult-client/ConsultClient";
import ClientDetailPreEvaluated from "./components/client-detail-pre-evaluated/ClientDetailPreEvaluated";
import ClientRegister from "./components/client-register/ClientRegister";
import CommercialOffer from "./components/commercial-offer/CommercialOffer";
import ProductDetail from "./components/product-detail/ProductDetail";
import OfferSummary from "./components/offer-summary/OfferSummary";
import EmbossingCreditCard from "./components/embossing-creditcard/EmbossingCreditCard";
import ActivationCreditCard from "./components/activation-creditcard/ActivationCreditCard";
import BackgroundFinish from "../../../components/background-finish/BackgroundFinish";
import HistoryPanel from "./components/history-panel/HistoryPanel";
import NewCommentForm from "../../../components/NewCommentForm/NewCommentForm";
// Actions
import { getConstantODC } from "../../../actions/generic/constant";

import {
  firstRegularCall,
  continueProcess,
  registerClient,
  commercialOffer,
  offerSummary,
  pendingEmbossing,
} from "../../../actions/odc-regular/odc-regular";

import {
  verifySolicitudeIsBlocked,
  blockSolicitude,
  unlockSolicitude
} from '../../../actions/odc-express/odc';

import {
  cancelActivity,
  genericActivity,
  simpleGenericActivity,
} from "../../../actions/odc-regular/odc-activity";
import { biometricZytrustService } from "../../../actions/generic/zytrust";

import {
  createClient,
  updateClient,
  createRelationship,
  createAccount,
  blockTypeCreditCard,
  createCreditCard,
  createSAE,
} from "../../../actions/pmp/pmpRegular";
import * as ActionPci from '../../../actions/pci/pci'
import * as ActionOdcMaster from '../../../actions/odc-master/odc-master'
// Utils
import { SmoothScroll } from "../../../utils/Utils";
import * as Constants from '../../../utils/Constants'
import * as OriginationUtils from '../../../utils/origination/Origination'
import * as ExpressUtils from '../../../utils/origination/Express'
import * as MasterOrigination from '../../../utils/origination/MasterOrigination'
// Config
import { URL_BASE } from "../../../actions/config";
// Effects
import Fade from "react-reveal/Fade";
import { Redirect } from "react-router-dom";
// Icons
import SendIcon from "@material-ui/icons/Send";
// Colors
import { green } from "@material-ui/core/colors";

const styles = (theme) => ({
  root: {
    width: "100%",
    position: "relative",
  },
  stepLabelOk: {
    background: "#2196f31c",
    padding: ".5em",
    borderRadius: ".5em",
    boxShadow: "0px 1.2px 1px #007ab8",
  },
  stepLabelDefault: {
    background: "#f0f0f080",
    padding: ".5em",
    borderRadius: "0 0 .8em 0",
    borderBottom: "1px solid gray",
  },
  button: {
    textTransform: "none",
  },
  buttonProgressWrapper: {
    position: "relative",
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
    textTransform: "none",
  },
  IconButton: {
    top: -37,
    left: -37,
    position: "absolute",
  },
  icon: {
    fontSize: 32,
  },
  actionsContainer: {
    marginBottom: theme.spacing.unit * 2,
  },
  resetContainer: {
    padding: theme.spacing.unit * 3,
    textAlign: "center",
  },
  historyPanel: {
    position: "fixed",
    top: "33.3vh",
    zIndex: 900,
  },
  NewCommentForm: {
    position: "fixed",
    top: "20vh",
    right: "5px",
    zIndex: 900,
  },
  fontSize: {
    fontSize: 12,
  },
});

const mapStateToProps = (state) => {
  return {
    odcRegular: state.odcRegularReducer,
    constantODC: state.constantODCReducer,
    zytrust: state.zytrustReducer,
    contingency: state.contingencyReducer,
    biometric: state.biometricReducer,
    preEvaluatedClient: state.preEvaluatedClientReducer,
    embossingCreditCard: state.embossingCreditCardReducer,
    odcRegularActivity: state.odcRegularActivityReducer,
    pmp: state.pmpRegularReducer,
    verifyEmail: state.verifyEmailReducer,
    verifyCellphone: state.verifyCellphoneReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  const actions = {
    getConstantODC: bindActionCreators(getConstantODC, dispatch),

    cancelActivity: bindActionCreators(cancelActivity, dispatch),
    genericActivity: bindActionCreators(genericActivity, dispatch),
    simpleGenericActivity: bindActionCreators(simpleGenericActivity, dispatch),

    firstCall: bindActionCreators(firstRegularCall, dispatch),
    registerClient: bindActionCreators(registerClient, dispatch),
    offerSummary: bindActionCreators(offerSummary, dispatch),
    commercialOffer: bindActionCreators(commercialOffer, dispatch),
    pendingEmbossing: bindActionCreators(pendingEmbossing, dispatch),

    createClient: bindActionCreators(createClient, dispatch),
    updateClient: bindActionCreators(updateClient, dispatch),
    createRelationship: bindActionCreators(createRelationship, dispatch),
    createAccount: bindActionCreators(createAccount, dispatch),
    createCreditCard: bindActionCreators(createCreditCard, dispatch),
    createSAE: bindActionCreators(createSAE, dispatch),
    blockTypeCreditCard: bindActionCreators(blockTypeCreditCard, dispatch),

    biometricZytrustService: bindActionCreators(biometricZytrustService, dispatch),
    continueProcess: bindActionCreators(continueProcess, dispatch),

    verifySolicitudeIsBlocked: bindActionCreators(verifySolicitudeIsBlocked, dispatch),
    blockSolicitude: bindActionCreators(blockSolicitude, dispatch),
    unlockSolicitude: bindActionCreators(unlockSolicitude, dispatch),

    encryptCardnumber: bindActionCreators(ActionPci.encryptCardnumber, dispatch),
    decryptCardnumber: bindActionCreators(ActionPci.decryptCardnumber, dispatch),

    registerClientIntoMaster: bindActionCreators(ActionOdcMaster.registerUpdateClient, dispatch),
    registerAccountIntoMaster: bindActionCreators(ActionOdcMaster.registerAccount, dispatch),
    registerCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.registerCreditCard, dispatch),
    updateBlockingCodeCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.updateBlockingCodeCreditCard, dispatch)
  };
  return actions;
};

class RegularPage extends Component {
  state = {
    activeStep: 0,
    comodinPMP: "A",
    showHistory: false,
    showVerifyEmail: true,
    showVerifyCellphone: true,
    listComments: [],
    origination: {
      number: 0,
      activityNumber: 0,
      fullNumber: "",
      previousStatePhaseFlow: 0,
      nextStatePhaseFlow: 0,
      typeSolicitudeCode: 340001,
      typeSolicitudeOriginationCode: 390002,
      typeSolicitudeReqCode: 0,
      rePrintMotiveCode: 0
    },
    pmp: {
      clientNumber: "",
      relationshipNumber: "",
      accountNumber: "",
      cardNumber: "",
      parallelCardNumber: "",
      token: ''
    },
    client: {
      code: 0,
      id: 0,
      documentTypeErro: false,
      documentTypeId: "",
      documentTypeAux: "",
      documentTypeInternalValue: "",
      documentType: "",
      documentNumber: "",
      firstName: "",
      secondName: "",
      lastName: "",
      motherLastName: "",
      fullName: "",
      shortName: "",
      birthday: "",
      genderId: "",
      gender: "",
      genderInternalValue: "",
      nationalityId: "",
      nationality: "",
      maritalStatusId: "",
      maritalStatusInternalValue: "",
      maritalStatus: "",
      academicDegreeId: "",
      academicDegree: "",
      academicDegreeInternalValue: "",
      email: "",
      emailConfirmed: false,
      cellphone: "",
      cellphoneConfirmed: false,
      telephone: "",
      homeAddress: {
        departmentId: "",
        department: "",
        provinceId: "",
        province: "",
        districtId: "",
        district: "",
        viaId: "",
        via: "",
        viaInternalValue: "",
        nameVia: "",
        number: "",
        building: "",
        inside: "",
        mz: "",
        lot: "",
        zoneId: "",
        zone: "",
        zoneInternalValue: "",
        nameZone: "",
        typeResidence: "",
        housingTypeId: "",
        housingType: "",
        housingTypeInternalValue: "",
        reference: "",
      },
      workAddress: {
        departmentId: "",
        department: "",
        provinceId: "",
        province: "",
        districtId: "",
        district: "",
        viaId: "",
        via: "",
        viaInternalValue: "",
        nameVia: "",
        number: "",
        building: "",
        inside: "",
        mz: "",
        lot: "",
        zoneId: "",
        zone: "",
        zoneInternalValue: "",
        nameZone: "",
        typeResidence: "",
        housingTypeId: "",
        housingType: "",
        reference: "",
      },
      employmentSituationId: "",
      employmentSituation: "",
      jobTitleId: "",
      jobTitle: "",
      economicActivityId: "",
      economicActivity: "",
      laborIncomeDate: "",
      businessOption: "",
      businessName: "",
      ruc: "",
      grossIncome: 0,
      workTelephoneExtension: "",
      workTelephone: "",
      extra: {
        paymentDate: "",
        paymentDateInternalValue: "",
        paymentDateId: "",
        sendCorrespondence: "",
        sendCorrespondenceId: "",
        accountStatusId: "",
        accountStatus: "",
        effectiveWithdrawal: "",
        alert: "",
        sendCommonPromotion: "",
        media: "",
        mediaId: "",
        promoterId: "",
        promoter: "",
        term: "",
      },
      relationTypeId: 330001
    },
    creditCard: null,
    pct: {
      number: "",
    },
    segment: "",
    effectiveProvision: 0,
    lineAvailable: -1,
    lineSAE: -1,

    finalEffectiveProvision: 0,
    finalLineAvailable: -1,
    finalLineSAE: -1,
    finalMaxAmount: -1,
    pctSAE: "",
    isSAE: false,

    firstCallCda: null,
    secondCallCda: null,
    adn: null,
    siebel: null,
    fraudPrevention: null,
    cancel: false,
    redirectError: false,
    openModal: false,
    reintentRegisterUpdateClient: 0,
    reintentProcessPMP: 0,
    reintentBlockSolicitude: 0,
    reintentBlockTypeProcessPMP: 0,
    isBiometricOk: false,
    sae: null,
    disbursementTypeId: 0,
  };
  componentWillMount() {
    // Redirect Step and Set Data Client
    const dataUpdate = this.props.location
      ? this.props.location.state
        ? this.props.location.state.dataUpdate
        : null
      : null;
    if (dataUpdate) {
      this.setState((state) => ({
        ...state,
        activeStep: -1,
      }));
    }
  }
  componentDidMount() {
    this.props.getConstantODC();
    // Set Value Process
    const dataUpdate = this.props.location
      ? this.props.location.state
        ? this.props.location.state.dataUpdate
        : null
      : null;
    const documentNumber = this.props.location
      ? this.props.location.state
        ? this.props.location.state.documentNumber
        : null
      : null;
    if (dataUpdate) {
      let { cod_solicitud_completa } = dataUpdate;
      this.props.continueProcess(cod_solicitud_completa);
    }
    if (documentNumber) {
      this.setState((state) => ({
        ...state,
        client: {
          ...state.client,
          documentNumber: documentNumber,
        },
      }));
    }
  }
  componentDidUpdate = (prevProps) => {
    if (prevProps.constantODC !== this.props.constantODC) {
      // Credit Card Constant - Error Service
      if (
        !this.props.constantODC.loading &&
        this.props.constantODC.response &&
        this.props.constantODC.success
      ) {
        let { data } = this.props.constantODC;
        let comodinPMP = data.find(
          (item) => item.des_abv_constante === "COMODIN_PMP"
        ).valor_texto;
        this.setState((state) => ({
          ...state,
          comodinPMP,
        }));
      }
      // Credit Card Constant - Error Service
      else if (
        !this.props.constantODC.loading &&
        this.props.constantODC.response &&
        !this.props.constantODC.success
      ) {
        // this.getNotistack(`Constante: ${this.props.constantODC.error}`, "error");
      }
      // Credit Card Constant - Error Service Connectivity
      else if (
        !this.props.constantODC.loading &&
        !this.props.constantODC.response &&
        !this.props.constantODC.success
      ) {
        // this.getNotistack(`Constante: ${this.props.constantODC.error}`, "error");
      }
    }

    if (
      prevProps.verifyEmail.updateClientMailService !==
      this.props.verifyEmail.updateClientMailService
    ) {
      // Update Mail Service - Success Service
      if (
        !this.props.verifyEmail.updateClientMailService.loading &&
        this.props.verifyEmail.updateClientMailService.response &&
        this.props.verifyEmail.updateClientMailService.success
      ) {
        let {
          actualizarEmail,
        } = this.props.verifyEmail.updateClientMailService.data;
        this.setState({
          client: {
            ...this.state.client,
            email: actualizarEmail.des_to,
            emailConfirmed: true,
          },
          showVerifyEmail: false,
        });
      }
    }

    if (
      prevProps.verifyCellphone.updateClientSMSService !==
      this.props.verifyCellphone.updateClientSMSService
    ) {
      // Update Mail Service - Success Service
      if (
        !this.props.verifyCellphone.updateClientSMSService.loading &&
        this.props.verifyCellphone.updateClientSMSService.response &&
        this.props.verifyCellphone.updateClientSMSService.success
      ) {
        // let {
        //   actualizarEmail,
        // } = this.props.verifyCellphone.updateClientSMSService.data;
        // this.setState({
        //   client: {
        //     ...this.state.client,
        //     email: actualizarEmail.des_to,
        //     cellphoneConfirmed: true,
        //   },
        //   showVerifyCellphone: false,
        // });
      }
    }

    if (prevProps.odcRegular !== this.props.odcRegular) {
      if (
        prevProps.odcRegular.consultClient !==
        this.props.odcRegular.consultClient
      ) {
        if (
          !this.props.odcRegular.consultClient.loading &&
          this.props.odcRegular.consultClient.response &&
          this.props.odcRegular.consultClient.success
        ) {
          let {
            origination,
            client,
            firstCallCda,
            secondCallCda,
            siebel,
            fraudPrevention,
            adn,
            validations,
            sae,
          } = this.props.odcRegular.consultClient.data;
          if (origination && firstCallCda && secondCallCda) {
            this.setState((state) => ({
              ...state,
              origination: {
                ...state.origination,
                ...origination,
              },
              client: {
                ...state.client,
                ...client,
              },
              firstCallCda: {
                ...state.firstCallCda,
                ...firstCallCda,
              },
              secondCallCda: {
                ...state.secondCallCda,
                ...secondCallCda,
              },
              adn: {
                ...state.adn,
                ...adn,
              },
              siebel: {
                ...state.siebel,
                ...siebel,
              },
              fraudPrevention: {
                ...state.fraudPrevention,
                ...fraudPrevention,
              },
              validations: validations,
              sae: sae,
            }));
          }
        }
      }
      if (prevProps.odcRegular.process !== this.props.odcRegular.process) {
        if (
          !this.props.odcRegular.process.loading &&
          this.props.odcRegular.process.response &&
          this.props.odcRegular.process.success
        ) {
          let { data } = this.props.odcRegular.process;

          if (data) {
            let { list_ori_sel_solicitud_retomar_direccion } = data;
            if (list_ori_sel_solicitud_retomar_direccion.length > 0) {
              let homeAddress = list_ori_sel_solicitud_retomar_direccion.find(
                (item) => item.cod_tipo_direccion === 260001
              );
              let workAddress = list_ori_sel_solicitud_retomar_direccion.find(
                (item) => item.cod_tipo_direccion === 260002
              );
              let stepData = this.redirectStep(
                this.props.odcRegular.process.data.cod_flujo_fase_estado
              );
              const dataReintentProcessPmp = {
                status: data.cod_flujo_fase_estado || -1,
                errorMessage: data.des_error_message_servicio
              }
              this.setState((state) => ({
                ...state,
                activeStep: stepData.step,
                showVerifyEmail: !data.flg_confirmado,
                showVerifyCellphone: false, //!data.flg_confirmado,
                origination: {
                  ...state.origination,
                  number: data.cod_solicitud ? data.cod_solicitud : 0,
                  activityNumber: data.cod_solicitud_actividad
                    ? data.cod_solicitud_actividad
                    : 0,
                  fullNumber: data.cod_solicitud_completa
                    ? data.cod_solicitud_completa
                    : "",
                  previousStatePhaseFlow: data.cod_flujo_fase_estado
                    ? data.cod_flujo_fase_estado
                    : -1,
                },
                pmp: {
                  ...state.pmp,
                  clientNumber: data.num_cliente,
                  relationshipNumber: data.num_relacion,
                  accountNumber: data.num_cuenta,
                  cardNumber: data.num_tarjeta,
                  parallelCardNumber: data.num_tarjeta_sae,
                  token: data.token
                },
                client: {
                  ...state.client,
                  id: data.cod_cliente ? data.cod_cliente : 0,
                  documentTypeId:
                    data.cod_tipo_documento === 1 ||
                    data.cod_tipo_documento === 100001
                      ? 100001
                      : 100002,
                  documentType:
                    data.cod_tipo_documento === 1 ||
                    data.cod_tipo_documento === 100001
                      ? "DNI"
                      : "CE",
                  documentTypeAux:
                    data.cod_tipo_documento === 1 ||
                    data.cod_tipo_documento === 100001
                      ? "1"
                      : "2",
                  documentTypeInternalValue:
                    data.cod_tipo_documento === 1 ||
                    data.cod_tipo_documento === 100001
                      ? "DU"
                      : "CE",
                  documentNumber: data.des_nro_documento,
                  //firstName: data.des_primer_nom? data.des_primer_nom: "",
                  secondName: data.des_segundo_nom ? data.des_segundo_nom : "",
                  lastName: data.des_ape_paterno ? data.des_ape_paterno : "",
                  motherLastName: data.des_ape_materno
                    ? data.des_ape_materno
                    : "",
                  fullName: `${
                    data.des_ape_paterno ? data.des_ape_paterno : ""
                  } ${data.des_ape_materno ? data.des_ape_materno : ""} ${
                    data.des_primer_nom ? data.des_primer_nom : ""
                  } ${data.des_segundo_nom ? data.des_segundo_nom : ""}`,
                  shortName: `${
                    data.des_ape_paterno ? data.des_ape_paterno : ""
                  } ${data.des_primer_nom ? data.des_primer_nom : ""}`,

                  birthday: data.fec_nacimiento ? data.fec_nacimiento : "",
                  genderId: data.cod_genero ? parseInt(data.cod_genero) : 0,
                  gender: data.des_genero ? data.des_genero : "",
                  genderInternalValue: data.des_genero_valor_interno
                    ? data.des_genero_valor_interno
                    : "",

                  nationalityId: data.cod_nacionalidad
                    ? parseInt(data.cod_nacionalidad)
                    : 0,
                  nationality: data.des_nacionalidad
                    ? data.des_nacionalidad
                    : 0,
                  nationalityInternalValue: data.nationalityInternalValue
                    ? data.nationalityInternalValue
                    : "",

                  maritalStatusId: data.cod_estado_civil
                    ? parseInt(data.cod_estado_civil)
                    : 0,
                  maritalStatus: data.des_estado_civil
                    ? data.des_estado_civil
                    : "",
                  maritalStatusInternalValue: data.des_estado_civil_valor_interno
                    ? data.des_estado_civil_valor_interno
                    : "",

                  academicDegreeId: data.cod_gradoacademico
                    ? parseInt(data.cod_gradoacademico)
                    : "",
                  academicDegree: data.des_gradoacademico
                    ? data.des_gradoacademico
                    : "",
                  academicDegreeInternalValue: data.des_gradoacademico_valor_interno
                    ? data.des_gradoacademico_valor_interno
                    : "",

                  email: data.des_correo ? data.des_correo : "",
                  emailConfirmed: data.flg_confirmado,
                  cellphone: data.des_telef_celular
                    ? data.des_telef_celular
                    : "",
                  cellphoneConfirmed: false,
                  telephone: data.des_telef_fijo ? data.des_telef_fijo : "",

                  homeAddress: {
                    ...state.homeAddress,
                    departmentId: homeAddress.cod_ubi_departamento
                      ? homeAddress.cod_ubi_departamento
                      : "000000",
                    department: homeAddress.des_ubi_departamento
                      ? homeAddress.des_ubi_departamento
                      : "",
                    provinceId: homeAddress.cod_ubi_provincia
                      ? homeAddress.cod_ubi_provincia
                      : "000000",
                    province: homeAddress.des_ubi_provicia
                      ? homeAddress.des_ubi_provicia
                      : "",
                    districtId: homeAddress.cod_ubi_distrito
                      ? homeAddress.cod_ubi_distrito
                      : "000000",
                    district: homeAddress.des_ubi_distrito
                      ? homeAddress.des_ubi_distrito
                      : "",
                    viaId: homeAddress.cod_via ? homeAddress.cod_via : 0,
                    via: homeAddress.des_via ? homeAddress.des_via : "", // Here Value
                    viaInternalValue: homeAddress.des_via_valor_interno
                      ? homeAddress.des_via_valor_interno
                      : "",
                    nameVia: homeAddress.des_nombre_via
                      ? homeAddress.des_nombre_via
                      : "",
                    number: homeAddress.des_nro ? homeAddress.des_nro : "",
                    building: homeAddress.des_departamento
                      ? homeAddress.des_departamento
                      : "",
                    inside: homeAddress.des_interior
                      ? homeAddress.des_interior
                      : "",
                    mz: homeAddress.des_manzana ? homeAddress.des_manzana : "",
                    lot: homeAddress.des_lote ? homeAddress.des_lote : "",
                    zoneId: homeAddress.cod_zona ? homeAddress.cod_zona : "",
                    zone: homeAddress.des_zona
                      ? homeAddress.des_zona
                      : "Desconocido",
                    zoneInternalValue: homeAddress.des_zona_valor_interno
                      ? homeAddress.des_zona_valor_interno
                      : "",
                    nameZone: homeAddress.des_zona || "-",
                    housingTypeId: homeAddress.cod_tipo_vivienda
                      ? homeAddress.cod_tipo_vivienda
                      : 0,
                    housingType: homeAddress.des_tipo_vivienda
                      ? homeAddress.des_tipo_vivienda
                      : 0,
                    housingTypeInternalValue: homeAddress.des_tipo_vivienda_valor_interno
                      ? homeAddress.des_tipo_vivienda_valor_interno
                      : 0,
                    reference: homeAddress.des_referencia_domicilio
                      ? homeAddress.des_referencia_domicilio
                      : "",
                  },
                  workAddress: {
                    ...state.workAddress,
                    departmentId: workAddress.cod_ubi_departamento
                      ? workAddress.cod_ubi_departamento
                      : "000000",
                    department: workAddress.des_ubi_departamento
                      ? workAddress.des_ubi_departamento
                      : "",
                    provinceId: workAddress.cod_ubi_provincia
                      ? workAddress.cod_ubi_provincia
                      : "000000",
                    province: workAddress.des_ubi_provicia
                      ? workAddress.des_ubi_provicia
                      : "",
                    districtId: workAddress.cod_ubi_distrito
                      ? workAddress.cod_ubi_distrito
                      : "000000",
                    district: workAddress.des_ubi_distrito
                      ? workAddress.des_ubi_distrito
                      : "",
                    viaId: workAddress.cod_via ? workAddress.cod_via : 0,
                    via: workAddress.des_via ? workAddress.des_via : "", // Here Value
                    viaInternalValue: workAddress.des_via_valor_interno
                      ? workAddress.des_via_valor_interno
                      : "",
                    nameVia: workAddress.des_nombre_via
                      ? workAddress.des_nombre_via
                      : "",
                    number: workAddress.des_nro ? workAddress.des_nro : "",
                    building: workAddress.des_departamento
                      ? workAddress.des_departamento
                      : "",
                    inside: workAddress.des_interior
                      ? workAddress.des_interior
                      : "",
                    mz: workAddress.des_manzana ? workAddress.des_manzana : "",
                    lot: workAddress.des_lote ? workAddress.des_lote : "",
                    zoneId: workAddress.cod_zona ? workAddress.cod_zona : "",
                    zone: workAddress.des_zona
                      ? workAddress.des_zona
                      : "Desconocido",
                    zoneInternalValue: workAddress.des_zona_valor_interno
                      ? workAddress.des_zona_valor_interno
                      : "",
                    nameZone: workAddress.des_zona || "-",
                    housingTypeId: workAddress.cod_tipo_vivienda
                      ? workAddress.cod_tipo_vivienda
                      : 0,
                    housingType: workAddress.des_tipo_vivienda
                      ? workAddress.des_tipo_vivienda
                      : 0,
                    housingTypeInternalValue: workAddress.des_tipo_vivienda_valor_interno
                      ? workAddress.des_tipo_vivienda_valor_interno
                      : 0,
                    reference: workAddress.des_referencia_domicilio
                      ? workAddress.des_referencia_domicilio
                      : "",
                  },
                  employmentSituationId: data.cod_situacion_laboral
                    ? data.cod_situacion_laboral
                    : 0,
                  employmentSituation: data.des_situacion_laboral
                    ? data.des_situacion_laboral
                    : "",
                  employmentSituationInternalValue: data.des_situacion_laboral_valor_interno
                    ? data.des_situacion_laboral_valor_interno
                    : "",

                  jobTitleId: data.cod_cargo_profesion
                    ? data.cod_cargo_profesion
                    : 0,
                  jobTitle: data.des_cargo_profesion
                    ? data.des_cargo_profesion
                    : "Desconocido",
                  jobTitleInternalValue: data.des_cargo_profesion_valor_interno
                    ? data.des_cargo_profesion_valor_interno
                    : "",

                  economicActivityId: data.cod_actividad_economica
                    ? data.cod_actividad_economica
                    : 0,
                  economicActivity: data.des_actividad_economica
                    ? data.des_actividad_economica
                    : "Desconocido",
                  economicActivityInternalValue: data.des_actividad_economica_valor_interno
                    ? data.des_actividad_economica_valor_interno
                    : "",

                  laborIncomeDate: data.fec_ingreso_laboral
                    ? data.fec_ingreso_laboral
                    : "",
                  businessOption: "",
                  businessName: data.des_razon_social
                    ? data.des_razon_social
                    : "",
                  ruc: data.des_ruc ? data.des_ruc : "",
                  grossIncome: data.num_ingreso_bruto
                    ? data.num_ingreso_bruto
                    : 0,
                  workTelephoneExtension: data.des_anexo_laboral
                    ? data.des_anexo_laboral
                    : "",
                  workTelephone: data.des_telefono_laboral
                    ? data.des_telefono_laboral
                    : "",

                  extra: {
                    ...state.extra,
                    accountStatus: data.des_estado_cuenta
                      ? data.des_estado_cuenta
                      : "",
                    accountStatusId: data.cod_estado_cuenta
                      ? data.cod_estado_cuenta
                      : 0,
                    alert: data.flg_alerta_uso_tar ? "1" : "0",
                    effectiveWithdrawal: data.flg_retiro_efectivo ? "1" : "0",
                    media: data.des_envio_comunicacion
                      ? data.des_envio_comunicacion
                      : "",
                    mediaId: data.cod_envio_comunicacion,
                    paymentDate: data.des_fecha_pago ? data.des_fecha_pago : 0,
                    paymentDateId: data.cod_fecha_pago
                      ? data.cod_fecha_pago
                      : 0,
                    paymentDateInternalValue: data.des_fecha_pago_valor_interno
                      ? data.des_fecha_pago_valor_interno
                      : "1",
                    promoter: "",
                    promoterId: data.cod_resp_promotor
                      ? data.cod_resp_promotor
                      : 0,
                    sendCommonPromotion: data.flg_envio_comunicacion
                      ? "1"
                      : "0",
                    sendCorrespondence: data.des_envio_correspondencia
                      ? data.des_envio_correspondencia
                      : 0,
                    sendCorrespondenceId: data.cod_envio_correspondencia
                      ? data.cod_envio_correspondencia
                      : 0,
                    term: data.flg_autoriza_datos_per ? "1" : "0",
                  },
                },
                creditCard: {
                  ...state.creditCard,
                  name: data.des_nom_producto ? data.des_nom_producto : "",
                  productId: data.cod_producto ? data.cod_producto : 0,
                  colorId: data.cod_valor_color ? data.cod_valor_color : 0,
                  color: data.des_valor_color ? data.des_valor_color : "",
                  colorAux: data.des_valor_color_2
                    ? data.des_valor_color_2
                    : "",
                  brandId: data.cod_valor_marca ? data.cod_valor_marca : 0,
                  brand: data.des_valor_marca ? data.des_valor_marca : "",
                  cardNumber: data.num_tarjeta || '',
                  bin: data.cod_bin_pro ? data.cod_bin_pro : "",
                  type: data.des_tip_pro ? data.des_tip_pro : "",
                },
                pct: {
                  number: data.number_pct ? data.number_pct : "001",
                },
                segment: data.des_segmento ? data.des_segmento : "", // No Value in Database
                effectiveProvision: data.disp_efec_porcentaje
                  ? data.disp_efec_porcentaje
                  : 0,
                finalEffectiveProvision: data.disp_efec_porcentaje
                  ? data.disp_efec_porcentaje
                  : 0,
                lineAvailable: data.linea_credito_oferta
                  ? data.linea_credito_oferta
                  : 0,
                finalLineAvailable: data.linea_credito_final
                  ? data.linea_credito_final
                  : 0,
                reintentProcessPMP: ExpressUtils.setReintentProcessPMPRegular(dataReintentProcessPmp),
                reintentBlockTypeProcessPMP: ExpressUtils.setReintentProcessPMPBlockingRegular(dataReintentProcessPmp),
                reintentBlockSolicitude: ExpressUtils.setReintentBlockSolicitude(data.cod_flujo_fase_estado || -1),
                isBiometricOk: data.flg_biometria ? data.flg_biometria : false,

                isSAE: data.cod_solicitud_sae && data.cod_solicitud_sae !== 0,
                finalLineSAE: data.linea_sae_final ? data.linea_sae_final : 0,
                finalMaxAmount: data.monto_max_final ? data.monto_max_final : 0,
                pctSAE: data.pct_sae_final ? data.pct_sae_final : 0,
                disbursementTypeId: data.cod_tipo_desembolso
                  ? data.cod_tipo_desembolso
                  : 0,
                sae: {
                  lineSAE: data.linea_sae ? data.linea_sae : 0,
                  lineAvailable: data.linea_credito_oferta
                    ? data.linea_credito_oferta
                    : 0,
                  maxAmount: data.monto_max ? data.monto_max : 0,
                  pctSAE1: data.pct_sae_1 ? data.pct_sae_1 : "",
                  pctSAE2: data.pct_sae_2 ? data.pct_sae_2 : "",
                  baseDate: data.fecha_act ? data.fecha_act : "",
                  block: data.flg_bq2 ? data.flg_bq2 : "",
                },
                tcea: data.tcea ? data.tcea : "",
                numero_cuotas: data.numero_cuotas ? data.numero_cuotas : "",
                monto_cuota: data.monto_cuota ? data.monto_cuota : "",
              }));

              let sendData = {
                cod_solicitud: data.cod_solicitud ? data.cod_solicitud : 0,
                cod_solicitud_completa: data.cod_solicitud_completa
                  ? data.cod_solicitud_completa
                  : "",
                cod_flujo_fase_estado_actual: 21201,
                cod_flujo_fase_estado_anterior: 21201,
                nom_actividad: "Retomar Solicitud Originación",
              };

              this.props.simpleGenericActivity(sendData, null, false);

              SmoothScroll(stepData.step);
            } else {
              this.setState((state) => ({
                ...state,
                redirectError: true,
              }));
            }
          } else {
            this.setState((state) => ({
              ...state,
              redirectError: true,
            }));
          }
        } else if (
          !this.props.odcRegular.process.loading &&
          this.props.odcRegular.process.response &&
          !this.props.odcRegular.process.success
        ) {
          this.setState((state) => ({
            ...state,
            redirectError: true,
          }));
        } else if (
          !this.props.odcRegular.process.loading &&
          !this.props.odcRegular.process.response &&
          !this.props.odcRegular.process.success
        ) {
          this.setState((state) => ({
            ...state,
            redirectError: true,
          }));
        }
      }
    }
  };

  // Open Modal Cancel
  handleOpenModalCancelRegular = () => {
    this.setState((state) => ({
      ...state,
      openModal: true,
    }));
  };
  // Close Modal Cancel
  handleCloseModalCancelRegular = () => {
    this.setState((state) => ({
      ...state,
      openModal: false,
    }));
  };
  // Cancel Regular
  handleCancelRegular = () => {
    this.handleCloseModalCancelRegular();
    this.handleReset();
  };
  // Submit - Cancel Regular
  handleSubmitCancelRegular = (e) => {
    e.preventDefault();
    let { origination, activeStep } = this.state;
    let cancel = this.selectStatePhaseFlowCancel(activeStep);
    let sendData = {
      cod_solicitud: origination.number,
      cod_solicitud_completa: origination.fullNumber,
      cod_flujo_fase_estado_actual: cancel.currentStatePhaseFlow,
      cod_flujo_fase_estado_anterior: cancel.previousStatePhaseFlow,
      nom_actividad: cancel.activityName,
    };
    this.props.cancelActivity(sendData);
  };
  // Select State Phase Flow Cancel
  selectStatePhaseFlowCancel = (step) => {
    switch (step) {
      case 1:
        return {
          currentStatePhaseFlow: 20103,
          previousStatePhaseFlow: 20101,
          activityName: "Validación: Cancelado",
        };
      case 2:
        return {
          currentStatePhaseFlow: 20203,
          previousStatePhaseFlow: 20201,
          activityName: "Regular: Cancelado",
        };
      case 3:
        return {
          currentStatePhaseFlow: 20303,
          previousStatePhaseFlow: 20301,
          activityName: "Oferta: Cancelado",
        };
      case 4:
        return {
          currentStatePhaseFlow: 20403,
          previousStatePhaseFlow: 20401,
          activityName: "Preventa: Cancelado",
        };
      default:
        return {
          currentStatePhaseFlow: 0,
          previousStatePhaseFlow: 0,
          activityName: "",
        };
    }
  };
  // Redirect State
  redirectStep(status) {
    switch (status) {
      // Proccess Origination
      case 20501:
        return {
          step: 4,
        };
      // Create Client Pending
      case 20502:
        return {
          step: 4,
        };
      // Create Client Pending
      case 20503:
        return {
          step: 4,
        };
      // Create Relationship Pending
      case 20504:
        return {
          step: 4,
        };
      // Create Account Pending
      case 20505:
        return {
          step: 4,
        };
      // Create Credit Card Pending
      case 20506:
        return {
          step: 4,
        };
      // Offer Summary Complete
      case 20601:
        return {
          step: 5,
        };
      // Embossing CreditCard
      case 20701:
        return {
          step: 6,
        };
      case 20702:
        return {
          step: 6,
        };
      case 20703:
        return {
          step: 6,
        };
      // Activate CreditCard
      case 20801:
        return {
          step: 7,
        };
      case 20802:
        return {
          step: 7,
        };
      // Activate CreditCard - Auth
      case 20804:
        return {
          step: 7,
        };
      // Activate CreditCard
      case 20805:
        return {
          step: 7,
        };
      // Activate CreditCard
      case 20806:
        return {
          step: 7,
        };
      // Activate CreditCard
      case 20807:
        return {
          step: 7,
        };
      default:
        return {
          step: 0,
        };
    }
  }
  handleNext = () => {
    this.setState(
      (state) => ({
        activeStep: state.activeStep + 1,
      }),
      () => SmoothScroll(this.state.activeStep)
    );
  };
  handleBack = () => {
    this.setState(
      (state) => ({
        activeStep: state.activeStep - 1,
      }),
      () => SmoothScroll(this.state.activeStep)
    );
  };
  handleReset = () => {
    this.props.history.push("/odc/express/nueva-solicitud");
  };

  // Step 1 - Client Validation
  handleClientValidationForm = (data) => {
    let { client } = data;
    if (client) {
      //Send Data
      let sendData = {
        tipo_doc: client.documentTypeAux,
        nro_doc: client.documentNumber,
        tipo_doc_letra: client.documentType,
        cod_flujo_fase_estado_actual: 20101,
        cod_flujo_fase_estado_anterior: 20100,
        cod_solicitud: 0,
        cod_solicitud_completa: "",
        cod_cliente: 0,
        cod_tipo_solicitud: 340001,
        cod_tipo_relacion: 330001,
        cod_tipo_solicitud_requerimiento: 0,
        cod_tipo_solicitud_originacion: 390002,
        maritalStatus: client.maritalStatus,
        dateJoinedCompany: client.laborIncomeDate,
        birthDate: client.birthday,
        studiesLevel: client.academicDegree,
        occupation: client.jobTitle,
        laborSituation: client.employmentSituation,
        cod_etapa_maestro: Constants.MasterConfigurationStage.masterRegularStage
      };

      let sendDataExtra = {
        documentTypeId: client.documentTypeId,
        documentTypeInternalValue: client.documentTypeInternalValue,
      };
      // Send first Call
      this.props.firstCall(sendData, sendDataExtra);
      this.setState((state) => ({
        ...state,
        client: {
          ...state.client,
          ...data.client,
        },
      }));
    }
  };
  // Step 2 - Pre-Evaluated Client
  handlePreEvaluatedClient = (data) => {
    if (data) {
      let { origination } = this.state;
      // Send Data
      let sendData = {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 20102,
        cod_flujo_fase_estado_anterior: 20101,
        nom_actividad: "Validación: Aceptado",
      };
      this.setState((state) => ({
        ...state,
        client: {
          ...state.client,
          ...data.client,
        },
        lineAvailable: data.lineAvailable,
        effectiveProvision: data.effectiveProvision,
        lineSAE: data.lineSAE,
        maxAmount: data.maxAmount,
        segment: data.segment,
      }));
      this.props.genericActivity(sendData, null, true);
    }
  };
  // Step 3 - Client Register
  handleClientRegister = ({ data, reset = false }) => {
    if (data) {
      const { reintentRegisterUpdateClient } = this.state
      const steps = this.getStepsRegisterUpdateClient()
      steps[reset ? 0 : reintentRegisterUpdateClient].action(data)
    }
  }
  getStepsRegisterUpdateClient = _ => ({
    0: {
      action: async data => {
        const { client } = data
        const telephone = client.telephone
        const workTelephone = client.workTelephone

        const sendData = {
          tipo_doc: client.documentTypeAux,
          nro_doc: client.documentNumber,
          tipo_doc_letra: client.documentType.toUpperCase(),
          maritalStatus: client.maritalStatusInternalValue,
          dateJoinedCompany: client.laborIncomeDate.toUpperCase(),
          birthDate: client.birthday,
          employerRUC: client.ruc,
          nationality: client.nationality.toUpperCase(),
          studiesLevel: client.academicDegree.toUpperCase(),
          occupation: client.jobTitle
            ? client.jobTitle.toUpperCase()
            : "Desconocido",
          gender: client.gender.toUpperCase(),
          laborSituation: client.employmentSituation.toUpperCase(),
          cellPhone: `${client.cellphone}`,
          homePhone:
            telephone !== "" ? `${telephone}`.substring(0, 9) : `0510000000`,
          workPhone:
            workTelephone !== ""
              ? `${workTelephone}`.substring(0, 9)
              : `0510000000`,
          housingType: client.homeAddress.housingType.toUpperCase(),
          cod_cliente: this.state.client.id,
          cod_tipo_documento: client.documentTypeId,
          des_nro_documento: client.documentNumber,
          des_primer_nom: client.firstName,
          des_segundo_nom: client.secondName,
          des_ape_paterno: client.lastName,
          des_ape_materno: client.motherLastName,
          fec_nacimiento: client.birthday,
          cod_genero: client.genderId.toString(),
          cod_estado_civil: client.maritalStatusId.toString(),
          cod_nacionalidad: client.nationalityId.toString(),
          cod_pais_residencia: 230183,
          cod_gradoacademico: client.academicDegreeId.toString(),
          des_correo: client.email,
          des_telef_celular: client.cellphone,
          des_telef_fijo:
            telephone !== ""
              ? `${telephone}`.substring(0, 12).replace(/\s/g, "").trim()
              : `0510000000`,
          cod_tipo_cliente: 290002,
          cod_situacion_laboral: client.employmentSituationId,
          cod_cargo_profesion: client.jobTitleId ? client.jobTitleId : 0,
          cod_actividad_economica: client.economicActivityId
            ? client.economicActivityId
            : 0,
          fec_ingreso_laboral: client.laborIncomeDate,
          des_ruc: client.ruc,
          des_razon_social: client.businessName,
          num_ingreso_bruto: client.grossIncome,
          des_telefono_laboral:
            workTelephone !== ""
              ? `${workTelephone}`.substring(0, 12).replace(/\s/g, "").trim()
              : `0510000000`,
          des_anexo_laboral: client.workTelephoneExtension,
          flg_autoriza_datos_per:
            client.extra.term === "1" || client.extra.term ? true : false,
          cod_solicitud: this.state.origination.number,
          cod_solicitud_completa: this.state.origination.fullNumber,
          cod_fecha_pago: client.extra.paymentDateId,
          cod_envio_correspondencia: client.extra.sendCorrespondenceId
            ? client.extra.sendCorrespondenceId
            : 0,
          cod_estado_cuenta: client.extra.accountStatusId,
          flg_consumo_internet: client.extra.internetConsumeAdvice === "1",
          flg_consumo_extranjero: client.extra.foreignConsumeAdvice === "1",
          flg_retirar_efectivo: client.extra.effectiveDisposition === "1",
          flg_sobregirar_credito: client.extra.overflipping === "1",
          flg_notificacion_email: client.extra.mailAlerts === "1",
          num_monto_minimo: client.extra.minimumForAlerts ? client.extra.minimumForAlerts : 0,
          cod_resp_promotor: client.extra.promoterId,
          cod_resp_registro: 0,
          cod_resp_actualiza: 0,
          cod_flujo_fase_estado_actual: 20204,
          cod_flujo_fase_estado_anterior: 20201,
          cod_agencia: 0,
          cod_flujo_fase_estado: 0,
          cod_valor_color: 0,
          disp_efec_porcentaje: 0,
          disp_efec_monto: 0,
          cod_valor_marca: 0,
          linea_credito_oferta: 0,
          linea_credito_final: 0,
          cod_laboral: 0,
          cod_producto: 0,
          cod_motivo_emboce: 0,
          des_motivo_comentario_emboce: "",
          flg_biometria: false,
          cod_tipo_relacion: 330001,
          cod_tipo_solicitud: 340001,
          cod_tipo_solicitud_requerimiento: 0,
          cod_tipo_solicitud_originacion: 390002,
          des_segmento: this.state.segment,
          list_direccion: [
            {
              cod_direccion: client.homeAddress.id ? client.homeAddress.id : 0,
              cod_cliente: this.state.client.id,
              cod_tipo_direccion: 260001,
              des_tipo_direccion: "Dirección personal",
              cod_ubigeo: client.homeAddress.districtId,
              cod_via: client.homeAddress.viaId,
              des_nombre_via: client.homeAddress.nameVia,
              des_nro: client.homeAddress.number,
              des_departamento: client.homeAddress.building,
              des_interior: client.homeAddress.inside,
              des_manzana: client.homeAddress.mz,
              des_lote: client.homeAddress.lot,
              cod_zona: client.homeAddress.zoneId ? client.homeAddress.zoneId : 0,
              des_nombre_zona: client.homeAddress.nameZone,
              cod_tipo_residencia: 0,
              cod_tipo_vivienda: client.homeAddress.housingTypeId,
              des_referencia_domicilio: client.homeAddress.reference,
            },
            {
              cod_direccion: client.workAddress.id ? client.workAddress.id : 0,
              cod_cliente: this.state.client.id,
              cod_tipo_direccion: 260002,
              des_tipo_direccion: "Dirección laboral",
              cod_ubigeo: client.workAddress.districtId,
              cod_via: client.workAddress.viaId,
              des_nombre_via: client.workAddress.nameVia,
              des_nro: client.workAddress.number,
              des_departamento: client.workAddress.building,
              des_interior: client.workAddress.inside,
              des_manzana: client.workAddress.mz,
              des_lote: client.workAddress.lot,
              cod_zona: client.workAddress.zoneId ? client.workAddress.zoneId : 0,
              des_nombre_zona: client.workAddress.nameZone,
              cod_tipo_residencia: "",
              cod_tipo_vivienda: "",
              des_referencia_domicilio: client.workAddress.reference,
            }
          ]
        }
        const result = await this.props.registerClient(sendData).then(response => response)
        if (result?.data?.success) {
          this.setState(state => ({
            ...state,
            client: {
              ...state.client,
              ...data.client,
            },
            reintentRegisterUpdateClient: 1
          }), _ => this.handleClientRegister({ data }))
        }
      }
    },
    1: {
      action: async data => {
        const { origination } = this.state
        const sendData = {
          solicitudeCode: origination.number,
          completeSolicitudeCode: origination.fullNumber,
          activityName: MasterOrigination.registerUpdateClientActivity.activityName,
          phaseCode: MasterOrigination.registerUpdateClientActivity.phaseCode,
          masterStageCode: Constants.MasterConfigurationStage.masterRegularStage
        }
        const result = await this.props.registerClientIntoMaster(sendData).then(response => response)
        if (result?.success) {
          this.setState(state => ({
            ...state,
            reintentRegisterUpdateClient: 2
          }), _ => this.handleClientRegister({ data }))
        }
      }
    },
    2: {
      action: _ => {
        const { origination } = this.state
        const sendData = {
          cod_solicitud: origination.number,
          cod_solicitud_completa: origination.fullNumber,
          cod_flujo_fase_estado_actual: 20301,
          cod_flujo_fase_estado_anterior: 20204,
          nom_actividad: 'Oferta: En Proceso'
        }
        this.props.simpleGenericActivity(sendData, { retomar: true }, true)
      }
    }
  })
  // Step 4 - Commercial Offer
  handleCommercialOffer = (data) => {
    const { origination, client, lineAvailable, sae } = this.state;
    if (data) {
      let sendData = {};
      if (sae && Object.keys(sae).length > 0) {
        sendData = {
          tipo_doc: data.client.documentTypeId === 100001 ? "1" : "2",
          nro_doc: data.client.documentNumber,
          tipo_doc_letra: data.client.documentType,
          nom_actividad: "Oferta: Aceptado",
          cod_flujo_fase_estado_actual: 20302,
          cod_flujo_fase_estado_anterior: 20301,
          cod_valor_color: data.creditCard.colorId,
          disp_efec_porcentaje: data.finalEffectiveProvision,
          disp_efec_monto:
            (data.finalEffectiveProvision * data.finalLineAvailable) / 100,
          cod_valor_marca: data.creditCard.brandId,
          cod_producto: data.creditCard.productId,
          linea_credito_oferta: lineAvailable,
          linea_credito_final: data.finalLineAvailable,
          cod_solicitud: origination.number,
          cod_solicitud_completa: origination.fullNumber,
          cod_cliente: client.id,

          cod_agencia: 0,
          fec_act_base_sae: sae.baseDate ? sae.baseDate : "",
          flg_bq2: sae.block ? sae.block : "",
          linea_sae: sae.lineSAE ? sae.lineSAE : 0,
          linea_sae_final: data.finalLineSAE ? data.finalLineSAE : 0,
          monto_max: sae.maxAmount ? sae.maxAmount : 0,
          monto_max_final: data.finalMaxAmount ? data.finalMaxAmount : 0,
          pct_sae_1: sae.pctSAE1 ? sae.pctSAE1 : "",
          pct_sae_2: sae.pctSAE2 ? sae.pctSAE2 : "",
          pct_sae_final: data.pctSAE ? data.pctSAE : "",
          des_cci: data.CCI ? data.CCI : "",
          numero_cuotas: data.numberFees ? data.numberFees : "",
          monto_cuota: data.feeAmount ? data.feeAmount : "",
          tcea: data.tcea,
          cod_tipo_desembolso: data.disbursementTypeId
            ? data.disbursementTypeId
            : 0,
          des_descripcion: data.description ? data.description : "",

          monto_cci: data.transferAmount ? data.transferAmount : 0,
          monto_efectivo: data.effectiveAmount ? data.effectiveAmount : 0,
        };
      } else {
        sendData = {
          tipo_doc: data.client.documentTypeId === 100001 ? "1" : "2",
          nro_doc: data.client.documentNumber,
          tipo_doc_letra: data.client.documentType,
          nom_actividad: "Oferta: Aceptado",
          cod_flujo_fase_estado_actual: 20302,
          cod_flujo_fase_estado_anterior: 20301,
          cod_valor_color: data.creditCard.colorId,
          disp_efec_porcentaje: data.finalEffectiveProvision,
          disp_efec_monto:
            (data.finalEffectiveProvision * data.finalLineAvailable) / 100,
          cod_valor_marca: data.creditCard.brandId,
          cod_producto: data.creditCard.productId,
          linea_credito_oferta: lineAvailable,
          linea_credito_final: data.finalLineAvailable,
          cod_solicitud: origination.number,
          cod_solicitud_completa: origination.fullNumber,
          cod_cliente: client.id,

          cod_agencia: 0,
          fec_act_base_sae: "",
          flg_bq2: "",
          linea_sae: 0,
          linea_sae_final: 0,
          monto_max: 0,
          monto_max_final: 0,
          pct_sae_1: "",
          pct_sae_2: "",
          pct_sae_final: "",
          des_cci: "",
          cod_tipo_desembolso: 0,
          monto_cci: 0,
          monto_efectivo: 0,
          numero_cuotas: "",
          monto_cuota: "",
          tcea: "",
        };
      }

      this.props.commercialOffer(sendData).then((response) => {
        if (response.data) {
          this.setState((state) => ({
            ...state,
            client: {
              ...state.client,
              ...data.client,
            },
            creditCard: {
              ...state.creditCard,
              ...data.creditCard,
            },
            finalEffectiveProvision: data.finalEffectiveProvision
              ? data.finalEffectiveProvision
              : 0,
            finalLineAvailable: data.finalLineAvailable
              ? data.finalLineAvailable
              : 0,
            finalLineSAE: data.finalLineSAE ? data.finalLineSAE : 0,
            finalMaxAmount: data.finalMaxAmount ? data.finalMaxAmount : 0,
            pctSAE: data.pctSAE ? data.pctSAE : "",
            isSAE: data.isSAE,
            numero_cuotas: data.numberFees ? data.numberFees : "",
            monto_cuota: data.feeAmount ? data.feeAmount : "",
            tcea: data.tcea,
            // segment: data.segment ? data.segment : "",
            disbursementTypeId: data.disbursementTypeId
              ? data.disbursementTypeId
              : 0,
          }));
        }
      });
    }
  };
  // Step 5 - Product Detail
  handleProductDetail = async data => {
    let { origination, reintentProcessPMP, reintentBlockSolicitude, client, creditCard, 
      finalLineAvailable, pmp, secondCallCda } = this.state
    let { pct } = data
    let organization = "641"
    if (reintentBlockSolicitude === 0) {
      const sendData = {
        solicitudeCode: origination.number,
        completeSolicitudeCode: origination.fullNumber,
        clientCode: client.id,
        documentTypeId: client.documentTypeAux,
        documentNumber: client.documentNumber,
        lineAvailable: finalLineAvailable,
        globalLineAvailable: secondCallCda?.solicitud?.salidaCDA?.ofertaFinal
      }
      this.props.blockSolicitude(sendData).then(response => {
        if (response?.success)
          this.setState(state => ({
            ...state,
            reintentBlockSolicitude: 1
          }), _ => this.handleProductDetail(data))
      })
    }
    if (reintentBlockSolicitude === 1) {
      if (reintentProcessPMP === 0) {
        const sendData = {
          cod_solicitud: origination.number,
          cod_solicitud_completa: origination.fullNumber,
          cod_flujo_fase_estado_actual: 20402,
          cod_flujo_fase_estado_anterior: 20401,
          nom_actividad: "Preventa: Aceptado"
        }
        this.props.genericActivity(sendData)
          .then(response => {
            if (response?.data?.success)
              this.setState(state => ({
                ...state,
                reintentProcessPMP: 1
              }), _ => this.handleProductDetail(data))
          })
      }
      if (reintentProcessPMP === 1) {
        const sendData = {
          organization: organization,
          cod_flujo_fase_estado_actual: 20502,
          cod_flujo_fase_estado_anterior: 20501,
          nom_actividad: "Originación Regular: PMP Crear Cliente",
          process: origination,
          client: client
        }
        const sendDataPmp = OriginationUtils.setDataCreateClientPMP(sendData)
        this.props.createClient(sendDataPmp)
          .then(response => {
            if (response?.data) {
              if (response.data.errorCode === "12") {
                this.setState(state => ({
                  ...state,
                  reintentProcessPMP: 1.5
                }), _ => this.handleProductDetail(data))
              } else if (response.data.success) {
                const { pmpCrearCliente } = response.data
                if (pmpCrearCliente) {
                  const { createCustomer } = pmpCrearCliente
                  this.setState(state => ({
                    ...state,
                    reintentProcessPMP: 2,
                    pmp: {
                      ...pmp,
                      clientNumber: createCustomer.customerNumber
                    }
                  }), _ => this.handleProductDetail(data))
                }
              }
            }
          })
      }
      if (reintentProcessPMP === 1.5) {
        const sendData = {
          organization: organization,
          cod_flujo_fase_estado_actual: 20503,
          cod_flujo_fase_estado_anterior: 20501,
          nom_actividad: "Originación Regular: PMP Editar Cliente",
          process: origination,
          client: client
        }
        const sendDataPmp = OriginationUtils.setDataUpdateClientePMP(sendData)
        this.props.updateClient(sendDataPmp)
          .then(response => {
            if (response?.data?.success) {
              const { pmpActualizarCliente } = response.data
              if (pmpActualizarCliente) {
                const { updateCustomer } = pmpActualizarCliente
                this.setState(state => ({
                  ...state,
                  reintentProcessPMP: 2,
                  pmp: {
                    ...pmp,
                    customerUpdated: true,
                    clientNumber: updateCustomer.customerNumber
                  }
                }), _ => this.handleProductDetail(data))
              }
            }
          })
      }
      if (reintentProcessPMP === 2) {
        const { customerUpdated } = pmp
        const sendData = {
          organization: organization,
          cod_flujo_fase_estado_actual: 20504,
          cod_flujo_fase_estado_anterior: customerUpdated ? 20503 : 20502,
          nom_actividad: "Originación Regular: PMP Crear Relación",
          process: origination,
          client: client,
          pmp: pmp,
          lineAvailable: finalLineAvailable
        }
        const sendDataPmp = OriginationUtils.setDataCreateRelationshipPMP(sendData)
        this.props.createRelationship(sendDataPmp)
          .then(response => {
            if (response?.data?.success) {
              const { pmpCrearRelacion } = response.data
              const { createCustomerRelationship } = pmpCrearRelacion
              this.setState(state => ({
                ...state,
                reintentProcessPMP: 3,
                pmp: {
                  ...state.pmp,
                  relationshipNumber: createCustomerRelationship.relationshipNumber
                }
              }), _ => this.handleProductDetail(data))
            }
          })
      }
      if (reintentProcessPMP === 3) {
        const sendData = {
          organization: organization,
          cod_flujo_fase_estado_actual: 20505,
          cod_flujo_fase_estado_anterior: 20504,
          nom_actividad: "Originación Regular: PMP Crear Cuenta",
          process: origination,
          client: client,
          creditCard: creditCard,
          pmp: pmp,
          pct: pct,
          lineAvailable: finalLineAvailable
        }
        const sendDataPmp = OriginationUtils.setDataCreateAccountPMP(sendData)
        this.props.createAccount(sendDataPmp)
          .then(response => {
            if (response?.data?.success) {
              const { pmpCrearCuenta } = response.data
              const { createCustomerAccount } = pmpCrearCuenta
              this.setState(state => ({
                ...state,
                reintentProcessPMP: 4,
                pmp: {
                  ...state.pmp,
                  accountNumber: createCustomerAccount.accountNumber
                }
              }), _ => this.handleProductDetail(data))
            }
          })
      }
      // Master - Create Account
      if (reintentProcessPMP === 4) {
        const sendData = {
            solicitudeCode: origination.number,
            completeSolicitudeCode: origination.fullNumber,
            activityName: MasterOrigination.registerAccountActivity.activityName,
            phaseCode: MasterOrigination.registerAccountActivity.phaseCode,
            masterStageCode: Constants.MasterConfigurationStage.masterRegularStage
        }
        const result = await this.props.registerAccountIntoMaster(sendData).then(response => response)
        if (result?.success) {
            this.setState(state => ({
                ...state,
                reintentProcessPMP: 5
            }), _ => this.handleProductDetail(data))
        }
      }
      if (reintentProcessPMP === 5) {
        const sendData = {
          organization: organization,
          cod_flujo_fase_estado_actual: 20506,
          cod_flujo_fase_estado_anterior: 20505,
          nom_actividad: "Originación Regular: PMP Crear Tarjeta",
          process: origination,
          client: client,
          creditCard: creditCard,
          pmp: pmp,
          pct: pct,
          lineAvailable: finalLineAvailable
        }
        const sendDataPmp = OriginationUtils.setDataCreateCreditCard(sendData)
        this.props.createCreditCard(sendDataPmp)
          .then(response => {
            if (response?.data?.success) {
              const { pmpCrearTarjetaCredito } = response.data
              const { createCreditCard } = pmpCrearTarjetaCredito
              this.setState(state => ({
                ...state,
                reintentProcessPMP: 6,
                pmp: {
                  ...state.pmp,
                  cardNumber: createCreditCard.cardNumber
                },
                creditCard: {
                  ...state.creditCard,
                  cardNumber: createCreditCard.cardNumber
                }
              }), _ => this.handleProductDetail(data))
            }
          })
      }
      // Encrypting Cardnumber
      if (reintentProcessPMP === 6) {
        const sendDataPci = {
          solicitudeCode: origination.number,
          completeSolicitudeCode: origination.fullNumber,
          pciStage: Constants.PciConfigurationStage.pciRegularStage
        }
        this.props.encryptCardnumber(sendDataPci)
          .then(response => {
            if (response?.success || response?.errorCode === Constants.CustomErrorCode.errorCodeCreditCardAlreadyPci) {
              this.setState(state => ({
                ...state,
                reintentProcessPMP: 7,
                pmp: {
                    ...state.pmp,
                    token: response.token
                }
              }), _ => this.handleProductDetail(data))
            }
          })
      }
      // Master - Create Credit Card
      if (reintentProcessPMP === 7) {
        const sendData = {
            solicitudeCode: origination.number,
            completeSolicitudeCode: origination.fullNumber,
            activityName: MasterOrigination.registerCreditCardActivity.activityName,
            phaseCode: MasterOrigination.registerCreditCardActivity.phaseCode,
            masterStageCode: Constants.MasterConfigurationStage.masterRegularStage
        }
        const result = await this.props.registerCreditCardIntoMaster(sendData).then(response => response)
        if (result?.success) {
            this.setState(state => ({
                ...state,
                reintentProcessPMP: 8
            }), _ => this.handleProductDetail(data))
        }
      }
      if (reintentProcessPMP === 8) {
        const sendData = {
          cod_solicitud: origination.number,
          cod_solicitud_completa: origination.fullNumber,
          cod_flujo_fase_estado_actual: 20507,
          cod_flujo_fase_estado_anterior: this.state.isSAE ? 20520 : 20506,
          nom_actividad: "Originación Regular: Aceptado"
        }
        this.props.genericActivity(sendData, null, true).then(response => {
          if (response?.data?.success)
            this.setState(state => ({
              ...state,
              reintentBlockSolicitude: 2
            }), _ => this.handleProductDetail(data))
        })
      }
    }
    if (reintentBlockSolicitude === 2) {
      const sendData = {
        solicitudeCode: origination.number,
        completeSolicitudeCode: origination.fullNumber,
        clientCode: client.id,
        documentTypeId: client.documentTypeAux,
        documentNumber: client.documentNumber
      }
      this.props.unlockSolicitude(sendData)
    }
  };
  // Step 6 - Offer Summary
  handleOfferSummary = _ => {
    let {
      origination,
      creditCard,
      lineAvailable,
      finalLineAvailable,
      finalEffectiveProvision,
    } = this.state;
    if (
      origination &&
      creditCard &&
      lineAvailable >= 0 &&
      finalLineAvailable >= 0 &&
      finalEffectiveProvision >= 0
    ) {
      let sendData = {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 20602,
        cod_flujo_fase_estado_anterior: 20601,
        nom_actividad: "Resumen: Aceptado",
        cod_valor_color: creditCard.colorId,
        disp_efec_porcentaje: finalEffectiveProvision,
        cod_valor_marca: creditCard.brandId,
        linea_credito_oferta: lineAvailable,
        linea_credito_final: finalLineAvailable,
      };
      this.props.offerSummary(sendData);
    }
  };
  // Step 7 - Embossing CreditCard
  handleEmbossingCreditCard = _ => {
    let { origination } = this.state;
    let previousStatePhaseFlow = 20701;
    if (origination.previousStatePhaseFlow === 20702) {
      previousStatePhaseFlow = 20702;
    }
    // Send Data
    let sendData = {
      cod_requerimiento: origination.requestNumber,
      cod_solicitud: origination.number,
      cod_solicitud_completa: origination.fullNumber,
      cod_flujo_fase_estado_actual: 20703,
      cod_flujo_fase_estado_anterior: previousStatePhaseFlow,
      nom_actividad: "Embozado: Aceptado",
    };
    this.props.genericActivity(sendData, null, true);
  };
  // Step 7 - Pending Embossing CreditCard
  handlePendingEmbossingCreditCard = (data) => {
    let { origination } = this.state;
    let { reason } = data;
    if (reason) {
      // Send Data
      let sendData = {
        cod_requerimiento: origination.requestNumber,
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 20702,
        cod_flujo_fase_estado_anterior: 20701,
        cod_motivo_emboce: reason.id,
        des_motivo_comentario_emboce: reason.description,
        nom_actividad: "Embozado: Pendiente",
      };
      this.props.pendingEmbossing(sendData);
    }
  };
  // Step 8 - Activation Credit Card
  handleActivationCreditCard = async _ => {
    const { origination, client, pmp, creditCard, comodinPMP, reintentBlockTypeProcessPMP } = this.state
    const organization = '641'
    if (reintentBlockTypeProcessPMP === 0) {
      const sendData = {
          solicitudeCode: origination.number,
          completeSolicitudeCode: origination.fullNumber,
          token: pmp.token,
          masterStageCode: Constants.MasterConfigurationStage.masterRegularStage
      }
      const result = await this.props.decryptCardnumber(sendData).then(response => response)
      if (result?.success)
          this.setState(state => ({
              ...state,
              reintentBlockTypeProcessPMP: 1,
              pmp: {
                  ...state.pmp,
                  cardNumber: result?.nro_tarjeta || pmp.cardNumber
              }
          }), _ => this.handleActivationCreditCard())
    }
    if (reintentBlockTypeProcessPMP === 1) {
      let previousStatePhaseFlow = 20802
      if (origination.previousStatePhaseFlow === 20804) {
        previousStatePhaseFlow = 20804
      }
      const sendData = {
        organization: organization,
        cod_flujo_fase_estado_actual: 20805,
        cod_flujo_fase_estado_anterior: previousStatePhaseFlow,
        nom_actividad: "Activación Tarjeta: PMP Activar TC",
        blockCode: comodinPMP,
        process: origination,
        client: client,
        pmp: pmp,
        creditCard: creditCard,
        isSAE: false
      }
      const sendDataPMP = OriginationUtils.setDataBlockTypeCreditCardPMP(sendData)
      this.props.blockTypeCreditCard(sendDataPMP, { isSAE: false }).then((response) => {
        if (response?.data?.success) {
          this.setState(state => ({
            ...state,
            reintentBlockTypeProcessPMP: 2
        }), _ => this.handleActivationCreditCard())
        }
      })
    }
    if (reintentBlockTypeProcessPMP === 2) {
      const sendData = {
        solicitudeCode: origination.number,
        completeSolicitudeCode: origination.fullNumber,
        accountNumber: pmp.accountNumber,
        token: pmp.token,
        blockingCode: comodinPMP,
        activityName: MasterOrigination.updateBlockingCodeCreditCardActivity.activityName,
        phaseCode: MasterOrigination.updateBlockingCodeCreditCardActivity.phaseCode,
        masterStageCode: Constants.MasterConfigurationStage.masterRegularStage
      }
      const result = await this.props.updateBlockingCodeCreditCardIntoMaster(sendData).then(response => response)
      if (result?.success) {
        this.setState(state => ({
          ...state,
          reintentBlockTypeProcessPMP: 3
        }), _ => this.handleActivationCreditCard())
      }
    }
    if (reintentBlockTypeProcessPMP === 3) {
      const sendData = {
        cod_solicitud: origination.number,
        cod_solicitud_completa: origination.fullNumber,
        cod_flujo_fase_estado_actual: 20806,
        cod_flujo_fase_estado_anterior: 20805,
        nom_actividad: 'Activación Tarjeta: Aceptado'
      }
      this.props.genericActivity(sendData, { finishProcess: false }, false).then(response => {
          if (response?.data?.success) {
            const sendData = {
              cod_solicitud: origination.number,
              cod_solicitud_completa: origination.fullNumber,
              cod_flujo_fase_estado_actual: 20902,
              cod_flujo_fase_estado_anterior: 20901,
              nom_actividad: 'Entrega Tarjeta: Aceptado'
            }
            this.props.genericActivity(sendData,{ finishProcess: true }, false)
          }
        }
      )
    }
  };
  // Step 8 - Pending Credit Card
  handlePendingActivationCredit = _ => {
    let { origination } = this.state;
    // Send Data
    let sendData = {
      cod_requerimiento: origination.requestNumber,
      cod_solicitud: origination.number,
      cod_solicitud_completa: origination.fullNumber,
      cod_flujo_fase_estado_actual: 20803,
      cod_flujo_fase_estado_anterior: 20802,
      nom_actividad: "Activación Tarjeta: Pendiente",
    };
    this.props.simpleGenericActivity(
      sendData,
      { finishProcess: false, pendingTC: true },
      true
    );
  };
  // Biometric Zytrust Service
  handleBiometricZytrustService = _ => {
    let { client } = this.state;
    const sendData = {
      tiDocCliente: client.documentTypeAux,
      nuDocCliente: client.documentNumber,
    };
    this.props.biometricZytrustService(sendData);
  };
  // Set Data Block Type Credit Card PMP
  setDataBlockTypeCreditCardPMP = (data) => {
    let { client, pmp, process, isSAE = false } = data;
    return {
      tipo_doc_letra: client.documentType,
      nom_actividad: data.nom_actividad,
      cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
      cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
      cod_solicitud: process.number,
      cod_solicitud_completa: process.fullNumber,
      cod_cliente: null,
      cod_cliente_titular: client ? client.id : 0,
      terceraLlamadaPmpTipoBloqueTarjeta: {
        tipo_doc: client.documentTypeAux,
        nro_doc: client.documentNumber,
        organization: data.organization,
        identityDocumentType: client.documentTypeInternalValue,
        identityDocumentNumber: client.documentNumber,
        isoMessageTypeActivar: "0",
        transactionNumberActivar: "0",
        cardNumberActivar: isSAE ? pmp.parallelCardNumber : pmp.cardNumber,
        cardSequenceActivar: "0001",
        amedBlockCodeActivar: data.blockCode,
        amedCardActionActivar: "0",
        amedRqtdCardTypeActivar: "0",
        amedCurrFirstUsageFlagActivar: "",
        amedPriorFirstUsageFlagActivar: "",
        amedMotiveBlockadeActivar: "01",
        internacionUsaRegionActivar: "",
        boletineoInterUsaDateActivar: "0",
        interCanRegionActivar: "",
        boletineoInterCanDateActivar: "0",
        internacionalCAmerRegionActivar: "",
        boletineoInterCAmerRegionActivar: "0",
        interAsiaRegionActivar: "",
        bolitineoInterAsiaDateActivar: "0",
        interRegionEuropaActivar: "",
        bolitineoInterDateEuropaActivar: "0",
        interEuropaRegionActivar: "",
        bolitineoInterEuropaDateActivar: "0",
        principalAccountActivar: "",
        documentTypeActivar: "",
        documentNumerActivar: "",
        clientNameActivar: "",
        clientAddressActivar: "",
        birthDateActivar: "",
        telephoneActivar: "0",
        cardNameActivar: "",
        cardAddressActivar: "",
        cardTypeActivar: "",
        applicantNameActivar: "",
        applicantDocumentActivar: "",
        applicantTelephoneActivar: "0",
        relationshipActivar: "",
        blockadeUserActivar: "",
        blockadeNumberActivar: "0",
        blockadeDateActivar: "0",
        blockadeTimeActivar: "0",
        userInitialsActivar: "",
        emisionTypeActivar: "",
      },
    };
  };
  getSteps = () => {
    return [
      "Originación Regular - Consulta cliente",
      "Validación Cliente",
      "Registro del Cliente",
      "Oferta Comercial",
      "Detalle Producto",
      "Resumen Oferta",
      "Emboce de Tarjeta",
      "Activación de Tarjeta"
    ];
  };
  getStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <div>
            <SnackbarProvider maxSnack={3}>
              <ConsultClient
                client={this.state.client}
                // State Redux
                consultClient={this.props.odcRegular.consultClient}
                // Function
                handleReset={this.handleReset}
                handleNext={this.handleNext}
                handleClientValidationForm={this.handleClientValidationForm}
              />
            </SnackbarProvider>
          </div>
        );
      case 1:
        return (
          <div>
            <SnackbarProvider maxSnack={3}>
              <ClientDetailPreEvaluated
                // State Redux
                cancelActivity={this.props.odcRegularActivity.cancelActivity}
                genericActivity={this.props.odcRegularActivity.genericActivity}
                // Data
                origination={this.state.origination}
                client={this.state.client}
                firstCallCda={this.state.firstCallCda}
                secondCallCda={this.state.secondCallCda}
                adn={this.state.adn}
                siebel={this.state.siebel}
                fraudPrevention={this.state.fraudPrevention}
                validations={this.state.validations}
                sae={this.state.sae}
                history={this.props.history}
                constantODC={ this.props.constantODC }
                // Function
                handlePreEvaluatedClient={this.handlePreEvaluatedClient}
                handleOpenModalCancelRegular={this.handleOpenModalCancelRegular}
                handleCancelRegular={this.handleCancelRegular}
                handleReset={this.handleReset}
                handleNext={this.handleNext}
              />
            </SnackbarProvider>
          </div>
        );
      case 2:
        return (
          <div>
            <SnackbarProvider maxSnack={6}>
              <ClientRegister
                // State Redux
                cancelActivity={this.props.odcRegularActivity.cancelActivity}
                simpleGenericActivity={
                  this.props.odcRegularActivity.simpleGenericActivity
                }
                registerClient={this.props.odcRegular.registerClient}
                // Data
                client={this.state.client}
                origination={this.state.origination}
                handleClientRegister={this.handleClientRegister}
                // Function
                handleOpenModalCancelRegular={this.handleOpenModalCancelRegular}
                handleCancelRegular={this.handleCancelRegular}
                handleReset={this.handleReset}
                handleNext={this.handleNext}
              />
            </SnackbarProvider>
          </div>
        );
      case 3:
        return (
          <div>
            <SnackbarProvider maxSnack={3}>
              <CommercialOffer
                // State Redux
                cancelActivity={this.props.odcRegularActivity.cancelActivity}
                // Data
                secondCallCda={this.state.secondCallCda}
                client={this.state.client}
                sae={this.state.sae}
                commercialOffer={this.props.odcRegular.commercialOffer}
                handleCommercialOffer={this.handleCommercialOffer}
                // Function
                handleOpenModalCancelRegular={this.handleOpenModalCancelRegular}
                handleCancelRegular={this.handleCancelRegular}
                handleReset={this.handleReset}
                handleNext={this.handleNext}
              />
            </SnackbarProvider>
          </div>
        );
      case 4:
        return (
          <div>
            <SnackbarProvider maxSnack={3}>
              <ProductDetail
                // State Redux
                cancelActivity={this.props.odcRegularActivity.cancelActivity}
                genericActivity={this.props.odcRegularActivity.genericActivity}
                createClient={this.props.pmp.createClient}
                updateClient={this.props.pmp.updateClient}
                createRelationship={this.props.pmp.createRelationship}
                createAccount={this.props.pmp.createAccount}
                createCreditCard={this.props.pmp.createCreditCard}
                createSAE={this.props.pmp.createSAE}
                blockTypeCreditCard={this.props.pmp.blockTypeCreditCard}
                // Data
                pmp={this.state.pmp}
                secondCallCda={this.state.secondCallCda}
                segment={this.state.segment}
                numero_cuotas={this.state.numero_cuotas}
                monto_cuota={this.state.monto_cuota}
                tcea={this.state.tcea}
                client={this.state.client}
                origination={ this.state.origination }
                creditCard={this.state.creditCard}
                lineAvailable={this.state.finalLineAvailable}
                effectiveProvision={this.state.finalEffectiveProvision}
                lineSAE={this.state.finalLineSAE}
                maxAmount={this.state.finalMaxAmount}
                pctSAE={this.state.pctSAE}
                isSAE={this.state.isSAE}
                // Function
                handleProductDetail={this.handleProductDetail}
                handleOpenModalCancelRegular={this.handleOpenModalCancelRegular}
                handleCancelRegular={this.handleCancelRegular}
                handleReset={this.handleReset}
                handleNext={this.handleNext}
              />
            </SnackbarProvider>
          </div>
        );
      case 5:
        return (
          <div>
            <SnackbarProvider maxSnack={3}>
              <OfferSummary
                // State Redux
                offerSummary={this.props.odcRegular.offerSummary}
                // Data
                origination={this.state.origination}
                client={this.state.client}
                creditCard={this.state.creditCard}
                lineAvailable={this.state.finalLineAvailable}
                effectiveProvision={this.state.finalEffectiveProvision}
                isSAE={this.state.isSAE}
                lineSAE={this.state.finalLineSAE}
                maxAmount={this.state.finalMaxAmount}
                tcea={this.state.tcea}
                // Function
                handleOfferSummary={this.handleOfferSummary}
                handleNext={this.handleNext}
              />
            </SnackbarProvider>
          </div>
        );
      case 6:
        return (
          <div>
            <SnackbarProvider maxSnack={3}>
              <EmbossingCreditCard
                // State Redux
                pendingEmbossing={this.props.odcRegular.pendingEmbossing}
                genericActivity={this.props.odcRegularActivity.genericActivity}
                // Data
                creditCard={this.state.creditCard}
                client={this.state.client}
                // Function
                handleEmbossingCreditCard={this.handleEmbossingCreditCard}
                handlePendingEmbossingCreditCard={
                  this.handlePendingEmbossingCreditCard
                }
                handleNext={this.handleNext}
                handleReset={this.handleReset}
              />
            </SnackbarProvider>
          </div>
        );
      case 7:
        return (
          <div>
            <SnackbarProvider maxSnack={3}>
              <ActivationCreditCard
                // State Redux
                simpleGenericActivity={
                  this.props.odcRegularActivity.simpleGenericActivity
                }
                genericActivity={this.props.odcRegularActivity.genericActivity}
                blockTypeCreditCard={this.props.pmp.blockTypeCreditCard}
                responseBioZytrustService={
                  this.props.zytrust.responseBioZytrustService
                }
                // Data
                origination={this.state.origination}
                client={this.state.client}
                isBiometricOk={this.state.isBiometricOk}
                // Function
                handleBiometricZytrustService={
                  this.handleBiometricZytrustService
                }
                handleActivationCreditCard={this.handleActivationCreditCard}
                handlePendingActivationCredit={
                  this.handlePendingActivationCredit
                }
                handleNext={this.handleNext}
                handleReset={this.handleReset}
                disbursementTypeId={this.state.disbursementTypeId}
              />
            </SnackbarProvider>
          </div>
        );
      default:
        return (
          <div>
            <PreLoaderImage />
          </div>
        );
    }
  };

  handleIconButton = (name) => (e) => {
    let showName = `show${name}`;
    this.setState((state) => ({
      ...state,
      [showName]: !state[showName],
    }));
  };

  render() {
    const { classes, odcRegularActivity } = this.props;
    const steps = this.getSteps();
    const { activeStep, redirectError } = this.state;
    if (redirectError) {
      return <Redirect to={{ pathname: `/${URL_BASE}/error` }} />;
    }
    return (
      <Hotkeys
        keyName="shift+i, ctrl+i"
        onKeyUp={this.handleIconButton("History")}
      >
        <div className={classNames(classes.root)}>
          {activeStep > 6 && (
            <SnackbarProvider maxSnack={3}>
              <Timer />
            </SnackbarProvider>
          )}
          {activeStep >= 6 && activeStep <= 7 && (
            <SnackbarProvider maxSnack={3}>
              <VerifyEmail
                client={this.state.client}
                origination={this.state.origination}
                onClick={this.handleIconButton("VerifyEmail")}
                showVerifyEmail={this.state.showVerifyEmail}
              />
            </SnackbarProvider>
          )}
          <HistoryPanel
            onClick={this.handleIconButton("History")}
            showHistory={this.state.showHistory}
            effectiveProvision={this.state.effectiveProvision}
            finalEffectiveProvision={this.state.finalEffectiveProvision}
            lineAvailable={this.state.lineAvailable}
            finalLineAvailable={this.state.finalLineAvailable}
            creditCard={this.state.creditCard}
            client={this.state.client}
            origination={this.state.origination}
            numero_cuotas={this.state.numero_cuotas}
            monto_cuota={this.state.monto_cuota}
            tcea={this.state.tcea}
            lineSAE={this.state.finalLineSAE}
          />
          <div className={classes.NewCommentForm}>
            <SnackbarProvider maxSnack={3}>
              <NewCommentForm
                fixed={true}
                codSolicitud={this.state.origination.number}
                codSolicitudCompleta={this.state.origination.fullNumber}
              />
            </SnackbarProvider>
          </div>

          {/* Container Steps */}
          <Stepper activeStep={activeStep} orientation="vertical">
            {steps.map((label, index) => (
              <Step key={label}>
                {/* Title Step */}
                <StepLabel>
                  <Fade>
                    <Typography
                      color={"primary"}
                      className={classNames(
                        classes.stepLabelOk,
                        "py-2 w-100 text-uppercase "
                      )}
                    >
                      {label}
                    </Typography>
                  </Fade>
                </StepLabel>
                {/* Content Step */}
                <StepContent>
                  {<div className="py-4">{this.getStepContent(index)}</div>}
                  {/* Button Prev - Next */}
                  <div className={classNames(classes.actionsContainer, "d-none")}>
                    <div>
                      <Button
                        disabled={activeStep === 0}
                        onClick={this.handleBack}
                        className={classes.button}
                      >
                        Atrás
                      </Button>

                      <Button
                        variant="contained"
                        color="primary"
                        onClick={this.handleNext}
                        className={classes.button}
                      >
                        {activeStep === steps.length - 1
                          ? "Finalizar"
                          : "Siguiente"}
                      </Button>
                    </div>
                  </div>
                </StepContent>
              </Step>
            ))}
          </Stepper>

          {activeStep === -1 && (
            <Paper square elevation={0} className={classes.resetContainer}>
              <PreLoaderImage />
            </Paper>
          )}
          {activeStep === steps.length && (
            <Paper square elevation={0} className={classes.resetContainer}>
              <SnackbarProvider maxSnack={3}>
                <BackgroundFinish
                  data={[
                    {
                      client: this.state.client,
                      process: this.state.origination,
                    },
                  ]}
                />
              </SnackbarProvider>
              <br />
              <Button
                variant="contained"
                color="primary"
                onClick={this.handleReset}
                className={classes.button}
              >
                Nueva Originación
              </Button>
            </Paper>
          )}
          {/*  Modal Cancel Regular */}
          <Dialog
            onClose={this.handleCloseModalCancelRegular}
            open={this.state.openModal}
            maxWidth='xs'
            aria-labelledby="form-dialog"
          >
            <DialogTitle id="form-dialog" className="bg-metal-blue">
              <Typography
                align="center"
                component="span"
                variant="h6"
                className="text-white text-shadow-black"
              >
                Cancelar Proceso de Originación
              </Typography>
            </DialogTitle>

            <form
              onSubmit={this.handleSubmitCancelRegular}
              autoComplete="off"
            >
              <DialogContent>
                <Typography align="center">
                  ¿Esta seguro de cancelar la originación?
                </Typography>
              </DialogContent>
              <DialogActions>
                <Grid container spacing={8}>
                  <Grid item xs={6}>
                    <div className={classes.buttonProgressWrapper}>
                      <Button
                        disabled={odcRegularActivity.cancelActivity.loading}
                        className={classes.button}
                        fullWidth={true}
                        color="secondary"
                        size="small"
                        onClick={this.handleCloseModalCancelRegular}
                        margin="normal"
                      >
                        No
                      </Button>
                    </div>
                  </Grid>
                  <Grid item xs={6}>
                    <div className={classes.buttonProgressWrapper}>
                      <Button
                        disabled={odcRegularActivity.cancelActivity.loading}
                        className={classes.button}
                        type="submit"
                        margin="normal"
                        color="primary"
                        size="small"
                        fullWidth={true}
                      >
                        Si
                        <SendIcon fontSize="small" className="ml-2" />
                        {odcRegularActivity.cancelActivity.loading && (
                          <CircularProgress
                            size={24}
                            className={classes.buttonProgress}
                          />
                        )}
                      </Button>
                    </div>
                  </Grid>
                </Grid>
              </DialogActions>
            </form>
          </Dialog>
        </div>
      </Hotkeys>
    );
  }
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(RegularPage))
);
