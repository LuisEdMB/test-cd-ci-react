// React
import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

// Material UI
import { withStyles, Stepper, Step, StepLabel, StepContent, Button, Paper, Typography, Dialog, DialogTitle, DialogContent, DialogActions, Grid, CircularProgress } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Material UI - Color
import { green } from '@material-ui/core/colors'

// Utils
import classNames from 'classnames'
import { SmoothScroll, resolveStateRedux } from '../../../utils/Utils';
import * as OriginationUtils from '../../../utils/origination/Origination'
import * as Constants from '../../../utils/Constants'
import * as MasterOrigination from '../../../utils/origination/MasterOrigination'

// Config
import { URL_BASE } from '../../../actions/config'

// Shorcurt
import Hotkeys from 'react-hot-keys'

// Notistack
import { SnackbarProvider } from 'notistack'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import { getConstantODC } from '../../../actions/generic/constant'
import { firstRegularCall, registerClient, commercialOffer, offerSummary } from '../../../actions/odc-regular/odc-regular'
import { blockSolicitude, unlockSolicitude } from '../../../actions/odc-express/odc'
import { cancelActivity, genericActivity, simpleGenericActivity } from '../../../actions/odc-regular/odc-activity'
import { createClient, updateClient, createRelationship, createAccount, createCreditCard } from '../../../actions/pmp/pmpRegular'
import * as ActionPci from '../../../actions/pci/pci'
import * as ActionOdcMaster from '../../../actions/odc-master/odc-master'

// Effects
import Fade from 'react-reveal/Fade'
import { Redirect } from 'react-router-dom'

// Components
import Timer from '../../../components/Timer'
import PreLoaderImage from '../../../components/PreLoaderImage'
import ConsultClient from './components/consult-client/ConsultClient'
import ClientDetailPreEvaluated from './components/client-detail-pre-evaluated/ClientDetailPreEvaluated'
import ClientRegister from './components/client-register/ClientRegister'
import CommercialOffer from './components/commercial-offer/CommercialOffer'
import ProductDetail from './components/product-detail/ProductDetail'
import OfferSummary from './components/offer-summary/OfferSummary'
import BackgroundFinish from '../../../components/background-finish/BackgroundFinish'
import HistoryPanel from './components/history-panel/HistoryPanel'
import NewCommentForm from '../../../components/NewCommentForm/NewCommentForm'

const styles = (theme) => ({
    root: {
        width: '100%',
        position: 'relative'
    },
    stepLabelOk: {
        background: '#2196f31c',
        padding: '.5em',
        borderRadius: '.5em',
        boxShadow: '0px 1.2px 1px #007ab8'
    },
    stepLabelDefault: {
        background: '#f0f0f080',
        padding: '.5em',
        borderRadius: '0 0 .8em 0',
        borderBottom: '1px solid gray'
    },
    button: {
        textTransform: 'none'
    },
    buttonProgressWrapper: {
        position: 'relative'
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none'
    },
    IconButton: {
        top: -37,
        left: -37,
        position: 'absolute'
    },
    icon: {
        fontSize: 32
    },
    actionsContainer: {
        marginBottom: theme.spacing.unit * 2
    },
    resetContainer: {
        padding: theme.spacing.unit * 3,
        textAlign: 'center'
    },
    historyPanel: {
        position: 'fixed',
        top: '33.3vh',
        zIndex: 900
    },
    NewCommentForm: {
        position: 'fixed',
        top: '20vh',
        right: '5px',
        zIndex: 900
    },
    fontSize: {
        fontSize: 12
    }
})

const mapStateToProps = (state) => {
    return {
        odcRegular: state.odcRegularReducer,
        constantODC: state.constantODCReducer,
        preEvaluatedClient: state.preEvaluatedClientReducer,
        odcRegularActivity: state.odcRegularActivityReducer,
        pmp: state.pmpRegularReducer,
        verifyCellphone: state.verifyCellphoneReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getConstantODC: bindActionCreators(getConstantODC, dispatch),
        cancelActivity: bindActionCreators(cancelActivity, dispatch),
        genericActivity: bindActionCreators(genericActivity, dispatch),
        simpleGenericActivity: bindActionCreators(simpleGenericActivity, dispatch),
        firstCall: bindActionCreators(firstRegularCall, dispatch),
        registerClient: bindActionCreators(registerClient, dispatch),
        offerSummary: bindActionCreators(offerSummary, dispatch),
        commercialOffer: bindActionCreators(commercialOffer, dispatch),
        createClient: bindActionCreators(createClient, dispatch),
        updateClient: bindActionCreators(updateClient, dispatch),
        createRelationship: bindActionCreators(createRelationship, dispatch),
        createAccount: bindActionCreators(createAccount, dispatch),
        createCreditCard: bindActionCreators(createCreditCard, dispatch),
        blockSolicitude: bindActionCreators(blockSolicitude, dispatch),
        unlockSolicitude: bindActionCreators(unlockSolicitude, dispatch),
        encryptCardnumber: bindActionCreators(ActionPci.encryptCardnumber, dispatch),
        registerClientIntoMaster: bindActionCreators(ActionOdcMaster.registerUpdateClient, dispatch),
        registerAccountIntoMaster: bindActionCreators(ActionOdcMaster.registerAccount, dispatch),
        registerCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.registerCreditCard, dispatch)
    }
}

class RegularCallCenterPage extends Component {
    state = {
        activeStep: 0,
        showHistory: false,
        showVerifyCellphone: true,
        listComments: [],
        origination: {
            number: 0,
            activityNumber: 0,
            fullNumber: '',
            previousStatePhaseFlow: 0,
            nextStatePhaseFlow: 0,
            typeSolicitudeCode: 340001,
            typeSolicitudeOriginationCode: 390002,
            typeSolicitudeReqCode: 0,
            rePrintMotiveCode: 0
        },
        pmp: {
            clientNumber: '',
            relationshipNumber: '',
            accountNumber: '',
            cardNumber: '',
            parallelCardNumber: ''
        },
        client: {
            code: 0,
            id: 0,
            documentTypeErro: false,
            documentTypeId: '',
            documentTypeAux: '',
            documentTypeInternalValue: '',
            documentType: '',
            documentNumber: '',
            firstName: '',
            secondName: '',
            lastName: '',
            motherLastName: '',
            fullName: '',
            shortName: '',
            birthday: '',
            genderId: '',
            gender: '',
            genderInternalValue: '',
            nationalityId: '',
            nationality: '',
            maritalStatusId: '',
            maritalStatusInternalValue: '',
            maritalStatus: '',
            academicDegreeId: '',
            academicDegree: '',
            academicDegreeInternalValue: '',
            email: '',
            cellphone: '',
            cellphoneConfirmed: false,
            telephone: '',
            homeAddress: {
                departmentId: '',
                department: '',
                provinceId: '',
                province: '',
                districtId: '',
                district: '',
                viaId: '',
                via: '',
                viaInternalValue: '',
                nameVia: '',
                number: '',
                building: '',
                inside: '',
                mz: '',
                lot: '',
                zoneId: '',
                zone: '',
                zoneInternalValue: '',
                nameZone: '',
                typeResidence: '',
                housingTypeId: '',
                housingType: '',
                housingTypeInternalValue: '',
                reference: ''
            },
            workAddress: {
                departmentId: '',
                department: '',
                provinceId: '',
                province: '',
                districtId: '',
                district: '',
                viaId: '',
                via: '',
                viaInternalValue: '',
                nameVia: '',
                number: '',
                building: '',
                inside: '',
                mz: '',
                lot: '',
                zoneId: '',
                zone: '',
                zoneInternalValue: '',
                nameZone: '',
                typeResidence: '',
                housingTypeId: '',
                housingType: '',
                reference: ''
            },
            employmentSituationId: '',
            employmentSituation: '',
            jobTitleId: '',
            jobTitle: '',
            economicActivityId: '',
            economicActivity: '',
            laborIncomeDate: '',
            businessOption: '',
            businessName: '',
            ruc: '',
            grossIncome: 0,
            workTelephoneExtension: '',
            workTelephone: '',
            extra: {
                paymentDate: '',
                paymentDateInternalValue: '',
                paymentDateId: '',
                sendCorrespondence: '',
                sendCorrespondenceId: '',
                accountStatusId: '',
                accountStatus: '',
                effectiveWithdrawal: '',
                alert: '',
                sendCommonPromotion: '',
                media: '',
                mediaId: '',
                promoterId: '',
                promoter: '',
                term: ''
            },
            relationTypeId: 330001
        },
        creditCard: null,
        pct: {
            number: ''
        },
        segment: '',
        effectiveProvision: 0,
        lineAvailable: -1,
        lineSAE: -1,
        finalEffectiveProvision: 0,
        finalLineAvailable: -1,
        finalLineSAE: -1,
        finalMaxAmount: -1,
        pctSAE: '',
        isSAE: false,
        firstCallCda: null,
        secondCallCda: null,
        adn: null,
        siebel: null,
        fraudPrevention: null,
        cancel: false,
        redirectError: false,
        openModal: false,
        reintentRegisterUpdateClient: 0,
        reintentProcessPMP: 0,
        reintentBlockSolicitude: 0,
        reintentBlockTypeProcessPMP: 0,
        isBiometricOk: false,
        sae: null,
        disbursementTypeId: 0
    }

  componentDidMount() {
    this.props.getConstantODC()
    const documentNumber = this.props.location?.state?.documentNumber || ''
    this.setState((state) => ({
        ...state,
        client: {
            ...state.client,
            documentNumber: documentNumber
        }
    }))
  }

  componentDidUpdate = (prevProps) => {
        resolveStateRedux(prevProps.odcRegular.consultClient, this.props.odcRegular.consultClient, null,
            _ => {
                const { origination, client, firstCallCda, secondCallCda, siebel, fraudPrevention, adn, validations, sae } = this.props.odcRegular.consultClient.data
                if (origination && firstCallCda && secondCallCda) {
                    this.setState(state => ({
                        ...state,
                        origination: {
                            ...state.origination,
                            ...origination
                        },
                        client: {
                            ...state.client,
                            ...client
                        },
                        firstCallCda: {
                            ...state.firstCallCda,
                            ...firstCallCda
                        },
                        secondCallCda: {
                            ...state.secondCallCda,
                            ...secondCallCda
                        },
                        adn: {
                            ...state.adn,
                            ...adn
                        },
                        siebel: {
                            ...state.siebel,
                            ...siebel
                        },
                        fraudPrevention: {
                            ...state.fraudPrevention,
                            ...fraudPrevention
                        },
                        validations: validations,
                        sae: sae
                    }))
                }
            }, null, null
        )
    }

    handleOpenModalCancelRegular = _ => {
        this.setState(state => ({
            ...state,
            openModal: true
        }))
    }

    handleCloseModalCancelRegular = _ => {
        this.setState(state => ({
            ...state,
            openModal: false
        }))
    }

    handleCancelRegular = _ => {
        this.handleCloseModalCancelRegular()
        this.handleReset()
    }

    handleSubmitCancelRegular = (e) => {
        e.preventDefault()
        let { origination, activeStep } = this.state
        let cancel = this.selectStatePhaseFlowCancel(activeStep)
        let sendData = {
            cod_solicitud: origination.number,
            cod_solicitud_completa: origination.fullNumber,
            cod_flujo_fase_estado_actual: cancel.currentStatePhaseFlow,
            cod_flujo_fase_estado_anterior: cancel.previousStatePhaseFlow,
            nom_actividad: cancel.activityName
        }
        this.props.cancelActivity(sendData)
    }

    selectStatePhaseFlowCancel = step => {
        switch (step) {
            case 1:
                return {
                    currentStatePhaseFlow: 20103,
                    previousStatePhaseFlow: 20101,
                    activityName: 'Validación: Cancelado'
                }
            case 2:
                return {
                    currentStatePhaseFlow: 20203,
                    previousStatePhaseFlow: 20201,
                    activityName: 'Regular: Cancelado'
                }
            case 3:
                return {
                    currentStatePhaseFlow: 20303,
                    previousStatePhaseFlow: 20301,
                    activityName: 'Oferta: Cancelado'
                }
            case 4:
                return {
                    currentStatePhaseFlow: 20403,
                    previousStatePhaseFlow: 20401,
                    activityName: 'Preventa: Cancelado'
                }
            default:
                return {
                    currentStatePhaseFlow: 0,
                    previousStatePhaseFlow: 0,
                    activityName: '',
                }
        }
    }

    handleNext = _ => {
        this.setState(state => ({
                activeStep: state.activeStep + 1
            }), _ => SmoothScroll(this.state.activeStep)
        )
    }

    handleReset = () => {
        this.props.history.push('/odc/express/call-center-nueva-solicitud')
    }

    handleClientValidationForm = (data) => {
        let { client } = data;
        if (client) {
            let sendData = {
                tipo_doc: client.documentTypeAux,
                nro_doc: client.documentNumber,
                tipo_doc_letra: client.documentType,
                cod_flujo_fase_estado_actual: 20101,
                cod_flujo_fase_estado_anterior: 20100,
                cod_solicitud: 0,
                cod_solicitud_completa: '',
                cod_cliente: 0,
                cod_tipo_solicitud: 340001,
                cod_tipo_relacion: 330001,
                cod_tipo_solicitud_requerimiento: 0,
                cod_tipo_solicitud_originacion: 390002,
                maritalStatus: client.maritalStatus,
                dateJoinedCompany: client.laborIncomeDate,
                birthDate: client.birthday,
                studiesLevel: client.academicDegree,
                occupation: client.jobTitle,
                laborSituation: client.employmentSituation,
                cod_etapa_maestro: Constants.MasterConfigurationStage.masterRegularStage
            }
            let sendDataExtra = {
                documentTypeId: client.documentTypeId,
                documentTypeInternalValue: client.documentTypeInternalValue
            }
            this.props.firstCall(sendData, sendDataExtra)
            this.setState(state => ({
                ...state,
                client: {
                    ...state.client,
                    ...data.client
                }
            }))
        }
    }

    handlePreEvaluatedClient = (data) => {
        if (data) {
            let { origination } = this.state
            let sendData = {
                cod_solicitud: origination.number,
                cod_solicitud_completa: origination.fullNumber,
                cod_flujo_fase_estado_actual: 20102,
                cod_flujo_fase_estado_anterior: 20101,
                nom_actividad: 'Validación: Aceptado'
            }
            this.setState((state) => ({
                ...state,
                client: {
                    ...state.client,
                    ...data.client
                },
                lineAvailable: data.lineAvailable,
                effectiveProvision: data.effectiveProvision,
                lineSAE: data.lineSAE,
                maxAmount: data.maxAmount,
                segment: data.segment
            }));
            this.props.genericActivity(sendData, null, true)
        }
    }

    handleClientRegister = ({ data, reset = false }) => {
        if (data) {
            const { reintentRegisterUpdateClient } = this.state
            const steps = this.getStepsRegisterUpdateClient()
            steps[reset ? 0 : reintentRegisterUpdateClient].action(data)
        }
    }
    
    getStepsRegisterUpdateClient = _ => ({
        0:  {
            action: async data => {
                const { client } = data
                const telephone = client.telephone
                const workTelephone = client.workTelephone

                const sendData = {
                    tipo_doc: client.documentTypeAux,
                    nro_doc: client.documentNumber,
                    tipo_doc_letra: client.documentType.toUpperCase(),
                    maritalStatus: client.maritalStatusInternalValue,
                    dateJoinedCompany: client.laborIncomeDate.toUpperCase(),
                    birthDate: client.birthday,
                    employerRUC: client.ruc,
                    nationality: client.nationality.toUpperCase(),
                    studiesLevel: client.academicDegree.toUpperCase(),
                    occupation: client.jobTitle
                        ? client.jobTitle.toUpperCase()
                        : 'Desconocido',
                    gender: client.gender.toUpperCase(),
                    laborSituation: client.employmentSituation.toUpperCase(),
                    cellPhone: `${client.cellphone}`,
                    homePhone: telephone !== '' ? `${telephone}`.substring(0, 9) : `0510000000`,
                    workPhone: workTelephone !== ''
                        ? `${workTelephone}`.substring(0, 9)
                        : `0510000000`,
                    housingType: client.homeAddress.housingType.toUpperCase(),
                    cod_cliente: this.state.client.id,
                    cod_tipo_documento: client.documentTypeId,
                    des_nro_documento: client.documentNumber,
                    des_primer_nom: client.firstName,
                    des_segundo_nom: client.secondName,
                    des_ape_paterno: client.lastName,
                    des_ape_materno: client.motherLastName,
                    fec_nacimiento: client.birthday,
                    cod_genero: client.genderId.toString(),
                    cod_estado_civil: client.maritalStatusId.toString(),
                    cod_nacionalidad: client.nationalityId.toString(),
                    cod_pais_residencia: 230183,
                    cod_gradoacademico: client.academicDegreeId.toString(),
                    des_correo: client.email,
                    des_telef_celular: client.cellphone,
                    des_telef_fijo: telephone !== ''
                        ? `${telephone}`.substring(0, 12).replace(/\s/g, '').trim()
                        : `0510000000`,
                    cod_tipo_cliente: 290002,
                    cod_situacion_laboral: client.employmentSituationId,
                    cod_cargo_profesion: client.jobTitleId ? client.jobTitleId : 0,
                    cod_actividad_economica: client.economicActivityId
                        ? client.economicActivityId
                        : 0,
                    fec_ingreso_laboral: client.laborIncomeDate,
                    des_ruc: client.ruc,
                    des_razon_social: client.businessName,
                    num_ingreso_bruto: client.grossIncome,
                    des_telefono_laboral: workTelephone !== ''
                        ? `${workTelephone}`.substring(0, 12).replace(/\s/g, '').trim()
                        : `0510000000`,
                    des_anexo_laboral: client.workTelephoneExtension,
                    flg_autoriza_datos_per: client.extra.term === '1' || client.extra.term ? true : false,
                    cod_solicitud: this.state.origination.number,
                    cod_solicitud_completa: this.state.origination.fullNumber,
                    cod_fecha_pago: client.extra.paymentDateId,
                    cod_envio_correspondencia: client.extra.sendCorrespondenceId
                        ? client.extra.sendCorrespondenceId
                        : 0,
                    cod_estado_cuenta: client.extra.accountStatusId,
                    flg_consumo_internet: client.extra.internetConsumeAdvice === '1',
                    flg_consumo_extranjero: client.extra.foreignConsumeAdvice === '1',
                    flg_retirar_efectivo: client.extra.effectiveDisposition === '1',
                    flg_sobregirar_credito: client.extra.overflipping === '1',
                    flg_notificacion_email: client.extra.mailAlerts === '1',
                    num_monto_minimo: client.extra.minimumForAlerts ? client.extra.minimumForAlerts : 0,
                    cod_resp_promotor: client.extra.promoterId,
                    cod_resp_registro: 0,
                    cod_resp_actualiza: 0,
                    cod_flujo_fase_estado_actual: 20204,
                    cod_flujo_fase_estado_anterior: 20201,
                    cod_agencia: 0,
                    cod_flujo_fase_estado: 0,
                    cod_valor_color: 0,
                    disp_efec_porcentaje: 0,
                    disp_efec_monto: 0,
                    cod_valor_marca: 0,
                    linea_credito_oferta: 0,
                    linea_credito_final: 0,
                    cod_laboral: 0,
                    cod_producto: 0,
                    cod_motivo_emboce: 0,
                    des_motivo_comentario_emboce: '',
                    flg_biometria: false,
                    cod_tipo_relacion: 330001,
                    cod_tipo_solicitud: 340001,
                    cod_tipo_solicitud_requerimiento: 0,
                    cod_tipo_solicitud_originacion: 390002,
                    des_segmento: this.state.segment,
                    list_direccion: [
                        {
                            cod_direccion: client.homeAddress.id ? client.homeAddress.id : 0,
                            cod_cliente: this.state.client.id,
                            cod_tipo_direccion: 260001,
                            des_tipo_direccion: 'Dirección personal',
                            cod_ubigeo: client.homeAddress.districtId,
                            cod_via: client.homeAddress.viaId,
                            des_nombre_via: client.homeAddress.nameVia,
                            des_nro: client.homeAddress.number,
                            des_departamento: client.homeAddress.building,
                            des_interior: client.homeAddress.inside,
                            des_manzana: client.homeAddress.mz,
                            des_lote: client.homeAddress.lot,
                            cod_zona: client.homeAddress.zoneId ? client.homeAddress.zoneId : 0,
                            des_nombre_zona: client.homeAddress.nameZone,
                            cod_tipo_residencia: 0,
                            cod_tipo_vivienda: client.homeAddress.housingTypeId,
                            des_referencia_domicilio: client.homeAddress.reference
                        },
                        {
                            cod_direccion: client.workAddress.id ? client.workAddress.id : 0,
                            cod_cliente: this.state.client.id,
                            cod_tipo_direccion: 260002,
                            des_tipo_direccion: 'Dirección laboral',
                            cod_ubigeo: client.workAddress.districtId,
                            cod_via: client.workAddress.viaId,
                            des_nombre_via: client.workAddress.nameVia,
                            des_nro: client.workAddress.number,
                            des_departamento: client.workAddress.building,
                            des_interior: client.workAddress.inside,
                            des_manzana: client.workAddress.mz,
                            des_lote: client.workAddress.lot,
                            cod_zona: client.workAddress.zoneId ? client.workAddress.zoneId : 0,
                            des_nombre_zona: client.workAddress.nameZone,
                            cod_tipo_residencia: '',
                            cod_tipo_vivienda: '',
                            des_referencia_domicilio: client.workAddress.reference
                        }
                    ]
                }
                const result = await this.props.registerClient(sendData).then(response => response)
                if (result?.data?.success) {
                    this.setState(state => ({
                        ...state,
                        client: {
                            ...state.client,
                            ...data.client,
                        },
                        reintentRegisterUpdateClient: 1
                    }), _ => this.handleClientRegister({ data }))
                }
            }
        },
        1: {
            action: async data => {
                const { origination } = this.state
                const sendData = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    activityName: MasterOrigination.registerUpdateClientActivity.activityName,
                    phaseCode: MasterOrigination.registerUpdateClientActivity.phaseCode,
                    masterStageCode: Constants.MasterConfigurationStage.masterRegularStage
                }
                const result = await this.props.registerClientIntoMaster(sendData).then(response => response)
                if (result?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentRegisterUpdateClient: 2
                    }), _ => this.handleClientRegister({ data }))
                }
            }
        },
        2: {
            action: _ => {
                const { origination } = this.state
                const sendData = {
                    cod_solicitud: origination.number,
                    cod_solicitud_completa: origination.fullNumber,
                    cod_flujo_fase_estado_actual: 20301,
                    cod_flujo_fase_estado_anterior: 20204,
                    nom_actividad: 'Oferta: En Proceso'
                }
                this.props.simpleGenericActivity(sendData, { retomar: true }, true)
            }
        }
    })

    handleCommercialOffer = (data) => {
        const { origination, client, lineAvailable, sae } = this.state
        if (data) {
            let sendData = { };
            if (sae && Object.keys(sae).length > 0) {
                sendData = {
                    tipo_doc: data.client.documentTypeId === 100001 ? '1' : '2',
                    nro_doc: data.client.documentNumber,
                    tipo_doc_letra: data.client.documentType,
                    nom_actividad: 'Oferta: Aceptado',
                    cod_flujo_fase_estado_actual: 20302,
                    cod_flujo_fase_estado_anterior: 20301,
                    cod_valor_color: data.creditCard.colorId,
                    disp_efec_porcentaje: data.finalEffectiveProvision,
                    disp_efec_monto: (data.finalEffectiveProvision * data.finalLineAvailable) / 100,
                    cod_valor_marca: data.creditCard.brandId,
                    cod_producto: data.creditCard.productId,
                    linea_credito_oferta: lineAvailable,
                    linea_credito_final: data.finalLineAvailable,
                    cod_solicitud: origination.number,
                    cod_solicitud_completa: origination.fullNumber,
                    cod_cliente: client.id,
                    cod_agencia: 0,
                    fec_act_base_sae: sae.baseDate ? sae.baseDate : '',
                    flg_bq2: sae.block ? sae.block : '',
                    linea_sae: sae.lineSAE ? sae.lineSAE : 0,
                    linea_sae_final: data.finalLineSAE ? data.finalLineSAE : 0,
                    monto_max: sae.maxAmount ? sae.maxAmount : 0,
                    monto_max_final: data.finalMaxAmount ? data.finalMaxAmount : 0,
                    pct_sae_1: sae.pctSAE1 ? sae.pctSAE1 : '',
                    pct_sae_2: sae.pctSAE2 ? sae.pctSAE2 : '',
                    pct_sae_final: data.pctSAE ? data.pctSAE : '',
                    des_cci: data.CCI ? data.CCI : '',
                    numero_cuotas: data.numberFees ? data.numberFees : '',
                    monto_cuota: data.feeAmount ? data.feeAmount : '',
                    tcea: data.tcea,
                    cod_tipo_desembolso: data.disbursementTypeId
                        ? data.disbursementTypeId
                        : 0,
                    des_descripcion: data.description ? data.description : '',
                    monto_cci: data.transferAmount ? data.transferAmount : 0,
                    monto_efectivo: data.effectiveAmount ? data.effectiveAmount : 0
                }
            } else {
                sendData = {
                    tipo_doc: data.client.documentTypeId === 100001 ? '1' : '2',
                    nro_doc: data.client.documentNumber,
                    tipo_doc_letra: data.client.documentType,
                    nom_actividad: 'Oferta: Aceptado',
                    cod_flujo_fase_estado_actual: 20302,
                    cod_flujo_fase_estado_anterior: 20301,
                    cod_valor_color: data.creditCard.colorId,
                    disp_efec_porcentaje: data.finalEffectiveProvision,
                    disp_efec_monto: (data.finalEffectiveProvision * data.finalLineAvailable) / 100,
                    cod_valor_marca: data.creditCard.brandId,
                    cod_producto: data.creditCard.productId,
                    linea_credito_oferta: lineAvailable,
                    linea_credito_final: data.finalLineAvailable,
                    cod_solicitud: origination.number,
                    cod_solicitud_completa: origination.fullNumber,
                    cod_cliente: client.id,
                    cod_agencia: 0,
                    fec_act_base_sae: '',
                    flg_bq2: '',
                    linea_sae: 0,
                    linea_sae_final: 0,
                    monto_max: 0,
                    monto_max_final: 0,
                    pct_sae_1: '',
                    pct_sae_2: '',
                    pct_sae_final: '',
                    des_cci: '',
                    cod_tipo_desembolso: 0,
                    monto_cci: 0,
                    monto_efectivo: 0,
                    numero_cuotas: '',
                    monto_cuota: '',
                    tcea: ''
                }
            }
            this.props.commercialOffer(sendData).then(response => {
                if (response.data) {
                    this.setState(state => ({
                        ...state,
                        client: {
                            ...state.client,
                            ...data.client,
                        },
                        creditCard: {
                            ...state.creditCard,
                            ...data.creditCard,
                        },
                        finalEffectiveProvision: data.finalEffectiveProvision
                            ? data.finalEffectiveProvision
                            : 0,
                        finalLineAvailable: data.finalLineAvailable
                            ? data.finalLineAvailable
                            : 0,
                        finalLineSAE: data.finalLineSAE ? data.finalLineSAE : 0,
                        finalMaxAmount: data.finalMaxAmount ? data.finalMaxAmount : 0,
                        pctSAE: data.pctSAE ? data.pctSAE : '',
                        isSAE: data.isSAE,
                        numero_cuotas: data.numberFees ? data.numberFees : '',
                        monto_cuota: data.feeAmount ? data.feeAmount : '',
                        tcea: data.tcea,
                        disbursementTypeId: data.disbursementTypeId
                            ? data.disbursementTypeId
                            : 0
                    }))
                }
            })
        }
    }

    handleProductDetail = async data => {
        let { origination, reintentProcessPMP, reintentBlockSolicitude, client, creditCard, 
            finalLineAvailable, pmp, secondCallCda } = this.state
        let { pct } = data
        let organization = '641'
        if (reintentBlockSolicitude === 0) {
            const sendData = {
                solicitudeCode: origination.number,
                completeSolicitudeCode: origination.fullNumber,
                clientCode: client.id,
                documentTypeId: client.documentTypeAux,
                documentNumber: client.documentNumber,
                lineAvailable: finalLineAvailable,
                globalLineAvailable: secondCallCda?.solicitud?.salidaCDA?.ofertaFinal
            }
            this.props.blockSolicitude(sendData).then(response => {
                if (response?.success)
                    this.setState(state => ({
                        ...state,
                        reintentBlockSolicitude: 1
                    }), _ => this.handleProductDetail(data))
            })
        }
        if (reintentBlockSolicitude === 1) {
            if (reintentProcessPMP === 0) {
                const sendData = {
                    cod_solicitud: origination.number,
                    cod_solicitud_completa: origination.fullNumber,
                    cod_flujo_fase_estado_actual: 20402,
                    cod_flujo_fase_estado_anterior: 20401,
                    nom_actividad: 'Preventa: Aceptado'
                }
                this.props.genericActivity(sendData)
                    .then(response => {
                        if (response?.data?.success)
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 1
                            }), _ => this.handleProductDetail(data))
                    })
            }
            if (reintentProcessPMP === 1) {
                const sendData = {
                    organization: organization,
                    cod_flujo_fase_estado_actual: 20502,
                    cod_flujo_fase_estado_anterior: 20501,
                    nom_actividad: 'Originación Regular: PMP Crear Cliente',
                    process: origination,
                    client: client
                }
                const sendDataPmp = OriginationUtils.setDataCreateClientPMP(sendData)
                this.props.createClient(sendDataPmp)
                    .then(response => {
                        if (response?.data) {
                            if (response.data.errorCode === '12') {
                                this.setState(state => ({
                                    ...state,
                                    reintentProcessPMP: 1.5
                                }), _ => this.handleProductDetail(data))
                            } else if (response.data.success) {
                                const { pmpCrearCliente } = response.data
                                if (pmpCrearCliente) {
                                    const { createCustomer } = pmpCrearCliente
                                    this.setState(state => ({
                                        ...state,
                                        reintentProcessPMP: 2,
                                        pmp: {
                                            ...pmp,
                                            clientNumber: createCustomer.customerNumber
                                        }
                                    }), _ => this.handleProductDetail(data))
                                }
                            }
                        }
                    })
            }
            if (reintentProcessPMP === 1.5) {
                const sendData = {
                    organization: organization,
                    cod_flujo_fase_estado_actual: 20503,
                    cod_flujo_fase_estado_anterior: 20501,
                    nom_actividad: 'Originación Regular: PMP Editar Cliente',
                    process: origination,
                    client: client
                }
                const sendDataPmp = OriginationUtils.setDataUpdateClientePMP(sendData)
                this.props.updateClient(sendDataPmp)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpActualizarCliente } = response.data
                            if (pmpActualizarCliente) {
                                const { updateCustomer } = pmpActualizarCliente
                                this.setState(state => ({
                                    ...state,
                                    reintentProcessPMP: 2,
                                    pmp: {
                                        ...pmp,
                                        customerUpdated: true,
                                        clientNumber: updateCustomer.customerNumber
                                    }
                                }), _ => this.handleProductDetail(data))
                            }
                        }
                    })
            }
            if (reintentProcessPMP === 2) {
                const { customerUpdated } = pmp
                const sendData = {
                    organization: organization,
                    cod_flujo_fase_estado_actual: 20504,
                    cod_flujo_fase_estado_anterior: customerUpdated ? 20503 : 20502,
                    nom_actividad: 'Originación Regular: PMP Crear Relación',
                    process: origination,
                    client: client,
                    pmp: pmp,
                    lineAvailable: finalLineAvailable
                }
                const sendDataPmp = OriginationUtils.setDataCreateRelationshipPMP(sendData)
                this.props.createRelationship(sendDataPmp)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpCrearRelacion } = response.data
                            const { createCustomerRelationship } = pmpCrearRelacion
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 3,
                                pmp: {
                                    ...state.pmp,
                                    relationshipNumber: createCustomerRelationship.relationshipNumber
                                }
                            }), _ => this.handleProductDetail(data))
                        }
                    })
            }
            if (reintentProcessPMP === 3) {
                const sendData = {
                    organization: organization,
                    cod_flujo_fase_estado_actual: 20505,
                    cod_flujo_fase_estado_anterior: 20504,
                    nom_actividad: 'Originación Regular: PMP Crear Cuenta',
                    process: origination,
                    client: client,
                    creditCard: creditCard,
                    pmp: pmp,
                    pct: pct,
                    lineAvailable: finalLineAvailable
                }
                const sendDataPmp = OriginationUtils.setDataCreateAccountPMP(sendData)
                this.props.createAccount(sendDataPmp)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpCrearCuenta } = response.data
                            const { createCustomerAccount } = pmpCrearCuenta
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 4,
                                pmp: {
                                    ...state.pmp,
                                    accountNumber: createCustomerAccount.accountNumber
                                }
                            }), _ => this.handleProductDetail(data))
                        }
                    })
            }
            // Master - Create Account
            if (reintentProcessPMP === 4) {
                const sendData = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    activityName: MasterOrigination.registerAccountActivity.activityName,
                    phaseCode: MasterOrigination.registerAccountActivity.phaseCode,
                    masterStageCode: Constants.MasterConfigurationStage.masterRegularStage
                }
                const result = await this.props.registerAccountIntoMaster(sendData).then(response => response)
                if (result?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentProcessPMP: 5
                    }), _ => this.handleProductDetail(data))
                }
            }
            if (reintentProcessPMP === 5) {
                const sendData = {
                    organization: organization,
                    cod_flujo_fase_estado_actual: 20506,
                    cod_flujo_fase_estado_anterior: 20505,
                    nom_actividad: 'Originación Regular: PMP Crear Tarjeta',
                    process: origination,
                    client: client,
                    creditCard: creditCard,
                    pmp: pmp,
                    pct: pct,
                    lineAvailable: finalLineAvailable
                }
                const sendDataPmp = OriginationUtils.setDataCreateCreditCard(sendData)
                this.props.createCreditCard(sendDataPmp)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpCrearTarjetaCredito } = response.data
                            const { createCreditCard } = pmpCrearTarjetaCredito
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 6,
                                pmp: {
                                    ...state.pmp,
                                    cardNumber: createCreditCard.cardNumber
                                },
                                creditCard: {
                                    ...state.creditCard,
                                    cardNumber: createCreditCard.cardNumber
                                }
                            }), _ => this.handleProductDetail(data))
                        }
                    })
            }
            // Encrypting Cardnumber
            if (reintentProcessPMP === 6) {
                const sendDataPci = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    pciStage: Constants.PciConfigurationStage.pciRegularStage
                }
                this.props.encryptCardnumber(sendDataPci).then(response => {
                    if (response?.success || response?.errorCode === Constants.CustomErrorCode.errorCodeCreditCardAlreadyPci) {
                        this.setState(state => ({
                            ...state,
                            reintentProcessPMP: 7
                        }), _ => this.handleProductDetail(data))
                    }
                })
            } 
            // Master - Create Credit Card
            if (reintentProcessPMP === 7) {
                const sendData = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    activityName: MasterOrigination.registerCreditCardActivity.activityName,
                    phaseCode: MasterOrigination.registerCreditCardActivity.phaseCode,
                    masterStageCode: Constants.MasterConfigurationStage.masterRegularStage
                }
                const result = await this.props.registerCreditCardIntoMaster(sendData).then(response => response)
                if (result?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentProcessPMP: 8
                    }), _ => this.handleProductDetail(data))
                }
            } 
            if (reintentProcessPMP === 8) {
                const sendData = {
                    cod_solicitud: origination.number,
                    cod_solicitud_completa: origination.fullNumber,
                    cod_flujo_fase_estado_actual: 20507,
                    cod_flujo_fase_estado_anterior: this.state.isSAE ? 20520 : 20506,
                    nom_actividad: 'Originación Regular: Aceptado'
                }
                this.props.genericActivity(sendData, null, true).then(response => {
                    if (response?.data?.success)
                        this.setState(state => ({
                            ...state,
                            reintentBlockSolicitude: 2
                        }), _ => this.handleProductDetail(data))
                })
            }
        }
        if (reintentBlockSolicitude === 2) {
            const sendData = {
                solicitudeCode: origination.number,
                completeSolicitudeCode: origination.fullNumber,
                clientCode: client.id,
                documentTypeId: client.documentTypeAux,
                documentNumber: client.documentNumber
            }
            this.props.unlockSolicitude(sendData)
        }
    }

    handleOfferSummary = _ => {
        let {
            origination,
            creditCard,
            lineAvailable,
            finalLineAvailable,
            finalEffectiveProvision,
        } = this.state
        if (
            origination &&
            creditCard &&
            lineAvailable >= 0 &&
            finalLineAvailable >= 0 &&
            finalEffectiveProvision >= 0
        ) {
            let sendData = {
                cod_solicitud: origination.number,
                cod_solicitud_completa: origination.fullNumber,
                cod_flujo_fase_estado_actual: 20602,
                cod_flujo_fase_estado_anterior: 20601,
                nom_actividad: 'Resumen: Aceptado',
                cod_valor_color: creditCard.colorId,
                disp_efec_porcentaje: finalEffectiveProvision,
                cod_valor_marca: creditCard.brandId,
                linea_credito_oferta: lineAvailable,
                linea_credito_final: finalLineAvailable
            }
            this.props.offerSummary(sendData)
        }
    }
  
    getSteps = _ => {
        return [
            'Originación Regular - Consulta cliente',
            'Validación Cliente',
            'Registro del Cliente',
            'Oferta Comercial',
            'Detalle Producto',
            'Resumen Oferta'
        ]
    }

    getStepContent = (step) => {
        switch (step) {
        case 0:
            return (
                <div>
                    <SnackbarProvider maxSnack={3}>
                        <ConsultClient
                            client={this.state.client}
                            consultClient={this.props.odcRegular.consultClient}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext}
                            handleClientValidationForm={this.handleClientValidationForm} />
                    </SnackbarProvider>
                </div>
            )
        case 1:
            return (
                <div>
                    <SnackbarProvider maxSnack={3}>
                        <ClientDetailPreEvaluated
                            cancelActivity={this.props.odcRegularActivity.cancelActivity}
                            genericActivity={this.props.odcRegularActivity.genericActivity}
                            origination={this.state.origination}
                            client={this.state.client}
                            firstCallCda={this.state.firstCallCda}
                            secondCallCda={this.state.secondCallCda}
                            adn={this.state.adn}
                            siebel={this.state.siebel}
                            fraudPrevention={this.state.fraudPrevention}
                            validations={this.state.validations}
                            sae={this.state.sae}
                            history={this.props.history}
                            constantODC={ this.props.constantODC }
                            handlePreEvaluatedClient={this.handlePreEvaluatedClient}
                            handleOpenModalCancelRegular={this.handleOpenModalCancelRegular}
                            handleCancelRegular={this.handleCancelRegular}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext} />
                    </SnackbarProvider>
                </div>
            )
        case 2:
            return (
                <div>
                    <SnackbarProvider maxSnack={6}>
                        <ClientRegister
                            cancelActivity={this.props.odcRegularActivity.cancelActivity}
                            simpleGenericActivity={
                                this.props.odcRegularActivity.simpleGenericActivity
                            }
                            registerClient={this.props.odcRegular.registerClient}
                            client={this.state.client}
                            origination={this.state.origination}
                            callCenter
                            handleClientRegister={this.handleClientRegister}
                            handleOpenModalCancelRegular={this.handleOpenModalCancelRegular}
                            handleCancelRegular={this.handleCancelRegular}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext} />
                    </SnackbarProvider>
                </div>
            )
        case 3:
            return (
                <div>
                    <SnackbarProvider maxSnack={3}>
                        <CommercialOffer
                            cancelActivity={this.props.odcRegularActivity.cancelActivity}
                            secondCallCda={this.state.secondCallCda}
                            client={this.state.client}
                            sae={this.state.sae}
                            commercialOffer={this.props.odcRegular.commercialOffer}
                            handleCommercialOffer={this.handleCommercialOffer}
                            handleOpenModalCancelRegular={this.handleOpenModalCancelRegular}
                            handleCancelRegular={this.handleCancelRegular}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext} />
                    </SnackbarProvider>
                </div>
            )
        case 4:
            return (
                <div>
                    <SnackbarProvider maxSnack={3}>
                        <ProductDetail
                            cancelActivity={this.props.odcRegularActivity.cancelActivity}
                            genericActivity={this.props.odcRegularActivity.genericActivity}
                            createClient={this.props.pmp.createClient}
                            updateClient={this.props.pmp.updateClient}
                            createRelationship={this.props.pmp.createRelationship}
                            createAccount={this.props.pmp.createAccount}
                            createCreditCard={this.props.pmp.createCreditCard}
                            createSAE={this.props.pmp.createSAE}
                            blockTypeCreditCard={this.props.pmp.blockTypeCreditCard}
                            pmp={this.state.pmp}
                            secondCallCda={this.state.secondCallCda}
                            segment={this.state.segment}
                            numero_cuotas={this.state.numero_cuotas}
                            monto_cuota={this.state.monto_cuota}
                            tcea={this.state.tcea}
                            client={this.state.client}
                            origination={ this.state.origination }
                            creditCard={this.state.creditCard}
                            lineAvailable={this.state.finalLineAvailable}
                            effectiveProvision={this.state.finalEffectiveProvision}
                            lineSAE={this.state.finalLineSAE}
                            maxAmount={this.state.finalMaxAmount}
                            pctSAE={this.state.pctSAE}
                            isSAE={this.state.isSAE}
                            handleProductDetail={this.handleProductDetail}
                            handleOpenModalCancelRegular={this.handleOpenModalCancelRegular}
                            handleCancelRegular={this.handleCancelRegular}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext} />
                    </SnackbarProvider>
                </div>
            )
        case 5:
            return (
                <div>
                    <SnackbarProvider maxSnack={3}>
                        <OfferSummary
                            offerSummary={this.props.odcRegular.offerSummary}
                            origination={this.state.origination}
                            client={this.state.client}
                            creditCard={this.state.creditCard}
                            lineAvailable={this.state.finalLineAvailable}
                            effectiveProvision={this.state.finalEffectiveProvision}
                            isSAE={this.state.isSAE}
                            lineSAE={this.state.finalLineSAE}
                            maxAmount={this.state.finalMaxAmount}
                            tcea={this.state.tcea}
                            handleOfferSummary={this.handleOfferSummary}
                            handleNext={this.handleNext} />
                    </SnackbarProvider>
                </div>
            );
        default:
            return (
                <div>
                    <PreLoaderImage />
                </div>
            )
        }
    }

    handleIconButton = (name) => _ => {
        let showName = `show${name}`
        this.setState((state) => ({
            ...state,
            [showName]: !state[showName]
        }))
    }

    render() {
        const { classes, odcRegularActivity } = this.props;
        const steps = this.getSteps();
        const { activeStep, redirectError } = this.state;
        if (redirectError) {
            return <Redirect to={{ pathname: `/${URL_BASE}/error` }} />;
        }
        return (
            <Hotkeys
                keyName='shift+i, ctrl+i'
                onKeyUp={this.handleIconButton('History')}>
                    <div className={classNames(classes.root)}>
                        {
                            activeStep > 6 && (
                                <SnackbarProvider maxSnack={3}>
                                    <Timer />
                                </SnackbarProvider>
                            )
                        }
                        <HistoryPanel
                            onClick={this.handleIconButton('History')}
                            showHistory={this.state.showHistory}
                            effectiveProvision={this.state.effectiveProvision}
                            finalEffectiveProvision={this.state.finalEffectiveProvision}
                            lineAvailable={this.state.lineAvailable}
                            finalLineAvailable={this.state.finalLineAvailable}
                            creditCard={this.state.creditCard}
                            client={this.state.client}
                            origination={this.state.origination}
                            numero_cuotas={this.state.numero_cuotas}
                            monto_cuota={this.state.monto_cuota}
                            tcea={this.state.tcea}
                            lineSAE={this.state.finalLineSAE} />
                        <div className={classes.NewCommentForm}>
                            <SnackbarProvider maxSnack={3}>
                                <NewCommentForm
                                    fixed={true}
                                    codSolicitud={this.state.origination.number}
                                    codSolicitudCompleta={this.state.origination.fullNumber} />
                            </SnackbarProvider>
                        </div>
                        <Stepper activeStep={activeStep} orientation='vertical'>
                            {steps.map((label, index) => (
                                <Step key={label}>
                                    <StepLabel>
                                        <Fade>
                                            <Typography
                                                color={'primary'}
                                                className={classNames(classes.stepLabelOk, 'py-2 w-100 text-uppercase ')}>
                                                    {label}
                                            </Typography>
                                        </Fade>
                                    </StepLabel>
                                    <StepContent>
                                        {
                                            <div className='py-4'>
                                                {this.getStepContent(index)}
                                            </div>
                                        }
                                        <div className={classNames(classes.actionsContainer, 'd-none')}>
                                            <div>
                                                <Button
                                                    disabled={activeStep === 0}
                                                    onClick={this.handleBack}
                                                    className={classes.button}>
                                                        Atrás
                                                </Button>
                                                <Button
                                                    variant='contained'
                                                    color='primary'
                                                    onClick={this.handleNext}
                                                    className={classes.button}>
                                                        {activeStep === steps.length - 1
                                                            ? 'Finalizar'
                                                            : 'Siguiente'}
                                                </Button>
                                            </div>
                                        </div>
                                    </StepContent>
                                </Step>
                            ))}
                        </Stepper>
                        {
                            activeStep === -1 && (
                                <Paper square elevation={0} className={classes.resetContainer}>
                                    <PreLoaderImage />
                                </Paper>
                            )
                        }
                        {
                            activeStep === steps.length && (
                                <Paper square elevation={0} className={classes.resetContainer}>
                                    <SnackbarProvider maxSnack={3}>
                                        <BackgroundFinish callCenter />
                                    </SnackbarProvider>
                                    <br />
                                    <Button
                                        variant='contained'
                                        color='primary'
                                        onClick={this.handleReset}
                                        className={classes.button} >
                                            Nueva Originación
                                    </Button>
                                </Paper>
                            )
                        }
                        <Dialog
                            onClose={this.handleCloseModalCancelRegular}
                            open={this.state.openModal}
                            maxWidth='xs'
                            aria-labelledby='form-dialog'>
                                <DialogTitle id='form-dialog' className='bg-metal-blue'>
                                    <Typography
                                        align='center'
                                        component='span'
                                        variant='h6'
                                        className='text-white text-shadow-black'>
                                            Cancelar Proceso de Originación
                                    </Typography>
                                </DialogTitle>
                                <form
                                    onSubmit={this.handleSubmitCancelRegular}
                                    autoComplete='off'>
                                        <DialogContent>
                                            <Typography align='center'>
                                                ¿Esta seguro de cancelar la originación?
                                            </Typography>
                                        </DialogContent>
                                        <DialogActions>
                                            <Grid container spacing={8}>
                                                <Grid item xs={6}>
                                                    <div className={classes.buttonProgressWrapper}>
                                                        <Button
                                                            disabled={odcRegularActivity.cancelActivity.loading}
                                                            className={classes.button}
                                                            fullWidth={true}
                                                            color='secondary'
                                                            size='small'
                                                            onClick={this.handleCloseModalCancelRegular}
                                                            margin='normal'>
                                                                No
                                                        </Button>
                                                    </div>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <div className={classes.buttonProgressWrapper}>
                                                        <Button
                                                            disabled={odcRegularActivity.cancelActivity.loading}
                                                            className={classes.button}
                                                            type='submit'
                                                            margin='normal'
                                                            color='primary'
                                                            size='small'
                                                            fullWidth={true}>
                                                                Si
                                                                <SendIcon fontSize='small' className='ml-2' />
                                                                    {odcRegularActivity.cancelActivity.loading && (
                                                                        <CircularProgress
                                                                            size={24}
                                                                            className={classes.buttonProgress}
                                                                />
                                                            )}
                                                        </Button>
                                                    </div>
                                                </Grid>
                                            </Grid>
                                        </DialogActions>
                                </form>
                        </Dialog>
                    </div>
            </Hotkeys>
        )
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(RegularCallCenterPage)))