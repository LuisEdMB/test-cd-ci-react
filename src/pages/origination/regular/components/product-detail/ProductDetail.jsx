import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import CreditCard from '../../../../../components/CreditCard';
import NotificationSolicitudeBlockedModal from '../../../../../components/modals/NotificationSolicitudeBlockedModal';
// Colors
import green from '@material-ui/core/colors/green';
import yellow from '@material-ui/core/colors/yellow';
import red from '@material-ui/core/colors/red';
// Icons 
import LabelIcon from '@material-ui/icons/Label';
import CancelIcon from '@material-ui/icons/Cancel'; 
import CloseIcon from '@material-ui/icons/Close'; 
import SendIcon from '@material-ui/icons/Send'; 
// Effects
import Zoom from 'react-reveal/Zoom';
import Bounce from 'react-reveal/Bounce';
// Utils 
import { getMoneyFormat, resolveStateRedux } from '../../../../../utils/Utils';
// Constants
import * as Constants from '../../../../../utils/Constants'
// Actions 
import { getCreditCardColor } from '../../../../../actions/value-list/credit-card-color';
import { getPCT } from '../../../../../actions/generic/pct';
import * as ActionOdc from '../../../../../actions/odc-express/odc'

const styles =  theme => ({
    root:{
        maxWidth:900
    },
    button: {
        textTransform: 'none',
    },
    contingencyFabWrapper:{
        overflow:"initial",
        [theme.breakpoints.up('md')]: {
            position:"absolute", 
            bottom:20 ,
            right:20
        }
    },
    contingencyfab: {
        backgroundColor: yellow[800],
        transition:".3s",
        '&:hover': {
            backgroundColor: yellow[900],
        },
    },
    wrapper: {
        position: 'relative',
    }, 
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    cancelButtonProgress:{
        color: red[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    textWhite:{
        color:"white"
    }, 
    originationButton:{
        backgroundColor: green[700],
        color:"white",
        transition:".3s",
        '&:hover': {
            backgroundColor: green[800],
        },
    },
    label:{
        fontSize:14*0.75,
    },
    labelValue:{
        padding:"6px 0"
    }
});

const mapStateToProps = (state) => {
    return {
        creditCardColor: state.creditCardColorReducer, 
        pct: state.pctReducer,
        odc: state.odcReducer,
        pci: state.pciReducer,
        odcMaster: state.odcMasterReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const actions = {
        getCreditCardColor: bindActionCreators(getCreditCardColor, dispatch), 
        getPCT: bindActionCreators(getPCT, dispatch),
        unlockSolicitude: bindActionCreators(ActionOdc.unlockSolicitude, dispatch)
    };
    return actions;
}

class ProductDetail extends Component {
    state = {
        countConnectionZytrust: 1,
        client: {
            code:0,
            id: 0,
            documentTypeId: 100001,
            documentType: "DNI",
            documentTypeInternalValue:"DU",
            documentNumber:"",
            firstName:"", 
            lastName:"", 
            gender:"",
            maritalStatusInternalValue:"",
            nationality:"",
            email:"", 
            cellphone:"",
            birthday:"",
            businessName:"",
            names:"", 
            surnames:"",
            shortName:"",
            fullName:"", 
            telephone:"(   )    -   ",
            homeAddress:{
                department:"",
                departmentId:"",
                provinceId:"",
                districtId:"",
                via:"", 
                nameVia:"",
                number:"",
                building:"",
                inside:"",
                mz:"",
                lot:"",
                zone:"", 
                nameZone:"", 
                reference:""
            },
            workAddress:{
                department:"",
                departmentId:"",
                provinceId:"",
                districtId:"",
                via:"", 
                nameVia:"",
                number:"",
                building:"",
                inside:"",
                mz:"",
                lot:"",
                zone:"", 
                nameZone:"", 
                reference:""
            },
            jobTitle:"",
            workTelephone: "(   )    -   ",
            extra:{
                effectiveWithdrawal:"", 
                paymentDateInternalValue:"", 
            }
        },
        pmp:{
            clientNumber:"",
            relationshipNumber:"", 
            accountNumber:"", 
            cardNumber:""
        },
        flowPhaseState:-1,
        creditCard: {
            type: "Clásica", 
            brandId: 200001,
            brand: "MasterCard",
            colorId: "",
            colorAux: "",
            color:"",
            productId:"",
            bin: "", 
            name: "CENCOSUD MASTERCARD",
            colorDisabled: false, 
            colorError: false
        }, 
        lineAvailable: {
            value:1000 
        }, 
        lineSAE:{
            value: 0
        },
        maxAmount: 0,
        isSAE: true,
        pctSAE: "",
        pct: null,
        segment: "",
        numero_cuotas: 0,
        monto_cuota: 0,
        tcea: 0,
        effectiveProvision: 77.7,
        readOnly:true, 
        cancelButtonDisabled:false,
        generationClientButtonDisabled:false,
        showNotificationSolicitudeBlockedModal: false,
        isCancelProcess: false
    }
    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    componentWillMount(){
        // Get Props
        let { 
            client, creditCard, lineAvailable, effectiveProvision, 
            segment, pmp, flowPhaseState,
            lineSAE, maxAmount, pctSAE, isSAE, monto_cuota,
            numero_cuotas, tcea
        } = this.props;


        if(client && creditCard) {
            this.setState(state => ({
                ...state, 
                client: {
                    ...state.client,
                    ...client
                }, 
                creditCard:{
                    ...state.creditCard,
                    ...creditCard
                }, 
                lineAvailable: {
                    ...state.lineAvailable,
                    value:lineAvailable
                }, 
                lineSAE:{
                    ...state.lineSAE,
                    value: lineSAE
                },
                maxAmount: maxAmount,
                pctSAE: pctSAE,
                isSAE: isSAE,
                monto_cuota: monto_cuota,
                numero_cuotas, tcea,
                effectiveProvision: effectiveProvision, 
                segment: segment, 
                pmp:{
                    ...pmp
                }, 
                flowPhaseState:flowPhaseState
            }));
        }
    }
    componentDidMount(){
        if(this.state.segment !== ""){
            this.props.getPCT(this.state.lineAvailable.value, this.state.effectiveProvision, this.state.segment);
        }
        // Get Service 
        this.props.getCreditCardColor();
    }

    componentDidUpdate = (prevProps) => {
        // PCT
        if(prevProps.pct !== this.props.pct){
            if(this.props.pct.loading){
                this.setState(state => ({ 
                    ...state,
                    cancelButtonDisabled: true,
                    generationClientButtonDisabled: true
                }));
            }
            if(!this.props.pct.loading && this.props.pct.response && this.props.pct.success){
                this.setState(state => ({
                    ...state,
                    pct:{
                        number: this.props.pct.data? this.props.pct.data.num_pct : ""
                    }
                }));
                this.getNotistack(`PCT: Calculado.`, "info");
                this.setState(state => ({ 
                    ...state,
                    cancelButtonDisabled:false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.pct.loading && this.props.pct.response && !this.props.pct.success){
                // Notistack
                this.getNotistack(`PCT: ${this.props.pct.error}`, "error");
                this.setState(state => ({ 
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: true
                }));
            }
            else if(!this.props.pct.loading && !this.props.pct.response && !this.props.pct.success){
                // Notistack
                this.getNotistack(`PCT: ${this.props.pct.error}`, "error");
                this.setState(state => ({ 
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: true
                }));
            }
        }
        // Activity - Cancel
        resolveStateRedux(prevProps.cancelActivity, this.props.cancelActivity, null,
            _ => {
                this.setState(state => ({
                    ...state,
                    isCancelProcess: true
                }))
                this.props.unlockSolicitude({
                    solicitudeCode: this.props.origination.number,
                    completeSolicitudeCode: this.props.origination.fullNumber,
                    clientCode: this.props.client.id,
                    documentTypeId: this.props.client.documentTypeAux,
                    documentNumber: this.props.client.documentNumber
                })
            },
            warningMessage => {
                this.getNotistack(warningMessage, "warning")
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: false
                }))
            },
            errorMessage => {
                this.getNotistack(errorMessage, "error")
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: false
                }))
            }
        )
        // Block Solicitude
        resolveStateRedux(prevProps.odc.solicitudeBlocked, this.props.odc.solicitudeBlocked,
            _ => this.setState(state => ({
                ...state,
                cancelButtonDisabled: true,
                generationClientButtonDisabled: true
            })),
            _ => this.getNotistack(`Originación Regular: Bloqueado`, 'info'),
            (warningMessage, errorCode) => {
                if (errorCode === Constants.CustomErrorCode.errorCodeSolicitudeBlocked ||
                    errorCode === Constants.CustomErrorCode.errorCodeIndebtedness) {
                    this.setState(state => ({
                        ...state,
                        showNotificationSolicitudeBlockedModal: true
                    }))
                }
                else {
                    this.getNotistack(warningMessage, "warning")
                    this.setState(state => ({
                        ...state,
                        cancelButtonDisabled: false,
                        generationClientButtonDisabled: false
                    }))
                }
            },
            errorMessage => {
                this.getNotistack(errorMessage, "error")
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: false
                }))
            }
        )
        // Unlock Solicitude
        resolveStateRedux(prevProps.odc.solicitudeUnlocked, this.props.odc.solicitudeUnlocked,
            _ => this.setState(state => ({
                ...state,
                cancelButtonDisabled: true,
                generationClientButtonDisabled: true
            })),
            _ => {
                if (this.state.isCancelProcess) this.props.handleCancelRegular()
                else {
                    this.getNotistack('Originación Regular: Desbloqueado', 'info');
                    setTimeout(() => {
                        this.getNotistack('Consulta Correcta, 5to Paso Ok!', 'success');
                    }, 1000)
                    setTimeout(() => {
                        this.props.handleNext();
                    }, 2500)
                }
            },
            warningMessage => {
                this.getNotistack(warningMessage, "warning")
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: false
                }))
            },
            errorMessage => {
                this.getNotistack(errorMessage, "error")
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: false
                }))
            }
        )
        // Activity - Generic
        if(prevProps.genericActivity !==  this.props.genericActivity ){
            if(this.props.genericActivity.loading){
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: true,
                    generationClientButtonDisabled:true
                }));
            }
            if(!this.props.genericActivity.loading && this.props.genericActivity.response && this.props.genericActivity.success){
                let { next } = this.props.genericActivity;
                if(next){
                    this.getNotistack("Originación Regular: Aceptado", "info");
                }
                else{
                    this.getNotistack("Originación Regular: En Proceso", "info");
                }
            }
            else if(!this.props.genericActivity.loading && this.props.genericActivity.response && !this.props.genericActivity.success){
                // Notistack
                this.getNotistack(this.props.genericActivity.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.genericActivity.loading && !this.props.genericActivity.response && !this.props.genericActivity.success){
                // Notistack
                this.getNotistack(this.props.genericActivity.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
        }
        // PMP - Client 
        if(prevProps.createClient !==  this.props.createClient ){
            if(this.props.createClient.loading){
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: true,
                    generationClientButtonDisabled:true
                }));
            }
            if(!this.props.createClient.loading && this.props.createClient.response && this.props.createClient.success){
                this.getNotistack("Originación Regular: PMP Crear Cliente Ok!", "success");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.createClient.loading && this.props.createClient.response && !this.props.createClient.success){
                // Notistack
                this.getNotistack(this.props.createClient.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.createClient.loading && !this.props.createClient.response && !this.props.createClient.success){
                // Notistack
                this.getNotistack(this.props.createClient.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
        }
        // PMP - Update Client 
        if(prevProps.updateClient !==  this.props.updateClient ){
            if(this.props.updateClient.loading){
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: true,
                    generationClientButtonDisabled:true
                }));
            }
            if(!this.props.updateClient.loading && this.props.updateClient.response && this.props.updateClient.success){
                this.getNotistack("Originación Regular: PMP Actualizar Cliente Ok!", "success");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.updateClient.loading && this.props.updateClient.response && !this.props.updateClient.success){
                // Notistack
                this.getNotistack(this.props.updateClient.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.updateClient.loading && !this.props.updateClient.response && !this.props.updateClient.success){
                // Notistack
                this.getNotistack(this.props.updateClient.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
        }
        // PMP - Relationship 
        if(prevProps.createRelationship !==  this.props.createRelationship ){
            if(this.props.createRelationship.loading){
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: true,
                    generationClientButtonDisabled:true
                }));
            }
            if(!this.props.createRelationship.loading && this.props.createRelationship.response && this.props.createRelationship.success){
                this.getNotistack("Originación Regular: PMP Crear Relación Ok!", "success");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.createRelationship.loading && this.props.createRelationship.response && !this.props.createRelationship.success){
                // Notistack
                this.getNotistack(this.props.createRelationship.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.createRelationship.loading && !this.props.createRelationship.response && !this.props.createRelationship.success){
                // Notistack
                this.getNotistack(this.props.createRelationship.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
        }
        // PMP - Account
        if(prevProps.createAccount !==  this.props.createAccount ){
            if(this.props.createAccount.loading){
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: true,
                    generationClientButtonDisabled:true
                }));
            }
            if(!this.props.createAccount.loading && this.props.createAccount.response && this.props.createAccount.success){
                this.getNotistack("Originación Regular: PMP Crear Cuenta Ok!", "success");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.createAccount.loading && this.props.createAccount.response && !this.props.createAccount.success){
                // Notistack
                this.getNotistack(this.props.createAccount.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.createAccount.loading && !this.props.createAccount.response && !this.props.createAccount.success){
                // Notistack
                this.getNotistack(this.props.createAccount.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
        }
        // PMP - Credit Card
        if(prevProps.createCreditCard !==  this.props.createCreditCard ){
            if(this.props.createCreditCard.loading){
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: true,
                    generationClientButtonDisabled:true
                }));
            }
            if(!this.props.createCreditCard.loading && this.props.createCreditCard.response && this.props.createCreditCard.success){
                this.getNotistack("Originación Regular: PMP Crear Tarjeta Ok!", "success");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.createCreditCard.loading && this.props.createCreditCard.response && !this.props.createCreditCard.success){
                // Notistack
                this.getNotistack(this.props.createCreditCard.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.createCreditCard.loading && !this.props.createCreditCard.response && !this.props.createCreditCard.success){
                // Notistack
                this.getNotistack(this.props.createCreditCard.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
        }
        if(prevProps.blockTypeCreditCard !==  this.props.blockTypeCreditCard ){
            if(this.props.blockTypeCreditCard.loading){
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: true,
                    generationClientButtonDisabled:true
                }));
            }
            if(!this.props.blockTypeCreditCard.loading && this.props.blockTypeCreditCard.response && this.props.blockTypeCreditCard.success){
                this.getNotistack("Originación Regular: PMP Activar TC Ok!", "success");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.blockTypeCreditCard.loading && this.props.blockTypeCreditCard.response && !this.props.blockTypeCreditCard.success){
                // Notistack
                this.getNotistack(this.props.blockTypeCreditCard.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
            else if(!this.props.blockTypeCreditCard.loading && !this.props.blockTypeCreditCard.response && !this.props.blockTypeCreditCard.success){
                // Notistack
                this.getNotistack(this.props.blockTypeCreditCard.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled:false
                }));
            }
        }
        resolveStateRedux(prevProps.pci.cardnumberEncrypted, this.props.pci.cardnumberEncrypted,
            _ => this.setState(state => ({
                ...state,
                cancelButtonDisabled: true,
                generationClientButtonDisabled: true
            })),
            _ => this.getNotistack('Originación Regular: PCI Tarjeta Ok!', 'success'),
            warningMessage => {
                this.getNotistack(`Originación Regular: PCI - ${ warningMessage }`, 'warning')
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: false
                }))
            },
            errorMessage => {
                this.getNotistack(`Originación Regular: PCI - ${ errorMessage }`, 'error')
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: false
                }))
            }
        )
        resolveStateRedux(prevProps.odcMaster.accountRegistered, this.props.odcMaster.accountRegistered,
            _ => this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: true,
                    generationClientButtonDisabled: true
            })),
            _ => this.getNotistack(`Originación Regular: Maestro Cuentas Ok!`, 'success'),
            warningMessage => {
                this.getNotistack(`Originación Regular: Maestro Cuentas - ${ warningMessage }`, 'warning')
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: false
                }))
            },
            errorMessage => {
                this.getNotistack(`Originación Regular: Maestro Cuentas - ${ errorMessage }`, 'error')
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: false
                }))
            }
        )
        resolveStateRedux(prevProps.odcMaster.creditCardRegistered, this.props.odcMaster.creditCardRegistered,
            _ => this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: true,
                    generationClientButtonDisabled: true
            })),
            _ => this.getNotistack(`Originación Regular: Maestro Tarjetas Ok!`, 'success'),
            warningMessage => {
                this.getNotistack(`Originación Regular: Maestro Tarjetas - ${ warningMessage }`, 'warning')
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: false
                }))
            },
            errorMessage => {
                this.getNotistack(`Originación Regular: Maestro Tarjetas - ${ errorMessage }`, 'error')
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    generationClientButtonDisabled: false
                }))
            }
        )
    }
    // Client - Document Type - Event Change TextField 
    handleChangeTextFieldDocumentType = e => {
        this.setState(state => ({
            ...state,
            client:{
                ...state.client, 
                documentType: e.target.value
            }
        }));
    }
    // Client - Document Number - Event Change TextField 
    handleChangeTextFieldDocumentNumber = e => {
        this.setState(state => ({
            ...state,
            client:{
                ...state.client, 
                documentNumber: e.target.value
            }
        }));
    }
    // Client - Names - Event Change TextField 
    handleChangeTextFieldFullName = e => {
        this.setState(state => ({
            ...state,
            client:{
                ...state.client, 
                fullName: e.target.value
            }
        }));
    }
    // Client - Surnames - Event Change TextField 
    handleChangeTextFieldSurnames = e => {
        this.setState(state => ({
            ...state,
            client:{
                ...state.client, 
                surnames: e.target.value
            }
        }));
    }
    // CreditCard - Brand - Event Change Select 
    handleChangeTextFieldCreditCardBrand = e => {
        this.setState(state => ({
            ...state,
            creditCard:{
                ...state.creditCard, 
                brandId: e.target.value
            }
        }));
    }
    // CreditCard - Color - Event Change Select
    handleChangeSelectCreditCardColor = e => {
        let object = this.props.creditCardColor.data.find(option => option.cod_valor === e.target.value);
        this.setState(state => ({
            ...state,
            creditCard:{
                ...state.creditCard,
                color: object.des_auxiliar, 
                colorId: e.target.value
            }
        }));
    }
    // CreditCard - Effective Provision - Event Change TextField 
    handleChangeTextFieldEffectiveProvision = e => {
        this.setState(state =>({
            ...state,
            effectiveProvision:e.target.value
        }));
    }

    // Send - Event Submit  
    handleSubmitProductDetailForm = e => {
        e.preventDefault();
        let { pct } = this.state;
        let sendData = {
            pct: pct
        }
        this.props.handleProductDetail(sendData);
        
    }

    // Submit - Origination
    handleCancelRegular = e => {
        e.preventDefault();
        this.props.handleOpenModalCancelRegular();
    }
    
    render(){
        let { classes, creditCardColor } = this.props;
        let { creditCard, client, readOnly, showNotificationSolicitudeBlockedModal } = this.state;
        let showColor = creditCard.productId !== 10008
        return (
            <>
                <Grid container spacing={8}>
                    <form method="post" className="w-100 p-1" onSubmit={this.handleSubmitProductDetailForm} >
                        {/* Form - CreditCard */}
                        <Grid container spacing={8}>
                            <Grid 
                                item xs={12}
                                style={{position:"relative"}} 
                                className={classNames("d-flex justify-content-center p-2 pb-4 p-sm-5 mb-2  rounded")}> 
                                <Grid container spacing={32} className={classNames(classes.root, "m-0 m-sm-3")}>
                                    {/* Form - Data */}
                                    <Grid item xs={12} sm={12} md={6} lg={7}>
                                        <Grid container spacing={8}>
                                            {/* Document Type */}
                                            <Grid item xs={6}>
                                                <TextField
                                                    fullWidth={true}
                                                    required
                                                    label="Tipo Documento"
                                                    type="text"
                                                    margin="normal"
                                                    variant="outlined"
                                                    color="default"
                                                    value={client.documentType}
                                                    onChange={this.handleChangeTextFieldDocumentType}
                                                    autoFocus
                                                    InputProps={{
                                                        readOnly: readOnly,
                                                    }}
                                                />
                                            </Grid>
                                            {/* Document Number */}
                                            <Grid item xs={6}>
                                                <TextField
                                                    fullWidth={true}
                                                    required
                                                    label="Número Documento"
                                                    type="text"
                                                    margin="normal"
                                                    variant="outlined"
                                                    color="default"
                                                    value={client.documentNumber}
                                                    onChange={this.handleChangeTextFieldDocumentNumber}
                                                    autoFocus
                                                    InputProps={{
                                                        readOnly: readOnly,
                                                    }}
                                                />
                                                    
                                            </Grid>
                                            {/* FullName */}
                                            <Grid item xs={12}>
                                                <TextField
                                                    fullWidth={true}
                                                    required
                                                    label="Nombre Completo"
                                                    type="text"
                                                    margin="normal"
                                                    variant="outlined"
                                                    color="default"
                                                    value={client.fullName? client.fullName: ""}
                                                    onChange={this.handleChangeTextFieldFullName}
                                                    InputProps={{
                                                        readOnly: readOnly,
                                                    }}
                                                />
                                            </Grid>          
                                            {/* Effective Provision */}              
                                            <Grid item xs={12} sm={6} md={6} lg={showColor ? 3 : 4}>
                                                <TextField
                                                    fullWidth={true}
                                                    required
                                                    label="Disp. Efectivo"
                                                    type="text"
                                                    margin="normal"
                                                    variant="outlined"
                                                    color="default"
                                                    value={`${this.state.effectiveProvision.toFixed(2)}%`}
                                                    onChange={this.handleChangeTextFieldEffectiveProvision}
                                                    inputProps={{
                                                        style: { textAlign: "right" }, 
                                                        readOnly: readOnly
                                                    }}
                                                />
                                            </Grid>
                                            {/* Color */}
                                            {showColor && <Grid item xs={12} sm={6} md={6} lg={3}>
                                                {
                                                creditCard.colorAux !== ""? 
                                                <FormControl 
                                                    required
                                                    disabled={creditCard.colorDisabled}
                                                    margin="normal"
                                                    variant="outlined"
                                                    fullWidth={true}>
                                                    <InputLabel htmlFor="color">Color Tarjeta</InputLabel>
                                                    <Select
                                                        value={creditCard.colorId}
                                                        input={
                                                            <OutlinedInput
                                                                readOnly = { readOnly }
                                                                labelWidth={95}
                                                                name="color"
                                                                id="color"
                                                            />
                                                        }
                                                        onChange={this.handleChangeSelectCreditCardColor}>
                                                        {
                                                            creditCardColor.data.map((item, index) => {
                                                                return (
                                                                    <MenuItem key={index} value={item.cod_valor} className="d-flex justify-content-between">
                                                                        <LabelIcon style={{ color:item.des_auxiliar, fontSize:"12px"}}/>
                                                                        <span className="px-2"> {item.des_valor} </span>
                                                                    </MenuItem> 
                                                                )
                                                            })
                                                        }
                                                    </Select>
                                                </FormControl> : 
                                                    <TextField 
                                                        label="Color Tarjeta"
                                                        fullWidth={true}
                                                        margin="normal"
                                                        variant="outlined"
                                                        value="No Aplica"
                                                        InputProps={{
                                                            readOnly: readOnly
                                                        }}
                                                    />
                                                }
                                            </Grid>
                                            }
                                            {/* Brand */}
                                            <Grid item xs={12} sm={6} md={6} lg={showColor ? 3 : 4}>
                                                <TextField
                                                    fullWidth={true}
                                                    required
                                                    label="Marca Tarjeta"
                                                    type="text"
                                                    margin="normal"
                                                    variant="outlined"
                                                    color="default"
                                                    value={creditCard.brandId === 200001 ? "MasterCard":"Visa"}
                                                    onChange={this.handleChangeTextFieldCreditCardBrand}
                                                    InputProps={{
                                                        readOnly: readOnly
                                                    }}
                                                />
                                                
                                            </Grid>
                                            {/* Line Available */}
                                            <Grid item xs={12} sm={6} md={6} lg={showColor ? 3 : 4}>
                                                <TextField
                                                    fullWidth={true}
                                                    required
                                                    label="Línea de Crédito"
                                                    type="text"
                                                    margin="normal"
                                                    variant="outlined"
                                                    color="default"
                                                    value={`S/ ${getMoneyFormat(this.state.lineAvailable.value.toString())}`}
                                                    inputProps={{
                                                        style: { textAlign: "right" }, 
                                                        readOnly: readOnly
                                                    }}
                                                />
                                            </Grid>
                                            {
                                                this.state.isSAE && 
                                                <>
                                                    {/* Line SAE */}
                                                    <Grid item xs={12} sm={6} md={6} lg={4}>
                                                        <TextField
                                                            fullWidth={true}
                                                            required
                                                            label="Línea de SAE"
                                                            type="text"
                                                            margin="normal"
                                                            variant="outlined"
                                                            color="default"
                                                            value={`S/ ${getMoneyFormat(this.state.lineSAE.value.toString())}`}
                                                            inputProps={{
                                                                style: { textAlign: "right" }, 
                                                                readOnly: readOnly
                                                            }}
                                                        />
                                                    </Grid>
                                                    {/* Max Amount */}
                                                    <Grid item xs={12} sm={6} md={6} lg={4}>
                                                        <TextField
                                                            fullWidth={true}
                                                            required
                                                            label="Línea Global"
                                                            type="text"
                                                            margin="normal"
                                                            variant="outlined"
                                                            color="default"
                                                            value={`S/ ${getMoneyFormat(this.state.maxAmount.toString())}`}
                                                            InputProps={{
                                                                readOnly: readOnly
                                                            }}
                                                        />
                                                    </Grid>
                                                    {/* PCT SAE */}
                                                    <Grid item xs={12} sm={6} md={6} lg={4}>
                                                        <TextField
                                                            fullWidth={true}
                                                            required
                                                            label="PCT SAE"
                                                            type="text"
                                                            margin="normal"
                                                            variant="outlined"
                                                            color="default"
                                                            value={this.state.pctSAE? this.state.pctSAE: "No Aplica"}
                                                            InputProps={{
                                                                readOnly: readOnly
                                                            }}
                                                        />
                                                    </Grid>
                                                    <Grid item xs={12} sm={6} md={6} lg={4}>
                                                        <TextField
                                                            fullWidth={true}
                                                            required
                                                            label="Número de Cuotas"
                                                            type="text"
                                                            margin="normal"
                                                            variant="outlined"
                                                            color="default"
                                                            value={this.state.numero_cuotas}
                                                            InputProps={{
                                                                readOnly: readOnly
                                                            }}
                                                        />
                                                    </Grid>
                                                    <Grid item xs={12} sm={6} md={6} lg={4}>
                                                        <TextField
                                                            fullWidth={true}
                                                            required
                                                            label="Valor aprox. Cuotas"
                                                            type="text"
                                                            margin="normal"
                                                            variant="outlined"
                                                            color="default"
                                                            value={`S/ ${getMoneyFormat(this.state.monto_cuota.toString())}`}
                                                            InputProps={{
                                                                readOnly: readOnly
                                                            }}
                                                        />
                                                    </Grid>
                                                    <Grid item xs={12} sm={6} md={6} lg={4}>
                                                        <TextField
                                                            fullWidth={true}
                                                            required
                                                            label="TCEA"
                                                            type="text"
                                                            margin="normal"
                                                            variant="outlined"
                                                            color="default"
                                                            value={`${parseFloat(this.state.tcea).toFixed(2)} %`}
                                                            InputProps={{
                                                                readOnly: readOnly
                                                            }}
                                                        />
                                                    </Grid>
                                                </>
                                            }
                                    </Grid> 
                                    </Grid>
                                    {/* Form - CreditCard */}
                                    <Grid item xs={12} sm={12} md={6} lg={5}>
                                        <Grid container spacing={8} >
                                            {/* Credit Card */}
                                            <Grid item xs={12} className="d-flex justify-content-center">
                                                <CreditCard 
                                                    client={client.shortName}
                                                    type={creditCard.type} 
                                                    customColor={ creditCard.colorAux }
                                                    image={creditCard.productId !== 10008}
                                                    bin={creditCard.bin}
                                                    brand={creditCard.brandId === 200001? 0 : 1} />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        {/* Form - Button */}
                        <Grid container spacing={8} className="d-flex justify-content-center align-items-center">
                            {/* Button - Cancel */}
                            <Grid item xs={12} sm={12} md={6} lg={3}>
                                <Tooltip TransitionComponent={Zoom} title="Cancelar Originación.">
                                    <div>
                                        <Bounce>
                                            <div className={classNames(classes.wrapper)}>
                                                <Button 
                                                    disabled={this.state.cancelButtonDisabled}
                                                    className={classes.button}
                                                    type="button" 
                                                    variant="contained" 
                                                    size="small"
                                                    margin="normal"
                                                    color="secondary" 
                                                    onClick={this.handleCancelRegular}
                                                    fullWidth={true}>
                                                    Cancelar
                                                    <CancelIcon fontSize="small" className="ml-2" />
                                                </Button>
                                            </div>
                                        </Bounce>
                                    </div>
                                </Tooltip>
                            </Grid>
                            {/* Button - Generation */}
                            <Grid item xs={12} sm={12} md={6} lg={3}>
                                <Tooltip TransitionComponent={Zoom} title="Generar una nueva tarjeta.">
                                    <div>
                                        <Bounce>
                                            <div className={classNames(classes.wrapper)}>
                                                <Button
                                                    disabled={this.state.generationClientButtonDisabled}
                                                    variant="contained"
                                                    type="submit" 
                                                    className={classNames(classes.button)}
                                                    color="primary"
                                                    fullWidth={true}
                                                    size="small">
                                                    Generar Tarjeta
                                                    <SendIcon fontSize="small" className="ml-2" />
                                                    {   this.state.generationClientButtonDisabled &&
                                                        <CircularProgress size={24} className={classes.buttonProgress} />
                                                    }
                                                </Button>
                                            </div>
                                        </Bounce>
                                    </div>
                                </Tooltip>
                            </Grid>
                        </Grid>     
                    </form>   
                </Grid>
                <NotificationSolicitudeBlockedModal
                    text={ this.props.odc.solicitudeBlocked.error }
                    open={ showNotificationSolicitudeBlockedModal }
                    onClickOk={ this.props.handleCancelRegular } />
            </>
        )
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ProductDetail)));

