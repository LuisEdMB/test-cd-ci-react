// React
import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import * as Moment from "moment";
import { extendMoment } from "moment-range";
import { withStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";
// React Router
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// Components
import {
  Button,
  CircularProgress, Divider,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  LinearProgress,
  MenuItem,
  Select,
  TextField,
  Typography
} from "@material-ui/core";
import green from "@material-ui/core/colors/green";
import Autocomplete from "../../../../../components/Autocomplete";
// Icons
import CloseIcon from "@material-ui/icons/Close";
import RecentActorsIcon from "@material-ui/icons/RecentActors";
import SendIcon from "@material-ui/icons/Send";
import CancelIcon from "@material-ui/icons/Cancel";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import WorkIcon from "@material-ui/icons/Work";
// Effects
import Fade from "react-reveal/Fade";
import Bounce from "react-reveal/Bounce";
// Actions
import { getAcademicDegree } from "../../../../../actions/value-list/academic-degree";
import { getIdentificationDocumentType } from "../../../../../actions/value-list/identification-document-type";
import { getJobTitle } from "../../../../../actions/value-list/job-title";
import { getMaritalStatus } from "../../../../../actions/value-list/marital-status";
import { getEconomicActivity } from "../../../../../actions/value-list/economic-activity";
import { getConstantODC } from "../../../../../actions/generic/constant";
import { getEmploymentSituation } from "../../../../../actions/value-list/employment-situation";
import * as ActionOdcMaster from '../../../../../actions/odc-master/odc-master'
// Utils
import * as MasterOrigination from '../../../../../utils/origination/MasterOrigination'
import * as Utils from '../../../../../utils/Utils'
import * as Constants from '../../../../../utils/Constants'

const moment = extendMoment(Moment);

const styles = (theme) => ({
  root: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      justifyContent: "center",
    },
  },
  heading: {
    display: "flex",
    textTransform: "uppercase",
  },
  buttonWrapper: {
    display: "flex",
    justifyContent: "center",
  },
  button: {
    textTransform: "none",
  },
  wrapper: {
    position: "relative",
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
    textTransform: "none",
  },
  documentNumberFormHelperText: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  fontSizeError: {
    fontSize: "0.75rem",
  },
});

const mapStateToProps = state => {
  return {
    idenfiticationDocumentType: state.identificationDocumentTypeReducer,
    maritalStatus: state.maritalStatusReducer,
    academicDegree: state.academicDegreeReducer,
    jobTitle: state.jobTitleReducer,
    constantODC: state.constantODCReducer,
    employmentSituation: state.employmentSituationReducer,
    economicActivity: state.economicActivityReducer,
    odc: state.odcReducer
  };
};

const mapDispatchToProps = dispatch => {
  const actions = {
    getIdentificationDocumentType: bindActionCreators(
      getIdentificationDocumentType,
      dispatch
    ),
    getEmploymentSituation: bindActionCreators(
      getEmploymentSituation,
      dispatch
    ),
    getMaritalStatus: bindActionCreators(getMaritalStatus, dispatch),
    getAcademicDegree: bindActionCreators(getAcademicDegree, dispatch),
    getJobTitle: bindActionCreators(getJobTitle, dispatch),
    getEconomicActivity: bindActionCreators(getEconomicActivity, dispatch),
    getConstantODC: bindActionCreators(getConstantODC, dispatch),
    preloadClientMaster: bindActionCreators(ActionOdcMaster.preloadClient, dispatch)
  };
  return actions;
};

class ConsultClient extends Component {
  state = {
    client: {
      documentTypeId: 100001,
      documentTypeAux: "1",
      documentTypeInternalValue: "DU",
      documentType: "DNI",
      documentTypeSelectDisabled: true,
      documentNumber: "",
      documentNumberOk: false,
      documentNumberError: false,
      validateClientButtonDisabled: false,

      maritalStatus: "",
      maritalStatusId: "",
      maritalStatusInternalValue: "",
      maritalStatusError: false,
      maritalStatusDisabled: true,

      academicDegree: "",
      academicDegreeId: "",
      academicDegreeInternalValue: "",
      academicDegreeError: false,
      academicDegreeDisabled: true,

      employmentSituation: "",
      employmentSituationId: "",
      employmentSituationInternalValue: "",
      employmentSituationError: false,
      employmentSituationDisabled: true,

      jobTitle: "",
      jobTitleId: "",
      jobTitleError: false,
      jobTitleDisabled: true,

      economicActivity: "",
      economicActivityId: "",
      economicActivityError: false,
      economicActivityDisabled: true,

      birthday: "1900-01-01",
      birthdayError: false,

      laborIncomeDate: moment().format("YYYY-MM-DD"),
      laborIncomeDateError: false,
    },

    maxBirthday: 0,
    minBirthday: 0,
    defaultMaxBirthday: "",
    defaultMinBirthday: "",

    maxLaborIncomeDate: 0,
    minLaborIncomeDate: 0,
    defaultMaxLaborIncomeDate: "",
    defaultMinLaborIncomeDate: "",

    loading: true
  };

  componentDidMount() {
    Promise.all([
      this.props.getConstantODC(),
      this.props.getIdentificationDocumentType(),
      this.props.getMaritalStatus(),
      this.props.getAcademicDegree(),
      this.props.getEmploymentSituation(),
      this.props.getEconomicActivity()
    ]).then((result) => {
      if (result) {
        const { origination } = this.props.odc?.firstCall?.data || { }
        const senDataPreloadClient = {
          solicitudeCode: origination.number,
          completeSolicitudeCode: origination.fullNumber,
          documentType: this.state.client.documentTypeAux,
          documentNumber: this.props.client.documentNumber,
          activityName: MasterOrigination.preloadClientActivity.activityName,
          phaseCode: MasterOrigination.preloadClientActivity.phaseCode,
          masterStageCode: Constants.MasterConfigurationStage.masterRegularStage,
          clientTypeCode: Constants.ClientTypes.holderClientCode
        }
        this.props.preloadClientMaster(senDataPreloadClient).then(response => {
          if (response?.success) {
            const { precargaCliente } = response || { }
            this.setState((state) => ({
              ...state,
              client: {
                ...state.client,
                id: precargaCliente.cod_cliente || 0,
                documentTypeAux: precargaCliente.cod_tipo_documento || '',
                documentTypeId: precargaCliente.cod_tipo_documento_2 || '',
                documentType: precargaCliente.des_tipo_documento || '',
                documentTypeInternalValue: precargaCliente.des_tipo_documento_valor_interno || '',
                documentNumber: precargaCliente.des_nro_documento || '',
                birthday: precargaCliente.fec_nacimiento?.split('T')[0] || '1900-01-01',
                maritalStatusId: Utils.verifyValueIntoArray(this.props.maritalStatus.data, 'cod_valor', precargaCliente.cod_estado_civil || 0),
                maritalStatus: precargaCliente.des_estado_civil || '',
                maritalStatusInternalValue: precargaCliente.des_estado_civil_valor_interno || '',
                academicDegreeId: Utils.verifyValueIntoArray(this.props.academicDegree.data, 'cod_valor', precargaCliente.cod_gradoacademico || 0),
                academicDegree: precargaCliente.des_gradoacademico || '',
                academicDegreeInternalValue: precargaCliente.des_gradoacademico_valor_interno ||'',
                employmentSituationId: Utils.verifyValueIntoArray(this.props.employmentSituation.data, 'cod_valor', precargaCliente.cod_situacion_laboral || 0),
                employmentSituation: precargaCliente.des_situacion_laboral || '',
                laborIncomeDate: precargaCliente.fec_ingreso_laboral?.split("T")[0] || moment().format('YYYY-MM-DD'),
                economicActivityId: Utils.verifyValueIntoArray(this.props.economicActivity.data, 'cod_valor', precargaCliente.cod_actividad_economica || 0),
                economicActivity: precargaCliente.des_actividad_economica || '',
                jobTitleId: Utils.verifyValueIntoArray(this.props.jobTitle.data, 'cod_valor', precargaCliente.cod_cargo_profesion || 0),
                jobTitle: precargaCliente.des_cargo_profesion || '',
              },
            }));
          }
          this.setState(state => ({
            ...state,
            loading: false
          }))
        })
      }
    });
  }

  componentDidUpdate = prevProps => {
    if (prevProps.client !== this.props.client) {
      this.setState((state) => ({
        ...state,
        client: {
          ...state.client,
          documentNumber: this.props.client.documentNumber,
        },
      }));
    }

    if (
      prevProps.idenfiticationDocumentType !==
      this.props.idenfiticationDocumentType
    ) {
      // Enabled Button - Validate Identity Client
      if (this.props.idenfiticationDocumentType.loading) {
        this.setState((state) => ({
          client: {
            ...state.client,
            documentTypeSelectDisabled: true,
          },
        }));
      }
      if (
        !this.props.idenfiticationDocumentType.loading &&
        this.props.idenfiticationDocumentType.response &&
        this.props.idenfiticationDocumentType.success
      ) {
        this.setState((state) => ({
          client: {
            ...state.client,
            documentTypeSelectDisabled: false,
            validateClientButtonDisabled: false,
          },
        }));
      } else if (
        !this.props.idenfiticationDocumentType.loading &&
        this.props.idenfiticationDocumentType.response &&
        !this.props.idenfiticationDocumentType.success
      ) {
        this.setState((state) => ({
          client: {
            ...state.client,
            documentTypeSelectDisabled: true,
            validateClientButtonDisabled: true,
          },
        }));
        // Notistack
        this.getNotistack(this.props.idenfiticationDocumentType.error, "error");
      } else if (
        !this.props.idenfiticationDocumentType.loading &&
        !this.props.idenfiticationDocumentType.response &&
        !this.props.idenfiticationDocumentType.success
      ) {
        this.setState((state) => ({
          client: {
            ...state.client,
            documentTypeSelectDisabled: true,
            validateClientButtonDisabled: true,
          },
        }));
        // Notistack
        this.getNotistack(this.props.idenfiticationDocumentType.error, "error");
      }
    }
    if (prevProps.consultClient !== this.props.consultClient) {
      // Enabled Button - Validate Identity Client
      if (this.props.consultClient.loading) {
        this.setState((state) => ({
          validateClientButtonDisabled: true,
        }));
      }
      if (
        !this.props.consultClient.loading &&
        this.props.consultClient.response &&
        this.props.consultClient.success
      ) {
        this.getNotistack("Consulta Correcta, 1er Paso Ok!", "success");
        setTimeout(() => {
          this.props.handleNext();
        }, 1500);
      } else if (
        !this.props.consultClient.loading &&
        this.props.consultClient.response &&
        !this.props.consultClient.success
      ) {
        this.setState((state) => ({
          validateClientButtonDisabled: false,
        }));
        // Notistack
        this.getNotistack(this.props.consultClient.error, "error");
      } else if (
        !this.props.consultClient.loading &&
        !this.props.consultClient.response &&
        !this.props.consultClient.success
      ) {
        this.setState((state) => ({
          validateClientButtonDisabled: false,
        }));
        // Notistack
        this.getNotistack(this.props.consultClient.error, "error");
      }
    }

    // Marital Status - Before
    if (prevProps.maritalStatus !== this.props.maritalStatus) {
      // Marital Status - Error Service
      if (
        !this.props.maritalStatus.loading &&
        this.props.maritalStatus.response &&
        this.props.maritalStatus.success
      ) {
      }
      // Marital Status - Error Service
      if (
        !this.props.maritalStatus.loading &&
        this.props.maritalStatus.response &&
        !this.props.maritalStatus.success
      ) {
        this.getNotistack(
          `Estado Civil: ${this.props.maritalStatus.error}`,
          "error"
        );
      }
      // Marital Status - Error Service Connectivity
      else if (
        !this.props.maritalStatus.loading &&
        !this.props.maritalStatus.response &&
        !this.props.maritalStatus.success
      ) {
        this.getNotistack(
          `Estado Civil: ${this.props.maritalStatus.error}`,
          "error"
        );
      }
    }

    // Academic Degree - Before
    if (prevProps.academicDegree !== this.props.academicDegree) {
      // Academic Degree - Error Service
      if (
        !this.props.academicDegree.loading &&
        this.props.academicDegree.response &&
        !this.props.academicDegree.success
      ) {
        this.getNotistack(
          `Grado Académico: ${this.props.academicDegree.error}`,
          "error"
        );
      }
      // Academic Degree - Error Service Connectivity
      else if (
        !this.props.academicDegree.loading &&
        !this.props.academicDegree.response &&
        !this.props.academicDegree.success
      ) {
        this.getNotistack(
          `Grado Académico: ${this.props.academicDegree.error}`,
          "error"
        );
      }
    }
    // Employment Situation - Before
    if (prevProps.employmentSituation !== this.props.employmentSituation) {
      // Employment Situation - Error Service
      if (
        !this.props.employmentSituation.loading &&
        this.props.employmentSituation.response &&
        !this.props.employmentSituation.success
      ) {
        this.getNotistack(
          `Situación Laboral: ${this.props.employmentSituation.error}`,
          "error"
        );
      }
      // Employment Situation - Error Service Connectivity
      else if (
        !this.props.employmentSituation.loading &&
        !this.props.employmentSituation.response &&
        !this.props.employmentSituation.success
      ) {
        this.getNotistack(
          `Situación Laboral: ${this.props.employmentSituation.error}`,
          "error"
        );
      }
    }
    if (prevProps.constantODC !== this.props.constantODC) {
      // ConstantODC - Success Service
      if (
        !this.props.constantODC.loading &&
        this.props.constantODC.response &&
        this.props.constantODC.success
      ) {
        const minAge = this.props.constantODC.data.find(
          (item) => item.des_abv_constante === "CDA_EDAD_MINIMA_CLIENTE_TITULAR"
        );
        const maxAge = this.props.constantODC.data.find(
          (item) => item.des_abv_constante === "CDA_EDAD_MAXIMA_CLIENTE_TITULAR"
        );

        const minWorkingYears = this.props.constantODC.data.find(
          (item) => item.des_abv_constante === "CDA_ANO_LABORAL_MINIMA"
        );
        const maxWorkingYears = this.props.constantODC.data.find(
          (item) => item.des_abv_constante === "CDA_ANO_LABORAL_MAXIMA"
        );

        const defaultMinBirthday = moment()
          .subtract(minAge.valor_numerico, "years")
          .format("YYYY-MM-DD");
        const defaultMaxBirthday = moment()
          .subtract(maxAge.valor_numerico, "years")
          .format("YYYY-MM-DD");

        const defaultMinLaborIncomeDate = moment()
          .subtract(minWorkingYears.valor_numerico, "years")
          .format("YYYY-MM-DD");
        const defaultMaxLaborIncomeDate = moment()
          .subtract(maxWorkingYears.valor_numerico, "years")
          .format("YYYY-MM-DD");

        this.setState((state) => ({
          ...state,
          client: {
            ...state.client,
          },
          defaultMinBirthday: defaultMinBirthday,
          defaultMaxBirthday: defaultMaxBirthday,
          minBirthday: minAge.valor_numerico,
          maxBirthday: maxAge.valor_numerico,

          defaultMaxLaborIncomeDate: defaultMaxLaborIncomeDate,
          defaultMinLaborIncomeDate: defaultMinLaborIncomeDate,
          minLaborIncomeDate: minWorkingYears.valor_numerico,
          maxLaborIncomeDate: maxWorkingYears.valor_numerico,
        }));
      }
      // ConstantODC - Error Service
      else if (
        !this.props.constantODC.loading &&
        this.props.constantODC.response &&
        !this.props.constantODC.success
      ) {
        this.getNotistack(`Constante ODC: ${this.props.gender.error}`, "error");
      }
      // ConstantODC - Error Service Connectivity
      else if (
        !this.props.constantODC.loading &&
        !this.props.constantODC.response &&
        !this.props.constantODC.success
      ) {
        this.getNotistack(`Constante ODC: ${this.props.gender.error}`, "error");
      }
    }
  };
  // Client - Document Type - Select Event Change
  handleChangeSelectDocumentType = (name) => (e) => {
    let object = this.props.idenfiticationDocumentType.data.find(
      (option) => option.cod_valor === e.target.value
    );
    this.setState((state) => ({
      client: {
        ...state.client,
        documentType: object.des_valor,
        documentTypeId: object.cod_valor,
        documentTypeInternalValue: object.valor_interno,
        documentNumber: "",
        documentNumberError: false,
        documentNumberOk: false,
      },
    }));
  };

  // Client - TextField - Event Change - Required
  handleChangeTextFieldRequired = (objectName, subObjectName, name) => (e) => {
    e.persist();
    let { value } = e.target;
    let nameError = `${name}Error`;

    if (subObjectName) {
      this.setState({
        [objectName]: {
          ...this.state[objectName],
          [subObjectName]: {
            ...this.state[objectName][subObjectName],
            [name]: value ? value : "",
            [nameError]: value !== "" ? false : true,
          },
        },
      });
    } else {
      this.setState({
        [objectName]: {
          ...this.state[objectName],
          [name]: value ? value : "",
          [nameError]: value !== "" ? false : true,
        },
      });
    }
  };

  // Client - Select - Event Change
  handleChangeSelectFullRequired = (
    objectName,
    subObjectName,
    data = [],
    name
  ) => (e) => {
    let nameError = `${name}Error`;
    let nameId = `${name}Id`;
    let nameAux = `${name}Aux`;
    let nameInternalValue = `${name}InternalValue`;
    let error = true;
    let valueDefault = "";
    let idDefault = "";
    let object = null;
    if (e !== null) {
      error = false;
      let { label, value } = e;
      object = data.find((option) => option.cod_valor === value);
      valueDefault = label;
      idDefault = value;
    }
    if (subObjectName) {
      this.setState((state) => ({
        ...state,
        [objectName]: {
          ...state[objectName],
          [subObjectName]: {
            ...state[objectName][subObjectName],
            [name]: valueDefault,
            [nameId]: idDefault,
            [nameAux]: object && object.des_auxiliar && object.des_auxiliar,
            [nameInternalValue]:
              object && object.valor_interno && object.valor_interno,
            [nameError]: error,
          },
        },
      }));
    } else {
      this.setState((state) => ({
        ...state,
        [objectName]: {
          ...state[objectName],
          [name]: valueDefault,
          [nameId]: idDefault,
          [nameAux]: object && object.des_auxiliar && object.des_auxiliar,
          [nameInternalValue]:
            object && object.valor_interno && object.valor_interno,
          [nameError]: error,
        },
      }));
    }
  };

  handleBlurTextFieldBirthday = (objectName, subObjectName, name) => (e) => {
    const { defaultMaxBirthday, defaultMinBirthday } = this.state;
    const nameError = `${name}Error`;
    const end = new Date(defaultMinBirthday);
    const start = new Date(defaultMaxBirthday);
    const value = new Date(e.target.value);
    const range = moment().range(start, end);
    let error = true;

    if (range.contains(value)) {
      error = false;
    }

    if (subObjectName) {
      this.setState((state) => ({
        ...state,
        [objectName]: {
          ...state[objectName],
          [subObjectName]: {
            ...state[objectName][subObjectName],
            [nameError]: error,
          },
        },
      }));
    } else {
      this.setState((state) => ({
        ...state,
        [objectName]: {
          ...state[objectName],
          [nameError]: error,
        },
      }));
    }
  };

  // Client - Labor Income Date - TextField Event Blur
  handleBlurTextFieldLaborIncomeDate = (objectName, subObjectName, name) => (
    e
  ) => {
    const { defaultMaxLaborIncomeDate, defaultMinLaborIncomeDate } = this.state;

    const nameError = `${name}Error`;
    const end = new Date(defaultMinLaborIncomeDate);
    const start = new Date(defaultMaxLaborIncomeDate);
    const value = new Date(e.target.value);
    const range = moment().range(start, end);
    let error = true;

    if (range.contains(value)) {
      error = false;
    }

    if (subObjectName) {
      this.setState((state) => ({
        ...state,
        [objectName]: {
          ...state[objectName],
          [subObjectName]: {
            ...state[objectName][subObjectName],
            [nameError]: error,
          },
        },
      }));
    } else {
      this.setState((state) => ({
        ...state,
        [objectName]: {
          ...state[objectName],
          [nameError]: error,
        },
      }));
    }
  };

  // Client - Select - Event Change
  handleChangeSelectRequired = (objectName, subObjectName, name) => (e) => {
    let nameError = `${name}Error`;
    let nameId = `${name}Id`;
    let error = true;
    let valueDefault = "";
    let idDefault = "";
    if (e !== null) {
      error = false;
      let { label, value } = e;
      valueDefault = label;
      idDefault = value;
    }
    if (subObjectName) {
      this.setState((state) => ({
        ...state,
        [objectName]: {
          ...state[objectName],
          [subObjectName]: {
            ...state[objectName][subObjectName],
            [name]: valueDefault,
            [nameId]: idDefault,
            [nameError]: error,
          },
        },
      }));
    } else {
      this.setState((state) => ({
        ...state,
        [objectName]: {
          ...state[objectName],
          [name]: valueDefault,
          [nameId]: idDefault,
          [nameError]: error,
        },
      }));
    }
  };

  // Validate Length Document Type
  validateLengthDocumentType = (value) => {
    switch (value) {
      case 8:
        return false;
      case 9:
        return false;
      default:
        return true;
    }
  };
  // Client - Document Number - TextFiled Event Change
  handleChangeTextFieldDocumentNumber = (name) => (e) => {
    let { value, name } = e.target;
    let nameError = `${name}Error`;
    if (!isNaN(Number(value))) {
      this.setState((state) => ({
        client: {
          ...state.client,
          [name]: value,
          [nameError]: this.validateLengthDocumentType(value.length),
        },
      }));
    }
  };
  // Client - Employment Situation - Select Event Change
  handleChangeSelectEmploymentSituation = (name) => (e) => {
    let jobTitleDisabled = true;
    let economicActivityDisabled = true;
    let employmentSituationError = true;
    let jobTitleError = true;
    let economicActivityError = true;

    let employmentSituation = "";
    let employmentSituationId = "";
    let jobTitle = "";
    let jobTitleId = "";
    let economicActivity = "";
    let economicActivityId = "";

    if (e !== null) {
      employmentSituation = e.label;
      employmentSituationId = e.value;
      if (e.value === 160001) {
        jobTitleDisabled = false;
        economicActivityError = false;
        employmentSituationError = false;
      } else if (e.value === 160002) {
        employmentSituationError = false;
        jobTitleDisabled = false;
        economicActivityError = false;
      } else if (e.value === 160003) {
        employmentSituationError = false;
        jobTitleError = false;
        economicActivityDisabled = false;
      } else if (e.value === 160004) {
        employmentSituationError = false;
        jobTitleError = false;
        economicActivityDisabled = false;
      } else {
        employmentSituationError = false;
        jobTitleError = false;
        economicActivityError = false;
      }
    } else {
      jobTitleError = false;
      economicActivityError = false;
    }

    this.setState((state) => ({
      ...state,
      client: {
        ...state.client,
        employmentSituation: employmentSituation,
        employmentSituationId: employmentSituationId,
        employmentSituationError: employmentSituationError,
        jobTitle: jobTitle,
        jobTitleId: jobTitleId,
        jobTitleError: jobTitleError,
        jobTitleDisabled: jobTitleDisabled,
        economicActivity: economicActivity,
        economicActivityId: economicActivityId,
        economicActivityError: economicActivityError,
        economicActivityDisabled: economicActivityDisabled,
      },
    }));
  };
  // Client - Data - Button Event Submit
  handleSubmitValidateClient = (e) => {
    e.preventDefault();
    let {
      client,
      defaultMaxBirthday,
      defaultMinBirthday,
      defaultMinLaborIncomeDate,
      defaultMaxLaborIncomeDate,
    } = this.state;

    let documentNumberError = true;
    let birthdayError = true;
    let academicDegreeError = true;
    let employmentSituationError = true;
    let maritalStatusError = true;
    // let jobTitleError = true;
    let laborIncomeDateError = true;

    const endB = new Date(defaultMinBirthday);
    const startB = new Date(defaultMaxBirthday);
    const valueB = new Date(client.birthday);
    const rangeB = moment().range(startB, endB);

    const endLI = new Date(defaultMinLaborIncomeDate);
    const startLI = new Date(defaultMaxLaborIncomeDate);
    const valueLI = new Date(client.laborIncomeDate);
    const rangeLI = moment().range(startLI, endLI);
    // Validate Value Data
    if (rangeB.contains(valueB)) {
      birthdayError = false;
    }
    if (rangeLI.contains(valueLI)) {
      laborIncomeDateError = false;
    }
    if (client.academicDegree !== "") {
      academicDegreeError = false;
    }
    if (client.maritalStatus !== "") {
      maritalStatusError = false;
    }
    if (client.employmentSituation !== "") employmentSituationError = false;
    // if (client.jobTitle !== "") {
    //     jobTitleError = false;
    // }
    if (
      (client.documentTypeId === 100001 &&
        this.props.client.documentNumber.trim().length === 8) ||
      (client.documentTypeId === 100002 &&
        this.props.client.documentNumber.trim().length === 9)
    ) {
      documentNumberError = false;
    }
    if (
      !documentNumberError &&
      !birthdayError &&
      !academicDegreeError &&
      !maritalStatusError &&
      //   !jobTitleError &&
      !employmentSituationError &&
      !laborIncomeDateError
    ) {
      // Set Value
      let data = {
        client: {
          documentType: client.documentType,
          documentTypeAux: client.documentNumber.length === 8 ? "1" : "2",
          documentTypeId: client.documentTypeId,
          documentTypeInternalValue: client.documentTypeInternalValue,
          documentNumber: this.props.client.documentNumber,
          maritalStatus: client.maritalStatus,
          maritalStatusId: client.maritalStatusId,
          maritalStatusInternalValue: client.maritalStatusInternalValue,
          laborIncomeDate: client.laborIncomeDate,
          birthday: client.birthday,
          academicDegree: client.academicDegree,
          academicDegreeId: client.academicDegreeId,
          academicDegreeInternalValue: client.academicDegreeInternalValue,
          employmentSituation: client.employmentSituation,
          employmentSituationId: client.employmentSituationId,
          employmentSituationInternalValue:
            client.employmentSituationInternalValue,
          jobTitle: client.jobTitle,
          jobTitleId: client.jobTitleId,
          jobTitleError: client.jobTitleError,
          jobTitleDisabled: client.jobTitleDisabled,
          economicActivity: client.economicActivity,
          economicActivityId: client.economicActivityId,
          economicActivityError: client.economicActivityError,
          economicActivityDisabled: client.economicActivityDisabled,
        },
      };
      // Send Data
      this.props.handleClientValidationForm(data);
    } else {
      if (documentNumberError)
        this.getNotistack("Número Documento: Verificar porfavor", "warning");
      if (birthdayError)
        this.getNotistack("Fecha Nacimiento: Verificar porfavor", "warning");
      if (laborIncomeDateError)
        this.getNotistack("Fecha de Ingreso: Verificar porfavor", "warning");
      if (academicDegreeError)
        this.getNotistack("Grado Académico: Verificar porfavor", "warning");
      if (maritalStatusError)
        this.getNotistack("Estado Civil: Verificar porfavor", "warning");
      //   if (jobTitleError)
      //     this.getNotistack("Cargo - Profesión: Verificar porfavor", "warning");
      if (employmentSituationError)
        this.getNotistack("Situación Laboral: Verificar porfavor", "warning");
      this.setState((state) => ({
        client: {
          ...state.client,
          documentNumberError: documentNumberError,
          birthdayError: birthdayError,
          academicDegreeError: academicDegreeError,
          maritalStatusError: maritalStatusError,
          //   jobTitleError: jobTitleError,
          laborIncomeDateError: laborIncomeDateError,
          employmentSituationError: employmentSituationError,
        },
      }));
    }
  };
  // Notistack
  getNotistack(message, variant = "default", duration = 6000) {
    let select = "default";
    switch (variant) {
      case "error":
        select = variant;
        break;
      case "success":
        select = variant;
        break;
      case "warning":
        select = variant;
        break;
      case "info":
        select = variant;
        break;
      default:
        select = variant;
        break;
    }
    // Notistack
    this.props.enqueueSnackbar(message, {
      variant: select,
      autoHideDuration: duration,
      action: (
        <IconButton>
          <CloseIcon size="small" className="text-white" color="inherit" />
        </IconButton>
      ),
    });
  }

  render() {
    let {
      classes,
      idenfiticationDocumentType,
      maritalStatus,
      academicDegree,
      employmentSituation,
    } = this.props;
    let { client, minLaborIncomeDate, maxLaborIncomeDate, loading } = this.state;

    // Format Marital Status
    let maritalData = maritalStatus.data.map((item) => ({
      value: item.cod_valor,
      label: item.des_valor,
    }));

    // Format Academic Degree
    let academicDegreeData = academicDegree.data.map((item) => ({
      value: item.cod_valor,
      label: item.des_valor,
    }));

    // Employment Situation
    let employmentSituationData = employmentSituation.data.map((item) => ({
      value: item.cod_valor,
      label: item.des_valor,
    }));

    return (
      <>
        <div className="mb-2">
          {
            loading 
              ?
                <>
                    <Typography align="center"> Cargando información del cliente... </Typography>
                    <LinearProgress />
                </>
              :
                <form
                  method="post"
                  onSubmit={this.handleSubmitValidateClient}
                  autoComplete="off"
                >
                  <Grid
                    container
                    spacing={8}
                    className={classNames(classes.root, "mb-2")}
                  >
                    {/* Client - Identification Document Type*/}
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                      <FormControl
                        required
                        margin="normal"
                        fullWidth={true}
                      >
                        <InputLabel htmlFor="documentType">Tipo Documento</InputLabel>
                        <Select
                          value={client.documentTypeId}
                          inputProps={{
                            name: "documentType",
                            id: "documentType",
                          }}
                        >
                          {idenfiticationDocumentType.data.map((item, index) => (
                            <MenuItem key={index} value={item.cod_valor}>
                              {item.des_valor_corto}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </Grid>
                    {/* Client - Identification Document Number*/}
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                      <TextField
                        error={client.documentNumberError}
                        fullWidth={true}
                        required
                        label="Número Documento"
                        type="text"
                        margin="normal"
                        color="default"
                        id="documentNumber"
                        name="documentNumber"
                        value={client.documentNumber}
                        onChange={this.handleChangeTextFieldDocumentNumber(
                          "documentNumber"
                        )}
                        InputProps={{
                          inputProps: {
                            readOnly: true,
                            maxLength: client.documentTypeId === 100001 ? 8 : 9,
                          },
                          endAdornment: (
                            <InputAdornment position="end">
                              <RecentActorsIcon
                                color={
                                  client.documentNumberError ? "secondary" : "inherit"
                                }
                                fontSize="small"
                              />
                            </InputAdornment>
                          ),
                        }}
                      />
                      <Fade>
                        <FormHelperText className={classes.documentNumberFormHelperText}>
                          <Typography
                            component="span"
                            variant="inherit"
                            color={client.documentNumberError ? "secondary" : "default"}
                          >
                            {client.documentNumberError &&
                              "La cantidad de dígitos no son los correctos"}
                          </Typography>
                          <Typography
                            component="span"
                            variant="inherit"
                            color={client.documentNumberError ? "secondary" : "default"}
                          >
                            {client.documentNumber.toString().length}/
                            {this.state.client.documentTypeId === 100001 ? 8 : 9}
                          </Typography>
                        </FormHelperText>
                      </Fade>
                    </Grid>
                  </Grid>
                  <Grid
                    container
                    spacing={8}
                    className={classNames(classes.root,"mb-2")}
                  >
                    <Grid item xs={12} sm={12} md={8} lg={6}>
                      <Typography className={classes.heading}>
                        <AccountCircleIcon fontSize="small" className="mr-2" />
                        Información Personal
                      </Typography>
                    <Divider />
                    </Grid>
                  </Grid>
                  <Grid
                    container
                    spacing={8}
                    className={classNames(classes.root, "mb-2")}
                  >
                    {/* Client - Birthday*/}
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                      <TextField
                        error={client.birthdayError}
                        fullWidth
                        name="birthday"
                        label="Fecha Nacimiento"
                        type="date"
                        margin="normal"
                        required
                        format="DD-MM-YYYY"
                        value={client.birthday}
                        onChange={this.handleChangeTextFieldRequired(
                          "client",
                          null,
                          "birthday"
                        )}
                        onBlur={this.handleBlurTextFieldBirthday(
                          "client",
                          null,
                          "birthday"
                        )}
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />

                      {client.birthdayError && (
                        <FormHelperText className="text-red">
                          {"Revisar fecha de nacimiento."}
                        </FormHelperText>
                      )}
                    </Grid>

                    {/* Client - Marital Status*/}
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                      <FormControl
                        fullWidth
                        error={client.maritalStatusError}
                        required={true}
                      >
                        <Autocomplete
                          error={client.maritalStatusError}
                          onChange={this.handleChangeSelectFullRequired(
                            "client",
                            null,
                            maritalStatus.data,
                            "maritalStatus"
                          )}
                          data={maritalData}
                          value={client.maritalStatusId}
                          placeholder={"Estado Civil *"}
                        />
                      </FormControl>
                    </Grid>
                  </Grid>
                  <Grid
                      container
                      spacing={8}
                      className={classNames(classes.root, "mb-2")}
                  >
                    {/* Client - Academic Degree */}
                    <Grid item xs={12} sm={12} md={8} lg={6}>
                      <FormControl
                          fullWidth
                          error={client.academicDegreeError}
                          required={true}
                      >
                        <Autocomplete
                            error={client.academicDegreeError}
                            onChange={this.handleChangeSelectFullRequired(
                                "client",
                                null,
                                academicDegree.data,
                                "academicDegree"
                            )}
                            value={client.academicDegreeId}
                            data={academicDegreeData}
                            placeholder={"Grado Académico *"}
                        />
                      </FormControl>
                    </Grid>
                  </Grid>
                  <Grid
                      container
                      spacing={8}
                      className={classNames(classes.root,"mb-2")}
                  >
                    <Grid item xs={12} sm={12} md={8} lg={6}>
                      <Typography className={classes.heading}>
                        <WorkIcon fontSize="small" className="mr-2" />
                        Información Laboral
                      </Typography>
                      <Divider />
                    </Grid>
                  </Grid>
                  <Grid
                    container
                    spacing={8}
                    className={classNames(classes.root, "mb-2")}
                  >
                    {/* Client - Employment Situation */}
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                      <FormControl fullWidth error={client.employmentSituationError}>
                        <Autocomplete
                          error={client.employmentSituationError}
                          onChange={this.handleChangeSelectEmploymentSituation(
                            "employmentSituation"
                          )}
                          value={client.employmentSituationId}
                          data={employmentSituationData}
                          placeholder={"Situación Laboral *"}
                        />
                      </FormControl>
                    </Grid>
                    {/* Client - Labor Income Date*/}
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                      <TextField
                        required
                        error={client.laborIncomeDateError}
                        fullWidth={true}
                        label="Fecha de Ingreso"
                        type="date"
                        margin="normal"
                        format="DD-MM-YYYY"
                        name="laborIncome"
                        value={client.laborIncomeDate}
                        onChange={this.handleChangeTextFieldRequired(
                          "client",
                          null,
                          "laborIncomeDate"
                        )}
                        onBlur={this.handleBlurTextFieldLaborIncomeDate(
                          "client",
                          null,
                          "laborIncomeDate"
                        )}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        InputProps={{
                          inputProps: {
                            title: `CDA: Antigüedad válida es entre ${minLaborIncomeDate} a ${maxLaborIncomeDate} año(s). `,
                          }
                        }}
                      />
                      {client.laborIncomeDateError && (
                        <FormHelperText className="text-red">
                          {`CDA: Antigüedad válida es entre ${minLaborIncomeDate} a ${maxLaborIncomeDate} año(s). `}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>
                  <Grid container spacing={8} className={classes.buttonWrapper}>
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                      <Bounce>
                        <div className={classNames(classes.wrapper)}>
                          <Button
                            className={classNames(classes.button)}
                            fullWidth={true}
                            variant="contained"
                            color="secondary"
                            size="small"
                            disabled={this.state.validateClientButtonDisabled}
                            onClick={this.props.handleReset}
                            margin="normal"
                            autoFocus
                          >
                            Cancelar
                            <CancelIcon fontSize="small" className="ml-2" />
                          </Button>
                        </div>
                      </Bounce>
                    </Grid>
                    {/* Validate Client */}
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                      <Bounce>
                        <div className={classNames(classes.wrapper)}>
                          <Button
                            variant="contained"
                            type="submit"
                            className={classNames(classes.button)}
                            disabled={this.state.validateClientButtonDisabled}
                            color="primary"
                            size="small"
                            fullWidth={true}
                          >
                            Continuar
                            <SendIcon fontSize="small" className="ml-2" />
                          </Button>
                          {this.state.validateClientButtonDisabled && (
                            <CircularProgress
                              size={24}
                              className={classes.buttonProgress}
                            />
                          )}
                        </div>
                      </Bounce>
                    </Grid>
                  </Grid>
                </form>
          }
        </div>
      </>
    );
  }
}

const { func } = PropTypes;

ConsultClient.propTypes = {
  handleClientValidationForm: func.isRequired,
};

export default withStyles(styles)(
  withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ConsultClient))
);
