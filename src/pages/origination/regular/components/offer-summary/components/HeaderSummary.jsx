import React from 'react';
import { withStyles } from '@material-ui/core/styles';
// Components
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    root: {
        display:"flex", 
        flexDirection:"column",
        [theme.breakpoints.up('md')]: {
            flexDirection:"row",
            justifyContent: "space-between"
        }
    },
    client:{
        display:"flex", 
        justifyContent:"center", 
        alignItems:"center",
    }
});

const HeaderSummary = ({ documentType, documentNumber, fullName, classes }) => (
    <div className={classes.root}>
        <Typography className="font-weight-bold mb-2" color="textSecondary">
            { `${ documentType } - ${documentNumber}`.toUpperCase() }
        </Typography>
        <Typography className="font-weight-bold" color="textSecondary">
            { `${fullName}`.toUpperCase() }
        </Typography>                                  
    </div>
)


HeaderSummary.defaultProps = {
    fullName:"ROBERTO GIAN FRANCO CARBONELL SARAVIA",
    documentType: "DNI",
    documentNumber: "71733184"
};
   


export default withStyles(styles)(HeaderSummary);



