import React from 'react';
import classNames from 'classnames';
// Components
import { withStyles, Typography } from '@material-ui/core';
//Icons 
import LabelIcon from '@material-ui/icons/Label';

const styles = theme => ({
    root: {
        display:"flex", 
        justifyContent:"space-between", 
        alignItems:"center",
    },
    nameItem:{
        display:"flex", 
        alignItems:"center"
    }, 
    valueItem:{
        display:"flex", 
        justifyContent:"space-between", 
        alignItems:"center"
    }
});

const ItemSummary = ({ icon, name, value, classes, ...props }) => (
    <Typography className={classNames(classes.root, "mb-2")}>
        <Typography component="span" className={classes.nameItem}>
            <Typography component="span" className="mt-1 mr-2">
                { icon }
            </Typography>
            <Typography component="span" >
                { name }
            </Typography>
        </Typography>
        <Typography component="span" className={classes.valueItem} color="textSecondary">
            { value }
        </Typography>
    </Typography>
);
  
ItemSummary.defaultProps = {
    icon: <LabelIcon fontSize="small"/>,
    name: "Nombre",
    value: "Valor", 
};

export default withStyles(styles)(ItemSummary);


