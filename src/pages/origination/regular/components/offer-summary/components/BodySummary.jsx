import React from "react";
import { withStyles } from "@material-ui/core/styles";
// Components
import ItemSummary from "./ItemSummary";
// Custom Components
import CreditCardIcon from "@material-ui/icons/CreditCard";
import LabelImportantIcon from "@material-ui/icons/LabelImportant";
import CardMembershipIcon from "@material-ui/icons/CardMembership";
import LensIcon from "@material-ui/icons/Lens";
import MoneyIcon from "@material-ui/icons/Money";
import InsertChartIcon from "@material-ui/icons/InsertChart";
// Utils
import { maskCreditCard, getMoneyFormat } from "../../../../../../utils/Utils";
const styles = (theme) => ({
  root: {
    padding: ".5em",
    border: "1px solid #c4c4c4",
    borderRadius: ".25rem",
    [theme.breakpoints.up("md")]: {
      padding: "1.5em",
    },
  },
});

const BodySummary = ({
  creditCard,
  fullNumber,
  lineAvailable,
  effectiveProvision,
  isSAE,
  lineSAE,
  maxAmount,
  classes,
  tcea,
}) => (
  <ul className={classes.root}>
    {/* # Additional */}
    <li>
      <ItemSummary name="Nro. Solicitud" value={fullNumber} />
    </li>
    {/* CreditCard - Number Card Number */}
    <li>
      <ItemSummary
        icon={<CreditCardIcon fontSize="small" />}
        name="Nro. Tarjeta"
        value={maskCreditCard(creditCard.cardNumber)}
      />
    </li>
    {/* CreditCard - Brand */}
    <li>
      <ItemSummary
        icon={<CardMembershipIcon fontSize="small" />}
        name="Marca Tarjeta"
        value={creditCard.brand.toUpperCase()}
      />
    </li>
    {/* CreditCard - Type */}
    <li>
      <ItemSummary
        icon={<LabelImportantIcon fontSize="small" />}
        name="Tipo Tarjeta"
        value={creditCard.name.toUpperCase()}
      />
    </li>
    {/* CreditCard - Color */}
    {creditCard.productId !== 10008 && (
      <li>
        <ItemSummary
          icon={<LensIcon fontSize="small" />}
          name="Color Tarjeta"
          value={
            creditCard.colorAux ? (
              <LensIcon style={{ color: creditCard.colorAux }} />
            ) : (
              "NO APLICA"
            )
          }
        />
      </li>
    )}
    {/* Effective Provision */}
    <li>
      <ItemSummary
        icon={<InsertChartIcon fontSize="small" />}
        name="Disposición de Efectivo"
        value={`S/ ${getMoneyFormat(
          ((lineAvailable * effectiveProvision) / 100).toString()
        )} (${effectiveProvision.toFixed(2)}%)`}
      />
    </li>
    {/* Line available */}
    <li>
      <ItemSummary
        icon={<MoneyIcon fontSize="small" />}
        name="Línea de Crédito"
        value={`S/ ${getMoneyFormat(lineAvailable.toString())}`}
      />
    </li>
    {isSAE ? (
      <>
        <li>
          <ItemSummary
            icon={<MoneyIcon fontSize="small" />}
            name="Línea de SAE"
            value={`S/ ${getMoneyFormat(lineSAE.toString())}`}
          />
        </li>
        <li>
          {/*<ItemSummary icon={<MoneyIcon fontSize="small"/>} name="Línea Global" 
                        value={`S/ ${getMoneyFormat((maxAmount).toString())}`}/>*/}
        </li>
      </>
    ) : (
      ""
    )}
    <li>
      {/*<ItemSummary name="TCEA" value={`S/ ${getMoneyFormat((tcea).toString())}`} />*/}
      <ItemSummary
        name="TCEA"
        value={tcea ? `${parseFloat(tcea).toFixed(2)}%` : "No Aplica"}
      />
    </li>
  </ul>
);

BodySummary.defaultProps = {
  fullNumber: "00000000000",
  creditCard: {
    colorAux: "",
    name: "CENCOSUD MASTERCARD BLACK",
    brand: "MASTERCARD",
    cardNumber: "000000000000000000",
  },
  lineAvailable: 0,
  effectiveProvision: 0,
  tcea: 0,
};

export default withStyles(styles)(BodySummary);
