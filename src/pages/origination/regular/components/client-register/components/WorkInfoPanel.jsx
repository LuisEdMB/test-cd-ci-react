import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// Custom Components 
import WorkBasicInfo from './work-info/WorkBasicInfo';
import WorkBusiness from './work-info/WorkBusiness';
import WorkExtraPhone from './work-info/WorkExtraPhone';
//Icons 
import WorkIcon from '@material-ui/icons/Work';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import MoneyIcon from '@material-ui/icons/Money';
import CloseIcon from '@material-ui/icons/Close';
//Actions 
import { getEmploymentSituation } from '../../../../../../actions/employment-situation';
import { getJobTitle } from '../../../../../../actions/job-title'; 
import { getEconomicActivity } from '../../../../../../actions/economic-activity';
//Utils
import { 
    onlyTextRegex,
    onlyNumberRegex,
    getDateConstraint, 
    getDateCurrent } from '../../../../../../utils/Utils';

const styles = theme => ({
    root:{
        borderRadius:".5em"
    },
    heading:{
        display:"flex", 
        justifyContent:"center", 
        alignItems:"center",
        textTransform: "uppercase",
    },
    expansionPanel: {
        backgroundColor: "#f0f0f045",
        borderRadius: ".5em",
    },
    button:{
        textTransform: 'none',
    }
});

const mapStateToProps = (state, props) => {
    return {
        employmentSituation:state.employmentSituationReducer, 
        jobTitle:state.jobTitleReducer, 
        economicActivity:state.economicActivityReducer, 
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getEmploymentSituation:bindActionCreators(getEmploymentSituation, dispatch), 
        getJobTitle:bindActionCreators(getJobTitle, dispatch), 
        getEconomicActivity:bindActionCreators(getEconomicActivity, dispatch), 
    };
    return actions;
}
class WorkInfoPanel extends PureComponent {
    constructor(props){
        super(props);
        let year = 22;
        let minData = getDateConstraint(new Date().getFullYear() - year, 0, 1).toString();
        let maxDate = getDateCurrent(3).split("T")[0]
        this.minLaborIncomeDate = minData;
        this.maxLaborIncomeDate = maxDate;
    }
    componentDidMount = () => {
        // Get API's -  Sever ODC
        this.props.getEmploymentSituation();
        this.props.getJobTitle();
        this.props.getEconomicActivity();
    }
    componentDidUpdate = (prevProps, prevState) => {
        // Employment Situation - Before
        if (prevProps.employmentSituation !== this.props.employmentSituation){
            // Employment Situation - Error Service 
            if(!this.props.employmentSituation.loading &&
                this.props.employmentSituation.response && 
                !this.props.employmentSituation.success){
                this.getNotistack(`Situación Laboral: ${this.props.employmentSituation.error}`, "error");
            }
            // Employment Situation - Error Service Connectivity
            else if(!this.props.employmentSituation.loading && 
                    !this.props.employmentSituation.response && 
                    !this.props.employmentSituation.success){
                this.getNotistack(`Situación Laboral: ${this.props.employmentSituation.error}`, "error");
            }
        }
        // Job Title - Before
        if (prevProps.jobTitle !== this.props.jobTitle){
            // Job Title - Error Service 
            if(!this.props.jobTitle.loading && 
               this.props.jobTitle.response && 
               !this.props.jobTitle.success){
               this.getNotistack(`Cargo - Profesión: ${this.props.jobTitle.error}`, "error");
            }
            // Job Title - Error Service Connectivity
            else if(!this.props.jobTitle.loading && 
                    !this.props.jobTitle.response && 
                    !this.props.jobTitle.success){
                     this.getNotistack(`Cargo - Profesión: ${this.props.jobTitle.error}`, "error");
            }
        }
        // Economic Activity - Before
        if (prevProps.economicActivity !== this.props.economicActivity){
            // Economic Activity - Error Service 
            if(!this.props.economicActivity.loading && 
                this.props.economicActivity.response && 
               !this.props.economicActivity.success){
                this.getNotistack(`Actividad Económica: ${this.props.economicActivity.error}`, "error");
            }
            // Economic Activity - Error Service Connectivity
            else if(!this.props.economicActivity.loading && 
                    !this.props.economicActivity.response && 
                    !this.props.economicActivity.success){
                this.getNotistack(`Actividad Económica: ${this.props.economicActivity.error}`, "error");
            }
        }
    }
    // Client - Employment Situation - Select Event Change
    handleChangeSelectEmploymentSituation = e => {
        let jobTitleDisabled=true;
        let economicActivityDisabled=true;
        
        let employmentSituationError=true;
        let jobTitleError=true;
        let economicActivityError=true;

        let employmentSituation="";
        let employmentSituationId="";
        let jobTitle="";
        let jobTitleId="";
        let economicActivity="";
        let economicActivityId="";

        if(e !== null){
            employmentSituation = e.label;
            employmentSituationId = e.value;
            if(e.value === 160001){
                jobTitleDisabled=false;
                economicActivityError=false;
                employmentSituationError=false;
            }
            else if(e.value === 160002){
                employmentSituationError=false;
                jobTitleDisabled=false;
                economicActivityError=false;
            }
            else if(e.value === 160003){
                employmentSituationError=false;
                jobTitleError=false;
                economicActivityDisabled=false;
            }
            else if(e.value === 160004){
                employmentSituationError=false;
                jobTitleError=false;
                economicActivityDisabled=false;
            }
            else{
                employmentSituationError=false;
                jobTitleError=false;
                economicActivityError=false;
            }
        }
        else{
            jobTitleError=false;
            economicActivityError=false;
        }

        this.props.handleSetState(state => ({
            client:{
               ...state.client, 
               employmentSituation:employmentSituation, 
               employmentSituationId:employmentSituationId,
               employmentSituationError:employmentSituationError, 
               employmentSituationFocus:false,
               jobTitle:jobTitle,
               jobTitleId:jobTitleId,
               jobTitleError:jobTitleError,
               jobTitleDisabled:jobTitleDisabled,
               economicActivity:economicActivity,
               economicActivityId:economicActivityId,
               economicActivityError:economicActivityError,
               economicActivityDisabled:economicActivityDisabled
           }
        }));
    }
    // Client - Generic Select
    handleChangeSelectRequired = (e, name) => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let nameFocus = `${name}Focus`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }
        this.props.handleSetState(state => ({
            client:{
                ...state.client,
                [name]: valueDefault, 
                [nameId]: idDefault,
                [nameError]: error,
                [nameFocus]: false
            }
        }));
    }
    // Client - Labor Income Date
    handleChangeTextFieldLaborIncomeDate = e => {
        let { value, name } = e.target;
        let nameError = `${name}Error`;
        this.props.handleSetState(state => ({
                client:{
                    ...state.client,
                    [name]:value,
                    [nameError]: value !== "" ? false : true
                }
            })
        );
    }
    // Generic - TextField - Required
    handleChangeTextFieldRequired = e => {
        let { value, name } = e.target;
        let nameError = `${name}Error`;
        this.props.handleSetState(state => ({
                client:{
                    ...state.client,
                    [name]:value,
                    [nameError]: value !== "" ? false : true
                }
            })
        );
    }
    // Generic - TextField - Only Text
    handleChangeTextFieldOnlyText = ({target: {value, name}}) => {
        if(onlyTextRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                        ...state.client,
                        [name]:value,
                    }
                })
            )
        }
    }
    // Generic - TextField - Only Number
    handleChangeTextFieldOnlyNumber = e => {
        let { value, name } = e.target;
        if(onlyNumberRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                        ...state.client,
                        [name]:value,
                    }
                })
            );
        }
    }
    // Notistack
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render(){
        let {   
            classes, state, 
            telephoneExtensionLength=4,
            employmentSituation, jobTitle, economicActivity
        } = this.props;
         // Employment Situation
         let employmentSituationData = employmentSituation.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        })); 
        // Job Title
        let jobTitleData = jobTitle.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        })); 
        // Economic Activity
        let economicActivityData = economicActivity.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        })); 
        // Format Extra Business
        let businessOptionData = [
            { value: "0", label: "RUC"}, 
            { value: "1", label: "Razón Social"}, 
        ]
        return(
            <ExpansionPanel className={classes.root}>
                <ExpansionPanelSummary 
                    className={classes.expansionPanel}
                    expandIcon={<ExpandMoreIcon fontSize="small" />}>
                    <Typography className={classes.heading}>
                        <WorkIcon fontSize="small" className="mr-2" />
                        Información Laboral
                    </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails >
                    <Grid container spacing={8}> 
                        <WorkBasicInfo 
                            client={state.client}
                            employmentSituation={employmentSituationData}
                            jobTitle={jobTitleData}
                            economicActivity={economicActivityData}
                            handleChangeSelectEmploymentSituation={this.handleChangeSelectEmploymentSituation}
                            handleChangeSelectJobTitle={this.handleChangeSelectRequired}
                            handleChangeSelectEconomicActivity={this.handleChangeSelectRequired}
                        />
                        {/* Client - Labor Income Date*/}        
                        <Grid item xs={12} sm={6} md={4} lg={3}>
                            <TextField
                                error={state.client.laborIncomeDateError}
                                fullWidth={true}
                                label="Fecha Ingreso Laboral"
                                type="date"
                                margin="normal"
                                required
                                format="DD-MM-YYYY"
                                defaultValue={state.client.laborIncomeDate}
                                name="laborIncomeDate"
                                onChange={this.handleChangeTextFieldRequired}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                InputProps={{
                                    inputProps:{
                                        min:this.minLaborIncomeDate,
                                        max:this.maxLaborIncomeDate
                                    },
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <CalendarTodayIcon color={state.laborIncomeDateError? "secondary":"inherit"}  fontSize="small" />
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>     
                        <WorkBusiness 
                            businessOption={businessOptionData}
                            client={state.client}
                            handleChangeSelectOptionBusiness={this.handleChangeSelectRequired}
                        />                        
                        {/* Client - Gross Income */}
                        <Grid item xs={12} sm={6} md={4} lg={3}>
                            <TextField
                                error={state.client.grossIncomeError}
                                fullWidth={true}
                                required
                                label="Ingreso Bruto (S/)"
                                type="number"
                                margin="normal"
                                name="grossIncome"
                                value={state.client.grossIncome} 
                                onChange={this.handleChangeTextFieldLaborIncomeDate}
                                InputProps={{
                                    inputProps:{
                                        step:"0.01", 
                                        min:"0",
                                        max:"100000"
                                    },
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <MoneyIcon color={state.client.grossIncomeError? "secondary": "inherit"} fontSize="small"/>
                                        </InputAdornment>
                                    ), 
                                }}
                            />
                        </Grid>      
                        <WorkExtraPhone 
                            handleChangeTextFieldTelephone={this.handleChangeTextFieldOnlyText}
                            handleChangeTextFieldTelephoneExtension={this.handleChangeTextFieldOnlyNumber}
                            client={state.client}
                            telephoneExtensionLength={telephoneExtensionLength}
                        />
                    </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        )
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(WorkInfoPanel)));

