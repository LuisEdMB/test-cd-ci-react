import React from 'react';
import { withStyles } from '@material-ui/core/styles';
// Components
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
// Utils
import TextMaskTelephone from '../../../../../../../utils/TextMaskTelephone';
// Icons
import PhoneIcon from '@material-ui/icons/Phone';
import DialpadIcon from '@material-ui/icons/Dialpad';

const styles = theme => ({

});

const WorkExtraPhone = ({
    client,
    telephoneExtensionLength = 4,
    handleChangeTextFieldTelephone, 
    handleChangeTextFieldTelephoneExtension
}) => (
    <>
        {/* Client - Work Telephone */}
        <Grid item xs={6} sm={6} md={4} lg={3}>
            <TextField
                fullWidth={true}
                label="Teléfono"
                type="text"
                margin="normal"
                color="default"
                value={client.workTelephone}
                onChange={handleChangeTextFieldTelephone}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <PhoneIcon fontSize="small"/>
                        </InputAdornment>
                    ), 
                    inputComponent: TextMaskTelephone,
                }}
            />
        </Grid>  
        {/* Client - Work Telephone Annex */}
        <Grid item xs={6} sm={6} md={4} lg={3}>
            <TextField
                fullWidth={true}
                label="Anexo Telefónico"
                type="text"
                margin="normal"
                color="default"
                value={client.workTelephoneExtension}
                name="workTelephoneExtension"
                onChange={handleChangeTextFieldTelephoneExtension}
                InputProps={{
                    inputProps:{
                        maxLength:telephoneExtensionLength
                    },
                    endAdornment: (
                        <InputAdornment position="end">
                            <DialpadIcon fontSize="small"/>
                        </InputAdornment>
                    )
                }}
            />
        </Grid>     
    </>
)

export default React.memo(withStyles(styles)(WorkExtraPhone));