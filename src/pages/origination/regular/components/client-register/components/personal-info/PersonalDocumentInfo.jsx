import React from 'react';
// Components
import { 
    Grid, 
    TextField,
    InputAdornment, 
    FormControl, 
    InputLabel, 
    Select,
    MenuItem, 
    FormHelperText
} from '@material-ui/core';
// Icons
import RecentActorsIcon from '@material-ui/icons/RecentActors';

const PersonalDocumentInfo = ({ 
        client,
        idenfiticationDocumentType = [],
        handleChangeSelectDocumentType,
        handleChangeTextFieldDocumentNumber, 
        ...props}) => ( 
        <Grid container spacing={8}> 
            {/* Client - Document Type */}
            <Grid item xs={12} sm={6} md={6} lg={3}>
                <FormControl 
                    required 
                    error={client.documentTypeError}
                    margin="normal"
                    fullWidth={true}>
                    <InputLabel htmlFor="documentType">Tipo Documento</InputLabel>
                    <Select
                        value={client.documentTypeId}
                        inputProps={{ 
                            readOnly: client.documentTypeReadOnly,
                            id: 'documentType' 
                        }}
                        onChange={handleChangeSelectDocumentType}>
                        {
                            idenfiticationDocumentType.map((item, index) => (
                                <MenuItem key={index} value={item.cod_valor}>{item.des_valor_corto}</MenuItem>
                            ))
                        }
                    </Select>
                </FormControl>
            </Grid>
            {/* Client - Document Number */}
            <Grid item xs={12} sm={6} md={6} lg={3}>
                <TextField
                    error={client.documentNumberError}
                    fullWidth={true}
                    required
                    label="Número Documento"
                    type="text"
                    margin="normal"
                    color="default"
                    value={client.documentNumber}
                    name="documentNumber"
                    onChange={handleChangeTextFieldDocumentNumber}
                    InputProps={{
                        readOnly: client.documentNumberReadOnly,
                        inputProps:{
                            maxLength: client.documentTypeId === 100001 ? 8:9,
                        },
                        endAdornment: (
                            <InputAdornment position="end">
                                <RecentActorsIcon color={ client.documentNumberError?"secondary": "inherit"}  fontSize="small"/>
                            </InputAdornment>
                        )
                    }}
                />
                <FormHelperText className="text-right">
                    {client.documentNumber.toString().length}/{client.documentTypeId === 100001 ? 8 : 9}
                </FormHelperText>
            </Grid>            
        </Grid>

    )

export default React.memo(PersonalDocumentInfo);