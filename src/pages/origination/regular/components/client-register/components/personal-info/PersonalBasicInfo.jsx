import React from 'react';
// Components
import { 
    Grid,
    TextField,
    InputAdornment
} from '@material-ui/core';
// Custom Components 
import Autocomplete from '../../../../../../../components/Autocomplete';
import FocusLock from 'react-focus-lock';
// Icons
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';

const PersonalBasicInfo = ({ 
        gender = [],
        maritalStatus = [], 
        country = [],
        handleChangeTextFieldBirthday,
        handleChangeSelectGender,
        handleChangeSelectMaritalStatus,
        handleChangeSelectNationality,
        minBirthday="1950-01-01",
        maxBirthday,
        client
    }) =>   (
                <Grid container spacing={8}>
                    {/* Client - Birthday*/}        
                    <Grid item xs={6} sm={6} md={3} lg={3}>
                        <TextField
                            error={client.birthdayError}
                            fullWidth={true}
                            name="birthday"
                            label="Fecha Nacimiento"
                            type="date"
                            margin="normal"
                            required
                            format="DD-MM-YYYY"
                            defaultValue={client.birthday}
                            onChange={handleChangeTextFieldBirthday}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            InputProps={{
                                inputProps:{
                                    min:minBirthday,
                                    max:maxBirthday
                                },
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <CalendarTodayIcon color={client.birthdayError? "secondary": "inherit"} fontSize="small" />
                                    </InputAdornment>
                                )
                            }}
                        />
                    </Grid>  
                    {/* Client - Gender */}
                    <Grid item xs={6} sm={6} md={3} lg={3}>
                        <FocusLock disabled={!client.genderFocus}>
                            <div>
                                <Autocomplete 
                                    error={client.genderError}
                                    onChange={(e) => handleChangeSelectGender(e, "gender")}
                                    data={gender}
                                    value={client.genderId}
                                    placeholder={"Género *"}
                                />  
                            </div>
                        </FocusLock>
                    </Grid>
                    {/* Client - Marital Status*/}        
                    <Grid item xs={6} sm={6} md={3} lg={3}>
                        <FocusLock disabled={!client.maritalStatusFocus}>
                            <div>
                                <Autocomplete 
                                    error={client.maritalStatusError}
                                    onChange={(e) => handleChangeSelectMaritalStatus(e, "maritalStatus")}
                                    data={maritalStatus}
                                    value={client.maritalStatusId}
                                    placeholder={"Estado Civil *"}
                                />  
                            </div>
                        </FocusLock>     
                    </Grid>  
                    {/* Client - Nationality */}
                    <Grid item xs={6} sm={6} md={3} lg={3}>
                        <FocusLock disabled={!client.nationalityFocus}>
                            <div>
                                <Autocomplete 
                                    error={client.nationalityError}
                                    onChange={(e) => handleChangeSelectNationality(e, "nationality")}
                                    value={client.nationalityId}
                                    data={country}
                                    placeholder={"Nacionalidad *"}
                                />  
                            </div>
                        </FocusLock>     
                    </Grid>    
                </Grid>
            )

export default React.memo(PersonalBasicInfo);