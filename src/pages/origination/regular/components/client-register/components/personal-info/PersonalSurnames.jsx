import React from 'react';
// Components
import {
    Grid,
    TextField, 
    InputAdornment 
}from '@material-ui/core';
//Icons 
import PermIdentityIcon from '@material-ui/icons/PermIdentity';


const PersonalSurnames = ({   
            handleChangeTextFieldLastName,
            handleChangeTextFieldMotherLastName,
            defaultLength,
            client
        }) => (
            <>
                {/* Client - Last Name */}
                <Grid item xs={12} sm={6} md={6} lg={3}>
                    <TextField
                        error={client.lastNameError}
                        fullWidth={true}
                        required
                        name="lastName"
                        label="Apellido Paterno"
                        type="text"
                        margin="normal"
                        color="default"
                        value={client.lastName}
                        onChange={handleChangeTextFieldLastName}
                        InputProps={{
                            inputProps:{
                                maxLength: defaultLength,
                            },
                            endAdornment: (
                                <InputAdornment position="end">
                                    <PermIdentityIcon color={client.lastNameError? "secondary": "inherit"} fontSize="small" />
                                </InputAdornment>
                            )
                        }}
                    />
                </Grid>
                {/* Client - Mother Last Name */}
                <Grid item xs={12} sm={6} md={6} lg={3}>
                    <TextField
                        fullWidth={true}
                        name="motherLastName"
                        label="Apellido Materno"
                        type="text"
                        margin="normal"
                        color="default"
                        value={client.motherLastName}
                        onChange={handleChangeTextFieldMotherLastName}
                        InputProps={{
                            inputProps:{
                                maxLength: defaultLength,
                            },
                            endAdornment: (
                                <InputAdornment position="end">
                                    <PermIdentityIcon color="inherit" fontSize="small"/>
                                </InputAdornment>
                            )
                        }}
                    />
                </Grid>
            </>
        )


export default React.memo(PersonalSurnames);