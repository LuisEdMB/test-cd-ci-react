import React from 'react';
import PersonalDocumentInfo from './PersonalDocumentInfo';
import PersonalFullName from './PersonalFullName';
import PersonalBasicInfo from './PersonalBasicInfo';
import PersonalExtraInfo from './PersonalExtraInfo';

const PersonalInfoWrapper = ({   client,
            idenfiticationDocumentType = [],
            gender = [],
            maritalStatus = [], 
            country = [],
            academicDegree = [], 
            minBirthday,
            maxBirthday, 

            handleChangeSelectDocumentType,
            handleChangeTextFieldDocumentNumber,
            handleChangeTextFieldFirstName,
            handleChangeTextFieldSecondName,
            handleChangeTextFieldLastName,
            handleChangeTextFieldMotherLastName,

            handleChangeTextFieldBirthday,
            handleChangeSelectGender,
            handleChangeSelectMaritalStatus,
            handleChangeSelectNationality,

            handleChangeAcademicDegree,
            handleChangeEmail,
            handleChangeCellphone,
            handleChangeTelephone,
            cellphoneLength = 15,
            emailLength = 50,
            defaultLength=30
        }) => (
        <>
            <PersonalDocumentInfo 
                idenfiticationDocumentType={idenfiticationDocumentType}
                handleChangeSelectDocumentType={handleChangeSelectDocumentType}
                handleChangeTextFieldDocumentNumber={handleChangeTextFieldDocumentNumber}
                client={client}
            />
            <PersonalFullName 
                handleChangeTextFieldFirstName={handleChangeTextFieldFirstName}
                handleChangeTextFieldSecondName={handleChangeTextFieldSecondName}
                handleChangeTextFieldLastName={handleChangeTextFieldLastName}
                handleChangeTextFieldMotherLastName={handleChangeTextFieldMotherLastName}
                client={client}
                defaultLength={defaultLength}
            />
            <PersonalBasicInfo 
                handleChangeTextFieldBirthday={handleChangeTextFieldBirthday}
                handleChangeSelectGender={handleChangeSelectGender}
                handleChangeSelectMaritalStatus={handleChangeSelectMaritalStatus}
                handleChangeSelectNationality={handleChangeSelectNationality}
                gender={gender}
                maritalStatus={maritalStatus} 
                country={country}
                client={client}
                minBirthday={minBirthday}
                maxBirthday={maxBirthday}
            />
            <PersonalExtraInfo 
                academicDegree={academicDegree}
                handleChangeAcademicDegree ={handleChangeAcademicDegree}
                handleChangeEmail={handleChangeEmail}
                handleChangeCellphone={handleChangeCellphone}
                handleChangeTelephone={handleChangeTelephone}
                client={client}
                emailLength={emailLength}
                cellphoneLength={cellphoneLength}
                defaultLength={defaultLength}
            />
        </>
    )


export default PersonalInfoWrapper;