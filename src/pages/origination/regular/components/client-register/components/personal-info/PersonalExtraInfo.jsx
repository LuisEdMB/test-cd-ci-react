import React from 'react';
// Components
import {
    Grid,
    TextField, 
    InputAdornment 
}from '@material-ui/core';
// Custom Components 
import PersonalExtraPhone from './PersonalExtraPhone';
import Autocomplete from '../../../../../../../components/Autocomplete';
import FocusLock from 'react-focus-lock';

// Icons
import EmailIcon from '@material-ui/icons/Email';


const PersonalExtraInfo = ({ 
        academicDegree = [],
        cellphoneLength = 15,
        emailLength = 50,
        handleChangeAcademicDegree,
        handleChangeEmail,
        handleChangeCellphone,
        handleChangeTelephone,
        client,
    }) => (
        <Grid container spacing={8}>   
            {/* Client - Academic Degree */}
            <Grid item xs={12} sm={6} md={6} lg={3}>
                <FocusLock disabled={!client.academicDegreeFocus}>
                    <div>  
                        <Autocomplete 
                            error={client.academicDegreeError}
                            onChange={(e) => handleChangeAcademicDegree(e, "academicDegree")}
                            data={academicDegree}
                            value={client.academicDegreeId}
                            placeholder={"Grado Académico *"}
                        />  
                    </div>
                </FocusLock>
            </Grid> 
            {/* Client - E-Mail */}
            <Grid item xs={12} sm={6} md={6} lg={3}>
                <TextField
                    required
                    error={client.emailError}
                    fullWidth={true}
                    label="Correo Electrónico"
                    type="email"
                    margin="normal"
                    color="default"
                    name="email"
                    value={client.email}
                    onChange={handleChangeEmail}
                    InputProps={{
                        inputProps:{
                            maxLength: emailLength,
                        },
                        endAdornment: (
                            <InputAdornment position="end">
                                <EmailIcon fontSize="small"/>
                            </InputAdornment>
                        )
                    }}
                />
            </Grid>
            <PersonalExtraPhone 
                handleChangeCellphone={handleChangeCellphone}
                handleChangeTelephone={handleChangeTelephone}
                cellphoneLength={cellphoneLength}
                client={client}
            />
        </Grid>
    )

export default React.memo(PersonalExtraInfo);