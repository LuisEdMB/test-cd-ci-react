import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components
import {
    Grid,
    Typography, 
    IconButton, 
    ExpansionPanel, 
    ExpansionPanelSummary, 
    ExpansionPanelDetails
}from '@material-ui/core';
// Custom Components 
import SearchPromotor from './benefits-functions-info/SearchPromotor';
import SelectInfo from './benefits-functions-info/SelectInfo';
//Icons 
import CloseIcon from '@material-ui/icons/Close'; 
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
//Actions 
import { getPaymentDate } from '../../../../../../actions/payment-date';
import { getSendCorrespondence } from '../../../../../../actions/send-correspondence';
import { getAccountStatus } from '../../../../../../actions/account-status';
import { getMedia } from '../../../../../../actions/media';
import { getPromoter } from '../../../../../../actions/promoter';

const styles = theme => ({
    root:{
        borderRadius:".5em"
    },
    heading:{
        display:"flex", 
        justifyContent:"center", 
        alignItems:"center",
        textTransform: "uppercase",
    },
    expansionPanel: {
        backgroundColor: "#f0f0f045",
        borderRadius: ".5em",
    },
    button:{
        textTransform: 'none',
    }
});

const mapStateToProps = (state, props) => {
    return {
        paymentDate: state.paymentDateReducer,
        sendCorrespondence: state.sendCorrespondenceReducer,
        accountStatus: state.accountStatusReducer,
        media: state.mediaReducer,
        promoter:state.promoterReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getPaymentDate:bindActionCreators(getPaymentDate, dispatch),
        getSendCorrespondence:bindActionCreators(getSendCorrespondence, dispatch),
        getAccountStatus:bindActionCreators(getAccountStatus, dispatch),
        getMedia:bindActionCreators(getMedia, dispatch),
        getPromoter:bindActionCreators(getPromoter, dispatch)
    };
    return actions;
}
class HomeAddressInfoPanel extends PureComponent {
    componentDidUpdate = (prevProps, prevState) => {
        // Promoter
        if (prevProps.promoter !== this.props.promoter) {
            if(!this.props.promoter.loading && this.props.promoter.response && this.props.promoter.success){
                let {
                        cod_escaner,
                        cod_promotor,
                        des_nom_promotor } = this.props.promoter.data;

                this.props.handleSetState(state => ({
                    client:{
                        ...state.client,
                        extra:{
                            ...state.client.extra,
                            scannerCode:cod_escaner,
                            promoterId:cod_promotor,
                            promoterName:des_nom_promotor,
                            promotorError:false,
                            scannerCodeError:false
                        }
                    }
                }));
            }
            else if(!this.props.promoter.loading && this.props.promoter.response && !this.props.promoter.success){
                this.props.handleSetState(state => ({
                    client:{
                        ...state.client, 
                        extra:{
                            ...state.client.extra, 
                            scannerCode:"",
                            promoterId:"",
                            promoterName:"", 
                            promotorError:true,
                            scannerCodeError:true
                        }
                    }
                }));
                this.getNotistack(`${this.props.promoter.error}`, "error");

            }
            else if(!this.props.promoter.loading && !this.props.promoter.response && !this.props.promoter.success){
                this.props.handleSetState(state => ({
                    client:{
                        ...state.client, 
                        extra:{
                            ...state.client.extra, 
                            scannerCode:"",
                            promoterId:"",
                            promoterName:"", 
                            promotorError:true,
                            scannerCodeError:true
                        }
                    }
                }));
                this.getNotistack(`Promotor: ${this.props.promoter.error}`, "error");
            }
        }
    }
    componentDidMount = () => {
        // Get API's -  Sever ODC
        this.props.getPaymentDate();
        this.props.getSendCorrespondence();
        this.props.getAccountStatus();
        this.props.getMedia();
    }
   
    // Client - Search Promoter 
    handleKeyPressTextFieldScannerCode = (e) => {
        let { value } = e.target;
        let { which } = e;
        if (which === 13) {
            this.props.getPromoter(value? value : "");
            e.preventDefault();
        }
    }
    handleChangeTextFieldScannerCode = ({target: {value, name}}) => {
        // Set Value in state
        this.props.handleSetState(state => ({
            client:{
                ...state.client, 
                extra:{
                    ...state.client.extra, 
                    scannerCode:value            
                }
            }
        }));
    }
    // Client - Payment Date - Full
    handleChangeSelectPaymentDateFullRequired = (e, name) =>{
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let nameFocus = `${name}Focus`;
        let nameInternalValue = `${name}InternalValue`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        let internalValue = "";
        if(e !== null){
            error = false;
            let { label, value } = e;
            let object = this.props.paymentDate.data.find((item => item.cod_valor ===  value))
            valueDefault = label;
            idDefault = value;
            internalValue = object.valor_interno;

        }
        this.props.handleSetState(state => ({
            client:{
                ...state.client,
                [name]: valueDefault, 
                [nameId]: idDefault,
                [nameInternalValue]:internalValue,
                [nameError]: error,
                [nameFocus]: false
            }
        }));
    }
    // Notistack
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render(){
        let {   state, 
                classes,
                paymentDate,
                sendCorrespondence,
                accountStatus,
                media } = this.props;
        let { client } = state;
        // Format Extra Payment Date 
        let paymentDateData = paymentDate.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        })); 

        // Format Extra Send Correspondence
        let sendCorrespondenceData = sendCorrespondence.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        })); 

        // Format Extra Account Status
        let accountStatusData = accountStatus.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        })); 
        // Format Extra Media
        let extraMediaData = media.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        })); 
        
        return (
            <ExpansionPanel className={classes.root}>
                <ExpansionPanelSummary 
                    className={classes.expansionPanel}
                    expandIcon={<ExpandMoreIcon fontSize="small" />}>
                    <Typography className={classes.heading}>
                        <LocalOfferIcon fontSize="small" className="mr-2"/>
                        Beneficios y Funcionalidades
                    </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails >
                <Grid container spacing={8}>
                    <Grid item xs={12}>
                        <Grid container spacing={8}>
                            <SelectInfo 
                                paymentDate={paymentDateData}
                                sendCorrespondence={sendCorrespondenceData}
                                accountStatus={accountStatusData}
                                extra={client.extra}
                                handleChangeSelectPaymentDate={this.handleChangeSelectPaymentDateFullRequired}
                            />
                            <SearchPromotor 
                                handleKeyPressTextFieldScannerCode={this.handleKeyPressTextFieldScannerCode}
                                handleChangeTextFieldScannerCode={this.handleChangeTextFieldScannerCode}
                                extra={client.extra}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        )
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(HomeAddressInfoPanel)));