
import React from 'react';
import FocusLock from 'react-focus-lock';
// Components 
import { 
    Grid,
    InputAdornment, 
    TextField
} from '@material-ui/core';
// Custom Components
import Autocomplete from '../../../../../../../components/Autocomplete';
// Icons
import PlaceIcon from '@material-ui/icons/Place';

const ViaAddress = ({  
            via = [],
            handleChangeSelectVia,
            handleChangeTextFieldNameVia,
            defaultLength,
            address
        }) => (
        <Grid container spacing={8}> 
            {/* Client - Home Address - Via */}
            <Grid item xs={12} sm={6} md={4} lg={2}>
                <FocusLock disabled={!address.viaFocus}>
                    <div>
                        <Autocomplete 
                            error={address.viaError}
                            onChange={e => handleChangeSelectVia(e, "via")}
                            value={address.viaId}
                            data={via}
                            placeholder={"Vía *"}
                        /> 
                    </div>
                </FocusLock>
            </Grid>
            {/* Client - Home Address - Name Via*/}
            <Grid item xs={12} sm={6} md={8} lg={6}>
                <TextField
                    error={address.nameViaError}
                    fullWidth={true}
                    required
                    label="Nombre Vía"
                    type="text"
                    margin="normal"
                    color="default"
                    value={address.nameVia}
                    name="nameVia"
                    onChange={handleChangeTextFieldNameVia}
                    InputProps={{
                        inputProps:{
                            maxLength: defaultLength,
                        },
                        endAdornment: (
                            <InputAdornment position="end">
                                <PlaceIcon color={address.nameViaError? "secondary":"inherit"} fontSize="small"/>
                            </InputAdornment>
                        )
                    }}
                />
            </Grid> 
        </Grid>
    )

export default React.memo(ViaAddress);