
import React from 'react';
// Components
import {
    TextField,
    InputAdornment, 
    FormHelperText
} from '@material-ui/core';

//Icons 
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';

const ReferenceAddress = ({
    fullLength=255,
    reference, 
    handleChangeTextFieldReference
}) => (
    <>
        <TextField
            fullWidth={true}
            label="Referencia del Domicilio"
            multiline
            rows="1"
            value={reference}
            name="reference"
            onChange={handleChangeTextFieldReference}
            type="text"
            margin="normal"
            InputProps={{
                inputProps:{
                    maxLength: fullLength,
                },
                endAdornment: (
                    <InputAdornment position="end">
                        <LocalLibraryIcon fontSize="small" />
                    </InputAdornment>
                )
            }}
        />
        <FormHelperText className="text-right">
            { reference.length }/255
        </FormHelperText>
    </>
);


export default React.memo(ReferenceAddress);