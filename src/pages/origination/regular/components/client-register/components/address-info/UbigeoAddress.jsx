
import React from 'react';
import Grid from '@material-ui/core/Grid';
import FocusLock from 'react-focus-lock';
import Autocomplete from '../../../../../../../components/Autocomplete';

const UbigeoAddress = ({   ubigeo,
            department = [],
            province = [],
            district = [],
            handleChangeSelectDepartment,
            handleChangeSelectProvince, 
            handleChangeSelectDistrict
        }) => (
        <Grid container spacing={8}>
            {/* Client - Home Address - Department */}
            <Grid item xs={12} sm={6} md={4} lg={4}> 
                <FocusLock disabled={!ubigeo.departmentFocus}>
                    <div>
                        <Autocomplete 
                            loading={ubigeo.departmentLoading}
                            disabled={ubigeo.departmentDisabled}
                            error={ubigeo.departmentError}
                            onChange={(e) => handleChangeSelectDepartment(e, "department")}
                            data={department}
                            value={ubigeo.departmentId}
                            placeholder={"Departamento *"}
                        /> 
                    </div>
                </FocusLock>
            </Grid>
            {/* Client - Home Address - Province */}
            <Grid item xs={12} sm={6} md={4} lg={4}> 
                <FocusLock disabled={!ubigeo.provinceFocus}>
                    <div>                        
                        <Autocomplete 
                            loading={ubigeo.provinceLoading}
                            disabled={ubigeo.provinceDisabled}
                            error={ubigeo.provinceError}
                            onChange={(e) => handleChangeSelectProvince(e, "province")}
                            data={province}
                            value={ubigeo.provinceId}
                            placeholder={"Provincia *"}
                        /> 
                    </div>
                </FocusLock>
            </Grid>
            {/* Client - Home Address - District */}
            <Grid item xs={12} sm={6} md={4} lg={4}> 
                <FocusLock disabled={!ubigeo.districtFocus}>
                    <div>   
                        <Autocomplete 
                            loading={ubigeo.districtLoading}
                            disabled={ubigeo.districtDisabled}
                            error={ubigeo.districtError}
                            onChange={(e) => handleChangeSelectDistrict(e, "district")}
                            value={ubigeo.districtId}
                            data={district}
                            placeholder={"Distrito *"}
                        /> 
                        </div>
                </FocusLock>
            </Grid>
        </Grid>
    )

export default React.memo(UbigeoAddress);