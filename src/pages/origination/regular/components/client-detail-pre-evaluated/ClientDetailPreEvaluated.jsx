// React
import React, { Component } from "react";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";
// Components
import Grid from "@material-ui/core/Grid";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import ItemDetailPreEvaluated from "./ItemDetailPreEvaluated";
import ItemExtraDetailPreEvaluated from "./ItemExtraDetailPreEvaluated";
import CircularProgress from "@material-ui/core/CircularProgress";
import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";
// import RedirectProcess from "../../../../../components/redirect/RedirectProcess";

// Effects
import Fade from "react-reveal/Fade";
import Bounce from "react-reveal/Bounce";
// Icons
import RemoveIcon from "@material-ui/icons/Remove";
import CloseIcon from "@material-ui/icons/Close";
import SendIcon from "@material-ui/icons/Send";
import CancelIcon from "@material-ui/icons/Cancel";
// Utils
import { getMoneyFormat } from "../../../../../utils/Utils";
//Effect
import Zoom from "@material-ui/core/Zoom";

const styles = (theme) => ({
  root: {
    maxWidth: 1000,
  },
  button: {
    textTransform: "none",
  },
  wrapper: {
    position: "relative",
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
    textTransform: "none",
  },
  cancelButtonProgress: {
    color: red[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
    textTransform: "none",
  },
  reason: {
    minHeight: 90,
    backgroundColor: "white"
  },
  separator: {
    [theme.breakpoints.down("xs")]: {
      borderLeft: "none",
    },
    [theme.breakpoints.up("sm")]: {
      borderLeft: "1px solid #bdbdbd",
    },
    [theme.breakpoints.up("md")]: {
      borderLeft: "1px solid #bdbdbd",
    },
    [theme.breakpoints.up("lg")]: {
      borderLeft: "1px solid #bdbdbd",
    },
  },
});

class ClientDetailPreEvaluated extends Component {
  state = {
    result: -1,
    origination: {
      number: 0,
      fullNumber: 0,
    },
    client: {
      fullName: "",
      document: "",
      documentType: "",
      documentTypeId: 100001,
      documentTypeInternalValue: "",
      documentNumber: "",
      birthday: "",
      email: "",
    },
    adn: {
      pep: -1,
      lic: -1,
    },
    offer: {
      name: "",
      value: 0,
    },

    cancelButtonDisabled: false,
    fraudPreventionButtonDisabled: false,
    effectiveProvision: 0,
    validations: [],

    fraudPrevention: 1,

    isRegular: 1,
    PEP: 1,
    PIB: 1,
    isProcessSiebel: 0,
    isProcessODC: 0,
    isProcessTcMaster: 0,
    sae: null,
    segment: "",

    minLineAvailableTc: 0
  };
  componentWillMount() {
    let {
      origination,
      client,
      firsCallCda,
      secondCallCda,
      adn,
      validations,
      sae,
      constantODC
    } = this.props;
    let {
      isRegular,
      PEP,
      PIB,
      isProcessSiebel,
      isProcessODC,
      isProcessTcMaster,
      offer,
      effectiveProvision,
      segment
    } = this.state;

    if (adn) {
      if (!adn && Object.keys(adn).length === 0) {
        PEP = -1;
      }
    }

    if (firsCallCda) {
      if (!firsCallCda && Object.keys(firsCallCda).length === 0) {
        PIB = -1;
      }
    }

    // High Restrictión
    if (secondCallCda && Object.keys(secondCallCda).length !== 0) {
      // CDA
      let { solicitante, salidaCDA } = secondCallCda.solicitud;
      let { porcentajeDisponibleEfectivo, oferta, ofertaFinal, segmento } = salidaCDA;
      let { experienciaCrediticea, fechaNacimiento } = solicitante;
      // Set data offer
      offer = {
        name: "Flujo Express",
        value: oferta ? oferta : 0,
      };
      // Regular Flow
      if (experienciaCrediticea === "0" || experienciaCrediticea === "20") {
        // Set data offer
        offer = {
          ...offer,
          name: "Flujo Regular",
          value: ofertaFinal ? ofertaFinal : 0,
        };
        // Is client Regular
        isRegular = 1;
      }

      client = {
        ...client,
        document: `${client.documentType} - ${client.documentNumber}`,
        birthday: fechaNacimiento,
      };
      segment = segmento;
      effectiveProvision = porcentajeDisponibleEfectivo;
    }

    if (validations) {
      // Validate Color Ball
      validations.map((item) => {
        if (
            item.des_solicitud_actividad_log ===
            "Validación cliente pendiente en odc."
        ) {
          if (item.des_error_message_servicio) {
            isProcessODC = 1;
          }
        } else if (
            item.des_solicitud_actividad_log ===
            "Consulta servicio cda primera llamada." ||
            item.des_solicitud_actividad_log ===
            "Consulta servicio cda segunda llamada."
        ) {
          if (item.des_error_message_servicio) {
            isRegular = 0;
          }
        } else if (
            item.des_solicitud_actividad_log === "Consulta servicio Pep."
        ) {
          if (item.des_error_message_servicio) {
            PEP = 0;
          }
        } else if (
            item.des_solicitud_actividad_log ===
            "Consulta servicio Lic y Fraudes."
        ) {
          if (item.des_error_message_servicio) {
            PIB = 0;
          }
        } else if (
            item.des_solicitud_actividad_log ===
            "Validación cliente pendiente en Siebel."
        ) {
          if (item.des_error_message_servicio) {
            isProcessSiebel = 1;
          }
        }
        else if (item.des_solicitud_actividad_log === "Validación tarjeta pendiente en maestro.") {
          if (item.des_error_message_servicio) {
              isProcessTcMaster = 1;
          }
        }
        return null;
      });
    }

    const lineaMinTC = constantODC?.data?.find(
      (item) => item.des_abv_constante === "LINEA_MIN_TC"
    ) || {};

    this.setState((state) => ({
      ...state,
      origination: {
        ...state.origination,
        ...origination,
      },
      client: client,
      effectiveProvision: effectiveProvision,
      offer: offer,
      PEP: PEP,
      PIB: PIB,
      isProcessSiebel: isProcessSiebel,
      isProcessODC: isProcessODC,
      isProcessTcMaster: isProcessTcMaster,
      isRegular: isRegular,
      validations: validations,
      sae: sae,
      segment: segment,
      minLineAvailableTc: lineaMinTC.valor_numerico || 0,
    }));
  }
  componentDidUpdate = (prevProps, prevState) => {
    // Activity -  Generic
    if (prevProps.genericActivity !== this.props.genericActivity) {
      if (this.props.genericActivity.loading) {
        this.setState((state) => ({
          ...state,
          fraudPreventionButtonDisabled: true,
          cancelButtonDisabled: true,
        }));
      }
      if (
        !this.props.genericActivity.loading &&
        this.props.genericActivity.response &&
        this.props.genericActivity.success
      ) {
        this.getNotistack("Consulta Correcta, 2do Paso Ok!", "success");
        setTimeout(() => {
          this.props.handleNext();
        }, 1500);
      } else if (
        !this.props.genericActivity.loading &&
        this.props.genericActivity.response &&
        !this.props.genericActivity.success
      ) {
        // Notistack
        this.getNotistack(this.props.genericActivity.error, "error");
        this.setState((state) => ({
          ...state,
          fraudPreventionButtonDisabled: false,
          cancelButtonDisabled: false,
        }));
      } else if (
        !this.props.genericActivity.loading &&
        !this.props.genericActivity.response &&
        !this.props.genericActivity.success
      ) {
        // Notistack
        this.getNotistack(this.props.genericActivity.error, "error");
        this.setState((state) => ({
          ...state,
          fraudPreventionButtonDisabled: false,
          cancelButtonDisabled: false,
        }));
      }
    }
    // Activity -  Cancel
    if (prevProps.cancelActivity !== this.props.cancelActivity) {
      if (this.props.cancelActivity.loading) {
        this.setState((state) => ({
          ...state,
          fraudPreventionButtonDisabled: true,
          cancelButtonDisabled: true,
        }));
      }
      if (
        !this.props.cancelActivity.loading &&
        this.props.cancelActivity.response &&
        this.props.cancelActivity.success
      ) {
        this.props.handleCancelRegular(); // Cancel

        this.setState((state) => ({
          ...state,
          fraudPreventionButtonDisabled: false,
          cancelButtonDisabled: false,
        }));
      } else if (
        !this.props.cancelActivity.loading &&
        this.props.cancelActivity.response &&
        !this.props.cancelActivity.success
      ) {
        // Notistack
        this.getNotistack(this.props.cancelActivity.error, "error");
        this.setState((state) => ({
          ...state,
          fraudPreventionButtonDisabled: false,
          cancelButtonDisabled: false,
        }));
      } else if (
        !this.props.cancelActivity.loading &&
        !this.props.cancelActivity.response &&
        !this.props.cancelActivity.success
      ) {
        // Notistack
        this.getNotistack(this.props.cancelActivity.error, "error");
        this.setState((state) => ({
          ...state,
          fraudPreventionButtonDisabled: false,
          cancelButtonDisabled: false,
        }));
      }
    }
  };
  // Notistack
  getNotistack(message, variant = "default", duration = 6000) {
    let select = "default";
    switch (variant) {
      case "error":
        select = variant;
        break;
      case "success":
        select = variant;
        break;
      case "warning":
        select = variant;
        break;
      case "info":
        select = variant;
        break;
      default:
        select = variant;
        break;
    }
    // Notistack
    this.props.enqueueSnackbar(message, {
      variant: select,
      autoHideDuration: duration,
      action: (
        <IconButton>
          <CloseIcon size="small" className="text-white" color="inherit" />
        </IconButton>
      ),
    });
  }

  handleCancelRegular = (e) => {
    e.preventDefault();
    this.props.handleOpenModalCancelRegular();
  };

  // Send
  handlePreEvaluatedClient = _ => {
    const { client, offer, effectiveProvision, sae, segment } = this.state;
    let data = {
      client: {
        ...client,
      },
      lineAvailable: offer.value ? offer.value : 0,
      effectiveProvision: effectiveProvision ? effectiveProvision : 0,
      lineSAE: sae.lineSAE ? sae.lineSAE : 0,
      maxAmount: sae.maxAmount ? sae.maxAmount : 0,
      segment: segment
    };
    this.props.handlePreEvaluatedClient(data);
  };

  render() {
    let { classes } = this.props;
    let {
      origination,
      effectiveProvision,
      client,
      offer,
      validations,
      PIB,
      PEP,
      isRegular,
      isProcessODC,
      isProcessTcMaster,
      isProcessSiebel,
      minLineAvailableTc
    } = this.state;

    return (
      <div className="mb-3">
        <Grid
          container
          spacing={8}
          className="mb-2 border border-light-gray rounded"
        >
          <Grid item xs={12} className="bg-lightgray p-1">
            <Fade>
              <Typography
                className={classNames(
                  "p-sm-0 text-center text-sm-left text-uppercase"
                )}
              >
                <Typography component="span" align="center" color="inherit">
                  Resultado de la Validación
                </Typography>
              </Typography>
            </Fade>
          </Grid>
          <Grid item xs={12} className="p-2 p-md-5">
            {/*
                            Detail
                        */}
            <Grid
              container
              spacing={8}
              className={classNames("d-flex justify-content-center ")}
            >
              <Grid item xs={12} lg={6}>
                <Grid container spacing={8}>
                  {/* Origination */}
                  <Grid item xs={12}>
                    <ItemExtraDetailPreEvaluated
                      data={{
                        name: "Nro. Solicitud",
                        value: origination.fullNumber,
                      }}
                    />
                  </Grid>
                  {/* Client - Document */}
                  <Grid item xs={12}>
                    <ItemExtraDetailPreEvaluated
                      data={{
                        name: "Documento",
                        value: client.document,
                      }}
                    />
                  </Grid>
                  {/* Pre-Evaluated */}
                  <Grid item xs={12}>
                    <ItemDetailPreEvaluated
                      data={{ name: "Cliente Regular", value: isRegular }}
                    />
                  </Grid>
                  {/* PIB */}
                  <Grid item xs={12}>
                    <ItemDetailPreEvaluated
                      data={{
                        name: "Politicas Internas del Banco",
                        value: PIB,
                      }}
                    />
                  </Grid>
                  {/* PEP */}
                  <Grid item xs={12}>
                    <ItemDetailPreEvaluated
                      data={{
                        name: "Personas Expuestas Politicamente",
                        initials: "PEP",
                        value: PEP,
                      }}
                    />
                  </Grid>
                  {/* Offer SAE */}
                  <Grid item xs={12}>
                    <ItemExtraDetailPreEvaluated
                      data={{
                        name: "Disposición de Efectivo (%)",
                        value: `${effectiveProvision}%`,
                      }}
                    />
                  </Grid>
                  {/* Offer */}
                  <Grid item xs={12}>
                    <ItemExtraDetailPreEvaluated
                      data={{
                        name: `Disposición de Efectivo (S/)`,
                        value: `S/ ${getMoneyFormat(
                          (effectiveProvision * offer.value) / 100
                        )}`,
                      }}
                    />
                  </Grid>

                  {/* */}
                  <Grid item xs={12}>
                    <ItemExtraDetailPreEvaluated
                      data={{
                        name: <>Línea de Crédito (S/) <b><i>(Min: { getMoneyFormat(minLineAvailableTc) })</i></b></>,
                        value: `S/ ${getMoneyFormat(offer.value)}`,
                      }}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>

            <Grid
              container
              spacing={8}
              className={classNames("d-flex justify-content-center")}
            >
              <Grid item xs={12} lg={6}>
                <Fade>
                  <div className="mb-2">
                    <Typography align="left">
                      <Typography component="span">
                        <strong>Motivo:</strong>
                      </Typography>
                    </Typography>
                  </div>
                  <div
                    className={classNames(
                      classes.reason,
                      "p-2 border border-light-gray rounded"
                    )}
                  >
                    {validations &&
                      validations.map((item, index) => {
                        if (!item.des_error_message_servicio) {
                          return null;
                        } else if (
                          item.des_error_message_servicio ===
                          "Cliente no encontrado-"
                        ) {
                          return null;
                        } else if (
                          item.des_solicitud_actividad_log ===
                          "Consulta servicio cda primera llamada."
                        ) {
                          return null;
                        }
                        return (
                          <Typography
                            align="left"
                            color="secondary"
                            key={index}
                          >
                            <Typography
                              color={
                                item.des_error_message_servicio ===
                                "Validación CONFORME. Por favor continuar con el proceso."
                                  ? "primary"
                                  : "secondary"
                              }
                              component="span"
                              className="d-flex align-items-start align-items-sm-center"
                            >
                              <RemoveIcon fontSize="small" color="inherit" />
                              {item.des_error_message_servicio}
                            </Typography>
                          </Typography>
                        );
                      })}
                  </div>
                </Fade>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid
          container
          spacing={8}
          className="d-flex justify-content-center align-items-center"
        >
          {PIB === 1 &&
          PEP === 1 &&
          isRegular === 1 &&
          isProcessODC === 0 &&
          isProcessTcMaster === 0 &&
          isProcessSiebel === 0 ? (
            <React.Fragment>
              <Grid item xs={12} sm={6} md={6} lg={3}>
                <Tooltip
                  TransitionComponent={Zoom}
                  title="Cancelar Originación."
                >
                  <div>
                    <Bounce left>
                      <div className={classNames(classes.wrapper)}>
                        <Button
                          className={classNames(classes.button)}
                          fullWidth={true}
                          variant="contained"
                          color="secondary"
                          size="small"
                          onClick={this.handleCancelRegular}
                          disabled={this.state.cancelButtonDisabled}
                          margin="normal"
                        >
                          Cancelar
                          <CancelIcon fontSize="small" className="ml-2" />
                        </Button>
                      </div>
                    </Bounce>
                  </div>
                </Tooltip>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={3}>
                <Tooltip
                  TransitionComponent={Zoom}
                  title="Continuar con el proceso de originación."
                >
                  <div>
                    <Bounce right>
                      <div className={classNames(classes.wrapper)}>
                        <Button
                          className={classNames(classes.button)}
                          fullWidth={true}
                          variant="contained"
                          color="primary"
                          size="small"
                          onClick={this.handlePreEvaluatedClient}
                          autoFocus
                          disabled={this.state.fraudPreventionButtonDisabled}
                          margin="normal"
                        >
                          Continuar
                          <SendIcon fontSize="small" className="ml-2" />
                          {this.state.fraudPreventionButtonDisabled && (
                            <CircularProgress
                              size={24}
                              className={classes.buttonProgress}
                            />
                          )}
                        </Button>
                      </div>
                    </Bounce>
                  </div>
                </Tooltip>
              </Grid>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Grid item xs={12} sm={12} md={6} lg={3}>
                <Bounce>
                  <div className={classNames(classes.wrapper)}>
                    <Button
                      className={classNames(classes.button)}
                      fullWidth={true}
                      variant="contained"
                      color="secondary"
                      size="small"
                      onClick={this.handleCancelRegular}
                      margin="normal"
                      autoFocus
                    >
                      Cancelar
                      <CancelIcon fontSize="small" className="ml-2" />
                    </Button>
                  </div>
                </Bounce>
              </Grid>
            </React.Fragment>
          )}
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(withSnackbar(ClientDetailPreEvaluated));
