// React
import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components
import { 
    AppBar,
    FormControl, 
    Button,
    IconButton,
    Grid,
    Tooltip, 
    Dialog, 
    DialogTitle,
    DialogContent, 
    DialogActions, 
    TextField, 
    Typography, 
    FormHelperText,
    CircularProgress } from '@material-ui/core';
// Components Custom
import Autocomplete from '../../../../../components/Autocomplete'
import CreditCard from '../../../../../components/CreditCard';
// Color 
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';
import yellow from '@material-ui/core/colors/yellow';
// Icons 
import SendIcon from '@material-ui/icons/Send';
import CloseIcon from '@material-ui/icons/Close'; 
// Effects
import Zoom from 'react-reveal/Zoom';
import Bounce from 'react-reveal/Bounce';
// Actions
import { getReasonEmbossing } from '../../../../../actions/value-list/reason-embossing';
// Utils
import { onlyTextKeyCode, checkInputKeyCode, defaultLengthInput } from '../../../../../utils/Utils';


const styles = theme => ({
    root:{
        minWidth:250, 
        maxWidth:320
    },
    modalRoot:{
        minWidth:280, 
        width:350,
        maxWidth:400,
    },
    buttonActions:{
        display:"flex", 
        justifyContent:"center", 
        alignItems:"center"
    },

    button: {
        textTransform: 'none',
    }, 
    buttonPendingProgress: {
        color: yellow[800],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    cancelButtonProgress:{
        color: red[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    buttonProgressWrapper: {
        position: 'relative',
    }, 
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    pendingEmbossingButton: {
        backgroundColor: yellow[800],
        transition:".3s",
        '&:hover': {
            backgroundColor: yellow[900],
        },
    },
    option: {
        color: "rgba(0, 0, 0, 0.87)",
        height: "24px",
        paddingLeft: "16px",
        paddingRight: "16px",
        fontSize: "14px",
        boxSizing: "content-box",
        fontWeight: "400",
        fontFamily: ["Roboto", "Helvetica", "Arial", "sans-serif"],
        lineHeight: "1.5em",
        whiteSpace: "nowrap",
        '&:hover':{
            backgroundColor: "rgba(0, 0, 0, 0.14)"
        }
    }, 
    appBar: {
        position: 'relative',
    },
});

const mapStateToProps = (state, props) => {
    return {
        reasonEmbossing:state.reasonEmbossingReducer,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getReasonEmbossing: bindActionCreators(getReasonEmbossing, dispatch),
    };
    return actions;
}

class EmbossingCreditCard extends Component {
    state = {
        reasonId:-1,
        reason:"",
        reasonError:false,
        description:"",
        descriptionError:false,

        pendingEmbossingModal:false, 
        embossingCreditCardButtonDisabled:false, 
        embossingPendingCreditCardButtonDisabled:false,
        acceptEmbossingPendingCreditCardButtonDisabled: false, 
        cancelEmbossingPendingCreditCardButtonDisabled: false,
        
        client:{

        },
        creditCard:{
            bin: 550218,
            brand: "MasterCard",
            brandId: 200001,
            color: "",
            colorAux: "#b69f4f",
            colorId: 0,
            name: "CENCOSUD MASTERCARD ORO",
            productId: 10005,
            type: "Gold",
            colorDisabled: false, 
            colorError: false,
            cardNumber:"0000000000000000"
        }
    }
    componentWillMount = () => {
        let { client, creditCard } = this.props;
        if(client && creditCard ){
            this.setState(state => ({
                client:{
                    ...state.client,
                    ...client
                }, 
                creditCard:{
                    ...state.creditCard,
                    ...creditCard
                }
            }));
        }
    }

    componentDidUpdate = (prevProps, prevState) => {
        // Reason
        if(prevProps.reasonEmbossing !== this.props.reasonEmbossing) {
            if(!this.props.reasonEmbossing.loading && this.props.reasonEmbossing.response && !this.props.reasonEmbossing.success){
                // Notistack
                this.getNotistack(this.props.reasonEmbossing.error, "error");
            }
            else if(!this.props.reasonEmbossing.loading && !this.props.reasonEmbossing.response && !this.props.reasonEmbossing.success){
                // Notistack
                this.getNotistack(this.props.reasonEmbossing.error, "error");
            }
        }
        if(prevProps.genericActivity !==  this.props.genericActivity ){
            if(this.props.genericActivity.loading){
                this.setState(state => ({
                    embossingCreditCardButtonDisabled:true,
                    embossingPendingCreditCardButtonDisabled:true, 
                }));
            }
            if(!this.props.genericActivity.loading && this.props.genericActivity.response && this.props.genericActivity.success){
                if(this.props.genericActivity.next){
                    this.getNotistack("Consulta Correcta, 7to Paso Ok!", "success");
                    setTimeout(()=>{
                        this.props.handleNext();
                    }, 2500);
                }
            }
            else if(!this.props.genericActivity.loading && this.props.genericActivity.response && !this.props.genericActivity.success){
                // Notistack
                this.getNotistack(this.props.genericActivity.error, "error");
                this.setState(state => ({
                    embossingCreditCardButtonDisabled:false, 
                    embossingPendingCreditCardButtonDisabled:false
                }));
            }
            else if(!this.props.genericActivity.loading && !this.props.genericActivity.response && !this.props.genericActivity.success){
                // Notistack
                this.getNotistack(this.props.genericActivity.error, "error");
                this.setState(state => ({
                    embossingCreditCardButtonDisabled:false,
                    embossingPendingCreditCardButtonDisabled:false
                }));
            }
        }
        if(prevProps.pendingEmbossing !==  this.props.pendingEmbossing ){
            if(this.props.pendingEmbossing.loading){
                this.setState(state => ({
                    embossingCreditCardButtonDisabled: true,
                    embossingPendingCreditCardButtonDisabled: true,
                    acceptEmbossingPendingCreditCardButtonDisabled: true, 
                    cancelEmbossingPendingCreditCardButtonDisabled: true,
                }));
            }
            if(!this.props.pendingEmbossing.loading && this.props.pendingEmbossing.response && this.props.pendingEmbossing.success){
                this.getNotistack("Pendiente de Emboce", "info");
                setTimeout(()=>{
                    this.props.handleReset();
                }, 1500);
            }
            else if(!this.props.pendingEmbossing.loading && this.props.pendingEmbossing.response && !this.props.pendingEmbossing.success){
                // Notistack
                this.getNotistack(this.props.pendingEmbossing.error, "error");
                this.setState(state => ({
                    embossingCreditCardButtonDisabled:false, 
                    embossingPendingCreditCardButtonDisabled:false, 
                    acceptEmbossingPendingCreditCardButtonDisabled: false, 
                    cancelEmbossingPendingCreditCardButtonDisabled: false,
                }));
            }
            else if(!this.props.pendingEmbossing.loading && !this.props.pendingEmbossing.response && !this.props.pendingEmbossing.success){
                // Notistack
                this.getNotistack(this.props.pendingEmbossing.error, "error");
                this.setState(state => ({
                    embossingCreditCardButtonDisabled:false,
                    embossingPendingCreditCardButtonDisabled:false, 
                    acceptEmbossingPendingCreditCardButtonDisabled: false, 
                    cancelEmbossingPendingCreditCardButtonDisabled: false,
                }));
            }
        }
    }
    // Pending Embossing Open
    handleClickOpenModalPendingEmbossing = e => {
        let { reasonEmbossing } = this.props;
        if(reasonEmbossing.data.length === 0){
            this.props.getReasonEmbossing();
        }
        this.setState(state => ({
            pendingEmbossingModal:true
        }));
    }
    // Pending Embossing Close
    handleClickCloseModalPendingEmbossing = e => {
        this.setState(state => ({
            pendingEmbossingModal:false, 
            reasonId:-1,
            reason:"",
            reasonError:false,
            description:"",
            descriptionError:false,
        }));
    }
    // Reason - Select Event Change
    handleChangeSelectReason = name => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }
        this.setState(state => ({
            [name]: valueDefault, 
            [nameId]: idDefault,
            [nameError]: error,
        }));
    }
    // Client - TextField - Event Change - Required    
    handleChangeTextFieldRequired = name => e => {
        e.persist();
        let { value } = e.target;
        let nameError = `${name}Error`;
        this.setState(state => ({
            ...state,
            [name]: value, 
            [nameError]: value !== "" ? false: true
        }));
    }
    // Only Text
    handleKeyPressTextFieldOnlyText = name => e =>{
        let code = (e.which) ? e.which : e.keyCode;
        if(!onlyTextKeyCode(code)){
            e.preventDefault();
        }       
    }

    // Check Input
    handleKeyPressTextFieldCheckInput = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!checkInputKeyCode(code)){
            e.preventDefault();
        }   
    }
    // Pending Embossing Form
    handleSubmitPendingEmbossingForm = e => {
        e.preventDefault();
        let reasonError =  true;
        if(this.state.reason !== ""){
            reasonError = false;
        }
        if(!reasonError){
            let data = {
                reason:{
                    id:this.state.reasonId,
                    label: this.state.reason,
                    description: this.state.description
                }
            }
            this.props.handlePendingEmbossingCreditCard(data);
        }
        else{
            if(reasonError){
                this.getNotistack(`Motivo: verificar por favor.`, "warning");
            }
            this.setState(state => ({
                ...state,
                reasonError:reasonError
            }));
        }
    }
    // Send Data
    handleClickEmbossingCreditCardForm = e => {
        e.preventDefault();
        this.props.handleEmbossingCreditCard();
    }
    // Notistack 
    getNotistack(message, variant="default", duration = 8000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }

    render() {
        let { classes, reasonEmbossing } = this.props;
        let { creditCard, client } = this.state;

        // Format Reason 
        let reasonEmbossingData = reasonEmbossing.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor,
        })); 

        return (
            <Grid container spacing={8}>
                <Grid item xs={12} className="d-flex justify-content-center mb-3">
                    <CreditCard 
                        client={client.shortName}
                        brand={creditCard.brandId === 200001? 0 : 1} 
                        type={creditCard.type}
                        customColor={creditCard.colorAux}
                        cardNumber={creditCard.cardNumber}/>
                </Grid>
                <Grid item xs={12}>
                    <form method="post" onSubmit={this.handleClickEmbossingCreditCardForm} autoComplete="off">
                        <Grid container spacing={8} className={classes.buttonActions}>
                            {/* Button - Pending Embossing */}
                            <Grid item xs={12} sm={12} md={4} lg={3}>
                                <Tooltip TransitionComponent={Zoom} title="Colocar solicitud como pendiente de emboce.">
                                    <div>
                                        <Bounce>
                                            <div className={classNames(classes.buttonProgressWrapper)}>
                                                <Button
                                                    disabled={this.state.embossingPendingCreditCardButtonDisabled}
                                                    variant="contained"
                                                    type="button"
                                                    onClick={this.handleClickOpenModalPendingEmbossing} 
                                                    className={classNames(classes.button, classes.pendingEmbossingButton)}
                                                    color="primary" 
                                                    size="small"
                                                    margin="normal"
                                                    fullWidth={true}>
                                                    Pendiente de Emboce
                                                </Button>
                                            </div>
                                        </Bounce>
                                    </div>
                                </Tooltip>
                            </Grid>
                            {/* Button - Ok */}
                            <Grid item xs={12} sm={12} md={4} lg={3}>
                                <Tooltip TransitionComponent={Zoom} title="Click para continuar con el proceso de reimpresión.">
                                    <div>
                                        <Bounce>
                                            <div className={classNames(classes.buttonProgressWrapper)}>
                                                <Button
                                                    disabled={this.state.embossingCreditCardButtonDisabled}
                                                    variant="contained"
                                                    type="submit" 
                                                    className={classNames(classes.button)}
                                                    color="primary" 
                                                    size="small"
                                                    margin="normal"
                                                    fullWidth={true}>
                                                    Tarjeta Embosada
                                                    <SendIcon fontSize="small" className="ml-2" />
                                                    {
                                                        this.state.embossingCreditCardButtonDisabled && <CircularProgress size={24} className={classes.buttonProgress} />
                                                    }
                                                </Button>
                                            </div>
                                        </Bounce>
                                    </div>
                                </Tooltip>
                            </Grid>
                        </Grid> 
                    </form>
                </Grid>
                <Dialog
                    onClose={this.handleClickCloseModalPendingEmbossing}
                    open={this.state.pendingEmbossingModal}
                    aria-labelledby="form-dialog">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-dialog" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                    Pendiente de Emboce
                            </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form onSubmit={this.handleSubmitPendingEmbossingForm} className={classes.modalRoot} autoComplete="off">
                        <DialogContent>
                            <Grid container spacing={8}>
                                <Grid item xs={12}>
                                    <FormControl fullWidth error={this.state.reasonError}>
                                        <Autocomplete 
                                            autoFocus={true}
                                            error={this.state.reasonError}
                                            onChange={this.handleChangeSelectReason("reason")}
                                            value={this.state.reasonId}
                                            data={reasonEmbossingData} 
                                            placeholder="Seleccionar motivo"/>
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12}>
                                    <TextField
                                        error={this.state.descriptionError}
                                        fullWidth={true}
                                        multiline
                                        rows="4"
                                        label="Descripción"
                                        type="text"
                                        margin="normal"
                                        color="default"
                                        name="description"
                                        value={this.state.description}
                                        onKeyPress={this.handleKeyPressTextFieldCheckInput("description")}
                                        onChange={this.handleChangeTextFieldRequired("description")}
                                        InputProps={{
                                            inputProps:{
                                                maxLength: defaultLengthInput,
                                            }
                                        }}
                                    />
                                     <FormHelperText className="text-right">
                                        {this.state.description.toString().length}/{defaultLengthInput}
                                    </FormHelperText>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            disabled={this.state.cancelEmbossingPendingCreditCardButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth={true} 
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickCloseModalPendingEmbossing}
                                            margin="normal">
                                            Cancelar
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={this.state.acceptEmbossingPendingCreditCardButtonDisabled}
                                            className={classNames(classes.button)}
                                            type="submit" 
                                            margin="normal"
                                            color="primary" 
                                            size="small"
                                            fullWidth={true}>
                                            Aceptar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                                this.state.acceptEmbossingPendingCreditCardButtonDisabled && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>
            </Grid>
        )
    }
}
export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(EmbossingCreditCard)));

