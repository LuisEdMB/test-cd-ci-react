import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Switch, Redirect } from 'react-router-dom';
import PrivateRoute from '../../utils/PrivateRoute';
import { SnackbarProvider } from 'notistack';
import ExpressPage from './express/ExpressPage';
import ExpressCallCenterPage from './express/ExpressCallCenterPage';
import ExistingClientPage from './existing-client/ExistingClientPage'
import ExistingClientCallCenterPage from './existing-client/ExistingClientCallCenterPage'
import PedingPage from './express/pending/PendingPage';
import AuthorizePage from './express/authorize/AuthorizePage';
import CancellationPage from './express/cancellations/CancellationPage';
import ActivateSAEPage from './express/activate-sae/ActivateSAEPage';

const mapStateToProps = (state, props) => {
    return {
        session: state.sessionReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {

    };
    return actions;
}

const ExpressPageRoute = ({ match, session }) =>
( <Switch>
        <Redirect exact from={`${match.path}`}to={`${match.path}/nueva-solicitud`}/>
        <PrivateRoute 
            exact
            path={`${match.path}/nueva-solicitud`} 
            component={ExpressPage}
            authenticated={session.authenticated}
        />
        <PrivateRoute 
            exact
            path={`${match.path}/nueva-solicitud/:solicitud`} 
            component={ExpressPage}
            authenticated={session.authenticated}
        />
        <PrivateRoute 
            exact
            path={`${match.path}/call-center-nueva-solicitud`} 
            component={ExpressCallCenterPage}
            authenticated={session.authenticated}
        />
        <PrivateRoute
            exact
            path={`${match.path}/clientes-existentes`}
            component={ExistingClientPage}
            authenticated={session.authenticated}
        />
        <PrivateRoute
              exact
              path={`${match.path}/clientes-existentes/:solicitud`}
              component={ExistingClientPage}
              authenticated={session.authenticated}
        />
        <PrivateRoute 
            exact
            path={`${match.path}/call-center-clientes-existentes`} 
            component={ ExistingClientCallCenterPage }
            authenticated={session.authenticated}
        />
        <PrivateRoute 
            path={`${match.path}/en-proceso`} 
            component={(props) => <SnackbarProvider max={3} > <PedingPage {...props}/> </SnackbarProvider>}
            authenticated={session.authenticated}
        />
        <PrivateRoute 
            path={`${match.path}/por-autorizar`} 
            component={(props) => <SnackbarProvider max={3} > <AuthorizePage {...props}/> </SnackbarProvider>}
            authenticated={session.authenticated}
        />
        <PrivateRoute 
            path={`${match.path}/cancelaciones`} 
            component={(props) => <SnackbarProvider max={3} > <CancellationPage {...props}/> </SnackbarProvider>}
            authenticated={session.authenticated}
        />
        <PrivateRoute 
            path={`${match.path}/por-activar-cec`} 
            component={(props) => <SnackbarProvider max={3} > <ActivateSAEPage {...props}/> </SnackbarProvider>}
            authenticated={session.authenticated}
        />
    </Switch>
)


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ExpressPageRoute));
