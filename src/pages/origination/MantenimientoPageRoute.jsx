import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Switch, Redirect } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
import PrivateRoute from '../../utils/PrivateRoute';
import EmpresasPage from './mantenimiento/empresas/EmpresasPage';


const mapStateToProps = (state, props) => {
    return {
        session: state.sessionReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {

    };
    return actions;
}

const mantenimientoPageRoute = (props) => {
    let { match, session } = props;
    return (
        <Switch>
            <Redirect exact from={`${match.path}`} to={`${match.path}/empresas`} />
            <PrivateRoute
                path={`${match.path}/empresas`}
                component={(props) => <SnackbarProvider max={3} > <EmpresasPage {...props} /> </SnackbarProvider>}
                authenticated={session.authenticated}
            />
        </Switch>
    )
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(mantenimientoPageRoute));
