import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; 
// React Router 
//import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//import classNames from 'classnames';
//import IconButton from '@material-ui/core/IconButton';
import Typography from "@material-ui/core/Typography";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import CencosudScotiaBank from '../../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg';

const styles = theme => ({
    searchInput:{
        width: "100%",
        [theme.breakpoints.down('sm')]: {
            
        },
        [theme.breakpoints.up('md')]: {
        },
        [theme.breakpoints.up('lg')]: {
            width: 320,
        },

    },
    avatar:{
        width:80, 
        height:48
    }
});


const mapStateToProps = (state, props) => {
    return {
        session: state.sessionReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
  const actions = {
  
  };
  return actions;
}


class SearchPage extends Component {
    render(){
        let { classes } = this.props;
        return (
           <Grid container>
               <Grid item xs={12} className="my-2">
                    <Paper className="p-3">
                        <figure className="d-flex align-items-center">
                            <img
                                className={classes.avatar}
                                src={CencosudScotiaBank}
                                alt="Cencosud ScotiaBank" 
                                />
                            <figcaption className="w-100">
                                <Typography align="center"  variant="h5" component="h4">
                                    Búsqueda de Solicitud
                                </Typography>
                            </figcaption>
                        </figure>
                    </Paper>
               </Grid>
               <Grid item xs={12}>
                 
               </Grid>
           </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(SearchPage))));

