import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Switch, Redirect } from 'react-router-dom';
import PrivateRoute from '../../utils/PrivateRoute';
import RegularPage from './regular/RegularPage';
import RegularCallCenterPage from './regular/RegularCallCenterPage';

const mapStateToProps = (state) => {
    return {
        session: state.sessionReducer
    }
}

const RegularPageRoute = ({ match, session }) =>
( <Switch>
        <Redirect exact from={`${match.path}`}to={`${match.path}/nueva-solicitud`}/>
        <PrivateRoute 
            exact
            path={`${match.path}/nueva-solicitud`} 
            component={RegularPage}
            authenticated={session.authenticated}
        />
        <PrivateRoute 
            exact
            path={`${match.path}/nueva-solicitud/:solicitud`} 
            component={RegularPage}
            authenticated={session.authenticated}
        />
        <PrivateRoute 
            exact
            path={`${match.path}/call-center-nueva-solicitud`} 
            component={RegularCallCenterPage}
            authenticated={session.authenticated}
        />
    </Switch>
)


export default withRouter(connect(mapStateToProps)(RegularPageRoute));
