import React from 'react'
import { connect } from 'react-redux'
import { withRouter, Switch, Redirect } from 'react-router-dom'
import PrivateRoute from '../../utils/PrivateRoute'
// Page 
import OriginatedCreditCard from './reports/originated-credit-card/OriginatedCreditCard'
import OriginationOperations from './reports/origination-operations/OriginationOperations'
import ClientConsult from './reports/client-consult/ClientConsult'
import DigitalDocumentDaily from "./reports/digital-documents-daily/DigitalDocumentsDaily"
import OriginationOperationsSae from './reports/origination-operations-sae/OriginationOperationsSae'
import ActivationOperationSae from './reports/activation-operations-sae/ActivationOperationsSae'
import ActivatedCards from './reports/activated-cards/ActivatedCards'

const mapStateToProps = (state, props) => {
    return {
        session: state.sessionReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {

    };
    return actions;
}
const ReportPageRoute = (props) => {
    let { match, session } = props;
    return(
        <Switch>
            <Redirect exact from={`${match.path}`} to={`${match.path}/rep-ope-consultatc`}/>
            <PrivateRoute 
                exact
                path={`${match.path}/rep-ope-consultatc`} 
                component={OriginationOperations}
                authenticated={session.authenticated}
            />
             <PrivateRoute 
                exact
                path={`${match.path}/rep-oriagencias`} 
                component={OriginatedCreditCard}
                authenticated={session.authenticated}
            />
            <PrivateRoute 
                exact
                path={`${match.path}/rep-consulta-cliente`} 
                component={ClientConsult}
                authenticated={session.authenticated}
            />
            <PrivateRoute
                exact
                path={`${match.path}/rep-daily-report`}
                component={DigitalDocumentDaily}
                authenticated={session.authenticated}
            />
            <PrivateRoute
                exact
                path={`${match.path}/rep-ori-sae`}
                component={OriginationOperationsSae}
                authenticated={session.authenticated}
            />
            <PrivateRoute
                exact
                path={`${match.path}/rep-act-cec`}
                component={ActivationOperationSae}
                authenticated={session.authenticated}
            />
            <PrivateRoute
                exact
                path={`${match.path}/rep-act-tarjetas`}
                component={ActivatedCards}
                authenticated={session.authenticated}
            />
        </Switch>
    )
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ReportPageRoute));
