// React
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { Redirect, withRouter } from 'react-router'

// Material UI
import { Button, Paper, Step, StepContent, StepLabel, Stepper, Typography, withStyles } from '@material-ui/core'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcReprint from '../../../actions/odc-reprint/odc-reprint'

// Hooks
import useReduxState from '../../../hooks/general/useReduxState'

// Effects
import Fade from 'react-reveal/Fade'

// Notify
import { SnackbarProvider, withSnackbar } from 'notistack'

// Utils
import classNames from 'classnames'
import * as Constants from '../../../utils/Constants'
import * as Config from '../../../actions/config'
import ReprintFactory from '../../../utils/reprint/ReprintFactory'

// Components
import NewCommentForm from '../../../components/NewCommentForm/NewCommentForm'
import Timer from '../../../components/Timer'
import ClientConsult from './components/client-consult/ClientConsult'
import CreditCardDetail from './components/credit-card-detail/CreditCardDetail'
import ReprintDetail from './components/reprint-detail/ReprintDetail'
import ReprintConfirm from './components/reprint-confirm/ReprintConfirm'
import ReprintSummary from './components/reprint-summary/ReprintSummary'
import CreditCardEmboss from './components/credit-card-emboss/CreditCardEmboss'
import CreditCardActivation from './components/credit-card-activation/CreditCardActivation'
import ModalCancelOrigination from './components/extra-components/ModalCancelOrigination'
import BackgroundFinish from '../../../components/background-finish/BackgroundFinish'
import PreLoaderImage from '../../../components/PreLoaderImage'

const styles = theme => ({
    root: {
        width: '100%',
        position: 'relative'
    },
    stepLabelOk:{
        background: '#2196f31c',
        padding: '.5em',
        borderRadius: '.5em',
        boxShadow: '0px 1.2px 1px #007ab8',
        opacity: '1 !important'
    },
    commentFormButton: {
        position: 'fixed',
        top: '20vh',
        right: '5px',
        zIndex: 900,
    },
    resetContainer: {
        padding: theme.spacing.unit * 3,
        textAlign: 'center'
    }
})

const steps = [
    { id: 1, title: 'Reimpresión de Tarjeta - Consulta cliente' },
    { id: 2, title: 'Detalle Tarjeta' },
    { id: 3, title: 'Detalle Reimpresión' },
    { id: 4, title: 'Confirmar Reimpresión' },
    { id: 5, title: 'Resumen Reimpresión' },
    { id: 6, title: 'Emboce de Tarjeta' },
    { id: 7, title: 'Activación de Tarjeta' }
]

function ReprintPage(props) {
    const { classes, location, continueProcess, odcReprint, registerContinueProcessActivity } = props

    const reprint = new ReprintFactory(Constants.ReprintFactoryTypes.reprint).createReprint()
    const prevOdcReprint = useRef(odcReprint)

    const [ activeStep, setActiveStep ] = useState(0)
    const [ modalCancelActivity, setModalCancelActivity ] = useState(false)
    const [ dataCancelActivity, setDataCancelActivity ] = useState({})
    const [ stateReprint, setStateReprint ] = useState({})
    const [ isBiometricOk, setIsBiometricOk ] = useState(false)
    const [ loadingContinueProcess, setLoadingContinueProcess ] = useState(false)
    const [ errorContinueProcess, setErrorContinueProcess ] = useState(false)
    const [ phaseCodeFromContinueProcess, setPhaseCodeFromContinueProcess ] = useState({
        status: 0,
        message: ''
    })

    const [ dataContinueProcess, isLoadingContinueProcess, isSuccessContinueProcess, isErrorContinueProcess ] =
        useReduxState({ props: odcReprint.process, prevProps: prevOdcReprint.current.process })

    useEffect(_ => {
        handleReset()
        const { cod_solicitud_completa } = location.state?.dataUpdate || {}
        if (cod_solicitud_completa) {
            setLoadingContinueProcess(true)
            continueProcess(reprint.getDataForContinueProcessApi(cod_solicitud_completa))
        }
        window.addEventListener('beforeunload', handleReloadPage)
        return _ => window.removeEventListener('beforeunload', handleReloadPage)
    }, [])

    useEffect(_ => {
        if (isLoadingContinueProcess) setLoadingContinueProcess(true)
        if (isErrorContinueProcess) setErrorContinueProcess(true)
        if (isSuccessContinueProcess) {
            const data = dataContinueProcess?.solicitud_Retomar || { }
            const homeAddress = (data.list_ori_sel_solicitud_retomar_direccion || []).find(address => address.cod_tipo_direccion === 260001) || {}
            const principalClient = {
                documentTypeAux: data.cod_tipo_documento_titular || '',
                documentNumber: data.des_nro_documento_titular || '',
                documentTypeLetter: data.des_tipo_documento || ''
            }
            const clientOnReprint = {
                reqCode: data.cod_requerimiento || 0,
                solicitudeCode: data.cod_solicitud || 0,
                completeSolicitudeCode: data.cod_solicitud_completa || '',
                prevSolicitudCode: data.cod_solicitud_antigua || 0,
                prevCompleteSolicitudeCode: data.cod_solicitud_completa_antigua || '',
                documentTypeLetter: data.des_tipo_documento || '',
                documentNumber: data.des_nro_documento || '',
                documentTypeAux: data.cod_tipo_documento || 0,
                documentTypeInternalValue: data.des_tipo_documento_valor_interno || '',
                firstName: data.des_primer_nom || '',
                secondName: data.des_segundo_nom || '',
                firstLastName: data.des_ape_paterno || '',
                secondLastName: data.des_ape_materno || '',
                principalClientId: data.cod_cliente_titular || 0,
                clientId: data.cod_cliente || 0,
                nationality: data.des_nacionalidad || '',
                relationTypeId: data.cod_tipo_relacion || 0,
                typeSolicitudeOriginationCodeInitial: data.cod_tipo_solicitud_originacion_inicial || 0,
                reprintReasonId: data.cod_motivo_reimpresion || 0,
                homeAddress: {
                    departmentId: homeAddress.cod_ubi_departamento || '',
                    department: homeAddress.des_ubi_departamento || '',
                    districtId: homeAddress.cod_ubi_distrito || '',
                    nameVia: homeAddress.des_nombre_via || '',
                    number:  homeAddress.des_nro || '',
                    building: homeAddress.des_departamento || '',
                    inside: homeAddress.des_interior || '',
                    mz: homeAddress.des_manzana || '',
                    lot: homeAddress.des_lote || '',
                    nameZone: homeAddress.des_nombre_zona || '',
                    reference: homeAddress.des_referencia_domicilio || ''
                },
                creditCard: {
                    color: data.cod_valor_color || 0,
                    colorAux: data.cod_color_pro || data.des_valor_color_2 || '',
                    brand: data.cod_valor_marca || 0,
                    brandDescription: data.des_valor_marca || '',
                    productId: data.cod_producto || 0,
                    productDescription: data.des_nom_producto || data.des_nombre_producto || '',
                    productType: data.des_tipo_producto || '',
                    cardNumber: data.num_tarjeta_antigua || '',
                    newCardNumber: data.num_tarjeta_pmp || '',
                    newToken: data.token || '',
                    token: data.token_base || '', 
                    clientNumber: data.num_cliente_pmp || data.num_cliente_pmp_base || '',
                    accountNumber: data.num_cuenta_pmp || data.num_cuenta_pmp_base || '',
                    relationNumber: data.num_relacion_pmp || data.num_relacion_pmp_base || '',
                    bin: data.cod_bin_pro || 0
                }
            }
            setStateReprint({ principalClient, clientOnReprint })
            setActiveStep(reprint.redirectStep(data.cod_flujo_fase_estado || 0))
            setIsBiometricOk(data.flg_biometria || false)
            setPhaseCodeFromContinueProcess({
                status: data.cod_flujo_fase_estado || 0,
                message: data.des_error_message_servicio || ''
            })
            registerContinueProcessActivity(reprint.getDataForRegisterContinueProcessActivityApi({
                reqCode: data.cod_requerimiento || 0,
                solicitudeCode: data.cod_solicitud || 0,
                completeSolicitudeCode: data.cod_solicitud_completa || ''
            }))
            setLoadingContinueProcess(false)
        }
    }, [dataContinueProcess, isLoadingContinueProcess, isSuccessContinueProcess, isErrorContinueProcess])

    const handleReloadPage = e => {
        (e || window.event).returnValue = 'o/'
        return 'o/'
    }

    const handleNextStep = useCallback(_ => {
        setActiveStep(activeStep + 1)
    }, [activeStep])

    const handleReset = useCallback(_ => {
        setActiveStep(0)
        setModalCancelActivity(false)
        setDataCancelActivity({})
        setStateReprint({})
        setIsBiometricOk(false)
        setLoadingContinueProcess(false)
        setErrorContinueProcess(false)
        setPhaseCodeFromContinueProcess({
            status: 0,
            message: ''
        })
    }, [])

    const showModalCancelActivity = useCallback(data => {
        setDataCancelActivity(data)
        setModalCancelActivity(true)
    }, [])

    const handleSaveStateReprint = useCallback(data => {
        setStateReprint(state => ({
            ...state,
            ...data
        }))
    }, [setStateReprint])

    if (errorContinueProcess) return <Redirect to={{ pathname: `/${ Config.URL_BASE }/error`, }} />
    if (loadingContinueProcess) return <PreLoaderImage text='Retomando proceso, espere por favor ...' />
    return (
        <SnackbarProvider
            maxSnack={ 3 }>
                <div
                    className={ classes.root }>
                        {
                            activeStep >= 5 && <Timer />
                        }
                        <div
                            className={ classes.commentFormButton }>
                                <NewCommentForm
                                    fixed
                                    codSolicitud={ stateReprint.clientOnReprint?.solicitudeCode || 0 }
                                    codSolicitudCompleta={ stateReprint.clientOnReprint?.completeSolicitudeCode || '' } />
                        </div>
                        <Stepper
                            activeStep={ activeStep }
                            orientation='vertical'>
                                {
                                    steps.map(step => 
                                        <Step
                                            key={ step.id }>
                                                <StepLabel>
                                                    <Fade>
                                                        <Typography
                                                            color='primary'
                                                            className={ classNames(classes.stepLabelOk, 'text-uppercase') }>
                                                                { step.title }
                                                        </Typography>
                                                    </Fade>
                                                </StepLabel>
                                                <StepContent>
                                                    <div
                                                        className='py-4'>
                                                            {
                                                                step.id === 1
                                                                    ? <ClientConsult
                                                                        reprintType={ Constants.ReprintFactoryTypes.reprint }
                                                                        handleSaveStateReprint={ handleSaveStateReprint }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 2
                                                                    ? <CreditCardDetail
                                                                        reprintType={ Constants.ReprintFactoryTypes.reprint }
                                                                        stateReprint={ stateReprint }
                                                                        showModalCancelActivity={ showModalCancelActivity }
                                                                        handleSaveStateReprint={ handleSaveStateReprint }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 3
                                                                    ? <ReprintDetail
                                                                        reprintType={ Constants.ReprintFactoryTypes.reprint }
                                                                        stateReprint={ stateReprint }
                                                                        handleSaveStateReprint={ handleSaveStateReprint }
                                                                        showModalCancelActivity={ showModalCancelActivity }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 4
                                                                    ? <ReprintConfirm
                                                                        reprintType={ Constants.ReprintFactoryTypes.reprint }
                                                                        stateReprint={ stateReprint }
                                                                        phaseCodeFromContinueProcess={ phaseCodeFromContinueProcess }
                                                                        handleSaveStateReprint={ handleSaveStateReprint }
                                                                        showModalCancelActivity={ showModalCancelActivity }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 5
                                                                    ? <ReprintSummary
                                                                        reprintType={ Constants.ReprintFactoryTypes.reprint }
                                                                        stateReprint={ stateReprint }
                                                                        handleNextStep={ handleNextStep } /> :
                                                                step.id === 6
                                                                    ? <CreditCardEmboss
                                                                        reprintType={ Constants.ReprintFactoryTypes.reprint }
                                                                        stateReprint={ stateReprint }
                                                                        handleNextStep={ handleNextStep }
                                                                        handleReset={ handleReset } /> :
                                                                step.id === 7
                                                                    ? <CreditCardActivation
                                                                        reprintType={ Constants.ReprintFactoryTypes.reprint }
                                                                        stateReprint={ stateReprint }
                                                                        handleNextStep={ handleNextStep }
                                                                        isBiometricOk={ isBiometricOk }
                                                                        phaseCodeFromContinueProcess={ phaseCodeFromContinueProcess }
                                                                        handleReset={ handleReset } /> :
                                                                    null
                                                            }
                                                    </div>
                                                </StepContent>
                                        </Step>
                                    )
                                }
                        </Stepper>
                        {
                            activeStep === steps.length && (
                                <Paper
                                    square
                                    elevation={ 0 }
                                    className={ classes.resetContainer }
                                    style={{ textAlign: 'center' }}>
                                        <BackgroundFinish 
                                            data={[{
                                                client: {
                                                    relationTypeId: stateReprint.clientOnReprint?.relationTypeId,
                                                    fullName: `${ stateReprint.clientOnReprint?.firstLastName || '' } ` +
                                                        `${ stateReprint.clientOnReprint?.secondLastName || '' } ` + 
                                                        `${ stateReprint.clientOnReprint?.firstName || '' } ` + 
                                                        `${ stateReprint.clientOnReprint?.secondName || '' }`
                                                },
                                                process: {
                                                    number: stateReprint.clientOnReprint?.solicitudeCode,
                                                    fullNumber: stateReprint.clientOnReprint?.completeSolicitudeCode,
                                                    typeSolicitudeCode: 340002,
                                                    typeSolicitudeReqCode: 350019,
                                                    typeSolicitudeOriginationCodeInitial: stateReprint.clientOnReprint?.typeSolicitudeOriginationCodeInitial,
                                                    rePrintMotiveCode: stateReprint.clientOnReprint?.reprintReasonId
                                                }
                                            }]} />
                                        <br />
                                        <Button
                                            variant='contained'
                                            color='primary'
                                            onClick={ handleReset }>
                                            Nueva Reimpresión
                                        </Button>
                                </Paper>
                            )
                        }
                        <ModalCancelOrigination
                            open={ modalCancelActivity }
                            close={ _ => setModalCancelActivity(false) }
                            data={ dataCancelActivity }
                            handleReset={ handleReset } />
                </div>
        </SnackbarProvider>
    )
}

function mapStateToProps(state) {
    return {
        odcReprint: state.odcReprintReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        continueProcess: bindActionCreators(ActionOdcReprint.continueProcess, dispatch),
        registerContinueProcessActivity: bindActionCreators(ActionOdcReprint.registerContinueProcessActivity, dispatch)
    }
}

export default withRouter(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ReprintPage))))
