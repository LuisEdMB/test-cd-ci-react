// React
import React, { useEffect, useRef, useState } from 'react'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../../utils/Notistack'

// Material UI
import { Grid, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Hooks
import useReduxState from '../../../../../../hooks/general/useReduxState'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcReprint from '../../../../../../actions/odc-reprint/odc-reprint'

// Utils
import ReprintFactory from '../../../../../../utils/reprint/ReprintFactory'

// Components
import Modal from '../../../../../../components/Modal'
import ActionButton from '../../../../../../components/ActionButton'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalContingencyProcess(props) {
    const { open = false, close, classes, handleReset, stateReprint, odcReprint, 
        registerContingencyProcess, enqueueSnackbar, reprintType } = props

    const reprint = new ReprintFactory(reprintType).createReprint()
    const prevOdcReprint = useRef(odcReprint)

    const [ loading, setLoading ] = useState(false)

    const [ dataRegisterContingency, isLoadingRegisterContingency, isSuccessRegisterContingency, isErrorRegisterContingency ] =
        useReduxState({ props: odcReprint.contingencyProcessRegistered, prevProps: prevOdcReprint.current.contingencyProcessRegistered,
            notify: enqueueSnackbar })

    useEffect(_ => {
        if (isLoadingRegisterContingency) setLoading(true)
        if (isSuccessRegisterContingency) {
            Notistack.getNotistack('Pendiente de Activación de TC.', enqueueSnackbar, 'info')
            setTimeout(_ => {
                handleReset()
            }, 1500)
        }
        if (isErrorRegisterContingency) setLoading(false)
    }, [dataRegisterContingency, isLoadingRegisterContingency, isSuccessRegisterContingency, isErrorRegisterContingency])

    const handleContinueProcess = _ => {
        const sendData = reprint.getDataForContingencyProcessApi(stateReprint.clientOnReprint)
        registerContingencyProcess(sendData)
    }

    return (
        <Modal
            title='Pendiente de Activación'
            body={
                <Typography
                    align='center'>
                        La tarjeta estará en la bandeja de pendientes por activar.
                </Typography>
            }
            open={ open } 
            actions={ 
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        loading={ loading }
                                        text='Cancelar'
                                        type='secondary'
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        loading={ loading }
                                        text='Aceptar'
                                        type='primary'
                                        handleAction={ _ => handleContinueProcess() }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
        }/>
    )
}

function mapStateToProps(state) {
    return {
        odcReprint: state.odcReprintReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerContingencyProcess: bindActionCreators(ActionOdcReprint.registerContingencyProcess, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalContingencyProcess))))