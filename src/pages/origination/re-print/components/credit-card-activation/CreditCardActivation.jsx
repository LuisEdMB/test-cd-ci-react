// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Fab, Grid, Tooltip, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import WarningIcon from '@material-ui/icons/Warning'
import FingerprintIcon from '@material-ui/icons/Fingerprint'
import SendIcon from '@material-ui/icons/Send'
import TouchAppIcon from '@material-ui/icons/TouchApp'

// Material UI - Colors
import { green, yellow } from '@material-ui/core/colors'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionBiometric from '../../../../../actions/generic/biometric'
import * as ActionZytrust from '../../../../../actions/generic/zytrust'
import * as Config from '../../../../../actions/config'
import * as ActionConstantOdc from '../../../../../actions/generic/constant'
import * as ActionPmp from '../../../../../actions/pmp/pmp'
import * as ActionPci from '../../../../../actions/pci/pci'
import * as ActionOdcReprint from '../../../../../actions/odc-reprint/odc-reprint'
import * as ActionOdcMaster from '../../../../../actions/odc-master/odc-master'

// Hooks
import useZytrustService from '../../../../../hooks/zytrust/useZytrustService'
import useBiometricValidation from '../../../../../hooks/zytrust/useBiometricValidation'
import useBiometricActivity from '../../../../../hooks/zytrust/useBiometricActivity'
import useDecryptCardNumber from '../../../../../hooks/pci/useDecryptCardNumber'
import useBlockTypeCreditCard from '../../../../../hooks/pmp/useBlockTypeCreditCard'
import useBlockTypeCreditCardMaster from '../../../../../hooks/master/useBlockTypeCreditCard'
import useReduxState from '../../../../../hooks/general/useReduxState'

// Utils
import * as Constants from '../../../../../utils/Constants'
import ReprintFactory from '../../../../../utils/reprint/ReprintFactory'

// Effects
import Bounce from 'react-reveal/Bounce'
import Zoom from 'react-reveal/Zoom'

// Utils
import classNames from 'classnames'
import ItemListActivationReprint from './components/ItemListActivationReprint'
import ActionButton from '../../../../../components/ActionButton'
import ModalContingencyProcess from './components/ModalContingencyProcess'

const styles = theme => ({
    rootWrapper: {
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    root: {
        minWidth: 300,
        maxWidth: 700,
        margin: '0 auto'
    },
    buttonActionsOpenModal: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    contingencyFabWrapper: {
        zIndex: 900,
        [theme.breakpoints.up('md')]: {
            position: 'absolute',
            bottom: 20,
            right: 20
        }
    },
    contingencyfab: {
        backgroundColor: '#ffa000',
        transition: '.3s',
        '&:hover': {
            backgroundColor: yellow[900]
        }
    },
    diplayFlexFabCenter: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    displayFlexFabCenterText: {
        display: 'none',
        [theme.breakpoints.down('sm')]: {
            display: 'block'
        }
    },
    buttonActions: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    activationCreditCard: {
        backgroundColor: green[700],
        transition: '.3s',
        '&:hover': {
            backgroundColor: green[900]
        }
    }
})

function CreditCardActivation(props) {
    const { classes, handleReset, handleNextStep, zytrust, biometric, biometricActivity, validateBiometric,
        validateBiometricZytrustService, biometricZytrustService, enqueueSnackbar, stateReprint,
        constantOdc, getConstantOdc, pmp, blockTypeCreditCard, activateCreditCardReprint, odcMaster,
        updateBlockingCodeCreditCardIntoMaster, odcReprint, registerActivity, reprintType,
        phaseCodeFromContinueProcess, isBiometricOk, pci, decryptCardnumber } = props

    const reprint = new ReprintFactory(reprintType).createReprint()
    const prevZytrust = useRef(zytrust)
    const prevBiometric = useRef(biometric)
    const prevOdcReprint = useRef(odcReprint)
    const prevConstantOdc = useRef(constantOdc)
    const prevPmp = useRef(pmp)
    const prevPci = useRef(pci)
    const prevOdcMaster = useRef(odcMaster)
    const processRetry = useRef(0)
    const isFinishProcess = useRef(false)

    const [ validateIdentityButton, setvalidateIdentityButton ] = useState({
        disabled: false,
        loading: false
    })
    const [ activateTcButton, setActivateTcButton ] = useState({
        disabled: true,
        loading: false
    })
    const [ contingencyButton, setContingencyButton ] = useState({
        disabled: true
    })
    const [ modalContingencyProcess, setModalContingencyProcess ] = useState(false)
    const [ activationBiometric, setActivationBiometric ] = useState({
        countConnectionZytrust: 1,
        isBiometricOk: false
    })

    const [ dataConstantOdc, isLoadingConstantOdc, isSuccessConstantOdc, isErrorConstantOdc ] =
        useReduxState({ props: constantOdc, prevProps: prevConstantOdc.current, notify: enqueueSnackbar,
            options: { stateName: 'Constantes' } })
    const [ dataZytrustService, configButtonZytrustService, isLoadingZytrustService, isSuccessZytrustService, isErrorZytrustService ] =
        useZytrustService({ props: zytrust.responseBioZytrustService, prevProps: prevZytrust.current.responseBioZytrustService })
    const [ dataBiometricValidation, configButtonBiometricValidation, isLoadingBiometricValidation, isSuccessBiometricValidation, isErrorBiometricValidation ] =
        useBiometricValidation({ props: biometric.biometricValidation, prevProps: prevBiometric.current.biometricValidation, notify: enqueueSnackbar })
    const [ configButtonBiometricActivity, isSuccessBiometricActivity, isErrorBiometricActivity ] =
        useBiometricActivity({ props: biometric.biometricActivity, prevProps: prevBiometric.current.biometricActivity, notify: enqueueSnackbar })
    const [ dataDecryptCardNumber, isLoadingDecryptCardNumber, isSuccessDecryptCardNumber, isErrorDecryptCardNumber ] =
        useDecryptCardNumber({ props: pci.cardnumberDecrypted, prevProps: prevPci.current.cardnumberDecrypted, notify: enqueueSnackbar,
            options: { flowName: 'Reimpresión' } })
    const [ isLoadingBlockTypeCreditCard, isSuccessBlockTypeCreditCard, isErrorBlockTypeCreditCard ] =
        useBlockTypeCreditCard({ props: pmp.blockTypeCreditCard, prevProps: prevPmp.current.blockTypeCreditCard, notify: enqueueSnackbar, 
            options: { typeBlockName: 'Activar', flowName: 'Reimpresión' } })
    const [ isLoadingActivateCreditCardReprint, isSuccessActivateCreditCardReprint, isErrorActivateCreditCardReprint ] =
        useBlockTypeCreditCard({ props: pmp.creditCardReprintActivated, prevProps: prevPmp.current.creditCardReprintActivated,
            notify: enqueueSnackbar, options: { typeBlockName: 'Activar', flowName: 'Reimpresión' } })
    const [ isLoadingBlockTypeCreditCardIntoMaster, isSuccessBlockTypeCreditCardIntoMaster, isErrorBlockTypeCreditCardIntoMaster ] =
        useBlockTypeCreditCardMaster({ props: odcMaster.blockingCodeCreditCardForExternalUpdated,
            prevProps: prevOdcMaster.current.blockingCodeCreditCardForExternalUpdated, notify: enqueueSnackbar,
            options: { typeBlockName: 'Activar', flowName: 'Reimpresión' } })
    const [ dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity ] =
        useReduxState({ props: odcReprint.activityRegistered, prevProps: prevOdcReprint.current.activityRegistered, notify: enqueueSnackbar })

    useEffect(_ => {
        getConstantOdc()
    }, [])

    useEffect(_ => {
        if (isLoadingConstantOdc) {
            setvalidateIdentityButton({ disabled: true, loading: true })
            setActivateTcButton({ disabled: true, loading: true })
        }
        if (isSuccessConstantOdc) {
            validateBiometricZytrustService()
            processRetry.current = reprint.getReintentProcessForCreditCardActivationSection(phaseCodeFromContinueProcess)
            let biometricButtonDisabled = false
            let tcButtonDisabled = true
            if (phaseCodeFromContinueProcess?.status >= 30904 || isBiometricOk) {
                biometricButtonDisabled = true
                tcButtonDisabled = false
            }
            setActivationBiometric(state => ({ ...state, isBiometricOk: isBiometricOk }))
            setvalidateIdentityButton({ disabled: biometricButtonDisabled, loading: false })
            setActivateTcButton({ disabled: tcButtonDisabled, loading: false })
            window.addEventListener('focus', validateBiometricZytrustService )
            window.addEventListener('blur', validateBiometricZytrustService )
            return _ => {
                window.removeEventListener('focus', validateBiometricZytrustService )
                window.removeEventListener('blur', validateBiometricZytrustService )
            }
        }
    }, [dataConstantOdc, isLoadingConstantOdc, isSuccessConstantOdc, isErrorConstantOdc])

    useEffect(_ => {
        if (isLoadingZytrustService) {
            setContingencyButton(configButtonZytrustService.contingency)
            setvalidateIdentityButton(configButtonZytrustService.validateIdentity)
        }
        if (isSuccessZytrustService || isErrorZytrustService) {
            const { data, error } = dataZytrustService || { }
            const message = data || error || { }
            const sendData = {
                solicitudeCode: stateReprint.clientOnReprint?.solicitudeCode,
                completeSolicitudeCode: stateReprint.clientOnReprint?.completeSolicitudeCode,
                intentos: activationBiometric.countConnectionZytrust,
                codeError: message.messageCode || 0,
                resultCode: message.resultCode || 0,
                message: message.message,
                restriccion: message.restriccion,
                validateXML: message.validateXML
            }
            validateBiometric(sendData)
        }
    }, [dataZytrustService, configButtonZytrustService, isLoadingZytrustService, isSuccessZytrustService, isErrorZytrustService])

    useEffect(_ => {
        if (isLoadingBiometricValidation) {
            setContingencyButton(configButtonBiometricValidation.contingency)
            setvalidateIdentityButton(configButtonBiometricValidation.validateIdentity)
        }
        if (isSuccessBiometricValidation) {
            const message = dataZytrustService?.data || dataZytrustService?.error || { }
            const isSuccess = dataBiometricValidation?.color === 'success'
            let sendData = {
                tipo_doc: stateReprint.clientOnReprint?.documentTypeAux,
                nro_doc: stateReprint.clientOnReprint?.documentNumber,
                tipo_doc_letra: stateReprint.clientOnReprint?.documentTypeLetter,
                inputXml: message.inputXML,
                outputXml: message.outputXML,
                errorCodeBiometrico: isSuccess ? '' : message.messageCode,
                errorMessageBiometrico: isSuccess ? '' : message.message,
                nom_actividad: 'Activación Tarjeta: Biometría',
                cod_flujo_fase_estado_actual: 30902,
                cod_flujo_fase_estado_anterior: 30901,
                cod_solicitud: stateReprint.clientOnReprint?.solicitudeCode,
                cod_solicitud_completa: stateReprint.clientOnReprint?.completeSolicitudeCode,
                cod_cliente: stateReprint.clientOnReprint?.clientId
            }
            biometricActivity(sendData)
        }
        if (isErrorBiometricValidation) {
            setvalidateIdentityButton(configButtonBiometricValidation.validateIdentity)
            setActivationBiometric(state => ({ ...state, isBiometricOk: false }))
        }
    }, [dataBiometricValidation, configButtonBiometricValidation, isLoadingBiometricValidation, isSuccessBiometricValidation, isErrorBiometricValidation])

    useEffect(_ => {
        if (isSuccessBiometricActivity) {
            const { des_error, color, btnActivar, btnContingencia, btnValidarIdentidad } = dataBiometricValidation || { }
            if (color === 'warning') setActivationBiometric(state => ({ ...state, countConnectionZytrust: activationBiometric.countConnectionZytrust + 1 }))
            setActivationBiometric(state => ({ ...state, isBiometricOk: btnActivar }))
            setContingencyButton({ disabled: !btnContingencia })
            setvalidateIdentityButton({ disabled: !btnValidarIdentidad, loading: false })
            setActivateTcButton(state => ({ ...state, disabled: !btnActivar }))
            if (des_error) {
                const messages = des_error.split('||')
                Notistack.getNotistack(`Biometría: ${ messages[0] }`, enqueueSnackbar, color)
                if (btnContingencia && messages[1])
                    Notistack.getNotistack(messages[1], enqueueSnackbar, 'warning', 10000)
            }
        }
        if (isErrorBiometricActivity) setvalidateIdentityButton(configButtonBiometricActivity?.validateIdentity)
    }, [configButtonBiometricActivity, isSuccessBiometricActivity, isErrorBiometricActivity])

    useEffect(_ => {
        if (isLoadingDecryptCardNumber) setActivateTcButton({ disabled: true, loading: true })
        if (isSuccessDecryptCardNumber) {
            processRetry.current = 1
            handleContinueProcess()
        }
        if (isErrorDecryptCardNumber) setActivateTcButton({ disabled: false, loading: false })
    }, [dataDecryptCardNumber, isLoadingDecryptCardNumber, isSuccessDecryptCardNumber, isErrorDecryptCardNumber])

    useEffect(_ => {
        if (isLoadingBlockTypeCreditCard || isLoadingActivateCreditCardReprint)
            setActivateTcButton({ disabled: true, loading: true })
        if (isSuccessBlockTypeCreditCard || isSuccessActivateCreditCardReprint) {
            processRetry.current = 2
            handleContinueProcess()
        }
        if (isErrorBlockTypeCreditCard || isErrorActivateCreditCardReprint)
            setActivateTcButton({ disabled: false, loading: false })
    }, [isLoadingBlockTypeCreditCard, isSuccessBlockTypeCreditCard, isErrorBlockTypeCreditCard,
        isLoadingActivateCreditCardReprint, isSuccessActivateCreditCardReprint, isErrorActivateCreditCardReprint])

    useEffect(_ => {
        if (isLoadingBlockTypeCreditCardIntoMaster) setActivateTcButton({ disabled: true, loading: true })
        if (isSuccessBlockTypeCreditCardIntoMaster) {
            processRetry.current = 3
            handleContinueProcess()
        }
        if (isErrorBlockTypeCreditCardIntoMaster) setActivateTcButton({ disabled: false, loading: false })
    }, [isLoadingBlockTypeCreditCardIntoMaster, isSuccessBlockTypeCreditCardIntoMaster, isErrorBlockTypeCreditCardIntoMaster])

    useEffect(_ => {
        if (isLoadingRegisterActivity) setActivateTcButton({ disabled: true, loading: true })
        if (isSuccessRegisterActivity) {
            if (isFinishProcess.current) {
                setTimeout(_ => {
                    Notistack.getNotistack('Consulta Correcta, 7mo Paso Ok!', enqueueSnackbar, 'success')
                }, 1000)
                setTimeout(_ => {
                   handleNextStep()
                }, 2500)
            } else {
                processRetry.current = 4
                handleContinueProcess()
            }
        }
        if (isErrorRegisterActivity) setActivateTcButton({ disabled: false, loading: false })
    }, [dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity])

    const handleBiometricZytrustService = _ => {
        biometricZytrustService({
            tiDocCliente: stateReprint.principalClient?.documentTypeAux,
            nuDocCliente: stateReprint.principalClient?.documentNumber
        })
    }

    const handleContinueProcess = _ => {
        const steps = getSteps()
        steps[processRetry.current].action()
    }

    const getSteps = _ => ({
        0: {
            action: _ => {
                const isOptionalToken = reprint.isOptionalTokenForActivateCreditCard
                const token = stateReprint.clientOnReprint?.creditCard?.newToken
                if (!isOptionalToken || (isOptionalToken && token)) {
                    const sendData = reprint.getDataForDecryptCardnumberApi(stateReprint.clientOnReprint)
                    decryptCardnumber(sendData)
                } else {
                    processRetry.current = 1
                    handleContinueProcess()
                }
            }
        },
        1: {
            action: _ => {
                const { creditCard } = stateReprint.clientOnReprint
                const sendData = reprint.getDataForActivateCreditCardApi({
                    ...stateReprint.clientOnReprint,
                    cardNumberToActivate: dataDecryptCardNumber?.nro_tarjeta || creditCard?.newCardNumber,
                    comodin: getComodin()
                })
                const actionToProcess = sendData.callPmpActivateReprint ? activateCreditCardReprint : blockTypeCreditCard
                actionToProcess(sendData)
            }
        },
        2: {
            action: _ => {
                const sendData = reprint.getDataForActivateCreditCardIntoMasterApi({
                    ...stateReprint.clientOnReprint,
                    comodin: getComodin()
                })
                if (sendData.next) {
                    processRetry.current = 3
                    handleContinueProcess()
                } else updateBlockingCodeCreditCardIntoMaster(sendData)
            }
        },
        3: {
            action: _ => {
                const sendData = reprint.getDataForRegisterActivityInitOnCreditCardActivationSectionApi(stateReprint.clientOnReprint)
                registerActivity(sendData)
            }
        },
        4: {
            action: _ => {
                isFinishProcess.current = true
                const sendData = reprint.getDataForRegisterActivityEndOnCreditCardActivationSectionApi(stateReprint.clientOnReprint)
                registerActivity(sendData)
            }
        }
    })

    const getComodin = _ => {
        const comodin = (dataConstantOdc?.find(item => 
            item.des_abv_constante === Constants.General.comodinCreditCardActivate) || {})?.valor_texto || ''
        return comodin
    }

    return (
        <>
            <Grid
                container
                spacing={ 8 }>
                    <Grid
                        item
                        xs={ 12 }>
                            <Grid
                                container
                                spacing={ 8 }
                                className={ classNames(classes.rootWrapper, 'p-sm-5') }>
                                <Grid
                                    item
                                    xs={ 12 }>
                                        <Grid
                                            container
                                            spacing={ 8 }
                                            className={ classes.root }>
                                                <Grid
                                                    item
                                                    xs={ 12 }
                                                    className='mb-2'>
                                                        <Typography
                                                            align='center'
                                                            component='h2'
                                                            variant='h6'>
                                                                Activación de Tarjeta
                                                        </Typography>
                                                </Grid>
                                        <Grid
                                            item
                                            xs={ 12 }
                                            className='mb-2'>
                                                <Grid
                                                    container
                                                    spacing={ 8 }>
                                                        <ItemListActivationReprint>
                                                            Realizar el proceso de generación de clave en el Sistema
                                                            <span className='font-weight-bold'> MIG</span>.
                                                        </ItemListActivationReprint>
                                                        <ItemListActivationReprint>
                                                            Validación Biométrica al Cliente.
                                                        </ItemListActivationReprint>
                                                        <ItemListActivationReprint>
                                                            Si luego de 3 intentos el cliente no ha podido ser validado, realizar la
                                                            <span className='font-weight-bold'> Modalidad de Contingencia</span>.
                                                        </ItemListActivationReprint>
                                                </Grid>
                                        </Grid>
                                        <Grid
                                            item
                                            xs={ 12 }
                                            className='mb-2'>
                                                <Grid
                                                    container 
                                                    spacing={ 8 }
                                                    className={ classes.buttonActionsOpenModal }>
                                                        <Grid
                                                            item
                                                            xs={ 12 }
                                                            sm={ 12 }
                                                            className={ classes.contingencyFabWrapper }>
                                                                <Bounce>
                                                                    <div
                                                                        className={ classes.diplayFlexFabCenter }>
                                                                            <Fab
                                                                                disabled={ contingencyButton.disabled }
                                                                                variant='extended'
                                                                                onClick={ _ => setModalContingencyProcess(true) }
                                                                                className={ classes.contingencyfab }>
                                                                                    <WarningIcon
                                                                                        className='text-white'
                                                                                        fontSize='small'
                                                                                        color='inherit' />
                                                                            </Fab>
                                                                            <Typography
                                                                                className={ classNames(classes.displayFlexFabCenterText, 'font-weight-bolder m-2') }>
                                                                                    Modalidad de Contingencia
                                                                            </Typography>
                                                                    </div>
                                                                </Bounce>
                                                        </Grid>
                                                </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid
                                container
                                spacing={ 8 }
                                className={ classes.buttonActions }>
                                    <Grid
                                        item
                                        xs={ 12 }>
                                            <Typography
                                                align='center'>
                                                    {
                                                        zytrust.validateBioZytrustService.data ? 
                                                        zytrust.validateBioZytrustService.data.online ? 
                                                            <Typography 
                                                                component='span'
                                                                className='font-weight-bolder text-green'>
                                                                    {
                                                                        zytrust.validateBioZytrustService.data.message
                                                                    }
                                                                    <TouchAppIcon 
                                                                        fontSize='small'
                                                                        className='ml-2' />
                                                            </Typography>
                                                            :
                                                            <Typography 
                                                                color='secondary'
                                                                component='span'
                                                                className='font-weight-bolder'>
                                                                    {
                                                                        zytrust.validateBioZytrustService.data.message ?
                                                                            <>
                                                                                { zytrust.validateBioZytrustService.data.message }
                                                                                <a 
                                                                                    href={ Config.URL_VALIDATE_ZYTRUST }
                                                                                    className='ml-2'
                                                                                    target='_blank'
                                                                                    rel='noopener noreferrer' >
                                                                                        Click Aqui
                                                                                </a>
                                                                            </> : null
                                                                    }
                                                            </Typography>
                                                        :
                                                        <Typography 
                                                            color='error'
                                                            component='span'
                                                            className='font-weight-bolder'>
                                                                {
                                                                    zytrust.validateBioZytrustService.data.message
                                                                }
                                                                Click Aqui
                                                        </Typography>
                                                    }
                                            </Typography>
                                    </Grid>
                                    <Grid
                                        item
                                        xs={ 12 }
                                        sm={ 6 }
                                        md={ 4 }
                                        lg={ 3 }>
                                            <Tooltip
                                                TransitionComponent={ Zoom }
                                                title='Click para validar la identidad del cliente.'>
                                                    <div>
                                                        <Bounce>
                                                            <ActionButton
                                                                loading={ validateIdentityButton.disabled }
                                                                text='Validar Identidad'
                                                                type='primary'
                                                                handleAction={ _ => handleBiometricZytrustService() }
                                                                icon={
                                                                    <FingerprintIcon
                                                                        fontSize='small'
                                                                        className='ml-2' />
                                                                }
                                                                showLoading={ validateIdentityButton.loading }
                                                            />
                                                        </Bounce>
                                                    </div>
                                            </Tooltip>
                                    </Grid>
                                    <Grid
                                        item
                                        xs={ 12 }
                                        sm={ 6 }
                                        md={ 4 }
                                        lg={ 3 }>
                                            <Tooltip
                                                TransitionComponent={ Zoom }
                                                title='Click para la activación de la tarjeta.'>
                                                    <div>
                                                        <Bounce>
                                                            <ActionButton
                                                                loading={ activateTcButton.disabled }
                                                                text='Activar Tarjeta'
                                                                type='primary'
                                                                className={ classes.activationCreditCard }
                                                                handleAction={ _ => handleContinueProcess() }
                                                                icon={
                                                                    <SendIcon
                                                                        fontSize='small'
                                                                        className='ml-2' />
                                                                }
                                                                showLoading={ activateTcButton.loading }
                                                            />
                                                        </Bounce>
                                                    </div>
                                            </Tooltip>
                                    </Grid>
                            </Grid>
                    </Grid>
            </Grid>
            <ModalContingencyProcess
                stateReprint={ stateReprint }
                open={ modalContingencyProcess }
                reprintType={ reprintType }
                close={ _ => setModalContingencyProcess(false) }
                handleReset={ handleReset } />
        </>
    )
}

function mapStateToProps(state) {
    return {
        zytrust: state.zytrustReducer,
        biometric: state.biometricReducer,
        constantOdc: state.constantODCReducer,
        pmp: state.pmpReducer,
        odcReprint: state.odcReprintReducer,
        pci: state.pciReducer,
        odcMaster: state.odcMasterReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        biometricActivity: bindActionCreators(ActionBiometric.biometricActivity, dispatch),
        validateBiometric: bindActionCreators(ActionBiometric.validateBiometric, dispatch),
        validateBiometricZytrustService: bindActionCreators(ActionZytrust.validateBiometricZytrustService, dispatch),
        biometricZytrustService: bindActionCreators(ActionZytrust.biometricZytrustService, dispatch),
        getConstantOdc: bindActionCreators(ActionConstantOdc.getConstantODC, dispatch),
        decryptCardnumber: bindActionCreators(ActionPci.decryptCardnumber, dispatch),
        blockTypeCreditCard: bindActionCreators(ActionPmp.blockTypeCreditCard, dispatch),
        activateCreditCardReprint: bindActionCreators(ActionPmp.activateCreditCardReprint, dispatch),
        updateBlockingCodeCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.updateBlockingCodeCreditCardForExternal, dispatch),
        registerActivity: bindActionCreators(ActionOdcReprint.registerActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(CreditCardActivation))))