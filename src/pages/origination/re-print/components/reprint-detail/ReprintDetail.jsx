// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { ExpansionPanel, ExpansionPanelDetails, FormControl, FormControlLabel, FormHelperText, FormLabel, Grid, InputLabel, MenuItem, Radio, RadioGroup, Select, TextField, Tooltip, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'
import CancelIcon from '@material-ui/icons/Cancel'
import Label from '@material-ui/icons/Label'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionProductColors from '../../../../../actions/value-list/product-color'
import * as ActionOdcReprint from '../../../../../actions/odc-reprint/odc-reprint'

// Effects
import Bounce from 'react-reveal/Bounce'
import Zoom from 'react-reveal/Zoom'
import Fade from 'react-reveal/Fade'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Utils
import classNames from 'classnames'
import * as Constants from '../../../../../utils/Constants'
import * as Utils from '../../../../../utils/Utils'
import * as Validations from '../../../../../utils/Validations'
import ReprintFactory from '../../../../../utils/reprint/ReprintFactory'

// Components
import ActionButton from '../../../../../components/ActionButton'
import CreditCard from '../../../../../components/CreditCard'

const styles = {
    root: {
        maxWidth: 900
    },
    wrapper: {
        position: 'relative'
    },
    creditCard: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    menu:{
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center'
    },
    labelError: {
        display: 'flex',
        justifyContent: 'space-between', 
        alignItems: 'center'
    }
}

function ReprintDetail(props) {
    const { classes, showModalCancelActivity, handleNextStep, stateReprint, productColors, getAllProductColors,
        registerReprintDetail, odcReprint, enqueueSnackbar, handleSaveStateReprint, reprintType } = props

    const reprint = new ReprintFactory(reprintType).createReprint()
    const prevOdcReprint = useRef(odcReprint)
    const prevProductColors = useRef(productColors)

    const [ loading, setLoading ] = useState(false)
    const [ showAdditionalServices, setShowAdditionalServices ] = useState(false)
    const [ availableColors, setAvailableColors ] = useState([])
    const [ colorCreditCard, setColorCreditCard ] = useState({
        color: 0,
        colorAux: ''
    })
    const [ validationColorCreditCard, setValidationColorCreditCard ] = useState({
        color: {
            error: false,
            message: '',
            ref: useRef(null),
            required: true
        }
    })
    const [ additionalServices, setAdditionalServices ] = useState({
        flagInternetConsume: '0',
        flagForeignConsume: '0',
        flagEffectiveDisposition: '0',
        flagOverdraftTC: '0'
    })

    const [ dataProductColors, isLoadingProductColors, isSuccessProductColors, isErrorProductColors ] =
        useReduxState({ props: productColors, prevProps: prevProductColors.current, notify: enqueueSnackbar,
            options: { stateName: 'Colores Productos' } })
    const [ dataRegisterReprintDetail, isLoadingRegisterReprintDetail, isSuccessRegisterReprintDetail, isErrorRegisterReprintDetail ] =
        useReduxState({ props: odcReprint.reprintDetailRegistered, prevProps: prevOdcReprint.current.reprintDetailRegistered, notify: enqueueSnackbar })

    useEffect(_ => {
        getAllProductColors()
    }, [])

    useEffect(_ => {
        if (isLoadingProductColors) setLoading(true)
        if (isSuccessProductColors) {
            setColorCreditCard({
                color: stateReprint.clientOnReprint?.creditCard?.color || 0,
                colorAux: stateReprint.clientOnReprint?.creditCard?.colorAux || ''
            })
            setShowAdditionalServices(reprint.showAdditionalServicesForAllReprintReasons || 
                (stateReprint.clientOnReprint.reprintReasonId !== Constants.ReprintReasonType.embossError))
            const colors = dataProductColors?.filter(item =>
                item.cod_producto === stateReprint.clientOnReprint?.creditCard?.productId &&
                item.flg_activo) || []
            setAvailableColors(colors)
            setValidationColorCreditCard({ color: { required: colors.length > 0 } })
            setLoading(false)
        }
    }, [dataProductColors, isLoadingProductColors, isSuccessProductColors, isErrorProductColors])

    useEffect(_ => {
        if (isLoadingRegisterReprintDetail) setLoading(true)
        if (isSuccessRegisterReprintDetail) {
            Notistack.getNotistack('Consulta Correcta, 3er Paso Ok!', enqueueSnackbar, 'success')
            setTimeout(_ => {
                handleNextStep()
            }, 1500)
        }
        if (isErrorRegisterReprintDetail) setLoading(false)
    }, [dataRegisterReprintDetail, isLoadingRegisterReprintDetail, isSuccessRegisterReprintDetail, isErrorRegisterReprintDetail])

    const handleChangeCreditCardColor = e => {
        const { value, call = _ => null } = e.target || e
        const colorSelected = availableColors.find(item => item.cod_color === value) || {}
        setColorCreditCard({
            color: value,
            colorAux: colorSelected.des_aux_color
        })
        setValidationColorCreditCard(state => {
            let validations = { ...state }
            if (Utils.findProperty(validations, 'color', ['ref']).length > 0 && state.color.required) {
                validations = {
                    color: {
                        ...state.color,
                        ...Validations.validateByField('color', value)
                    }
                }
            }
            call(validations)
            return validations
        })
    }

    const handleChangeAdditionalServices = e => {
        const { name, value } = e.target || e
        setAdditionalServices(state => ({
            ...state,
            [name]: value
        }))
    }

    const handleContinueProcess = _ => {
        Validations.comprobeAllValidationsSuccess({ color: colorCreditCard.color }, handleChangeCreditCardColor).then(valid => {
            if (valid) {
                const data = {
                    ...stateReprint.clientOnReprint,
                    creditCard: {
                        ...stateReprint.clientOnReprint?.creditCard,
                        ...colorCreditCard
                    },
                    additionalServices
                }
                const sendData = reprint.getDataForRegisterReprintDetailApi(data)
                handleSaveStateReprint({ clientOnReprint: data })
                registerReprintDetail(sendData)
            }
        })
    }

    const handleCancelProcess = _ => {
        const dataCancel = reprint.getDataForReprintDetailSectionCancelApi(stateReprint.clientOnReprint)
        showModalCancelActivity(dataCancel)
    }

    return (
        <>
            <Grid
                container
                spacing={ 8 }
                className='d-flex justify-content-center align-items-center'>
                    <Grid
                        container
                        spacing={ 8 }
                        className={ classNames(classes.root, 'm-0 m-sm-3') }>
                            <Grid
                                item
                                xs={ 12 }
                                sm={ 12 }
                                md={ 6 }
                                lg={ 7 }>
                                    <Grid
                                        container
                                        spacing={ 8 } >
                                            {
                                                (stateReprint.clientOnReprint?.relationTypeId === Constants.ClientRelationTypes.holderClientCode) &&
                                                    <>
                                                        <Grid
                                                            item
                                                            xs={ 6 }>
                                                                <TextField
                                                                    fullWidth
                                                                    label='Tipo Documento'
                                                                    type='text'
                                                                    margin='normal'
                                                                    value={ stateReprint.clientOnReprint?.documentTypeLetter || '' }
                                                                    color='default'
                                                                    InputProps={ {
                                                                        readOnly: true
                                                                    } } />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 6 }>
                                                                <TextField
                                                                    fullWidth
                                                                    label='Número Documento'
                                                                    type='text'
                                                                    margin='normal'
                                                                    value={ stateReprint.clientOnReprint?.documentNumber || '' }
                                                                    color='default'
                                                                    InputProps={ {
                                                                        readOnly: true
                                                                    } } />
                                                        </Grid>
                                                    </>
                                            }
                                            <Grid
                                                item
                                                xs={ 12 }>
                                                    <TextField
                                                        fullWidth
                                                        label='Nombres'
                                                        type='text'
                                                        margin='normal'
                                                        value={ `${ stateReprint.clientOnReprint?.firstName } ${ stateReprint.clientOnReprint?.secondName }` }
                                                        color='default'
                                                        InputProps={ {
                                                            readOnly: true
                                                        } } />
                                            </Grid>
                                            <Grid
                                                item
                                                xs={ 12 }>
                                                    <TextField
                                                        fullWidth
                                                        label='Apellidos'
                                                        type='text'
                                                        margin='normal'
                                                        value={ `${ stateReprint.clientOnReprint?.firstLastName } ${ stateReprint.clientOnReprint?.secondLastName }` }
                                                        color='default'
                                                        InputProps={ {
                                                            readOnly: true
                                                        } } />
                                            </Grid>
                                            {
                                                availableColors.length > 0 &&
                                                    <Grid
                                                        item
                                                        xs={ 12 }
                                                        sm={ 4 }>
                                                            <FormControl
                                                                error={ validationColorCreditCard.color.error }
                                                                fullWidth
                                                                margin='normal'>
                                                                    <InputLabel
                                                                        htmlFor='color'>
                                                                            Color Tarjeta
                                                                    </InputLabel>
                                                                    <Select
                                                                        value={ colorCreditCard.color }
                                                                        inputProps={{ id: 'color' }}
                                                                        onChange={ e => handleChangeCreditCardColor(e) }>
                                                                            <MenuItem
                                                                                disabled>
                                                                                    Seleccionar Color
                                                                            </MenuItem>
                                                                            {
                                                                                availableColors.map((item, index) => (
                                                                                    <MenuItem
                                                                                        selected={ index === 0 }
                                                                                        key={ index }
                                                                                        value={ item.cod_color }>
                                                                                            <div
                                                                                                className={ classes.menu }>
                                                                                                    <Label
                                                                                                        style={{ color: item.des_aux_color, fontSize: 12 }}
                                                                                                        variant='inherit' />
                                                                                                    <Typography
                                                                                                        component='span'
                                                                                                        className='px-2'
                                                                                                        variant='inherit' >
                                                                                                            { item.des_color }
                                                                                                    </Typography>
                                                                                            </div>
                                                                                    </MenuItem>
                                                                                ))
                                                                            }
                                                                    </Select>
                                                            </FormControl>
                                                            <Fade>
                                                                <FormHelperText
                                                                    className={ classes.labelError }>
                                                                        <Typography
                                                                            component='span'
                                                                            variant='inherit'
                                                                            color={ validationColorCreditCard.color.error 
                                                                                ? 'secondary' : 'default' }>
                                                                                    {
                                                                                        validationColorCreditCard.color.error &&
                                                                                        validationColorCreditCard.color.message
                                                                                    }
                                                                        </Typography>
                                                                </FormHelperText>
                                                            </Fade>
                                                    </Grid>
                                            }
                                            <Grid
                                                item
                                                xs={ 12 }
                                                sm={ 8 }>
                                                    <TextField
                                                        fullWidth
                                                        label='Producto'
                                                        type='text'
                                                        margin='normal'
                                                        value={ stateReprint.clientOnReprint?.creditCard?.productDescription }
                                                        color='default'
                                                        InputProps={ {
                                                            readOnly: true
                                                        } } />
                                            </Grid>
                                    </Grid>
                            </Grid>
                            <Grid
                                item
                                xs={ 12 }
                                sm={ 12 }
                                md={ 6 }
                                lg={ 5 }>
                                    <Grid
                                        container
                                        spacing={ 8 }
                                        className='d-flex justify-content-center'>
                                            <Grid
                                                item
                                                xs={ 12 }
                                                className={ classNames(classes.creditCard,
                                                    `py-${ (stateReprint.clientOnReprint?.relationTypeId === Constants.ClientRelationTypes.holderClientCode)
                                                        ? 5
                                                        : 2 } rounded`) }>
                                                    <div
                                                        style={{ minHeight: 230 }}>
                                                            {
                                                                <Fade>
                                                                    <CreditCard
                                                                        client={ `${ stateReprint.clientOnReprint?.firstLastName } ${ stateReprint.clientOnReprint?.firstName }` }
                                                                        type={ stateReprint.clientOnReprint?.creditCard?.productType }
                                                                        customColor={ colorCreditCard.colorAux }
                                                                        bin={ stateReprint.clientOnReprint?.creditCard?.bin }
                                                                        cardNumber={ stateReprint.clientOnReprint?.creditCard?.cardNumber }
                                                                        brand={ stateReprint.clientOnReprint?.creditCard?.brand === 200001 ? 0 : 1 } />
                                                                </Fade>
                                                            }
                                                    </div>
                                            </Grid>
                                    </Grid>
                            </Grid>
                            {
                                showAdditionalServices &&
                                    <Grid
                                        item
                                        xs={ 12 }
                                        sm={ 12 }
                                        md={ 12 }
                                        lg={ 12 }>
                                            <ExpansionPanel
                                                defaultExpanded>
                                                    <ExpansionPanelDetails>
                                                        <Grid
                                                            container
                                                            spacing={ 8 }>
                                                                <Grid
                                                                    item
                                                                    xs={ 12 }
                                                                    sm={ 12 }
                                                                    md={ 6 }
                                                                    lg={ 6 }>
                                                                        <FormControl
                                                                            margin='normal'
                                                                            fullWidth
                                                                            color='primary'
                                                                            component='fieldset'>
                                                                                <FormLabel
                                                                                    component='legend'>
                                                                                        ¿Desea habilitar la opción de consumos por internet?
                                                                                </FormLabel>
                                                                                <RadioGroup
                                                                                    row
                                                                                    className='d-flex justify-content-md-start'
                                                                                    value={ additionalServices.flagInternetConsume }
                                                                                    name='flagInternetConsume'
                                                                                    onChange={ e => handleChangeAdditionalServices(e) }>
                                                                                        <FormControlLabel
                                                                                            value={ '1' }
                                                                                            control={
                                                                                                <Radio
                                                                                                    className='d-flex'
                                                                                                    color='primary' />
                                                                                            }
                                                                                            label='Si' />
                                                                                        <FormControlLabel
                                                                                            value={ '0' }
                                                                                            control={
                                                                                                <Radio
                                                                                                    className='d-flex'
                                                                                                    color='primary' />}
                                                                                            label='No'
                                                                                        />
                                                                                </RadioGroup>
                                                                            </FormControl>
                                                                </Grid>
                                                                <Grid
                                                                    item
                                                                    xs={ 12 }
                                                                    sm={ 12 }
                                                                    md={ 6 }
                                                                    lg={ 6 }>
                                                                        <FormControl
                                                                            margin='normal'
                                                                            fullWidth
                                                                            color='primary'
                                                                            component='fieldset'>
                                                                                <FormLabel
                                                                                    component='legend'>
                                                                                        ¿Desea habilitar la opción de consumos en el extranjero?
                                                                                </FormLabel>
                                                                                <RadioGroup
                                                                                    row
                                                                                    className='d-flex justify-content-md-start'
                                                                                    value={ additionalServices.flagForeignConsume }
                                                                                    name='flagForeignConsume'
                                                                                    onChange={ e => handleChangeAdditionalServices(e) }>
                                                                                        <FormControlLabel
                                                                                            value={ '1' }
                                                                                            control={
                                                                                                <Radio
                                                                                                    className='d-flex'
                                                                                                    color='primary' />
                                                                                            }
                                                                                            label='Si' />
                                                                                        <FormControlLabel
                                                                                            value={ '0' }
                                                                                            control={
                                                                                                <Radio
                                                                                                    className='d-flex'
                                                                                                    color='primary' />
                                                                                            }
                                                                                            label='No' />
                                                                                </RadioGroup>
                                                                        </FormControl>
                                                                </Grid>
                                                                <Grid
                                                                    item
                                                                    xs={ 12 }
                                                                    sm={ 12 }
                                                                    md={ 6 }
                                                                    lg={ 6 }>
                                                                        <FormControl
                                                                            margin='normal'
                                                                            fullWidth
                                                                            color='primary'
                                                                            component='fieldset'>
                                                                                <FormLabel
                                                                                    component='legend'>
                                                                                        ¿Desea contar con la posibilidad de retirar efectivo con cargo a su Tarjeta Cencosud?
                                                                                </FormLabel>
                                                                            <RadioGroup
                                                                                row
                                                                                className='d-flex justify-content-md-start'
                                                                                value={ additionalServices.flagEffectiveDisposition }
                                                                                name='flagEffectiveDisposition'
                                                                                onChange={ e => handleChangeAdditionalServices(e) }>
                                                                                    <FormControlLabel
                                                                                        value={ '1' }
                                                                                        control={
                                                                                            <Radio
                                                                                                className='d-flex'
                                                                                                color='primary' />
                                                                                        }
                                                                                        label='Si' />
                                                                                    <FormControlLabel
                                                                                        value={ '0' }
                                                                                        control={
                                                                                            <Radio
                                                                                                className='d-flex'
                                                                                                color='primary' />
                                                                                        }
                                                                                        label='No' />
                                                                            </RadioGroup>
                                                                        </FormControl>
                                                                </Grid>
                                                                <Grid
                                                                    item
                                                                    xs={ 12 }
                                                                    sm={ 12 }
                                                                    md={ 6 }
                                                                    lg={ 6 }>
                                                                        <FormControl
                                                                            margin='normal'
                                                                            fullWidth
                                                                            color='primary'
                                                                            component='fieldset'>
                                                                            <FormLabel
                                                                                component='legend'>
                                                                                    ¿Desea contar con la posibilidad de sobregirar la línea de crédito de su Tarjeta Cencosud?
                                                                            </FormLabel>
                                                                            <RadioGroup
                                                                                row
                                                                                className='d-flex justify-content-md-start'
                                                                                value={ additionalServices.flagOverdraftTC }
                                                                                name='flagOverdraftTC'
                                                                                onChange={ e => handleChangeAdditionalServices(e) }>
                                                                                    <FormControlLabel
                                                                                        value={ '1' }
                                                                                        control={
                                                                                            <Radio
                                                                                                className='d-flex'
                                                                                                color='primary' />
                                                                                        }
                                                                                        label='Si' />
                                                                                    <FormControlLabel
                                                                                        value={ '0' }
                                                                                        control={
                                                                                            <Radio
                                                                                                className='d-flex'
                                                                                                color='primary' />
                                                                                        }
                                                                                        label='No' />
                                                                            </RadioGroup>
                                                                        </FormControl>
                                                                </Grid>
                                                        </Grid>
                                                    </ExpansionPanelDetails>
                                            </ExpansionPanel>
                                    </Grid>
                            }
                    </Grid>
            </Grid>
            <Grid
                container
                spacing={ 8 }
                className='d-flex justify-content-center align-items-center'>
                    <>
                        <Grid
                            item
                            xs={ 12 }
                            sm={ 6 }
                            md={ 6 }
                            lg={ 3 }>
                                <Tooltip
                                    TransitionComponent={ Zoom }
                                    title='Cancelar reimpresión'>
                                        <div>
                                            <Bounce 
                                                left>
                                                    <div
                                                        className={ classes.wrapper }>
                                                            <ActionButton
                                                                text='Cancelar'
                                                                type='secondary'
                                                                loading={ loading }
                                                                handleAction={ _ => handleCancelProcess() }
                                                                icon={
                                                                    <CancelIcon 
                                                                        fontSize='small' 
                                                                        className='ml-2' />
                                                                }/>
                                                    </div>
                                            </Bounce>
                                        </div>
                                </Tooltip>
                        </Grid>
                        <Grid
                            item
                            xs={ 12 }
                            sm={ 6 }
                            md={ 6 }
                            lg={ 3 }>
                                <Tooltip
                                    TransitionComponent={ Zoom }
                                    title='Click para continuar con el proceso de reimpresión'>
                                        <div>
                                            <Bounce 
                                                right>
                                                    <div
                                                        className={ classes.wrapper }>
                                                            <ActionButton
                                                                text='Continuar'
                                                                type='primary'
                                                                loading={ loading }
                                                                handleAction={ _ => handleContinueProcess() }
                                                                icon={
                                                                    <SendIcon 
                                                                        fontSize='small' 
                                                                        className='ml-2' />
                                                                }
                                                                showLoading />
                                                    </div>
                                            </Bounce>
                                        </div>
                                </Tooltip>
                        </Grid>
                    </>
            </Grid>
        </>
    )
}

function mapStateToProps(state) {
    return {
        productColors: state.productColorReducer,
        odcReprint: state.odcReprintReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getAllProductColors: bindActionCreators(ActionProductColors.getAllProductColors, dispatch),
        registerReprintDetail: bindActionCreators(ActionOdcReprint.registerReprintDetail, dispatch)
    }
}


export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ReprintDetail))))