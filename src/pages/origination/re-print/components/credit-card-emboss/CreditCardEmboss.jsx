// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, Tooltip, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Material UI - Colors
import { yellow } from '@material-ui/core/colors'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcReprint from '../../../../../actions/odc-reprint/odc-reprint'

// Effects
import Bounce from 'react-reveal/Bounce'
import Zoom from 'react-reveal/Zoom'
import Fade from 'react-reveal/Fade'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Utils
import ReprintFactory from '../../../../../utils/reprint/ReprintFactory'

// Components
import ActionButton from '../../../../../components/ActionButton'
import CreditCard from '../../../../../components/CreditCard'
import ModalPendingEmboss from './components/ModalPendingEmboss'

const styles = {
    wrapper: {
        position: 'relative'
    },
    pendingEmbossButton: {
        backgroundColor: yellow[800],
        transition: '.3s',
        '&:hover': {
            backgroundColor: yellow[900],
        },
    }
}

function CreditCardEmboss(props) {
    const { classes, handleNextStep, handleReset, stateReprint, odcReprint, registerActivity, enqueueSnackbar, 
        reprintType } = props

    const reprint = new ReprintFactory(reprintType).createReprint()
    const prevOdcReprint = useRef(odcReprint)

    const [ loading, setLoading ] = useState(false)
    const [ modalPendingEmboss, setModalPendingEmboss ] = useState(false)

    const [ dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity ] =
        useReduxState({ props: odcReprint.activityRegistered, prevProps: prevOdcReprint.current.activityRegistered, notify: enqueueSnackbar })

    useEffect(_ => {
        if (isLoadingRegisterActivity) setLoading(true)
        if (isSuccessRegisterActivity) {
            Notistack.getNotistack('Consulta Correcta, 6to Paso Ok!', enqueueSnackbar, 'success')
            setTimeout(_ => {
                handleNextStep()
            }, 2500)
        }
        if (isErrorRegisterActivity) setLoading(false)
    }, [dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity])

    const handleContinueProcess = _ => {
        const sendData = reprint.getDataForRegisterActivityOnCreditCardEmbossSectionApi(stateReprint.clientOnReprint)
        registerActivity(sendData)
    }

    return (
        <>
            <Grid
                container
                spacing={ 8 }
                className='d-flex justify-content-center align-items-center my-5'>
                    <Fade>
                        <CreditCard
                            client={ `${ stateReprint.clientOnReprint?.firstLastName } ${ stateReprint.clientOnReprint?.firstName }` }
                            type={ stateReprint.clientOnReprint?.creditCard?.productType }
                            customColor={ stateReprint.clientOnReprint?.creditCard?.colorAux }
                            bin={ stateReprint.clientOnReprint?.creditCard?.bin }
                            cardNumber={ stateReprint.clientOnReprint?.creditCard?.newCardNumber }
                            brand={ stateReprint.clientOnReprint?.creditCard?.brand === 200001 ? 0 : 1 } />
                    </Fade>
            </Grid>
            <Grid
                container
                spacing={ 8 }
                className='d-flex justify-content-center align-items-center'>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 6 }
                        md={ 4 }
                        lg={ 3 }>
                            <Tooltip
                                TransitionComponent={ Zoom }
                                title='Proceso pendiente de emboce.'>
                                    <div>
                                        <Bounce 
                                            left>
                                                <div
                                                    className={ classes.wrapper }>
                                                        <ActionButton
                                                            text='Pendiente de Emboce'
                                                            type='secondary'
                                                            loading={ loading }
                                                            className={ classes.pendingEmbossButton }
                                                            handleAction={ _ => setModalPendingEmboss(true) }/>
                                                </div>
                                        </Bounce>
                                    </div>
                            </Tooltip>
                    </Grid>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 6 }
                        md={ 4 }
                        lg={ 3 }>
                            <Tooltip
                                TransitionComponent={ Zoom }
                                title='Click para continuar con el proceso de reimpresión.'>
                                    <div>
                                        <Bounce 
                                            right>
                                                <div
                                                    className={ classes.wrapper }>
                                                        <ActionButton
                                                            text='Tarjeta Embosada'
                                                            type='primary'
                                                            loading={ loading }
                                                            handleAction={ _ => handleContinueProcess() }
                                                            icon={
                                                                <SendIcon 
                                                                    fontSize='small' 
                                                                    className='ml-2' />
                                                            }
                                                            showLoading />
                                                </div>
                                        </Bounce>
                                    </div>
                            </Tooltip>
                    </Grid>
            </Grid>
            <ModalPendingEmboss
                open={ modalPendingEmboss }
                reprintType={ reprintType }
                stateReprint={ stateReprint }
                close={ _ => setModalPendingEmboss(false) }
                handleReset={ handleReset } />
        </>
    )
}

function mapStateToProps(state) {
    return {
        odcReprint: state.odcReprintReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerActivity: bindActionCreators(ActionOdcReprint.registerActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(CreditCardEmboss))))