// React
import React, { useEffect, useRef, useState } from 'react'

// Redux
import { connect } from 'react-redux'

// Material UI
import { Grid, Paper, Tooltip, Typography, withStyles, withWidth } from '@material-ui/core'

// Material UI - Icons
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined'
import SendIcon from '@material-ui/icons/Send'
import CancelIcon from '@material-ui/icons/Cancel'

// Effects
import Bounce from 'react-reveal/Bounce'
import Zoom from 'react-reveal/Zoom'

// Utils
import ReprintFactory from '../../../../../utils/reprint/ReprintFactory'
import classNames from 'classnames'

// Notify
import { withSnackbar } from 'notistack'

// Components
import ActionButton from '../../../../../components/ActionButton'
import DevGridComponent from '../../../../../components/dev-grid/DevGridComponent'
import ModalReprintReason from './components/ModalReprintReason.jsx'

const styles = theme => ({
    container: {
        paddingLeft: 0,
        paddingRight: 0,
        [theme.breakpoints.up('md')]: {
            paddingLeft: '10%',
            paddingRight: '10%',
        },
        [theme.breakpoints.up('lg')]: {
            paddingLeft: '15%',
            paddingRight: '15%',
        }
    },
    wrapper: {
        position: 'relative'
    },
    paperInfoHolder: {
        borderLeft: '5px #007ab8 solid'
    }
})

function CreditCardDetail(props) {
    const { classes, width, showModalCancelActivity, handleNextStep, odcReprint, handleSaveStateReprint,
        reprintType, stateReprint } = props

    const reprint = new ReprintFactory(reprintType).createReprint()
    const columnsConfig = reprint.getConfigColumnsToCreditCardDetailSection()
    const prevWidth = useRef(width)

    const [ columns, setColumns ] = useState([])
    const [ rows, setRows ] = useState([])
    const [ holderClientInfo, setHolderClientInfo ] = useState({})
    const [ rowSelected, setRowSelected ] = useState({
        selected: [],
        data: {}
    })
    const [ continueButtonDisabled, setContinueButtonDisabled ] = useState(true)
    const [ modalReprintReason, setModalReprintReason ] = useState(false)

    useEffect(_ => {
        const data = getDataResponseOfConsultClient()
        setHolderClientInfo(data)
        setRows(data.reprints)
        setColumns(columnsConfig[width])
    }, [])

    useEffect(_ => {
        if (prevWidth.current !== width) setColumns(columnsConfig[width])
    }, [width])

    const getDataResponseOfConsultClient = _ => {
        return reprint.processDataResponseOfConsultClient(odcReprint.clientConsulted.data || { })
    }

    const handleChangeSelection = selection => {
        const item = selection.pop()
        const itemSelected = item ? [ item ] : []
        setRowSelected({
            selected: itemSelected,
            data: rows.find(row => row.cod_solicitud === itemSelected[0])
        })
        setContinueButtonDisabled(itemSelected.length < 1)
    }

    const handleContinueProcess = _ => {
        setRowSelected(state => ({
            ...state,
            data: {
                principalClient: stateReprint.principalClient,
                ...state.data,
                ...getDataResponseOfConsultClient()
            }
        }))
        setModalReprintReason(true)
    }

    const handleCancelProcess = _ => {
        const data = getDataResponseOfConsultClient()
        const dataCancel = reprint.getDataForCreditCardDetailSectionCancelApi(data)
        showModalCancelActivity(dataCancel)
    }

    return (
        <>
            <div
                className={ classes.container }>
                    {
                        reprint.showInfoHolderClientForCreditCardDetailSection &&
                            <Grid
                                container
                                spacing={ 8 }
                                className='d-flex justify-content align-items-center py-4'>
                                    <Grid
                                        item>
                                            <Paper
                                                className={ classNames(classes.paperInfoHolder, 'd-flex justify-content') }
                                                elevation={ 3 }>
                                                    <div
                                                        style={{ padding: 15 }}>
                                                            <Grid
                                                                alignItems='center'
                                                                container>
                                                                    <Grid
                                                                        style={{ display: 'flex' }}
                                                                        item>
                                                                            <InfoOutlinedIcon
                                                                                color='primary' />
                                                                    </Grid>
                                                                    <Grid
                                                                        style={{ display: 'flex', marginLeft: 10 }}
                                                                        item>
                                                                            <Typography
                                                                                component='span'
                                                                                variant='h6' >
                                                                                    Datos del Titular
                                                                            </Typography>
                                                                    </Grid>
                                                                    <Grid xs={ 12 } item><br/></Grid>
                                                                    <Grid
                                                                        xs={ 12 }
                                                                        sm={ 9 }
                                                                        md={ 9 }
                                                                        lg={ 9 }
                                                                        style={{ paddingBottom: 6 }}
                                                                        item>
                                                                            <Typography
                                                                                component='span' >
                                                                                    <b>Correo Electrónico: </b> { holderClientInfo.emailHolder }
                                                                            </Typography>
                                                                    </Grid>
                                                                    <Grid
                                                                        xs={ 12 }
                                                                        sm={ 3 }
                                                                        md={ 3 }
                                                                        lg={ 3 }
                                                                        style={{ paddingBottom: 6 }}
                                                                        item>
                                                                            <Typography
                                                                                component='span' >
                                                                                    <b>Celular:</b> { holderClientInfo.cellphoneHolder }
                                                                            </Typography>
                                                                    </Grid>
                                                            </Grid>
                                                    </div>
                                            </Paper>
                                    </Grid>
                            </Grid>
                    }
                    <Grid
                        container
                        spacing={ 8 }
                        className='d-flex justify-content align-items-center'>
                            <Grid
                                item
                                style={{ overflow: 'auto' }}>
                                    <DevGridComponent
                                        columns={ columns }
                                        rows={ rows }
                                        width={ width }
                                        check
                                        pagination={ false }
                                        selection={ rowSelected.selected }
                                        onSelectionChange={ row => handleChangeSelection(row) }
                                        getRowId={ row => row.cod_solicitud }
                                        defaultColumnWidths={ columnsConfig.default }/>
                            </Grid>
                    </Grid>
            </div>
            <Grid
                container
                spacing={ 8 }
                className='d-flex justify-content-center align-items-center'>
                    <>
                        <Grid
                            item
                            xs={ 12 }
                            sm={ 6 }
                            md={ 4 }
                            lg={ 3 }>
                                <Tooltip
                                    TransitionComponent={ Zoom }
                                    title='Cancelar reimpresión'>
                                        <div>
                                            <Bounce 
                                                left>
                                                    <div
                                                        className={ classes.wrapper }>
                                                            <ActionButton
                                                                text='Cancelar'
                                                                type='secondary'
                                                                handleAction={ _ => handleCancelProcess() }
                                                                icon={
                                                                    <CancelIcon 
                                                                        fontSize='small' 
                                                                        className='ml-2' />
                                                                }/>
                                                    </div>
                                            </Bounce>
                                        </div>
                                </Tooltip>
                        </Grid>
                        <Grid
                            item
                            xs={ 12 }
                            sm={ 6 }
                            md={ 4 }
                            lg={ 3 }>
                                <Tooltip
                                    TransitionComponent={ Zoom }
                                    title='Click para continuar con el proceso de reimpresión'>
                                        <div>
                                            <Bounce 
                                                right>
                                                    <div
                                                        className={ classes.wrapper }>
                                                            <ActionButton
                                                                text='Continuar'
                                                                type='primary'
                                                                loading={ continueButtonDisabled }
                                                                handleAction={ _ => handleContinueProcess() }
                                                                icon={
                                                                    <SendIcon 
                                                                        fontSize='small' 
                                                                        className='ml-2' />
                                                                }/>
                                                    </div>
                                            </Bounce>
                                        </div>
                                </Tooltip>
                        </Grid>
                    </>
            </Grid>
            <ModalReprintReason
                open={ modalReprintReason }
                dataRow={ rowSelected.data }
                reprintType={ reprintType }
                close={ _ => setModalReprintReason(false) }
                handleSaveStateReprint={ handleSaveStateReprint }
                handleNextStep={ handleNextStep } />
        </>
    )
}

function mapStateToProps(state) {
    return {
        odcReprint: state.odcReprintReducer
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps)(withWidth()(CreditCardDetail)))))