// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Button, FormControl, FormHelperText, FormLabel, Grid, LinearProgress, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'
import AttachFileIcon from '@material-ui/icons/AttachFile'

// Hooks
import useReduxState from '../../../../../../hooks/general/useReduxState'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionCreditCardBin from '../../../../../../actions/value-list/credit-card-bin'
import * as ActionReprintReasons from '../../../../../../actions/value-list/reason-reprint'
import * as ActionOdcReprint from '../../../../../../actions/odc-reprint/odc-reprint'
import * as ActionOdcMaster from '../../../../../../actions/odc-master/odc-master'

// Utils
import classNames from 'classnames'
import * as Utils from '../../../../../../utils/Utils'
import * as Validations from '../../../../../../utils/Validations'
import * as Constants from '../../../../../../utils/Constants'
import * as MasterOrigination from '../../../../../../utils/origination/MasterOrigination'
import ReprintFactory from '../../../../../../utils/reprint/ReprintFactory'

// Effects
import Fade from 'react-reveal/Fade'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../../utils/Notistack'

// Components
import Modal from '../../../../../../components/Modal'
import ActionButton from '../../../../../../components/ActionButton'
import Autocomplete from '../../../../../../components/Autocomplete'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    },
    upload: {
        display: 'none'
    },
    dropzone: {
        height: 90,
        border: '1px solid #bdbdbd',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        textTransform: 'none',
        margin: '.5em 0',
        borderRadius: 5
    },
    dropzoneHelperWrapper: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    labelError: {
        display: 'flex',
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    textSuccess:{
        color:'#5cb660'
    }
}

function ModalReprintReason(props) {
    const { classes, open = false, close, dataRow, reprintReason, getReprintReason, enqueueSnackbar, handleNextStep,
        uploadDocument, odcReprint, registerReprintReason, handleSaveStateReprint, reprintType, documentType,
        creditCardBin, getCreditCardBin, odcMaster, preloadHolderClient, preloadAdditionalClient } = props

    const reprint = new ReprintFactory(reprintType).createReprint()
    const prevReprintReason = useRef(reprintReason)
    const prevCreditCardBin = useRef(creditCardBin)
    const prevOdcReprint = useRef(odcReprint)
    const prevOdcMaster = useRef(odcMaster)
    const reintentProcess = useRef(0)

    const [ buttonsStatus, setButtonsStatus ] = useState({
        cancel: {
            disable: false
        },
        continue: {
            loading: false,
            disable: false
        }
    })
    const [ reasons, setReasons ] = useState([])
    const [ reason, setReason ] = useState({
        reprintReasonId: 0
    })
    const [ document, setDocument ] = useState({
        file: null,
        progressUploadFile: 0
    })
    const [ validationReason, setValidationReason ] = useState({
        reprintReasonId: {
            error: false,
            message: '',
            ref: useRef(null)
        }
    })
    const [ validationDocument, setValidationDocument ] = useState({
        file: {
            error: false,
            message: '',
            ref: useRef(null)
        }
    })

    const [ dataCreditCardBin, isLoadingCreditCardBin, isSuccessCreditCardBin, isErrorCreditCardBin ] =
        useReduxState({ props: creditCardBin, prevProps: prevCreditCardBin.current, notify: enqueueSnackbar,
            options: { stateName: 'Productos' } })
    const [ dataReprintReason, isLoadingReprintReason, isSuccessReprintReason, isErrorReprintReason ] =
        useReduxState({ props: reprintReason, prevProps: prevReprintReason.current, notify: enqueueSnackbar,
            options: { stateName: 'Motivos Reimpresión' } })
    const [ dataDocumentUpload, isLoadingDocumentUpload, isSuccessDocumentUpload, isErrorDocumentUpload ] =
        useReduxState({ props: odcReprint.documentUploaded, prevProps: prevOdcReprint.current.documentUploaded, notify: enqueueSnackbar })
    const [ dataPreloadHolderClient, isLoadingPreloadHolderClient, isSuccessPreloadHolderClient, isErrorPreloadHolderClient ] =
        useReduxState({ props: odcMaster.clientPreloaded, prevProps: prevOdcMaster.current.clientPreloaded, notify: enqueueSnackbar })
    const [ dataPreloadAdditionalClient, isLoadingPreloadAdditionalClient, isSuccessPreloadAdditionalClient, isErrorPreloadAdditionalClient ] =
        useReduxState({ props: odcMaster.additionalClientPreloaded, prevProps: prevOdcMaster.current.additionalClientPreloaded, notify: enqueueSnackbar })
    const [ dataRegisterReprintReason, isLoadingRegisterReprintReason, isSuccessRegisterReprintReason, isErrorRegisterReprintReason ] =
        useReduxState({ props: odcReprint.reprintReasonRegistered, prevProps: prevOdcReprint.current.reprintReasonRegistered, notify: enqueueSnackbar })

    useEffect(_ => {
        if (open) {
            setReason({ reprintReasonId: 0 })
            reintentProcess.current = 0
            getCreditCardBin()
            getReprintReason()
        }
    }, [open])

    useEffect(_ => {
        if (isLoadingReprintReason || isLoadingCreditCardBin) changeButtonStatus({ cancelDisabled: true, continueLoading: true, continueDisabled: true })
        if (isSuccessReprintReason && isSuccessCreditCardBin) {
            const reasonTypes = dataReprintReason?.map(item => ({
                value: item.cod_valor,
                label: item.des_valor
            })) || []
            changeButtonStatus({ cancelDisabled: false, continueLoading: false, continueDisabled: false })
            setReasons(reasonTypes)
        }
        if (isErrorReprintReason || isErrorCreditCardBin) changeButtonStatus({ cancelDisabled: false, continueLoading: false, continueDisabled: true })
    }, [dataReprintReason, isLoadingReprintReason, isSuccessReprintReason, isErrorReprintReason,
        dataCreditCardBin, isLoadingCreditCardBin, isSuccessCreditCardBin, isErrorCreditCardBin])

    useEffect(_ => {
        if (isLoadingDocumentUpload) changeButtonStatus({ cancelDisabled: true, continueLoading: true, continueDisabled: true })
        if (isSuccessDocumentUpload) {
            const { des_archivo_adjunto_reimpresion } = dataDocumentUpload?.documento
            if (des_archivo_adjunto_reimpresion)
                Notistack.getNotistack('Documento adjunto almacenado.', enqueueSnackbar, 'info')
            else
                Notistack.getNotistack('No ha adjuntado documento.', enqueueSnackbar, 'warning')
            reintentProcess.current = 1
            handleContinueProcess()
        }
        if (isErrorDocumentUpload) {
            reintentProcess.current = 1
            handleContinueProcess()
        }
    }, [dataDocumentUpload, isLoadingDocumentUpload, isSuccessDocumentUpload, isErrorDocumentUpload])

    useEffect(_ => {
        if (isLoadingPreloadHolderClient) changeButtonStatus({ cancelDisabled: true, continueLoading: true, continueDisabled: true })
        if (isSuccessPreloadHolderClient) {
            reintentProcess.current = 3
            handleContinueProcess()
        }
        if (isErrorPreloadHolderClient) changeButtonStatus({ cancelDisabled: false, continueLoading: false, continueDisabled: false })
    }, [dataPreloadHolderClient, isLoadingPreloadHolderClient, isSuccessPreloadHolderClient, isErrorPreloadHolderClient])

    useEffect(_ => {
        if (isLoadingPreloadAdditionalClient) changeButtonStatus({ cancelDisabled: true, continueLoading: true, continueDisabled: true })
        if (isSuccessPreloadAdditionalClient) {
            reintentProcess.current = 3
            handleContinueProcess()
        }
        if (isErrorPreloadAdditionalClient) changeButtonStatus({ cancelDisabled: false, continueLoading: false, continueDisabled: false })
    }, [dataPreloadAdditionalClient, isLoadingPreloadAdditionalClient, isSuccessPreloadAdditionalClient, isErrorPreloadAdditionalClient])

    useEffect(_ => {
        if (isLoadingRegisterReprintReason) changeButtonStatus({ cancelDisabled: true, continueLoading: true, continueDisabled: true })
        if (isSuccessRegisterReprintReason) {
            const document = documentType.data.find(document => document.cod_valor === dataRow.cod_tipo_documento ||
                document.des_valor_corto === dataRow.des_tipo_documento)
            const data = reprint.getDataForCreditCardDetailSection({
                ...dataRow,
                clientPreloaded: (Utils.comprobeObjectIsEmpty(dataPreloadHolderClient) ? dataPreloadAdditionalClient : dataPreloadHolderClient) || { },
                documentTypeInternalValue: document.valor_interno,
                documentTypeAux: document.des_auxiliar,
                reprintReasonId: reason.reprintReasonId,
                responseOfRegisterReprintReason: dataRegisterReprintReason,
                bins: dataCreditCardBin
            })
            handleSaveStateReprint({ clientOnReprint: data })
            Notistack.getNotistack('Consulta Correcta, 2do Paso Ok!', enqueueSnackbar, 'success')
            setTimeout(_ => {
                handleNextStep()
            }, 1500)
        }
        if (isErrorRegisterReprintReason) changeButtonStatus({ cancelDisabled: false, continueLoading: false, continueDisabled: false })
    }, [dataRegisterReprintReason, isLoadingRegisterReprintReason, isSuccessRegisterReprintReason, isErrorRegisterReprintReason])

    const changeButtonStatus = ({ cancelDisabled, continueLoading, continueDisabled }) => {
        setButtonsStatus({ cancel: { disable: cancelDisabled }, continue: { loading: continueLoading, disable: continueDisabled } })
    }

    const handleChangeState = e => {
        const { value, call = _ => null } = e
        setReason({ reprintReasonId: value })
        setValidationReason(state => {
            let validations = { ...state }
            if (Utils.findProperty(validations, 'reprintReasonId', ['ref']).length > 0) {
                validations = {
                    reprintReasonId: {
                        ...Validations.validateByField('reprintReasonId', value)
                    }
                }
            }
            call(validations)
            return validations
        })
    }
    
    const handleUploadFile = e => {
        const { files } = e.target || []
        const file = Utils.comprobeFileProperties({
            file: files[0],
            maxSize: 2.9e+7,
            extensions: [ 'pdf' ]
        })
        setDocument(state => ({
            ...state,
            file: file.file
        }))
        setValidationDocument(state => ({
            ...state,
            file: {
                ...state.file,
                error: !file.file,
                message: file.message
            }
        }))
    }

    const handleContinueProcess = _ => {
        Validations.comprobeAllValidationsSuccess(reason, handleChangeState).then(valid => {
            if (valid) {
                const steps = getSteps()
                steps[reintentProcess.current].action()
            }
        })
    }

    const getSteps = _ => ({
        0: {
            action: _ => {
                const dataUploadFile = reprint.getDataForUploadFileApi({ ...dataRow, file: document.file })
                const sendDataUploadFile = {
                    object: dataUploadFile,
                    onUploadProgress: (progressEvent) => {
                        const percentCompleted = Utils.calculatePercentUploadFile(progressEvent.loaded, progressEvent.total)
                        setDocument(state => ({
                            ...state,
                            progressUploadFile: percentCompleted
                        }))
                    }
                }
                uploadDocument(sendDataUploadFile)
            }
        },
        1: {
            action: _ => {
                const isHolderClient = dataRow.cod_tipo_relacion === Constants.ClientRelationTypes.holderClientCode
                if (isHolderClient) {
                    const document = documentType.data.find(document => document.cod_valor === dataRow.cod_tipo_documento ||
                        document.des_valor_corto === dataRow.des_tipo_documento)
                    const sendData = {
                        solicitudeCode: dataRow.solicitudeCode,
                        completeSolicitudeCode: dataRow.completeSolicitudeCode,
                        documentType: document.des_auxiliar,
                        documentNumber: dataRow.des_nro_documento,
                        activityName: MasterOrigination.preloadClientActivity.activityName,
                        phaseCode: MasterOrigination.preloadClientActivity.phaseCode,
                        masterStageCode: Constants.MasterConfigurationStage.masterReprintStage,
                        clientTypeCode: Constants.ClientTypes.holderClientCode
                    }
                    preloadHolderClient(sendData)
                } else {
                    reintentProcess.current = 2
                    handleContinueProcess()
                }
            }
        },
        2: {
            action: _ => {
                const document = documentType.data.find(document => document.cod_valor === dataRow.cod_tipo_documento ||
                    document.des_valor_corto === dataRow.des_tipo_documento)
                const sendData = {
                    solicitudeCode: dataRow.solicitudeCode,
                    completeSolicitudeCode: dataRow.completeSolicitudeCode,
                    additionalDocumentTypeAux: document.des_auxiliar,
                    additionalDocumentNumber: dataRow.des_nro_documento,
                    holderDocumentTypeAux: dataRow.principalClient.documentTypeAux,
                    holderDocumentNumber: dataRow.principalClient.documentNumber,
                    clientTypeCode: Constants.ClientTypes.additionalClientCode,
                    masterStageCode: Constants.MasterConfigurationStage.masterReprintStage
                }
                preloadAdditionalClient(sendData)
            }
        },
        3: {
            action: _ => {
                const sendData = reprint.getDataForRegisterReprintReasonApi({ 
                    ...dataRow,
                    clientPreloaded: Utils.comprobeObjectIsEmpty(dataPreloadHolderClient) ? dataPreloadAdditionalClient : dataPreloadHolderClient,
                    reprintReasonId: reason.reprintReasonId,
                    documentUploaded: dataDocumentUpload?.documento?.des_archivo_adjunto_reimpresion || ''
                })
                registerReprintReason(sendData)
            }
        }
    })

    return (
        <Modal
            title='Motivo Reimpresión'
            body={
                <>
                    <Grid
                        item>
                            <FormControl
                                error={ validationReason.reprintReasonId.error }
                                fullWidth>
                                    <Autocomplete
                                        error={ validationReason.reprintReasonId.error }
                                        autoFocus
                                        onChange={ e => handleChangeState({ value: e?.value || 0 }) }
                                        value={ reason.reprintReasonId }
                                        data={ reasons }
                                        placeholder='Seleccionar motivo' />
                            </FormControl>
                            <Fade>
                                <FormHelperText
                                    className={ classes.labelError }>
                                        <Typography
                                            component='span'
                                            variant='inherit'
                                            color={ validationReason.reprintReasonId.error 
                                                ? 'secondary' : 'default' }>
                                                    {
                                                        validationReason.reprintReasonId.error &&
                                                        validationReason.reprintReasonId.message
                                                    }
                                        </Typography>
                                </FormHelperText>
                            </Fade>
                    </Grid>
                    <Grid
                        item
                        className='p-sm-4'>
                            <input
                                className={ classes.upload }
                                name='file'
                                accept='.pdf'
                                onChange={ e => handleUploadFile(e) }
                                id='file'
                                type='file' />
                            <FormLabel
                                htmlFor='file'
                                style={{ width: '100%' }}>
                                    <Button
                                        className={ classes.dropzone }
                                        margin='normal'
                                        variant='outlined'
                                        component='span' >
                                            <AttachFileIcon />
                                            <FormLabel
                                                component='span'
                                                variant='inherit'
                                                align='center' >
                                                    <Typography
                                                        component='span'
                                                        variant='inherit'
                                                        align='center' >
                                                            <Typography
                                                                component='span'
                                                                variant='inherit'
                                                                className='mb-1' >
                                                                    Click Adjuntar Documento
                                                            </Typography>
                                                    </Typography>
                                                    <Typography
                                                        component='span'
                                                        variant='inherit'>
                                                            Limite 29MB Max.
                                                    </Typography>
                                            </FormLabel>
                                    </Button>
                            </FormLabel>
                            <FormHelperText
                                className={ classNames(classes.dropzoneHelperWrapper, 'mt-2') }>
                                    <Typography
                                        component='span'
                                        noWrap
                                        variant='inherit'>
                                            <Typography 
                                                className={ classes.textSuccess }
                                                component='span'
                                                noWrap
                                                variant='inherit'>
                                                    { (!validationDocument.file.error && document.file) && document.file.name }
                                            </Typography>
                                            <Typography 
                                                color={ validationDocument.file.error
                                                    ? 'secondary'
                                                    : 'default'}
                                                component='span'
                                                noWrap
                                                variant='inherit'>
                                                    { validationDocument.file.error &&
                                                        validationDocument.file.message }
                                            </Typography>
                                    </Typography>
                                    <Typography
                                        component='span'
                                        variant='inherit'
                                        className='ml-2'>
                                            { document.file ? Number(document.file.size / 1e+6).toFixed(2) : 0 }MB.
                                    </Typography>
                            </FormHelperText>
                    </Grid>
                    <Grid
                        item
                        xs={ 12 }>
                        { document.progressUploadFile > 0 &&
                            <LinearProgress
                                variant='determinate'
                                value={ document.progressUploadFile } /> }
                    </Grid>
                </>
            }
            open={ open }
            actions={
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        loading={ buttonsStatus.cancel.disable }
                                        text='Cancelar'
                                        type='secondary'
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        loading={ buttonsStatus.continue.disable }
                                        text='Aceptar'
                                        type='primary'
                                        handleAction={ _ => handleContinueProcess() }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        showLoading={ buttonsStatus.continue.loading }/>
                                </div>
                        </Grid>
                </Grid>
            }/>
    )
}

function mapStateToProps(state) {
    return {
        documentType: state.identificationDocumentTypeReducer,
        reprintReason: state.reasonReprintReducer,
        odcReprint: state.odcReprintReducer,
        creditCardBin: state.creditCardBinReducer,
        odcMaster: state.odcMasterReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getCreditCardBin: bindActionCreators(ActionCreditCardBin.getCreditCardBin, dispatch),
        getReprintReason: bindActionCreators(ActionReprintReasons.getReasonReprint, dispatch),
        uploadDocument: bindActionCreators(ActionOdcReprint.uploadDocument, dispatch),
        registerReprintReason: bindActionCreators(ActionOdcReprint.registerReprintReason, dispatch),
        preloadHolderClient: bindActionCreators(ActionOdcMaster.preloadClient, dispatch),
        preloadAdditionalClient: bindActionCreators(ActionOdcMaster.preloadAdditionalClient, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalReprintReason))))