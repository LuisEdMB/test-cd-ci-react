// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Divider, Grid, Tooltip, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import LabelIcon from '@material-ui/icons/Label'
import CreditCardIcon from '@material-ui/icons/CreditCard'
import CardMembershipIcon from '@material-ui/icons/CardMembership'
import LabelImportantIcon from '@material-ui/icons/LabelImportant'
import LensIcon from '@material-ui/icons/Lens'
import SendIcon from '@material-ui/icons/Send'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcReprint from '../../../../../actions/odc-reprint/odc-reprint'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Effects
import Fade from 'react-reveal/Fade'
import Bounce from 'react-reveal/Bounce'
import Zoom from 'react-reveal/Zoom'
import * as Fireworks from 'fireworks-canvas'

// Utils
import classNames from 'classnames'
import * as Utils from '../../../../../utils/Utils'
import ReprintFactory from '../../../../../utils/reprint/ReprintFactory'

// Components
import Image from '../../../../../components/Image'
import ItemReprintSummary from './components/ItemReprintSummary'
import ActionButton from '../../../../../components/ActionButton'

// Media
import LogoCencosud from '../../../../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg'

const styles = theme => ({
    root: {
        position: 'relative'
    },
    firework: {
        width: '100%',
        height: 500,
        position: 'absolute',
        top: 20
    },
    summaryWrapper: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    summary: {
        minWidth: 300,
        maxWidth: 700
    },
    titleContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatarTitleWrapper: {
        [theme.breakpoints.down('sm')]: {
            display: 'none'
        },
    },
    avatarTitle: {
        margin: '0 .5em',
        height: 36, 
        width: 64,
        [theme.breakpoints.down('sm')]: {

        },
        [theme.breakpoints.up('md')]: {
            height: 48, 
            width: 80
        },
        [theme.breakpoints.up('lg')]: {

        }
    },
    titleWrapper: {
        margin: '0 1em',
    },
    title: {
        [theme.breakpoints.down('sm')]: {
            fontSize: 18
        }
    },
    dateWrapper: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
    },
    headerContainer: {
        display: 'flex', 
        flexDirection: 'column',
        [theme.breakpoints.up('md')]: {
            flexDirection: 'row',
            justifyContent: 'space-between'
        }
    },
    bodyContainer: {
        padding: '.5em',
        border: '1px solid #c4c4c4',
        borderRadius: '.25rem',
        [theme.breakpoints.up('md')]: {
            padding: '1.5em',
        }
    },
    buttonActions: {
        display: 'flex', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    buttonProgressWrapper: {
        position: 'relative'
    }
})

function ReprintSummary(props) {
    const { classes, handleNextStep, stateReprint, odcReprint, registerActivity, enqueueSnackbar, reprintType } = props

    const reprint = new ReprintFactory(reprintType).createReprint()
    const prevOdcReprint = useRef(odcReprint)

    const [ loading, setLoading ] = useState(false)

    const [ dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity ] =
        useReduxState({ props: odcReprint.activityRegistered, prevProps: prevOdcReprint.current.activityRegistered, notify: enqueueSnackbar })

    useEffect(_ => {
        const container = document.getElementById('summary-container')
        const options = {
            maxRockets: 3,
            rocketSpawnInterval: 10,
            numParticles: 70,
            explosionMinHeight: 0.2,
            explosionMaxHeight: 0.9,
            explosionChance: 0.08
        }
        const fireworks = new Fireworks(container, options)
        fireworks.start()
        fireworks.fire()
    }, [])

    useEffect(_ => {
        if (isLoadingRegisterActivity) setLoading(true)
        if (isSuccessRegisterActivity) {
            Notistack.getNotistack('Consulta Correcta, 5to Paso Ok!', enqueueSnackbar, 'success')
            setTimeout(_ => {
                handleNextStep()
            }, 2500)
        }
        if (isErrorRegisterActivity) setLoading(false)
    }, [dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity])

    const handleContinueProcess = _ => {
        const sendData = reprint.getDataForRegisterActivityOnReprintSummarySectionApi(stateReprint.clientOnReprint)
        registerActivity(sendData)
    }

    return (
        <Grid
            container
            spacing={ 8 }
            className={ classes.root }>
                <Grid
                    id='summary-container'
                    item
                    xs={ 12 }
                    className={ classes.firework } />
                <Grid
                    item
                    xs={ 12 }
                    className={ classes.summaryWrapper }>
                        <Grid
                            container
                            spacing={ 8 }
                            className={ classNames(classes.summary, 'mb-4') }>
                                <Grid
                                    item
                                    xs={ 12 }>
                                        <Fade>
                                            <figure
                                                className={ classNames(classes.titleContainer, 'p-3') }>
                                                    <div
                                                        className={ classes.avatarTitleWrapper }>
                                                            <Bounce>
                                                                <Image
                                                                    className={ classes.avatarTitle }
                                                                    src={ LogoCencosud }
                                                                    minHeight={ 48 }
                                                                    alt ='Cencosud' />
                                                            </Bounce>
                                                    </div>
                                                    <div
                                                        className={ classes.titleWrapper }>
                                                            <Typography 
                                                                align='center' 
                                                                component='title' 
                                                                className={ classes.title }
                                                                variant='h6'>
                                                                ¡Reimpresión de Tarjeta Exitosa!
                                                            </Typography>
                                                    </div>
                                                    <div
                                                        className={ classes.dateWrapper }>
                                                            <Typography
                                                                component='span'
                                                                className='mt-1 mr-2'>
                                                                    <CalendarTodayIcon
                                                                        fontSize='small'/>
                                                            </Typography>
                                                            <Typography
                                                                component='span'>
                                                                { Utils.getDateCurrent(1, false, true).replace('T', ' ') }
                                                            </Typography>
                                                    </div>
                                            </figure>
                                        </Fade>
                                </Grid>
                                <Grid
                                    item
                                    xs={ 12 }
                                    className='mb-2'>
                                        <Divider />
                                </Grid>
                                <Grid
                                    item
                                    xs={ 12 }
                                    className='py-3'>
                                        <div
                                            className={ classes.headerContainer }>
                                                <Typography
                                                    className='font-weight-bold mb-2'
                                                    color='textSecondary'>
                                                        { stateReprint.clientOnReprint?.documentNumber || '' }
                                                </Typography>
                                                <Typography
                                                    className='font-weight-bold'
                                                    color='textSecondary'>
                                                        { `${ stateReprint.clientOnReprint?.firstLastName } ` +
                                                            `${ stateReprint.clientOnReprint?.secondLastName } ` +
                                                            `${ stateReprint.clientOnReprint?.firstName } ` +
                                                            `${ stateReprint.clientOnReprint?.secondName }` }
                                                </Typography>                                  
                                        </div>
                                </Grid>
                                <Grid
                                    item
                                    xs={ 12 }>
                                        <ul
                                            className={ classes.bodyContainer }>
                                                <ItemReprintSummary
                                                    title='Nro. Solicitud'
                                                    icon={
                                                        <LabelIcon
                                                            fontSize='small'/>
                                                    }
                                                    value={ stateReprint.clientOnReprint?.completeSolicitudeCode }
                                                />
                                                <ItemReprintSummary
                                                    title='Nro. Tarjeta'
                                                    icon={
                                                        <CreditCardIcon
                                                            fontSize='small' />
                                                    }
                                                    value={ Utils.maskCreditCard(stateReprint.clientOnReprint?.creditCard?.newCardNumber || '') }
                                                />
                                                <ItemReprintSummary
                                                    title='Marca Tarjeta'
                                                    icon={
                                                        <CardMembershipIcon
                                                            fontSize='small' />
                                                    }
                                                    value={ stateReprint.clientOnReprint?.creditCard?.brandDescription || '' }
                                                />
                                                <ItemReprintSummary
                                                    title='Tipo Tarjeta'
                                                    icon={
                                                        <LabelImportantIcon
                                                            fontSize='small' />
                                                    }
                                                    value={ stateReprint.clientOnReprint?.creditCard?.productDescription || '' }
                                                />
                                                <ItemReprintSummary
                                                    title='Color Tarjeta'
                                                    icon={
                                                        <LensIcon
                                                            fontSize='small'/>
                                                    }
                                                    value={
                                                        stateReprint.clientOnReprint?.creditCard?.colorAux
                                                            ? <LensIcon
                                                                style={{ color: stateReprint.clientOnReprint?.creditCard?.colorAux }} />
                                                            : 'NO APLICA'
                                                    }
                                                />
                                        </ul>
                                </Grid>
                        </Grid>
                </Grid>
                <Grid
                    item
                    xs={ 12 }
                    className={ classes.buttonActions }>
                        <Grid
                            container
                            spacing={ 8 }>
                                <Grid
                                    item
                                    xs={ 12 }
                                    sm={ 6 }
                                    md={ 4 }
                                    lg={ 3 }
                                    style={{ margin: '0 auto' }}>
                                        <Tooltip
                                            TransitionComponent={ Zoom }
                                            title='Click para continuar con el proceso de reimpresión.'>
                                                <div>
                                                    <Bounce>
                                                        <div
                                                            className={ classes.buttonProgressWrapper }>
                                                                <ActionButton
                                                                    text='Continuar'
                                                                    type='primary'
                                                                    loading={ loading }
                                                                    handleAction={ _ => handleContinueProcess() }
                                                                    icon={
                                                                        <SendIcon
                                                                            fontSize='small'
                                                                            className='ml-2' />
                                                                    }
                                                                    showLoading />
                                                        </div>
                                                    </Bounce>
                                                </div>
                                        </Tooltip>
                                </Grid>
                        </Grid>
                </Grid>
        </Grid>
    )
}

function mapStateToProps(state) {
    return {
        odcReprint: state.odcReprintReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerActivity: bindActionCreators(ActionOdcReprint.registerActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ReprintSummary))))