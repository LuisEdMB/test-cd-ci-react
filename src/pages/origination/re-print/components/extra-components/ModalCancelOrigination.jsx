// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'

// Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Redux - Actions
import * as ActionOdcReprint from '../../../../../actions/odc-reprint/odc-reprint'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'

// Notify
import { withSnackbar } from 'notistack'

// Components
import Modal from '../../../../../components/Modal'
import ActionButton from '../../../../../components/ActionButton'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalCancelOrigination(props) {
    const { classes, open = false, close, data, odcReprint, cancelActivity, enqueueSnackbar, handleReset } = props

    const prevOdcReprint = useRef(odcReprint)

    const [ loading, setLoading ] = useState(false)

    const [ dataCancelActivity, isLoadingCancelActivity, isSuccessCancelActivity, isErrorCancelActivity ] = 
        useReduxState({ props: odcReprint.activityCanceled, prevProps: prevOdcReprint.current.activityCanceled, notify: enqueueSnackbar })

    useEffect(_ => {
        if (isLoadingCancelActivity) setLoading(true)
        if (isSuccessCancelActivity) {
            handleReset()
            setLoading(false)
        }
        if (isErrorCancelActivity) setLoading(false)
    }, [dataCancelActivity, isLoadingCancelActivity, isSuccessCancelActivity, isErrorCancelActivity])

    const handleRegisterCancelActivity = _ => {
        cancelActivity(data)
    }

    return (
        <Modal
            title='Cancelar Proceso de Reimpresión'
            body= { 
                <Typography
                    align='center'>
                        ¿Esta seguro de cancelar la reimpresión?
                </Typography>
            }
            open={ open } 
            actions={ 
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        loading={ loading }
                                        text='No'
                                        type='secondary'
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        loading={ loading }
                                        text='Sí'
                                        type='primary'
                                        handleAction={ _ => handleRegisterCancelActivity() }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
        }/>
    )
}

function mapStateToProps(state) {
    return {
        odcReprint: state.odcReprintReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        cancelActivity: bindActionCreators(ActionOdcReprint.cancelActivity, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalCancelOrigination))))