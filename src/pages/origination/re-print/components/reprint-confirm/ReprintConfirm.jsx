// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { FormControl, Grid, InputLabel, MenuItem, OutlinedInput, Select, TextField, Tooltip, Typography, withStyles } from '@material-ui/core'
import Label from '@material-ui/icons/Label'

// Material UI - Icons
import SendIcon from '@material-ui/icons/Send'
import CancelIcon from '@material-ui/icons/Cancel'

// Hooks
import useReduxState from '../../../../../hooks/general/useReduxState'
import useDecryptCardNumber from '../../../../../hooks/pci/useDecryptCardNumber'
import useBlockTypeCreditCard from '../../../../../hooks/pmp/useBlockTypeCreditCard'
import useBlockTypeCreditCardMaster from '../../../../../hooks/master/useBlockTypeCreditCard'
import useCreateCreditCard from '../../../../../hooks/pmp/useCreateCreditCard'
import useEncryptCardNumber from '../../../../../hooks/pci/useEncryptCardNumber'
import useCreateCreditCardMaster from '../../../../../hooks/master/useCreateCreditCard'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionProductColors from '../../../../../actions/value-list/product-color'
import * as ActionOdcReprint from '../../../../../actions/odc-reprint/odc-reprint'
import * as ActionPmp from '../../../../../actions/pmp/pmp'
import * as ActionPci from '../../../../../actions/pci/pci'
import * as ActionOdcMaster from '../../../../../actions/odc-master/odc-master'

// Effects
import Bounce from 'react-reveal/Bounce'
import Zoom from 'react-reveal/Zoom'
import Fade from 'react-reveal/Fade'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../utils/Notistack'

// Utils
import classNames from 'classnames'
import * as Constants from '../../../../../utils/Constants'
import ReprintFactory from '../../../../../utils/reprint/ReprintFactory'

// Components
import ActionButton from '../../../../../components/ActionButton'
import CreditCard from '../../../../../components/CreditCard'

const styles = {
    root: {
        maxWidth: 900
    },
    wrapper: {
        position: 'relative'
    },
    creditCard: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    menu: {
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center'
    }
}

function ReprintConfirmOld(props) {
    const { classes, showModalCancelActivity, handleNextStep, stateReprint, productColors, getAllProductColors,
        odcReprint, registerActivity, pmp, pci, decryptCardnumber, blockTypeCreditCard, blockTypeCreditCardReprint,
        createCreditCard, encryptCardnumber, odcMaster, updateBlockingCodeCreditCardIntoMaster, 
        registerCreditCardIntoMaster, enqueueSnackbar, handleSaveStateReprint, reprintType,
        phaseCodeFromContinueProcess } = props

    const reprint = new ReprintFactory(reprintType).createReprint()
    const isFinish = useRef(false)
    const reintentProcess = useRef(0)
    const prevOdcReprint = useRef(odcReprint)
    const prevProductColors = useRef(productColors)
    const prevPmp = useRef(pmp)
    const prevPci = useRef(pci)
    const prevOdcMaster = useRef(odcMaster)

    const [ loading, setLoading ] = useState(false)
    const [ availableColors, setAvailableColors ] = useState([])

    const [ dataProductColors, isLoadingProductColors, isSuccessProductColors, isErrorProductColors ] =
        useReduxState({ props: productColors, prevProps: prevProductColors.current, notify: enqueueSnackbar,
            options: { stateName: 'Colores Productos' } })
    const [ dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity ] =
        useReduxState({ props: odcReprint.activityRegistered, prevProps: prevOdcReprint.current.activityRegistered, notify: enqueueSnackbar })
    const [ dataDecryptCardNumber, isLoadingDecryptCardNumber, isSuccessDecryptCardNumber, isErrorDecryptCardNumber ] =
        useDecryptCardNumber({ props: pci.cardnumberDecrypted, prevProps: prevPci.current.cardnumberDecrypted, notify: enqueueSnackbar,
            options: { flowName: 'Reimpresión' } })
    const [ isLoadingBlockTypeCreditCard, isSuccessBlockTypeCreditCard, isErrorBlockTypeCreditCard ] =
        useBlockTypeCreditCard({ props: pmp.blockTypeCreditCard, prevProps: prevPmp.current.blockTypeCreditCard, notify: enqueueSnackbar,
            options: { typeBlockName: 'Bloquear', flowName: 'Reimpresión' } })
    const [ isLoadingBlockTypeCreditCardReprint, isSuccessBlockTypeCreditCardReprint, isErrorBlockTypeCreditCardReprint ] =
        useBlockTypeCreditCard({ props: pmp.creditCardReprintBlocked, prevProps: prevPmp.current.creditCardReprintBlocked,
            notify: enqueueSnackbar, options: { typeBlockName: 'Bloquear', flowName: 'Reimpresión' } })
    const [ isLoadingBlockTypeCreditCardMaster, isSuccessBlockTypeCreditCardMaster, isErrorBlockTypeCreditCardMaster ] =
        useBlockTypeCreditCardMaster({ props: odcMaster.blockingCodeCreditCardForExternalUpdated,
            prevProps: prevOdcMaster.current.blockingCodeCreditCardForExternalUpdated, notify: enqueueSnackbar,
            options: { typeBlockName: 'Bloquear', flowName: 'Reimpresión' } })
    const [ dataCreateCreditCard, isLoadingCreateCreditCard, isSuccessCreateCreditCard, isErrorCreateCreditCard ] =
        useCreateCreditCard({ props: pmp.createCreditCard, prevProps: prevPmp.current.createCreditCard, notify: enqueueSnackbar,
            options: { flowName: 'Reimpresión' } })
    const [ dataEncryptCardNumber, isLoadingEncryptCardNumber, isSuccessEncryptCardNumber, isErrorEncryptCardNumber,
        errorCodeEncryptCardNumber ] =
            useEncryptCardNumber({ props: pci.cardnumberEncrypted, prevProps: prevPci.current.cardnumberEncrypted, notify: enqueueSnackbar,
                options: { flowName: 'Reimpresión' } })
    const [ isLoadingCreateCreditCardMaster, isSuccessCreateCreditCardMaster, isErrorCreateCreditCardMaster ] =
        useCreateCreditCardMaster({ props: odcMaster.creditCardForExternalRegistered,
            prevProps: prevOdcMaster.current.creditCardForExternalRegistered, notify: enqueueSnackbar,
            options: { flowName: 'Reimpresión' } })

    useEffect(_ => {
        getAllProductColors()
    }, [])

    useEffect(_ => {
        if (isLoadingProductColors) setLoading(true)
        if (isSuccessProductColors) {
            reintentProcess.current = reprint.getReintentProcessForReprintConfirmSection(phaseCodeFromContinueProcess)
            setAvailableColors(dataProductColors?.filter(item =>
                item.cod_producto === stateReprint.clientOnReprint?.creditCard?.productId &&
                item.flg_activo) || [])
            setLoading(false)
        }
    }, [dataProductColors, isLoadingProductColors, isSuccessProductColors, isErrorProductColors])

    useEffect(_ => {
        if (isLoadingRegisterActivity) setLoading(true)
        if (isSuccessRegisterActivity) {
            if (isFinish.current) {
                const { createCreditCard } = dataCreateCreditCard?.pmpCrearTarjetaCredito || { }
                const { creditCard } = stateReprint.clientOnReprint
                handleSaveStateReprint({
                    clientOnReprint: {
                        ...stateReprint.clientOnReprint,
                        creditCard: {
                            ...creditCard,
                            newCardNumber: creditCard.newCardNumber || createCreditCard?.cardNumber,
                            newToken: creditCard.newToken || dataEncryptCardNumber?.token
                        }
                    }
                })
                Notistack.getNotistack('Reimpresión: Aceptado', enqueueSnackbar, 'info')
                setTimeout(_ => {
                    Notistack.getNotistack('Consulta Correcta, 4to Paso Ok!', enqueueSnackbar, 'success')
                }, 1000)
                setTimeout(_ => {
                    handleNextStep()
                }, 2500)
            } else {
                Notistack.getNotistack('Reimpresión: En Proceso', enqueueSnackbar, 'info')
                reintentProcess.current = 1
                handleConfirmReprint()
            }
        }
        if (isErrorRegisterActivity) setLoading(false)
    }, [dataRegisterActivity, isLoadingRegisterActivity, isSuccessRegisterActivity, isErrorRegisterActivity])

    useEffect(_ => {
        if (isLoadingDecryptCardNumber) setLoading(true)
        if (isSuccessDecryptCardNumber) {
            reintentProcess.current = 2
            handleConfirmReprint()
        }
        if (isErrorDecryptCardNumber) setLoading(false)
    }, [dataDecryptCardNumber, isLoadingDecryptCardNumber, isSuccessDecryptCardNumber, isErrorDecryptCardNumber])

    useEffect(_ => {
        if (isLoadingBlockTypeCreditCard || isLoadingBlockTypeCreditCardReprint) setLoading(true)
        if (isSuccessBlockTypeCreditCard || isSuccessBlockTypeCreditCardReprint) {
            reintentProcess.current = 3
            handleConfirmReprint()
        }
        if (isErrorBlockTypeCreditCard || isErrorBlockTypeCreditCardReprint) setLoading(false)
    }, [isLoadingBlockTypeCreditCard, isSuccessBlockTypeCreditCard, isErrorBlockTypeCreditCard, 
        isLoadingBlockTypeCreditCardReprint, isSuccessBlockTypeCreditCardReprint,
        isErrorBlockTypeCreditCardReprint])

    useEffect(_ => {
        if (isLoadingBlockTypeCreditCardMaster) setLoading(true)
        if (isSuccessBlockTypeCreditCardMaster) {
            reintentProcess.current = 4
            handleConfirmReprint()
        }
        if (isErrorBlockTypeCreditCardMaster) setLoading(false)
    }, [isLoadingBlockTypeCreditCardMaster, isSuccessBlockTypeCreditCardMaster, isErrorBlockTypeCreditCardMaster])

    useEffect(_ => {
        if (isLoadingCreateCreditCard) setLoading(true)
        if (isSuccessCreateCreditCard) {
            reintentProcess.current = 5
            handleConfirmReprint()
        }
        if (isErrorCreateCreditCard) setLoading(false)
    }, [dataCreateCreditCard, isLoadingCreateCreditCard, isSuccessCreateCreditCard, isErrorCreateCreditCard])

    useEffect(_ => {
        if (isLoadingEncryptCardNumber) setLoading(true)
        if (isSuccessEncryptCardNumber) {
            reintentProcess.current = 6
            handleConfirmReprint()
        }
        if (isErrorEncryptCardNumber) {
            if (errorCodeEncryptCardNumber === Constants.CustomErrorCode.errorCodeCreditCardAlreadyPci) {
                reintentProcess.current = 6
                handleConfirmReprint()
            } else setLoading(false)
        }
    }, [dataEncryptCardNumber, isLoadingEncryptCardNumber, isSuccessEncryptCardNumber, isErrorEncryptCardNumber, errorCodeEncryptCardNumber])

    useEffect(_ => {
        if (isLoadingCreateCreditCardMaster) setLoading(true)
        if (isSuccessCreateCreditCardMaster) {
            reintentProcess.current = 7
            handleConfirmReprint()
        }
        if (isErrorCreateCreditCardMaster) setLoading(false)
    }, [isLoadingCreateCreditCardMaster, isSuccessCreateCreditCardMaster, isErrorCreateCreditCardMaster])

    const handleConfirmReprint = _ => {
        const steps = getSteps()
        steps[reintentProcess.current].action()
    }

    const getSteps = _ => ({
        0: {
            action: _ => {
                const sendData = reprint.getDataForRegisterActivityInitOnReprintConfirmSectionApi(stateReprint.clientOnReprint)
                registerActivity(sendData)
            }
        },
        1: {
            action: _ => {
                const isOptionalToken = reprint.isOptionalTokenForDeactivatePreviousCreditCard
                const token = stateReprint.clientOnReprint?.creditCard?.token
                if (!isOptionalToken || (isOptionalToken && token)) {
                    const sendData = reprint.getDataForDecryptPreviousCardnumberApi(stateReprint.clientOnReprint)
                    decryptCardnumber(sendData)
                } else {
                    reintentProcess.current = 2
                    handleConfirmReprint()
                }
                
            }
        },
        2: {
            action: _ => {
                const { creditCard } = stateReprint.clientOnReprint
                const sendData = reprint.getDataForBlockTypeCreditCardApi({
                    ...stateReprint.clientOnReprint,
                    cardNumberToDesactivate: dataDecryptCardNumber?.nro_tarjeta || creditCard?.cardNumber
                })
                const actionToProcess = sendData.callPmpBlockingReprint ? blockTypeCreditCardReprint : blockTypeCreditCard
                actionToProcess(sendData)
            }
        },
        3: {
            action: _ => {
                const sendData = reprint.getDataForBlockTypeCreditCardIntoMasterApi(stateReprint.clientOnReprint)
                if (sendData.next) {
                    reintentProcess.current = 4
                    handleConfirmReprint()
                } else updateBlockingCodeCreditCardIntoMaster(sendData)
            }
        },
        4: {
            action: _ => {
                const sendData = reprint.getDataForCreateCreditCardApi(stateReprint.clientOnReprint)
                createCreditCard(sendData)
            }
        },
        5: {
            action: _ => {
                const sendData = reprint.getDataForEncryptCardnumberApi(stateReprint.clientOnReprint)
                encryptCardnumber(sendData)
            }
        },
        6: {
            action: _ => {
                const sendData = reprint.getDataForCreateCreditCardIntoMasterApi(stateReprint.clientOnReprint)
                if (sendData.next) {
                    reintentProcess.current = 7
                    handleConfirmReprint()
                } else registerCreditCardIntoMaster(sendData)
            }
        },
        7: {
            action: _ => {
                const sendData = reprint.getDataForRegisterActivityEndOnReprintConfirmSectionApi(stateReprint.clientOnReprint)
                isFinish.current = true
                registerActivity(sendData)
            }
        }
    })

    const handleCancelProcess = _ => {
        const dataCancel = reprint.getDataForReprintConfirmSectionCancelApi(stateReprint.clientOnReprint)
        showModalCancelActivity(dataCancel)
    }

    return (
        <>
            <Grid
                container
                spacing={ 8 }
                className='d-flex justify-content-center align-items-center'>
                    <Grid
                        container
                        spacing={ 32 }
                        className={ classNames(classes.root, 'm-0 m-sm-3') }>
                            <Grid
                                item
                                xs={ 12 }
                                sm={ 12 }
                                md={ 6 }
                                lg={ 7 }>
                                    <Grid
                                        container
                                        spacing={ 8 } >
                                            {
                                                (stateReprint.clientOnReprint?.relationTypeId === Constants.ClientRelationTypes.holderClientCode) &&
                                                    <>
                                                        <Grid
                                                            item
                                                            xs={ 6 }>
                                                                <TextField
                                                                    fullWidth
                                                                    label='Tipo Documento'
                                                                    type='text'
                                                                    margin='normal'
                                                                    value={ stateReprint.clientOnReprint?.documentTypeLetter || '' }
                                                                    color='default'
                                                                    variant='outlined'
                                                                    InputProps={ {
                                                                        readOnly: true
                                                                    } } />
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={ 6 }>
                                                                <TextField
                                                                    fullWidth
                                                                    label='Número Documento'
                                                                    type='text'
                                                                    margin='normal'
                                                                    value={ stateReprint.clientOnReprint?.documentNumber || '' }
                                                                    color='default'
                                                                    variant='outlined'
                                                                    InputProps={ {
                                                                        readOnly: true
                                                                    } } />
                                                        </Grid>
                                                    </>
                                            }
                                            <Grid
                                                item
                                                xs={ 12 }>
                                                    <TextField
                                                        fullWidth
                                                        label='Nombres y Apellidos'
                                                        type='text'
                                                        margin='normal'
                                                        value={ `${ stateReprint.clientOnReprint?.firstLastName } ${ stateReprint.clientOnReprint?.secondLastName } ` +
                                                                `${ stateReprint.clientOnReprint?.firstName } ${ stateReprint.clientOnReprint?.secondName }` }
                                                        color='default'
                                                        variant='outlined'
                                                        InputProps={ {
                                                            readOnly: true
                                                        } } />
                                            </Grid>
                                            {
                                                availableColors.length > 0 &&
                                                    <Grid
                                                        item
                                                        xs={ 12 }
                                                        sm={ 4 }>
                                                            <FormControl
                                                                fullWidth
                                                                margin='normal'
                                                                variant='outlined'>
                                                                    <InputLabel
                                                                        htmlFor='color'>
                                                                            Color Tarjeta
                                                                    </InputLabel>
                                                                    <Select
                                                                        value={ stateReprint.clientOnReprint?.creditCard?.color }
                                                                        displayEmpty
                                                                        input={
                                                                            <OutlinedInput
                                                                                readOnly
                                                                                labelWidth={ 95 } />
                                                                        }>
                                                                            <MenuItem
                                                                                value=''
                                                                                disabled>Seleccionar Color
                                                                            </MenuItem>
                                                                            {
                                                                                availableColors.map((item, index) => (
                                                                                    <MenuItem
                                                                                        key={ index }
                                                                                        value={ item.cod_color }>
                                                                                            <div
                                                                                                className={ classes.menu }>
                                                                                                <Label
                                                                                                    style={{ color: item.des_aux_color, fontSize: 12}}
                                                                                                    variant='inherit'/>
                                                                                                <Typography
                                                                                                    component='span'
                                                                                                    className='px-2'
                                                                                                    variant='inherit'>
                                                                                                        { item.des_color }
                                                                                                </Typography>
                                                                                            </div>
                                                                                    </MenuItem>
                                                                                ))
                                                                            }
                                                                    </Select>
                                                            </FormControl>
                                                    </Grid>
                                            }
                                            <Grid
                                                item
                                                xs={ 12 }
                                                sm={ 8 }>
                                                    <TextField
                                                        fullWidth
                                                        label='Producto'
                                                        type='text'
                                                        margin='normal'
                                                        value={ stateReprint.clientOnReprint?.creditCard?.productDescription }
                                                        color='default'
                                                        variant='outlined'
                                                        InputProps={ {
                                                            readOnly: true
                                                        } } />
                                            </Grid>
                                    </Grid>
                            </Grid>
                            <Grid
                                item
                                xs={ 12 }
                                sm={ 12 }
                                md={ 6 }
                                lg={ 5 }>
                                    <Grid
                                        container
                                        spacing={ 8 }
                                        className='d-flex justify-content-center'>
                                            <Grid
                                                item
                                                xs={ 12 }
                                                className={ classNames(classes.creditCard,
                                                    `py-${ (stateReprint.clientOnReprint?.relationTypeId === Constants.ClientRelationTypes.holderClientCode)
                                                        ? 2
                                                        : 2 } rounded`) }>
                                                    <div
                                                        style={{ minHeight: 230 }}>
                                                            {
                                                                <Fade>
                                                                    <CreditCard
                                                                        client={ `${ stateReprint.clientOnReprint?.firstLastName } ${ stateReprint.clientOnReprint?.firstName }` }
                                                                        type={ stateReprint.clientOnReprint?.creditCard?.productType }
                                                                        customColor={ stateReprint.clientOnReprint?.creditCard?.colorAux }
                                                                        bin={ stateReprint.clientOnReprint?.creditCard?.bin }
                                                                        cardNumber={ stateReprint.clientOnReprint?.creditCard?.newCardNumber ||
                                                                            stateReprint.clientOnReprint?.creditCard?.cardNumber }
                                                                        brand={ stateReprint.clientOnReprint?.creditCard?.brand === 200001 ? 0 : 1 } />
                                                                </Fade>
                                                            }
                                                    </div>
                                            </Grid>
                                    </Grid>
                            </Grid>
                    </Grid>
            </Grid>
            <Grid
                container
                spacing={ 8 }
                className='d-flex justify-content-center align-items-center'>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 6 }
                        md={ 6 }
                        lg={ 3 }>
                            <Tooltip
                                TransitionComponent={ Zoom }
                                title='Cancelar reimpresión'>
                                    <div>
                                        <Bounce 
                                            left>
                                                <div
                                                    className={ classes.wrapper }>
                                                        <ActionButton
                                                            text='Cancelar'
                                                            type='secondary'
                                                            loading={ loading }
                                                            handleAction={ _ => handleCancelProcess() }
                                                            icon={
                                                                <CancelIcon 
                                                                    fontSize='small' 
                                                                    className='ml-2' />
                                                            }/>
                                                </div>
                                        </Bounce>
                                    </div>
                            </Tooltip>
                    </Grid>
                    <Grid
                        item
                        xs={ 12 }
                        sm={ 6 }
                        md={ 6 }
                        lg={ 3 }>
                            <Tooltip
                                TransitionComponent={ Zoom }
                                title='Click para continuar con el proceso de reimpresión'>
                                    <div>
                                        <Bounce 
                                            right>
                                                <div
                                                    className={ classes.wrapper }>
                                                        <ActionButton
                                                            text='Generar Reimpresión'
                                                            type='primary'
                                                            loading={ loading }
                                                            handleAction={ _ => handleConfirmReprint() }
                                                            icon={
                                                                <SendIcon 
                                                                    fontSize='small' 
                                                                    className='ml-2' />
                                                            }
                                                            showLoading />
                                                </div>
                                        </Bounce>
                                    </div>
                            </Tooltip>
                    </Grid>
            </Grid>
        </>
    )
}

function mapStateToProps(state) {
    return {
        productColors: state.productColorReducer,
        odcReprint: state.odcReprintReducer,
        pmp: state.pmpReducer,
        pci: state.pciReducer,
        odcMaster: state.odcMasterReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getAllProductColors: bindActionCreators(ActionProductColors.getAllProductColors, dispatch),
        registerActivity: bindActionCreators(ActionOdcReprint.registerActivity, dispatch),
        decryptCardnumber: bindActionCreators(ActionPci.decryptCardnumber, dispatch),
        blockTypeCreditCard: bindActionCreators(ActionPmp.blockTypeCreditCard, dispatch),
        blockTypeCreditCardReprint: bindActionCreators(ActionPmp.blockTypeCreditCardReprint, dispatch),
        updateBlockingCodeCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.updateBlockingCodeCreditCardForExternal, dispatch),
        createCreditCard: bindActionCreators(ActionPmp.createCreditCard, dispatch),
        encryptCardnumber: bindActionCreators(ActionPci.encryptCardnumber, dispatch),
        registerCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.registerCreditCardForExternal, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ReprintConfirmOld))))