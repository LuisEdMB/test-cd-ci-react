// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, TextField, withStyles } from '@material-ui/core'

// Material UI - Icons
import CancelIcon from '@material-ui/icons/Cancel'
import SendIcon from '@material-ui/icons/Send'

// Hooks
import useReduxState from '../../../../../../../../hooks/general/useReduxState'

// Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Redux - Actions
import * as ActionOdcReprint from '../../../../../../../../actions/odc-reprint/odc-reprint'

// Notify
import { withSnackbar } from 'notistack'
import * as Notistack from '../../../../../../../../utils/Notistack'

// Components
import Modal from '../../../../../../../../components/Modal'
import ActionButton from '../../../../../../../../components/ActionButton'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalObserve(props) {
    const { classes, open = false, close, data, odcReprint, registerObservationValidated, enqueueSnackbar } = props

    const prevOdcReprint = useRef(odcReprint)

    const [ loading, setLoading ] = useState(false)

    const [ dataRegisterObservation, isLoadingRegisterObservation, isSuccessRegisterObservation, isErrorRegisterObservation ] =
        useReduxState({ props: odcReprint.observationValidatedRegistered,
            prevProps: prevOdcReprint.current.observationValidatedRegistered, notify: enqueueSnackbar })

    useEffect(_ => {
        if (open) setLoading(false)
    }, [open])

    useEffect(_ => {
        if (isLoadingRegisterObservation) setLoading(true)
        if (isSuccessRegisterObservation) {
            Notistack.getNotistack(`Reimpresión: Solicitud ${ data.cod_solicitud_completa } - Aceptar.`, enqueueSnackbar, 'success')
            close()
        }
        if (isErrorRegisterObservation) setLoading(false)
    }, [dataRegisterObservation, isLoadingRegisterObservation, isSuccessRegisterObservation, isErrorRegisterObservation])

    const handleValidateObservation = _ => {
        const sendData = {
            cod_requerimiento: data.cod_requerimiento,
            cod_solicitud: data.cod_solicitud,
            cod_solicitud_completa: data.cod_solicitud_completa,
            actividadGenericoSimple: {
                cod_flujo_fase_estado_actual: 31204,
                cod_flujo_fase_estado_anterior: 31203,
                nom_actividad: 'Rechazar Reimpresión: Aceptar'
            }
        }
        registerObservationValidated(sendData)
    }

    return (
        <Modal
            title='Observaciones del Validación'
            body={
                <TextField
                    fullWidth
                    multiline
                    rows='4'
                    label='Observaciones'
                    type='text'
                    margin='normal'
                    color='default'
                    variant='outlined'
                    value={ data.des_observacion }
                    InputLabelProps={{
                        shrink: true
                    }}
                    InputProps={{
                        readOnly: true
                    }} />
            }
            open={ open }
            actions={
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        text='Cancelar'
                                        loading={ loading }
                                        type='secondary'
                                        icon={ 
                                            <CancelIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        text='Aceptar'
                                        type='primary'
                                        loading={ loading }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        handleAction={ _ => handleValidateObservation() }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
            }
        />
    )
}

function mapStateToProps(state) {
    return {
        odcReprint: state.odcReprintReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerObservationValidated: bindActionCreators(ActionOdcReprint.registerObservationValidated, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalObserve))))