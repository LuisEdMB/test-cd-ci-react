// React
import React, { useState } from 'react'

// Material UI
import { Fab, Tooltip, withStyles } from '@material-ui/core'

// Material - Colors
import { yellow } from '@material-ui/core/colors'

// Material UI - Icons
import VisibilityIcon from '@material-ui/icons/Visibility'

// Components
import ModalObserve from './components/ModalObserve'

const styles = {
    fab: {
        minHeight: 0,
        height: 24,
        width: 24,
        color: 'white',
        transition: '.3s',
        backgroundColor: yellow[700],
        '&:hover': {
            backgroundColor: yellow[800]
        },
        textTransform: 'none'
    },
    icon: {
        minHeight: 0,
        height: 12,
        width: 12
    }
}

function ObserveActionButton(props) {
    const { classes, row, disabled = true } = props

    const [ modalObserve, setModalObserve ]  = useState(false)

    return (
        <div
            key={ row.cod_solicitud }>
                <Tooltip
                    title={ !disabled ? `Solicitud Rechazada: ${ row.cod_solicitud_completa }`: `Solicitud: ${ row.cod_solicitud_completa }`}
                    placement='left'>
                        <div>
                            <Fab
                                className={ classes.fab }
                                onClick={ _ => setModalObserve(true) }
                                disabled={ disabled }>
                                    <VisibilityIcon
                                        className={ classes.icon }
                                        fontSize='small'/>
                            </Fab>
                        </div>
                </Tooltip>
                {
                    modalObserve && <ModalObserve
                        open={ modalObserve }
                        close={ _ => setModalObserve(false) }
                        data={ row } />
                }
        </div>
    )
}

export default React.memo(withStyles(styles)(ObserveActionButton))