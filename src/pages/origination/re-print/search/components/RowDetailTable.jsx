// React
import React from 'react'

// Material UI
import { withStyles, Typography } from '@material-ui/core'

const styles = {
    fontSizeDefault: {
        fontSize: 12
    }
}

const RowDetailTable = ({ row, classes }) => (
    <ul>
        <li
            className='mb-2'>
                <Typography
                    className={ classes.fontSizeDefault }>
                        Cliente:
                </Typography>
                <Typography
                    className={ classes.fontSizeDefault }
                    color='textSecondary'>
                        { row.des_nombre_completo }
                </Typography>
        </li>
        <li
            className='mb-2'>
                <Typography
                    className={ classes.fontSizeDefault }>
                        Usuario:
                </Typography>
                <Typography
                    className={ classes.fontSizeDefault }
                    color='textSecondary'>
                        { row.des_usu_reg }
                </Typography>
        </li>
        <li
            className='mb-2'>
                <Typography
                    className={ classes.fontSizeDefault }>
                        Agencia:
                </Typography>
                <Typography
                    className={ classes.fontSizeDefault }
                    color='textSecondary'>
                        { row.des_agencia }
                </Typography>
        </li>
        <li
            className='mb-2'>
                <Typography
                    className={ classes.fontSizeDefault }>
                        Fecha:
                </Typography>
                <Typography
                    className={ classes.fontSizeDefault }
                    color='textSecondary'>
                        { row.fec_reg }
                </Typography>
        </li>

        <li
            className='mb-2'>
                <Typography
                    className={ classes.fontSizeDefault }>
                        Flujo - Fase - Estado:
                </Typography>
                <Typography
                    className={ classes.fontSizeDefault }
                    color='textSecondary'>
                        { row.des_jerarquias_flujo_fase_estado }
                </Typography>
        </li>
    </ul>
);

export default React.memo(withStyles(styles)(RowDetailTable))