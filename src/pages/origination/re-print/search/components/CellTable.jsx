// React
import React from 'react'

// Material UI
import { Table } from '@devexpress/dx-react-grid-material-ui'

// Utils
import * as moment from 'moment'

// Components
import NewCommentForm from '../../../../../components/NewCommentForm/NewCommentForm'
import AttachedActionButton from './actions/attached-button/AttachedActionButton'

function CellTable(props) {
    const { row } = props
    let disabled = true
    const rowFPS = row.cod_flujo_fase_estado || 0
    const attached = row.des_archivo_adjunto_reimpresion ? true : false

    if (props.column.name === 'fec_reg') {
        const value = props.value? moment(props.value.replace('T', ' ')).format('DD/MM/YYYY HH:mm') : ''
        return <Table.Cell {...props} value={value} />
    }
    else if (props.column.name === 'attached') {
        if(rowFPS >= 30302 && attached){
            disabled = false
        }
        return <Table.Cell {...props}>
            <AttachedActionButton {...props}  disabled={disabled}/>
        </Table.Cell>
    }
    if (props.column.name === 'observation') {
        return <Table.Cell {...props}>
           <NewCommentForm {...props}/>
        </Table.Cell>
    }
    return <Table.Cell {...props} />
}

export default React.memo(CellTable)