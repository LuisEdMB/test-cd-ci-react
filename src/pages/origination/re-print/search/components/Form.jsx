import React, { Component } from 'react';
import * as moment from 'moment';
import { withSnackbar } from 'notistack';
import {
    TextField,
    Grid,
    Button,
    withStyles,
    InputAdornment
} from '@material-ui/core';
// React Router 
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import Autocomplete from '../../../../../components/Autocomplete';
// Actions
import { getAgency } from '../../../../../actions/generic/agency';
// Icons 
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import SearchIcon from '@material-ui/icons/Search';
// Utils
import { 
    defaultMinDate, 
    defaultLengthFullNumber,
    validateLengthDocumentType, 
    validateLengthFullNumber, 
    onlyNumberKeyCode, 
    defaultFinalDate,
    defaultInitialDate
} from '../../../../../utils/Utils';
import { Bounce } from 'react-reveal';

const mapStateToProps = (state, props) => {
    return {
        agency: state.agencyReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getAgency:  bindActionCreators(getAgency, dispatch)
    };
    return actions;
}

const styles = theme => ({
    button: {
        textTransform: 'none',
    }
});

class Form extends Component {
    state = {
        fullNumberError: false,
        fullNumber: "",

        documentNumberError: false,
        documentTypeId: 100001,
        documentNumber: "",
        agencyError: false,
        
        agencyId: "",
        agency: "",
        agencyMessage: "",

        initialRegistrationDateError: false,
        initialRegistrationDate: "", 
        initialRegistrationDateMessage: "",

        finalRegistrationDateError: false,
        finalRegistrationDate: "", 
        finalRegistrationDateMessage: ""
    }
    handleSubmit = (e) => {
        e.preventDefault();

        let { 
            fullNumber,
            documentNumber,
            agencyId,
            initialRegistrationDate,
            finalRegistrationDate,
        } = this.state;
              
        const sendData = {
            fullNumber: fullNumber? fullNumber: "",
            documentNumber: documentNumber? documentNumber: "",
            agencyId: agencyId? agencyId: "",
            initialRegistrationDate: initialRegistrationDate? initialRegistrationDate: "",
            finalRegistrationDate: finalRegistrationDate? finalRegistrationDate: "",
        }
        this.props.handleSubmit(sendData);
    }

    componentWillMount = () => {
        this.setState(state => ({
            ...state, 
            initialRegistrationDate: defaultInitialDate, 
            finalRegistrationDate: defaultFinalDate
        }));
    }
    componentDidMount = () => {
        this.props.getAgency();
    }

    // Only Number
    handleKeyPressTextFieldOnlyNumber = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!onlyNumberKeyCode(code)){
            e.preventDefault();
        }   
    }

    // Document Number - TextField Event Change
    handleChangeTextFieldDocumentNumber = name => e =>{
        let { value } = e.target;
        let nameError = `${name}Error`;

        this.setState(state => ({
            ...state,
            [name]:value,
            [nameError]: !validateLengthDocumentType(value.length)
        }));        

    }

    // Document Number - TextField Event Change
    handleChangeTextFieldFullNumber = name => e =>{
        let { value } = e.target;
        let nameError = `${name}Error`;
        this.setState(state => ({
            ...state,
            [name]:value,
            [nameError]: !validateLengthFullNumber(value.length)
        }));        
    }

    // Client - Select - Event Change
    handleChangeSelectRequired = (name) => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";

        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }

        this.setState(state => ({
            ...state, 
            [name]: valueDefault, 
            [nameId]: idDefault,
            [nameError]: error,
        }));
    }

    // Client - TextField - Event Change - Required    
    handleChangeTextFieldRequired = (name) => e => {
        e.persist();
        let { value } = e.target;
        let nameError = `${name}Error`;
        this.setState(state => ({
            [name]: value,
            [nameError]: value !== "" ? false : true
        }));
    }

    render = () => {
        const { classes, agency } = this.props;
        const { 
            fullNumber,
            documentTypeId,
            documentNumber,
            finalRegistrationDateError,
            finalRegistrationDate, 
            initialRegistrationDateError,
            initialRegistrationDate, 
        } = this.state;
        const agencyData = agency.data.map((item, index) => ({
            value: item.cod_agencia, 
            label: item.des_agencia
        }))
        return(<form autoComplete="off" onSubmit={this.handleSubmit}>
                    <Grid container spacing={8}>
                        {/* Document Number */}
                        <Grid item xs={6} sm={6} md={4} lg={2}>
                            <TextField
                                //error={documentNumberError}
                                fullWidth
                                id="documentNumber"
                                label="Nro. Documento"
                                name="documentNumber"
                                margin="normal"
                                value={documentNumber}
                                onKeyPress={this.handleKeyPressTextFieldOnlyNumber("documentNumber")}
                                onChange={this.handleChangeTextFieldDocumentNumber("documentNumber")}
                                placeholder="Ingresar nro. documento."
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                InputProps={{
                                    inputProps:{
                                        maxLength: documentTypeId === 100001 ? 8:9,
                                    }
                                }}
                            />
                        </Grid>
                        {/* Final Date */}
                        <Grid item xs={6} sm={6} md={4} lg={2}>
                            <TextField
                                //error={fullNumberError}
                                fullWidth
                                id="fullNumber"
                                label="Solicitud"
                                name="fullNumber"
                                margin="normal"
                                value={fullNumber}
                                onKeyPress={this.handleKeyPressTextFieldOnlyNumber("documentNumber")}
                                onChange={this.handleChangeTextFieldFullNumber("fullNumber")}
                                placeholder="Ingresar solicitud."
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                InputProps={{
                                    inputProps:{
                                        maxLength: defaultLengthFullNumber,
                                    }
                                }}
                            />
                        </Grid>
                        {/* Initial Date */}
                        <Grid item xs={6} sm={6} md={4} lg={2}>
                            <TextField
                                error={initialRegistrationDateError}
                                fullWidth
                                id="initialRegistrationDate"
                                label="Fecha Inicio"
                                type="date"
                                name="initialRegistrationDate"
                                defaultValue={initialRegistrationDate}
                                onChange={this.handleChangeTextFieldRequired("initialRegistrationDate")}
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                InputProps={{
                                    inputProps:{
                                        min: defaultMinDate,
                                        max: moment().format('YYYY-MM-DD')
                                    },
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <CalendarTodayIcon color={initialRegistrationDateError? "secondary": "inherit"} fontSize="small" />
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                        {/* Final Date */}
                        <Grid item xs={6} sm={6} md={4} lg={2}>
                            <TextField
                                error={finalRegistrationDateError}
                                fullWidth
                                id="finalRegistrationDate"
                                label="Fecha Final"
                                type="date"
                                name="finalRegistrationDate"
                                defaultValue={finalRegistrationDate}
                                onChange={this.handleChangeTextFieldRequired("finalRegistrationDate")}
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                InputProps={{
                                    inputProps:{
                                        min: defaultMinDate,
                                        max: moment().format('YYYY-MM-DD')
                                    },
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <CalendarTodayIcon color={finalRegistrationDateError? "secondary": "inherit"} fontSize="small" />
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                        {/* Agency */}
                        <Grid item xs={12} sm={6} md={4} lg={3}>
                            <Autocomplete                                            
                                //error={this.state.agencyError}
                                onChange={this.handleChangeSelectRequired("agency")}
                                value={this.state.agencyId}
                                data={agencyData}
                                placeholder={"Agencias"}
                            />
                        </Grid>
                        {/* Search */}
                        <Grid item xs={12} sm={12} md={4} lg={1} className="d-flex justify-content-center align-items-center">
                            <div className="w-100">
                                <Bounce> 
                                    <div>
                                        <Button className={classes.button} fullWidth variant="contained" type="submit"  color="primary">
                                            Buscar
                                            <SearchIcon  className="ml-2" size="small" />
                                        </Button>
                                    </div>
                                </Bounce>
                            </div>
                        </Grid>
                    </Grid>
                </form>
        )
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(Form)));
