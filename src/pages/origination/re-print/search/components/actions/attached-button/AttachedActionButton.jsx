// React
import React, { useState } from 'react'

// Material UI
import { Fab, Tooltip, withStyles } from '@material-ui/core'

// Material UI - Icons
import CloudDownloadIcon from '@material-ui/icons/CloudDownload'

// Material UI - Colors
import { blue } from '@material-ui/core/colors'
import { withSnackbar } from 'notistack'

// Components
import ModalDownloadAttach from './components/ModalDownloadAttach'

const styles = {
    fab: {
        minHeight: 0,
        height: 24,
        width: 24,
        color: "white",
        transition: ".3s",
        backgroundColor: blue[700],
        '&:hover': {
            backgroundColor: blue[900]
        },
        textTransform: 'none'
    },
    icon: {
        minHeight: 0,
        height: 12,
        width: 12
    }
}

function AttachedActionButton(props) {
    const { row, disabled, classes } = props

    const [ modalDownloadAttach, setModalDownloadAttach ] = useState(false)

    return (
        <div
            key={ row.cod_solicitud }>
                <Tooltip
                    title={ !disabled ? `Ver Adjunto! Nro. Solicitud: ${ row.cod_solicitud_completa }` : `Solicitud sin documento adjunto: ${ row.cod_solicitud_completa }` }
                    placement='left'>
                    <div>
                        <Fab
                            className={ classes.fab }
                            onClick={ _ => setModalDownloadAttach(true) }
                            disabled={ disabled }>
                                <CloudDownloadIcon
                                    className={ classes.icon }
                                    fontSize='small'/>
                        </Fab>
                    </div>
                </Tooltip>
                {
                    modalDownloadAttach && <ModalDownloadAttach
                        open={ modalDownloadAttach }
                        close={ _ => setModalDownloadAttach(false) }
                        data={ row } />
                }
        </div>
    )
}

export default React.memo(withStyles(styles)(withSnackbar(AttachedActionButton)))