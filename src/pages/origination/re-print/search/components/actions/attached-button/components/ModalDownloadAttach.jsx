// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Grid, IconButton, LinearProgress, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import CancelIcon from '@material-ui/icons/Cancel'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload'

// Hooks
import useReduxState from '../../../../../../../../hooks/general/useReduxState'

// Redux
import { connect } from 'react-redux'

// Redux - Actions
import * as ActionOdcDocument from '../../../../../../../../actions/odc-document/odc-document'

// Notify
import { withSnackbar } from 'notistack'

// Components
import Modal from '../../../../../../../../components/Modal'
import ActionButton from '../../../../../../../../components/ActionButton'
import { bindActionCreators } from 'redux'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    }
}

function ModalDownloadAttach(props) {
    const { classes, open = false, close, data, odcDocument, downloadAttach, enqueueSnackbar } = props

    const prevOdcDocument = useRef(odcDocument)

    const [ loading, setLoading ] = useState(false)

    const [ dataDownloadDocument, isLoadingDownloadDocument, isSuccessDownloadDocument, isErrorDownloadDocument ] =
        useReduxState({ props: odcDocument.downloadReprintAttachedDocument,
            prevProps: prevOdcDocument.current.downloadReprintAttachedDocument, notify: enqueueSnackbar })

    useEffect(_ => {
        if (open) setLoading(false)
    }, [open])

    useEffect(_ => {
        if (isLoadingDownloadDocument) setLoading(true)
        if (isSuccessDownloadDocument) {
            const url = window.URL.createObjectURL(new Blob([data]))
            let link = document.createElement('a')
            link.href = url
            link.setAttribute('download', `${ dataDownloadDocument?.des_archivo_adjunto_reimpresion || '' }`)
            link.click()
            setLoading(false)
        }
        if (isErrorDownloadDocument) setLoading(false)
    }, [dataDownloadDocument, isLoadingDownloadDocument, isSuccessDownloadDocument, isErrorDownloadDocument])

    const handleDownloadAttach = _ => {
        downloadAttach({ cod_solicitud_completa: data.cod_solicitud_completa })
    }

    return (
        <Modal
            title='Documento Adjunto'
            body={
                <>
                    <Grid
                        item
                        xs={ 12 }>
                            <Typography
                                className='d-flex justify-content-between align-items-center'>
                                    <Typography
                                        component='span'
                                        color='inherit'>
                                            { data.des_archivo_adjunto_reimpresion }
                                    </Typography>
                                    <IconButton
                                        disabled={ loading }
                                        color='secondary'
                                        size='small'
                                        className='ml-2'>
                                            <CloudDownloadIcon
                                                onClick={ _ => handleDownloadAttach() }/>
                                    </IconButton>
                            </Typography>
                    </Grid>
                    <Grid
                        item
                        xs={ 12 }>
                            {
                                loading && <LinearProgress />
                            }
                    </Grid>
                </>
            }
            open={ open }
            actions={
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 12 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        text='Cancelar'
                                        type='secondary'
                                        loading={ loading }
                                        icon={ 
                                            <CancelIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                </Grid>
            }
        />
    )
}

function mapStateToProps(state) {
    return {
        odcDocument: state.odcDocumentReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        downloadAttach: bindActionCreators(ActionOdcDocument.downloadReprintAttachedDocument, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalDownloadAttach))))