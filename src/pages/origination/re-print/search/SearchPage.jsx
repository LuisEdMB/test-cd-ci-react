// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { Divider, Grid, LinearProgress, Paper, Typography, withWidth } from '@material-ui/core'

// Hooks
import useReduxState from '../../../../hooks/general/useReduxState'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcReprint from '../../../../actions/odc-reprint/odc-reprint'

// Effects
import Fade from 'react-reveal/Fade'

// Notify
import { withSnackbar } from 'notistack'

// Components
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent'
import Form from './components/Form'
import RowDetailTable from './components/RowDetailTable'
import CellTable from './components/CellTable'

const columnsConfig = {
    xs: [
        { name: 'cod_solicitud_completa', title: 'Solicitud' },
        { name: 'attached', title: 'Ver Adjunto' }
    ],
    sm: [
        { name: 'cod_solicitud_completa', title: 'Solicitud' },
        { name: 'des_nro_documento', title: 'Nro Documento' },
        { name: 'attached', title: 'Ver Adjunto' }
    ],
    md: [
        { name: 'cod_solicitud_completa', title: 'Solicitud' },
        { name: 'des_tipo_documento', title: 'Nro Documento' },
        { name: 'des_nro_documento', title: 'Nro Documento' },
        { name: 'des_nombre_completo', title: 'Cliente' },
        { name: 'des_motivo_reimpresion', title: 'Motivo' },
        { name: 'des_agencia', title: 'Agencia' },
        { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
        { name: 'fec_reg', title: 'Fecha Registro' },
        { name: 'attached', title: 'Ver Adjunto' },
        { name: 'observation', title: 'Ver Comentario' }
    ],
    lg: [
        { name: 'cod_solicitud_completa', title: 'Solicitud' },
        { name: 'des_tipo_documento', title: 'Tipo Documento' },
        { name: 'des_nro_documento', title: 'Nro Documento' },
        { name: 'des_nombre_completo', title: 'Cliente' },
        { name: 'des_motivo_reimpresion', title: 'Motivo' },
        { name: 'des_agencia', title: 'Agencia' },
        { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
        { name: 'fec_reg', title: 'Fecha Registro' },
        { name: 'attached', title: 'Ver Adjunto' },
        { name: 'observation', title: 'Ver Comentario' }
    ],
    xl: [
        { name: 'cod_solicitud_completa', title: 'Solicitud' },
        { name: 'des_tipo_documento', title: 'Tipo Documento' },
        { name: 'des_nro_documento', title: 'Nro Documento' },
        { name: 'des_nombre_completo', title: 'Cliente' },
        { name: 'des_motivo_reimpresion', title: 'Motivo' },
        { name: 'des_agencia', title: 'Agencia' },
        { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
        { name: 'fec_reg', title: 'Fecha Registro' },
        { name: 'attached', title: 'Ver Adjunto' },
        { name: 'observation', title: 'Ver Comentario' }
    ],
    default: [
        { columnName: 'cod_solicitud_completa', width: 125 },
        { columnName: 'des_jerarquias_flujo_fase_estado', width: 300 },
        { columnName: 'des_tipo_documento', width: 120 },
        { columnName: 'des_nro_documento', width: 125 }, 
        { columnName: 'des_nombre_completo', width: 150 },
        { columnName: 'des_motivo_reimpresion', width: 150 },
        { columnName: 'des_agencia', width: 120 },    
        { columnName: 'des_usu_reg', width: 150 },
        { columnName: 'fec_reg', width: 150 },
        { columnName: 'attached', width: 120 },
        { columnName: 'observation', width: 150 }
    ],
    extensions: [
        { columnName: 'attached', align: 'center' },
        { columnName: 'observation', align: 'center' }
    ]
}

function SearchPage(props) {
    const { odcReprint, width, getReprintsForSearching, enqueueSnackbar } = props

    const prevWidth = useRef(width)
    const prevOdcReprint = useRef(odcReprint)

    const [ columns, setColumns ] = useState([])
    const [ rows, setRows ] = useState([])

    const [ dataReprints, isLoadingReprints, isSuccessReprints, isErrorReprints ] =
        useReduxState({ props: odcReprint.reprintsForSearching, prevProps: prevOdcReprint.current.reprintsForSearching, notify: enqueueSnackbar })

    useEffect(_ => {
        setColumns(columnsConfig[width])
    }, [])

    useEffect(_ => {
        if (prevWidth.current !== width) setColumns(columnsConfig[width])
    }, [width])

    useEffect(_ => {
        if (isLoadingReprints || isErrorReprints) setRows([])
        if (isSuccessReprints) setRows(dataReprints?.solicitudesReimpresionesConsulta || [])
    }, [dataReprints, isLoadingReprints, isSuccessReprints, isErrorReprints])

    const handleSearchReprints = data => {
        const sendData = {
            cod_solicitud_completa: data.fullNumber,
            des_nro_documento: data.documentNumber,
            cod_agencia: data.agencyId,
            fec_inicial: data.initialRegistrationDate,
            fec_final: data.finalRegistrationDate
        }
        getReprintsForSearching(sendData)
    }

    return (
        <Grid
            container
            className='p-1'>
                <Grid
                    item
                    xs={ 12 }>
                    {
                        isLoadingReprints && <LinearProgress />
                    }
                </Grid>
                <Grid
                    item
                    xs={ 12 }
                    className='mb-2'>                       
                        <Paper
                            className='py-4'>
                                <div
                                    className='d-flex justify-content-between align-items-center mb-2'>
                                        <Fade>
                                            <Typography
                                                variant='h5'
                                                color='textSecondary'>
                                                    Bandeja de Búsqueda de Reimpresione(s)
                                            </Typography>
                                        </Fade>
                                </div>
                                <Divider
                                    className='mb-2'/>  
                                <Form
                                    handleSubmit={ data => handleSearchReprints(data) } />
                        </Paper>
                </Grid>
                <Grid
                    item
                    xs={ 12 }>
                        <DevGridComponent
                            rows={ rows }
                            columns={ columns }
                            width={ width }
                            columnExtensions={ columnsConfig.extensions }
                            defaultColumnWidths={ columnsConfig.default }
                            RowDetailComponent={ RowDetailTable }
                            CellComponent={ CellTable }/>
                </Grid>
        </Grid>
    )
}

function mapStateToProps(state) {
    return {
        odcReprint: state.odcReprintReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getReprintsForSearching: bindActionCreators(ActionOdcReprint.getReprintsForSearching, dispatch)
    }
}

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(SearchPage)))