//React
import React from 'react'

// Material UI
import { Paper, Typography, withStyles } from '@material-ui/core'

// Media
import CencosudScotiaBank from '../../../../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg';

const styles = theme =>({
    avatar: {
        [theme.breakpoints.down('md')]: {
            width: 68,
            height: 36
        },
        width: 80,
        height: 48
    },
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title: {
        [theme.breakpoints.down('md')]: {
            fontSize:14
        }
    }
})

const Header = ({ title='', classes }) => (
    <Paper
        className='py-1'
        color='primary'>
            <figure
                className={ classes.root }>
                    <img
                        className={ classes.avatar }
                        src={ CencosudScotiaBank }
                        alt='Cencosud ScotiaBank' />
                <figcaption
                    className='w-100'>
                        <Typography
                            align='center'
                            variant='h5'
                            component='h4'
                            className={ classes.title }>
                                {title}
                        </Typography>
                </figcaption>
            </figure>
    </Paper>
)

export default React.memo(withStyles(styles)(Header))