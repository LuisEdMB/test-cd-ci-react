// React
import React, { useEffect, useRef, useState } from 'react'

// Material UI
import { FormHelperText, Grid, TextField, Typography, withStyles } from '@material-ui/core'

// Material UI - Icons
import CancelIcon from '@material-ui/icons/Cancel'
import SendIcon from '@material-ui/icons/Send'

// Hooks
import useReduxState from '../../../../../../../../hooks/general/useReduxState'
import useDecryptCardNumber from '../../../../../../../../hooks/pci/useDecryptCardNumber'
import useBlockTypeCreditCard from '../../../../../../../../hooks/pmp/useBlockTypeCreditCard'
import useBlockTypeCreditCardMaster from '../../../../../../../../hooks/master/useBlockTypeCreditCard'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Redux - Actions
import * as ActionOdcReprint from '../../../../../../../../actions/odc-reprint/odc-reprint'
import * as ActionPmp from '../../../../../../../../actions/pmp/pmp'
import * as ActionPci from '../../../../../../../../actions/pci/pci'
import * as ActionOdcMaster from '../../../../../../../../actions/odc-master/odc-master'

// Notify
import { withSnackbar } from 'notistack'

// Effects
import Fade from 'react-reveal/Fade'

// Utils
import * as Utils from '../../../../../../../../utils/Utils'
import * as Validations from '../../../../../../../../utils/Validations'
import * as Constants from '../../../../../../../../utils/Constants'
import * as OriginationUtils from '../../../../../../../../utils/origination/Origination'
import * as MasterOrigination from '../../../../../../../../utils/origination/MasterOrigination'

// Components
import Modal from '../../../../../../../../components/Modal'
import ActionButton from '../../../../../../../../components/ActionButton'

const styles = {
    buttonProgressWrapper: {
        position: 'relative'
    },
    labelError: {
        display: 'flex',
        justifyContent: 'space-between', 
        alignItems: 'center'
    }
}

function ModalRejectAuthorize(props) {
    const { classes, open = false, close, data, odcReprint, rejectActivationProcess, enqueueSnackbar,
        pci, decryptCardNumber, pmp, blockTypeCreditCard, odcMaster, updateBlockingCodeCreditCardIntoMaster,
        closeModalParent } = props

    const prevOdcReprint = useRef(odcReprint)
    const prevPmp = useRef(pmp)
    const prevPci = useRef(pci)
    const prevOdcMaster = useRef(odcMaster)
    const reintentProcess = useRef(0)

    const [ loading, setLoading ] = useState(false)
    const [ reject, setReject ] = useState({
        rejectActivationComment: ''
    })
    const [ validationReject, setValidationReject ] = useState({
        rejectActivationComment: {
            error: false,
            message: '',
            ref: useRef(null)
        }
    })

    const [ dataReject, isLoadingReject, isSuccessReject, isErrorReject ] =
        useReduxState({ props: odcReprint.activationProcessRejected, prevProps: prevOdcReprint.current.activationProcessRejected })
    const [ dataDecryptCardNumber, isLoadingDecryptCardNumber, isSuccessDecryptCardNumber, isErrorDecryptCardNumber ] =
        useDecryptCardNumber({ props: pci.cardnumberDecrypted, prevProps: prevPci.current.cardnumberDecrypted, notify: enqueueSnackbar,
            options: { flowName: 'Reimpresión' } })
    const [ isLoadingBlockTypeCreditCard, isSuccessBlockTypeCreditCard, isErrorBlockTypeCreditCard ] =
        useBlockTypeCreditCard({ props: pmp.blockTypeCreditCard, prevProps: prevPmp.current.blockTypeCreditCard, notify: enqueueSnackbar, 
            options: { typeBlockName: 'Cancelar', flowName: 'Reimpresión' } })
    const [ isLoadingBlockTypeCreditCardMaster, isSuccessBlockTypeCreditCardMaster, isErrorBlockTypeCreditCardMaster ] =
        useBlockTypeCreditCardMaster({ props: odcMaster.blockingCodeCreditCardUpdated,
            prevProps: prevOdcMaster.current.blockingCodeCreditCardUpdated, notify: enqueueSnackbar,
            options: { typeBlockName: 'Cancelar', flowName: 'Reimpresión' } })

    useEffect(_ => {
        if (open) {
            setLoading(false)
            setReject({ rejectActivationComment: '' })
            reintentProcess.current = 0
        }
    }, [open])

    useEffect(_ => {
        if (isLoadingReject) setLoading(true)
        if (isSuccessReject) {
            reintentProcess.current = 1 + Number(!data.token)
            handleProcessReject()
        }
        if (isErrorReject) setLoading(false)
    }, [dataReject, isLoadingReject, isSuccessReject, isErrorReject])

    useEffect(_ => {
        if (isLoadingDecryptCardNumber) setLoading(true)
        if (isSuccessDecryptCardNumber) {
            reintentProcess.current = 2
            handleProcessReject()
        }
        if (isErrorDecryptCardNumber) setLoading(false)
    }, [dataDecryptCardNumber, isLoadingDecryptCardNumber, isSuccessDecryptCardNumber, isErrorDecryptCardNumber])

    useEffect(_ => {
        if (isLoadingBlockTypeCreditCard) setLoading(true)
        if (isSuccessBlockTypeCreditCard) {
            reintentProcess.current = 3
            handleProcessReject()
        }
        if (isErrorBlockTypeCreditCard) setLoading(false)
    }, [isLoadingBlockTypeCreditCard, isSuccessBlockTypeCreditCard, isErrorBlockTypeCreditCard])

    useEffect(_ => {
        if (isLoadingBlockTypeCreditCardMaster) setLoading(true)
        if (isSuccessBlockTypeCreditCardMaster) {
            close()
            closeModalParent()
        }
        if (isErrorBlockTypeCreditCardMaster) setLoading(false)
    }, [isLoadingBlockTypeCreditCardMaster, isSuccessBlockTypeCreditCardMaster, isErrorBlockTypeCreditCardMaster])

    const handleChangeState = e => {
        const { value, call = _ => null } = e.target || e
        setReject({
            rejectActivationComment: value
        })
        setValidationReject(state => {
            let validations = { ...state }
            if (Utils.findProperty(validations, 'rejectActivationComment', ['ref']).length > 0) {
                validations = {
                    rejectActivationComment: {
                        ...Validations.validateByField('rejectActivationComment', value)
                    }
                }
            }
            call(validations)
            return validations
        })
    }

    const handleReject = _ => {
        Validations.comprobeAllValidationsSuccess(reject, handleChangeState).then(valid => {
            if (valid) handleProcessReject()
        })
    }

    const handleProcessReject = _ => {
        const steps = getSteps()
        steps[reintentProcess.current].action()
    }

    const getSteps = _ => ({
        0: {
            action: _ => {
                const sendData = {
                    cod_solicitud: data.cod_solicitud,
                    cod_solicitud_completa: data.cod_solicitud_completa,
                    nom_actividad: 'Rechazar Reimpresión: Rechazar',
                    cod_flujo_fase_estado_actual: 31201,
                    cod_flujo_fase_estado_anterior: 30903,
                    des_observacion: reject.rejectActivationComment
                }
                rejectActivationProcess(sendData)
            }
        },
        1: {
            action: _ => {
                const sendData = {
                    solicitudeCode: data.cod_solicitud,
                    completeSolicitudeCode: data.cod_solicitud_completa,
                    token: data.token
                }
                decryptCardNumber(sendData)
            }
        },
        2: {
            action: _ => {
                const sendData = OriginationUtils.setDataBlockTypeCreditCardPMP({
                    organization: Constants.General.organization,
                    cod_flujo_fase_estado_actual: 31203,
                    cod_flujo_fase_estado_anterior: 31201,
                    nom_actividad: 'Rechazar Reimpresión: PMP Cancelar TC',
                    blockCode: Constants.BlockCodePmpTypes.blockTypeCreditCardCanceledByEnterprise,
                    process: {
                        fullNumber: data.cod_solicitud_completa,
                        number: data.cod_solicitud
                    },
                    client: {
                        id: data.cod_cliente_titular || 0,
                        documentTypeAux: data.cod_tipo_documento || '',
                        documentNumber: data.des_nro_documento || '',
                        documentTypeInternalValue: data.des_tipo_documento_valor_interno || ''
                    },
                    pmp: {
                        cardNumber: dataDecryptCardNumber?.nro_tarjeta || data.num_tarjeta_pmp
                    }
                })
                blockTypeCreditCard(sendData)
            }
        },
        3: {
            action: _ => {
                const sendData = {
                    solicitudeCode: data.cod_solicitud,
                    completeSolicitudeCode: data.cod_solicitud_completa,
                    accountNumber: data.num_cuenta_pmp,
                    token: data.token,
                    blockingCode: Constants.BlockCodePmpTypes.blockTypeCreditCardCanceledByEnterprise,
                    activityName: MasterOrigination.updateBlockingCodeCreditCardActivity.activityName,
                    phaseCode: MasterOrigination.updateBlockingCodeCreditCardActivity.phaseCode,
                    masterStageCode: Constants.MasterConfigurationStage.masterTrayStage
                }
                updateBlockingCodeCreditCardIntoMaster(sendData)
            }
        }
    })

    return (
        <Modal
            title='Observaciones de Rechazo'
            body={
                <>
                    <Grid
                        item
                        xs={ 12 }>
                            <TextField
                                error={ validationReject.rejectActivationComment.error }
                                autoFocus
                                fullWidth
                                multiline
                                rows='4'
                                label='Observaciones'
                                type='text'
                                margin='normal'
                                color='default'
                                name='rejectActivationComment'
                                variant='outlined'
                                value={ reject.rejectActivationComment }
                                InputLabelProps={{
                                    shrink: true
                                }}
                                InputProps={{
                                    inputProps:{
                                        maxLength: Utils.defaultFullMaxLengthInput
                                    }
                                }}
                                onChange={ e => {
                                    if (Utils.onlyTextRegex(e.target.value))
                                        handleChangeState(e)
                                } }
                            />
                            <Fade>
                                <FormHelperText
                                    className={ classes.labelError }>
                                        <Typography
                                            component='span'
                                            variant='inherit'
                                            color={ validationReject.rejectActivationComment.error 
                                                ? 'secondary' : 'default' }>
                                                    {
                                                        validationReject.rejectActivationComment.error &&
                                                        validationReject.rejectActivationComment.message
                                                    }
                                        </Typography>
                                        <Typography
                                            component='span'
                                            variant='inherit'
                                            color={ validationReject.rejectActivationComment.error
                                                ? 'secondary' : 'default' }>
                                                { `${ reject.rejectActivationComment.length }/${ Utils.defaultFullMaxLengthInput }` }
                                        </Typography>
                                </FormHelperText>
                            </Fade>
                    </Grid>
                </>
            }
            open={ open }
            actions={
                <Grid
                    container
                    spacing={ 8 }>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton
                                        loading={ loading }
                                        text='Cancelar'
                                        type='secondary'
                                        icon={ 
                                            <CancelIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        handleAction={ _ => close() }/>
                                </div>
                        </Grid>
                        <Grid
                            item
                            xs={ 6 }>
                                <div className={ classes.buttonProgressWrapper }>
                                    <ActionButton 
                                        loading={ loading }
                                        text='Aceptar'
                                        type='primary'
                                        handleAction={ _ => handleReject() }
                                        icon={ 
                                            <SendIcon 
                                                fontSize='small' 
                                                className='ml-2'/>
                                        }
                                        showLoading/>
                                </div>
                        </Grid>
                </Grid>
            }
        />
    )
}

function mapStateToProps(state) {
    return {
        odcReprint: state.odcReprintReducer,
        pmp: state.pmpReducer,
        pci: state.pciReducer,
        odcMaster: state.odcMasterReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        rejectActivationProcess: bindActionCreators(ActionOdcReprint.rejectActivationProcess, dispatch),
        decryptCardNumber: bindActionCreators(ActionPci.decryptCardnumber, dispatch),
        blockTypeCreditCard: bindActionCreators(ActionPmp.blockTypeCreditCard, dispatch),
        updateBlockingCodeCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.updateBlockingCodeCreditCard, dispatch)
    }
}

export default React.memo(withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ModalRejectAuthorize))))