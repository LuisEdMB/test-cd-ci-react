// React
import React from 'react'

// Material UI
import { Table } from '@devexpress/dx-react-grid-material-ui'

// Utils
import * as moment from 'moment'

// Components
import AuthorizeActionButton from './actions/authorize-button/AuthorizeActionButton'
import NewCommentForm from '../../../../../components/NewCommentForm/NewCommentForm'

const CellTable = ({...props}) => {
    if (props.column.name === 'fec_reg') {
        const value = props.value? moment(props.value.replace('T', ' ')).format('DD/MM/YYYY HH:mm') : ''
        return <Table.Cell {...props} value={value} />
    }
    else if(props.column.name === 'authorize') {
        return <Table.Cell {...props}>
            <AuthorizeActionButton {...props} />
        </Table.Cell>
    }
    if(props.column.name === 'observation') {
        return <Table.Cell {...props}>
           <NewCommentForm {...props}/>
        </Table.Cell>
    }
    return <Table.Cell {...props} />
}

export default React.memo(CellTable)