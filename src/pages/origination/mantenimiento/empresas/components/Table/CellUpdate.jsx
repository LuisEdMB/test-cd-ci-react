import React from "react";
import * as moment from 'moment';
import { Table } from '@devexpress/dx-react-grid-material-ui';
import UpdateModalButton from '../modal/UpdateModalButton'
import DeleteButton from "../DeleteButton";

const MyField = (props) => (
    <span style={{color: 'white', padding: '.2em .5em', backgroundColor: props.value ? 'green' : 'red', borderRadius: '.2em'}}>
        {props.value ? 'Activo' : 'Inactivo'}
    </span>
)

const  Cell = ({...props}) => {
    if(props.column.name === "fec_reg"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    else if(props.column.name === "update"){
        return <Table.Cell {...props}>
            <UpdateModalButton {...props} />
        </Table.Cell>
    }
    else if(props.column.name === "delete"){
        return <Table.Cell {...props}>
            <DeleteButton  {...props} />
        </Table.Cell>
    }
    else if(props.column.name === "fec_act"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    else if(props.column.name === "flg_visible") {
        return <Table.Cell {...props} value={<MyField value={props.value}/>} />
    }

    return <Table.Cell {...props} />
}

export default Cell;
