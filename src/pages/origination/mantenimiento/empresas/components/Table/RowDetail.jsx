import React from "react";
import { withStyles, Typography } from "@material-ui/core";

const styles = theme => ({
    fontSizeDefault: {
        fontSize:12
    },
});

const RowDetail = ({ row, classes }) => (
    <ul>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                RUC:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.ruc_emp }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Nombre:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.nom_emp }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Usuario Actualizador:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.des_usu_act }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha de Actualizacion:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.fec_act }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha de registro:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.fec_reg }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Usuario Creador:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.des_usu_reg }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Observaciones:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.des_observaciones }
            </Typography>
        </li>
    </ul>
);

export default withStyles(styles)(RowDetail);

