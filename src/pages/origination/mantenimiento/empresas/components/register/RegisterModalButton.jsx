import React, { Component } from "react";
import { 
    AppBar,
    Tooltip, 
    withStyles, 
    Dialog, 
    DialogTitle,
    Typography,
    Grid,
    Button,
    DialogActions,
    DialogContent,
    TextField,
    IconButton,
    Switch,
    FormControlLabel
 } from "@material-ui/core";
import * as moment from 'moment';
import { onlyNumberKeyCode } from '../../../../../../utils/Utils' 
// Icons
import SendIcon from '@material-ui/icons/Send';
import BusinessIcon from '@material-ui/icons/Business';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close';

import { createBusiness } from '../../../../../../actions/odc-express/odc'
import { withSnackbar } from 'notistack'
// React Router 
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {removeSpecialCharacters} from '../../../../../../utils/Utils'
const styles = theme => ({
    modalRoot:{
        minWidth:280, 
        width:450,
        maxWidth:680,
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    button:{
        textTransform: "none"
    },
    appBar: {
        position: 'relative',
    }
});

const mapStateToProps = (state) => {
    return {
        odc: state.odcReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const actions = {
        createBusiness: bindActionCreators(createBusiness, dispatch)
    }
    return actions
}


class RegisterModalButton extends Component{
    state = {
        business: {
            rucNumber: "",
            businessName: "",
            description: "",
            visible: true
        },
        error: {
            rucNumber: false,
            businessName: false,
        },
        openModal: false
    }

    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }

    // Las modificaciones en este ciclo de vida no causan actualizaciones en el componente, 

    componentWillMount = () => {
        let  initialRegistrationDate = moment().subtract(7, "day").format('YYYY-MM-DD');
        let  finalRegistrationDate = moment().format('YYYY-MM-DD');

        this.setState(state => ({
            ...state, 
            initialRegistrationDate: initialRegistrationDate, 
            finalRegistrationDate: finalRegistrationDate
        }));
    }

    handleKeyPressTextFieldOnlyNumber = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!onlyNumberKeyCode(code)){
            e.preventDefault();
        }   
    };

    handleChange = (e) => {
        e.preventDefault();
        this.setState({
            business: {
                ...this.state.business,
                rucNumber: e.target.value,
            },
            error:{
                ...this.state.error,
                rucNumber: e.target.value.length === 11
            }
        })
    }

    handleChange2 = (e) => {
        e.preventDefault();
        let value = removeSpecialCharacters(e.target.value)
        this.setState({
            business: {
                ...this.state.business,
                businessName: value
            },
            error:{
                ...this.state.error,
                businessName: value !== ''
            }
        })
    }

    handleChange3 = (e) => {
        e.preventDefault();
        this.setState({
            business: {
                ...this.state.business,
                description: removeSpecialCharacters(e.target.value),
            },
        })
    }

    handleChangeSwitchVisible = (e) => {
        //e.preventDefault()
        this.setState({
            business:{
                ...this.state.business,
                visible: e.target.checked
            }
        })
    }

    handleSubmit = data => {
        let { business, error } = this.state
        data.persist();
        let sendData = {
            ruc_emp: business.rucNumber,
            nom_emp: business.businessName.trim(),
            des_observaciones: business.description.trim(),
            flg_visible: business.visible
        }
        if(error.businessName && error.rucNumber){
            // Set client
            this.props.createBusiness(sendData)
            .then((response) => {
                this.setState({
                    ...this.state,
                    business: {
                        rucNumber: "",
                        businessName: "",
                        description: "",
                        visible: true
                    },
                    error: {
                        rucNumber: false,
                        businessName: false,
                    },

                }, () => {
                    if(response.status=== 200){
                        this.handleClickCloseModal()
                    } else{
                        this.getNotistack(response.data.errorMessage, "error")
                    }
                })
            }).catch(error=>{
                console.error(error)
                console.error('Empresa mal creada')
                this.getNotistack(error.response.data.errorMessage, "error")
                //window.location.reload()

            });

        } else {
            if(!error.businessName){
                this.getNotistack("Ingresar Razón Social de la Empresa", "warning")
            }
            if(!error.rucNumber){
                this.getNotistack("Ingresar RUC de la Empresa", "warning")
            }
        }

        //window.location.reload(false);
    }

    handleClickOpenModal = () => {
        this.setState(state => ({ ...state, openModal: true}));
    }
    handleClickCloseModal = () => {
        this.setState(
            state => ({ 
                ...state, 
                openModal: false,
                business: {
                    rucNumber: "",
                    businessName: "",
                    description: "",
                    visible: true
                },

            }));
    }

    render = () => {
        const { classes, title = true } = this.props;
        const { openModal, error } = this.state;
        return(
            <>
                <Tooltip title={"Registrar Empresa"} placement="left">
                    <div>
                        <Button
                            className={classes.button}
                            variant="contained"
                            color="primary"
                            fullWidth
                            onClick={this.handleClickOpenModal}
                            >
                            { title ? 'Nuevo' : ''}
                            <BusinessIcon className="ml-2" size="small"/>
                        </Button>
                    </div>
                </Tooltip>  
                {/* Modal Files */}                      
                <Dialog
                    fullWidth
                    onClose={this.handleClickCloseModal}
                    open={openModal}
                    aria-labelledby="form-dialog">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-dialog" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                Registro de Ruc
                            </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form method="post" >
                        <DialogContent>
                            <Grid container spacing={8}>
    
                                <Grid item xs={12}>
                                    <Grid container spacing={8}>
                                        <Grid item xs={12}>
                                            <TextField
                                                error={!error.rucNumber}
                                                fullWidth={true}
                                                label="Nro Ruc *"
                                                type="text"
                                                margin="normal"
                                                color="default"
                                                id="rucNumberInput"
                                                name="rucNumberInput"
                                                onChange={this.handleChange}
                                                onKeyPress={this.handleKeyPressTextFieldOnlyNumber("rucNumber")}
                                                placeholder="Ingresar nro. Ruc."
                                                value={this.state.business.rucNumber}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        ref: this.rucNumberInput,
                                                        maxLength: 11,
                                                    }
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                error={!error.businessName}
                                                fullWidth={true}
                                                label="Razón Social *"
                                                type="text"
                                                margin="normal"
                                                color="default"
                                                id="razonSocial"
                                                name="razonSocial"
                                                onChange={this.handleChange2}
                                                onBlur={this.handleChange2}
                                                placeholder="Ingresar razón social."
                                                value={this.state.business.businessName}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        ref: this.razonSocial,
                                                        maxLength: 100,
                                                    }
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField 
                                                fullWidth={true}
                                                label="Descripción"
                                                type="text"
                                                margin="normal"
                                                color="default"
                                                id="descripcion"
                                                name="descripcion"
                                                onChange={this.handleChange3}
                                                placeholder="Ingresar Descripción."
                                                value={this.state.business.description}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        ref: this.descripcion,
                                                        maxLength: 300,
                                                    }
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControlLabel
                                                control={
                                                    <Switch
                                                        checked={this.state.business.visible}
                                                        onChange={this.handleChangeSwitchVisible}
                                                        value={true}
                                                        color="primary"
                                                    />
                                                }
                                                label="Visible"
                                            />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                            <Grid item xs={12}>
                                    <div>
                                        <Button 
                                            className={classes.button}
                                            fullWidth={true} 
                                            color="primary"
                                            size="small" 
                                            onClick={this.handleSubmit}
                                            margin="normal">
                                            Aceptar
                                            <SendIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={12}>
                                    <div>
                                        <Button 
                                            className={classes.button}
                                            fullWidth={true} 
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickCloseModal}
                                            margin="normal">
                                            Cancelar
                                            <CancelIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  
            </>
        )
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(RegisterModalButton)));
