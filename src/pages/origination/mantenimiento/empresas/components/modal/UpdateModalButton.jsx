import React, { Component } from "react";
import {
    AppBar,
    Fab,
    Tooltip,
    withStyles,
    Dialog,
    DialogTitle,
    Typography,
    Grid,
    DialogActions,
    TextField,
    Button,
    DialogContent,
    Switch,
    FormControlLabel, IconButton
} from "@material-ui/core";
 import * as moment from 'moment';
 //import { onlyNumberKeyCode } from '../../../../../../utils/Utils' 
// Colors  
import { orange } from '@material-ui/core/colors'
// Icons
import CreateIcon from '@material-ui/icons/Create'
import { withSnackbar } from 'notistack';
import SendIcon from '@material-ui/icons/Send';
import CancelIcon from '@material-ui/icons/Cancel';

import { updateBusiness } from '../../../../../../actions/odc-express/odc'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

//import Zoom from 'react-reveal/Zoom';
import {removeSpecialCharacters} from '../../../../../../utils/Utils'
import CloseIcon from "@material-ui/icons/Close";

const styles = theme => ({
    modalRoot:{
        minWidth:280, 
        width:450,
        maxWidth:680,
    },
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: orange[700],
        '&:hover': {
            backgroundColor: orange[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    button:{
        textTransform: "none"
    },
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
      },
      title: {
        display: 'none',
        flex: 1,
		fontSize: 16,
		color:'white',
		textTransform: "uppercase",
		textShadow:"1px 1px 6px black",
		[theme.breakpoints.up('sm')]: {
		    display: 'block',
		},
  	},
});

const mapStateToProps = (state) => {
    return {
        odc: state.odcReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const actions = {
        updateBusiness: bindActionCreators(updateBusiness, dispatch)
    }
    return actions
}

class UpdateModalButton extends Component{
    state = {
        business: {
            rucNumber: "",
            businessName: "",
            description: "",
            visible: ''
        },
        error: {
            rucNumber: false,
            businessName: false,
        },
        openModal: false
    }

    componentDidMount() {
        this.setState({
            business:{
                rucNumber: this.props.row.ruc_emp,
                businessName: this.props.row.nom_emp,
                description: this.props.row.des_observaciones,
                visible:  this.props.row.flg_visible
            }, error :{
                rucNumber: this.props.row.ruc_emp.length === 11,
                businessName: this.props.row.nom_emp !== ''
            }
        })
    }

    componentWillMount = () => {
        let  initialRegistrationDate = moment().subtract(7, "day").format('YYYY-MM-DD');
        let  finalRegistrationDate = moment().format('YYYY-MM-DD');

        this.setState(state => ({
            ...state, 
            initialRegistrationDate: initialRegistrationDate, 
            finalRegistrationDate: finalRegistrationDate
        }));
    }

    handleChangeTextFieldRequired = (objectName, subObjectName, name) => e => {
        e.persist();
        let { value } = e.target;
        let nameError = `${name}Error`;

        if(subObjectName){
            this.setState(state => ({
                ...state,
                [objectName]:{
                    ...state[objectName], 
                    [subObjectName]:{
                        ...state[objectName][subObjectName],
                        [name]: value? value: "",
                        [nameError]: value !== "" ? false : true,
                        
                    }
                }
            }));
        }else{
            this.setState(state => ({
                ...state,
                [objectName]:{
                    ...state[objectName], 
                    [name]: value? value: "",
                    [nameError]: value !== "" ? false : true
                }
            }));
        }
    }
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error":
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default:
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    handleChange = (e) => {
        e.preventDefault();
        this.setState({
            business: {
                ...this.state.business,
                rucNumber: e.target.value,
            }
        })
    }

    handleChange2 = (e) => {
        e.preventDefault();
        let value = removeSpecialCharacters(e.target.value)
        this.setState({
            business: {
                ...this.state.business,
                businessName: value
            },
            error:{
                ...this.state.error,
                businessName: value !== ''
            }
        })
    }

    handleChange3 = (e) => {
        e.preventDefault();
        this.setState({
            business: {
                ...this.state.business,
                description: removeSpecialCharacters(e.target.value),
            }
        })
    }

    handleChangeSwitchVisible = (e) => {
        //e.preventDefault()
        this.setState({
            business:{
                ...this.state.business,
                visible: e.target.checked
            }
        })
    }

    handleSubmit = data => {
        
        let { row } = this.props;
        let { business, error } = this.state
        data.persist();
        let sendData = {
            cod_emp: row.cod_emp,
            ruc_emp: business.rucNumber,
            nom_emp: business.businessName.trim(),
            des_observaciones: business.description,
            flg_visible: business.visible
        }
        if(error.businessName && error.rucNumber) {
            if (business) {
                // Set client
                this.props.updateBusiness(sendData)
                    .then((response) => {
                        this.setState({
                            ...this.state,
                            business: {
                                ...this.state.business,
                                ...data
                            },
                            error: {
                                rucNumber: false,
                                businessName: false,
                            },
                            openModal: false
                        }, () => {
                            if(response.status=== 200){
                                this.handleClickCloseModal()
                            } else{
                                this.getNotistack(response.data.errorMessage, "error")
                            }
                        })
                    }).catch(error=>{
                        console.error(error)
                        console.error('Empresa mal creada')
                        this.getNotistack(error.response.data.errorMessage, "error")
                    });
            }
        } else {
            if(!error.businessName){
                this.getNotistack("Ingresar Razón Social de la Empresa", "warning")
            }
            if(!error.rucNumber){
                this.getNotistack("Ingresar RUC de la Empresa", "warning")
            }
        }
    }

    handleClickOpenModal = () => {

        this.setState(state => ({ 
            ...state,
            openModal: true
        }));
        
    }
    handleClickCloseModal = () => {
        this.setState(state => ({ ...state, openModal: false}));
    }

    render = () => {
        const { disabled, classes = true } = this.props;
        
        let {business} = this.state
        const { openModal, error } = this.state;

        return(
            <>
                <Tooltip 
                    title={
                        !disabled ? `Actualizar Datos de Ruc: 
                        ${business.rucNumber}` : "Ruc no registrado."
                    } placement="left"
                >
                    <div>
                        <Fab 
                            onClick={this.handleClickOpenModal}
                            className={classes.fab}>
                            <CreateIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>  
                {/* Modal Files */}
                <Dialog
                    fullWidth
                    onClose={this.handleClickCloseModal}
                    open={openModal}
                    aria-labelledby="form-dialog">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-dialog" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                Actualizar Ruc
                            </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form method="post" >
                        <DialogContent>
                            <Grid container spacing={8}>
    
                                <Grid item xs={12}>
                                    <Grid container spacing={8}>
                                        <Grid item xs={12}>
                                            <TextField 
                                            disabled
                                                fullWidth={true}
                                                label="Nro Ruc *"
                                                type="text"
                                                margin="normal"
                                                color="default"
                                                id="rucNumberInput"
                                                name="rucNumberInput"
                                                onChange={this.handleChange}
                                                //onKeyPress={this.handleKeyPressTextFieldOnlyNumber("rucNumber")}
                                                placeholder="Ingresar nro. Ruc."
                                                value={business.rucNumber}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        ref: this.rucNumberInput,
                                                        maxLength: 11,
                                                    }
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                error={!error.businessName}
                                                fullWidth={true}
                                                label="Razón Social *"
                                                type="text"
                                                margin="normal"
                                                color="default"
                                                id="razonSocial"
                                                name="razonSocial"
                                                onChange={this.handleChange2}
                                                onBlur={this.handleChange2}
                                                value={business.businessName}
                                                placeholder="Ingresar razón socialsss."
                                                defaultValue={business.businessName}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        ref: this.razonSocial,
                                                        maxLength: 100,
                                                    }
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField 
                                                fullWidth={true}
                                                label="Descripción"
                                                type="text"
                                                margin="normal"
                                                color="default"
                                                id="descripcion"
                                                name="descripcion"
                                                onChange={this.handleChange3}
                                                placeholder="Ingresar Descripción."
                                                defaultValue={business.description}
                                                value={business.description}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        ref: this.descripcion,
                                                        maxLength: 300,
                                                    }
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControlLabel
                                                control={
                                                    <Switch
                                                        checked={this.state.business.visible}
                                                        onChange={this.handleChangeSwitchVisible}
                                                        value={this.state.business.visible}
                                                        color="primary"
                                                    />
                                                }
                                                label="Visible"
                                            />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                            <Grid item xs={12}>
                                    <div>
                                        <Button 
                                            className={classes.button}
                                            fullWidth={true} 
                                            color="primary"
                                            size="small" 
                                            onClick={this.handleSubmit}
                                            margin="normal">
                                            Aceptar
                                            <SendIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={12}>
                                    <div>
                                        <Button 
                                            className={classes.button}
                                            fullWidth={true} 
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickCloseModal}
                                            margin="normal">
                                            Cancelar
                                            <CancelIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>
            </>
        )
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(UpdateModalButton)));
