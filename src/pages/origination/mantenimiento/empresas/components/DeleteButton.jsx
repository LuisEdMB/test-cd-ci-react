import React, { Component } from "react";
import {
    Fab, 
    Tooltip, 
    withStyles, 
    Dialog,
    Typography,
    DialogTitle,
    DialogContent,
    DialogActions,
    Grid,
    Button
 } from "@material-ui/core";
// Colors
import { red } from '@material-ui/core/colors'
// Icons
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { withSnackbar } from 'notistack';
//
import { deleteBusiness, SearchBusiness } from '../../../../../actions/odc-express/odc'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

const styles = theme => ({
    modalRoot:{
        minWidth:280, 
        width:450,
        maxWidth:680,
    },
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: red[500],
        '&:hover': {
            backgroundColor: red[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }
});

const mapStateToProps = (state) => {
    return {
        odc: state.odcReducer
    }
}


const mapDispatchToProps = (dispatch) => {
    const actions = {
        deleteBusiness: bindActionCreators(deleteBusiness, dispatch),
        SearchBusiness: bindActionCreators(SearchBusiness, dispatch)
    }
    return actions
}

class DeleteButon extends Component{
    state = {
        business: {
            codBusiness: "",
            rucNumber: "",
            businessName: "",
            description: ""
        },
        openModal: false
    }

    handleClickOpenModal = () => {
        this.setState(state => ({ ...state, openModal: true}));
    }

    handleClickCloseModal = () => {
        this.setState(state => ({ ...state, openModal: false}));
    }

    handleSubmit = data => {
        let { row } = this.props;
        let { business } = this.state
        data.persist();
        let sendData = {
            cod_emp: row.cod_emp,
            //ruc_emp: business.rucNumber,
            //nom_emp: business.businessName,
            //des_observacione: business.description
        }
        if(business)
        // Set client
        this.props.deleteBusiness(sendData)
        .then(() => {
            this.setState({
                ...this.state,
                business:{
                    ...this.state.business, 
                    ...data
                },
                openModal: false
            })
        });
        window.location.reload(false);
    }

    render = () => {
        const { disabled, row, classes = true } = this.props;
        //const { openModal } = this.state;
        let rn = row.ruc_emp
        let nm = row.nom_emp

        return(
            <>
                <Tooltip 
                    title={
                        !disabled ? `Eliminar Ruc:: 
                        ${rn}` : "Ruc no registrado." 
                    } placement="left"
                >
                    <div>
                        <Fab 
                            onClick={this.handleClickOpenModal}
                            //onClick={this.handleSubmit}
                            className={classes.fab}>
                            <DeleteForeverIcon className={classes.icon} fontSize="small"/>
                        </Fab><Dialog
                        onClose={this.handleCloseModalCancelExpress}
                        open={this.state.openModal}
                        aria-labelledby="form-dialog">
                    <DialogTitle 
                        id="form-dialog" 
                        className="bg-metal-blue">
                        <Typography align="center" component="span" variant="h6" className="text-white text-shadow-black">
                            Eliminar Empresa
                        </Typography>
                    </DialogTitle>
                    <form className={classes.modalRoot} autoComplete="off">
                        <DialogContent>
                            <Typography align="center">
                                ¿Esta seguro de eliminar la empresa?
                            </Typography>
                            <Typography align="center">
                                {nm}
                            </Typography>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div className={classes.buttonProgressWrapper}>
                                        <Button 
                                            //disabled={odcActivity.cancelActivity.loading}
                                            className={classes.button}
                                            fullWidth={true} 
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickCloseModal}
                                            margin="normal">
                                            No
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                <div className={classes.buttonProgressWrapper}>
                                        <Button 
                                            //disabled={odcActivity.cancelActivity.loading}
                                            className={classes.button}
                                            fullWidth={true} 
                                            color="primary"
                                            size="small" 
                                            onClick={this.handleSubmit}
                                            margin="normal">
                                            Si
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>
                    </div>
                </Tooltip>  
            </>
        )
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(DeleteButon)));
