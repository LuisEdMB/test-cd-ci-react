import React, { Component } from "react";
import * as moment from 'moment';
//import { withSnackbar } from 'notistack';
// React Router 
//import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//import Autocomplete from '../../../../../components/Autocomplete'
//import { getNotistack } from '../../../../../utils/Notistack'
import {  
    Grid,
    Button,
    TextField, 
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Typography,
    FormHelperText,
    withStyles
} from '@material-ui/core';

// Icons
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FilterListIcon from '@material-ui/icons/FilterList';
import SearchIcon from '@material-ui/icons/Search';

// Utils 

import { validateLengthDocumentType, checkInputKeyCode, onlyNumberKeyCode } from '../../../../../utils/Utils';
import { Bounce } from 'react-reveal';

import RegisterModalButton from './register/RegisterModalButton'

const styles = theme => ({
    root:{
        width: "100%",
        //borderRadius: "0 0 .5em .5em",
        boxShadow: 'none',
        borderBottom: "1px solid #80808033",
    },
    paper:{
        padding: 20
    },
    buttonWrapper:{
        display: "center",
        alignItems: "center"
    }, 
    heading:{
        display: "flex",
        alignItem: "center",
        [theme.breakpoints.down('sm')]: {
            fontSize:14, 
        },
        [theme.breakpoints.up('md')]: {
            fontSize:16, 
        }
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        //idenfiticationDocumentType: state.identificationDocumentTypeReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        //getIdentificationDocumentType: bindActionCreators(getIdentificationDocumentType, dispatch)
    };
  return actions;
}

class BusinessFilter extends Component {
    state = {
            cod_emp:"",
            business: {
                rucNumber: "",
                businessName: "",
            },
            loading: true,
    }

    componentWillMount = () => {
        let  initialRegistrationDate = moment().subtract(7, "day").format('YYYY-MM-DD');
        let  finalRegistrationDate = moment().format('YYYY-MM-DD');

        this.setState(state => ({
            ...state, 
            initialRegistrationDate: initialRegistrationDate, 
            finalRegistrationDate: finalRegistrationDate
        }));
    }
    handleSubmitSearch = data => {
        data.preventDefault();
        const { 
            rucNumber,
            businessName
            } = this.state.business;
        let sendData  = {
            rucNumber,
            businessName
        } 
        this.props.handleSubmitFilter(sendData);
    }
    // Client - Document Number - TextField Event Change
    handleChangeTextFieldDocumentNumber = name => e =>{
        e.preventDefault();
        let { value } = e.target;
        //let nameError = `${name}Error`;
        if(!isNaN(Number(value))){
            this.setState({
                business: {
                    ...this.state.business,
                    rucNumber: value,
                    nameError: !validateLengthDocumentType(value.length)

                }
            });        
        }
    }
    //
    handleChangeBusiness = (e) => {
        e.preventDefault();
        this.setState({
            business: {
                ...this.state.business,
                businessName: e.target.value,
            }
        })
    }

    // Only Number
    handleKeyPressTextFieldOnlyNumber = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!onlyNumberKeyCode(code)){
            e.preventDefault();
        }   
    }

    // Check Input
    handleKeyPressTextFieldCheckInput = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!checkInputKeyCode(code)){
            e.preventDefault();
        }   
    }

    handleOnClickReload = () => {
        this.props.history.go(0)
    }

    render = () => {
        const {
                classes, 
                title, 
                } = this.props;
        
        return( <div>
                    <ExpansionPanel className={classes.root}  defaultExpanded>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel-content"
                            id="panel-header"
                        >
                            <Typography variant="h6" className={classes.heading}>
                                <FilterListIcon className="mr-2" />
                                {title}
                            </Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <div className="w-100">
                                <form method="post" /*autoComplete="off"*/ onSubmit={this.handleSubmitSearch}>
                                    <Grid container spacing={8} className="mb-2">
                                        {/* Document Number */}
                                        <Grid item xs={12} sm={6} md={4} lg={2}>
                                            <TextField
                                                //error={this.state.documentNumberError}
                                                fullWidth={true}
                                                label="Nro Ruc"
                                                type="text"
                                                margin="normal"
                                                color="default"
                                                id="rucNumber"
                                                name="rucNumber"
                                                defaultValue={this.state.rucNumber}
                                                onKeyPress={this.handleKeyPressTextFieldOnlyNumber("rucNumber")}
                                                onChange={this.handleChangeTextFieldDocumentNumber("rucNumber")}
                                                placeholder="Ingresar nro. Ruc."
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        maxLength: this.state.documentTypeId === 100001 ? 8:11,
                                                    }
                                                }}
                                            />
                                            
                                            <FormHelperText className="text-right">
                                            {/**{this.state.documentNumber.toString().length}/{this.state.documentTypeId === 100001 ? 8 : 9} */}
                                            </FormHelperText>
                                        </Grid> 
                                        <Grid item xs={12} sm={6} md={4} lg={2}>
                                            <TextField
                                                //error={this.state.documentNumberError}
                                                fullWidth={true}
                                                label="Nombre Empresa"
                                                type="text"
                                                margin="normal"
                                                color="default"
                                                id="businessName"
                                                name="businessName"
                                                defaultValue={this.state.businessName}
                                                onKeyPress={this.handleKeyPressTextFieldCheckInput("businessName")}
                                                onChange={this.handleChangeBusiness}
                                                placeholder="Ingresar Nombre de la Empresa."
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12} md={4} lg={1} className="d-flex justify-content-center align-items-center">
                                            <div className="w-100">
                                                <Bounce> 
                                                    <div>
                                                        <Button className={classes.button} fullWidth variant="contained" type="submit"  color="primary">
                                                             Consultar
                                                            <SearchIcon className="ml-2" size="small" />
                                                        </Button>
                                                    </div>
                                                </Bounce>
                                            </div>
                                        </Grid>
                                        <Grid item xs={12} sm={12} md={4} lg={1} className="d-flex justify-content-center align-items-center">
                                        <div className="w-100">
                                                <Bounce> 
                                                    <div>
                                                        <RegisterModalButton history={this.props.history} />
                                                    </div>
                                                </Bounce>
                                            </div>
                                        </Grid>
                                    </Grid>
                                </form>
                            </div>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </div>
            )
    }
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(BusinessFilter));
