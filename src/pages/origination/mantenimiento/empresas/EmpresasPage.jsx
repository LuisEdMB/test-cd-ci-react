import React, { Component } from "react";
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
import { bindActionCreators } from "redux";
// React Router 
import { connect } from 'react-redux';
// Component
import{ 
    IconButton,
    Grid, 
    LinearProgress,
    withStyles,
    Divider
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close'; 
//
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent'
import RowDetail from './components/Table/RowDetail'
// Actions 
import { SearchBusiness } from '../../../../actions/odc-express/odc'
// Utils 
import { defaultPageSizes } from '../../../../utils/Utils';
//
import BusinessFilter from './components/BusinessFilter';
import Header from './components/Header'
import Cell from './components/Table/CellUpdate'

const styles = theme => ({
    avatar:{
        width:80, 
        height:48
    },
    modalRoot:{
       
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        odc: state.odcReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        SearchBusiness: bindActionCreators(SearchBusiness, dispatch),
    };
    return actions;
}

class EmpresasPage extends Component {

    state = {
        columnExcel:[],
        defaultColumnWidths: [
            { columnName: 'update', width: 120 },
            //{ columnName: 'delete', width: 120 },
            { columnName: 'ruc_emp', width: 125},
            { columnName: 'nom_emp', width: 230 },
            { columnName: 'flg_visible', width: 100 },
            { columnName: 'des_usu_act', width: 230 },
            { columnName: 'fec_act', width: 230 },
            { columnName: 'fec_reg', width: 230 },
            { columnName: 'des_usu_reg', width: 230 },
            { columnName: 'des_observaciones', width: 230 },

        ],
        columns:[],
        tableColumnExtensions: [
            { columnName: 'authorize', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        rowsExcel:[],
        additional: 0, 
        reprint: 0, 
        express: 0,
        pageSizes: defaultPageSizes,
        dateDiffDays: 0
        
    };
    
    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
            
        }
        if(prevProps.odc !== this.props.odc) {
           
            if (prevProps.odc.SearchBusiness !== this.props.odc.SearchBusiness) {	
                if(this.props.odc.SearchBusiness.loading){
                    this.setState(state => ({
                        ...state, 
                        rows: [],
                        rowsExcel: []
                    }));
                }
                if(!this.props.odc.SearchBusiness.loading && 
                    this.props.odc.SearchBusiness.response && 
                    this.props.odc.SearchBusiness.success){
                        
                    let { data } = this.props.odc.SearchBusiness;
                    
                    let buscarEmpresa = data
    
                    if(buscarEmpresa){
    
                        let columnExcel = Object.keys(buscarEmpresa.length > 0);
                        columnExcel = columnExcel.map((item, index) => (
                            {value: item, label: item}
                        ));
        
                        this.setState(state => ({
                            ...state,
                            rows: buscarEmpresa,
                            columnExcel: columnExcel,
                            rowsExcel: buscarEmpresa
                        }));
                    }
                    
                }
                else if(!this.props.odc.SearchBusiness.loading && 
                         this.props.odc.SearchBusiness.response && 
                        !this.props.odc.SearchBusiness.success){
                    // Notistack
                    this.getNotistack(this.props.odc.SearchBusiness.error, "error");
                    this.setState(state => ({  ...state, rows: [],  rowsExcel:[], columnExcel: []}));
                }
                else if(!this.props.odc.SearchBusiness.loading && 
                        !this.props.odc.SearchBusiness.response && 
                        !this.props.odc.SearchBusiness.success){
                    // NotistackhandleSubmitFilter
                    this.getNotistack(this.props.odc.SearchBusiness.error, "error");
                    this.setState(state => ({  ...state, rows: [],  rowsExcel:[], columnExcel: []}));
                }
            }

            if (prevProps.odc.createBusiness !== this.props.odc.createBusiness){
                if(!this.props.odc.createBusiness.loading && 
                    this.props.odc.createBusiness.response && 
                    this.props.odc.createBusiness.success){
                        this.getNotistack("Empresa creada satisfactoriamente", "success")
                        this.reloadFilter();
                }
            } 
            if (prevProps.odc.updateBusiness !== this.props.odc.updateBusiness){
                if(!this.props.odc.updateBusiness.loading && 
                    this.props.odc.updateBusiness.response && 
                    this.props.odc.updateBusiness.success){
                        this.getNotistack("Empresa actualizada satisfactoriamente", "success")
                        this.reloadFilter();
                }
            }
        }
        
    }

    componentWillMount() {
        const columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns:columns
        }));
    }

    componentDidMount() {
        this.reloadFilter();
    }
    
    reloadFilter(){
        let sendData = {
            rucNumber: "", 
            businessName: ""
        }
        this.handleSubmitFilter(sendData);
    }

    getColumnsRender(){
        let columns = [];
        if(this.props.width === "xs"){
            columns = [
                { name: 'update', title: 'Actualizar' },
                //{ name: 'delete', title: 'Eliminar' },
                { name: 'ruc_emp', title: 'Ruc' },
                { name: 'flg_visible', title: 'Estado' }
            ]
        }
        else if(this.props.width === "sm"){
            columns = [
                { name: 'update', title: 'Actualizar' },
                // { name: 'delete', title: 'Eliminar' },
                { name: 'ruc_emp', title: 'Ruc' },
                { name: 'nom_emp', title: 'Nombre ' },
                { name: 'flg_visible', title: 'Estado' },
                { name: 'des_usu_act', title: 'Usuario Actualizador' },
                { name: 'fec_reg', title: 'Fecha de registro' }
            ]
        }
        else if(this.props.width === "md"){
            columns = [
                { name: 'update', title: 'Actualizar' },
                // { name: 'delete', title: 'Eliminar' },
                { name: 'ruc_emp', title: 'Ruc' },
                { name: 'nom_emp', title: 'Nombre ' },
                { name: 'flg_visible', title: 'Estado' },
                { name: 'des_usu_act', title: 'Usuario Actualizador' },
                {name: 'fec_act', title: 'Fecha de Actualizacion'},
                { name: 'fec_reg', title: 'Fecha de registro' },
                { name: 'des_observaciones', title: 'Observaciones' }
            ]
        }
        else if(this.props.width === "lg"){
            columns = [
                { name: 'update', title: 'Actualizar' },
                // { name: 'delete', title: 'Eliminar' },
                { name: 'ruc_emp', title: 'Ruc' },
                { name: 'nom_emp', title: 'Nombre ' },
                { name: 'flg_visible', title: 'Estado' },
                { name: 'des_usu_act', title: 'Usuario Actualizador' },
                {name: 'fec_act', title: 'Fecha de Actualizacion'},
                { name: 'fec_reg', title: 'Fecha de registro' },
                { name: 'des_usu_reg', title: "Usuario Creador" },
                { name: 'des_observaciones', title: 'Observaciones' }
            ]
        }
        else if(this.props.width === "xl"){
            columns = [
                { name: 'update', title: 'Actualizar' },
                // { name: 'delete', title: 'Eliminar' },
                { name: 'ruc_emp', title: 'Ruc' },
                { name: 'nom_emp', title: 'Nombre ' },
                { name: 'flg_visible', title: 'Estado' },
                { name: 'des_usu_act', title: 'Usuario Actualizador' },
                {name: 'fec_act', title: 'Fecha de Actualizacion'},
                { name: 'fec_reg', title: 'Fecha de registro' },
                { name: 'des_usu_reg', title: "Usuario Creador" },
                { name: 'des_observaciones', title: 'Observaciones' }
                
            ]
        }
        return columns;
    }
   
    handleSubmitFilter = data => {
        if(data){
            this.props.SearchBusiness(data.rucNumber, data.businessName);
        }
    }

    handleShowHideSearchInput = e => {
        this.setState(state => ({
            showHideSearchInput: !state.showHideSearchInput
        }));
    }

    handleClickOpenSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: true
        }));
    }

    handleClickCloseSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: false
        }));
    }

    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }

        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }

    render() {
        const { width, odc, history } = this.props;
        const { 
                columns,
                defaultColumnWidths, 
                defaultHiddenColumnNames, 
                rows,
                tableColumnExtensions,
            } = this.state;
            
        return (
            <Grid container className="p-1">
                {
                <Grid  item xs={12}>
                    <Grid  item xs={12} style={{height: 12}}>
                        { odc.SearchBusiness.loading ?  <LinearProgress /> : "" }
                    </Grid>
                </Grid>
                 }
                <Grid item xs={12}>
                    <Header 
                        title={"Empresas" }
                    />
                </Grid>
                <Grid item xs={12}>
                    <Divider className="mb-2"/>
                </Grid>
                <Grid item xs={12}  className="mb-2">
                    <BusinessFilter title={"Filtros de búsqueda"} 
                        handleSubmitFilter={this.handleSubmitFilter}
                                    history={history}
                    />
                </Grid>
                <Grid item xs={12}>
                      <DevGridComponent 
                        rows={rows}
                        columns={columns}
                        width={width}
                        tableColumnExtensions={tableColumnExtensions}
                        defaultColumnWidths={defaultColumnWidths}
                        defaultHiddenColumnNames={defaultHiddenColumnNames}
                        RowDetailComponent={RowDetail}
                        CellComponent={Cell}
                    />
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(EmpresasPage))));
