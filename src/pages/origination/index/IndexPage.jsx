import React, { Component } from 'react';
import { Link }  from 'react-router-dom';
// Material UI
import { withStyles } from '@material-ui/core/styles';
// Component
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import Icon from '@material-ui/core/Icon';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { URL_BASE } from '../../../actions/config'; 
// Icons 
import { decrypt } from '../../../utils/Utils';

const styles = theme => ({
    root:{
        width:"100%", 
        height:"100vh"
    }, 
    card: {
        maxWidth: 345,
        transition:".3s",
        border:"1px solid #bdbdbd",
        '&:hover':{
            boxShadow:"1px 1px 1px orange",
            border:"1px solid orange",
        }
    },
    media: {
        height: 130,
        padding:".5em"
    },
    text:{
        fontSize:12
    }
});

class IndexPage extends Component {
    state = {
        menus:[], 
        submenus:[],
        index:-1
    }

    handleClickListItemMenu = (e) => {        
        this.setState(state => ({ openList: !state.openList }));
    }
    handleListItemClick = (e, index) => {
        this.setState(state => ({ selectedListItemIndex: index }));
    }

    handleClick = (e, index)   => {
        if(this.state.index === index){
            this.setState(state => ({ index: -1 }));
        }
        else{
            this.setState(state => ({ index: index }));
        }
    }

    render() {
        let { classes } = this.props;
        let menuId = -1;
        let submenuId = -1;
        let menuFather = [];
        // Get localstorage
        const decryptData = sessionStorage.getItem('data') 
        let data = decrypt(decryptData);

        // Start Logic
        data.profiles.permissions.sort((a,b) => (a.option.levelId || b.option.levelId) > 1 && 
            ((a.option.order > b.option.order) ? 1 : ((b.option.order > a.option.order) ? -1 : 0)))
        .filter(permission => permission.option.visible && !permission.option.active)
        .map((permission, index) => {
            if (menuId !== permission.option.id){
                if (permission.option.fatherOptionId === 0){
                    let children = data.profiles.permissions.filter((element) => {
                        return element.option.fatherOptionId === permission.option.id;
                    })
                    menuFather.push( {
                        id:1, 
                        component:
                        <Card key={index} className={classes.card}>
                            <CardActionArea>
                                <CardMedia
                                    className={classes.media}
                                    //image={`https://picsum.photos/200/${index + 100}`}
                                    image={permission.option.image}
                                    title={permission.option.name}
                                />
                            </CardActionArea>
                            <CardContent style={{padding:8}}>
                                <ListItem 
                                    disabled={children.length > 0 ? false:true }
                                    button onClick={(e) => this.handleClick(e, index)}>
                                    <ListItemIcon>
                                        <Icon>
                                            { permission.option.icon }
                                        </Icon>
                                    </ListItemIcon>
                                    {}
                                    <ListItemText inset primary={permission.option.name} />
                                    {
                                        children.length > 0 ? this.state.index === index? <ExpandLessIcon /> : <ExpandMoreIcon /> :""
                                    }
                                </ListItem> 
                                <Collapse in={this.state.index === index} timeout="auto" unmountOnExit> 
                                    {
                                        data.profiles.permissions
                                        .filter(permission => permission.option.visible && !permission.option.active)
                                        .map((child, index) => {
                                            if (child.option.fatherOptionId === permission.option.id){
                                                if (submenuId !== child.option.id){
                                                    submenuId = child.option.id;
                                                    return (
                                                        <List component="div" disablePadding key={index}>
                                                            <Link 
                                                                to={permission.option.url !== "/"?
                                                                `/${URL_BASE}/${permission.option.url}/${child.option.url}`
                                                                :`/${URL_BASE}/${child.option.url}`
                                                                } 
                                                                className="d-flex w-100"> 
                                                                {/* Option Child */}
                                                                <ListItem 
                                                                    color="primary"
                                                                    button 
                                                                    className={classes.nested}
                                                                    selected={this.state.selectedListItemIndex === index}
                                                                    onClick={e => this.handleListItemClick(e, index)}>
                                                                    {/* Option - Icon */}
                                                                    <ListItemIcon>
                                                                        <Icon>
                                                                            {child.option.icon}
                                                                        </Icon>
                                                                    {/* Option - Name */}
                                                                    </ListItemIcon>
                                                                    <ListItemText inset primary={child.option.name} />
                                                                </ListItem>
                                                            </Link>
                                                        </List>
                                                    )
                                                }
                                            }
                                            return "";
                                        })
                                    }
                                </Collapse>
                            </CardContent>
                        </Card>
                    })
                }
                menuId = permission.option.id
            }
            return null;
        });
        return (
            <div className="p-5">
                <Grid container spacing={16} className="d-flex justify-content-center" >
                    {
                        menuFather.map((item, index) => (
                            <Grid key={index} item xs={12} sm={6} md={4} lg={3}>
                                { item.component}
                            </Grid>
                        ))
                    }                    
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(IndexPage);
