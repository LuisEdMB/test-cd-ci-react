import React from "react";
import * as moment from "moment";
import { Table } from '@devexpress/dx-react-grid-material-ui';
import ActivateActionButton from '../action-button/ActivateActionButton';
import AttachedActionButton from '../action-button/AttachedActionButton';
import RegisterActionButton from '../action-button/RegisterActionButton'
import NewCommentForm from '../../../../../../components/NewCommentForm/NewCommentForm';


import { getMoneyFormat } from '../../../../../../utils/Utils'

const Cell = ({...props}) => {
    let { row } = props;
    
    //let disabled = row.cod_bloqueo_tarjeta_pmp === "V"? false : true
    let rowFPS = row.cod_flujo_fase_estado ? row.cod_flujo_fase_estado : 0;

    //let disabled = false;
    let disabled = (rowFPS === 11011 || rowFPS === 90910 || rowFPS === 100802)
    let d = (rowFPS === 11011 || rowFPS === 90910 || rowFPS === 100802) ? false : true

    if(props.column.name === "monto_cci"){//
        return <Table.Cell {...props} value={props.value? `S/. ${getMoneyFormat(props.value)}`: '-'} />
    }
    if(props.column.name === "fec_reg"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    else if(props.column.name === "registrado"){
        return <Table.Cell {...props}>
            <RegisterActionButton {...props} disabled={!disabled} />
        </Table.Cell>
    }
    else if(props.column.name === "authorize"){
        return <Table.Cell {...props}>
            <ActivateActionButton {...props} disabled={d}  />
        </Table.Cell>
    }
    else if(props.column.name === "attached"){
        if(rowFPS >= 30302){
            disabled = false;
        }
        return <Table.Cell {...props}>
            <AttachedActionButton {...props}  disabled={disabled} />
        </Table.Cell>
    }
    if(props.column.name === "observation"){
        return <Table.Cell {...props}>
           <NewCommentForm {...props}/>
        </Table.Cell>
    }
    return <Table.Cell {...props} />
}

export default Cell;
