import React, { Component } from "react";
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
// React Router 
//import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components 
import { 
    Fab, 
    Tooltip, 
    withStyles, 
    Dialog, 
    DialogTitle,
    Typography,
    Grid,
    Button,
    DialogActions,
    DialogContent,
    IconButton,
    LinearProgress
 } from "@material-ui/core";
// Colors
import blue from '@material-ui/core/colors/blue';
// Icons 
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close'; 
// Actions 
//import { downloadReprintAttachedDocument } from '../../../../../../actions/odc-document/odc-document';
//import {getDocuments, downloadDocument} from '../../../../actions/odc-document/odc-document';
//import {getDocuments, downloadDocument} from '../../../../../../actions/odc-document/odc-document';

const styles = theme => ({
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: blue[700],
        '&:hover': {
            backgroundColor: blue[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    button:{
        textTransform: "none"
    }
});

const mapStateToProps = (state, props) => {
    return {
        odc: state.odcReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        //downloadReprintAttachedDocument: bindActionCreators(downloadReprintAttachedDocument, dispatch)
        //getDocuments: bindActionCreators(getDocuments, dispatch),
        //downloadDocument: bindActionCreators(downloadDocument, dispatch),
    };
  return actions;
}

class AttachedActionButton extends Component{
    state = {
        fileModal: false, 
        buttonLoading: false, 
        buttonDisabled: false,
        saeDocName: "",
        saeCodControl: ""
    }

    // Notistack
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }

    handleClickOpenModalFile = (e) => {
        const { row } = this.props
        let sendData = { 
            cod_solicitud_completa: row.cod_solicitud_completa,
        }

        this.props.getDocuments(sendData)
        .then(response => {
            if(response.data){
                const saeCodControl = response.data.documento[7].cod_control   
                const saeDoc = response.data.documento[7].nom_documento

                this.setState(state => ({
                    saeDocName: saeDoc,
                    saeCodControl: saeCodControl
                }));
            }else {
                this.setState(state => ({
                    fileModal: false
                }))
                //this.getNotistack(response.data.errorMessage, "error")
                this.getNotistack('SAE: Documentos no han sido encontrados o aun no ha sido generados.', "error");
                //this.getNotistack(`SAE: ${this.props.getDocuments.error}`, "error");
            }
            
        });

        this.setState(state => ({
            fileModal: true,
        }));
    }

    handleClickCloseModalFile = (e) => {
        this.setState(state => ({
            fileModal: false
        }));
    } 
    // Download Document
    handleDownloadDocument = item => e => {
        const { saeCodControl } = this.state 
        if(item){
            let sendData = { 
                //cod_control: 1001
                cod_control: saeCodControl
            }
            this.props.downloadDocument(sendData)
            .then(response => {
                if(response){
                    if(response.data){
                        const url = window.URL.createObjectURL(new Blob([response.data]));
                        let link = document.createElement('a');
                        link.href = url;
                        link.setAttribute('download', `${item.nom_documento}.pdf`);
                        link.click();
                    }
                }
            });
        }
    }
    
    render = () => {
        const { row, disabled, classes} = this.props;
        const { fileModal, buttonLoading, buttonDisabled, saeDocName } = this.state;
        return (
            <>
                <Tooltip 
                    title={
                            !disabled? `Ver Adjunto! Nro. Solicitud: ${row.cod_solicitud_completa}` : 
                            `Solicitud sin documento adjunto: ${row.cod_solicitud_completa}`
                        } 
                        placement="left">
                    <div>
                        <Fab className={classes.fab} onClick={this.handleClickOpenModalFile} disabled={disabled}>
                            <CloudDownloadIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>
                {/* Modal Files */}                      
                <Dialog
                    //onClose={this.handleClickCloseModalFile}
                    open={fileModal}
                    aria-labelledby="form-dialog-document">
                    <DialogTitle 
                        id="form-dialog-document" 
                        className="bg-metal-blue">
                        <Typography align="center" component="span" variant="h6" className="text-white text-shadow-black">
                            Formato de Desembolso
                        </Typography>
                    </DialogTitle>
    
                    <form className={classes.modalRoot}>
                        <DialogContent>
                            <Grid container spacing={8}>
                                <Grid item xs={12}>
                                    <Typography className="d-flex justify-content-between align-items-center">
                                        <Typography component="span" color="inherit">
                                            { saeDocName }
                                        </Typography>
                                        <IconButton  
                                            disabled={buttonDisabled}
                                            color="secondary" size="small" className="ml-2"
                                        >
                                            <CloudDownloadIcon 
                                                onClick={this.handleDownloadDocument(row)}
                                            />
                                        </IconButton>
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    {
                                        buttonLoading && <LinearProgress />
                                    }
                                </Grid>
                               
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={12}>
                                    <div>
                                        <Button 
                                            disabled={buttonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth={true} 
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickCloseModalFile}
                                            margin="normal">
                                            Cancelar
                                            <CancelIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  
            </>  
        )
   }
}    


export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(AttachedActionButton)));
