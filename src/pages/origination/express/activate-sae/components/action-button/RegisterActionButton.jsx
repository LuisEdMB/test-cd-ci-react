import React, { Component } from "react";
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components 
import { 
    Fab, 
    Tooltip, 
    withStyles, 
    Dialog, 
    DialogTitle,
    Typography,
    Grid,
    Button,
    DialogActions,
    DialogContent,
    IconButton,
    CircularProgress,
    //LinearProgress
 } from "@material-ui/core";
// Colors
import blue from '@material-ui/core/colors/blue';
import green from '@material-ui/core/colors/green'
// Icons 
import CheckIcon from '@material-ui/icons/Check';
import SendIcon from '@material-ui/icons/Send';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close'; 
// Actions 
import { simpleGenericActivityConfirmation } from '../../../../../../actions/odc-express/odc-activity'
import * as ActionOdcExistingClient from '../../../../../../actions/odc-existing-client/odc-existing-client'
// Utils
import * as Utils from '../../../../../../utils/Utils'

const styles = _ => ({
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: blue[700],
        '&:hover': {
            backgroundColor: blue[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    button:{
        textTransform: "none"
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    }
});

const mapStateToProps = (state) => {
    return {
        odcActivity: state.odcActivityReducer,
        odcExistingClient: state.odcExistingClientReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const actions = {
        simpleGenericActivityConfirmation: bindActionCreators(simpleGenericActivityConfirmation, dispatch),
        registerActivity: bindActionCreators(ActionOdcExistingClient.registerActivity, dispatch)

    };
  return actions;
}

class RegisterActionButton extends Component{
    state = {
        fileModal: false, 
        buttonAcceptLoading: false,
        buttonNotAcceptLoading: false, 
        buttonDisabled: false,
        saeDocName: "",
        saeCodControl: ""
    }

    // Notistack
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }

    componentDidUpdate(prevProps, _) {
        Utils.resolveStateRedux(prevProps.odcActivity.simpleGenericActivityConfirmation, this.props.odcActivity.simpleGenericActivityConfirmation,
            _ => this.setState(state => ({
                ...state,
                buttonDisabled: true,
                buttonAcceptLoading: true,
                buttonNotAcceptLoading: true
            })),
            _ => this.setState(state => ({
                ...state,
                buttonDisabled: false,
                buttonAcceptLoading: false,
                buttonNotAcceptLoading: false,
                fileModal: false
            })),
            _ => this.setState(state => ({
                ...state,
                buttonDisabled: false,
                buttonAcceptLoading: false,
                buttonNotAcceptLoading: false
            })),
            _ => this.setState(state => ({
                ...state,
                buttonDisabled: false,
                buttonAcceptLoading: false,
                buttonNotAcceptLoading: false
            })))
        Utils.resolveStateRedux(prevProps.odcExistingClient.activityRegistered, this.props.odcExistingClient.activityRegistered,
            _ => this.setState(state => ({
                ...state,
                buttonDisabled: true,
                buttonAcceptLoading: true,
                buttonNotAcceptLoading: true
            })),
            _ => this.setState(state => ({
                ...state,
                buttonDisabled: false,
                buttonAcceptLoading: false,
                buttonNotAcceptLoading: false,
                fileModal: false
            })),
            _ => this.setState(state => ({
                ...state,
                buttonDisabled: false,
                buttonAcceptLoading: false,
                buttonNotAcceptLoading: false
            })),
            _ => this.setState(state => ({
                ...state,
                buttonDisabled: false,
                buttonAcceptLoading: false,
                buttonNotAcceptLoading: false
            })))
    }

    handleClickOpenModalFile = (e) => {
        this.setState(state => ({
            fileModal: true,
        }));
    }

    handleClickCloseModalFile = (e) => {
        this.setState(state => ({
            fileModal: false
        }));
    }
    
    handleSubmitConformidad = (row, e, conforme) => {
        e.preventDefault()
        let sendData = {
            cod_solicitud: row.cod_solicitud || 0,
            cod_solicitud_completa: row.cod_solicitud_completa || "",
            cod_flujo_fase_estado_actual: conforme ? 90911 : 90912, 
            cod_flujo_fase_estado_anterior: 90910, 
            nom_actividad: conforme ? 'Registrado - Conforme CEC' : 'Registrado - No Conforme CEC'
        }
        this.props.simpleGenericActivityConfirmation(sendData)
    }
    
    handleSubmitConformidadExistingClient = (row, e, conforme) => {
        e.preventDefault()
        const activity = {
            solicitudeCode: row.cod_solicitud || 0,
            completeSolicitudeCode: row.cod_solicitud_completa || '',
            currentlyPhaseCode: conforme ? 100803 : 100804,
            previousPhaseCode: 100802,
            activityName: conforme ? 'Registrado - Conforme CE' : 'Registrado - No Conforme CE'
        }
        this.props.registerActivity(activity, true)
    }

    render = () => {
        const { row, disabled, classes} = this.props;
        const { fileModal, buttonDisabled, buttonAcceptLoading, buttonNotAcceptLoading } = this.state;
        return (
            <>
                <Tooltip 
                    title={
                            !disabled? `Nro. Solicitud: ${row.cod_solicitud_completa}` : 
                            `Solicitud sin documento adjunto: ${row.cod_solicitud_completa}`
                        } 
                        placement="left">
                    <div>
                        <Fab className={classes.fab} onClick={this.handleClickOpenModalFile} disabled={disabled}>
                            <CheckIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>
                {/* Modal Files */}                      
                <Dialog
                    onClose={this.handleClickCloseModalFile}
                    open={fileModal}
                    maxWidth='xs'
                    aria-labelledby="form-dialog-document">
                    <DialogTitle 
                        id="form-dialog-document" 
                        className="bg-metal-blue">
                        <Typography align="center" component="span" variant="h6" className="text-white text-shadow-black">
                            Confirmacion del Registro
                        </Typography>
                    </DialogTitle>
    
                    <form>
                        <DialogContent>
                            <Grid container spacing={8}>
                                <Grid item xs={12}>
                                    <Typography align="center" >
                                    ¿Se realizó el registro en el Vision Plus  para la solicitud : <br></br><strong> {row.cod_solicitud_completa}</strong>?
                                    </Typography>
                                </Grid>             
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div>
                                        <Button 
                                            disabled={buttonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth={true} 
                                            color="secondary"
                                            size="small" 
                                            onClick={ e => row.flg_cliente_existente 
                                                ? this.handleSubmitConformidadExistingClient(row, e, false)
                                                : this.handleSubmitConformidad(row, e, false) }
                                            margin="normal">
                                            No Conforme
                                            <CancelIcon fontSize="small" className="ml-2" />
                                            {
                                               buttonNotAcceptLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div>
                                        <Button 
                                            disabled={buttonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth={true} 
                                            color="primary"
                                            size="small" 
                                            onClick={ e => row.flg_cliente_existente
                                                ? this.handleSubmitConformidadExistingClient(row, e, true)
                                                : this.handleSubmitConformidad(row, e, true) }
                                            margin="normal">
                                            Conforme
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                               buttonAcceptLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  
            </>  
        )
   }
}    


export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(RegisterActionButton)));
