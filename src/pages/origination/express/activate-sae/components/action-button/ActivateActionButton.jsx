import React, { Component } from "react";
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Draggable from 'react-draggable';
// Components 
import { 
    AppBar,
    Fab, 
    Tooltip, 
    Button,
    withStyles, 
    Dialog,
    DialogTitle,
    Typography,
    DialogContent,
    Grid,
    DialogActions,
    CircularProgress, 
    Paper,  
    IconButton,
    TextField,
    FormHelperText
 } from "@material-ui/core";
// Colors
import green from '@material-ui/core/colors/green';
// Icons 
import SendIcon from '@material-ui/icons/Send';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close'; 
import DoneIcon from '@material-ui/icons/Done';
//import NotInterestedIcon from '@material-ui/icons/NotInterested'; 

// Actions 
import { simpleGenericActivity } from '../../../../../../actions/odc-express/odc-activity';
import { blockTypeCreditCard } from '../../../../../../actions/pmp/pmp';
// Utils
import { onlyTextKeyCode, checkInputKeyCode } from '../../../../../../utils/Utils';
const styles = theme => ({
    modalRoot:{
        minWidth:280, 
        width:350,
        maxWidth:400,
    },
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: green[700],
        '&:hover': {
            backgroundColor: green[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    buttonProgressWrapper:{
        position: 'relative'
    },
    button:{
        textTransform: 'none',
    },
    buttonCustom:{
        color: theme.palette.getContrastText(green[500]),
        '&:hover': {
            backgroundColor: green[700],
        },
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    appBar: {
        position: 'relative',
    }
});

function PaperComponent(props) {
    return (
      <Draggable>
        <Paper {...props} />
      </Draggable>
    );
}

const mapStateToProps = (state, props) => {
    return {
        odc: state.odcReducer,
        odcActivity: state.odcActivityReducer,
        pmp: state.pmpReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        simpleGenericActivity: bindActionCreators(simpleGenericActivity, dispatch), 
        blockTypeCreditCard: bindActionCreators(blockTypeCreditCard, dispatch)
    };
  return actions;
}

class AuthorizeActionButton extends Component {
    state = {
        openAuthorizeModal: false,

        authorizeButtonLoading: false, 
        cancelButtonLoading: false, 
        rejectButtonLoading: false,

        authorizeButtonDisabled: false, 
        cancelButtonDisabled: false,
        rejectButtonDisaled: false,

        acceptRejectButtonDisabled: false,
        acceptRejectButtonLoading: false,

        cancelRejectButtonDisabled: false,
        cancelRejectButtonLoading: false,

        observation: "",
        openObservationModal: false,        
        observationButtonLoading: false, 
        observationButtonDisabled: false, 
        reintentProcessPMP: 0
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(prevProps.odcActivity !== this.props.odcActivity){
           // Simple Generic Activity
            if(prevProps.odcActivity.simpleGenericActivity !== this.props.odcActivity.simpleGenericActivity){
                if(this.props.odcActivity.simpleGenericActivity.loading){
                    this.setState(state => ({
                        authorizeButtonLoading: true, 
                        authorizeButtonDisabled: true, 
                        acceptRejectButtonDisabled: true, 
                        acceptRejectButtonLoading: true,
                        cancelRejectButtonDisabled: true,
                    }));
                }
                if(!this.props.odcActivity.simpleGenericActivity.loading && 
                    this.props.odcActivity.simpleGenericActivity.response && 
                    this.props.odcActivity.simpleGenericActivity.success){
                    this.setState(state => ({
                        ...state,
                        authorizeButtonLoading: false, 
                        authorizeButtonDisabled: false, 
                        openAuthorizeModal: false, 
                        acceptRejectButtonDisabled: false, 
                        cancelRejectButtonDisabled: false,
                        acceptRejectButtonLoading: false,
                        openObservationModal: false,
                        observation: ""
                    }));
                }
                else if(!this.props.odcActivity.simpleGenericActivity.loading && 
                         this.props.odcActivity.simpleGenericActivity.response && 
                        !this.props.odcActivity.simpleGenericActivity.success){
                    this.setState(state => ({
                        ...state,
                        authorizeButtonLoading: false, 
                        authorizeButtonDisabled: false,
                        acceptRejectButtonDisabled: false, 
                        cancelRejectButtonDisabled: false,
                        acceptRejectButtonLoading: false,
                    }));
                }
                else if(!this.props.odcActivity.simpleGenericActivity.loading && 
                        !this.props.odcActivity.simpleGenericActivity.response && 
                        !this.props.odcActivity.simpleGenericActivity.success){
                    this.setState(state => ({
                        ...state,
                        authorizeButtonLoading: false, 
                        authorizeButtonDisabled: false,
                        acceptRejectButtonDisabled: false, 
                        cancelRejectButtonDisabled: false,
                        acceptRejectButtonLoading: false,
                    }));
                }
            }
        }
        // block Type Credit Card
        if(prevProps.pmp.blockTypeCreditCard !==  this.props.pmp.blockTypeCreditCard ){
            if(this.props.pmp.blockTypeCreditCard.loading){
                this.setState(state => ({
                    ...state,
                    authorizeButtonLoading: true, 
                    authorizeButtonDisabled: true, 
                    acceptRejectButtonDisabled: true, 
                    acceptRejectButtonLoading: true,
                    cancelRejectButtonDisabled: true,
                }));
            }
            if(!this.props.pmp.blockTypeCreditCard.loading && this.props.pmp.blockTypeCreditCard.response && this.props.pmp.blockTypeCreditCard.success){                
                this.setState(state => ({
                    ...state,
                    authorizeButtonLoading: false, 
                    authorizeButtonDisabled: false, 
                    openAuthorizeModal: false, 
                    acceptRejectButtonDisabled: false, 
                    cancelRejectButtonDisabled: false,
                    acceptRejectButtonLoading: false,
                    openObservationModal: false,
                    observation: ""
                }));
            }
            else if(!this.props.pmp.blockTypeCreditCard.loading && this.props.pmp.blockTypeCreditCard.response && !this.props.pmp.blockTypeCreditCard.success){
                // Notistack
                this.setState(state => ({
                    ...state,
                    authorizeButtonLoading: false, 
                    authorizeButtonDisabled: false,
                    acceptRejectButtonDisabled: false, 
                    cancelRejectButtonDisabled: false,
                    acceptRejectButtonLoading: false,
                }));
            }
            else if(!this.props.pmp.blockTypeCreditCard.loading && !this.props.pmp.blockTypeCreditCard.response && !this.props.pmp.blockTypeCreditCard.success){
                // Notistack
                this.setState(state => ({
                    ...state,
                    authorizeButtonLoading: false, 
                    authorizeButtonDisabled: false,
                    acceptRejectButtonDisabled: false, 
                    cancelRejectButtonDisabled: false,
                    acceptRejectButtonLoading: false,
                }));
            }
        }
    }

    handleSubmitAuthorizeCreditCardActivation = row => e => {
        e.preventDefault();
        // Send Data
        /*let sendData = {
            organization: "641",
            cod_flujo_fase_estado_actual: 10807, 
            cod_flujo_fase_estado_anterior: row.cod_flujo_fase_estado? row.cod_flujo_fase_estado: 0, 
            nom_actividad: "Activación Tarjeta: PMP Activar SAE",
            blockCode: "A", //A
            process: {
                fullNumber: row.cod_solicitud_completa?  row.cod_solicitud_completa: "",
                number: row.cod_solicitud? row.cod_solicitud: 0
            }, 
            client: {
                id: row.cod_cliente? row.cod_cliente: 0,
                documentTypeAux: row.cod_tipo_documento? row.cod_tipo_documento: "",
                documentNumber: row.des_nro_documento? row.des_nro_documento: "",
                documentTypeInternalValue: row.des_tipo_documento_valor_interno? row.des_tipo_documento_valor_interno: "",
            }, 
            pmp: {
                parallelCardNumber: row.num_tarjeta_pmp,
            }, 
            isSAE: true
        }
        // Set Data Request PMP - Block Type
        let sendDataPMP = this.setDataBlockTypeCreditCardPMP(sendData);
        this.props.blockTypeCreditCard(sendDataPMP)
        .then(response => {
            if(response){
                if(response.data){
                    if(response.data.success){
                         // Send Data
                         let sendData = {
                            cod_solicitud: row.cod_solicitud? row.cod_solicitud: 0,
                            cod_solicitud_completa: row.cod_solicitud_completa?  row.cod_solicitud_completa: "",
                            cod_flujo_fase_estado_actual: 11001, 
                            cod_flujo_fase_estado_anterior: 11010, 
                            nom_actividad: "Fin Proceso SAE",
                        }
                        this.props.simpleGenericActivity(sendData, null, false)
                    }
                }
            }
        });*/

        let sendData = {
            cod_solicitud: row.cod_solicitud? row.cod_solicitud: 0,
            cod_solicitud_completa: row.cod_solicitud_completa?  row.cod_solicitud_completa: "",
            cod_flujo_fase_estado_actual: 11010, 
            cod_flujo_fase_estado_anterior: 11001, 
            nom_actividad: "Pendiente Atención SAE",
        }
        this.props.simpleGenericActivity(sendData, null, false)
        .then(response => {
            if(response) {
                if(response.data) {
                    if(response.data.success) {
                        let sendData = {
                            organization: "641",
                            cod_flujo_fase_estado_actual: 10807, 
                            cod_flujo_fase_estado_anterior: 11010,//row.cod_flujo_fase_estado? row.cod_flujo_fase_estado: 0, 
                            nom_actividad: "Activación Tarjeta: PMP Activar SAE",
                            blockCode: "A", //A
                            process: {
                                fullNumber: row.cod_solicitud_completa?  row.cod_solicitud_completa: "",
                                number: row.cod_solicitud? row.cod_solicitud: 0
                            }, 
                            client: {
                                id: row.cod_cliente? row.cod_cliente: 0,
                                documentTypeAux: row.cod_tipo_documento? row.cod_tipo_documento: "",
                                documentNumber: row.des_nro_documento? row.des_nro_documento: "",
                                documentTypeInternalValue: row.des_tipo_documento_valor_interno? row.des_tipo_documento_valor_interno: "",
                            }, 
                            pmp: {
                                parallelCardNumber: row.num_tarjeta_pmp,
                            }, 
                            isSAE: true
                        }
                        // Set Data Request PMP - Block Type
                        let sendDataPMP = this.setDataBlockTypeCreditCardPMP(sendData);
                        this.props.blockTypeCreditCard(sendDataPMP)
                        .then(response => {
                            if(response) {
                                if(response.data) {
                                    if(response.data.success) {
                                        let sendData = {
                                            cod_solicitud: row.cod_solicitud? row.cod_solicitud: 0,
                                            cod_solicitud_completa: row.cod_solicitud_completa?  row.cod_solicitud_completa: "",
                                            cod_flujo_fase_estado_actual: 11011 , 
                                            cod_flujo_fase_estado_anterior: 10807, 
                                            nom_actividad: "Activado",
                                        }
                                        this.props.simpleGenericActivity(sendData, null, false)
                                    }
                                }
                            }
                        })
                    }
                }
            }
        })
        //window.location.reload()
    }

    handleClickAuthorizeCreditCardActivationToggleModal = e => {
        this.setState(state => ({
            ...state,
            openAuthorizeModal: !state.openAuthorizeModal
        }));
    }

    handleClickObservationToggleModal = e => {
        this.setState(state => ({
            openObservationModal: !state.openObservationModal
        }));
    }

    // Client - TextField - Event Change - Required    
    handleChangeTextFieldRequired = name => e => {
        e.persist();
        let { value } = e.target;
        this.setState(state => ({
            ...state,
            [name]: value, 
        }));
    }
    // Only Text
    handleKeyPressTextFieldOnlyText = name => e =>{
        let code = (e.which) ? e.which : e.keyCode;
        if(!onlyTextKeyCode(code)){
            e.preventDefault();
        }       
    }
    // Check Input
    handleKeyPressTextFieldCheckInput = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!checkInputKeyCode(code)){
            e.preventDefault();
        }   
    }


    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    // Set Data Block Type Credit Card PMP
    setDataBlockTypeCreditCardPMP = data => {
        let { client, pmp, process, isSAE=false } = data;
        return {
            tipo_doc_letra: client.documentType,
            nom_actividad: data.nom_actividad,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            cod_solicitud: process.number,
            cod_solicitud_completa: process.fullNumber,
            cod_cliente: client? client.id: 0, 
            cod_cliente_titular: client? client.id: 0,
            terceraLlamadaPmpTipoBloqueTarjeta: {
                tipo_doc: client.documentTypeAux,
                nro_doc: client.documentNumber,
                organization: data.organization,
                identityDocumentType: client.documentTypeInternalValue,
                identityDocumentNumber: client.documentNumber,
                isoMessageTypeActivar: "0",
                transactionNumberActivar: "0",
                cardNumberActivar: isSAE? pmp.parallelCardNumber: pmp.cardNumber,
                cardSequenceActivar:"0001",
                amedBlockCodeActivar: data.blockCode, // Code Block
                amedCardActionActivar:"0",
                amedRqtdCardTypeActivar:"0",
                amedCurrFirstUsageFlagActivar:"",
                amedPriorFirstUsageFlagActivar:"",
                amedMotiveBlockadeActivar:"01",
                internacionUsaRegionActivar:"",
                boletineoInterUsaDateActivar:"0",
                interCanRegionActivar:"",
                boletineoInterCanDateActivar:"0",
                internacionalCAmerRegionActivar:"",
                boletineoInterCAmerRegionActivar:"0",
                interAsiaRegionActivar:"",
                bolitineoInterAsiaDateActivar:"0",
                interRegionEuropaActivar:"",
                bolitineoInterDateEuropaActivar:"0",
                interEuropaRegionActivar:"",
                bolitineoInterEuropaDateActivar:"0",
                principalAccountActivar:"",
                documentTypeActivar:"",
                documentNumerActivar:"",
                clientNameActivar:"",
                clientAddressActivar:"",
                birthDateActivar:"",
                telephoneActivar:"0",
                cardNameActivar:"",
                cardAddressActivar:"",
                cardTypeActivar:"",
                applicantNameActivar:"",
                applicantDocumentActivar:"",
                applicantTelephoneActivar:"0",
                relationshipActivar:"",
                blockadeUserActivar:"",
                blockadeNumberActivar:"0",
                blockadeDateActivar:"0",
                blockadeTimeActivar:"0",
                userInitialsActivar:"",
                emisionTypeActivar:""
            }
        }
    }
    render = () => {
        const { classes, row, disabled } = this.props;
        let { 
            openAuthorizeModal, authorizeButtonLoading, 
            authorizeButtonDisabled, cancelButtonDisabled,
            openObservationModal, observation
        } = this.state;
        return  (
            <React.Fragment>
                <Tooltip title={`Activar SAE - Solicitud: ${row.cod_solicitud_completa}`} placement="left">
                    <div>
                        <Fab className={classes.fab} onClick={this.handleClickAuthorizeCreditCardActivationToggleModal} disabled={disabled}>
                            <DoneIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>  
                            
                <Dialog
                    onClose={this.handleClickAuthorizeCreditCardActivationToggleModal}
                    open={openAuthorizeModal}
                    PaperComponent={PaperComponent}
                    aria-labelledby="form-authorize">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-authorize" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                Activación SAE.
                            </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form  onSubmit={this.handleSubmitAuthorizeCreditCardActivation(row)} className={classes.modalRoot}>
                        <DialogContent>
                            <Typography align="center">
                                ¿Desea realizar la activación del SAE para la solicitud: <strong> {row.cod_solicitud_completa}</strong>?
                            </Typography>
                        </DialogContent>
                        <DialogActions>
                            <Grid container >
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            disabled={cancelButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickAuthorizeCreditCardActivationToggleModal}
                                            margin="normal">
                                            Cancelar
                                            <CancelIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                                {/* Opcion "Rechazar" */}
                                {/* <Grid item xs={4}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            //disabled={rejectButtonDisaled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickObservationToggleModal}
                                            margin="normal">
                                            Rechazar
                                            <NotInterestedIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid> */}
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={authorizeButtonDisabled}
                                            className={classNames(classes.button)}
                                            type="submit" 
                                            margin="normal"
                                            color="primary" 
                                            size="small"
                                            fullWidth>
                                            Activar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                               authorizeButtonLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>

                {/* Modal de observacion para Rechazo */}
                <Dialog
                    //onClose={this.handleClickObservationToggleModal}
                    open={openObservationModal}
                    //PaperComponent={PaperComponent}
                    aria-labelledby="form-observation">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-observation" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                Observaciones de Rechazo               
                            </Typography>
                        </DialogTitle>
                    </AppBar>

                    <form method="post" /*onSubmit={this.handleSubmitReject(row)}*/ className={classes.modalRoot}>
                        <DialogContent>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    autoFocus
                                    fullWidth
                                    multiline
                                    rows="4"
                                    label="Observaciones"
                                    type="text"
                                    margin="normal"
                                    color="default"
                                    name="observation"
                                    variant="outlined"
                                    value={observation}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    InputProps={{
                                        inputProps:{
                                            maxLength: 300,
                                        },
                                    }}
                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("observation")}
                                    onChange={this.handleChangeTextFieldRequired("observation")}
                                />
                                <FormHelperText className="d-flex justify-content-end">
                                    {observation.toString().length}/300
                                </FormHelperText>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            //disabled={cancelRejectButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickObservationToggleModal}
                                            margin="normal">
                                            Cancelar
                                            <CancelIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            //disabled={acceptRejectButtonDisabled}
                                            className={classNames(classes.button)}
                                            type="submit" 
                                            margin="normal"
                                            color="primary" 
                                            size="small"
                                            fullWidth>
                                            Aceptar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {/*
                                               acceptRejectButtonLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            */}
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>

            </React.Fragment>
        )        
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(AuthorizeActionButton)));
