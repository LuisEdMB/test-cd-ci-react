import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
// React Router 
import { connect } from 'react-redux';
// Component
import {
    IconButton,
    Grid,
    LinearProgress,
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close';
// Components Custom 
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
import Header from './components/Header';
// Actions 
import { pendingActivateSAE } from '../../../../actions/odc-express/odc';
import { bindActionCreators } from "redux";
// Utils
import * as Utils from '../../../../utils/Utils'

const styles = theme => ({
    avatar: {
        width: 80,
        height: 48
    },
    modalRoot: {

    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state) => {
    return {
        odc: state.odcReducer,
        odcActivity: state.odcActivityReducer,
        pmp: state.pmpReducer,
        odcExistingClient: state.odcExistingClientReducer
    }
}
const mapDispatchToProps = (dispatch) => {
    const actions = {
        pendingActivateSAE: bindActionCreators(pendingActivateSAE, dispatch)
    };
    return actions;
}

class ActivateSAEPage extends Component {

    state = {
        defaultColumnWidths: [
            { columnName: 'cod_solicitud', width: 120 },
            { columnName: 'cod_solicitud_completa', width: 125 },
            { columnName: 'des_tipo_documento', width: 100 },
            { columnName: 'des_nro_documento', width: 125 },
            { columnName: 'des_nombre_completo', width: 300 },
            { columnName: 'des_jerarquias_flujo_fase_estado', width: 300 },
            { columnName: 'num_tarjeta_pmp', width: 170 },
            { columnName: 'monto_cci', width: 200 },
            { columnName: 'des_cci', width: 200 },
            { columnName: 'cuota', width: 120 },
            { columnName: 'des_entidad_financiera', width: 200 },
            { columnName: 'fec_reg', width: 150 },
            { columnName: 'des_usu_reg', width: 180 },
            { columnName: 'cod_agencia', width: 120 },
            { columnName: 'des_agencia', width: 150 },

            { columnName: 'registrado', width: 120 },
            { columnName: 'authorize', width: 120 },
            { columnName: 'attached', width: 120 },
            { columnName: 'observation', width: 150 },
        ],
        columns: [],
        tableColumnExtensions: [
            { columnName: 'authorize', align: 'center' },
            { columnName: 'attached', align: 'center' },
            { columnName: 'monto_cci', align: 'center' },
            { columnName: 'des_usu_reg', align: 'center' },
            { columnName: 'cod_agencia', align: 'center' },
            { columnName: 'registrado', align: 'center' }
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        additional: 0,
        reprint: 0,
        express: 0,
        pageSizes: [5, 10, 15, 20, 0],
        columnExcel: [],
        showHideSearchInput: false,
        openSearchModal: false
    };
    componentDidUpdate = (prevProps) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }

        Utils.resolveStateRedux(prevProps.odcExistingClient.activityRegistered, this.props.odcExistingClient.activityRegistered,
            null, _ => {
                this.getNotistack('Fin Proceso Crédito Efectivo Cencosud', 'success')
                let sendData = {
                    cod_solicitud_completa: '',
                    des_nro_documento: ''
                }
                this.props.pendingActivateSAE(sendData)
            },
            warningMessage => this.getNotistack(warningMessage, 'warning'),
            errorMessage => this.getNotistack(errorMessage, 'error'))

        if (prevProps.odc !== this.props.odc) {
            if (prevProps.odc.pendingActivateSAE !== this.props.odc.pendingActivateSAE) {
                if (!this.props.odc.pendingActivateSAE.loading &&
                    this.props.odc.pendingActivateSAE.response &&
                    this.props.odc.pendingActivateSAE.success) {
                    this.setState(state => ({
                        ...state,
                        rows: this.props.odc.pendingActivateSAE.data
                    }));
                }
                else if (!this.props.odc.pendingActivateSAE.loading &&
                    this.props.odc.pendingActivateSAE.response &&
                    !this.props.odc.pendingActivateSAE.success) {
                    this.setState(state => ({
                        ...state,
                        rows: []
                    }));
                    // Notistack
                    this.getNotistack(this.props.odc.pendingActivateSAE.error, "error");
                }
                else if (!this.props.odc.pendingActivateSAE.loading &&
                    !this.props.odc.pendingActivateSAE.response &&
                    !this.props.odc.pendingActivateSAE.success) {
                    // Notistack
                    this.getNotistack(this.props.odc.pendingActivateSAE.error, "error");
                    this.setState(state => ({
                        ...state,
                        rows: []
                    }));
                }
            }
        }

        Utils.resolveStateRedux(
            prevProps.odcActivity.simpleGenericActivityConfirmation,
            this.props.odcActivity.simpleGenericActivityConfirmation,
            null, _ => {
                this.getNotistack('Fin Proceso Crédito Efectivo Cencosud', 'success')
                let sendData = {
                    cod_solicitud_completa: '',
                    des_nro_documento: ''
                }
                this.props.pendingActivateSAE(sendData)
            },
            warningMessage => this.getNotistack(warningMessage, 'warning'),
            errorMessage => this.getNotistack(errorMessage, 'error'))

        if (prevProps.pmp !== this.props.pmp) {
            // block Type Credit Card
            if (prevProps.pmp.blockTypeCreditCard !== this.props.pmp.blockTypeCreditCard) {
                if (!this.props.pmp.blockTypeCreditCard.loading && this.props.pmp.blockTypeCreditCard.response && this.props.pmp.blockTypeCreditCard.success) {
                    this.getNotistack("Originación Express: PMP Activar Efectivo Cencosud Ok!", "success");
                    let sendData = {
                        cod_solicitud_completa: "",
                        des_nro_documento: ""
                    }
                    this.props.pendingActivateSAE(sendData);
                }
                else if (!this.props.pmp.blockTypeCreditCard.loading && this.props.pmp.blockTypeCreditCard.response && !this.props.pmp.blockTypeCreditCard.success) {
                    // Notistack
                    this.getNotistack(this.props.pmp.blockTypeCreditCard.error, "error");
                }
                else if (!this.props.pmp.blockTypeCreditCard.loading && !this.props.pmp.blockTypeCreditCard.response && !this.props.pmp.blockTypeCreditCard.success) {
                    // Notistack
                    this.getNotistack(this.props.pmp.blockTypeCreditCard.error, "error");
                }
            }
        }
    }

    componentWillMount() {
        let columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns: columns
        }));
    }
    componentDidMount() {
        let sendData = {
            cod_solicitud_completa: "",
            des_nro_documento: ""
        }
        this.props.pendingActivateSAE(sendData);
    }

    getColumnsRender() {
        let columns = [];
        if (this.props.width === "xs") {
            columns = [
                //{ name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                //{ name: 'attached', title: 'Descargar' },
                // { name: 'authorize', title: 'Activar' }
            ]
        }
        else if (this.props.width === "sm") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                // { name: 'authorize', title: 'Activar' },
                { name: 'registrado', title: 'Registrado' }
            ]
        }
        else if (this.props.width === "md") {
            columns = [
                //{ name: 'attached', title: 'Descargar' },
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro Documento ' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                // { name: 'authorize', title: 'Activar' },
                { name: 'registrado', title: 'Registrado' }
            ]
        }
        else if (this.props.width === "lg") {
            columns = [
                //{ name: 'attached', title: 'Descargar' },
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Nombres y Apellidos del cliente' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
                { name: 'num_tarjeta_pmp', title: 'Efectivo Cencosud' },
                { name: 'des_cci', title: 'Número CCI' },
                { name: 'des_entidad_financiera', title: 'Banco' },
                { name: 'monto_cci', title: 'Importe de desembolso' },
                { name: 'cuota', title: 'Nro. Cuotas' },
                { name: 'des_usu_reg', title: 'Cód. Usuario de registro' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                // { name: 'authorize', title: 'Activar' },
                { name: 'registrado', title: 'Registrado' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        else if (this.props.width === "xl") {
            columns = [
                //{ name: 'attached', title: 'Descargar' },
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Nombres y Apellidos del cliente' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
                { name: 'num_tarjeta_pmp', title: 'Efectivo Cencosud' },
                { name: 'des_cci', title: 'Número CCI' },
                { name: 'des_entidad_financiera', title: 'Banco' },
                { name: 'monto_cci', title: 'Importe de desembolso' },
                { name: 'cuota', title: 'Nro. Cuotas' },
                { name: 'des_usu_reg', title: 'Cód. Usuario de registro' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                // { name: 'authorize', title: 'Activar' },
                { name: 'registrado', title: 'Registrado' },
                { name: 'observation', title: 'Ver Comentario' },


            ]
        }
        return columns;
    }

    handleShowHideSearchInput = e => {
        this.setState(state => ({
            showHideSearchInput: !state.showHideSearchInput
        }));
    }
    handleClickOpenSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: true
        }));
    }
    handleClickCloseSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: false
        }));
    }

    // Notistack 
    getNotistack(message, variant = "default", duration = 6000) {
        let select = "default";
        switch (variant) {
            case "error":
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default:
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit" />
                </IconButton>
            ),
        });
    }
    render() {
        const { width, odc } = this.props;
        const {
            columns,
            defaultColumnWidths,
            defaultHiddenColumnNames,
            rows,
            tableColumnExtensions,
            //disabled
        } = this.state;
        return (
            <Grid container className="p-1">
                <Grid item xs={12} className="">
                    {
                        odc.pendingActivateSAE.loading ? <LinearProgress /> : ""
                    }
                </Grid>
                <Grid item xs={12} className="mb-2">
                    <Header
                        title="Bandeja de Registro Efectivo Cencosud"
                    />
                </Grid>

                <Grid item xs={12}>
                    <DevGridComponent
                        rows={rows}
                        columns={columns}
                        width={width}
                        search={true}
                        columnExtensions={tableColumnExtensions}
                        defaultColumnWidths={defaultColumnWidths}
                        defaultHiddenColumnNames={defaultHiddenColumnNames}
                        RowDetailComponent={RowDetail}
                        CellComponent={Cell} />
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(ActivateSAEPage))));

