import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";
import withWidth from "@material-ui/core/withWidth"; //{ isWidthUp, isWidthDown }
// React Router
import { connect } from "react-redux";
// Component
import { IconButton, Grid, LinearProgress } from "@material-ui/core";
// Icons
import CloseIcon from "@material-ui/icons/Close";
// Components Custom
import DevGridComponent from "../../../../components/dev-grid/DevGridComponent";
import Cell from "./components/table/Cell";
import RowDetail from "./components/table/RowDetail";
import Header from "./components/Header";
// Actions
import { pendingProcessesActivation } from "../../../../actions/odc-express/odc";
import { bindActionCreators } from "redux";
// Utils
import * as Utils from '../../../../utils/Utils'

const styles = (theme) => ({
  avatar: {
    width: 80,
    height: 48,
  },
  modalRoot: {},
  button: {
    textTransform: "none",
  },
});

const mapStateToProps = (state) => {
  return {
    odc: state.odcReducer,
    odcRegular: state.odcRegularReducer,
    odcExistingClient: state.odcExistingClientReducer,
    pmp: state.pmpReducer,
    pci: state.pciReducer,
    odcMaster: state.odcMasterReducer
  };
};
const mapDispatchToProps = (dispatch) => {
  const actions = {
    pendingProcessesActivation: bindActionCreators(
      pendingProcessesActivation,
      dispatch
    ),
  };
  return actions;
};

class AuthorizePage extends Component {
  state = {
    defaultColumnWidths: [
      { columnName: "cod_solicitud", width: 120 },
      { columnName: "cod_solicitud_completa", width: 125 },
      { columnName: "des_nro_documento", width: 125 },
      { columnName: "des_nombre_completo", width: 150 },
      { columnName: "des_jerarquias_flujo_fase_estado", width: 300 },
      { columnName: "fec_reg_solicitud", width: 150 },
      { columnName: "des_usu_reg_solicitud", width: 150 },
      { columnName: "agencia", width: 120 },
      { columnName: "authorize", width: 120 },
      { columnName: "observation", width: 150 },
    ],
    columns: [],
    tableColumnExtensions: [
      { columnName: "authorize", align: "center" },
      { columnName: "observation", align: "center" },
    ],
    defaultHiddenColumnNames: [],
    rows: [],
    additional: 0,
    reprint: 0,
    express: 0,
    pageSizes: [5, 10, 15, 20, 0],
    columnExcel: [],
    showHideSearchInput: false,
    openSearchModal: false,
  };
  componentDidUpdate = (prevProps) => {
    if (prevProps.width !== this.props.width) {
      let columns = this.getColumnsRender();
      this.setState((state) => ({
        ...state,
        columns: columns,
      }));
    }
    Utils.resolveStateRedux(prevProps.odcExistingClient.activityRegistered, this.props.odcExistingClient.activityRegistered, 
      null, _ => {
        const { cod_solicitud_completa } = this.props.odcExistingClient.activityRegistered.data.actividadGenerico
        this.getNotistack(`Originación Express: Solicitud ${ cod_solicitud_completa } - Autorizado.`, 'success')
        let sendData = {
          cod_solicitud_completa: "",
          des_nro_documento: "",
        };
        this.props.pendingProcessesActivation(sendData);
      }, warningMessage => this.getNotistack(warningMessage, 'warning'),
         errorMessage => this.getNotistack(errorMessage, 'warning')
    )
    Utils.resolveStateRedux(prevProps.odcExistingClient.activityRejectedRegistered, this.props.odcExistingClient.activityRejectedRegistered, 
      null, _ => {
        this.getNotistack('Rechazar Originación Efectivo: Cancelar EC Ok!', 'success')
        let sendData = {
          cod_solicitud_completa: "",
          des_nro_documento: "",
        }
        this.props.pendingProcessesActivation(sendData)
      }, warningMessage => this.getNotistack(warningMessage, 'warning'),
         errorMessage => this.getNotistack(errorMessage, 'warning')
    )
    if (prevProps.odc !== this.props.odc) {
      if (
        prevProps.odc.pendingProcessesActivation !==
        this.props.odc.pendingProcessesActivation
      ) {
        if (
          !this.props.odc.pendingProcessesActivation.loading &&
          this.props.odc.pendingProcessesActivation.response &&
          this.props.odc.pendingProcessesActivation.success
        ) {
          this.setState((state) => ({
            ...state,
            rows: this.props.odc.pendingProcessesActivation.data,
          }));
        } else if (
          !this.props.odc.pendingProcessesActivation.loading &&
          this.props.odc.pendingProcessesActivation.response &&
          !this.props.odc.pendingProcessesActivation.success
        ) {
          this.setState((state) => ({
            ...state,
            rows: [],
          }));
          // Notistack
          this.getNotistack(
            this.props.odc.pendingProcessesActivation.error,
            "error"
          );
        } else if (
          !this.props.odc.pendingProcessesActivation.loading &&
          !this.props.odc.pendingProcessesActivation.response &&
          !this.props.odc.pendingProcessesActivation.success
        ) {
          // Notistack
          this.getNotistack(
            this.props.odc.pendingProcessesActivation.error,
            "error"
          );
          this.setState((state) => ({
            ...state,
            rows: [],
          }));
        }
      }

      if (
        prevProps.odc.authorizeActivationProcess !==
        this.props.odc.authorizeActivationProcess
      ) {
        if (
          !this.props.odc.authorizeActivationProcess.loading &&
          this.props.odc.authorizeActivationProcess.response &&
          this.props.odc.authorizeActivationProcess.success
        ) {
          if (this.props.odc.authorizeActivationProcess.data) {
            const { data } = this.props.odc.authorizeActivationProcess;
            if (data) {
              const { actividadGenerico } = data;
              if (actividadGenerico) {
                this.getNotistack(
                  `Originación Express: Solicitud ${actividadGenerico.cod_solicitud_completa} - Autorizado.`,
                  "success"
                );
                let sendData = {
                  cod_solicitud_completa: "",
                  des_nro_documento: "",
                };
                this.props.pendingProcessesActivation(sendData);
              }
            }
          }
        } else if (
          !this.props.odc.authorizeActivationProcess.loading &&
          this.props.odc.authorizeActivationProcess.response &&
          !this.props.odc.authorizeActivationProcess.success
        ) {
          this.getNotistack(
            this.props.odc.authorizeActivationProcess.error,
            "error"
          );
        } else if (
          !this.props.odc.authorizeActivationProcess.loading &&
          !this.props.odc.authorizeActivationProcess.response &&
          !this.props.odc.authorizeActivationProcess.success
        ) {
          this.getNotistack(
            this.props.odc.authorizeActivationProcess.error,
            "error"
          );
        }
      }
    }
    if (prevProps.odcRegular !== this.props.odcRegular) {
      if (
        prevProps.odcRegular.authorizeActivationProcess !==
        this.props.odcRegular.authorizeActivationProcess
      ) {
        if (
          !this.props.odcRegular.authorizeActivationProcess.loading &&
          this.props.odcRegular.authorizeActivationProcess.response &&
          this.props.odcRegular.authorizeActivationProcess.success
        ) {
          if (this.props.odcRegular.authorizeActivationProcess.data) {
            const { data } = this.props.odcRegular.authorizeActivationProcess;
            if (data) {
              const { actividadGenerico } = data;
              if (actividadGenerico) {
                this.getNotistack(
                  `Originación Regular: Solicitud ${actividadGenerico.cod_solicitud_completa} - Autorizado.`,
                  "success"
                );
                let sendData = {
                  cod_solicitud_completa: "",
                  des_nro_documento: "",
                };
                this.props.pendingProcessesActivation(sendData);
              }
            }
          }
        } else if (
          !this.props.odcRegular.authorizeActivationProcess.loading &&
          this.props.odcRegular.authorizeActivationProcess.response &&
          !this.props.odcRegular.authorizeActivationProcess.success
        ) {
          this.getNotistack(
            this.props.odcRegular.authorizeActivationProcess.error,
            "error"
          );
        } else if (
          !this.props.odcRegular.authorizeActivationProcess.loading &&
          !this.props.odcRegular.authorizeActivationProcess.response &&
          !this.props.odcRegular.authorizeActivationProcess.success
        ) {
          this.getNotistack(
            this.props.odcRegular.authorizeActivationProcess.error,
            "error"
          );
        }
      }
    }
    if (prevProps.pmp !== this.props.pmp) {
      // block Type Credit Card
      if (
        prevProps.pmp.blockTypeCreditCard !== this.props.pmp.blockTypeCreditCard
      ) {
        if (
          !this.props.pmp.blockTypeCreditCard.loading &&
          this.props.pmp.blockTypeCreditCard.response &&
          this.props.pmp.blockTypeCreditCard.success
        ) {
          this.getNotistack("Rechazar Originación: PMP Cancelar Ok!", "success")
        } else if (
          !this.props.pmp.blockTypeCreditCard.loading &&
          this.props.pmp.blockTypeCreditCard.response &&
          !this.props.pmp.blockTypeCreditCard.success
        ) {
          // Notistack
          this.getNotistack(this.props.pmp.blockTypeCreditCard.error, "error");
        } else if (
          !this.props.pmp.blockTypeCreditCard.loading &&
          !this.props.pmp.blockTypeCreditCard.response &&
          !this.props.pmp.blockTypeCreditCard.success
        ) {
          // Notistack
          this.getNotistack(this.props.pmp.blockTypeCreditCard.error, "error");
        }
      }
    }
    Utils.resolveStateRedux(prevProps.pci.cardnumberDecrypted, this.props.pci.cardnumberDecrypted, null,
      _ => this.getNotistack('Rechazar Originación: PCI Desencriptar OK!', 'success'),
      warningMessage => this.getNotistack(`Rechazar Originación: PCI Desencriptar - ${ warningMessage }`, 'warning'),
      errorMessage => this.getNotistack(`Rechazar Originación: PCI Desencriptar - ${ errorMessage }`, 'error')
    )
    Utils.resolveStateRedux(prevProps.odcMaster.blockingCodeCreditCardUpdated,
      this.props.odcMaster.blockingCodeCreditCardUpdated, null,
      _ => {
        this.getNotistack('Rechazar Originación: Cambio Bloqueo Maestro OK!', 'success') 
        this.props.pendingProcessesActivation({
          cod_solicitud_completa: "",
          des_nro_documento: "",
        })
      },
      warningMessage => this.getNotistack(`Rechazar Originación: Cambio Bloqueo Maestro - ${ warningMessage }`, 'warning'),
      errorMessage => this.getNotistack(`Rechazar Originación: Cambio Bloqueo Maestro - ${ errorMessage }`, 'error')
    )
  };

  componentWillMount() {
    let columns = this.getColumnsRender();
    this.setState((state) => ({
      ...state,
      columns: columns,
    }));
  }
  componentDidMount() {
    let sendData = {
      cod_solicitud_completa: "",
      des_nro_documento: "",
    };
    this.props.pendingProcessesActivation(sendData);
  }

  getColumnsRender() {
    let columns = [];
    if (this.props.width === "xs") {
      columns = [
        { name: "cod_solicitud_completa", title: "Nro Solicitud" },
        { name: "des_nro_documento", title: "Nro Documento" },
        { name: "authorize", title: "Autorizar" },
      ];
    } else if (this.props.width === "sm") {
      columns = [
        { name: "cod_solicitud_completa", title: "Nro Solicitud" },
        { name: "des_nro_documento", title: "Nro Documento" },
        { name: "authorize", title: "Autorizar" },
      ];
    } else if (this.props.width === "md") {
      columns = [
        { name: "cod_solicitud_completa", title: "Nro Solicitud" },
        { name: "des_nro_documento", title: "Nro Documento " },
        {
          name: "des_jerarquias_flujo_fase_estado",
          title: "Flujo - Fase - Estado",
        },
        { name: "fec_reg_solicitud", title: "Fecha Registro" },
        { name: "authorize", title: "Autorizar" },
        { name: "observation", title: "Ver Comentario" },
      ];
    } else if (this.props.width === "lg") {
      columns = [
        { name: "cod_solicitud_completa", title: "Nro Solicitud" },
        { name: "des_nro_documento", title: "Nro Documento" },
        { name: "des_nombre_completo", title: "Cliente" },
        {
          name: "des_jerarquias_flujo_fase_estado",
          title: "Flujo - Fase - Estado",
        },
        { name: "fec_reg_solicitud", title: "Fecha Registro" },
        { name: "des_usu_reg_solicitud", title: "Usuario Registra" },
        { name: "authorize", title: "Autorizar" },
        { name: "observation", title: "Ver Comentario" },
      ];
    } else if (this.props.width === "xl") {
      columns = [
        { name: "cod_solicitud_completa", title: "Nro Solicitud" },
        { name: "des_nro_documento", title: "Nro Documento" },
        { name: "des_nombre_completo", title: "Cliente" },
        {
          name: "des_jerarquias_flujo_fase_estado",
          title: "Flujo - Fase - Estado",
        },
        { name: "fec_reg_solicitud", title: "Fecha Registro" },
        { name: "des_usu_reg_solicitud", title: "Usuario Registra" },
        { name: "authorize", title: "Autorizar" },
        { name: "observation", title: "Ver Comentario" },
      ];
    }
    return columns;
  }

  handleShowHideSearchInput = (e) => {
    this.setState((state) => ({
      showHideSearchInput: !state.showHideSearchInput,
    }));
  };
  handleClickOpenSearchModal = (e) => {
    this.setState((state) => ({
      ...state,
      openSearchModal: true,
    }));
  };
  handleClickCloseSearchModal = (e) => {
    this.setState((state) => ({
      ...state,
      openSearchModal: false,
    }));
  };

  // Notistack
  getNotistack(message, variant = "default", duration = 6000) {
    let select = "default";
    switch (variant) {
      case "error":
        select = variant;
        break;
      case "success":
        select = variant;
        break;
      case "warning":
        select = variant;
        break;
      case "info":
        select = variant;
        break;
      default:
        select = variant;
        break;
    }
    // Notistack
    this.props.enqueueSnackbar(message, {
      variant: select,
      autoHideDuration: duration,
      action: (
        <IconButton>
          <CloseIcon size="small" className="text-white" color="inherit" />
        </IconButton>
      ),
    });
  }
  render() {
    const { width } = this.props;
    const {
      columns,
      defaultColumnWidths,
      defaultHiddenColumnNames,
      rows,
      tableColumnExtensions,
    } = this.state;
    return (
      <Grid container className="p-1">
        <Grid item xs={12} className="">
          {this.props.odc.pendingProcessesActivation.loading ? <LinearProgress /> : ""}
        </Grid>
        <Grid item xs={12} className="mb-2">
          <Header title="Bandeja de Aprobación de Originación(es)" />
        </Grid>

        <Grid item xs={12}>
          <DevGridComponent
            rows={rows}
            columns={columns}
            width={width}
            search={true}
            columnExtensions={tableColumnExtensions}
            defaultColumnWidths={defaultColumnWidths}
            defaultHiddenColumnNames={defaultHiddenColumnNames}
            RowDetailComponent={RowDetail}
            CellComponent={Cell}
          />
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(
  withSnackbar(
    connect(mapStateToProps, mapDispatchToProps)(withWidth()(AuthorizePage))
  )
);
