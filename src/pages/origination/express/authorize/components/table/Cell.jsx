import React from "react";
import * as moment from "moment";
import { Table } from "@devexpress/dx-react-grid-material-ui";
import AuthorizeActionButton from "../action-button/AuthorizeActionButton";
import AuthorizeRegularActionButton from "../action-button/AuthorizeRegularActionButton";
import NewCommentForm from "../../../../../../components/NewCommentForm/NewCommentForm";

const Cell = ({ ...props }) => {
  if (props.column.name === "fec_reg_solicitud") {
    const value = props.value
      ? moment(props.value.replace("T", " ")).format("DD/MM/YYYY HH:mm")
      : "";
    return <Table.Cell {...props} value={value} />;
  } else if (props.column.name === "authorize") {
    return (
      <Table.Cell {...props}>
        {props.row.cod_tipo_solicitud_originacion === "390001" ? (
          <AuthorizeActionButton {...props} />
        ) : (
          <AuthorizeRegularActionButton {...props} />
        )}
      </Table.Cell>
    );
  }
  if (props.column.name === "observation") {
    return (
      <Table.Cell {...props}>
        <NewCommentForm {...props} />
      </Table.Cell>
    );
  }

  return <Table.Cell {...props} />;
};

export default Cell;
