﻿import React, { Component } from 'react';
import classNames from 'classnames';
import { withRouter, Redirect, } from 'react-router-dom';
// Shorcurt
import Hotkeys from 'react-hot-keys';
// Notistack
import { SnackbarProvider } from 'notistack';
// Redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components
import VerifyEmail from '../../../components/VerifyEmail';
import {
    withStyles,
    Stepper,
    Step,
    StepLabel,
    StepContent,
    Button,
    Paper,
    Typography,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Grid,
    CircularProgress
} from '@material-ui/core';
import Timer from '../../../components/Timer'
//Steps
import PreLoaderImage from '../../../components/PreLoaderImage';
import ConsultClient from './components/consult-client/ConsultClient';
import ClientDetailPreEvaluated from './components/client-detail-pre-evaluated/ClientDetailPreEvaluated';
import ClientRegister from './components/client-register/ClientRegister';
import CommercialOffer from './components/commercial-offer/CommercialOffer';
import ProductDetail from './components/product-detail/ProductDetail';
import OfferSummary from './components/offer-summary/OfferSummary';
import EmbossingCreditCard from './components/embossing-creditcard/EmbossingCreditCard';
import ActivationCreditCard from './components/activation-creditcard/ActivationCreditCard';
import BackgroundFinish from '../../../components/background-finish/BackgroundFinish';
import HistoryPanel from './components/history-panel/HistoryPanel';
import NewCommentForm from '../../../components/NewCommentForm/NewCommentForm'
import { loadComment } from '../../../actions/odc-express/odc'
// Actions
import { getConstantODC } from '../../../actions/generic/constant';
import {
    firstCall,
    secondCall,
    offerSummary,
    commercialOffer,
    pendingEmbossing,
    continueProcess,
    verifySolicitudeIsBlocked,
    blockSolicitude,
    unlockSolicitude
} from '../../../actions/odc-express/odc';
import { cancelActivity, genericActivity, simpleGenericActivity } from '../../../actions/odc-express/odc-activity';
import { biometricZytrustService } from '../../../actions/generic/zytrust';

import {
    createClient,
    updateClient,
    createRelationship,
    createAccount,
    blockTypeCreditCard,
    createCreditCard
} from '../../../actions/pmp/pmp';
import * as ActionPci from '../../../actions/pci/pci'
import * as ActionOdcMaster from '../../../actions/odc-master/odc-master'
// Utils
import * as Constants from '../../../utils/Constants'
import { SmoothScroll, evaluatePrevaluated, cencosudEffectiveBin } from '../../../utils/Utils'
import {
    sendDataSecondCall,
    setDataCreateClientPMP,
    setDataUpdateClientePMP,
    setDataCreateRelationshipPMP,
    setDataCreateAccountPMP,
    setDataCreateCreditCard,
    setDataBlockTypeCreditCardPMP
} from '../../../utils/origination/Origination'
import {
    redirectStep,
    sendDataSimpleGenericActivityStepClientRegister,
    sendDataCommercialOffer,
    sendDataSimpleGenericActivityStepProductDetail,
    sendDataOfferSummary,
    sendDatagenericActivityStepEmbossing,
    sendDataPendingEmbossing,
    sendDataActivationCreditCard,
    sendDataGenericActivityActivateCard,
    sendDataGenericActivityServingCardAccepted,
    sendDataSimpleGenericActivityPendingActivationCredit,
    setReintentProcessPMP,
    setReintentBlockSolicitude,
    sendDataCreateClientPMP,
    sendDataUpdateClientePMP,
    sendDataCreateRelationshipPMP,
    sendDataCreateAccountPMP,
    sendDataCreateCreditCard,
    sendDataAcceptedOrigination,
    setReintentProcessPMPBlocking,
} from '../../../utils/origination/Express'
import {
    sendSimpleGenericActivityStepClientRegisterCE,
    sendDataCommercialOfferCE,
    sendDataSimpleGenericActivityStepProductDetailCE,
    sendDataOfferSummaryCE,
    sendDataGenericActivityActivateCardCE,
    sendDataGenericActivityServingCardAcceptedCE,
    sendDataGenericActivityPendingProcessCE,
    sendDataActivationCreditCardCE,
    sendDataSimpleGenericActivityPendingActivationCreditCE,
    setReintentProcessPMPCE,
    sendDataCreateClientPMPCE,
    sendDataUpdateClientePMPCE,
    sendDataCreateRelationshipPMPCE,
    sendDataCreateAccountPMPCE,
    sendDataCreateCreditCardCE,
    sendDataAcceptedOriginationCE,
    setReintentProcessPMPCEBlocking
} from '../../../utils/origination/Efectivo'
import * as MasterOrigination from '../../../utils/origination/MasterOrigination'
// Config
import { URL_BASE } from '../../../actions/config';
// Effects
import Fade from 'react-reveal/Fade';
// Icons
import SendIcon from '@material-ui/icons/Send';
// Colors
import { green } from '@material-ui/core/colors';

const styles = theme => ({
    root: {
        width: '100%',
        position: "relative"
    },
    stepLabelOk: {
        background: "#2196f31c",
        padding: ".5em",
        borderRadius: ".5em",
        boxShadow: "0px 1.2px 1px #007ab8"
    },
    stepLabelDefault: {
        background: "#f0f0f080",
        padding: ".5em",
        borderRadius: "0 0 .8em 0",
        borderBottom: "1px solid gray"
    },
    button: {
        textTransform: 'none',
    },
    buttonProgressWrapper: {
        position: 'relative',
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    IconButton: {
        top: -37,
        left: -37,
        position: "absolute",
    },
    icon: {
        fontSize: 32
    },
    actionsContainer: {
        marginBottom: theme.spacing.unit * 2,
    },
    resetContainer: {
        padding: theme.spacing.unit * 3,
        textAlign: 'center'
    },
    historyPanel: {
        position: "fixed",
        top: "33.3vh",
        zIndex: 900
    },
    NewCommentForm: {
        position: "fixed",
        top: "20vh",
        right: "5px",
        zIndex: 900,
    },
    fontSize: {
        fontSize: 12
    }
});

const mapStateToProps = (state) => {
    return {
        odc: state.odcReducer,
        constantODC: state.constantODCReducer,
        zytrust: state.zytrustReducer,
        contingency: state.contingencyReducer,
        biometric: state.biometricReducer,
        preEvaluatedClient: state.preEvaluatedClientReducer,
        embossingCreditCard: state.embossingCreditCardReducer,
        odcActivity: state.odcActivityReducer,
        pmp: state.pmpReducer,
        verifyEmail: state.verifyEmailReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    const actions = {
        loadComment: bindActionCreators(loadComment, dispatch),
        getConstantODC: bindActionCreators(getConstantODC, dispatch),

        cancelActivity: bindActionCreators(cancelActivity, dispatch),
        genericActivity: bindActionCreators(genericActivity, dispatch),
        simpleGenericActivity: bindActionCreators(simpleGenericActivity, dispatch),

        firstCall: bindActionCreators(firstCall, dispatch),
        secondCall: bindActionCreators(secondCall, dispatch),
        offerSummary: bindActionCreators(offerSummary, dispatch),
        commercialOffer: bindActionCreators(commercialOffer, dispatch),
        pendingEmbossing: bindActionCreators(pendingEmbossing, dispatch),

        createClient: bindActionCreators(createClient, dispatch),
        updateClient: bindActionCreators(updateClient, dispatch),
        createRelationship: bindActionCreators(createRelationship, dispatch),
        createAccount: bindActionCreators(createAccount, dispatch),
        createCreditCard: bindActionCreators(createCreditCard, dispatch),
        blockTypeCreditCard: bindActionCreators(blockTypeCreditCard, dispatch),

        biometricZytrustService: bindActionCreators(biometricZytrustService, dispatch),
        continueProcess: bindActionCreators(continueProcess, dispatch),

        verifySolicitudeIsBlocked: bindActionCreators(verifySolicitudeIsBlocked, dispatch),
        blockSolicitude: bindActionCreators(blockSolicitude, dispatch),
        unlockSolicitude: bindActionCreators(unlockSolicitude, dispatch),

        encryptCardnumber: bindActionCreators(ActionPci.encryptCardnumber, dispatch),
        decryptCardnumber: bindActionCreators(ActionPci.decryptCardnumber, dispatch),

        registerClientIntoMaster: bindActionCreators(ActionOdcMaster.registerUpdateClient, dispatch),
        registerAccountIntoMaster: bindActionCreators(ActionOdcMaster.registerAccount, dispatch),
        registerCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.registerCreditCard, dispatch),
        updateBlockingCodeCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.updateBlockingCodeCreditCard, dispatch)
    };
    return actions;
}

const STATE = {
    activeStep: 0,
    comodinPMP: "A",
    showHistory: false,
    showVerifyEmail: true,
    listComments: [],
    origination: {
        number: 0,
        activityNumber: 0,
        fullNumber: "",
        previousStatePhaseFlow: 0,
        nextStatePhaseFlow: 0,
        typeSolicitudeCode: 340001,
        typeSolicitudeOriginationCode: 390001,
        typeSolicitudeReqCode: 0,
        rePrintMotiveCode: 0
    },
    client: {
        code: 0,
        id: 0,
        documentTypeErro: false,
        documentTypeId: "",
        documentTypeAux: "",
        documentTypeInternalValue: "",
        documentType: "",
        documentNumber: "",
        firstName: "",
        secondName: "",
        lastName: "",
        motherLastName: "",
        fullName: "",
        shortName: "",
        birthday: "",
        genderId: "",
        gender: "",
        genderInternalValue: "",
        nationalityId: "",
        nationality: "",
        maritalStatusId: "",
        maritalStatusInternalValue: "",
        maritalStatus: "",
        academicDegreeId: "",
        academicDegree: "",
        academicDegreeInternalValue: "",
        email: "",
        emailConfirmed: false,
        cellphone: "",
        telephone: "",
        homeAddress: {
            departmentId: "",
            department: "",
            provinceId: "",
            province: "",
            districtId: "",
            district: "",
            viaId: "",
            via: "",
            viaInternalValue: "",
            nameVia: "",
            number: "",
            building: "",
            inside: "",
            mz: "",
            lot: "",
            zoneId: "",
            zone: "",
            zoneInternalValue: "",
            nameZone: "",
            typeResidence: "",
            housingTypeId: "",
            housingType: "",
            housingTypeInternalValue: "",
            reference: "",
        },
        workAddress: {
            departmentId: "",
            department: "",
            provinceId: "",
            province: "",
            districtId: "",
            district: "",
            viaId: "",
            via: "",
            viaInternalValue: "",
            nameVia: "",
            number: "",
            building: "",
            inside: "",
            mz: "",
            lot: "",
            zoneId: "",
            zone: "",
            zoneInternalValue: "",
            nameZone: "",
            typeResidence: "",
            housingTypeId: "",
            housingType: "",
            reference: "",
        },
        employmentSituationId: "",
        employmentSituation: "",
        jobTitleId: "",
        jobTitle: "",
        economicActivityId: "",
        economicActivity: "",
        laborIncomeDate: "",
        businessOption: "",
        businessName: "",
        ruc: "",
        grossIncome: 0,
        workTelephoneExtension: "",
        workTelephone: "",
        extra: {
            paymentDate: "",
            paymentDateInternalValue: "",
            paymentDateId: "",
            sendCorrespondence: "",
            sendCorrespondenceId: "",
            accountStatusId: "",
            accountStatus: "",
            effectiveWithdrawal: "",
            alert: "",
            sendCommonPromotion: "",
            media: "",
            mediaId: "",
            promoterId: "",
            promoter: "",
            term: "",
        },
        relationTypeId: 330001
    },
    pmp: {
        clientNumber: "",
        relationshipNumber: "",
        accountNumber: "",
        cardNumber: "",
        parallelCardNumber: "",
        token: ''
    },
    cda: null,
    adn: null,
    siebel: null,
    fraudPrevention: null,
    sae: null,
    creditProducts: null,
    pct: {
        number: ""
    },
    creditCard: null,
    segment: "",
    effectiveProvision: 0,
    isSAEProcess: true,
    lineAvailable: {
        min: 0,
        value: 1500,
        max: 3000,
    },
    initialLineAvailable: {
        lineAvailableEc: 0,
        lineAvailable: 0,
        globalLineaAvailable: 0,
        lineAvailableTc: 0
    },
    lineSAE: -1,
    finalEffectiveProvision: 0,
    finalLineAvailable: -1,
    finalLineSAE: -1,
    finalMaxAmount: -1,
    pctSAE: "",
    cancel: false,
    redirectError: false,
    openModal: false,
    reintentRegisterUpdateClient: 0,
    reintentProcessPMP: 0,
    reintentBlockSolicitude: 0,
    reintentBlockTypeProcessPMP: 0,
    isBiometricOk: false,
    disbursementTypeId: 0,
    numero_cuotas: 0,
    monto_cuota: 0,
    tcea: 0
};
class ExpressPage extends Component {
    state = STATE;

    constructor(props) {
        super(props);
        this.handleUnload = this.handleUnload.bind(this);
    }

    componentWillMount() {
        // Redirect Step and Set Data Client
        const dataUpdate = this.props.location ? this.props.location.state ? this.props.location.state.dataUpdate : null : null;
        if (dataUpdate !== null) {
            this.setState(state => ({
                ...state, activeStep: -1
            }));
        }
    }
    componentWillUnmount() {
        window.removeEventListener('beforeunload', this.handleUnload);
    }
    handleUnload(e) {
        var message = "o/";
        (e || window.event).returnValue = message;
        return message;
    }
    componentDidMount() {
        this.props.getConstantODC();
        // Set Value Process
        const dataUpdate = this.props.location ? this.props.location.state ? this.props.location.state.dataUpdate : null : null;
        if (dataUpdate !== null) {
            let { cod_solicitud_completa } = dataUpdate;
            this.props.continueProcess(cod_solicitud_completa);
        }
        window.addEventListener('beforeunload', this.handleUnload);
    }
    componentDidUpdate = (prevProps) => {

        if (prevProps.constantODC !== this.props.constantODC) {
            // Credit Card Constant - Error Service
            if (!this.props.constantODC.loading &&
                this.props.constantODC.response &&
                this.props.constantODC.success) {
                let { data } = this.props.constantODC
                let comodinPMP = data.find((item) => item.des_abv_constante === "COMODIN_PMP").valor_texto;
                this.setState(state => ({
                    ...state,
                    comodinPMP
                }));
            }
            // Credit Card Constant - Error Service
            else if (!this.props.constantODC.loading &&
                this.props.constantODC.response &&
                !this.props.constantODC.success) {
            }
            // Credit Card Constant - Error Service Connectivity
            else if (!this.props.constantODC.loading &&
                !this.props.constantODC.response &&
                !this.props.constantODC.success) {
            }
        }

        if (
            prevProps.verifyEmail.updateClientMailService !==
            this.props.verifyEmail.updateClientMailService
        ) {

            // Update Mail Service - Success Service
            if (
                !this.props.verifyEmail.updateClientMailService.loading &&
                this.props.verifyEmail.updateClientMailService.response &&
                this.props.verifyEmail.updateClientMailService.success
            ) {
                let { actualizarEmail } = this.props.verifyEmail.updateClientMailService.data
                this.setState({
                    client: {
                        ...this.state.client,
                        email: actualizarEmail.des_to,
                        emailConfirmed: true,
                    },
                    showVerifyEmail: false
                })
            }
        }

        if (prevProps.odc !== this.props.odc) {
            if (prevProps.odc.firstCall !== this.props.odc.firstCall) {
                if (!this.props.odc.firstCall.loading
                    && this.props.odc.firstCall.response
                    && this.props.odc.firstCall.success) {

                    let { origination, client, cda, siebel, fraudPrevention, adn, validations, sae, creditProducts, initialLineAvailable } = this.props.odc.firstCall.data;
                    if (this.redirectRegular({ cda, adn, validations, client })) {
                        this.props.history.push(
                            {
                                pathname: '/odc/regular/nueva-solicitud',
                                state: { documentNumber: client.documentNumber }
                            }
                        );
                    } else {
                        if (origination && cda) {
                            this.setState(state => ({
                                ...state,
                                origination: {
                                    ...state.origination,
                                    ...origination
                                },
                                client: {
                                    ...state.client,
                                    ...client
                                },
                                cda: {
                                    ...state.cda,
                                    ...cda
                                },
                                adn: {
                                    ...state.adn,
                                    ...adn
                                },
                                siebel: {
                                    ...state.siebel,
                                    ...siebel
                                },
                                fraudPrevention: {
                                    ...state.fraudPrevention,
                                    ...fraudPrevention
                                },
                                validations: validations,
                                sae: sae,
                                creditProducts: creditProducts,
                                initialLineAvailable: initialLineAvailable
                            }));
                        }
                    }
                }
            }
            if (prevProps.odc.process !== this.props.odc.process) {
                if (!this.props.odc.process.loading
                    && this.props.odc.process.response
                    && this.props.odc.process.success) {

                    let { data } = this.props.odc.process;

                    if (data) {
                        let { list_ori_sel_solicitud_retomar_direccion } = data;
                        if (list_ori_sel_solicitud_retomar_direccion.length > 0) {
                            let homeAddress = list_ori_sel_solicitud_retomar_direccion.find(item => item.cod_tipo_direccion === 260001);
                            let workAddress = list_ori_sel_solicitud_retomar_direccion.find(item => item.cod_tipo_direccion === 260002);
                            let stepData = redirectStep(data.cod_flujo_fase_estado)
                            const dataReintentProcessPmp = {
                                status: data.cod_flujo_fase_estado || -1,
                                errorMessage: data.des_error_message_servicio
                            }
                            const reintentProcessPMP = data.flg_cec || data.cod_bin_pro === cencosudEffectiveBin
                                ? setReintentProcessPMPCE(dataReintentProcessPmp)
                                : setReintentProcessPMP(dataReintentProcessPmp)
                            const reintentProcessPMPBlocking = data.flg_cec || data.cod_bin_pro === cencosudEffectiveBin
                                ? setReintentProcessPMPCEBlocking(dataReintentProcessPmp)
                                : setReintentProcessPMPBlocking(dataReintentProcessPmp)
                            const reintentBlockSolicitude = setReintentBlockSolicitude(data.cod_flujo_fase_estado || -1)
                            this.setState(state => ({
                                ...state,
                                activeStep: stepData.step,
                                showVerifyEmail: !data.flg_confirmado,
                                origination: {
                                    ...state.origination,
                                    number: data.cod_solicitud ? data.cod_solicitud : 0,
                                    activityNumber: data.cod_solicitud_actividad ? data.cod_solicitud_actividad : 0,
                                    fullNumber: data.cod_solicitud_completa ? data.cod_solicitud_completa : "",
                                    previousStatePhaseFlow: data.cod_flujo_fase_estado ? data.cod_flujo_fase_estado : -1
                                },
                                pmp: {
                                    ...state.pmp,
                                    clientNumber: data.num_cliente,
                                    relationshipNumber: data.num_relacion,
                                    accountNumber: data.num_cuenta,
                                    cardNumber: data.num_tarjeta,
                                    parallelCardNumber: data.num_tarjeta_sae,
                                    token: data.token
                                },
                                client: {
                                    ...state.client,
                                    id: data.cod_cliente ? data.cod_cliente : 0,
                                    documentTypeId: data.cod_tipo_documento === 1 || data.cod_tipo_documento === 100001 ? 100001 : 100002,
                                    documentType: data.cod_tipo_documento === 1 || data.cod_tipo_documento === 100001 ? "DNI" : "CE",
                                    documentTypeAux: data.cod_tipo_documento === 1 || data.cod_tipo_documento === 100001 ? "1" : "2",
                                    documentTypeInternalValue: data.cod_tipo_documento === 1 || data.cod_tipo_documento === 100001 ? "DU" : "CE",
                                    documentNumber: data.des_nro_documento,
                                    secondName: data.des_segundo_nom ? data.des_segundo_nom : "",
                                    lastName: data.des_ape_paterno ? data.des_ape_paterno : "",
                                    motherLastName: data.des_ape_materno ? data.des_ape_materno : "",
                                    fullName: `${data.des_ape_paterno ? data.des_ape_paterno : ""} ${data.des_ape_materno ? data.des_ape_materno : ""} ${data.des_primer_nom ? data.des_primer_nom : ""} ${data.des_segundo_nom ? data.des_segundo_nom : ""}`,
                                    shortName: `${data.des_ape_paterno ? data.des_ape_paterno : ""} ${data.des_primer_nom ? data.des_primer_nom : ""}`,

                                    birthday: data.fec_nacimiento ? data.fec_nacimiento : "",
                                    genderId: data.cod_genero ? parseInt(data.cod_genero) : 0,
                                    gender: data.des_genero ? data.des_genero : "",
                                    genderInternalValue: data.des_genero_valor_interno ? data.des_genero_valor_interno : "",

                                    nationalityId: data.cod_nacionalidad ? parseInt(data.cod_nacionalidad) : 0,
                                    nationality: data.des_nacionalidad ? data.des_nacionalidad : 0,
                                    nationalityInternalValue: data.nationalityInternalValue ? data.nationalityInternalValue : "",

                                    maritalStatusId: data.cod_estado_civil ? parseInt(data.cod_estado_civil) : 0,
                                    maritalStatus: data.des_estado_civil ? data.des_estado_civil : "",
                                    maritalStatusInternalValue: data.des_estado_civil_valor_interno ? data.des_estado_civil_valor_interno : "",

                                    academicDegreeId: data.cod_gradoacademico ? parseInt(data.cod_gradoacademico) : "",
                                    academicDegree: data.des_gradoacademico ? data.des_gradoacademico : "",
                                    academicDegreeInternalValue: data.des_gradoacademico_valor_interno ? data.des_gradoacademico_valor_interno : "",

                                    email: data.des_correo ? data.des_correo : "",
                                    emailConfirmed: data.flg_confirmado,
                                    cellphone: data.des_telef_celular ? data.des_telef_celular : "",
                                    telephone: data.des_telef_fijo ? data.des_telef_fijo : "",

                                    homeAddress: {
                                        ...state.homeAddress,
                                        departmentId: homeAddress.cod_ubi_departamento ? homeAddress.cod_ubi_departamento : "000000",
                                        department: homeAddress.des_ubi_departamento ? homeAddress.des_ubi_departamento : "",
                                        provinceId: homeAddress.cod_ubi_provincia ? homeAddress.cod_ubi_provincia : "000000",
                                        province: homeAddress.des_ubi_provicia ? homeAddress.des_ubi_provicia : "",
                                        districtId: homeAddress.cod_ubi_distrito ? homeAddress.cod_ubi_distrito : "000000",
                                        district: homeAddress.des_ubi_distrito ? homeAddress.des_ubi_distrito : "",
                                        viaId: homeAddress.cod_via ? homeAddress.cod_via : 0,
                                        via: homeAddress.des_via ? homeAddress.des_via : "", // Here Value
                                        viaInternalValue: homeAddress.des_via_valor_interno ? homeAddress.des_via_valor_interno : "",
                                        nameVia: homeAddress.des_nombre_via ? homeAddress.des_nombre_via : "",
                                        number: homeAddress.des_nro ? homeAddress.des_nro : "",
                                        building: homeAddress.des_departamento ? homeAddress.des_departamento : "",
                                        inside: homeAddress.des_interior ? homeAddress.des_interior : "",
                                        mz: homeAddress.des_manzana ? homeAddress.des_manzana : "",
                                        lot: homeAddress.des_lote ? homeAddress.des_lote : "",
                                        zoneId: homeAddress.cod_zona ? homeAddress.cod_zona : "",
                                        zone: homeAddress.des_zona ? homeAddress.des_zona : "Desconocido",
                                        zoneInternalValue: homeAddress.des_zona_valor_interno ? homeAddress.des_zona_valor_interno : "",
                                        nameZone: homeAddress.des_zona || "-",
                                        housingTypeId: homeAddress.cod_tipo_vivienda ? homeAddress.cod_tipo_vivienda : 0,
                                        housingType: homeAddress.des_tipo_vivienda ? homeAddress.des_tipo_vivienda : 0,
                                        housingTypeInternalValue: homeAddress.des_tipo_vivienda_valor_interno ? homeAddress.des_tipo_vivienda_valor_interno : 0,
                                        reference: homeAddress.des_referencia_domicilio ? homeAddress.des_referencia_domicilio : ""
                                    },
                                    workAddress: {
                                        ...state.workAddress,
                                        departmentId: workAddress.cod_ubi_departamento ? workAddress.cod_ubi_departamento : "000000",
                                        department: workAddress.des_ubi_departamento ? workAddress.des_ubi_departamento : "",
                                        provinceId: workAddress.cod_ubi_provincia ? workAddress.cod_ubi_provincia : "000000",
                                        province: workAddress.des_ubi_provicia ? workAddress.des_ubi_provicia : "",
                                        districtId: workAddress.cod_ubi_distrito ? workAddress.cod_ubi_distrito : "000000",
                                        district: workAddress.des_ubi_distrito ? workAddress.des_ubi_distrito : "",
                                        viaId: workAddress.cod_via ? workAddress.cod_via : 0,
                                        via: workAddress.des_via ? workAddress.des_via : "", // Here Value
                                        viaInternalValue: workAddress.des_via_valor_interno ? workAddress.des_via_valor_interno : "",
                                        nameVia: workAddress.des_nombre_via ? workAddress.des_nombre_via : "",
                                        number: workAddress.des_nro ? workAddress.des_nro : "",
                                        building: workAddress.des_departamento ? workAddress.des_departamento : "",
                                        inside: workAddress.des_interior ? workAddress.des_interior : "",
                                        mz: workAddress.des_manzana ? workAddress.des_manzana : "",
                                        lot: workAddress.des_lote ? workAddress.des_lote : "",
                                        zoneId: workAddress.cod_zona ? workAddress.cod_zona : "",
                                        zone: workAddress.des_zona ? workAddress.des_zona : "Desconocido",
                                        zoneInternalValue: workAddress.des_zona_valor_interno ? workAddress.des_zona_valor_interno : "",
                                        nameZone: workAddress.des_zona || "-",
                                        housingTypeId: workAddress.cod_tipo_vivienda ? workAddress.cod_tipo_vivienda : 0,
                                        housingType: workAddress.des_tipo_vivienda ? workAddress.des_tipo_vivienda : 0,
                                        housingTypeInternalValue: workAddress.des_tipo_vivienda_valor_interno ? workAddress.des_tipo_vivienda_valor_interno : 0,
                                        reference: workAddress.des_referencia_domicilio ? workAddress.des_referencia_domicilio : ""
                                    },
                                    employmentSituationId: data.cod_situacion_laboral ? data.cod_situacion_laboral : 0,
                                    employmentSituation: data.des_situacion_laboral ? data.des_situacion_laboral : "",
                                    employmentSituationInternalValue: data.des_situacion_laboral_valor_interno ? data.des_situacion_laboral_valor_interno : "",

                                    jobTitleId: data.cod_cargo_profesion ? data.cod_cargo_profesion : 0,
                                    jobTitle: data.des_cargo_profesion ? data.des_cargo_profesion : "Desconocido",
                                    jobTitleInternalValue: data.des_cargo_profesion_valor_interno ? data.des_cargo_profesion_valor_interno : "",

                                    economicActivityId: data.cod_actividad_economica ? data.cod_actividad_economica : 0,
                                    economicActivity: data.des_actividad_economica ? data.des_actividad_economica : "Desconocido",
                                    economicActivityInternalValue: data.des_actividad_economica_valor_interno ? data.des_actividad_economica_valor_interno : "",

                                    laborIncomeDate: data.fec_ingreso_laboral ? data.fec_ingreso_laboral : "",
                                    businessOption: "",
                                    businessName: data.des_razon_social ? data.des_razon_social : "",
                                    ruc: data.des_ruc ? data.des_ruc : "",
                                    grossIncome: data.num_ingreso_bruto ? data.num_ingreso_bruto : 0,
                                    workTelephoneExtension: data.des_anexo_laboral ? data.des_anexo_laboral : "",
                                    workTelephone: data.des_telefono_laboral ? data.des_telefono_laboral : "",

                                    extra: {
                                        ...state.extra,
                                        accountStatus: data.des_estado_cuenta ? data.des_estado_cuenta : "",
                                        accountStatusId: data.cod_estado_cuenta ? data.cod_estado_cuenta : 0,
                                        alert: data.flg_alerta_uso_tar ? "1" : "0",
                                        effectiveWithdrawal: data.flg_retiro_efectivo ? "1" : "0",
                                        media: data.des_envio_comunicacion ? data.des_envio_comunicacion : "",
                                        mediaId: data.cod_envio_comunicacion,
                                        paymentDate: data.des_fecha_pago ? data.des_fecha_pago : 0,
                                        paymentDateId: data.cod_fecha_pago ? data.cod_fecha_pago : 0,
                                        paymentDateInternalValue: data.des_fecha_pago_valor_interno ? data.des_fecha_pago_valor_interno : "1",
                                        promoter: "",
                                        promoterId: data.cod_resp_promotor ? data.cod_resp_promotor : 0,
                                        sendCommonPromotion: data.flg_envio_comunicacion ? "1" : "0",
                                        sendCorrespondence: data.des_envio_correspondencia ? data.des_envio_correspondencia : 0,
                                        sendCorrespondenceId: data.cod_envio_correspondencia ? data.cod_envio_correspondencia : 0,
                                        term: data.flg_autoriza_datos_per ? "1" : "0"
                                    },
                                },
                                creditCard: {
                                    ...state.creditCard,
                                    name: data.des_nom_producto ? data.des_nom_producto : "",
                                    productId: data.cod_producto ? data.cod_producto : 0,
                                    colorId: data.cod_valor_color ? data.cod_valor_color : 0,
                                    color: data.des_valor_color ? data.des_valor_color : "",
                                    colorAux: data.des_valor_color_2 ? data.des_valor_color_2 : "",
                                    brandId: data.cod_valor_marca ? data.cod_valor_marca : 0,
                                    brand: data.des_valor_marca ? data.des_valor_marca : "",
                                    cardNumber: data.num_tarjeta || '',
                                    bin: data.cod_bin_pro ? data.cod_bin_pro : "",
                                    type: data.des_tip_pro ? data.des_tip_pro : "",
                                },
                                pct: {
                                    number: data.number_pct ? data.number_pct : "001"
                                },
                                segment: data.des_segmento ? data.des_segmento : "", // No Value in Database
                                effectiveProvision: data.disp_efec_porcentaje ? data.disp_efec_porcentaje : 0,
                                finalEffectiveProvision: data.disp_efec_porcentaje ? data.disp_efec_porcentaje : 0,
                                lineAvailable: {
                                    value: data.linea_credito_oferta ? data.linea_credito_oferta : 0,
                                    max: data.monto_max_final ? data.monto_max_final : 0,
                                },
                                finalLineAvailable: data.linea_credito_final ? data.linea_credito_final : 0,
                                reintentProcessPMP: reintentProcessPMP,
                                reintentBlockTypeProcessPMP: reintentProcessPMPBlocking,
                                reintentBlockSolicitude: reintentBlockSolicitude,
                                isBiometricOk: data.flg_biometria ? data.flg_biometria : false,

                                isSAE: data.cod_solicitud_sae && data.cod_solicitud_sae !== 0 ? true : false,
                                isSAEProcess: data.flg_cec || data.cod_bin_pro === cencosudEffectiveBin,
                                finalLineSAE: data.linea_sae_final ? data.linea_sae_final : 0,
                                finalMaxAmount: data.monto_max_final ? data.monto_max_final : 0,
                                pctSAE: data.pct_sae_final ? data.pct_sae_final : 0,
                                disbursementTypeId: data.cod_tipo_desembolso ? data.cod_tipo_desembolso : 0,
                                sae: {
                                    lineSAE: data.linea_sae ? data.linea_sae : 0,
                                    lineAvailable: data.linea_credito_oferta ? data.linea_credito_oferta : 0,
                                    maxAmount: data.monto_max ? data.monto_max : 0,
                                    pctSAE1: data.pct_sae_1 ? data.pct_sae_1 : "",
                                    pctSAE2: data.pct_sae_2 ? data.pct_sae_2 : "",
                                    baseDate: data.fecha_act ? data.fecha_act : "",
                                    block: data.flg_bq2 ? data.flg_bq2 : ""
                                },
                                tcea: data.tcea ? data.tcea : '',
                                numero_cuotas: data.numero_cuotas ? data.numero_cuotas : '',
                                monto_cuota: data.monto_cuota ? data.monto_cuota : '',
                            }));

                            let sendData = {
                                cod_solicitud: data.cod_solicitud ? data.cod_solicitud : 0,
                                cod_solicitud_completa: data.cod_solicitud_completa ? data.cod_solicitud_completa : "",
                                cod_flujo_fase_estado_actual: data.flg_cec || data.cod_bin_pro === cencosudEffectiveBin ? 91101 : 11201,
                                cod_flujo_fase_estado_anterior: data.flg_cec || data.cod_bin_pro === cencosudEffectiveBin ? 91101 : 11201,
                                nom_actividad: data.flg_cec || data.cod_bin_pro === cencosudEffectiveBin ? "Retomar Solicitud CEC" : "Retomar Solicitud Originación"
                            }

                            this.props.simpleGenericActivity(sendData, null, false)

                            SmoothScroll(stepData.step);
                        }
                        else {
                            this.setState(state => ({
                                ...state,
                                redirectError: true
                            }));
                        }
                    }
                    else {
                        this.setState(state => ({
                            ...state,
                            redirectError: true
                        }));
                    }
                }
                else if (!this.props.odc.process.loading
                    && this.props.odc.process.response
                    && !this.props.odc.process.success) {
                    this.setState(state => ({
                        ...state,
                        redirectError: true
                    }));
                }
                else if (!this.props.odc.process.loading
                    && !this.props.odc.process.response
                    && !this.props.odc.process.success) {
                    this.setState(state => ({
                        ...state,
                        redirectError: true
                    }));
                }
            }
        }
    }

    // Evaluate and Redirect to Regular Process
    redirectRegular = (data) => {
        let { isPrevailed,
            PEP,
            PIB,
            isProcessSiebel,
            isProcessODC,
        } = evaluatePrevaluated(data);
        return (PIB === 1 && PEP === 1 && isPrevailed === 0 && isProcessODC === 0 && isProcessSiebel === 0)
    }

    // Open Modal Cancel
    handleOpenModalCancelExpress = () => {
        this.setState(state => ({
            ...state,
            openModal: true
        }));
    }
    // Close Modal Cancel
    handleCloseModalCancelExpress = () => {
        this.setState(state => ({
            ...state,
            openModal: false
        }));
    }
    // Cancel Express
    handleCancelExpress = () => {
        this.handleCloseModalCancelExpress();
        this.handleReset();
    }
    // Submit - Cancel Express
    handleSubmitCancelExpress = (e) => {
        e.preventDefault();
        let { origination, activeStep } = this.state;
        let cancel = this.selectStatePhaseFlowCancel(activeStep);
        let sendData = {
            cod_solicitud: origination.number,
            cod_solicitud_completa: origination.fullNumber,
            cod_flujo_fase_estado_actual: cancel.currentStatePhaseFlow,
            cod_flujo_fase_estado_anterior: cancel.previousStatePhaseFlow,
            nom_actividad: cancel.activityName
        }
        this.props.cancelActivity(sendData);
    }
    // TO DO
    // Select State Phase Flow Cancel
    selectStatePhaseFlowCancel = (step) => {
        switch (step) {
            case 1: return {
                currentStatePhaseFlow: 10103,
                previousStatePhaseFlow: 10101,
                activityName: "Validación: Cancelado"
            }
            case 2: return {
                currentStatePhaseFlow: 10203,
                previousStatePhaseFlow: 10201,
                activityName: "Preevaluado: Cancelado"
            }
            case 3: return {
                currentStatePhaseFlow: 10303,
                previousStatePhaseFlow: 10301,
                activityName: "Oferta: Cancelado"
            }
            case 4: return {
                currentStatePhaseFlow: 10403,
                previousStatePhaseFlow: 10401,
                activityName: "Preventa: Cancelado"
            }
            default: return {
                currentStatePhaseFlow: 0,
                previousStatePhaseFlow: 0,
                activityName: ""
            }
        }
    }

    handleNext = () => {
        this.setState(state => ({
            activeStep: state.activeStep + 1,
        }), () => SmoothScroll(this.state.activeStep));
    }
    handleBack = () => {
        this.setState(state => ({
            activeStep: state.activeStep - 1,
        }), () => SmoothScroll(this.state.activeStep));
    }
    handleReset = () => {
        this.setState({ ...STATE },
            () => SmoothScroll(this.state.activeStep));
    }

    // Step 1 - Client Validation
    handleClientValidationForm = data => {
        let { client } = data;
        if (client) {
            //Send Data
            let sendData = {
                tipo_doc: client.documentTypeAux,
                nro_doc: client.documentNumber,
                tipo_doc_letra: client.documentType,
                cod_flujo_fase_estado_actual: 10101,
                cod_flujo_fase_estado_anterior: 10100,
                cod_solicitud: 0,
                cod_solicitud_completa: "",
                cod_cliente: 0,
                cod_tipo_solicitud: 340001,
                cod_tipo_relacion: 330001,
                cod_tipo_solicitud_requerimiento: 0,
                cod_etapa_maestro: Constants.MasterConfigurationStage.masterExpressStage
            }

            let sendDataExtra = {
                documentTypeId: client.documentTypeId,
                documentTypeInternalValue: client.documentTypeInternalValue
            }
            // Send first Call
            this.props.firstCall(sendData, sendDataExtra);
        }
    }
    // Step 2 - Pre-Evaluated Client
    handlePreEvaluatedClient = data => {
        if (data) {
            let { origination } = this.state;
            // Send Data
            let sendData = {
                cod_solicitud: origination.number,
                cod_solicitud_completa: origination.fullNumber,
                cod_flujo_fase_estado_actual: data.isSAEProcess ? 90102 : 10102,
                cod_flujo_fase_estado_anterior: 10101,
                nom_actividad: "Validación: Aceptado",
            }
            this.setState(state => ({
                ...state,
                client: {
                    ...state.client,
                    ...data.client
                },
                lineAvailable: { ...data.lineAvailable },
                cda: data.cda,
                isSAEProcess: data.isSAEProcess,
                effectiveProvision: data.effectiveProvision
            }));
            this.props.genericActivity(sendData, null, true);
        }
    }
    // Step 3 - Client Register
    handleClientRegister = ({ data, reset = false }) => {
        if (data) {
            const { reintentRegisterUpdateClient } = this.state
            const steps = this.getStepsRegisterUpdateClient()
            steps[reset ? 0 : reintentRegisterUpdateClient].action(data)
        }
    }
    getStepsRegisterUpdateClient = _ => ({
        0: {
            action: async data => {
                const sendData = sendDataSecondCall(data, this.state.origination, this.state.client.id, this.state.isSAEProcess)
                const result = await this.props.secondCall(sendData).then(response => response)
                if (result?.data?.success) {
                    this.setState(state => ({
                        ...state,
                        client: {
                            ...state.client,
                            ...data.client
                        },
                        cda: {
                            ...state.cda,
                            ...result.data.segundaLlamadaCda.solicitud.salidaCDA
                        },
                        reintentRegisterUpdateClient: 1
                    }), _ => this.handleClientRegister({ data }))
                }
            }
        },
        1: {
            action: async data => {
                const { origination, isSAEProcess } = this.state
                const sendData = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    activityName: MasterOrigination.registerUpdateClientActivity.activityName,
                    phaseCode: MasterOrigination.registerUpdateClientActivity.phaseCode,
                    masterStageCode: isSAEProcess
                        ? Constants.MasterConfigurationStage.masterCECStage
                        : Constants.MasterConfigurationStage.masterExpressStage
                }
                const result = await this.props.registerClientIntoMaster(sendData).then(response => response)
                if (result?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentRegisterUpdateClient: 2
                    }), _ => this.handleClientRegister({ data }))
                }
            }
        },
        2: {
            action: _ => {
                const sendData = this.state.isSAEProcess
                    ? sendSimpleGenericActivityStepClientRegisterCE(this.state.origination)
                    : sendDataSimpleGenericActivityStepClientRegister(this.state.origination)
                this.props.simpleGenericActivity(sendData, { retomar: true }, true)
            }
        }
    })
    // Step 4 - Commercial Offer
    handleCommercialOffer = data => {
        const { origination, client, lineAvailable, initialLineAvailable, sae, isSAEProcess } = this.state;
        if (data) {
            let sendData = {};
            if (isSAEProcess && sae && Object.keys(sae).length > 0) {
                sendData = sendDataCommercialOfferCE(data, lineAvailable, initialLineAvailable, origination, client, sae);
            }
            else {
                sendData = sendDataCommercialOffer(data, lineAvailable, initialLineAvailable, origination, client);
            }
            this.props.commercialOffer(sendData)
                .then(response => {
                    if (response.data) {
                        this.setState(state => ({
                            ...state,
                            client: {
                                ...state.client,
                                ...data.client
                            },
                            creditCard: {
                                ...state.creditCard,
                                ...data.creditCard
                            },
                            finalEffectiveProvision: data.finalEffectiveProvision ? data.finalEffectiveProvision : 0,
                            finalLineAvailable: data.finalLineAvailable ? data.finalLineAvailable : 0,
                            finalLineSAE: data.finalLineSAE ? data.finalLineSAE : 0,
                            finalMaxAmount: data.finalMaxAmount ? data.finalMaxAmount : 0,
                            pctSAE: data.pctSAE ? data.pctSAE : "",
                            isSAE: data.isSAE,
                            numero_cuotas: data.numberFees ? data.numberFees : '',
                            monto_cuota: data.feeAmount ? data.feeAmount : '',
                            tcea: data.tcea,
                            segment: data.segment ? data.segment : "",
                            disbursementTypeId: data.disbursementTypeId ? data.disbursementTypeId : 0
                        }));
                    }
                });
        }
    }
    // Step 5 - Product Detail
    handleProductDetail = async data => {
        let {
            origination,
            reintentProcessPMP,
            reintentBlockSolicitude,
            client,
            creditCard,
            finalLineAvailable,
            initialLineAvailable,
            pmp,
            isSAEProcess
        } = this.state;
        let {
            pct
        } = data;
        let organization = "641";
        if (reintentBlockSolicitude === 0) {
            const sendData = {
                solicitudeCode: origination.number,
                completeSolicitudeCode: origination.fullNumber,
                clientCode: client.id,
                documentTypeId: client.documentTypeAux,
                documentNumber: client.documentNumber,
                lineAvailable: finalLineAvailable,
                globalLineAvailable: initialLineAvailable.globalLineaAvailable
            }
            this.props.blockSolicitude(sendData).then(response => {
                if (response?.success)
                    this.setState(state => ({
                        ...state,
                        reintentBlockSolicitude: 1
                    }), _ => this.handleProductDetail(data))
            })
        }
        if (reintentBlockSolicitude === 1) {
            // Init - Start
            if (reintentProcessPMP === 0) {
                let sendData = this.state.isSAEProcess
                    ? sendDataSimpleGenericActivityStepProductDetailCE(origination)
                    : sendDataSimpleGenericActivityStepProductDetail(origination)
                this.props.genericActivity(sendData)
                    .then(response => {
                        if (response?.data?.success) {
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 1
                            }), _ => this.handleProductDetail(data))
                        }
                    })
            }
            // Init - PMP - Create Client
            if (reintentProcessPMP === 1) {
                // Send Data
                let sendData = this.state.isSAEProcess
                    ? sendDataCreateClientPMPCE(organization, origination, client)
                    : sendDataCreateClientPMP(organization, origination, client)
                // Set Data Request PMP - Client
                let sendDataPMP = setDataCreateClientPMP(sendData);
                this.props.createClient(sendDataPMP)
                    .then(response => {
                        if (response?.data) {
                            if (response.data.errorCode === "12") {
                                this.setState(state => ({
                                    ...state,
                                    reintentProcessPMP: 1.5
                                }), _ => this.handleProductDetail(data))
                            } else if (response.data.success) {
                                const { pmpCrearCliente } = response.data
                                if (pmpCrearCliente) {
                                    const { createCustomer } = pmpCrearCliente
                                    this.setState(state => ({
                                        ...state,
                                        reintentProcessPMP: 2,
                                        pmp: {
                                            ...pmp,
                                            clientNumber: createCustomer.customerNumber
                                        }
                                    }), _ => this.handleProductDetail(data))
                                }
                            }
                        }
                    })
            }
            // Init - PMP - Update Client
            if (reintentProcessPMP === 1.5) {
                // Send Data
                let sendData = this.state.isSAEProcess
                    ? sendDataUpdateClientePMPCE(organization, origination, client)
                    : sendDataUpdateClientePMP(organization, origination, client);
                // Set Data Request PMP - Client
                let sendDataPMP = setDataUpdateClientePMP(sendData);
                this.props.updateClient(sendDataPMP)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpActualizarCliente } = response.data
                            if (pmpActualizarCliente) {
                                const { updateCustomer } = pmpActualizarCliente
                                this.setState(state => ({
                                    ...state,
                                    reintentProcessPMP: 2,
                                    pmp: {
                                        ...pmp,
                                        clientNumber: updateCustomer.customerNumber
                                    }
                                }), _ => this.handleProductDetail(data))
                            }
                        }
                    });
            }
            // Init - PMP - Create Relationship
            if (reintentProcessPMP === 2) {
                // Send Data
                let sendData = this.state.isSAEProcess ? sendDataCreateRelationshipPMPCE({
                    organization,
                    origination,
                    client,
                    pmp,
                    finalLineAvailable
                }) : sendDataCreateRelationshipPMP({
                    organization,
                    origination,
                    client,
                    pmp,
                    finalLineAvailable
                });
                // Set Data Request PMP - Relationship
                let sendDataPMP = setDataCreateRelationshipPMP(sendData);
                this.props.createRelationship(sendDataPMP)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpCrearRelacion } = response.data
                            const { createCustomerRelationship } = pmpCrearRelacion
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 3,
                                pmp: {
                                    ...state.pmp,
                                    relationshipNumber: createCustomerRelationship.relationshipNumber
                                }
                            }), _ => this.handleProductDetail(data))
                        }
                    });
            }
            // Init - PMP - Create Account
            if (reintentProcessPMP === 3) {
                // Send Data
                let sendData = this.state.isSAEProcess ? sendDataCreateAccountPMPCE({
                    organization,
                    origination,
                    client,
                    creditCard,
                    pmp,
                    pct,
                    finalLineAvailable
                }) : sendDataCreateAccountPMP({
                    organization,
                    origination,
                    client,
                    creditCard,
                    pmp,
                    pct,
                    finalLineAvailable
                })
                // Set Data Request PMP - Account
                let sendDataPMP = setDataCreateAccountPMP(sendData);
                this.props.createAccount(sendDataPMP)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpCrearCuenta } = response.data
                            const { createCustomerAccount } = pmpCrearCuenta
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 4,
                                pmp: {
                                    ...state.pmp,
                                    accountNumber: createCustomerAccount.accountNumber
                                }
                            }), _ => this.handleProductDetail(data))
                        }
                    });
            }
            // Master - Create Account
            if (reintentProcessPMP === 4) {
                const sendData = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    activityName: MasterOrigination.registerAccountActivity.activityName,
                    phaseCode: MasterOrigination.registerAccountActivity.phaseCode,
                    masterStageCode: isSAEProcess
                        ? Constants.MasterConfigurationStage.masterCECStage
                        : Constants.MasterConfigurationStage.masterExpressStage
                }
                const result = await this.props.registerAccountIntoMaster(sendData).then(response => response)
                if (result?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentProcessPMP: 5
                    }), _ => this.handleProductDetail(data))
                }
            }
            // Init - PMP - Create Credit Card
            if (reintentProcessPMP === 5) {
                // Send Data
                let sendData = this.state.isSAEProcess ? sendDataCreateCreditCardCE({
                    organization,
                    origination,
                    client,
                    creditCard,
                    pmp,
                    pct,
                    finalLineAvailable
                }) : sendDataCreateCreditCard({
                    organization,
                    origination,
                    client,
                    creditCard,
                    pmp,
                    pct,
                    finalLineAvailable
                })

                // Set Data Request PMP - Credit Card
                let sendDataPMP = setDataCreateCreditCard(sendData);
                this.props.createCreditCard(sendDataPMP)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpCrearTarjetaCredito } = response.data
                            const { createCreditCard } = pmpCrearTarjetaCredito
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 6,
                                pmp: {
                                    ...state.pmp,
                                    cardNumber: createCreditCard.cardNumber
                                },
                                creditCard: {
                                    ...state.creditCard,
                                    cardNumber: createCreditCard.cardNumber
                                }
                            }), _ => this.handleProductDetail(data))
                        }
                    });
            }
            // Encrypting Cardnumber
            if (reintentProcessPMP === 6) {
                const sendDataPci = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    pciStage: Constants.PciConfigurationStage.pciExpressStage
                }
                this.props.encryptCardnumber(sendDataPci)
                    .then(response => {
                        if (response?.success || response?.errorCode === Constants.CustomErrorCode.errorCodeCreditCardAlreadyPci) {
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 7,
                                pmp: {
                                    ...state.pmp,
                                    token: response.token
                                }
                            }), _ => this.handleProductDetail(data))
                        }
                    })
            }
            // Master - Create Credit Card
            if (reintentProcessPMP === 7) {
                const sendData = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    activityName: MasterOrigination.registerCreditCardActivity.activityName,
                    phaseCode: MasterOrigination.registerCreditCardActivity.phaseCode,
                    masterStageCode: isSAEProcess
                        ? Constants.MasterConfigurationStage.masterCECStage
                        : Constants.MasterConfigurationStage.masterExpressStage
                }
                const result = await this.props.registerCreditCardIntoMaster(sendData).then(response => response)
                if (result?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentProcessPMP: 8
                    }), _ => this.handleProductDetail(data))
                }
            }
            // Init - End
            if (reintentProcessPMP === 8) {
                let sendData = this.state.isSAEProcess
                    ? sendDataAcceptedOriginationCE(origination)
                    : sendDataAcceptedOrigination(origination);
                this.props.genericActivity(sendData, null, true).then(response => {
                    if (response?.data?.success)
                        this.setState(state => ({
                            ...state,
                            reintentBlockSolicitude: 2
                        }), _ => this.handleProductDetail(data))
                });
            }
        }
        if (reintentBlockSolicitude === 2) {
            const sendData = {
                solicitudeCode: origination.number,
                completeSolicitudeCode: origination.fullNumber,
                clientCode: client.id,
                documentTypeId: client.documentTypeAux,
                documentNumber: client.documentNumber
            }
            this.props.unlockSolicitude(sendData)
        }
    }
    // Step 6 - Offer Summary
    handleOfferSummary = _ => {
        let { origination, creditCard, lineAvailable, finalLineAvailable, finalEffectiveProvision } = this.state;
        if (origination && creditCard && finalLineAvailable >= 0 && finalEffectiveProvision >= 0) {
            let sendData = this.state.isSAEProcess ? sendDataOfferSummaryCE(origination, creditCard, finalEffectiveProvision, lineAvailable, finalLineAvailable) : sendDataOfferSummary(origination, creditCard, finalEffectiveProvision, lineAvailable, finalLineAvailable);
            this.props.offerSummary(sendData);
        }
    }
    // Step 7 - Embossing CreditCard
    handleEmbossingCreditCard = _ => {
        let { origination } = this.state;
        // Send Data
        let sendData = sendDatagenericActivityStepEmbossing(origination);
        this.props.genericActivity(sendData, null, true);
    }
    // Step 7 - Pending Embossing CreditCard
    handlePendingEmbossingCreditCard = data => {
        let { origination } = this.state;
        let { reason } = data;
        if (reason) {
            // Send Data
            let sendData = sendDataPendingEmbossing(origination, reason);
            this.props.pendingEmbossing(sendData);
        }
    }
    // Step 8 - Activation Credit Card
    handleActivationCreditCard = async _ => {
        const { origination, client, pmp, creditCard, disbursementTypeId, comodinPMP, isSAEProcess, 
            reintentBlockTypeProcessPMP } = this.state
        const organization = '641'
        if (reintentBlockTypeProcessPMP === 0) {
            const sendData = {
                solicitudeCode: origination.number,
                completeSolicitudeCode: origination.fullNumber,
                token: pmp.token,
                masterStageCode: isSAEProcess
                    ? Constants.MasterConfigurationStage.masterCECStage
                    : Constants.MasterConfigurationStage.masterExpressStage
            }
            const result = await this.props.decryptCardnumber(sendData).then(response => response)
            if (result?.success)
                this.setState(state => ({
                    ...state,
                    reintentBlockTypeProcessPMP: 1,
                    pmp: {
                        ...state.pmp,
                        cardNumber: result?.nro_tarjeta || pmp.cardNumber
                    }
                }), _ => this.handleActivationCreditCard())
        }
        if (reintentBlockTypeProcessPMP === 1) {
            const sendData = isSAEProcess
                ? sendDataActivationCreditCardCE(organization, comodinPMP, origination, client, pmp, creditCard)
                : sendDataActivationCreditCard(organization, comodinPMP, origination, client, pmp, creditCard)
            const sendDataPmp = setDataBlockTypeCreditCardPMP(sendData)
            this.props.blockTypeCreditCard(sendDataPmp, { isSAE: isSAEProcess }).then(response => {
                if (response?.data?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentBlockTypeProcessPMP: 2
                    }), _ => this.handleActivationCreditCard())
                }
            })
        }
        if (reintentBlockTypeProcessPMP === 2) {
            const sendData = {
                solicitudeCode: origination.number,
                completeSolicitudeCode: origination.fullNumber,
                accountNumber: pmp.accountNumber,
                token: pmp.token,
                blockingCode: comodinPMP,
                activityName: MasterOrigination.updateBlockingCodeCreditCardActivity.activityName,
                phaseCode: MasterOrigination.updateBlockingCodeCreditCardActivity.phaseCode,
                masterStageCode: isSAEProcess
                    ? Constants.MasterConfigurationStage.masterCECStage
                    : Constants.MasterConfigurationStage.masterExpressStage
            }
            const result = await this.props.updateBlockingCodeCreditCardIntoMaster(sendData).then(response => response)
            if (result?.success) {
                this.setState(state => ({
                    ...state,
                    reintentBlockTypeProcessPMP: 3
                }), _ => this.handleActivationCreditCard())
            }
        }
        if (reintentBlockTypeProcessPMP === 3) {
            if (isSAEProcess) {
                const sendData = sendDataGenericActivityActivateCardCE(origination)
                this.props.genericActivity(sendData, { finishProcess: false }, false)
                    .then(response => {
                        if (response?.data?.success) {
                            const sendData = sendDataGenericActivityServingCardAcceptedCE(origination)
                            this.props.genericActivity(sendData, { finishProcess: disbursementTypeId === 380002 }, false)
                                .then(response => {
                                    if (response?.data?.success && disbursementTypeId !== 380002) {
                                        const sendData = sendDataGenericActivityPendingProcessCE(origination)
                                        this.props.simpleGenericActivity(sendData, { finishProcess: true }, false)
                                    }
                                })
                        }
                    })
            }
            else {
                const sendData = sendDataGenericActivityActivateCard(origination);
                this.props.genericActivity(sendData, { finishProcess: false }, false)
                    .then(response => {
                        if (response?.data?.success) {
                            let sendData = sendDataGenericActivityServingCardAccepted(origination)
                            this.props.genericActivity(sendData, { finishProcess: true }, false);
                        }
                    });
            }
        }
    }
    // Step 8 - Pending Credit Card
    handlePendingActivationCredit = _ => {
        let { origination } = this.state;
        // Send Data
        let sendData = this.state.isSAEProcess ? sendDataSimpleGenericActivityPendingActivationCreditCE(origination) : sendDataSimpleGenericActivityPendingActivationCredit(origination);
        this.props.simpleGenericActivity(sendData, { finishProcess: false, pendingTC: true }, true);
    }
    // Biometric Zytrust Service
    handleBiometricZytrustService = _ => {
        let { client } = this.state;
        const sendData = {
            tiDocCliente: client.documentTypeAux,
            nuDocCliente: client.documentNumber,
        };
        this.props.biometricZytrustService(sendData);
    };

    getSteps = () => {
        return this.state.isSAEProcess ?
            [
                'Originación - Consulta cliente',
                'Validación Cliente',
                'Registro del Cliente',
                'Oferta Comercial',
                'Detalle Producto',
                'Resumen Oferta',
                'Activación de Efectivo Cencosud',
            ] :
            [
                'Originación - Consulta cliente',
                'Validación Cliente',
                'Registro del Cliente',
                'Oferta Comercial',
                'Detalle Producto',
                'Resumen Oferta',
                'Emboce de Tarjeta',
                'Activación de Tarjeta',
            ];

    }
    getStepContent = (step) => {

        if (this.state.isSAEProcess && step === 6) {
            step++;
        }
        switch (step) {

            case 0:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <ConsultClient
                            // State Redux
                            firstCall={this.props.odc.firstCall}
                            // Function
                            handleNext={this.handleNext}
                            handleClientValidationForm={this.handleClientValidationForm} />
                    </SnackbarProvider>
                </div>
            case 1:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <ClientDetailPreEvaluated
                            // State Redux
                            cancelActivity={this.props.odcActivity.cancelActivity}
                            genericActivity={this.props.odcActivity.genericActivity}
                            // Data
                            origination={this.state.origination}
                            client={this.state.client}
                            cda={this.state.cda}
                            adn={this.state.adn}
                            siebel={this.state.siebel}
                            fraudPrevention={this.state.fraudPrevention}
                            validations={this.state.validations}
                            sae={this.state.sae}
                            creditProducts={this.state.creditProducts}
                            initialLineAvailable={ this.state.initialLineAvailable }
                            constantODC={ this.props.constantODC }
                            history={this.props.history}
                            // Function
                            handlePreEvaluatedClient={this.handlePreEvaluatedClient}
                            handleOpenModalCancelExpress={this.handleOpenModalCancelExpress}
                            handleCancelExpress={this.handleCancelExpress}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext}
                        />
                    </SnackbarProvider>
                </div>
            case 2:
                return <div>
                    <SnackbarProvider maxSnack={6}>
                        <ClientRegister
                            // State Redux
                            cancelActivity={this.props.odcActivity.cancelActivity}
                            simpleGenericActivity={this.props.odcActivity.simpleGenericActivity}
                            secondCall={this.props.odc.secondCall}
                            // Data
                            client={this.state.client}
                            isSAEProcess={this.state.isSAEProcess}
                            origination={ this.state.origination }
                            handleClientRegister={this.handleClientRegister}
                            // Function
                            handleOpenModalCancelExpress={this.handleOpenModalCancelExpress}
                            handleCancelExpress={this.handleCancelExpress}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext} />
                    </SnackbarProvider>
                </div>
            case 3:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <CommercialOffer
                            // State Redux
                            cancelActivity={this.props.odcActivity.cancelActivity}
                            // Data
                            cda={this.state.cda}
                            client={this.state.client}
                            lineAvailable={this.state.lineAvailable}
                            sae={this.state.sae}
                            isSAEProcess={this.state.isSAEProcess}
                            commercialOffer={this.props.odc.commercialOffer}
                            handleCommercialOffer={this.handleCommercialOffer}

                            // Function
                            handleOpenModalCancelExpress={this.handleOpenModalCancelExpress}
                            handleCancelExpress={this.handleCancelExpress}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext}
                        />
                    </SnackbarProvider>
                </div>
            case 4:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <ProductDetail
                            // State Redux
                            cancelActivity={this.props.odcActivity.cancelActivity}
                            genericActivity={this.props.odcActivity.genericActivity}
                            createClient={this.props.pmp.createClient}
                            updateClient={this.props.pmp.updateClient}
                            createRelationship={this.props.pmp.createRelationship}
                            createAccount={this.props.pmp.createAccount}
                            createCreditCard={this.props.pmp.createCreditCard}
                            blockTypeCreditCard={this.props.pmp.blockTypeCreditCard}
                            // Data
                            pmp={this.state.pmp}
                            cda={this.state.cda}
                            segment={this.state.segment}
                            numero_cuotas={this.state.numero_cuotas}
                            monto_cuota={this.state.monto_cuota}
                            tcea={this.state.tcea}
                            client={this.state.client}
                            origination={ this.state.origination }
                            creditCard={this.state.creditCard}
                            lineAvailable={this.state.finalLineAvailable}
                            effectiveProvision={this.state.finalEffectiveProvision}
                            lineSAE={this.state.finalLineSAE}
                            maxAmount={this.state.finalMaxAmount}
                            pctSAE={this.state.pctSAE}
                            isSAEProcess={this.state.isSAEProcess}
                            // Function
                            handleProductDetail={this.handleProductDetail}
                            handleOpenModalCancelExpress={this.handleOpenModalCancelExpress}
                            handleCancelExpress={this.handleCancelExpress}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext}
                        />
                    </SnackbarProvider>
                </div>
            case 5:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <OfferSummary
                            // State Redux
                            offerSummary={this.props.odc.offerSummary}
                            // Data
                            origination={this.state.origination}
                            client={this.state.client}
                            creditCard={this.state.creditCard}
                            lineAvailable={this.state.finalLineAvailable}
                            effectiveProvision={this.state.finalEffectiveProvision}
                            isSAEProcess={this.state.isSAEProcess}
                            numero_cuotas={this.state.numero_cuotas}
                            monto_cuota={this.state.monto_cuota}
                            lineSAE={this.state.finalLineSAE}
                            maxAmount={this.state.finalMaxAmount}
                            tcea={this.state.tcea}
                            // Function
                            handleOfferSummary={this.handleOfferSummary}
                            handleNext={this.handleNext}
                        />
                    </SnackbarProvider>
                </div>
            case 6:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <EmbossingCreditCard
                            // State Redux
                            pendingEmbossing={this.props.odc.pendingEmbossing}
                            genericActivity={this.props.odcActivity.genericActivity}
                            // Data
                            creditCard={this.state.creditCard}
                            client={this.state.client}
                            // Function
                            handleEmbossingCreditCard={this.handleEmbossingCreditCard}
                            handlePendingEmbossingCreditCard={this.handlePendingEmbossingCreditCard}
                            handleNext={this.handleNext}
                            handleReset={this.handleReset}
                        />
                    </SnackbarProvider>
                </div>
            case 7:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <ActivationCreditCard
                            // State Redux
                            simpleGenericActivity={this.props.odcActivity.simpleGenericActivity}
                            genericActivity={this.props.odcActivity.genericActivity}
                            blockTypeCreditCard={this.props.pmp.blockTypeCreditCard}
                            responseBioZytrustService={this.props.zytrust.responseBioZytrustService}
                            // Data
                            origination={this.state.origination}
                            client={this.state.client}
                            isBiometricOk={this.state.isBiometricOk}
                            isSAEProcess={this.state.isSAEProcess}
                            // Function
                            handleBiometricZytrustService={this.handleBiometricZytrustService}
                            handleActivationCreditCard={this.handleActivationCreditCard}
                            handlePendingActivationCredit={this.handlePendingActivationCredit}
                            handleNext={this.handleNext}
                            handleReset={this.handleReset}
                            disbursementTypeId={this.state.disbursementTypeId}
                        />
                    </SnackbarProvider>
                </div>
            default:
                return <div>
                    <PreLoaderImage />
                </div>;
        }
    }
    handleIconButtonHistory = _ => {
        this.setState(state => ({
            ...state,
            showHistory: !state.showHistory
        }));
    }

    handleIconButtonVerifyMail = _ => {
        this.setState(state => ({
            ...state,
            showVerifyEmail: !state.showVerifyEmail
        }));
    }

    render() {
        const { classes, odcActivity } = this.props;
        const steps = this.getSteps();
        const { activeStep, redirectError } = this.state;
        if (redirectError) {
            return <Redirect to={{ pathname: `/${URL_BASE}/error`, }} />
        }
        return (
            <Hotkeys
                keyName="shift+i, ctrl+i"
                onKeyUp={this.handleIconButtonHistory}
            >
                <div className={classNames(classes.root)}>
                    {
                        activeStep > 6 &&
                        <SnackbarProvider maxSnack={3}>
                            <Timer />
                        </SnackbarProvider>
                    }
                    {
                        (this.state.isSAEProcess && activeStep === 6) || (!this.state.isSAEProcess && (activeStep >= 6 && activeStep <= 7)) ?
                            <SnackbarProvider maxSnack={3}>
                                <VerifyEmail
                                    client={this.state.client}
                                    origination={this.state.origination}
                                    onClick={this.handleIconButtonVerifyMail}
                                    showVerifyEmail={this.state.showVerifyEmail}
                                />
                            </SnackbarProvider> : null
                    }
                    <HistoryPanel
                        onClick={this.handleIconButtonHistory}
                        showHistory={this.state.showHistory}
                        effectiveProvision={this.state.effectiveProvision}
                        finalEffectiveProvision={this.state.finalEffectiveProvision}
                        lineAvailable={this.state.lineAvailable}
                        finalLineAvailable={ this.state.isSAEProcess ? -1 : this.state.finalLineAvailable}
                        creditCard={this.state.creditCard}
                        client={this.state.client}
                        origination={this.state.origination}
                        numero_cuotas={this.state.numero_cuotas}
                        monto_cuota={this.state.monto_cuota}
                        tcea={this.state.tcea}
                        lineSAE={ this.state.isSAEProcess ? this.state.finalLineSAE : -1 }
                    />
                    <div className={classes.NewCommentForm}>
                        <SnackbarProvider maxSnack={3}>
                            <NewCommentForm
                                fixed={true}
                                codSolicitud={this.state.origination.number}
                                codSolicitudCompleta={this.state.origination.fullNumber}
                            />
                        </SnackbarProvider>
                    </div>
                    {/* Container Steps */}
                    <Stepper activeStep={activeStep} orientation="vertical">
                        {
                            steps.map((label, index) => (
                                <Step key={label}>
                                    {/* Title Step */}
                                    <StepLabel>
                                        <Fade>
                                            <Typography
                                                color={"primary"}
                                                className={classNames(classes.stepLabelOk, "py-2 w-100 text-uppercase ")}>
                                                {label}
                                            </Typography>
                                        </Fade>
                                    </StepLabel>
                                    {/* Content Step */}
                                    <StepContent>
                                        {
                                            <div className="py-4">
                                                {this.getStepContent(index)}
                                            </div>
                                        }
                                        {/* Button Prev - Next */}
                                        <div className={classNames(classes.actionsContainer, "d-none")}>
                                            <div>
                                                <Button
                                                    disabled={activeStep === 0}
                                                    onClick={this.handleBack}
                                                    className={classes.button}
                                                >
                                                    Atrás
                                                </Button>

                                                <Button
                                                    variant="contained"
                                                    color="primary"
                                                    onClick={this.handleNext}
                                                    className={classes.button}
                                                >
                                                    {activeStep === steps.length - 1 ? 'Finalizar' : 'Siguiente'}
                                                </Button>
                                            </div>
                                        </div>
                                    </StepContent>
                                </Step>
                            ))}
                    </Stepper>

                    {activeStep === -1 &&
                        <Paper square elevation={0} className={classes.resetContainer}>
                            <PreLoaderImage />
                        </Paper>
                    }
                    {activeStep === steps.length && (
                        <Paper square elevation={0} className={classes.resetContainer}>
                            <SnackbarProvider maxSnack={3}>
                                <BackgroundFinish data={[
                                    { client: this.state.client, process: this.state.origination }
                                ]}
                                isSAEProcess={ this.state.isSAEProcess } />
                            </SnackbarProvider>
                            <br />
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={this.handleReset}
                                className={classes.button}>
                                Nueva Originación
                            </Button>
                        </Paper>
                    )}
                    {/*  Modal Cancel Express */}
                    <Dialog
                        onClose={this.handleCloseModalCancelExpress}
                        open={this.state.openModal}
                        maxWidth='xs'
                        aria-labelledby="form-dialog">
                        <DialogTitle
                            id="form-dialog"
                            className="bg-metal-blue">
                            <Typography align="center" component="span" variant="h6" className="text-white text-shadow-black">
                                Cancelar Proceso de Originación
                        </Typography>
                        </DialogTitle>

                        <form onSubmit={this.handleSubmitCancelExpress} autoComplete="off">
                            <DialogContent>
                                <Typography align="center">
                                    ¿Esta seguro de cancelar la originación?
                            </Typography>
                            </DialogContent>
                            <DialogActions>
                                <Grid container spacing={8}>
                                    <Grid item xs={6}>
                                        <div className={classes.buttonProgressWrapper}>
                                            <Button
                                                disabled={odcActivity.cancelActivity.loading}
                                                className={classes.button}
                                                fullWidth={true}
                                                color="secondary"
                                                size="small"
                                                onClick={this.handleCloseModalCancelExpress}
                                                margin="normal">
                                                No
                                        </Button>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div className={classes.buttonProgressWrapper}>
                                            <Button
                                                disabled={odcActivity.cancelActivity.loading}
                                                className={classes.button}
                                                type="submit"
                                                margin="normal"
                                                color="primary"
                                                size="small"
                                                fullWidth={true}>
                                                Si
                                            <SendIcon fontSize="small" className="ml-2" />
                                                {
                                                    odcActivity.cancelActivity.loading && <CircularProgress size={24} className={classes.buttonProgress} />
                                                }
                                            </Button>
                                        </div>
                                    </Grid>
                                </Grid>
                            </DialogActions>
                        </form>
                    </Dialog>
                </div>
            </Hotkeys>
        );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ExpressPage)));
