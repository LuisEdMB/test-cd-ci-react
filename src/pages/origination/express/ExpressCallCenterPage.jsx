﻿import React, { Component } from 'react';
import classNames from 'classnames';
// import * as moment from 'moment';
import { withRouter, Redirect, } from 'react-router-dom';
// Shorcurt
import Hotkeys from 'react-hot-keys';
// Notistack
import { SnackbarProvider } from 'notistack';
// Redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components
import {
    withStyles,
    Stepper,
    Step,
    StepLabel,
    StepContent,
    Button,
    Paper,
    Typography,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Grid,
    CircularProgress
} from '@material-ui/core';
//Steps
import PreLoaderImage from '../../../components/PreLoaderImage';
import ConsultClient from './components/consult-client/ConsultClient';
import ClientDetailPreEvaluated from './components/client-detail-pre-evaluated/ClientDetailPreEvaluated';
import ClientRegister from './components/client-register/ClientRegister';
import CommercialOffer from './components/commercial-offer/CommercialOffer';
import ProductDetail from './components/product-detail/ProductDetail';
import OfferSummary from './components/offer-summary/OfferSummary';
import BackgroundFinish from '../../../components/background-finish/BackgroundFinish';
import HistoryPanel from './components/history-panel/HistoryPanel';
import NewCommentForm from '../../../components/NewCommentForm/NewCommentForm'
// Actions
import { getConstantODC } from '../../../actions/generic/constant';
import {
    firstCall,
    secondCall,
    commercialOffer,
    offerSummary,
    verifySolicitudeIsBlocked,
    blockSolicitude,
    unlockSolicitude
} from '../../../actions/odc-express/odc';
import { cancelActivity, genericActivity, simpleGenericActivity } from '../../../actions/odc-express/odc-activity';

import {
    createClient,
    updateClient,
    createRelationship,
    createAccount,
    blockTypeCreditCard,
    createCreditCard
    
} from '../../../actions/pmp/pmp';
import * as ActionPci from '../../../actions/pci/pci'
import * as ActionOdcMaster from '../../../actions/odc-master/odc-master'
// Utils
import { SmoothScroll } from '../../../utils/Utils'
import {
    sendDataSecondCall,
    setDataCreateClientPMP,
    setDataUpdateClientePMP,
    setDataCreateRelationshipPMP,
    setDataCreateAccountPMP,
    setDataCreateCreditCard
} from '../../../utils/origination/Origination'
import {
    sendDataSimpleGenericActivityStepClientRegister,
    sendDataCommercialOffer,
    sendDataSimpleGenericActivityStepProductDetail,
    sendDataOfferSummary,
    sendDataCreateClientPMP,
    sendDataUpdateClientePMP,
    sendDataCreateRelationshipPMP,
    sendDataCreateAccountPMP,
    sendDataCreateCreditCard,
    sendDataAcceptedOrigination
} from '../../../utils/origination/Express'
import {
    sendSimpleGenericActivityStepClientRegisterCE,
    sendDataCommercialOfferCE,
    sendDataSimpleGenericActivityStepProductDetailCE,
    sendDataOfferSummaryCE,
    sendDataCreateClientPMPCE,
    sendDataUpdateClientePMPCE,
    sendDataCreateRelationshipPMPCE,
    sendDataCreateAccountPMPCE,
    sendDataCreateCreditCardCE,
    sendDataAcceptedOriginationCE
} from '../../../utils/origination/Efectivo'
import * as MasterOrigination from '../../../utils/origination/MasterOrigination'
// Config
import { URL_BASE } from '../../../actions/config';
// Effects
import Fade from 'react-reveal/Fade';
// Icons
import SendIcon from '@material-ui/icons/Send';
// Colors
import { green } from '@material-ui/core/colors';
// Utils
import * as Utils from '../../../utils/Utils'
import * as Constants from '../../../utils/Constants'

const styles = theme => ({
    root: {
        width: '100%',
        position: "relative"
    },
    stepLabelOk: {
        background: "#2196f31c",
        padding: ".5em",
        borderRadius: ".5em",
        boxShadow: "0px 1.2px 1px #007ab8"
    },
    stepLabelDefault: {
        background: "#f0f0f080",
        padding: ".5em",
        borderRadius: "0 0 .8em 0",
        borderBottom: "1px solid gray"
    },
    button: {
        textTransform: 'none',
    },
    buttonProgressWrapper: {
        position: 'relative',
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    IconButton: {
        top: -37,
        left: -37,
        position: "absolute",
    },
    icon: {
        fontSize: 32
    },
    actionsContainer: {
        marginBottom: theme.spacing.unit * 2,
    },
    resetContainer: {
        padding: theme.spacing.unit * 3,
        textAlign: 'center'
    },
    historyPanel: {
        position: "fixed",
        top: "33.3vh",
        zIndex: 900
    },
    NewCommentForm: {
        position: "fixed",
        top: "20vh",
        right: "5px",
        zIndex: 900,
    },
    fontSize: {
        fontSize: 12
    }
});

const mapStateToProps = (state) => {
    return {
        odc: state.odcReducer,
        constantODC: state.constantODCReducer,
        preEvaluatedClient: state.preEvaluatedClientReducer,
        odcActivity: state.odcActivityReducer,
        pmp: state.pmpReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const actions = {
        getConstantODC: bindActionCreators(getConstantODC, dispatch),

        cancelActivity: bindActionCreators(cancelActivity, dispatch),
        genericActivity: bindActionCreators(genericActivity, dispatch),
        simpleGenericActivity: bindActionCreators(simpleGenericActivity, dispatch),

        firstCall: bindActionCreators(firstCall, dispatch),
        secondCall: bindActionCreators(secondCall, dispatch),
        commercialOffer: bindActionCreators(commercialOffer, dispatch),
        offerSummary: bindActionCreators(offerSummary, dispatch),

        createClient: bindActionCreators(createClient, dispatch),
        updateClient: bindActionCreators(updateClient, dispatch),
        createRelationship: bindActionCreators(createRelationship, dispatch),
        createAccount: bindActionCreators(createAccount, dispatch),
        createCreditCard: bindActionCreators(createCreditCard, dispatch),
        blockTypeCreditCard: bindActionCreators(blockTypeCreditCard, dispatch),

        verifySolicitudeIsBlocked: bindActionCreators(verifySolicitudeIsBlocked, dispatch),
        blockSolicitude: bindActionCreators(blockSolicitude, dispatch),
        unlockSolicitude: bindActionCreators(unlockSolicitude, dispatch),

        encryptCardnumber: bindActionCreators(ActionPci.encryptCardnumber, dispatch),

        registerClientIntoMaster: bindActionCreators(ActionOdcMaster.registerUpdateClient, dispatch),
        registerAccountIntoMaster: bindActionCreators(ActionOdcMaster.registerAccount, dispatch),
        registerCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.registerCreditCard, dispatch)
    };
    return actions;
}

const STATE = {
    activeStep: 0,
    showHistory: false,
    showVerifyEmail: true,
    listComments: [],
    origination: {
        number: 0,
        activityNumber: 0,
        fullNumber: "",
        previousStatePhaseFlow: 0,
        nextStatePhaseFlow: 0,
        typeSolicitudeCode: 340001,
        typeSolicitudeOriginationCode: 390001,
        typeSolicitudeReqCode: 0,
        rePrintMotiveCode: 0
    },
    client: {
        code: 0,
        id: 0,
        documentTypeErro: false,
        documentTypeId: "",
        documentTypeAux: "",
        documentTypeInternalValue: "",
        documentType: "",
        documentNumber: "",
        firstName: "",
        secondName: "",
        lastName: "",
        motherLastName: "",
        fullName: "",
        shortName: "",
        birthday: "",
        genderId: "",
        gender: "",
        genderInternalValue: "",
        nationalityId: "",
        nationality: "",
        maritalStatusId: "",
        maritalStatusInternalValue: "",
        maritalStatus: "",
        academicDegreeId: "",
        academicDegree: "",
        academicDegreeInternalValue: "",
        email: "",
        emailConfirmed: false,
        cellphone: "",
        telephone: "",
        homeAddress: {
            departmentId: "",
            department: "",
            provinceId: "",
            province: "",
            districtId: "",
            district: "",
            viaId: "",
            via: "",
            viaInternalValue: "",
            nameVia: "",
            number: "",
            building: "",
            inside: "",
            mz: "",
            lot: "",
            zoneId: "",
            zone: "",
            zoneInternalValue: "",
            nameZone: "",
            typeResidence: "",
            housingTypeId: "",
            housingType: "",
            housingTypeInternalValue: "",
            reference: "",
        },
        workAddress: {
            departmentId: "",
            department: "",
            provinceId: "",
            province: "",
            districtId: "",
            district: "",
            viaId: "",
            via: "",
            viaInternalValue: "",
            nameVia: "",
            number: "",
            building: "",
            inside: "",
            mz: "",
            lot: "",
            zoneId: "",
            zone: "",
            zoneInternalValue: "",
            nameZone: "",
            typeResidence: "",
            housingTypeId: "",
            housingType: "",
            reference: "",
        },
        employmentSituationId: "",
        employmentSituation: "",
        jobTitleId: "",
        jobTitle: "",
        economicActivityId: "",
        economicActivity: "",
        laborIncomeDate: "",
        businessOption: "",
        businessName: "",
        ruc: "",
        grossIncome: 0,
        workTelephoneExtension: "",
        workTelephone: "",
        extra: {
            paymentDate: "",
            paymentDateInternalValue: "",
            paymentDateId: "",
            sendCorrespondence: "",
            sendCorrespondenceId: "",
            accountStatusId: "",
            accountStatus: "",
            effectiveWithdrawal: "",
            alert: "",
            sendCommonPromotion: "",
            media: "",
            mediaId: "",
            promoterId: "",
            promoter: "",
            term: "",
        },
        relationTypeId: 330001
    },
    pmp: {
        clientNumber: "",
        relationshipNumber: "",
        accountNumber: "",
        cardNumber: "",
        parallelCardNumber: ""
    },
    cda: null,
    adn: null,
    siebel: null,
    fraudPrevention: null,
    sae: null,
    creditProducts: null,
    pct: {
        number: ""
    },
    creditCard: null,
    segment: "",
    effectiveProvision: 0,
    isSAEProcess: true,
    lineAvailable: {
        min: 0,
        value: 1500,
        max: 3000,
    },
    initialLineAvailable: {
        lineAvailableEc: 0,
        lineAvailable: 0,
        globalLineaAvailable: 0,
        lineAvailableTc: 0
    },
    lineSAE: -1,
    finalEffectiveProvision: 0,
    finalLineAvailable: -1,
    finalLineSAE: -1,
    finalMaxAmount: -1,
    pctSAE: "",
    cancel: false,
    redirectError: false,
    openModal: false,
    reintentRegisterUpdateClient: 0,
    reintentProcessPMP: 0,
    reintentBlockSolicitude: 0,
    reintentBlockTypeProcessPMP: 0,
    isBiometricOk: false,
    disbursementTypeId: 0,
    numero_cuotas: 0,
    monto_cuota: 0,
    tcea: 0
};
class ExpressPage extends Component {
    state = STATE;
    constructor(props) {
        super(props);
        this.handleUnload = this.handleUnload.bind(this);
    }
    componentWillUnmount() {
        window.removeEventListener('beforeunload', this.handleUnload);
    }
    handleUnload(e) {
        var message = "o/";
        (e || window.event).returnValue = message;
        return message;
    }
    componentDidMount() {
        this.props.getConstantODC();
        window.addEventListener('beforeunload', this.handleUnload);
    }
    componentDidUpdate = (prevProps) => {
        Utils.resolveStateRedux(prevProps.odc.firstCall, this.props.odc.firstCall, null,
            _ => {
                let { origination, client, cda, siebel, fraudPrevention, adn, validations, sae, creditProducts, initialLineAvailable } = this.props.odc.firstCall.data;
                if (this.redirectRegular({ cda, adn, validations, client })) {
                    this.props.history.push({
                        pathname: '/odc/regular/call-center-nueva-solicitud',
                        state: { documentNumber: client.documentNumber }
                    })
                } else {
                    if (origination && cda) {
                        this.setState(state => ({
                            ...state,
                            origination: {
                                ...state.origination,
                                ...origination
                            },
                            client: {
                                ...state.client,
                                ...client
                            },
                            cda: {
                                ...state.cda,
                                ...cda
                            },
                            adn: {
                                ...state.adn,
                                ...adn
                            },
                            siebel: {
                                ...state.siebel,
                                ...siebel
                            },
                            fraudPrevention: {
                                ...state.fraudPrevention,
                                ...fraudPrevention
                            },
                            validations: validations,
                            sae: sae,
                            creditProducts: creditProducts,
                            initialLineAvailable: initialLineAvailable
                        }));
                    }
                }
            })
    }
    // Evaluate and Redirect to Regular Process
    redirectRegular = (data) => {
        let { isPrevailed,
            PEP,
            PIB,
            isProcessSiebel,
            isProcessODC,
        } = Utils.evaluatePrevaluated(data);
        return (PIB === 1 && PEP === 1 && isPrevailed === 0 && isProcessODC === 0 && isProcessSiebel === 0)
    }
    // Open Modal Cancel
    handleOpenModalCancelExpress = () => {
        this.setState(state => ({
            ...state,
            openModal: true
        }));
    }
    // Close Modal Cancel
    handleCloseModalCancelExpress = () => {
        this.setState(state => ({
            ...state,
            openModal: false
        }));
    }
    // Cancel Express
    handleCancelExpress = () => {
        this.handleCloseModalCancelExpress();
        this.handleReset();
    }
    // Submit - Cancel Express
    handleSubmitCancelExpress = (e) => {
        e.preventDefault();
        let { origination, activeStep } = this.state;
        let cancel = this.selectStatePhaseFlowCancel(activeStep);
        let sendData = {
            cod_solicitud: origination.number,
            cod_solicitud_completa: origination.fullNumber,
            cod_flujo_fase_estado_actual: cancel.currentStatePhaseFlow,
            cod_flujo_fase_estado_anterior: cancel.previousStatePhaseFlow,
            nom_actividad: cancel.activityName
        }
        this.props.cancelActivity(sendData);
    }
    // Select State Phase Flow Cancel
    selectStatePhaseFlowCancel = (step) => {
        switch (step) {
            case 1: return {
                currentStatePhaseFlow: 10103,
                previousStatePhaseFlow: 10101,
                activityName: "Validación: Cancelado"
            }
            case 2: return {
                currentStatePhaseFlow: 10203,
                previousStatePhaseFlow: 10201,
                activityName: "Preevaluado: Cancelado"
            }
            case 3: return {
                currentStatePhaseFlow: 10303,
                previousStatePhaseFlow: 10301,
                activityName: "Oferta: Cancelado"
            }
            case 4: return {
                currentStatePhaseFlow: 10403,
                previousStatePhaseFlow: 10401,
                activityName: "Preventa: Cancelado"
            }
            default: return {
                currentStatePhaseFlow: 0,
                previousStatePhaseFlow: 0,
                activityName: ""
            }
        }
    }
    handleNext = () => {
        this.setState(state => ({
            activeStep: state.activeStep + 1,
        }), () => SmoothScroll(this.state.activeStep));
    }
    handleBack = () => {
        this.setState(state => ({
            activeStep: state.activeStep - 1,
        }), () => SmoothScroll(this.state.activeStep));
    }
    handleReset = () => {
        this.setState({ ...STATE },
            () => SmoothScroll(this.state.activeStep));
    }
    // Step 1 - Client Validation
    handleClientValidationForm = data => {
        let { client } = data;
        if (client) {
            //Send Data
            let sendData = {
                tipo_doc: client.documentTypeAux,
                nro_doc: client.documentNumber,
                tipo_doc_letra: client.documentType,
                cod_flujo_fase_estado_actual: 10101,
                cod_flujo_fase_estado_anterior: 10100,
                cod_solicitud: 0,
                cod_solicitud_completa: "",
                cod_cliente: 0,
                cod_tipo_solicitud: 340001,
                cod_tipo_relacion: 330001,
                cod_tipo_solicitud_requerimiento: 0,
                cod_etapa_maestro: Constants.MasterConfigurationStage.masterExpressStage
            }

            let sendDataExtra = {
                documentTypeId: client.documentTypeId,
                documentTypeInternalValue: client.documentTypeInternalValue
            }
            // Send first Call
            this.props.firstCall(sendData, sendDataExtra);
        }
    }
    // Step 2 - Pre-Evaluated Client
    handlePreEvaluatedClient = data => {
        if (data) {
            let { origination } = this.state;
            // Send Data
            let sendData = {
                cod_solicitud: origination.number,
                cod_solicitud_completa: origination.fullNumber,
                cod_flujo_fase_estado_actual: data.isSAEProcess ? 90102 : 10102,
                cod_flujo_fase_estado_anterior: 10101,
                nom_actividad: "Validación: Aceptado",
            }
            this.setState(state => ({
                ...state,
                client: {
                    ...state.client,
                    ...data.client
                },
                lineAvailable: { ...data.lineAvailable },
                cda: data.cda,
                isSAEProcess: data.isSAEProcess,
                effectiveProvision: data.effectiveProvision
            }));
            this.props.genericActivity(sendData, null, true);
        }
    }
    // Step 3 - Client Register
    handleClientRegister = ({ data, reset = false }) => {
        if (data) {
            const { reintentRegisterUpdateClient } = this.state
            const steps = this.getStepsRegisterUpdateClient()
            steps[reset ? 0 : reintentRegisterUpdateClient].action(data)
        }
    }
    getStepsRegisterUpdateClient = _ => ({
        0: {
            action: async data => {
                const sendData = sendDataSecondCall(data, this.state.origination, this.state.client.id, this.state.isSAEProcess)
                const result = await this.props.secondCall(sendData).then(response => response)
                if (result?.data?.success) {
                    this.setState(state => ({
                        ...state,
                        client: {
                            ...state.client,
                            ...data.client
                        },
                        cda: {
                            ...state.cda,
                            ...result.data.segundaLlamadaCda.solicitud.salidaCDA
                        },
                        reintentRegisterUpdateClient: 1
                    }), _ => this.handleClientRegister({ data }))
                }
            }
        },
        1: {
            action: async data => {
                const { origination, isSAEProcess } = this.state
                const sendData = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    activityName: MasterOrigination.registerUpdateClientActivity.activityName,
                    phaseCode: MasterOrigination.registerUpdateClientActivity.phaseCode,
                    masterStageCode: isSAEProcess
                        ? Constants.MasterConfigurationStage.masterCECStage
                        : Constants.MasterConfigurationStage.masterExpressStage
                }
                const result = await this.props.registerClientIntoMaster(sendData).then(response => response)
                if (result?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentRegisterUpdateClient: 2
                    }), _ => this.handleClientRegister({ data }))
                }
            }
        },
        2: {
            action: _ => {
                const sendData = this.state.isSAEProcess
                    ? sendSimpleGenericActivityStepClientRegisterCE(this.state.origination)
                    : sendDataSimpleGenericActivityStepClientRegister(this.state.origination)
                this.props.simpleGenericActivity(sendData, { retomar: true }, true)
            }
        }
    })
    // Step 4 - Commercial Offer
    handleCommercialOffer = data => {
        const { origination, client, lineAvailable, initialLineAvailable, sae, isSAEProcess } = this.state;
        if (data) {
            let sendData = {};
            if (isSAEProcess && sae && Object.keys(sae).length > 0) {
                sendData = sendDataCommercialOfferCE(data, lineAvailable, initialLineAvailable, origination, client, sae);
            }
            else {
                sendData = sendDataCommercialOffer(data, lineAvailable, initialLineAvailable, origination, client);
            }
            this.props.commercialOffer(sendData)
                .then(response => {
                    if (response.data) {
                        this.setState(state => ({
                            ...state,
                            client: {
                                ...state.client,
                                ...data.client
                            },
                            creditCard: {
                                ...state.creditCard,
                                ...data.creditCard
                            },
                            finalEffectiveProvision: data.finalEffectiveProvision ? data.finalEffectiveProvision : 0,
                            finalLineAvailable: data.finalLineAvailable ? data.finalLineAvailable : 0,
                            finalLineSAE: data.finalLineSAE ? data.finalLineSAE : 0,
                            finalMaxAmount: data.finalMaxAmount ? data.finalMaxAmount : 0,
                            pctSAE: data.pctSAE ? data.pctSAE : "",
                            isSAE: data.isSAE,
                            numero_cuotas: data.numberFees ? data.numberFees : '',
                            monto_cuota: data.feeAmount ? data.feeAmount : '',
                            tcea: data.tcea,
                            segment: data.segment ? data.segment : "",
                            disbursementTypeId: data.disbursementTypeId ? data.disbursementTypeId : 0
                        }));
                    }
                });
        }
    }
    // Step 5 - Product Detail
    handleProductDetail = async data => {
        let {
            origination,
            reintentProcessPMP,
            reintentBlockSolicitude,
            client,
            creditCard,
            finalLineAvailable,
            initialLineAvailable,
            pmp,
            isSAEProcess
        } = this.state;
        let {
            pct
        } = data;
        let organization = "641";
        if (reintentBlockSolicitude === 0) {
            const sendData = {
                solicitudeCode: origination.number,
                completeSolicitudeCode: origination.fullNumber,
                clientCode: client.id,
                documentTypeId: client.documentTypeAux,
                documentNumber: client.documentNumber,
                lineAvailable: finalLineAvailable,
                globalLineAvailable: initialLineAvailable.globalLineaAvailable
            }
            this.props.blockSolicitude(sendData).then(response => {
                if (response?.success)
                    this.setState(state => ({
                        ...state,
                        reintentBlockSolicitude: 1
                    }), _ => this.handleProductDetail(data))
            })
        }
        if (reintentBlockSolicitude === 1) {
            // Init - Start
            if (reintentProcessPMP === 0) {
                let sendData = this.state.isSAEProcess
                    ? sendDataSimpleGenericActivityStepProductDetailCE(origination)
                    : sendDataSimpleGenericActivityStepProductDetail(origination)
                this.props.genericActivity(sendData)
                    .then(response => {
                        if (response?.data?.success) {
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 1
                            }), _ => this.handleProductDetail(data))
                        }
                    })
            }
            // Init - PMP - Create Client
            if (reintentProcessPMP === 1) {
                // Send Data
                let sendData = this.state.isSAEProcess
                    ? sendDataCreateClientPMPCE(organization, origination, client)
                    : sendDataCreateClientPMP(organization, origination, client)

                // Set Data Request PMP - Client
                let sendDataPMP = setDataCreateClientPMP(sendData);
                this.props.createClient(sendDataPMP)
                    .then(response => {
                        if (response?.data) {
                            if (response.data.errorCode === "12") {
                                this.setState(state => ({
                                    ...state,
                                    reintentProcessPMP: 1.5
                                }), _ => this.handleProductDetail(data))
                            } else if (response.data.success) {
                                const { pmpCrearCliente } = response.data
                                if (pmpCrearCliente) {
                                    const { createCustomer } = pmpCrearCliente
                                    this.setState(state => ({
                                        ...state,
                                        reintentProcessPMP: 2,
                                        pmp: {
                                            ...pmp,
                                            clientNumber: createCustomer.customerNumber
                                        }
                                    }), _ => this.handleProductDetail(data))
                                }
                            }
                        }
                    })
            }
            // Init - PMP - Update Client
            if (reintentProcessPMP === 1.5) {
                // Send Data
                let sendData = this.state.isSAEProcess
                    ? sendDataUpdateClientePMPCE(organization, origination, client)
                    : sendDataUpdateClientePMP(organization, origination, client);
                // Set Data Request PMP - Client
                let sendDataPMP = setDataUpdateClientePMP(sendData);
                this.props.updateClient(sendDataPMP)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpActualizarCliente } = response.data
                            if (pmpActualizarCliente) {
                                const { updateCustomer } = pmpActualizarCliente
                                this.setState(state => ({
                                    ...state,
                                    reintentProcessPMP: 2,
                                    pmp: {
                                        ...pmp,
                                        clientNumber: updateCustomer.customerNumber
                                    }
                                }), _ => this.handleProductDetail(data))
                            }
                        }
                    });
            }
            // Init - PMP - Create Relationship
            if (reintentProcessPMP === 2) {
                // Send Data
                let sendData = this.state.isSAEProcess
                    ? sendDataCreateRelationshipPMPCE({
                        organization,
                        origination,
                        client,
                        pmp,
                        finalLineAvailable})
                    : sendDataCreateRelationshipPMP({
                        organization,
                        origination,
                        client,
                        pmp,
                        finalLineAvailable});
                // Set Data Request PMP - Relationship
                let sendDataPMP = setDataCreateRelationshipPMP(sendData);
                this.props.createRelationship(sendDataPMP)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpCrearRelacion } = response.data
                            const { createCustomerRelationship } = pmpCrearRelacion
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 3,
                                pmp: {
                                    ...state.pmp,
                                    relationshipNumber: createCustomerRelationship.relationshipNumber
                                }
                            }), _ => this.handleProductDetail(data))
                        }
                    });
            }
            // Init - PMP - Create Account
            if (reintentProcessPMP === 3) {
                // Send Data
                let sendData = this.state.isSAEProcess
                    ? sendDataCreateAccountPMPCE({
                        organization,
                        origination,
                        client,
                        creditCard,
                        pmp,
                        pct,
                        finalLineAvailable})
                    : sendDataCreateAccountPMP({
                        organization,
                        origination,
                        client,
                        creditCard,
                        pmp,
                        pct,
                        finalLineAvailable})
                // Set Data Request PMP - Account
                let sendDataPMP = setDataCreateAccountPMP(sendData);
                this.props.createAccount(sendDataPMP)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpCrearCuenta } = response.data
                            const { createCustomerAccount } = pmpCrearCuenta
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 4,
                                pmp: {
                                    ...state.pmp,
                                    accountNumber: createCustomerAccount.accountNumber
                                }
                            }), _ => this.handleProductDetail(data))
                        }
                    });
            }
            // Master - Create Account
            if (reintentProcessPMP === 4) {
                const sendData = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    activityName: MasterOrigination.registerAccountActivity.activityName,
                    phaseCode: MasterOrigination.registerAccountActivity.phaseCode,
                    masterStageCode: isSAEProcess
                        ? Constants.MasterConfigurationStage.masterCECStage
                        : Constants.MasterConfigurationStage.masterExpressStage
                }
                const result = await this.props.registerAccountIntoMaster(sendData).then(response => response)
                if (result?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentProcessPMP: 5
                    }), _ => this.handleProductDetail(data))
                }
            }
            // Init - PMP - Create Credit Card
            if (reintentProcessPMP === 5) {
                // Send Data
                let sendData = this.state.isSAEProcess
                    ? sendDataCreateCreditCardCE({
                        organization,
                        origination,
                        client,
                        creditCard,
                        pmp,
                        pct,
                        finalLineAvailable})
                    : sendDataCreateCreditCard({
                        organization,
                        origination,
                        client,
                        creditCard,
                        pmp,
                        pct,
                        finalLineAvailable})

                // Set Data Request PMP - Credit Card
                let sendDataPMP = setDataCreateCreditCard(sendData);
                this.props.createCreditCard(sendDataPMP)
                    .then(response => {
                        if (response?.data?.success) {
                            const { pmpCrearTarjetaCredito } = response.data
                            const { createCreditCard } = pmpCrearTarjetaCredito
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 6,
                                pmp: {
                                    ...state.pmp,
                                    cardNumber: createCreditCard.cardNumber
                                },
                                creditCard: {
                                    ...state.creditCard,
                                    cardNumber: createCreditCard.cardNumber
                                }
                            }), _ => this.handleProductDetail(data))
                        }
                    });
            }
            // Encrypting Cardnumber
            if (reintentProcessPMP === 6) {
                const sendDataPci = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    pciStage: Constants.PciConfigurationStage.pciExpressStage
                }
                this.props.encryptCardnumber(sendDataPci)
                    .then(response => {
                        if (response?.success || response?.errorCode === Constants.CustomErrorCode.errorCodeCreditCardAlreadyPci) {
                            this.setState(state => ({
                                ...state,
                                reintentProcessPMP: 7
                            }), _ => this.handleProductDetail(data))
                        }
                    })
            }
            // Master - Create Credit Card
            if (reintentProcessPMP === 7) {
                const sendData = {
                    solicitudeCode: origination.number,
                    completeSolicitudeCode: origination.fullNumber,
                    activityName: MasterOrigination.registerCreditCardActivity.activityName,
                    phaseCode: MasterOrigination.registerCreditCardActivity.phaseCode,
                    masterStageCode: isSAEProcess
                        ? Constants.MasterConfigurationStage.masterCECStage
                        : Constants.MasterConfigurationStage.masterExpressStage
                }
                const result = await this.props.registerCreditCardIntoMaster(sendData).then(response => response)
                if (result?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentProcessPMP: 8
                    }), _ => this.handleProductDetail(data))
                }
            }
            // Init - End
            if (reintentProcessPMP === 8) {
                let sendData = this.state.isSAEProcess
                    ? sendDataAcceptedOriginationCE(origination)
                    : sendDataAcceptedOrigination(origination);
                this.props.genericActivity(sendData, null, true).then(response => {
                    if (response?.data?.success)
                        this.setState(state => ({
                            ...state,
                            reintentBlockSolicitude: 2
                        }), _ => this.handleProductDetail(data))
                });
            }
        }
        if (reintentBlockSolicitude === 2) {
            const sendData = {
                solicitudeCode: origination.number,
                completeSolicitudeCode: origination.fullNumber,
                clientCode: client.id,
                documentTypeId: client.documentTypeAux,
                documentNumber: client.documentNumber
            }
            this.props.unlockSolicitude(sendData)
        }
    }
    // Step 6 - Offer Summary
    handleOfferSummary = _ => {
        let { origination, creditCard, lineAvailable, finalLineAvailable, finalEffectiveProvision } = this.state;
        if (origination && creditCard && finalLineAvailable >= 0 && finalEffectiveProvision >= 0) {
            let sendData = this.state.isSAEProcess
                ? sendDataOfferSummaryCE(origination, creditCard, finalEffectiveProvision, lineAvailable, finalLineAvailable)
                : sendDataOfferSummary(origination, creditCard, finalEffectiveProvision, lineAvailable, finalLineAvailable);
            this.props.offerSummary(sendData);
        }
    }
    getSteps = () => {
        return this.state.isSAEProcess ?
            [
                'Originación - Consulta cliente',
                'Validación Cliente',
                'Registro del Cliente',
                'Oferta Comercial',
                'Detalle Producto',
                'Resumen Oferta'
            ] :
            [
                'Originación - Consulta cliente',
                'Validación Cliente',
                'Registro del Cliente',
                'Oferta Comercial',
                'Detalle Producto',
                'Resumen Oferta'
            ];

    }
    getStepContent = (step) => {
        switch (step) {
            case 0:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <ConsultClient
                            firstCall={this.props.odc.firstCall}
                            handleNext={this.handleNext}
                            handleClientValidationForm={this.handleClientValidationForm} />
                    </SnackbarProvider>
                </div>
            case 1:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <ClientDetailPreEvaluated
                            cancelActivity={this.props.odcActivity.cancelActivity}
                            genericActivity={this.props.odcActivity.genericActivity}
                            origination={this.state.origination}
                            client={this.state.client}
                            cda={this.state.cda}
                            adn={this.state.adn}
                            siebel={this.state.siebel}
                            fraudPrevention={this.state.fraudPrevention}
                            validations={this.state.validations}
                            sae={this.state.sae}
                            creditProducts={this.state.creditProducts}
                            initialLineAvailable={ this.state.initialLineAvailable }
                            constantODC={ this.props.constantODC }
                            history={this.props.history}
                            handlePreEvaluatedClient={this.handlePreEvaluatedClient}
                            handleOpenModalCancelExpress={this.handleOpenModalCancelExpress}
                            handleCancelExpress={this.handleCancelExpress}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext}
                        />
                    </SnackbarProvider>
                </div>
            case 2:
                return <div>
                    <SnackbarProvider maxSnack={6}>
                        <ClientRegister
                            cancelActivity={this.props.odcActivity.cancelActivity}
                            simpleGenericActivity={this.props.odcActivity.simpleGenericActivity}
                            secondCall={this.props.odc.secondCall}
                            client={this.state.client}
                            isSAEProcess={this.state.isSAEProcess}
                            origination={ this.state.origination }
                            callCenter
                            handleClientRegister={this.handleClientRegister}
                            handleOpenModalCancelExpress={this.handleOpenModalCancelExpress}
                            handleCancelExpress={this.handleCancelExpress}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext} />
                    </SnackbarProvider>
                </div>
            case 3:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <CommercialOffer
                            cancelActivity={this.props.odcActivity.cancelActivity}
                            cda={this.state.cda}
                            client={this.state.client}
                            lineAvailable={this.state.lineAvailable}
                            sae={this.state.sae}
                            isSAEProcess={this.state.isSAEProcess}
                            commercialOffer={this.props.odc.commercialOffer}
                            handleCommercialOffer={this.handleCommercialOffer}
                            handleOpenModalCancelExpress={this.handleOpenModalCancelExpress}
                            handleCancelExpress={this.handleCancelExpress}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext}
                        />
                    </SnackbarProvider>
                </div>
            case 4:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <ProductDetail
                            cancelActivity={this.props.odcActivity.cancelActivity}
                            genericActivity={this.props.odcActivity.genericActivity}
                            createClient={this.props.pmp.createClient}
                            updateClient={this.props.pmp.updateClient}
                            createRelationship={this.props.pmp.createRelationship}
                            createAccount={this.props.pmp.createAccount}
                            createCreditCard={this.props.pmp.createCreditCard}
                            blockTypeCreditCard={this.props.pmp.blockTypeCreditCard}
                            pmp={this.state.pmp}
                            cda={this.state.cda}
                            segment={this.state.segment}
                            numero_cuotas={this.state.numero_cuotas}
                            monto_cuota={this.state.monto_cuota}
                            tcea={this.state.tcea}
                            client={this.state.client}
                            origination={ this.state.origination }
                            creditCard={this.state.creditCard}
                            lineAvailable={this.state.finalLineAvailable}
                            effectiveProvision={this.state.finalEffectiveProvision}
                            lineSAE={this.state.finalLineSAE}
                            maxAmount={this.state.finalMaxAmount}
                            pctSAE={this.state.pctSAE}
                            isSAEProcess={this.state.isSAEProcess}
                            handleProductDetail={this.handleProductDetail}
                            handleOpenModalCancelExpress={this.handleOpenModalCancelExpress}
                            handleCancelExpress={this.handleCancelExpress}
                            handleReset={this.handleReset}
                            handleNext={this.handleNext}
                        />
                    </SnackbarProvider>
                </div>
            case 5:
                return <div>
                    <SnackbarProvider maxSnack={3}>
                        <OfferSummary
                            offerSummary={this.props.odc.offerSummary}
                            origination={this.state.origination}
                            client={this.state.client}
                            creditCard={this.state.creditCard}
                            lineAvailable={this.state.finalLineAvailable}
                            effectiveProvision={this.state.finalEffectiveProvision}
                            isSAEProcess={this.state.isSAEProcess}
                            numero_cuotas={this.state.numero_cuotas}
                            monto_cuota={this.state.monto_cuota}
                            lineSAE={this.state.finalLineSAE}
                            maxAmount={this.state.finalMaxAmount}
                            tcea={this.state.tcea}
                            handleOfferSummary={this.handleOfferSummary}
                            handleNext={this.handleNext}
                        />
                    </SnackbarProvider>
                </div>
            default:
                return <div>
                    <PreLoaderImage />
                </div>;
        }
    }
    handleIconButtonHistory = _ => {
        this.setState(state => ({
            ...state,
            showHistory: !state.showHistory
        }));
    }
    render() {
        const { classes, odcActivity } = this.props;
        const steps = this.getSteps();
        const { activeStep, redirectError } = this.state;
        if (redirectError) {
            return <Redirect to={{ pathname: `/${URL_BASE}/error`, }} />
        }
        return (
            <Hotkeys
                keyName="shift+i, ctrl+i"
                onKeyUp={this.handleIconButtonHistory}
            >
                <div className={classNames(classes.root)}>
                    <HistoryPanel
                        onClick={this.handleIconButtonHistory}
                        showHistory={this.state.showHistory}
                        effectiveProvision={this.state.effectiveProvision}
                        finalEffectiveProvision={this.state.finalEffectiveProvision}
                        lineAvailable={this.state.lineAvailable}
                        finalLineAvailable={ this.state.isSAEProcess ? -1 : this.state.finalLineAvailable}
                        creditCard={this.state.creditCard}
                        client={this.state.client}
                        origination={this.state.origination}
                        numero_cuotas={this.state.numero_cuotas}
                        monto_cuota={this.state.monto_cuota}
                        tcea={this.state.tcea}
                        lineSAE={ this.state.isSAEProcess ? this.state.finalLineSAE : -1 }
                    />
                    <div className={classes.NewCommentForm}>
                        <SnackbarProvider maxSnack={3}>
                            <NewCommentForm
                                fixed={true}
                                codSolicitud={this.state.origination.number}
                                codSolicitudCompleta={this.state.origination.fullNumber}
                            />
                        </SnackbarProvider>
                    </div>
                    <Stepper activeStep={activeStep} orientation="vertical">
                        {
                            steps.map((label, index) => (
                                <Step key={label}>
                                    <StepLabel>
                                        <Fade>
                                            <Typography
                                                color={"primary"}
                                                className={classNames(classes.stepLabelOk, "py-2 w-100 text-uppercase ")}>
                                                {label}
                                            </Typography>
                                        </Fade>
                                    </StepLabel>
                                    <StepContent>
                                        {
                                            <div className="py-4">
                                                {this.getStepContent(index)}
                                            </div>
                                        }
                                        <div className={classNames(classes.actionsContainer, "d-none")}>
                                            <div>
                                                <Button
                                                    disabled={activeStep === 0}
                                                    onClick={this.handleBack}
                                                    className={classes.button}
                                                >
                                                    Atrás
                                                </Button>

                                                <Button
                                                    variant="contained"
                                                    color="primary"
                                                    onClick={this.handleNext}
                                                    className={classes.button}
                                                >
                                                    {activeStep === steps.length - 1 ? 'Finalizar' : 'Siguiente'}
                                                </Button>
                                            </div>
                                        </div>
                                    </StepContent>
                                </Step>
                            ))}
                    </Stepper>

                    {activeStep === -1 &&
                        <Paper square elevation={0} className={classes.resetContainer}>
                            <PreLoaderImage />
                        </Paper>
                    }
                    {activeStep === steps.length && (
                        <Paper square elevation={0} className={classes.resetContainer}>
                            <SnackbarProvider maxSnack={3}>
                                <BackgroundFinish
                                    callCenter />
                            </SnackbarProvider>
                            <br />
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={this.handleReset}
                                className={classes.button}>
                                Nueva Originación
                            </Button>
                        </Paper>
                    )}
                    <Dialog
                        onClose={this.handleCloseModalCancelExpress}
                        open={this.state.openModal}
                        maxWidth='xs'
                        aria-labelledby="form-dialog">
                        <DialogTitle
                            id="form-dialog"
                            className="bg-metal-blue">
                            <Typography align="center" component="span" variant="h6" className="text-white text-shadow-black">
                                Cancelar Proceso de Originación
                        </Typography>
                        </DialogTitle>

                        <form onSubmit={this.handleSubmitCancelExpress} autoComplete="off">
                            <DialogContent>
                                <Typography align="center">
                                    ¿Esta seguro de cancelar la originación?
                            </Typography>
                            </DialogContent>
                            <DialogActions>
                                <Grid container spacing={8}>
                                    <Grid item xs={6}>
                                        <div className={classes.buttonProgressWrapper}>
                                            <Button
                                                disabled={odcActivity.cancelActivity.loading}
                                                className={classes.button}
                                                fullWidth={true}
                                                color="secondary"
                                                size="small"
                                                onClick={this.handleCloseModalCancelExpress}
                                                margin="normal">
                                                No
                                        </Button>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div className={classes.buttonProgressWrapper}>
                                            <Button
                                                disabled={odcActivity.cancelActivity.loading}
                                                className={classes.button}
                                                type="submit"
                                                margin="normal"
                                                color="primary"
                                                size="small"
                                                fullWidth={true}>
                                                Si
                                            <SendIcon fontSize="small" className="ml-2" />
                                                {
                                                    odcActivity.cancelActivity.loading && <CircularProgress size={24} className={classes.buttonProgress} />
                                                }
                                            </Button>
                                        </div>
                                    </Grid>
                                </Grid>
                            </DialogActions>
                        </form>
                    </Dialog>
                </div>
            </Hotkeys>
        );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ExpressPage)));
