import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
// React Router 
import { connect } from 'react-redux';
// Component
import{ 
    IconButton,
    Grid, 
    LinearProgress,
    Divider
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close'; 
// Components Custom 
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
import Header from './components/Header';
import FormPanel from './components/FormPanel';

// Actions 
import { pendingProcessesCancellation } from '../../../../actions/odc-express/odc';
import { bindActionCreators } from "redux";
// Utils
import * as Utils from '../../../../utils/Utils'

const styles = theme => ({
    avatar:{
        width:80, 
        height:48
    },
    modalRoot:{
        
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state) => {
    return {
        odc: state.odcReducer,
        pmp: state.pmpReducer,
        odcExistingClient: state.odcExistingClientReducer,
        pci: state.pciReducer,
        odcMaster: state.odcMasterReducer
    }
}
const mapDispatchToProps = (dispatch) => {
    const actions = {
        pendingProcessesCancellation: bindActionCreators(pendingProcessesCancellation, dispatch), 
    };
    return actions;
}

class AuthorizePage extends Component {

    state = {
        defaultColumnWidths: [
            { columnName: 'cod_solicitud', width: 120 },
            { columnName: 'cod_solicitud_completa', width: 125 },
            { columnName: 'des_tipo_documento', width: 125 },
            { columnName: 'des_nro_documento', width: 125 }, 
            { columnName: 'des_nombre_completo', width: 150 },
            { columnName: 'num_tarjeta_pmp', width: 150 },
            { columnName: 'num_cuenta_pmp', width: 150 },
            { columnName: 'des_jerarquias_flujo_fase_estado', width: 300 },
            { columnName: 'fec_reg', width: 150 },
            { columnName: 'des_usu_reg', width: 150 },
            { columnName: 'des_agencia', width: 120 },    
            { columnName: 'cancel', width: 120 },
            { columnName: 'observation', width: 150 },
        ],
        columns:[],
        tableColumnExtensions: [
            { columnName: 'cancel', align: 'center' },
            { columnName: 'observation', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        additional: 0, 
        reprint: 0, 
        express: 0,
        pageSizes: [5, 10, 15, 20, 0],
        columnExcel: [],
        showHideSearchInput: false, 
        openSearchModal: false 
    };
    componentDidUpdate = (prevProps) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }

        if(prevProps.odc !== this.props.odc){
            if (prevProps.odc.pendingProcessesCancellation !== this.props.odc.pendingProcessesCancellation) {	
                if(!this.props.odc.pendingProcessesCancellation.loading && 
                    this.props.odc.pendingProcessesCancellation.response && 
                    this.props.odc.pendingProcessesCancellation.success){
                    this.setState(state => ({
                        ...state, 
                        rows: this.props.odc.pendingProcessesCancellation.data
                    }));
                }
                else if(!this.props.odc.pendingProcessesCancellation.loading && 
                        this.props.odc.pendingProcessesCancellation.response && 
                        !this.props.odc.pendingProcessesCancellation.success){
                    this.setState(state => ({
                        ...state, 
                        rows: []
                    }));
                    // Notistack
                    this.getNotistack(this.props.odc.pendingProcessesCancellation.error, "error");
                }
                else if(!this.props.odc.pendingProcessesCancellation.loading && 
                        !this.props.odc.pendingProcessesCancellation.response && 
                        !this.props.odc.pendingProcessesCancellation.success){
                    // Notistack
                    this.getNotistack(this.props.odc.pendingProcessesCancellation.error, "error");
                    this.setState(state => ({
                        ...state, 
                        rows: []
                    }));
                }
            }
        
            if(prevProps.odc.cancelProcess !== this.props.odc.cancelProcess) {	
                if(!this.props.odc.cancelProcess.loading && 
                    this.props.odc.cancelProcess.response && 
                    this.props.odc.cancelProcess.success){
                    if(this.props.odc.cancelProcess.data){
                        const { data } = this.props.odc.cancelProcess;
                        if (data) {
                            const { cancelacion } = data;
                            if (cancelacion) {
                                this.getNotistack("Rechazar Originación: PMP Cancelar TC Ok!", "success");
                                this.getNotistack(`Originación: Solicitud ${cancelacion.cod_solicitud_completa} - Cancelado.`, "success");
                            }
                            this.setState(state => ({
                                ...state, 
                                rows: []
                            }));
                        }
                    }
                }
                else if(!this.props.odc.cancelProcess.loading && 
                        this.props.odc.cancelProcess.response && 
                        !this.props.odc.cancelProcess.success){
                    this.getNotistack(this.props.odc.cancelProcess.error, "error");
                }
                else if(!this.props.odc.cancelProcess.loading && 
                        !this.props.odc.cancelProcess.response && 
                        !this.props.odc.cancelProcess.success){
                    this.getNotistack(this.props.odc.cancelProcess.error, "error");
                }
            }
        }
        
        Utils.resolveStateRedux(prevProps.odcExistingClient.originationCanceled, this.props.odcExistingClient.originationCanceled, 
            null,
            _ => {
                const { actividadRechazar } = this.props.odcExistingClient.originationCanceled.data
                this.getNotistack('Rechazar Originación Efectivo: Cancelar EC Ok!', 'success')
                this.getNotistack(`Originación Express: Solicitud ${ actividadRechazar.cod_solicitud_completa } - Cancelado.`, "success");
                this.setState(state => ({
                    ...state, 
                    rows: []
                }))
            },
            warningMessage => this.getNotistack(warningMessage, 'warning'),
            errorMessage => this.getNotistack(errorMessage, 'error'))
        
        Utils.resolveStateRedux(prevProps.pci.cardnumberDecrypted, this.props.pci.cardnumberDecrypted, null,
            _ => this.getNotistack('Rechazar Originación: PCI Desencriptar OK!', 'success'),
            warningMessage => this.getNotistack(`Rechazar Originación: PCI Desencriptar - ${ warningMessage }`, 'warning'),
            errorMessage => this.getNotistack(`Rechazar Originación: PCI Desencriptar - ${ errorMessage }`, 'error')
        )
        
        Utils.resolveStateRedux(prevProps.odcMaster.blockingCodeCreditCardUpdated,
            this.props.odcMaster.blockingCodeCreditCardUpdated, null,
            _ => this.getNotistack('Rechazar Originación: Cambio Bloqueo Maestro OK!', 'success'),
            warningMessage => this.getNotistack(`Rechazar Originación: Cambio Bloqueo Maestro - ${ warningMessage }`, 'warning'),
            errorMessage => this.getNotistack(`Rechazar Originación: Cambio Bloqueo Maestro - ${ errorMessage }`, 'error')
        )
    }

    componentWillMount(){
        let columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns:columns
        }));
    }

    getColumnsRender(){
        let columns = [];
        if(this.props.width === "xs"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'cancel', title: 'Cancelar' }
            ]
        }
        else if(this.props.width === "sm"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'cancel', title: 'Cancelar' }
            ]
        }
        else if(this.props.width === "md"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento ' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'cancel', title: 'Cancelar' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        else if(this.props.width === "lg"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo de Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
                { name: 'num_tarjeta_pmp', title: 'Nro. de Tarjeta' },
                { name: 'num_cuenta_pmp', title: 'Nro. de Cuenta' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_usu_reg', title: 'Usuario Registra' },
                { name: 'cancel', title: 'Cancelar' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        else if(this.props.width === "xl"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo de Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
                { name: 'num_tarjeta_pmp', title: 'Nro. de Tarjeta' },
                { name: 'num_cuenta_pmp', title: 'Nro. de Cuenta' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_usu_reg', title: 'Usuario Registra' },
                { name: 'cancel', title: 'Cancelar' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        return columns;
    }

    handleSubmitFilter = data => {
        if(data){
            let sendData = {
                // cod_solicitud_completa: "",
                des_nro_documento: data.documentNumber,
            }
            this.props.pendingProcessesCancellation(sendData);
        }
    }
   
    handleShowHideSearchInput = e => {
        this.setState(state => ({
            showHideSearchInput: !state.showHideSearchInput
        }));
    }
    handleClickOpenSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: true
        }));
    }
    handleClickCloseSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: false
        }));
    }

    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render() {
        const { width, odc } = this.props;
        const { 
            columns,
            defaultColumnWidths, 
            defaultHiddenColumnNames, 
            rows, 
            tableColumnExtensions,
        } = this.state;
        return (
                <Grid container className="p-1">
                    <Grid item xs={12} className="">
                        {
                            odc.pendingProcessesCancellation.loading ?  <LinearProgress /> : ""
                        }
                    </Grid>
                    <Grid item xs={12} className="mb-2">
                        <Header 
                            title="Bandeja de Cancelación de Originación(es)"                             
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Divider className="mb-2"/>  
                    </Grid>
                    <Grid item xs={12}  className="mb-2">
                        <FormPanel title={"Filtros de búsqueda"} 
                            handleSubmitFilter={this.handleSubmitFilter}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <DevGridComponent 
                            rows={rows}
                            columns={columns}
                            width={width}
                            search={true}
                            columnExtensions={tableColumnExtensions}
                            defaultColumnWidths={defaultColumnWidths}
                            defaultHiddenColumnNames={defaultHiddenColumnNames}
                            RowDetailComponent={RowDetail}
                            CellComponent={Cell}/>
                    </Grid>
                </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(AuthorizePage))));

