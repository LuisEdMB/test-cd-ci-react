import React, { Component } from "react";
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components 
import { 
    AppBar,
    Fab, 
    Tooltip, 
    Button,
    withStyles, 
    Dialog,
    DialogTitle,
    Typography,
    DialogContent,
    Grid,
    DialogActions,
    CircularProgress,
    IconButton,
    TextField,
    FormHelperText
 } from "@material-ui/core";
// Colors
import orange from '@material-ui/core/colors/orange';
import green from '@material-ui/core/colors/green';
// Icons 
import SendIcon from '@material-ui/icons/Send';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close'; 
// Actions 
import { authorizeActivationProcess, rejectProcess, cancelProcess } from '../../../../../../actions/odc-express/odc';
import { blockTypeCreditCard } from '../../../../../../actions/pmp/pmp';
import * as ActionExistingClient from '../../../../../../actions/odc-existing-client/odc-existing-client'
import * as ActionPci from '../../../../../../actions/pci/pci'
import * as ActionOdcMaster from '../../../../../../actions/odc-master/odc-master'
// Utils
import { onlyTextKeyCode, checkInputKeyCode, resolveStateRedux } from '../../../../../../utils/Utils';
import * as OriginationCancellation from '../../../../../../utils/origination/Cancellation'
import * as Constants from '../../../../../../utils/Constants'
import * as MasterOrigination from '../../../../../../utils/origination/MasterOrigination'

const styles = theme => ({
    modalRoot:{
        minWidth:280, 
        width:350,
        maxWidth:400,
    },
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: orange[700],
        '&:hover': {
            backgroundColor: orange[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    buttonProgressWrapper:{
        position: 'relative'
    },
    button:{
        textTransform: 'none',
    },
    buttonCustom:{
        color: theme.palette.getContrastText(green[500]),
        '&:hover': {
            backgroundColor: green[700],
        },
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    appBar: {
        position: 'relative',
    }
});

const mapStateToProps = (state) => {
    return {
        odc: state.odcReducer,
        pmp: state.pmpReducer,
        odcExistingClient: state.odcExistingClientReducer,
        pci: state.pciReducer,
        odcMaster: state.odcMasterReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const actions = {
        authorizeActivationProcess: bindActionCreators(authorizeActivationProcess, dispatch),
        rejectProcess: bindActionCreators(rejectProcess, dispatch), 
        cancelProcess: bindActionCreators(cancelProcess, dispatch), 
        blockTypeCreditCard: bindActionCreators(blockTypeCreditCard, dispatch),
        cancelOrigination: bindActionCreators(ActionExistingClient.cancelOrigination, dispatch),
        decryptCardnumber: bindActionCreators(ActionPci.decryptCardnumber, dispatch),
        updateBlockingCodeCreditCardIntoMaster: bindActionCreators(ActionOdcMaster.updateBlockingCodeCreditCard, dispatch)
    };
  return actions;
}

class AuthorizeActionButton extends Component {
    state = {
        openAuthorizeModal: false,

        authorizeButtonLoading: false, 
        cancelButtonLoading: false, 
        rejectButtonLoading: false,

        authorizeButtonDisabled: false, 
        cancelButtonDisabled: false,
        rejectButtonDisaled: false,

        acceptRejectButtonDisabled: false,
        acceptRejectButtonLoading: false,

        cancelRejectButtonDisabled: false,
        cancelRejectButtonLoading: false,

        observation: "",
        openObservationModal: false,        
        observationButtonLoading: false, 
        observationButtonDisabled: false, 
        reintentProcessPMP: 0,
        creditCardNumber: ''
    }

    componentWillMount = () => {
        const { row } = this.props;
        if(row){
            let authorizeButtonDisabled = row.cod_flujo_fase_estado === 11101? true: false;
            this.setState(state => ({
                ...state,
                authorizeButtonDisabled: authorizeButtonDisabled,             
            }));
        }
    }
    componentDidUpdate = (prevProps) => {
        if(prevProps.odc !== this.props.odc){
            if(prevProps.odc.cancelProcess !== this.props.odc.cancelProcess){
                if(this.props.odc.cancelProcess.loading){
                    this.setState(state => ({
                        ...state,
                        acceptRejectButtonDisabled: true,
                        cancelRejectButtonDisabled: true,
                        acceptRejectButtonLoading: true,
                    }));
                }
                if(!this.props.odc.cancelProcess.loading && 
                    this.props.odc.cancelProcess.response && 
                    this.props.odc.cancelProcess.success){
                    this.setState(state => ({
                        ...state,
                        acceptRejectButtonDisabled: false,
                        cancelRejectButtonDisabled: false,
                        acceptRejectButtonLoading: false,
                        authorizeButtonLoading: false, 
                        authorizeButtonDisabled: false, 
                        openAuthorizeModal: false, 
                        openObservationModal: false,
                        observation: "",
                    }));
                }
                else if(!this.props.odc.cancelProcess.loading && 
                        this.props.odc.cancelProcess.response && 
                        !this.props.odc.cancelProcess.success){
                    this.setState(state => ({
                        ...state,
                        acceptRejectButtonDisabled: false,
                        cancelRejectButtonDisabled: false,
                        acceptRejectButtonLoading: false
                    }));
                }
                else if(!this.props.odc.cancelProcess.loading && 
                        !this.props.odc.cancelProcess.response && 
                        !this.props.odc.cancelProcess.success){
                    this.setState(state => ({
                        ...state,
                        acceptRejectButtonDisabled: false,
                        cancelRejectButtonDisabled: false,
                        acceptRejectButtonLoading: false
                    }));
                }
            }
    
            if (prevProps.odc.authorizeActivationProcess !== this.props.odc.authorizeActivationProcess) {	
                if(this.props.odc.authorizeActivationProcess.loading){
                    this.setState(state => ({
                        ...state,
                        authorizeButtonLoading: true, 
                        authorizeButtonDisabled: true
                    }));
                }
                if(!this.props.odc.authorizeActivationProcess.loading && 
                    this.props.odc.authorizeActivationProcess.response && 
                    this.props.odc.authorizeActivationProcess.success){
                    this.setState(state => ({
                        ...state,
                        authorizeButtonLoading: false, 
                        authorizeButtonDisabled: false, 
                        openAuthorizeModal: false, 
                    }));
                }
                else if(!this.props.odc.authorizeActivationProcess.loading && 
                        this.props.odc.authorizeActivationProcess.response && 
                        !this.props.odc.authorizeActivationProcess.success){
                    this.setState(state => ({
                        ...state,
                        authorizeButtonLoading: false, 
                        authorizeButtonDisabled: false
                    }));
                }
                else if(!this.props.odc.authorizeActivationProcess.loading && 
                        !this.props.odc.authorizeActivationProcess.response && 
                        !this.props.odc.authorizeActivationProcess.success){
                    this.setState(state => ({
                        ...state,
                        authorizeButtonLoading: false, 
                        authorizeButtonDisabled: false
                    }));
                }
            }
        }
        // block Type Credit Card
        if(prevProps.pmp.blockTypeCreditCard !==  this.props.pmp.blockTypeCreditCard ){
            if(this.props.pmp.blockTypeCreditCard.loading){
                this.setState(state => ({
                    ...state,
                    authorizeButtonLoading: true, 
                    authorizeButtonDisabled: true, 
                    acceptRejectButtonDisabled: true, 
                    acceptRejectButtonLoading: true,
                    cancelRejectButtonDisabled: true,
                }));
            }
            else if(!this.props.pmp.blockTypeCreditCard.loading && this.props.pmp.blockTypeCreditCard.response && !this.props.pmp.blockTypeCreditCard.success){
                // Notistack
                this.getNotistack(
                    this.props.pmp.blockTypeCreditCard.error,
                    "error"
                  );
                this.setState(state => ({
                    ...state,
                    authorizeButtonLoading: false, 
                    authorizeButtonDisabled: false,
                    acceptRejectButtonDisabled: false, 
                    cancelRejectButtonDisabled: false,
                    acceptRejectButtonLoading: false,
                }));
            }
            else if(!this.props.pmp.blockTypeCreditCard.loading && !this.props.pmp.blockTypeCreditCard.response && !this.props.pmp.blockTypeCreditCard.success){
                // Notistack
                this.getNotistack(
                    this.props.pmp.blockTypeCreditCard.error,
                    "error"
                  );
                this.setState(state => ({
                    ...state,
                    authorizeButtonLoading: false, 
                    authorizeButtonDisabled: false,
                    acceptRejectButtonDisabled: false, 
                    cancelRejectButtonDisabled: false,
                    acceptRejectButtonLoading: false,
                }));
            }
        }
        resolveStateRedux(prevProps.odcExistingClient.originationCanceled, this.props.odcExistingClient.originationCanceled, 
            _ => this.setState(state => ({
                ...state,
                authorizeButtonLoading: true, 
                authorizeButtonDisabled: true, 
                acceptRejectButtonDisabled: true, 
                acceptRejectButtonLoading: true,
                cancelRejectButtonDisabled: true
            })),
            _ => this.setState(state => ({
                ...state,
                authorizeButtonLoading: false, 
                authorizeButtonDisabled: false, 
                openAuthorizeModal: false, 
                acceptRejectButtonDisabled: false, 
                cancelRejectButtonDisabled: false,
                acceptRejectButtonLoading: false,
                observation: ''
            })),
            _ => this.setState(state => ({
                ...state,
                authorizeButtonLoading: false, 
                authorizeButtonDisabled: false,
                acceptRejectButtonDisabled: false, 
                cancelRejectButtonDisabled: false,
                acceptRejectButtonLoading: false
            })),
            _ => this.setState(state => ({
                ...state,
                authorizeButtonLoading: false, 
                authorizeButtonDisabled: false,
                acceptRejectButtonDisabled: false, 
                cancelRejectButtonDisabled: false,
                acceptRejectButtonLoading: false
            })))
        resolveStateRedux(prevProps.pci.cardnumberDecrypted, this.props.pci.cardnumberDecrypted,
            _ => this.setState(state => ({
                ...state,
                authorizeButtonLoading: true, 
                authorizeButtonDisabled: true, 
                acceptRejectButtonDisabled: true, 
                acceptRejectButtonLoading: true,
                cancelRejectButtonDisabled: true
            })), null,
            _ => this.setState(state => ({
                ...state,
                authorizeButtonLoading: false, 
                authorizeButtonDisabled: false,
                acceptRejectButtonDisabled: false, 
                cancelRejectButtonDisabled: false,
                acceptRejectButtonLoading: false
            })),
            _ => this.setState(state => ({
                ...state,
                authorizeButtonLoading: false, 
                authorizeButtonDisabled: false,
                acceptRejectButtonDisabled: false, 
                cancelRejectButtonDisabled: false,
                acceptRejectButtonLoading: false
            }))
        )
        resolveStateRedux(prevProps.odcMaster.blockingCodeCreditCardUpdated,
            this.props.odcMaster.blockingCodeCreditCardUpdated,
            _ => this.setState(state => ({
                ...state,
                authorizeButtonLoading: true, 
                authorizeButtonDisabled: true, 
                acceptRejectButtonDisabled: true, 
                acceptRejectButtonLoading: true,
                cancelRejectButtonDisabled: true
            })), null,
            _ => this.setState(state => ({
                ...state,
                authorizeButtonLoading: false, 
                authorizeButtonDisabled: false,
                acceptRejectButtonDisabled: false, 
                cancelRejectButtonDisabled: false,
                acceptRejectButtonLoading: false
            })),
            _ => this.setState(state => ({
                ...state,
                authorizeButtonLoading: false, 
                authorizeButtonDisabled: false,
                acceptRejectButtonDisabled: false, 
                cancelRejectButtonDisabled: false,
                acceptRejectButtonLoading: false
            }))
        )
    }
    handleSubmitAuthorizeCreditCardActivation = row => e => {
        e.preventDefault();
        let sendData = {
            nom_actividad: "Activación Tarjeta: Autorizado",
            cod_flujo_fase_estado_actual: 10804,
            cod_flujo_fase_estado_anterior: 10803,
            cod_solicitud: row.cod_solicitud,
            cod_solicitud_completa: row.cod_solicitud_completa,
        }
        this.props.authorizeActivationProcess(sendData);
    }

    handleSubmitReject = (row, e) => {
        e.preventDefault();
        const steps = this.getSteps()
        const flowTypeForBlockingCreditCard = OriginationCancellation.getFlowTypeForCancellation({
            cod_tipo_solicitud_originacion: row.cod_tipo_solicitud_originacion,
            flg_cec: row.flg_cec,
            cod_tipo_relacion: row.cod_tipo_relacion,
            cod_motivo_reimpresion: row.cod_motivo_reimpresion })
        const preDataForBlockingCreditCard = OriginationCancellation.getDataForCancelCreditCardByFlowOrigination(
            flowTypeForBlockingCreditCard, { cod_motivo_reimpresion: row.cod_motivo_reimpresion,
            cod_tipo_relacion: row.cod_tipo_relacion })
        steps[this.state.reintentProcessPMP].action(row, e, preDataForBlockingCreditCard)
    }

    getSteps = _ => ({
        0: {
            action: async (row, e) => {
                const sendData = {
                    solicitudeCode: row.cod_solicitud,
                    completeSolicitudeCode: row.cod_solicitud_completa,
                    token: row.token
                }
                let result = { next: true }
                if (row.token) result = await this.props.decryptCardnumber(sendData).then(response => response)
                if (result?.success || result?.next) {
                    this.setState(state => ({
                        ...state,
                        reintentProcessPMP: 1,
                        creditCardNumber: result?.nro_tarjeta || row.num_tarjeta_pmp
                    }), _ => this.handleSubmitReject(row, e))
                }
            }
        },
        1: {
            action: async (row, e, preDataForBlockingCreditCard) => {
                const sendData = {
                    ...preDataForBlockingCreditCard,
                    organization: Constants.General.organization,
                    process: {
                        fullNumber: row.cod_solicitud_completa,
                        number: row.cod_solicitud
                    }, 
                    client: {
                        id: row.cod_cliente || 0,
                        documentTypeAux: row.cod_tipo_documento || '',
                        documentNumber: row.des_nro_documento || '',
                        documentTypeInternalValue: row.des_tipo_documento_valor_interno || ''
                    }, 
                    pmp: {
                        cardNumber: this.state.creditCardNumber
                    }, 
                    isSAE: false
                }
                const sendDataPMP = this.setDataBlockTypeCreditCardPMP(sendData);
                const result = await this.props.blockTypeCreditCard(sendDataPMP).then(response => response)
                if (result?.data?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentProcessPMP: 2
                    }), _ => this.handleSubmitReject(row, e))
                }
            }
        },
        2: {
            action: async (row, e, preDataForBlockingCreditCard) => {
                const sendData = {
                    solicitudeCode: row.cod_solicitud,
                    completeSolicitudeCode: row.cod_solicitud_completa,
                    accountNumber: row.num_cuenta_pmp,
                    token: row.token,
                    blockingCode: preDataForBlockingCreditCard.blockCode,
                    activityName: MasterOrigination.updateBlockingCodeCreditCardActivity.activityName,
                    phaseCode: MasterOrigination.updateBlockingCodeCreditCardActivity.phaseCode,
                    masterStageCode: Constants.MasterConfigurationStage.masterTrayStage
                }
                const result = await this.props.updateBlockingCodeCreditCardIntoMaster(sendData).then(response => response)
                if (result?.success) {
                    this.setState(state => ({
                        ...state,
                        reintentProcessPMP: 3
                    }), _ => this.handleSubmitReject(row, e))
                }
            }
        },
        3: {
            action: async (row) => {
                const sendData = {
                    cod_solicitud: row.cod_solicitud,
                    des_observacion: this.state.observation,
                    cod_solicitud_completa: row.cod_solicitud_completa
                }
                await this.props.cancelProcess(sendData)
            }
        }
    })

    handleSubmitRejectExistingClient = (row, e) => {
        e.preventDefault()
        const activity = {
            solicitudeCode: row.cod_solicitud,
            completeSolicitudeCode: row.cod_solicitud_completa,
            currentlyPhaseCode: 101102,
            previousPhaseCode: 101102,
            activityName: 'Rechazar Originación Efectivo: Cancelar EC',
            observation: this.state.observation
        }
        this.props.cancelOrigination(activity)
    }

    handleClickAuthorizeCreditCardActivationToggleModal = e => {
        this.setState(state => ({
            ...state,
            openAuthorizeModal: !state.openAuthorizeModal
        }));
    }

    handleClickObservationToggleModal = e => {
        this.setState(state => ({
            openObservationModal: !state.openObservationModal
        }));
    }

    // Client - TextField - Event Change - Required    
    handleChangeTextFieldRequired = name => e => {
        e.persist();
        let { value } = e.target;
        this.setState(state => ({
            ...state,
            [name]: value, 
        }));
    }
    // Only Text
    handleKeyPressTextFieldOnlyText = name => e =>{
        let code = (e.which) ? e.which : e.keyCode;
        if(!onlyTextKeyCode(code)){
            e.preventDefault();
        }       
    }
    // Check Input
    handleKeyPressTextFieldCheckInput = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!checkInputKeyCode(code)){
            e.preventDefault();
        }   
    }


    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    // Set Data Block Type Credit Card PMP
    setDataBlockTypeCreditCardPMP = data => {
        let { client, pmp, process, isSAE=false } = data;
        return {
            tipo_doc_letra: client.documentType,
            nom_actividad: data.nom_actividad,
            cod_flujo_fase_estado_actual: data.cod_flujo_fase_estado_actual,
            cod_flujo_fase_estado_anterior: data.cod_flujo_fase_estado_anterior,
            cod_solicitud: process.number,
            cod_solicitud_completa: process.fullNumber,
            cod_cliente: null,
            cod_cliente_titular: client? client.id: 0,
            terceraLlamadaPmpTipoBloqueTarjeta: {
                tipo_doc: client.documentTypeAux,
                nro_doc: client.documentNumber,
                organization: data.organization,
                identityDocumentType: client.documentTypeInternalValue,
                identityDocumentNumber: client.documentNumber,
                isoMessageTypeActivar: "0",
                transactionNumberActivar: "0",
                cardNumberActivar: isSAE? pmp.parallelCardNumber: pmp.cardNumber,
                cardSequenceActivar:"0001",
                amedBlockCodeActivar: data.blockCode, // Code Block
                amedCardActionActivar:"0",
                amedRqtdCardTypeActivar:"0",
                amedCurrFirstUsageFlagActivar:"",
                amedPriorFirstUsageFlagActivar:"",
                amedMotiveBlockadeActivar:"01",
                internacionUsaRegionActivar:"",
                boletineoInterUsaDateActivar:"0",
                interCanRegionActivar:"",
                boletineoInterCanDateActivar:"0",
                internacionalCAmerRegionActivar:"",
                boletineoInterCAmerRegionActivar:"0",
                interAsiaRegionActivar:"",
                bolitineoInterAsiaDateActivar:"0",
                interRegionEuropaActivar:"",
                bolitineoInterDateEuropaActivar:"0",
                interEuropaRegionActivar:"",
                bolitineoInterEuropaDateActivar:"0",
                principalAccountActivar:"",
                documentTypeActivar:"",
                documentNumerActivar:"",
                clientNameActivar:"",
                clientAddressActivar:"",
                birthDateActivar:"",
                telephoneActivar:"0",
                cardNameActivar:"",
                cardAddressActivar:"",
                cardTypeActivar:"",
                applicantNameActivar:"",
                applicantDocumentActivar:"",
                applicantTelephoneActivar:"0",
                relationshipActivar:"",
                blockadeUserActivar:"",
                blockadeNumberActivar:"0",
                blockadeDateActivar:"0",
                blockadeTimeActivar:"0",
                userInitialsActivar:"",
                emisionTypeActivar:""
            }
        }
    }
    render = () => {
        const { classes, row } = this.props;
        let { 
            openAuthorizeModal, authorizeButtonLoading, 
            authorizeButtonDisabled, cancelButtonDisabled, observation
        } = this.state;
        return  (
            <React.Fragment>
                <Tooltip title={`Autorizar Solicitud: ${row.cod_solicitud_completa}`} placement="left">
                    <div>
                        <Fab className={classes.fab} onClick={this.handleClickAuthorizeCreditCardActivationToggleModal}>
                            <CancelIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>  
                            
                <Dialog
                    onClose={this.handleClickAuthorizeCreditCardActivationToggleModal}
                    open={openAuthorizeModal}
                    maxWidth='xs'
                    aria-labelledby="form-authorize">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-authorize" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                            Autorizar Cancelación TC.
                            </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form  
                        onSubmit={ e => row.flg_cliente_existente
                            ? this.handleSubmitRejectExistingClient(row, e)
                            : this.handleSubmitReject(row, e) }>
                        <DialogContent>
                            <Typography align="center">
                                ¿Deseas autorizar la cancelación de la Tarjeta de Crédito para la solicitud: <strong> {row.cod_solicitud_completa}</strong>?
                            </Typography>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    autoFocus
                                    fullWidth
                                    multiline
                                    rows="4"
                                    label="Observaciones"
                                    type="text"
                                    margin="normal"
                                    color="default"
                                    name="observation"
                                    variant="outlined"
                                    value={observation}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    InputProps={{
                                        inputProps:{
                                            maxLength: 300,
                                        },
                                    }}
                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("observation")}
                                    onChange={this.handleChangeTextFieldRequired("observation")}
                                />
                                <FormHelperText className="d-flex justify-content-end">
                                    {observation.toString().length}/300
                                </FormHelperText>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container >
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            disabled={cancelButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickAuthorizeCreditCardActivationToggleModal}
                                            margin="normal">
                                            Rechazar
                                            <CancelIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={authorizeButtonDisabled}
                                            className={classNames(classes.button)}
                                            type="submit" 
                                            margin="normal"
                                            color="primary" 
                                            size="small"
                                            fullWidth>
                                            Aceptar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                               authorizeButtonLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>
            </React.Fragment>
        )        
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(AuthorizeActionButton)));
