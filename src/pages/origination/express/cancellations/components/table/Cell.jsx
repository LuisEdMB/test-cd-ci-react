import React from "react";
import * as moment from "moment";
import { Table } from '@devexpress/dx-react-grid-material-ui';
import AuthorizeActionButton from '../action-button/AuthorizeActionButton';
import NewCommentForm from '../../../../../../components/NewCommentForm/NewCommentForm';
import * as Utils from '../../../../../../utils/Utils'

const Cell = ({...props}) => {
    if(props.column.name === "fec_reg"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    else if(props.column.name === "cancel"){
        return <Table.Cell {...props}>
            <AuthorizeActionButton {...props} />
        </Table.Cell>
    }
    else if(props.column.name === "num_tarjeta_pmp"){
        const value = Utils.maskCreditCard(props.value || '');
        return <Table.Cell {...props} value={value} />
    }
    if(props.column.name === "observation"){
        return <Table.Cell {...props}>
           <NewCommentForm {...props}/>
        </Table.Cell>
    }


    return <Table.Cell {...props} />
}

export default Cell;
