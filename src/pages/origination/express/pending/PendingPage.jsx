import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth';
// React Router 
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
// Component
import {
    IconButton,
    Grid, 
    LinearProgress,
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close'; 
// Components Custom 
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
import Header from './components/Header';
import FormPanel from './components/FormPanel';
import { pendingProcesses } from '../../../../actions/odc-express/odc';
import NotificationSolicitudeBlockedModal from '../../../../components/modals/NotificationSolicitudeBlockedModal'
// Utils
import * as Utils from '../../../../utils/Utils'

const styles = _ => ({
    avatar:{
        width:80, 
        height:48
    },
    modalRoot:{
       
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state) => {
    return {
        odc: state.odcReducer,
        odcActivity: state.odcActivityReducer
    }
}
const mapDispatchToProps = (dispatch) => {
    const actions = {
        pendingProcesses: bindActionCreators(pendingProcesses, dispatch)
    };
    return actions;
}

class PendingPage extends Component {

    state = {
        defaultColumnWidths: [
            { columnName: 'cod_solicitud', width: 120 },
            { columnName: 'cod_solicitud_completa', width: 125 },
            { columnName: 'des_nro_documento', width: 125 }, 
            { columnName: 'des_nombre_completo', width: 150 },
            { columnName: 'des_jerarquias_flujo_fase_estado', width: 300 },
            { columnName: 'fec_reg_solicitud', width: 150 },
            { columnName: 'des_usu_reg_solicitud', width: 150 },
            { columnName: 'agencia', width: 120 },    
            { columnName: 'detail', width: 90 },
            { columnName: 'process', width: 90 },
            { columnName: 'observe', width: 120 },
            { columnName: 'activate', width: 90 },
            { columnName: 'flg_confirmado', width: 150 },
            { columnName: 'observation', width: 150 },
        ],
        columns:[],
        tableColumnExtensions: [
            { columnName: 'detail', align: 'center' },
            { columnName: 'process', align: 'center' },
            { columnName: 'observe', align: 'center' },
            { columnName: 'activate', align: 'center' },
            { columnName: 'flg_confirmado', align: 'center' },
            { columnName: 'observation', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        additional: 0, 
        reprint: 0, 
        express: 0,
        pageSizes: [5, 10, 15, 20, 0],
        columnExcel: [],
        showNotificationSolicitudeBlockedModal: false,
        messageNotificationSolicitudeBlockedModal: ''
    }

    componentDidUpdate = (prevProps) => {
        Utils.resolveStateRedux(prevProps.odc.solicitudeBlockedVerified, this.props.odc.solicitudeBlockedVerified,
            null, _ => {
                const { consultaClienteEnProceso } = this.props.odc.solicitudeBlockedVerified.data
                if (consultaClienteEnProceso?.des_mensaje)
                    this.setState(state => ({
                        ...state,
                        showNotificationSolicitudeBlockedModal: true,
                        messageNotificationSolicitudeBlockedModal: consultaClienteEnProceso?.des_mensaje
                    }))
            },
            warningMessage => this.getNotistack(warningMessage, "warning"),
            errorMessage => this.getNotistack(errorMessage, "error")
        )

        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }

        if(prevProps.odc !== this.props.odc){
            if (prevProps.odc.pendingProcesses !== this.props.odc.pendingProcesses) {	
                if(this.props.odc.pendingProcesses.loading){
                    this.setState(state => ({
                        ...state, 
                        rows: [], 
                    }));
                }

                if(!this.props.odc.pendingProcesses.loading && 
                    this.props.odc.pendingProcesses.response && 
                    this.props.odc.pendingProcesses.success){
                    let { data } = this.props.odc.pendingProcesses;
                    if(data){
                        this.setState(state => ({
                            ...state, 
                            rows: data
                        }));
                    }
                }
                else if(!this.props.odc.pendingProcesses.loading && 
                        this.props.odc.pendingProcesses.response && 
                        !this.props.odc.pendingProcesses.success){
                    // Notistack
                    this.getNotistack(this.props.odc.pendingProcesses.error, "error");
                }
                else if(!this.props.odc.pendingProcesses.loading && 
                        !this.props.odc.pendingProcesses.response && 
                        !this.props.odc.pendingProcesses.success){
                    // Notistack
                    this.getNotistack(this.props.odc.pendingProcesses.error, "error");
                }
            }
        }
        if (prevProps.odcActivity !== this.props.odcActivity){
            if (prevProps.odcActivity.simpleGenericActivity !== this.props.odcActivity.simpleGenericActivity) {	
                if(!this.props.odcActivity.simpleGenericActivity.loading && 
                    this.props.odcActivity.simpleGenericActivity.response && 
                    this.props.odcActivity.simpleGenericActivity.success){
                    const { data } = this.props.odcActivity.simpleGenericActivity;
                    if(data){
                        const { actividadGenerico } = data;
                        if(actividadGenerico){
                            this.getNotistack(`Rechazar Originación Express: Solicitud ${actividadGenerico.cod_solicitud_completa} - Aceptar.`, "success");
                        }
                    }
                }
                else if(!this.props.odcActivity.simpleGenericActivity.loading && 
                        this.props.odcActivity.simpleGenericActivity.response && 
                        !this.props.odcActivity.simpleGenericActivity.success){
                    // Notistack
                    this.getNotistack(this.props.odcActivity.simpleGenericActivity.error, "error");

                }
                else if(!this.props.odcActivity.simpleGenericActivity.loading && 
                        !this.props.odcActivity.simpleGenericActivity.response && 
                        !this.props.odcActivity.simpleGenericActivity.success){
                    // Notistack
                    this.getNotistack(this.props.odcActivity.simpleGenericActivity.error, "error");
                }
            }
        }
    }

    componentWillMount(){
        let columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns:columns
        }));
    }

    handleSubmitFilter = (data) => {
        if(data){
            let sendData = {
                cod_tipo_documento: data.documentTypeAux,
                des_nro_documento: data.documentNumber,
            }
            this.props.pendingProcesses(sendData);
        }
    }
    
    getColumnsRender(){
        let columns = [];
        if(this.props.width === "xs"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'process', title: 'Retomar' },
                { name: 'detail', title: 'Detalle' },
                { name: 'observe', title: 'Conting. Obs.' },
                { name: 'activate', title: 'Activar' },
                { name: 'observation', title: 'Ver Comentario' },
            ];
        }
        else if(this.props.width === "sm"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'detail', title: 'Detalle' },
                { name: 'process', title: 'Retomar' },
                { name: 'observe', title: 'Conting. Obs.' },
                { name: 'activate', title: 'Activar' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        else if(this.props.width === "md"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento ' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
                { name: 'fec_reg_solicitud', title: 'Fecha Registro' },
                { name: 'detail', title: 'Detalle' },
                { name: 'process', title: 'Retomar' },
                { name: 'observe', title: 'Conting. Obs.' },
                { name: 'activate', title: 'Activar' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        else if(this.props.width === "lg"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
                { name: 'fec_reg_solicitud', title: 'Fecha Registro' },
                { name: 'des_usu_reg_solicitud', title: 'Usuario Registra' },
                { name: 'detail', title: 'Detalle' },
                { name: 'process', title: 'Retomar' },
                { name: 'observe', title: 'Conting. Obs.' },
                { name: 'activate', title: 'Activar' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        else if(this.props.width === "xl"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
                { name: 'fec_reg_solicitud', title: 'Fecha Registro' },
                { name: 'des_usu_reg_solicitud', title: 'Usuario Registra' },
                { name: 'detail', title: 'Detalle' },
                { name: 'process', title: 'Retomar' },
                { name: 'observe', title: 'Conting. Obs.' },
                { name: 'activate', title: 'Activar' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        return columns;
    }
    
    handleShowHideSearchInput = e => {
        this.setState(state => ({
            showHideSearchInput: !state.showHideSearchInput
        }));
    }
    handleClickOpenSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: true
        }));
    }
    handleClickCloseSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: false
        }));
    }
    handleSubmitSearchItem = e => {
        e.preventDefault();
    }

    handleCloseNotificationSolicitudeBlockedModal = _ => {
        this.setState(state => ({
            ...state,
            showNotificationSolicitudeBlockedModal: false,
            messageNotificationSolicitudeBlockedModal: ''
        }))
    }

    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render() {
        const { width, odc } = this.props;
        const { 
            columns,
            defaultColumnWidths, 
            defaultHiddenColumnNames, 
            rows, 
            tableColumnExtensions,
            showNotificationSolicitudeBlockedModal,
            messageNotificationSolicitudeBlockedModal
        } = this.state;

        return (
            <>
                <Grid container className="p-1">
                    <Grid item xs={12} className="">
                        {
                            odc.pendingProcesses.loading ? <LinearProgress /> : ""
                        }
                    </Grid>
                    <Grid item xs={12} className="mb-2">
                        <Header 
                            title="Bandeja en Proceso de Originación(es)"
                        />
                    </Grid>
                    <Grid item xs={12}  className="mb-2">
                        <FormPanel title={"Filtros de búsqueda"} 
                            handleSubmitFilter={this.handleSubmitFilter}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <DevGridComponent 
                            rows={rows}
                            columns={columns}
                            width={width}
                            search={false}
                            columnExtensions={tableColumnExtensions}
                            defaultColumnWidths={defaultColumnWidths}
                            defaultHiddenColumnNames={defaultHiddenColumnNames}
                            RowDetailComponent={RowDetail}
                            CellComponent={Cell}/>
                    </Grid>
                </Grid>
                <NotificationSolicitudeBlockedModal
                    text={ messageNotificationSolicitudeBlockedModal }
                    open={ showNotificationSolicitudeBlockedModal }
                    wait={ false }
                    onClickOk={ this.handleCloseNotificationSolicitudeBlockedModal } />
            </>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(PendingPage))));
