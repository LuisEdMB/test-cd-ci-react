import React from 'react';
import ClassNames from 'classnames';
import { 
    Paper,
    Typography,
    withStyles
} from '@material-ui/core';
import CencosudScotiaBank from '../../../../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg';

const styles = theme =>({
    rootWrapper:{
       // borderBottom:"1px solid #007ab8"
    },
    avatar:{
        [theme.breakpoints.down('md')]: {
            width: 68, 
            height: 36
        },
        width:80, 
        height:48
    },
    root:{
        display: "flex", 
        justifyContent: "space-between", 
        alignItems: "center",
       
    },
    actionRoot:{
        display: "flex", 
        justifyContent: "center", 
        alignItems: "center"
    }, 
    green:{
        background: `radial-gradient(circle at center, red 0, blue, green 100%)`
    },
    title:{
        [theme.breakpoints.down('md')]: {
            fontSize:14, 
        }
    }
});

const Header = ({title="", express = 0, reprint = 0, additional = 0, max = 999, excel, classes}) => (
    <Paper className={ClassNames(classes.rootWrapper, "py-1")} color="primary">
        <figure className={classes.root}>
            <img
                className={classes.avatar}
                src={CencosudScotiaBank}
                alt="Cencosud ScotiaBank" 
                />
            <figcaption className="w-100">
                <Typography align="center" variant="h5" component="h4" className={classes.title}>
                    {title}
                </Typography>
            </figcaption>
            <div className={classes.actionRoot}>
                
            </div>
        </figure>
    </Paper>
)

export default withStyles(styles)(Header);