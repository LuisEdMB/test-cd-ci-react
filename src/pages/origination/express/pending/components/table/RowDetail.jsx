import React from "react";
import { withStyles, Typography } from "@material-ui/core";

const styles = theme => ({
    fontSizeDefault: {
        fontSize:12
    },
});

const RowDetail = ({ row, classes }) => (
    <ul>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Cliente:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.des_nombre_completo }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Usuario:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_usu_reg_solicitud && row.des_usu_reg_solicitud }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Agencia:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.des_agencia }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.fec_reg_solicitud && row.fec_reg_solicitud }
            </Typography>
        </li>

        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Flujo - Fase - Estado:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_jerarquias_flujo_fase_estado }
            </Typography>
        </li>
    </ul>
);

export default withStyles(styles)(RowDetail);

