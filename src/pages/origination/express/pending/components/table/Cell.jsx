import React from "react";
import * as moment from "moment";
import { Table } from "@devexpress/dx-react-grid-material-ui";
import DetailActionButton from "../action-button/DetailActionButton";
import ProcessActionButton from "../action-button/ProcessActionButton";
import ActivateActionButton from "../action-button/ActivateActionButton";
import ObserveActionButton from "../action-button/ObserveActionButton";
import NewCommentForm from "../../../../../../components/NewCommentForm/NewCommentForm";

const Cell = (props) => {
  let { row } = props;
  let disabled = true;
  let rowFPS = row.cod_flujo_fase_estado ? row.cod_flujo_fase_estado : 0;
  if (props.column.name === "fec_reg_solicitud") {
    const value = props.value
      ? moment(props.value.replace("T", " ")).format("DD/MM/YYYY HH:mm")
      : "";
    return <Table.Cell {...props} value={value} />;
  } else if (props.column.name === "detail") {
    if (rowFPS >= 10301) {
      disabled = false;
    }
    return (
      <Table.Cell {...props}>
        <DetailActionButton {...props} disabled={disabled} />
      </Table.Cell>
    );
  } else if (props.column.name === "process") {
    if (
      (rowFPS >= 10501 && rowFPS <= 10802) ||
      (rowFPS >= 20501 && rowFPS <= 20802) ||
      (rowFPS >= 90501 && rowFPS <= 90702) ||
      (rowFPS >= 100401 && rowFPS <= 100702)
    ) {
      disabled = false;
    }
    return (
      <Table.Cell {...props}>
        <ProcessActionButton {...props} disabled={disabled} />
      </Table.Cell>
    );
  } else if (props.column.name === "activate") {
    if (
      (rowFPS >= 10804 && rowFPS <= 10807) ||
      (rowFPS >= 20804 && rowFPS <= 20807) ||
      (rowFPS >= 90704 && rowFPS <= 90706) ||
      (rowFPS >= 100704 && rowFPS <= 100705)
    ) {
      disabled = false;
    }
    return (
      <Table.Cell {...props}>
        <ActivateActionButton {...props} disabled={disabled} />
      </Table.Cell>
    );
  } else if (props.column.name === "flg_confirmado") {
    return (
      <Table.Cell {...props}>
        <p style={{ color: props.value ? "green" : "red" }}>
          {props.value ? "Correo Confirmado" : "Correo No Confirmado"}
        </p>
      </Table.Cell>
    );
  } else if (props.column.name === "observe") {
    if (rowFPS === 11103 || rowFPS === 11104) {
      disabled = false;
    }
    return (
      <Table.Cell {...props}>
        <ObserveActionButton {...props} disabled={disabled} />
      </Table.Cell>
    );
  }
  if (props.column.name === "observation") {
    return (
      <Table.Cell {...props}>
        <NewCommentForm {...props} />
      </Table.Cell>
    );
  }
  return <Table.Cell {...props} />;
};

export default Cell;
