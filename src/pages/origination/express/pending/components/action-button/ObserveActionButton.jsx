import React, { Component } from "react";
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components 
import { 
    AppBar,
    Fab, 
    IconButton,
    Tooltip, 
    Button,
    withStyles, 
    Dialog,
    DialogTitle,
    Typography,
    DialogContent,
    Grid,
    DialogActions,
    CircularProgress, 
    TextField
 } from "@material-ui/core";
// Colors
import { yellow, green } from '@material-ui/core/colors';
// Icons 
import VisibilityIcon from '@material-ui/icons/Visibility'
import SendIcon from '@material-ui/icons/Send';
import CloseIcon from '@material-ui/icons/Close'; 
// Actions 
import { simpleGenericActivity } from '../../../../../../actions/odc-express/odc-activity';

const styles = _ => ({
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: yellow[700],
        '&:hover': {
            backgroundColor: yellow[800],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    buttonProgressWrapper:{
        position: 'relative'
    },
    button:{
        textTransform: 'none',
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    appBar: {
        position: 'relative',
    }
});

const mapStateToProps = (state) => {
    return {
        odcActivity: state.odcActivityReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const actions = {
        simpleGenericActivity: bindActionCreators(simpleGenericActivity, dispatch)
    };
    return actions;
}
class ObserveActionButton extends Component {
    state = {
        openObservationModal: false,
        observationButtonLoading: false, 
        observationButtonDisabled: false, 
        observation: "",
        buttonDisabled: false,
        origination:{
            fullNumber: "", 
            number: 0
        }
    }
    componentWillMount = () => {
        let { row } = this.props;
        if(row){
            this.setState(state => ({
                ...state, 
                buttonDisabled: this.props.disabled, 
                observation: row.des_observacion? row.des_observacion: "",
                origination: {
                    fullNumber: row.cod_solicitud_completa? row.cod_solicitud_completa: "", 
                    number: row.cod_solicitud? row.cod_solicitud: 0
                }
            }));
        }
    }
    componentDidUpdate = (prevProps) => {
        if (prevProps.odcActivity.simpleGenericActivity !== this.props.odcActivity.simpleGenericActivity) {	
            if(this.props.odcActivity.simpleGenericActivity.loading){
                this.setState(state => ({
                    ...state,
                    observationButtonDisabled: true, 
                    observationButtonLoading: true
                }));
            }
            if(!this.props.odcActivity.simpleGenericActivity.loading && 
                this.props.odcActivity.simpleGenericActivity.response && 
                this.props.odcActivity.simpleGenericActivity.success){
                this.setState(state => ({
                    ...state,
                    observationButtonDisabled: false, 
                    observationButtonLoading: false,
                    openObservationModal: false
                }));
            }
            else if(!this.props.odcActivity.simpleGenericActivity.loading && 
                     this.props.odcActivity.simpleGenericActivity.response && 
                    !this.props.odcActivity.simpleGenericActivity.success){
                // Notistack
                this.setState(state => ({
                    ...state,
                    observationButtonDisabled: false, 
                    observationButtonLoading: false
                }));
            }
            else if(!this.props.odcActivity.simpleGenericActivity.loading && 
                    !this.props.odcActivity.simpleGenericActivity.response && 
                    !this.props.odcActivity.simpleGenericActivity.success){
                // Notistack
                this.setState(state => ({
                    ...state,
                    observationButtonDisabled: false, 
                    observationButtonLoading: false
                }));
            }
        }
    }
    handleSubmitObserve = e => {
        e.preventDefault();
        let { origination } = this.state;
        let sendData = {
            cod_requerimiento: 0,
            cod_solicitud: origination.number, 
            cod_solicitud_completa: origination.fullNumber,
            cod_flujo_fase_estado_actual: 11104, 
            cod_flujo_fase_estado_anterior: 11103, 
            nom_actividad: "Rechazar Originación Express: Aceptar"
        }
        this.props.simpleGenericActivity(sendData, null, true);
    }
    
    handleClickObservationToggleModal = e => {
        this.setState(state => ({
            openObservationModal: !state.openObservationModal
        }));
    }

    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render = () => {
        const { classes, row } = this.props;
        let { 
                openObservationModal, 
                observationButtonLoading,
                observationButtonDisabled,
                buttonDisabled 
            } = this.state;
        return  (
            <React.Fragment>
                <Tooltip title={!buttonDisabled? `Solicitud Rechazada: ${row.cod_solicitud_completa? row.cod_solicitud_completa: ""}`: ""} placement="left">
                    <div>
                        <Fab className={classes.fab} onClick={this.handleClickObservationToggleModal} disabled={buttonDisabled}>
                            <VisibilityIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>  
                            
                <Dialog
                    onClose={this.handleClickObservationToggleModal}
                    open={openObservationModal}
                    maxWidth='xs'
                    aria-labelledby="form-observe-legajo">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-observe-legajo" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                Observaciones del Validación
                            </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form  onSubmit={this.handleSubmitObserve}>
                        <DialogContent>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    multiline
                                    rows="4"
                                    label="Observaciones"
                                    type="text"
                                    margin="normal"
                                    color="default"
                                    name="observation"
                                    variant="outlined"
                                    value={this.state.observation}
                                    InputLabelProps={{
                                        shrink: true
                                    }}
                                    InputProps={{
                                        readOnly: true
                                    }}
                                />
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            disabled={observationButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickObservationToggleModal}
                                            margin="normal">
                                            Cancelar
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={observationButtonDisabled}
                                            className={classNames(classes.button)}
                                            type="submit" 
                                            margin="normal"
                                            color="primary" 
                                            size="small"
                                            fullWidth>
                                            Aceptar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                               observationButtonLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  
            </React.Fragment>
        )        
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ObserveActionButton)));
