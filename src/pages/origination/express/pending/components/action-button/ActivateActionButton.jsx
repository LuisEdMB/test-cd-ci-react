import React from "react";
import { Link } from "react-router-dom";
// Components
import { Fab, Tooltip, withStyles } from "@material-ui/core";
// Colors
import green from "@material-ui/core/colors/green";
// Icons
import DoneIcon from "@material-ui/icons/Done";
import { URL_BASE } from "../../../../../../actions/config";

const styles = (theme) => ({
  fab: {
    minHeight: 0,
    height: 24,
    width: 24,
    color: "white",
    transition: ".3s",
    backgroundColor: green[700],
    "&:hover": {
      backgroundColor: green[900],
    },
    textTransform: "none",
  },
  icon: {
    minHeight: 0,
    height: 12,
    width: 12,
  },
});

const ActivateActionButton = ({
  row,
  disabled = false,
  onClick,
  classes,
  props,
}) => (
  <Tooltip
    title={
      !disabled
        ? `Hacer Click! Activar Tarjeta: ${row.cod_solicitud_completa}`
        : `Solicitud: ${row.cod_solicitud_completa}`
    }
    placement="left"
  >
    {row.cod_tipo_solicitud_originacion === "390001" ? (
      <Link
        onClick={(e) => disabled && e.preventDefault()}
        to={{
          pathname: row.flg_cliente_existente
            ? `/${URL_BASE}/express/clientes-existentes/${row.cod_solicitud_completa}`
            : `/${URL_BASE}/express/nueva-solicitud/${row.cod_solicitud_completa}`,
          state: { dataUpdate: row },
        }}
      >
        <Fab className={classes.fab} onClick={onClick} disabled={disabled}>
          <DoneIcon className={classes.icon} fontSize="small" />
        </Fab>
      </Link>
    ) : (
      <Link
        onClick={(e) => disabled && e.preventDefault()}
        to={{
          pathname: `/${URL_BASE}/regular/nueva-solicitud/${row.cod_solicitud_completa}`,
          state: { dataUpdate: row },
        }}
      >
        <Fab className={classes.fab} onClick={onClick} disabled={disabled}>
          <DoneIcon className={classes.icon} fontSize="small" />
        </Fab>
      </Link>
    )}
  </Tooltip>
);

export default withStyles(styles)(ActivateActionButton);
