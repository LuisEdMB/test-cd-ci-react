import React, { useState } from "react";
import { withRouter } from "react-router-dom";
// Redux
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
// Actions - Redux
import * as ActionOdcExpress from '../../../../../../actions/odc-express/odc'
// Components
import { CircularProgress, Fab, Tooltip, withStyles } from "@material-ui/core";
// Colors
import { green, orange } from "@material-ui/core/colors";
// Icons
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import { URL_BASE } from "../../../../../../actions/config";

function mapDispatchToProps(dispatch) {
  return {
    verifySolicitudeIsBlocked: bindActionCreators(ActionOdcExpress.verifySolicitudeIsBlocked, dispatch)
  }
}

const styles = _ => ({
  fab: {
    minHeight: 0,
    height: 24,
    width: 24,
    color: "white",
    transition: ".3s",
    backgroundColor: orange[700],
    "&:hover": {
      backgroundColor: orange[900],
    },
    textTransform: "none",
  },
  icon: {
    minHeight: 0,
    height: 12,
    width: 12,
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
    textTransform: 'none'
  }
});

const ProcessActionButton = (props) => {
  const { classes, disabled = false, row, history, verifySolicitudeIsBlocked } = props

  const [ loading, setLoading ] = useState(false)

  const handleVerifySolicitudeIsBlocked = _ => {
    if (row.cod_tipo_solicitud_originacion === "390001" && row.flg_cliente_existente) {
      history.push({
        pathname: `/${URL_BASE}/express/clientes-existentes/${row.cod_solicitud_completa}`,
        state: { dataUpdate: row }
      })
    }
    else {
      setLoading(true)
      const sendData = {
        solicitudeCode: row.cod_solicitud,
        completeSolicitudeCode: row.cod_solicitud_completa,
        clientCode: row.cod_cliente,
        documentTypeId: row.cod_tipo_documento,
        documentNumber: row.des_nro_documento
      }
      verifySolicitudeIsBlocked(sendData).then(response => {
        if (response?.success) {
          const { consultaClienteEnProceso } = response
          if (!consultaClienteEnProceso?.des_mensaje)
            history.push({
              pathname: row.cod_tipo_solicitud_originacion === "390001"
                ? `/${URL_BASE}/express/nueva-solicitud/${row.cod_solicitud_completa}`
                : `/${URL_BASE}/regular/nueva-solicitud/${row.cod_solicitud_completa}`,
              state: { dataUpdate: row }
            })
        }
        setLoading(false)
      })
    }
  }

  return (
    <Tooltip
      title={
        !disabled
          ? `Hacer Click! Retomar Proceso: ${row.cod_solicitud_completa}`
          : `Solicitud: ${row.cod_solicitud_completa}`
      }
      placement="left"
    >
      <Fab className={classes.fab} onClick={ handleVerifySolicitudeIsBlocked } disabled={loading || disabled}>
        <PlayArrowIcon className={classes.icon} fontSize="small" />
        {
          loading && <CircularProgress 
            size={ 24 } 
            className={ classes.buttonProgress }/>
        }
      </Fab>
    </Tooltip>
  )
}

export default withRouter(withStyles(styles)(connect(null, mapDispatchToProps)(ProcessActionButton)));
