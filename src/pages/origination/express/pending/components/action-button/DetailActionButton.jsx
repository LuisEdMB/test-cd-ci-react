import React, { Component } from "react";
import { 
    AppBar,
    Fab, 
    Tooltip, 
    withStyles, 
    Dialog, 
    DialogTitle,
    Typography,
    Grid,
    Button,
    DialogActions,
    DialogContent
 } from "@material-ui/core";
import ClientPreview from '../../../../../../components/info-preview/ClientPreview';

// Colors  
import blue from '@material-ui/core/colors/blue';
import orange from '@material-ui/core/colors/orange';
// Icons
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import CancelIcon from '@material-ui/icons/Cancel';

const styles = _ => ({
    clientProfileButton:{
        minHeight:0, 
        height:24,
        width:24, 
        margin:"0 .3em 0 0",
        backgroundColor: blue[700],
        color:"white",
        transition:".3s",
        '&:hover': {
            backgroundColor: blue[900],
        },
    }, 
    creditCardButton:{
        minHeight:0, 
        height:24,
        width:24, 
        margin:"0 0 0 .3em",
        backgroundColor: orange[700],
        color:"white",
        transition:".3s",
        '&:hover': {
            backgroundColor: orange[900],
        },
    },
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    button:{
        textTransform: "none"
    },
    appBar: {
        position: 'relative',
    }
});

class DetailActionButton extends Component{

    state = {
        openModal: false
    }
    handleClickOpenModal = () => {
        this.setState(state => ({ ...state, openModal: true}));
    }
    handleClickCloseModal = () => {
        this.setState(state => ({ ...state, openModal: false}));
    }

    render = () => {
        const { row, classes, disabled = true } = this.props;
        const { openModal } = this.state;
        return(
            <>
                <Tooltip title={!disabled ? `Hacer Click! Perfil Cliente: ${row.des_nro_documento_titular || row.des_nro_documento}` : "Datos del cliente no registrado." } placement="left">
                    <div>
                        <Fab 
                            disabled={disabled}
                            onClick={this.handleClickOpenModal}
                            className={classes.clientProfileButton}>
                            <AccountCircleIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>  
                {/* Modal Files */}                      
                <Dialog
                    fullWidth
                    maxWidth={"md"}
                    onClose={this.handleClickCloseModal}
                    open={openModal}
                    aria-labelledby="form-dialog">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-dialog" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                Información de Cliente
                            </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form >
                        <DialogContent>
                            <Grid container spacing={8}>
                                <Grid item xs={12}>
                                    {/* <ClientInfo data={row}/> */}
                                    <ClientPreview 
                                        data={{
                                            documentType: row.des_tipo_documento? row.des_tipo_documento: "",
                                            documentNumber: row.des_nro_documento ? row.des_nro_documento: "",
                                            fullName: row.des_nombre_completo? row.des_nombre_completo: "",
                                            firstName: row.des_primer_nom? row.des_primer_nom: "",
                                            secondName: row.des_segundo_nom? row.des_segundo_nom: "",
                                            lastName: row.des_ape_paterno? row.des_ape_paterno: "",
                                            motherLastName: row.des_ape_materno? row.des_ape_materno: "",
                                            birthday: row.fec_nacimiento? row.fec_nacimiento: "",
                                            maritalStatus: row.des_estado_civil? row.des_estado_civil : "",
                                            gender: row.des_genero? row.des_genero: "",
                                            nationality: row.des_nacionalidad? row.des_nacionalidad: "",
                                            academicDegree: row.des_gradoacademico? row.des_gradoacademico: "",
                                            email: row.des_correo? row.des_correo: "",
                                            telephone: row.des_telef_fijo? row.des_telef_fijo: "", 
                                            cellphone: row.des_telef_celular? row.des_telef_celular: "",
                                        }} 
                                    />
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={12}>
                                    <div>
                                        <Button 
                                            className={classes.button}
                                            fullWidth={true} 
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickCloseModal}
                                            margin="normal">
                                            Cancelar
                                            <CancelIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  
            </>
        )
    }
}


export default withStyles(styles)(DetailActionButton);
