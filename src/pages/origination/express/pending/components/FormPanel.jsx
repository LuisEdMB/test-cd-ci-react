import React, { Component } from "react";
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Autocomplete from '../../../../../components/Autocomplete';
import {  
    Grid,
    Button,
    TextField, 
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Typography,
    FormHelperText,
    withStyles
} from '@material-ui/core';
import { getIdentificationDocumentType } from '../../../../../actions/value-list/identification-document-type';
// Icons
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FilterListIcon from '@material-ui/icons/FilterList';
import SearchIcon from '@material-ui/icons/Search';
// Utils 
import { getNotistack } from '../../../../../utils/Notistack';
import { validateLengthDocumentType, checkInputKeyCode, onlyNumberKeyCode } from '../../../../../utils/Utils';
import { Bounce } from 'react-reveal';

const styles = theme => ({
    root:{
        width: "100%",
        borderRadius: "0 0 .5em .5em",
        boxShadow: 'none',
        borderBottom: "1px solid #80808033",
    },
    paper:{
        padding: 20
    },
    buttonWrapper:{
        display: "center",
        alignItems: "center"
    }, 
    heading:{
        display: "flex",
        alignItem: "center",
        [theme.breakpoints.down('sm')]: {
            fontSize:14, 
        },
        [theme.breakpoints.up('md')]: {
            fontSize:16, 
        }
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        idenfiticationDocumentType: state.identificationDocumentTypeReducer,
        odc: state.odcReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getIdentificationDocumentType: bindActionCreators(getIdentificationDocumentType, dispatch), 
    };
  return actions;
}

class FormPanel extends Component {
    state = {
        documentTypeError: false,
        documentTypeId: 0,
        documentType:"",
        documentTypeAux: "",
        documentTypeInternalValue: "",

        documentNumberError: false,
        documentNumber: "", 
    }

    componentDidMount = () => {
        // Get Api's Services
        this.props.getIdentificationDocumentType();
    }
    componentDidUpdate(prevProps, prevState) {
        // Identification Document Type - Before
        if (prevProps.idenfiticationDocumentType !== this.props.idenfiticationDocumentType){
            // Identification Document Type - Success Service 
            if(!this.props.idenfiticationDocumentType.loading &&
                this.props.idenfiticationDocumentType.response && 
                this.props.idenfiticationDocumentType.success){
                const idenfiticationDocumentType = this.props.idenfiticationDocumentType.data.find(item => item.val_orden === 1);
                this.setState(state => ({
                    ...state, 
                    documentTypeId: idenfiticationDocumentType.cod_valor,
                    documentType: idenfiticationDocumentType.des_valor,
                    documentTypeAux: idenfiticationDocumentType.des_auxiliar,
                    documentTypeInternalValue: idenfiticationDocumentType.valor_interno
                }));
            }
            // Identification Document Type - Error Service 
            else if(!this.props.idenfiticationDocumentType.loading &&
                     this.props.idenfiticationDocumentType.response && 
                    !this.props.idenfiticationDocumentType.success){
                getNotistack(`Tipo Documento: ${this.props.idenfiticationDocumentType.error}`, this.props.enqueueSnackbar, "error");
            }
            // Identification Document Type - Error Service Connectivity
            else if(!this.props.idenfiticationDocumentType.loading && 
                    !this.props.idenfiticationDocumentType.response && 
                    !this.props.idenfiticationDocumentType.success){
                getNotistack(`Tipo Documento: ${this.props.idenfiticationDocumentType.error}`, this.props.enqueueSnackbar, "error");
            }
        }
    }

    handleSubmitSearch = e => {
        e.preventDefault();
        const { 
            documentTypeAux,
            documentNumber
        } = this.state;

        let sendData  = { 
            documentTypeAux, 
            documentNumber
        }
        this.props.handleSubmitFilter(sendData);
    }

    // Client - Select - Event Change
    handleChangeSelectRequired = (name) => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";

        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }

        this.setState(state => ({
            ...state, 
            [name]: valueDefault, 
            [nameId]: idDefault,
            [nameError]: error,
        }));
    }

    handleChangeTextFieldRequired = name => e => {
        e.persist();
        let { value } = e.target;
        let nameError = `${name}Error`;

        this.setState(state => ({
            ...state,
            [name]: value, 
            [nameError]: value !== "" ? false : true
        }));
    }

    // Client - Document Number - TextField Event Change
    handleChangeTextFieldDocumentNumber = name => e =>{
        let { value } = e.target;
        let nameError = `${name}Error`;
        if(!isNaN(Number(value))){
            this.setState(state => ({
                [name]:value,
                [nameError]: !validateLengthDocumentType(value.length)
            }));        
        }
    }

    // Only Number
    handleKeyPressTextFieldOnlyNumber = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!onlyNumberKeyCode(code)){
            e.preventDefault();
        }   
    }

    // Check Input
    handleKeyPressTextFieldCheckInput = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!checkInputKeyCode(code)){
            e.preventDefault();
        }   
    }
    render = () => {
        const { idenfiticationDocumentType,
                classes, 
                title, 
                } = this.props;
        
        const idenfiticationDocumentTypeData = idenfiticationDocumentType.data.map((item, index) => ({
               value: item.cod_valor,
               label: item.des_valor_corto
           }
        ));
        
        return( <div>
                    <ExpansionPanel className={classes.root}  defaultExpanded>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel-content"
                            id="panel-header"
                        >
                            <Typography variant="h6" className={classes.heading}>
                                <FilterListIcon className="mr-2" />
                                {title}
                            </Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <div className="w-100">
                                <form method="post" autoComplete="off" onSubmit={this.handleSubmitSearch}>
                                    <Grid container spacing={8} className="mb-2">
                                        {/* Document Type */}
                                        <Grid item xs={12} sm={6} md={4} lg={2}>
                                            <Autocomplete                                             
                                                error={this.state.documentTypeError}
                                                onChange={this.handleChangeSelectRequired("documentType")}
                                                value={this.state.documentTypeId}
                                                data={idenfiticationDocumentTypeData}
                                                placeholder={"Tipo Documento"}
                                            />
                                        </Grid>
                                        {/* Document Number */}
                                        <Grid item xs={12} sm={6} md={4} lg={2}>
                                            <TextField
                                                error={this.state.documentNumberError}
                                                fullWidth={true}
                                                label="Nro Documento"
                                                type="text"
                                                margin="normal"
                                                color="default"
                                                id="documentNumber"
                                                name="documentNumber"
                                                value={this.state.documentNumber}
                                                onKeyPress={this.handleKeyPressTextFieldOnlyNumber("documentNumber")}
                                                onChange={this.handleChangeTextFieldDocumentNumber("documentNumber")}
                                                placeholder="Ingresar nro. documento."
                                                required
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        maxLength: 8
                                                        // maxLength: this.state.documentTypeId === 100001 ? 8:9,
                                                    }
                                                }}
                                            />
                                            <FormHelperText className="text-right">
                                                {this.state.documentNumber.toString().length}/{this.state.documentTypeId === 100001 ? 8 : 9}
                                            </FormHelperText>
                                        </Grid> 
                                          
                                        <Grid item xs={12} sm={12} md={4} lg={1} className="d-flex justify-content-center align-items-center">
                                            <div className="w-100">
                                                <Bounce> 
                                                    <div>
                                                        <Button 
                                                            className={classes.button} fullWidth variant="contained" type="submit"  color="primary"
                                                            disabled={this.props.odc.pendingProcesses.loading}>
                                                            Consultar
                                                            <SearchIcon  className="ml-2" size="small" />
                                                        </Button>
                                                    </div>
                                                </Bounce>
                                            </div>
                                        </Grid>
                                    </Grid>
                                </form>
                            </div>
                            
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </div>
            )
    }
}


FormPanel.defaultProps = {
    form:{
        agencyId:0,
        initialDateError:false,
        finalDateError:false,
        initialDate:"",
        finalDate:""
    }
};

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(FormPanel)));
