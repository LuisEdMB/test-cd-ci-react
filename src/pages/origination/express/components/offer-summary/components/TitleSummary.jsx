import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// Component
import Typography from '@material-ui/core/Typography';
// Icona
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
// Effects 
import Fade from 'react-reveal/Fade';
import Bounce from 'react-reveal/Bounce';
// Images
import Cencosud from '../../../../../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg';

const styles = theme => ({
    root:{
        display:"flex",
        justifyContent:"center",
        alignItems:"center"
    },
    titleWrapper:{
        margin:"0 1em",
    },
    title:{
        [theme.breakpoints.down('sm')]: {
            fontSize:18
        },
    },
    avatarWrapper:{
        [theme.breakpoints.down('sm')]: {
            display:"none"
        },
    },
    avatar:{
        margin:"0 .5em",
        height:36, 
        width:64,
        [theme.breakpoints.down('sm')]: {

        },
        [theme.breakpoints.up('md')]: {
            height:48, 
            width:80,
        },
        [theme.breakpoints.up('lg')]: {

        },
    },
    dateWrapper:{
        display:"none",
        [theme.breakpoints.up('md')]: {
            display:"flex",
            justifyContent:"space-between",
            alignItems:"center"
        },
    },
});

const TitleAdditionalSummary = ({ classes, date, title }) => (
    <div>
        <Fade>
            <figure className={classNames(classes.root, "p-3")}>
                <div className={classes.avatarWrapper}>
                    <Bounce>
                        <img className={classes.avatar} src={Cencosud} alt ="Cencosud" />
                    </Bounce>
                </div>
                <div className={classes.titleWrapper}>
                    <Typography 
                        align="center" 
                        component="title" 
                        className={classes.title}
                        variant="h6">
                        {title}
                    </Typography>
                </div>
                <div className={classes.dateWrapper}>
                    <Typography component="span" className="mt-1 mr-2">
                        <CalendarTodayIcon fontSize="small"/>
                    </Typography>
                    <Typography component="span">
                        {date}
                    </Typography>
                </div>
            </figure>
        </Fade>
    </div>
);

export default withStyles(styles)(TitleAdditionalSummary);