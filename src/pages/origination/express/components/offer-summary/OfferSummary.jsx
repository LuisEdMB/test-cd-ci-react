// React
import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
// Components
import {
    Divider,
    Button,
    IconButton,
    Grid,
    Tooltip,
    CircularProgress
} from '@material-ui/core';
// Components Custom
import TitleSummary from './components/TitleSummary';
import HeaderSummary from './components/HeaderSummary';
import BodySummary from './components/BodySummary';
// Colors
import green from '@material-ui/core/colors/green';
// Effects
import Zoom from 'react-reveal/Zoom';
import Bounce from 'react-reveal/Bounce';
// Icons
import CloseIcon from '@material-ui/icons/Close';
import SendIcon from '@material-ui/icons/Send';
import { getDateCurrent, maskCreditCard } from '../../../../../utils/Utils';
import * as Fireworks from 'fireworks-canvas' //


const styles = theme => ({
    root: {
        position: "relative"
    },
    firework: {
        width: "100%",
        height: 500,
        position: "absolute",
        top: 20
    },
    summaryWrapper: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    summary: {
        minWidth: 300,
        maxWidth: 700
    },
    button: {
        textTransform: 'none',
    },
    buttonProgressWrapper: {
        position: 'relative',
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    buttonActions: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }
});
class OfferSummary extends Component {
    state = {
        date: getDateCurrent(1, false, true).replace("T", " "),
        client: {
            documentType: "",
            documentNumber: "",
            fullName: ""
        },
        creditCard: {
            cardNumber: "0000********0000",
            type: "",
            name: "Clásica",
            brandId: 200001,
            brand: "MasterCard",
            colorId: 0,
            color: "",
            colorAux: "",
            bin: "529206"
        },
        origination: {
            number: 0,
            fullNumber: "000000000000"
        },
        summaryButtonDisabled: false,
        lineAvailable: 0,
        effectiveProvision: 0,
        isSAE: true,
        lineSAE: 0,
        maxAmount: 0,
        tcea: ""
    }
    componentWillMount = () => {
        let { client, creditCard, origination,
            lineAvailable, effectiveProvision,
            isSAEProcess, lineSAE, maxAmount, tcea
        } = this.props;
        if (client && creditCard && origination) {
            this.setState(state => ({
                client: {
                    ...state.client,
                    ...client
                },
                creditCard: {
                    ...state.creditCard,
                    ...creditCard
                },
                origination: {
                    ...state.origination,
                    ...origination
                },
                lineAvailable: lineAvailable,
                effectiveProvision: effectiveProvision,
                isSAEProcess: isSAEProcess,
                lineSAE: lineSAE,
                maxAmount: maxAmount,
                tcea: tcea
            }));
        }
    }
    componentDidMount = () => {
        // needs at least a container element, you can provide options
        // (options are optional, defaults defined below)
        const container = document.getElementById('summary-container')
        const options = {
            maxRockets: 3,            // max # of rockets to spawn
            rocketSpawnInterval: 10, // millisends to check if new rockets should spawn
            numParticles: 70,        // number of particles to spawn when rocket explodes (+0-10)
            explosionMinHeight: 0.2,  // percentage. min height at which rockets can explode
            explosionMaxHeight: 0.9,  // percentage. max height before a particle is exploded
            explosionChance: 0.08     // chance in each tick the rocket will explode
        }
        // instantiate the class and call start
        // this returns a disposable - calling it will stop fireworks.
        const fireworks = new Fireworks(container, options)
        fireworks.start()
        fireworks.fire()
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.offerSummary !== this.props.offerSummary) {
            if (this.props.offerSummary.loading) {
                this.setState(state => ({
                    summaryButtonDisabled: true
                }));
            }
            if (!this.props.offerSummary.loading && this.props.offerSummary.response && this.props.offerSummary.success) {
                this.getNotistack("Consulta Correcta, 6to Paso Ok!", "success");

                setTimeout(() => {
                    this.props.handleNext();
                }, 2500)

            }
            else if (!this.props.offerSummary.loading && this.props.offerSummary.response && !this.props.offerSummary.success) {
                // Notistack
                this.getNotistack(this.props.offerSummary.error, "error");
                this.setState(state => ({
                    summaryButtonDisabled: false
                }));
            }
            else if (!this.props.offerSummary.loading && !this.props.offerSummary.response && !this.props.offerSummary.success) {
                // Notistack
                this.getNotistack(this.props.offerSummary.error, "error");
                this.setState(state => ({
                    summaryButtonDisabled: false
                }));
            }
        }
    }
    handleOfferSummary = (e) => {
        this.props.handleOfferSummary();
    }

    // Notistack
    getNotistack(message, variant = "default", duration = 6000) {
        let select = "default";
        switch (variant) {
            case "error":
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default:
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit" />
                </IconButton>
            ),
        });
    }

    render() {
        let { classes, isSAEProcess, numero_cuotas, monto_cuota } = this.props;
        let { creditCard,
            client,
            origination,
            lineAvailable,
            effectiveProvision,
            lineSAE,
            maxAmount,
            tcea
        } = this.state;
        return (
            <Grid container spacing={8} className={classes.root}>
                <Grid item xs={12} id="summary-container" className={classes.firework}></Grid>
                <Grid item xs={12} className={classes.summaryWrapper}>
                    <Grid container spacing={8} className={classNames(classes.summary, "mb-4")}>
                        {/* Title*/}
                        <Grid item xs={12}>
                            <TitleSummary
                                title={isSAEProcess ? "¡Generación de Crédito Efectivo Cencosud Exitosa! " : "¡Generación de Tarjeta Exitosa!"}
                                date={this.state.date} />
                        </Grid>
                        <Grid item xs={12} className="mb-2">
                            <Divider />
                        </Grid>
                        {/* Header */}
                        <Grid item xs={12} className="py-3">
                            <HeaderSummary fullName={client.fullName} documentType={client.documentType} documentNumber={client.documentNumber} />
                        </Grid>
                        {/* Body */}
                        <Grid item xs={12}>
                            <BodySummary
                                fullNumber={origination.fullNumber}
                                creditCard={creditCard}
                                cardNumber={maskCreditCard(creditCard.cardNumber)}
                                lineAvailable={lineAvailable}
                                effectiveProvision={effectiveProvision}
                                isSAEProcess={isSAEProcess}
                                numero_cuotas={numero_cuotas}
                                monto_cuota={monto_cuota}
                                lineSAE={lineSAE}
                                maxAmount={maxAmount}
                                tcea={tcea}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12} className={classes.buttonActions}>
                    <Grid container spacing={8}>
                        {/* Button - Ok */}
                        <Grid item xs={12} sm={12} md={4} lg={3} style={{ margin: "0 auto" }}>
                            <Tooltip TransitionComponent={Zoom} title="Click para continuar con el proceso de originación.">
                                <div>
                                    <Bounce>
                                        <div className={classNames(classes.buttonProgressWrapper)}>
                                            <Button
                                                disabled={this.state.summaryButtonDisabled}
                                                variant="contained"
                                                type="submit"
                                                className={classNames(classes.button)}
                                                color="primary"
                                                size="small"
                                                onClick={this.handleOfferSummary}
                                                fullWidth={true}>
                                                Continuar
                                                <SendIcon fontSize="small" className="ml-2" />
                                                {this.state.summaryButtonDisabled && <CircularProgress size={24} className={classes.buttonProgress} />}
                                            </Button>
                                        </div>
                                    </Bounce>
                                </div>
                            </Tooltip>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(styles)(withSnackbar(OfferSummary));