import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// Custom Components 
import UbigeoAddress from './address-info/UbigeoAddress';
import ViaAddress from './address-info/ViaAddress';
import ZoneAddress from './address-info/ZoneAddress';
import ExtraAddress from './address-info/ExtraAddress';
import ReferenceAddress from './address-info/ReferenceAddress';
//Icons 
import CloseIcon from '@material-ui/icons/Close'; 
import BusinessIcon from '@material-ui/icons/Business';
//Actions 
import { getDepartment  } from '../../../../../../actions/department'; 
import { getWorkAddressProvince } from '../../../../../../actions/province'; 
import { getWorkAddressDistrict } from '../../../../../../actions/district'; 
import { getVia } from '../../../../../../actions/via'; 
import { getZone } from '../../../../../../actions/zone';
//Utils
import { onlyNumberRegex, onlyTextRegex, checkInputRegex } from '../../../../../../utils/Utils';

const styles = theme => ({
    root:{
        borderRadius:".5em"
    },
    heading:{
        display:"flex", 
        justifyContent:"center", 
        alignItems:"center",
        textTransform: "uppercase",
    },
    expansionPanel: {
        backgroundColor: "#f0f0f045",
        borderRadius: ".5em",
    },
    button:{
        textTransform: 'none',
    }
});

const mapStateToProps = (state, props) => {
    return {
        department:state.departmentReducer,
        provinceFilter:state.provinceReducer, 
        districtFilter:state.districtReducer,
        via:state.viaReducer, 
        zone:state.zoneReducer,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getDepartment:bindActionCreators(getDepartment, dispatch),
        getWorkAddressProvince:bindActionCreators(getWorkAddressProvince, dispatch),
        getWorkAddressDistrict:bindActionCreators(getWorkAddressDistrict, dispatch),
        getVia:bindActionCreators(getVia, dispatch),
        getZone:bindActionCreators(getZone, dispatch),
    };
    return actions;
}
class WorkAddressInfoPanel extends PureComponent {
    componentDidUpdate = (prevProps, prevState) => {
        // Department - Before
        if (prevProps.department !== this.props.department){
            // Department - Error Service 
            if(!this.props.department.loading &&
                this.props.department.response && 
                !this.props.department.success){
                this.getNotistack(`Dir. Laboral - Departamento: ${this.props.department.error}`, "error");
            }
            // Department- Error Service Connectivity
            else if(!this.props.department.loading && 
                    !this.props.department.response && 
                    !this.props.department.success){
                this.getNotistack(`Dir. Laboral - Departamento: ${this.props.department.error}`, "error");
            }
        }
        // Province - Before 
        if (prevProps.provinceFilter !== this.props.provinceFilter){
            if(this.props.provinceFilter.workAddressProvince.loading ){
                this.props.handleSetState(state => ({
                    client:{
                        ...state.client,
                        workAddress:{
                            ...state.client.workAddress,
                            provinceDisabled: true,
                            provinceLoading: true
                        }
                    }
                }));
            }

            if(!this.props.provinceFilter.workAddressProvince.loading &&
                this.props.provinceFilter.workAddressProvince.response && 
                this.props.provinceFilter.workAddressProvince.success){
                this.props.handleSetState(state => ({
                    client:{
                        ...state.client,
                        workAddress:{
                            ...state.client.workAddress,
                            provinceDisabled: false,
                            provinceLoading: false
                        }
                    }
                }));
            }
            // Work Address Province - Error Service 
            else if(!this.props.provinceFilter.workAddressProvince.loading &&
                this.props.provinceFilter.workAddressProvince.response && 
                !this.props.provinceFilter.workAddressProvince.success){
                if(this.props.provinceFilter.workAddressProvince.error){
                    this.getNotistack(`Dir. Laboral - Provincia: ${this.props.provinceFilter.homeAddressProvince.error}`, "error");
                    this.props.handleSetState(state => ({
                        client:{
                            ...state.client,
                            workAddress:{
                                ...state.client.workAddress,
                                provinceDisabled: false,
                                provinceLoading: false
                            }
                        }
                    }));
                }
            }
            // Work Address Province - Error Service Connectivity
            else if(!this.props.provinceFilter.workAddressProvince.loading && 
                    !this.props.provinceFilter.workAddressProvince.response && 
                    !this.props.provinceFilter.workAddressProvince.success){
                if(this.props.provinceFilter.workAddressProvince.error){
                    this.getNotistack(`Dir. Laboral - Provincia: ${this.props.provinceFilter.workAddressProvince.error}`, "error");
                    this.props.handleSetState(state => ({
                        client:{
                            ...state.client,
                            workAddress:{
                                ...state.client.workAddress,
                                provinceDisabled: false,
                                provinceLoading: false
                            }
                        }
                    }));
                }        
            }
        }
        // District - Before 
        if (prevProps.districtFilter !== this.props.districtFilter){
            if(this.props.districtFilter.workAddressDistrict.loading ){
                this.props.handleSetState(state => ({
                    client:{
                        ...state.client,
                        workAddress:{
                            ...state.client.workAddress,
                            districtDisabled: true,
                            districtLoading: true
                        }
                    }
                }));
            }

            if(!this.props.districtFilter.workAddressDistrict.loading &&
                this.props.districtFilter.workAddressDistrict.response && 
                this.props.districtFilter.workAddressDistrict.success){
                this.props.handleSetState(state => ({
                    client:{
                        ...state.client,
                        workAddress:{
                            ...state.client.workAddress,
                            districtDisabled: false,
                            districtLoading: false
                        }
                    }
                }));
            }
            // Work Address District - Error Service 
            else if(!this.props.districtFilter.workAddressDistrict.loading &&
                this.props.districtFilter.workAddressDistrict.response && 
                !this.props.districtFilter.workAddressDistrict.success){
                if(this.props.districtFilter.workAddressDistrict.error){
                    this.getNotistack(`Dir. Laboral - Distrito: ${this.props.districtFilter.workAddressDistrict.error}`, "error");
                    this.props.handleSetState(state => ({
                        client:{
                            ...state.client,
                            workAddress:{
                                ...state.client.workAddress,
                                districtDisabled: false,
                                districtLoading: false
                            }
                        }
                    }));
                }
            }
            // Work Address District - Error Service Connectivity
            else if(!this.props.districtFilter.workAddressDistrict.loading && 
                    !this.props.districtFilter.workAddressDistrict.response && 
                    !this.props.districtFilter.workAddressDistrict.success){
                if(this.props.districtFilter.workAddressDistrict.error){
                    this.getNotistack(`Dir. Laboral - Distrito: ${this.props.districtFilter.workAddressDistrict.error}`, "error");
                    this.props.handleSetState(state => ({
                        client:{
                            ...state.client,
                            workAddress:{
                                ...state.client.workAddress,
                                districtDisabled: false,
                                districtLoading: false
                            }
                        }
                    }));
                }        
            }
        }
        // Via - Before
        if (prevProps.via !== this.props.via){
            // Via - Error Service 
            if(!this.props.via.loading &&
                this.props.via.response && 
                !this.props.via.success){
                
                this.getNotistack(`Vía: ${this.props.via.error}`, "error");
            }
            // Via - Error Service Connectivity
            else if(!this.props.via.loading && 
                    !this.props.via.response && 
                    !this.props.via.success){
                this.getNotistack(`Vía: ${this.props.via.error}`, "error");
            }
        }
        // Zone - Before
        if (prevProps.zone !== this.props.zone){
            // Zone - Error Service 
            if(!this.props.zone.loading &&
                this.props.zone.response && 
                !this.props.zone.success){
                
                this.getNotistack(`Zona: ${this.props.zone.error}`, "error");
            }
            // Zone - Error Service Connectivity
            else if(!this.props.zone.loading && 
                    !this.props.zone.response && 
                    !this.props.zone.success){
                this.getNotistack(`Zona: ${this.props.zone.error}`, "error");
            }
        }
    }
    componentDidMount = () => {
        // Get API's -  Sever ODC
        this.props.getDepartment();
        this.props.getVia();
        this.props.getZone();
    }
    // Filter Departament
    handleChangeSelectDepartmentRequired = (e, name) => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let nameFocus = `${name}Focus`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        let provinceError = false;
        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
            provinceError = true;
            let data = {
                type:"PR",
                value: value
            }
            this.props.getWorkAddressProvince(data); 
        }
        this.props.handleSetState(state => ({
            client:{
                ...state.client,
                workAddress:{
                    ...state.client.workAddress,
                    [name]: valueDefault, 
                    [nameId]: idDefault,
                    [nameError]: error,
                    [nameFocus]: false,
                    provinceError: provinceError,
                    provinceDisabled: true,
                    provinceId:"",
                    province: "",
                    districtDisabled: true,
                    districtId:"",
                    district: ""
                }
            }
        }));
    }
    // Filter Province
    handleChangeSelectProvinceRequired = (e, name) => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let nameFocus = `${name}Focus`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        let districtError = false;
        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
            districtError = true;
            let data = {
                type:"DI",
                value:value
            }
            this.props.getWorkAddressDistrict(data);
        }
        this.props.handleSetState(state => ({
            client:{
                ...state.client,
                workAddress:{
                    ...state.client.workAddress,
                    [name]: valueDefault, 
                    [nameId]: idDefault,
                    [nameError]: error,
                    [nameFocus]: false,
                    districtError: districtError,
                    districtDisabled: true,
                    districtId:"",
                    district: ""
                }
            }
        }));
    }
    // Filter District
    handleChangeSelectDistrictRequired = (e, name) => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let nameFocus = `${name}Focus`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }
        this.props.handleSetState(state => ({
            client:{
                ...state.client,
                workAddress:{
                    ...state.client.workAddress,
                    [name]: valueDefault, 
                    [nameId]: idDefault,
                    [nameError]: error,
                    [nameFocus]: false,
                }
            }
        }));
    }
    // Work Address - Zone - Event Change
    handleChangeSelectZone = e => {
        let nameZoneDisabled = true;
        let nameZone = "";
        let nameZoneError = true;
        if(e !== null){
            if(this.props.state.client.workAddress.nameZone !== ""){
                nameZoneError = false;
            }
            nameZoneDisabled = false;
            nameZone =  this.props.state.client.workAddress.nameZone;
            // Get  Value
            let { value, label } = e;
            // Get all value
            let object = this.props.zone.data.find((item => item.cod_valor ===  value));
            // Set Value in state
            this.props.handleSetState(state => ({
                client:{
                    ...state.client, 
                    workAddress:{
                        ...state.client.homeAddress, 
                        zone:label,
                        zoneId:value, 
                        zoneInternalValue: object.des_auxiliar,
                        nameZone:nameZone,
                        nameZoneError:nameZoneError,
                        nameZoneDisabled:nameZoneDisabled
                    }
                }
            }));
        }
        else{
            this.props.handleSetState(state => ({
                client:{
                    ...state.client, 
                    workAddress:{
                        ...state.client.workAddress, 
                        zone:"", 
                        zoneId:"",
                        nameZone:"",
                        nameZoneError:!nameZoneError,
                        nameZoneDisabled:nameZoneDisabled
                    }
                }
            }));
        }
    }
    // Work Address - Select - Event Change
    handleChangeSelectRequired = (e, name) => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let nameFocus = `${name}Focus`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }
        this.props.handleSetState(state => ({
            client:{
                ...state.client,
                workAddress:{
                    ...state.client.workAddress,
                    [name]: valueDefault, 
                    [nameId]: idDefault,
                    [nameError]: error,
                    [nameFocus]: false,
                }
            }
        }));
    }
    // Work Addres - TextField Check Input Required - Event Change 
    handleChangeTextFieldCheckInputRequired = e => {
        let { value, name } = e.target;
        let nameError = `${name}Error`;
        if(checkInputRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                        ...state.client,
                        workAddress:{
                            ...state.client.workAddress,
                            [name]:value,
                            [nameError]: value !== "" ? false : true
                        }
                    }
                })
            );
        }
    }
    // Work Addres - TextField Only Text Required - Event Change 
    handleChangeTextFieldOnlyTextRequired = e => {
        let { value, name } = e.target;
        let nameError = `${name}Error`;
        if(onlyTextRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                        ...state.client,
                        workAddress:{
                            ...state.client.workAddress,
                            [name]:value,
                            [nameError]: value !== "" ? false : true
                        }
                    }
                })
            );
        }
    }
    // Work Addres - TextField Only Text - Event Change 
    handleChangeTextFieldOnlyText = e => {
        let { value, name } = e.target;
        if(onlyTextRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                    ...state.client,
                        workAddress:{
                            ...state.client.workAddress,
                            [name]:value,
                        }
                    }
                })
            )
        }
    }
    // Work Addres - TextField Only Number - Event Change 
    handleChangeTextFieldOnlyNumber = e => {
        let { value, name } = e.target;
        if(onlyNumberRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                        ...state.client,
                        workAddress:{
                            ...state.client.workAddress,
                            [name]:value,
                        }
                    }
                })
            );
        }
    }
    // Work Addres - TextField Check Input Required - Event Change 
    handleChangeTextFieldCheckInput = e => {
        let { value, name } = e.target;
        if(checkInputRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                        ...state.client,
                        workAddress:{
                            ...state.client.workAddress,
                            [name]:value
                        }
                    }
                })
            );
        }
    }
    // Generic - Full Value - Required 
    handleChangeSelectFullViaRequired = (e, name) =>{
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let nameFocus = `${name}Focus`;
        let nameInternalValue = `${name}InternalValue`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        let internalValue = "";
        if(e !== null){
            error = false;
            let { label, value } = e;
            let object = this.props.via.data.find((item => item.cod_valor ===  value))
            valueDefault = label;
            idDefault = value;
            internalValue = object.valor_interno;
        }
        this.props.handleSetState(state => ({
            client:{
                ...state.client,
                workAddress:{
                    ...state.client.workAddress,
                    [name]: valueDefault, 
                    [nameId]: idDefault,
                    [nameError]: error,
                    [nameFocus]: false,
                    [nameInternalValue]: internalValue
                }
            }
        }));
    }
    // Notistack
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render(){
        let { state, fullLength, defaultLength, complementaryAddressLength=6, 
              classes, department, provinceFilter, districtFilter, via, zone }  = this.props;
        // Format Home Address Department 
        let departmentData = department.data.map(item => ({
            value: item.cod_det_ubi, //cod_ubigeo
            label: item.val_ubigeo
        })); 
        // Format Home Address Province 
        let provinceData = provinceFilter.workAddressProvince.data.map(item => ({
            value: item.cod_det_ubi,
            label: item.val_ubigeo
        })); 
        // Format Home Address District 
        let districtData = districtFilter.workAddressDistrict.data.map(item => ({
            value: item.cod_det_ubi,
            label: item.val_ubigeo
        })); 
        // Format Home Address Via 
        let viaData = via.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        })); 
        // Format Home Address Zone 
        let zoneData = zone.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        })); 

        return (
            <ExpansionPanel className={classes.root}>
                <ExpansionPanelSummary 
                    className={classes.expansionPanel}
                    expandIcon={<ExpandMoreIcon fontSize="small" />}>
                    <Typography className={classes.heading}>
                        <BusinessIcon fontSize="small" className="mr-2"/>
                        Dirección Personal
                    </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails >
                    <Grid container spacing={8}>
                        <Grid item xs={12}>
                            <UbigeoAddress 
                                department={departmentData}
                                province={provinceData}
                                district={districtData}
                                ubigeo={state.client.workAddress}
                                handleChangeSelectDepartment={this.handleChangeSelectDepartmentRequired}
                                handleChangeSelectProvince={this.handleChangeSelectProvinceRequired} 
                                handleChangeSelectDistrict={this.handleChangeSelectDistrictRequired}
                            />
                            <ViaAddress 
                                via = {viaData} 
                                defaultLength = {defaultLength}
                                handleChangeSelectVia = {this.handleChangeSelectFullViaRequired}
                                handleChangeTextFieldNameVia = {this.handleChangeTextFieldCheckInputRequired }
                                address={state.client.workAddress}
                            />
                            <ExtraAddress
                                handleChangeNumber={this.handleChangeTextFieldOnlyNumber}
                                handleChangeBuilding={this.handleChangeTextFieldCheckInput}
                                handleChangeInside={this.handleChangeTextFieldCheckInput}
                                handleChangeMZ={this.handleChangeTextFieldCheckInput}
                                handleChangeLot={this.handleChangeTextFieldCheckInput}
                                address={state.client.workAddress}
                                complementaryAddressLength={complementaryAddressLength}
                            />
                            <ZoneAddress 
                                handleChangeSelectZone={this.handleChangeSelectZone}
                                handleChangeTextFieldZoneName={this.handleChangeTextFieldCheckInputRequired}
                                defaultLength={defaultLength}
                                zone={zoneData}
                                address={state.client.workAddress}
                            />
                            <Grid container spacing={8}>
                                {/* Client - Home Address - Reference */}  
                                <Grid item xs={12} sm={12} md={12} lg={12}>
                                    <ReferenceAddress 
                                        reference={state.client.workAddress.reference}
                                        handleChangeTextFieldReference={this.handleChangeTextFieldCheckInput}
                                        fullLength={fullLength}
                                    />
                                </Grid>    
                            </Grid>    
                        </Grid>
                    </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        )
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(WorkAddressInfoPanel)));