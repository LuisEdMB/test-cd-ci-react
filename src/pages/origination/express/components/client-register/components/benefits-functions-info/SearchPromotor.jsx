import React from 'react';
// Components 
import {
    Grid, 
    TextField, 
    Typography, 
    FormHelperText, 
    InputAdornment 
}
from '@material-ui/core';
// Icons
import CodeIcon from '@material-ui/icons/Code';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';

const SearchPromotor = ({
    extra,
    handleKeyPressTextFieldScannerCode,
    handleChangeTextFieldScannerCode
}) => (
        <>
            {/* Client - Extra - Scanner Code */}
            <Grid item xs={12} sm={6} md={4} lg={4}>
                <TextField
                    error={extra.scannerCodeError}
                    fullWidth={true}
                    required
                    label="Código de Escáner / DNI / Usuario de Red"
                    name="scannerCode"
                    // value={extra.scannerCode}
                    onKeyPress={handleKeyPressTextFieldScannerCode}
                    onChange={handleChangeTextFieldScannerCode}
                    type="text"
                    margin="normal"
                    InputProps={{
                        inputProps:{
                            maxLength: 20,
                        },
                        endAdornment: (
                            <InputAdornment position="end">
                                <CodeIcon fontSize="small" color={extra.scannerCodeError? "secondary":"inherit"} />
                            </InputAdornment>
                        )
                    }}
                />
                <FormHelperText component="span">
                    <Typography 
                        component="span"  
                        variant="inherit"
                        color={extra.scannerCodeError === true ? "secondary":"default"}>
                        {"Presionar <Enter> para buscar promotor"}
                    </Typography>
                </FormHelperText>
            </Grid>
            {/* Client - Extra - Promoter Name */}
            <Grid item xs={12} sm={6} md={4} lg={4}>
                <TextField
                    error={extra.promotorError}
                    fullWidth={true}
                    label="Nombre Promotor"
                    value={extra.promoterName}
                    type="text"
                    margin="normal"
                    InputLabelProps={{ shrink: true }}
                    InputProps={{
                        readOnly:true,
                        endAdornment: (
                            <InputAdornment position="end">
                                <PermIdentityIcon fontSize="small" color={extra.promotorError? "secondary":"inherit"} />
                            </InputAdornment>
                        )
                    }}
                />
                 <FormHelperText component="span">
                    <Typography 
                        component="span"  
                        variant="inherit"
                        color={ extra.promotorError === true? "secondary":"default"}>
                        { extra.promotorError  && "Volver a realizar la búsqueda, por favor."}
                    </Typography>
                </FormHelperText>
            </Grid>
        </>
    )
    
export default React.memo(SearchPromotor);