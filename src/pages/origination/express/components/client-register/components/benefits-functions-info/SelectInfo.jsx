
import React from 'react';
// Components
import { 
    Grid
} from '@material-ui/core'
// Components Custom
import Autocomplete from '../../../../../../../components/Autocomplete';

const SelectInfo = ({
    paymentDate=[], 
    sendCorrespondence=[],
    accountStatus=[],
    extra,
    handleChangeSelectPaymentDate,
    handleChangeSelectSendCorrespondence,
    handleChangeSelectAccountStatus }) => (
    <>
        {/* Client - Extra - Payment Date */}        
        <Grid item xs={12} sm={6} md={4} lg={4}>
            <Autocomplete 
                error={extra.paymentDateError}
                onChange={handleChangeSelectPaymentDate}
                data={paymentDate}
                value={extra.paymentDateId}
                placeholder={"Fecha de Pago *"}
            /> 
        </Grid> 
        {/* Client - Extra - Send Correspondence */}
        <Grid item xs={12} sm={6} md={4} lg={4}>
            <Autocomplete 
                error={extra.sendCorrespondenceError}
                onChange={handleChangeSelectSendCorrespondence}
                data={sendCorrespondence}
                value={extra.sendCorrespondenceId}
                placeholder={"Envio Correspondencia *"}
            /> 
        </Grid>      
        {/* Client - Extra - Account Status */}
        <Grid item xs={12} sm={6} md={4} lg={4}>
            <Autocomplete 
                error={extra.accountStatusError}
                onChange={handleChangeSelectAccountStatus}
                data={accountStatus}
                value={extra.accountStatusId}
                placeholder={"Estado de Cuenta *"}
            /> 
        </Grid> 
    </>
) 

export default SelectInfo;

