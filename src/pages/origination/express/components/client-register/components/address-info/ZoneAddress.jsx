
import React from 'react';
// Components 
import { 
    Grid,
    InputAdornment, 
    TextField
} from '@material-ui/core';
// Custom Components
import Autocomplete from '../../../../../../../components/Autocomplete';
// Icons
import NearMeIcon from '@material-ui/icons/NearMe';

const ZoneAddress = ({  
            zone = [],
            handleChangeSelectZone,
            handleChangeTextFieldZoneName,
            defaultLength,
            address
        }) => (
        <Grid container spacing={8}>
            {/* Client - Home Address - Zone */}
            <Grid item xs={12} sm={4}  md={4} lg={2}>
                <Autocomplete 
                    error={address.zoneError}
                    onChange={(e)=> handleChangeSelectZone(e, "zone")}
                    data={zone}
                    value={address.zoneId}
                    placeholder={!address.nameZoneDisabled? "Zona *": "Zona"}
                /> 
            </Grid>
            {/* Client - Home Address - Name Zone */}  
            <Grid item xs={12} sm={8} md={8} lg={6}>
                <TextField
                    error={address.nameZoneError}
                    fullWidth={true}
                    required={!address.nameZoneDisabled}
                    disabled={address.nameZoneDisabled}
                    label="Nombre Zona"
                    value={address.nameZone}
                    name="nameZone"
                    onChange={handleChangeTextFieldZoneName}
                    type="text"
                    margin="normal"
                    InputProps={{
                        inputProps:{
                            maxLength: defaultLength,
                        },
                        endAdornment: (
                            <InputAdornment position="end">
                                <NearMeIcon fontSize="small" color={address.nameZoneError? "secondary":"inherit"} />
                            </InputAdornment>
                        )
                    }}
                />
            </Grid>     
        </Grid>
    )


export default React.memo(ZoneAddress);