
import React from 'react';
import {
    Grid, 
    TextField, 
    InputAdornment} from '@material-ui/core';
//Icons 
import Filter9PlusIcon from '@material-ui/icons/Filter9Plus';
import HomeIcon from '@material-ui/icons/Home';
import LastPageIcon from '@material-ui/icons/LastPage';
import CropDinIcon from '@material-ui/icons/CropDin';
import TabUnselectedIcon from '@material-ui/icons/TabUnselected';

const ExtraAddress = ({   
            address,
            complementaryAddressLength,
            handleChangeNumber,
            handleChangeBuilding,
            handleChangeInside,
            handleChangeMZ,
            handleChangeLot
        }) => (
        <Grid container spacing={8}>
            {/* Client - Address - Number */}  
            <Grid item xs={6} sm={4} md={4} lg={2}>
                <TextField
                    fullWidth={true}
                    label="Número"
                    value={address.number}
                    onChange={handleChangeNumber}
                    type="text"
                    name="number"
                    margin="normal"
                    InputProps={{
                        inputProps:{
                            maxLength: complementaryAddressLength,
                        },
                        endAdornment: (
                            <InputAdornment position="end">
                                <Filter9PlusIcon fontSize="small" />
                            </InputAdornment>
                        )
                    }}
                />
            </Grid>       
            {/* Client - Home Address - Department */}  
            <Grid item xs={6} sm={4} md={4} lg={2}>
                <TextField
                    fullWidth={true}
                    label="Departamento"
                    value={address.building}
                    onChange={handleChangeBuilding}
                    type="text"
                    name="building"
                    margin="normal"
                    InputProps={{
                        inputProps:{
                            maxLength: complementaryAddressLength,
                        },
                        endAdornment: (
                            <InputAdornment position="end">
                                <HomeIcon fontSize="small" />
                            </InputAdornment>
                        )
                    }}
                />
            </Grid>   
            {/* Client - Address - Inside */}  
            <Grid item xs={6} sm={4} md={4} lg={2}>
                <TextField
                    fullWidth={true}
                    label="Interior"
                    value={address.inside}
                    name="inside"
                    onChange={handleChangeInside}
                    type="text"
                    margin="normal"
                    InputProps={{
                        inputProps:{
                            maxLength: complementaryAddressLength,
                        },
                        endAdornment: (
                            <InputAdornment position="end">
                                <LastPageIcon fontSize="small"/>
                            </InputAdornment>
                        )
                    }}
                />
            </Grid>     
            {/* Client - Address - MZ */} 
            <Grid item xs={6} sm={4} md={4} lg={2}>
                <TextField
                    fullWidth={true}
                    label="Mz"
                    value={address.mz}
                    name="mz"
                    onChange={handleChangeMZ}
                    type="text"
                    margin="normal"
                    InputProps={{
                        inputProps:{
                            maxLength: complementaryAddressLength,
                        },
                        endAdornment: (
                            <InputAdornment position="end">
                                <CropDinIcon fontSize="small"/>
                            </InputAdornment>
                        )
                    }}
                />
            </Grid>   
            {/* Client - Address - Lot */}  
            <Grid item xs={6} sm={4} md={4} lg={2}>
                <TextField
                    fullWidth={true}
                    id="clientLot"
                    label="Lote"
                    value={address.lot}
                    name="lot"
                    onChange={handleChangeLot}
                    type="text"
                    margin="normal"
                    InputProps={{
                        inputProps:{
                            maxLength: complementaryAddressLength,
                        },
                        endAdornment: (
                            <InputAdornment position="end">
                                <TabUnselectedIcon fontSize="small"/>
                            </InputAdornment>
                        )
                    }}
                />
            </Grid>
        </Grid>
    )

export default React.memo(ExtraAddress);