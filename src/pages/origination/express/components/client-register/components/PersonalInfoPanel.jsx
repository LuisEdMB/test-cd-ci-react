import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import { debounce } from 'throttle-debounce';

// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// Custom Components 
import PersonalInfoWrapper from './personal-info/PersonalInfoWrapper'
//Icons 
import CloseIcon from '@material-ui/icons/Close'; 
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
// Utils
import { 
    onlyNumberRegex, 
    onlyTextRegex, 
    checkInputRegex, 
    getDateCurrent } from '../../../../../../utils/Utils';
//Actions 
import { getIdentificationDocumentType } from '../../../../../../actions/identification-document-type';
import { getGender } from '../../../../../../actions/gender';
import { getMaritalStatus } from '../../../../../../actions/marital-status';
import { getAcademicDegree } from '../../../../../../actions/academic-degree';
import { getCountry } from '../../../../../../actions/country';

const styles = theme => ({
    root:{
        borderRadius:".5em"
    },
    heading:{
        display:"flex", 
        justifyContent:"center", 
        alignItems:"center",
        textTransform: "uppercase",
    },
    expansionPanel: {
        backgroundColor: "#f0f0f045",
        borderRadius: ".5em",
    },
    button:{
        textTransform: 'none',
    }
});

const mapStateToProps = (state, props) => {
    return {
        idenfiticationDocumentType:state.identificationDocumentTypeReducer,
        gender:state.genderReducer, 
        maritalStatus:state.maritalStatusReducer, 
        academicDegree:state.academicDegreeReducer, 
        country:state.countryReducer,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getIdentificationDocumentType:bindActionCreators(getIdentificationDocumentType, dispatch),
        getGender:bindActionCreators(getGender, dispatch),
        getMaritalStatus:bindActionCreators(getMaritalStatus, dispatch),
        getAcademicDegree:bindActionCreators(getAcademicDegree, dispatch),
        getCountry:bindActionCreators(getCountry, dispatch)
    };
    return actions;
}

class PersonalInfoPanel extends PureComponent {
    constructor(props){
        super(props);
        let maxDate = getDateCurrent(3);
        this.minBirthday = "1950-01-01";
        this.maxBirthday = maxDate.split("T")[0];
    }
    componentDidMount(){
        // Get API's -  Sever ODC
        this.props.getIdentificationDocumentType();
        this.props.getGender();
        this.props.getMaritalStatus();
        this.props.getCountry();
        this.props.getAcademicDegree();
    }
    componentDidUpdate(prevProps, prevState) {
        // Identification Document Type - Before
        if (prevProps.idenfiticationDocumentType !== this.props.idenfiticationDocumentType){
            // Identification Document Type - Success Service 
            if(!this.props.idenfiticationDocumentType.loading &&
                this.props.idenfiticationDocumentType.response && 
                this.props.idenfiticationDocumentType.success){
            }
            // Identification Document Type - Error Service 
            if(!this.props.idenfiticationDocumentType.loading &&
                this.props.idenfiticationDocumentType.response && 
                !this.props.idenfiticationDocumentType.success){
                
                this.getNotistack(`Tipo Documento: ${this.props.idenfiticationDocumentType.error}`, "error");
            }
            // Identification Document Type - Error Service Connectivity
            else if(!this.props.idenfiticationDocumentType.loading && 
                    !this.props.idenfiticationDocumentType.response && 
                    !this.props.idenfiticationDocumentType.success){
                this.getNotistack(`Tipo Documento: ${this.props.idenfiticationDocumentType.error}`, "error");
            }
        }
        // Gender - Before
        if (prevProps.gender !== this.props.gender){
             // Gender - Success Service 
             if(!this.props.gender.loading &&
                this.props.gender.response && 
                this.props.gender.success){    
            }
            // Gender - Error Service 
            if(!this.props.gender.loading &&
                this.props.gender.response && 
                !this.props.gender.success){
                this.getNotistack(`Género: ${this.props.gender.error}`, "error");
            }
            // Gender - Error Service Connectivity
            else if(!this.props.gender.loading && 
                    !this.props.gender.response && 
                    !this.props.gender.success){
                this.getNotistack(`Género: ${this.props.gender.error}`, "error");
            }
        }
        // Marital Status - Before
        if (prevProps.maritalStatus !== this.props.maritalStatus){
             // Marital Status - Error Service 
             if(!this.props.maritalStatus.loading &&
                this.props.maritalStatus.response && 
                this.props.maritalStatus.success){
            }
            // Marital Status - Error Service 
            if(!this.props.maritalStatus.loading &&
                this.props.maritalStatus.response && 
                !this.props.maritalStatus.success){
                this.getNotistack(`Estado Civil: ${this.props.maritalStatus.error}`, "error");
            }
            // Marital Status - Error Service Connectivity
            else if(!this.props.maritalStatus.loading && 
                    !this.props.maritalStatus.response && 
                    !this.props.maritalStatus.success){
                this.getNotistack(`Estado Civil: ${this.props.maritalStatus.error}`, "error");
            }
        }
        // Country - Before
        if (prevProps.country !== this.props.country){
            // Country - Error Service 
            if(!this.props.country.loading &&
                this.props.country.response && 
                this.props.country.success){
            }
            // Country - Error Service 
            if(!this.props.country.loading &&
                this.props.country.response && 
                !this.props.country.success){
                
                this.getNotistack(`Nacionalidad: ${this.props.country.error}`, "error");
                this.setState({
                    ...this.state,
                    nationalityDisabled: true
                });
            }
            // Country - Error Service Connectivity
            else if(!this.props.country.loading && 
                    !this.props.country.response && 
                    !this.props.country.success){
                this.getNotistack(`Nacionalidad: ${this.props.country.error}`, "error");
                this.setState({
                    ...this.state,
                    nationalityDisabled: true
                });
            }
        }
        // Academic Degree - Before
        if (prevProps.academicDegree !== this.props.academicDegree){
            // Academic Degree - Error Service 
            if(!this.props.academicDegree.loading &&
                this.props.academicDegree.response && 
                !this.props.academicDegree.success){
                
                this.getNotistack(`Grado Académico: ${this.props.academicDegree.error}`, "error");
            }
            // Academic Degree - Error Service Connectivity
            else if(!this.props.academicDegree.loading && 
                    !this.props.academicDegree.response && 
                    !this.props.academicDegree.success){
                this.getNotistack(`Grado Académico: ${this.props.academicDegree.error}`, "error");
            }
        }
    }

    // Client - Document Type - Select Event Change
    handleChangeSelectDocumentType = (e) => {
        let object = this.props.idenfiticationDocumentType.data.find(option => option.cod_valor === e.target.value);
        this.props.handleSetState(state => ({
            client:{
                ...state.client,
                documentType:object.des_valor,
                documentTypeId:object.cod_valor,
                documentTypeInternalValue:object.valor_interno, 
                documentNumber:""
            }
        }));
    } 
    // Validate Length Document Type
    validateLengthDocumentType = (value) => {
        switch(value){
            case 8 : return false;
            case 9 : return false;
            default : return true;
        }
    }
    // Client - Document Number - TextFiled Event Change
    handleChangeTextFieldDocumentNumber = (e) =>{
        let {value, name} = e.target;
        let nameError = `${name}Error`;
        if(onlyNumberRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                    ...state.client,
                    [name]:value,
                    [nameError]: this.validateLengthDocumentType(value.length)
                }
            }));
        }
    }
    // Generic - TextField - Required
    handleChangeTextFieldRequired = ({target: {value, name}}) => {
        let nameError = `${name}Error`;
        this.props.handleSetState(state => ({
                client:{
                    ...state.client,
                    [name]:value,
                    [nameError]: value !== "" ? false : true
                }
            })
        );
    }
    // Generic - TextField
    handleChangeTextField = ({target: {value, name}}) => {
        this.props.handleSetState(state => ({
                client:{
                    ...state.client,
                    [name]:value
                }
            })
        );
    }
    // Generic - TextField - Only Text - Required
    handleChangeTextFieldOnlyTextRequired = ({target: {value, name}}) => {
        let nameError = `${name}Error`;
        if(onlyTextRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                        ...state.client,
                        [name]:value,
                        [nameError]: value !== "" ? false : true
                    }
                })
            );
        }
    }
    // Generic - TextField - Only Number - Required
    handleChangeTextFieldOnlyNumberRequired = ({target: {value, name}}) => {
        let nameError = `${name}Error`;
        if(onlyNumberRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                        ...state.client,
                        [name]:value,
                        [nameError]: value !== "" ? false : true
                    }
                })
            );
        }
    }
    // Generic - TextField - Only Number - Required
    handleChangeTextFieldCheckInputRequired = ({target: {value, name}}) => {
        let nameError = `${name}Error`;
        if(checkInputRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                        ...state.client,
                        [name]:value,
                        [nameError]: value !== "" ? false : true
                    }
                })
            );
        }
    }
    // Generic - TextField - Only Text 
    handleChangeTextFieldOnlyText = ({target: {value, name}}) => {
        if(onlyTextRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                        ...state.client,
                        [name]:value,
                    }
                })
            )
        }
    }
    // Generic - TextField - Only Number
    handleChangeTextFieldOnlyNumber = ({target: {value, name}}) => {
        if(onlyNumberRegex(value)){
            this.props.handleSetState(state => ({
                client:{
                        ...state.client,
                        [name]:value,
                    }
                })
            );
        }
    }
    // Generic - Select - Required 
    handleChangeSelectRequired = (e, name) => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let nameFocus = `${name}Focus`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }
        this.props.handleSetState(state => ({
            client:{
                ...state.client,
                [name]: valueDefault, 
                [nameId]: idDefault,
                [nameError]: error,
                [nameFocus]: false
            }
        }));
    }
    // Generic - Full Value - Required 
    handleChangeSelectFullRequired = (e, name) =>{
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let nameFocus = `${name}Focus`;
        let nameInternalValue = `${name}InternalValue`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        let internalValue = "";
        if(e !== null){
            error = false;
            let { label, value } = e;
            let object = this.props.maritalStatus.data.find((item => item.cod_valor ===  value))
            valueDefault = label;
            idDefault = value;
            internalValue = object.valor_interno;

        }
        this.props.handleSetState(state => ({
            client:{
                ...state.client,
                [name]: valueDefault, 
                [nameId]: idDefault,
                [nameInternalValue]:internalValue,
                [nameError]: error,
                [nameFocus]: false
            }
        }));
    }

    // Notistack
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render(){
        let {
            classes, idenfiticationDocumentType, gender, maritalStatus, country, academicDegree,
            state, defaultLength, emailLength, cellphoneLength } = this.props;
        // Format Gender
        let genderData = gender.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor,
        })); 
        // Format Marital Status 
        let maritalStatusData = maritalStatus.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor,
        })); 
        // Format Nationality 
        let countryData = country.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor,
        })); 
        // Format Academic Degree 
        let academicDegreeData = academicDegree.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor,
        })); 

        return (
                <ExpansionPanel className={classes.root}>
                    <ExpansionPanelSummary 
                        className={classes.expansionPanel}
                        expandIcon={<ExpandMoreIcon fontSize="small" />}>
                        <Typography className={classes.heading}>
                            <AccountCircleIcon fontSize="small" className="mr-2"/> 
                            Información Personal
                        </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails >
                        <Grid container spacing={8}>
                            <Grid item xs={12}>
                                <PersonalInfoWrapper 
                                    idenfiticationDocumentType={idenfiticationDocumentType.data}
                                    gender={genderData}
                                    maritalStatus={maritalStatusData} 
                                    country={countryData}
                                    academicDegree={academicDegreeData}

                                    handleChangeSelectDocumentType={this.handleChangeSelectDocumentType}
                                    handleChangeTextFieldDocumentNumber={this.handleChangeTextFieldDocumentNumber}

                                    handleChangeTextFieldFirstName={this.handleChangeTextFieldOnlyTextRequired}
                                    handleChangeTextFieldSecondName={this.handleChangeTextFieldOnlyText}
                                    handleChangeTextFieldLastName={this.handleChangeTextFieldOnlyTextRequired}
                                    handleChangeTextFieldMotherLastName={this.handleChangeTextFieldOnlyText}

                                    handleChangeTextFieldBirthday={this.handleChangeTextFieldRequired}
                                    handleChangeSelectGender={this.handleChangeSelectRequired}
                                    handleChangeSelectMaritalStatus={this.handleChangeSelectFullRequired}
                                    handleChangeSelectNationality={this.handleChangeSelectRequired}

                                    handleChangeAcademicDegree ={this.handleChangeSelectRequired}
                                    handleChangeEmail={this.handleChangeTextFieldRequired}
                                    handleChangeCellphone={this.handleChangeTextFieldOnlyNumberRequired}
                                    handleChangeTelephone={this.handleChangeTextField}

                                    minBirthday = {this.minBirthday}
                                    maxBirthday = {this.maxBirthday}
                                    
                                    defaultLength={defaultLength}
                                    emailLength={emailLength}
                                    cellphoneLength={cellphoneLength}
                                    client={state.client} 
                                />
                        </Grid>
                    </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            )
        }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withSnackbar(PersonalInfoPanel)));

