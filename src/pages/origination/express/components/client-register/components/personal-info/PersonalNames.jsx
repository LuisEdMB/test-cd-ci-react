import React from 'react';
import {
    Grid,
    TextField, 
    InputAdornment 
}from '@material-ui/core';
//Icons 
import Filter1Icon from '@material-ui/icons/Filter1';
import Filter2Icon from '@material-ui/icons/Filter2';


const PersonalNames = ({   
            handleChangeTextFieldFirstName,
            handleChangeTextFieldSecondName,
            defaultLength,
            client,
            ...props
        }) => (
            <>
                {/* Client - First Name */}
                <Grid item xs={12} sm={6} md={6} lg={3}>
                    <TextField
                        error = {client.firstNameError}
                        fullWidth={true}
                        required
                        name="firstName"
                        label="Primer Nombre"
                        type="text"
                        margin="normal"
                        value={client.firstName}
                        onChange={handleChangeTextFieldFirstName}
                        InputProps={{
                            inputProps:{
                                maxLength: defaultLength,
                            },
                            endAdornment: (
                                <InputAdornment position="end">
                                    <Filter1Icon color={ client.firstNameError ? "secondary": "inherit"} fontSize="small"/>
                                </InputAdornment>
                            )
                        }}
                    />
                </Grid>
                {/* Client - Second Name */}
                <Grid item xs={12} sm={6} md={6} lg={3}>
                    <TextField
                        fullWidth={true}
                        name="secondName"
                        label="Segundo Nombre"
                        type="text"
                        margin="normal"
                        value={client.secondName}
                        onChange={handleChangeTextFieldSecondName}
                        InputProps={{
                            inputProps:{
                                maxLength: defaultLength,
                            },
                            endAdornment: (
                                <InputAdornment position="end">
                                    <Filter2Icon fontSize="small"/>
                                </InputAdornment>
                            )
                        }}
                    />
                </Grid>
            </>
        )

export default React.memo(PersonalNames);