import React from 'react';
// Components
import {
    Grid
}from '@material-ui/core';
// Custom Components
import PersonalNames from './PersonalNames';
import PersonalSurnames from './PersonalSurnames';

const PersonalFullName = ({   
            handleChangeTextFieldFirstName,
            handleChangeTextFieldSecondName,
            handleChangeTextFieldLastName,
            handleChangeTextFieldMotherLastName,
            defaultLength,
            client,
        }) => (
            <Grid container spacing={8}>
                <PersonalNames 
                    handleChangeTextFieldFirstName={handleChangeTextFieldFirstName}
                    handleChangeTextFieldSecondName={handleChangeTextFieldSecondName}
                    defaultLength={defaultLength}
                    client={client}
                />
                <PersonalSurnames 
                    handleChangeTextFieldLastName={handleChangeTextFieldLastName}
                    handleChangeTextFieldMotherLastName={handleChangeTextFieldMotherLastName}
                    defaultLength={defaultLength}
                    client={client}
                />
            </Grid>
        )



export default React.memo(PersonalFullName);