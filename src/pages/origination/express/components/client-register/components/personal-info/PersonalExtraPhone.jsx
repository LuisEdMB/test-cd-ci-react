import React from 'react';
// Components
import {
    Grid,
    TextField, 
    InputAdornment,
    FormHelperText
}from '@material-ui/core';
// Utils
import TextMaskTelephone from '../../../../../../../utils/TextMaskTelephone';
// Icons
import StayPrimaryPortraitIcon from '@material-ui/icons/StayPrimaryPortrait';
import PhoneIcon from '@material-ui/icons/Phone';

const PersonalExtraPhone = (props) => {
    let { 
        cellphoneLength = 15,
        handleChangeCellphone,
        handleChangeTelephone,
        client,
    } = props;

    return(
        <>   
            {/* Client - Cellphone */}
            <Grid item xs={6} sm={6} md={6} lg={3}>
                <TextField
                    error={client.cellphoneError}
                    fullWidth={true}
                    required
                    label="Celular"
                    type="text"
                    margin="normal"
                    color="default"
                    value={client.cellphone}
                    name="cellphone"
                    onChange={handleChangeCellphone}
                    InputProps={{
                        inputProps:{
                            maxLength: cellphoneLength,
                        },
                        endAdornment: (
                            <InputAdornment position="end">
                                <StayPrimaryPortraitIcon color={client.cellphoneError? "secondary": "inherit"} fontSize="small" />
                            </InputAdornment>
                        )
                    }}
                />
                <FormHelperText className="text-right">
                    {client.cellphone.toString().length}/9
                </FormHelperText>
            </Grid>
            {/* Client - Telephone */}
            <Grid item xs={6} sm={6} md={6} lg={3}>
                <TextField
                    fullWidth={true}
                    label="Teléfono Fijo"
                    type="text"
                    margin="normal"
                    color="default"
                    value={client.telephone}
                    name="telephone"
                    onChange={handleChangeTelephone}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <PhoneIcon color="inherit" fontSize="small"/>
                            </InputAdornment>
                        ), 
                        inputComponent: TextMaskTelephone,
                    }}
                />
                <FormHelperText>
                    (Código Región) + Número Telefónico
                </FormHelperText>
            </Grid>
        </>
    )
}


export default React.memo(PersonalExtraPhone);