import React from 'react';
// Components
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
//Icons 
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; 
var yyyy = today.getFullYear();
var date = `${yyyy}-${mm}-${dd}`;

const WorkLaborIncomeDate = ({
    client, 
    handleChangeTextFieldLaborIncomeDate, 
    max=date,
    mix
}) => (
    <>
        <TextField
            error={client.laborIncomeDateError}
            fullWidth={true}
            label="Fecha Ingreso Laboral"
            type="date"
            margin="normal"
            required
            format="DD-MM-YYYY"
            defaultValue={client.laborIncomeDate}
            name="laborIncomeDate"
            onChange={handleChangeTextFieldLaborIncomeDate}
            InputLabelProps={{
                shrink: true,
            }}
            InputProps={{

                endAdornment: (
                    <InputAdornment position="end">
                        <CalendarTodayIcon color={client.laborIncomeDateError? "secondary":"inherit"}  fontSize="small" />
                    </InputAdornment>
                )
            }}
        />
    </>   
)

export default WorkLaborIncomeDate;