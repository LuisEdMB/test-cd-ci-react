import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import FocusLock from 'react-focus-lock';
// Components
import Grid from '@material-ui/core/Grid';
import Autocomplete from '../../../../../../../components/Autocomplete';

const styles = theme => ({

});

const WorkBasicInfo = ({
    client,
    employmentSituation=[],
    jobTitle=[],
    economicActivity=[],
    handleChangeSelectEmploymentSituation,
    handleChangeSelectJobTitle, 
    handleChangeSelectEconomicActivity
}) => (
    <>
        <Grid item xs={12} sm={6} md={4} lg={3}>
            <FocusLock disabled={!client.employmentSituationFocus}>
                <div>     
                    <Autocomplete 
                        error={client.employmentSituationError}
                        onChange={(e) => handleChangeSelectEmploymentSituation(e, "employmentSituation")}
                        data={employmentSituation}
                        value={client.employmentSituationId}
                        placeholder={"Situación Laboral*"}
                    /> 
                </div>
            </FocusLock>
        </Grid>
        {/* Client - Job Title */}
        <Grid item xs={12} sm={6} md={4} lg={3}>
            <FocusLock disabled={!client.jobTitleFocus}>
                <div>
                    <Autocomplete 
                        error={client.jobTitleError}
                        disabled={client.jobTitleDisabled}
                        onChange={(e) => handleChangeSelectJobTitle(e, "jobTitle")}
                        data={jobTitle}
                        value={client.jobTitleId}
                        placeholder={"Cargo Profesión"}
                    />            
                </div>
            </FocusLock>
        </Grid>        
        {/* Client - Economic Activity */}
        <Grid item xs={12} sm={6} md={4} lg={3}>
            <FocusLock disabled={!client.economicActivityFocus}>
                <div>
                    <Autocomplete 
                        error={client.economicActivityError}
                        disabled={client.economicActivityDisabled}
                        onChange={(e) => handleChangeSelectEconomicActivity(e, "economicActivity")}
                        data={economicActivity}
                        value={client.economicActivityId}
                        placeholder={"Actividad Económica"}
                    />    
                </div>
            </FocusLock>
        </Grid>  
    </>
)

export default React.memo(withStyles(styles)(WorkBasicInfo));