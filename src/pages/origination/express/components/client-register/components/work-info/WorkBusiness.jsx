import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import FocusLock from 'react-focus-lock';
// Components
import Grid from '@material-ui/core/Grid';
import Autocomplete from '../../../../../../../components/Autocomplete';

const styles = theme => ({

});

const WorkBusiness = ({
    client, 
    businessOption=[],
    handleChangeSelectOptionBusiness
}) => (
    <>
        {/* Client Option Business */}
        <Grid item xs={12} sm={6} md={4} lg={3}>
            <FocusLock disabled={!client.businessOptionFocus}>
                <div>       
                    <Autocomplete 
                        error={client.businessOptionError}
                        onChange={(e) => handleChangeSelectOptionBusiness(e, "businessOption")}
                        data={businessOption}
                        value={client.businessOptionId}
                        placeholder={"Seleccionar RUC | Razón Social"}
                    />      
                </div>
            </FocusLock> 
        </Grid>
    </>
)

export default React.memo(withStyles(styles)(WorkBusiness));