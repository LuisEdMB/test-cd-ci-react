import React, { PureComponent } from 'react';
import classNames from 'classnames';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components
import Autocomplete from '../../../../../components/Autocomplete';
import RegisterModalButton from '../../../../origination/mantenimiento/empresas/components/register/RegisterModalButton'
import VerifyCellphone from "../../../../../components/VerifyCellphone"
import {
    AppBar,
    withStyles,
    Button,
    Checkbox,
    CircularProgress,
    ClickAwayListener,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary,
    FormControl,
    FormControlLabel,
    FormHelperText,
    FormLabel,
    Grid,
    Grow,
    IconButton,
    InputAdornment,
    InputLabel,
    MenuItem,
    MenuList,
    Paper,
    Popper,
    Radio,
    RadioGroup,
    Select,
    TextField,
    Tooltip,
    Typography,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    LinearProgress
} from '@material-ui/core';
// Colors  
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';
// Icons 
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import Filter1Icon from '@material-ui/icons/Filter1';
import Filter2Icon from '@material-ui/icons/Filter2';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import PlaceIcon from '@material-ui/icons/Place';
import StayPrimaryPortraitIcon from '@material-ui/icons/StayPrimaryPortrait';
import EmailIcon from '@material-ui/icons/MailOutline';
import PhoneIcon from '@material-ui/icons/Phone';
import MoneyIcon from '@material-ui/icons/Money';
import HomeIcon from '@material-ui/icons/Home';
import BusinessIcon from '@material-ui/icons/Business';
import LastPageIcon from '@material-ui/icons/LastPage';
import CropDinIcon from '@material-ui/icons/CropDin';
import TabUnselectedIcon from '@material-ui/icons/TabUnselected';
import NearMeIcon from '@material-ui/icons/NearMe';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import Filter9PlusIcon from '@material-ui/icons/Filter9Plus';
import DialpadIcon from '@material-ui/icons/Dialpad';
import CodeIcon from '@material-ui/icons/Code';
import WorkIcon from '@material-ui/icons/Work';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close';
import SendIcon from '@material-ui/icons/Send';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import HelpIcon from '@material-ui/icons/Help';
// Custom 
import ClientPreview from '../../../../../components/info-preview/ClientPreview';
// Actions
import { getAcademicDegree } from '../../../../../actions/value-list/academic-degree';
import { getAccountStatus } from '../../../../../actions/value-list/account-status';
import { getBusiness, getFilterByNameBusinessOrRuc } from '../../../../../actions/generic/business';
import { getCountry } from '../../../../../actions/value-list/country';
import { getDepartment } from '../../../../../actions/ubigeo/department';
import { getEconomicActivity } from '../../../../../actions/value-list/economic-activity';
import { getEmploymentSituation } from '../../../../../actions/value-list/employment-situation';
import { getGender } from '../../../../../actions/value-list/gender';
import { getHomeAddressDistrict, getWorkAddressDistrict } from '../../../../../actions/ubigeo/district';
import { getHomeAddressProvince, getWorkAddressProvince } from '../../../../../actions/ubigeo/province';
import { getHousingType } from '../../../../../actions/value-list/housing-type';
import { getIdentificationDocumentType } from '../../../../../actions/value-list/identification-document-type';
import { getJobTitle } from '../../../../../actions/value-list/job-title';
import { getMaritalStatus } from '../../../../../actions/value-list/marital-status';
import { getPaymentDate } from '../../../../../actions/value-list/payment-date';
import { getPromoter } from '../../../../../actions/generic/promoter';
import { getSendCorrespondence } from '../../../../../actions/value-list/send-correspondence';
import { getVia } from '../../../../../actions/value-list/via';
import { getZone } from '../../../../../actions/value-list/zone';
import { getConstantODC } from '../../../../../actions/generic/constant';
import * as ActionOdcMaster from '../../../../../actions/odc-master/odc-master'
import { sendSMSCode } from '../../../../../actions/generic/verifyCellphone'
// Utils 
import {
    checkEmailInputRegex,
    onlyTextKeyCode,
    onlyNumberKeyCode,
    checkInputKeyCode,
    defaultLengthInput,
    defaultCellphoneLengthInput,
    defaultViaLengthInput,
    defaultReferenceLengthInput,
    defaultEmailLengthInput,
    defaultComplementaryLengthInput,
    defaultTelephoneExtensionLengthInput,
    filterMaskTelephone,
    defaultvalCuotasLength,
    checkInputRegex,
    resolveStateRedux,
    handleEnterKeyPress,
    verifyValueIntoArray
} from '../../../../../utils/Utils';
import TextMaskTelephone from '../../../../../utils/TextMaskTelephone';
import NumberMaskMoney from '../../../../../utils/NumberMaskMoney';
import * as MasterOrigination from '../../../../../utils/origination/MasterOrigination'
import * as Constants from '../../../../../utils/Constants'
// Effects
import Zoom from 'react-reveal/Zoom';
import Bounce from 'react-reveal/Bounce';

const moment = extendMoment(Moment);

const styles = theme => ({
    root: {
        borderRadius: ".5em"
    },
    modalCancelRoot: {
        minWidth: 280,
        width: 350,
        maxWidth: 400,
    },
    heading: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        textTransform: "uppercase",
    },
    button: {
        textTransform: 'none',
    },
    wrapper: {
        position: 'relative',
    },
    cancelButtonProgress: {
        color: red[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    title: {
        fontSize: 12
    },
    saveClientButton: {
        backgroundColor: green[700],
        color: "white",
        transition: ".3s",
        '&:hover': {
            backgroundColor: green[800],
        },
    },
    menuItem: {
        zIndex: 900
    },
    panel: {
        [theme.breakpoints.down('sm')]: {
            minWidth: 270,
        },
        [theme.breakpoints.up('md')]: {
        },
        [theme.breakpoints.up('lg')]: {
        },
        minWidth: "100%",
    },
    appBar: {
        position: 'relative',
    },
    upload: {
        display: "none",
    },
    dropzone: {
        height: 90,
        border: "1px solid #bdbdbd",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        textTransform: "none",
        margin: ".5em 0",
        borderRadius: 5,
    },
    dropzoneHelperWrapper: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
    }
});

const mapStateToProps = (state) => {
    return {
        idenfiticationDocumentType: state.identificationDocumentTypeReducer,
        gender: state.genderReducer,
        maritalStatus: state.maritalStatusReducer,
        academicDegree: state.academicDegreeReducer,
        country: state.countryReducer,
        via: state.viaReducer,
        department: state.departmentReducer,
        provinceFilter: state.provinceReducer,
        districtFilter: state.districtReducer,
        zone: state.zoneReducer,
        employmentSituation: state.employmentSituationReducer,
        businessData: state.businessReducer,
        jobTitle: state.jobTitleReducer,
        paymentDate: state.paymentDateReducer,
        economicActivity: state.economicActivityReducer,
        housingType: state.housingTypeReducer,
        sendCorrespondence: state.sendCorrespondenceReducer,
        accountStatus: state.accountStatusReducer,
        promoter: state.promoterReducer,
        constantODC: state.constantODCReducer,
        odc: state.odcReducer,
        verifyCellphone: state.verifyCellphoneReducer,
        odcMaster: state.odcMasterReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const actions = {
        getIdentificationDocumentType: bindActionCreators(getIdentificationDocumentType, dispatch),
        getGender: bindActionCreators(getGender, dispatch),
        getMaritalStatus: bindActionCreators(getMaritalStatus, dispatch),
        getAcademicDegree: bindActionCreators(getAcademicDegree, dispatch),
        getCountry: bindActionCreators(getCountry, dispatch),
        getDepartment: bindActionCreators(getDepartment, dispatch),
        getHomeAddressProvince: bindActionCreators(getHomeAddressProvince, dispatch),
        getHomeAddressDistrict: bindActionCreators(getHomeAddressDistrict, dispatch),
        getWorkAddressProvince: bindActionCreators(getWorkAddressProvince, dispatch),
        getWorkAddressDistrict: bindActionCreators(getWorkAddressDistrict, dispatch),
        getVia: bindActionCreators(getVia, dispatch),
        getZone: bindActionCreators(getZone, dispatch),
        getEmploymentSituation: bindActionCreators(getEmploymentSituation, dispatch),
        getBusiness: bindActionCreators(getBusiness, dispatch),
        getFilterByNameBusinessOrRuc: bindActionCreators(getFilterByNameBusinessOrRuc, dispatch),
        getJobTitle: bindActionCreators(getJobTitle, dispatch),
        getPaymentDate: bindActionCreators(getPaymentDate, dispatch),
        getEconomicActivity: bindActionCreators(getEconomicActivity, dispatch),
        getHousingType: bindActionCreators(getHousingType, dispatch),
        getSendCorrespondence: bindActionCreators(getSendCorrespondence, dispatch),
        getAccountStatus: bindActionCreators(getAccountStatus, dispatch),
        getPromoter: bindActionCreators(getPromoter, dispatch),
        getConstantODC: bindActionCreators(getConstantODC, dispatch),
        preloadClientMaster: bindActionCreators(ActionOdcMaster.preloadClient, dispatch),
        sendSMSCode: bindActionCreators(sendSMSCode, dispatch)
    };
    return actions;
}

class ClientRegister extends PureComponent {
    constructor(props) {
        super(props);
        // Input - Personal Info
        this.firstNameInput = React.createRef();
        this.secondNameInput = React.createRef();
        this.lastNameInput = React.createRef();
        this.motherLastNameInput = React.createRef();
        this.emailInput = React.createRef();
        this.telephoneInput = React.createRef();
        this.cellphoneInput = React.createRef();
        // Input - Home Address
        this.homeAddressNameViaInput = React.createRef();
        this.homeAddressNumberInput = React.createRef();
        this.homeAddressBuildingInput = React.createRef();
        this.homeAddressInsideInput = React.createRef();
        this.homeAddressMzInput = React.createRef();
        this.homeAddressLotInput = React.createRef();
        this.homeAddressNameZoneInput = React.createRef();
        this.homeAddressReferenceInput = React.createRef();
        // Input - Work Address
        this.workAddressNameViaInput = React.createRef();
        this.workAddressNumberInput = React.createRef();
        this.workAddressBuildingInput = React.createRef();
        this.workAddressInsideInput = React.createRef();
        this.workAddressMzInput = React.createRef();
        this.workAddressLotInput = React.createRef();
        this.workAddressNameZoneInput = React.createRef();
        this.workAddressReferenceInput = React.createRef();
        // Work Info
        this.grossIncomeInput = React.createRef();
        this.workTelephoneInput = React.createRef();
        this.workTelephoneExtensionInput = React.createRef();
        // Extra
        this.scannerCodeInput = React.createRef();
        this.minimumForAlertsInput = React.createRef();
    }
    state = {

        client: {
            infoPreviewButtonDisabled: false,
            cancelButtonDisabled: false,

            documentNumberReadOnly: false,
            documentTypeReadOnly: false,
            birthdayReadOnly: false,

            documentTypeIdDisabled: true,
            genderDisabled: true,
            maritalStatusDisabled: true,
            nationalityDisabled: true,
            academicDegreeDisabled: true,

            documentNumberDisabled: true,
            documentNumberError: false,
            documentTypeError: false,

            firstNameError: false,
            lastNameError: false,
            birthdayError: false,
            genderError: false,
            maritalStatusError: false,
            nationalityError: false,
            academicDegreeError: false,
            emailError: false,
            cellphoneError: false,
            telephoneError: false,

            documentTypeId: 100001,
            documentType: "DNI",
            documentTypeAux: "1",
            documentTypeInternalValue: "DU",
            documentNumber: "",
            firstName: "",
            secondName: "",
            lastName: "",
            motherLastName: "",
            birthday: "",
            genderId: "",
            gender: "",
            genderInternalValue: "",
            nationalityId: "",
            nationality: "",
            maritalStatusInternalValue: "",
            maritalStatusId: "",
            maritalStatus: "",
            academicDegreeId: "",
            academicDegree: "",
            academicDegreeInternalValue: "",
            email: "",
            cellphone: "",
            cellphoneValidationSMS: {
                validate: false,
                confirmed: false,
                visiblePopup: false
            },
            telephone: "",
            homeAddress: {
                departmentError: false,
                provinceError: false,
                districtError: false,
                viaError: false,
                nameViaError: false,
                nameZoneError: false,
                housingTypeError: false,
                departmentLoading: false,
                provinceLoading: false,
                districtLoading: false,
                departmentDisabled: true,
                provinceDisabled: true,
                districtDisabled: true,
                nameZoneDisabled: true,

                id: 0,
                departmentId: "",
                department: "",
                provinceId: "",
                province: "",
                districtId: "",
                district: "",
                viaId: "",
                via: "",
                viaInteralValue: "",
                nameVia: "",
                number: "",
                building: "",
                inside: "",
                mz: "",
                lot: "",
                zoneId: "",
                zone: "",
                zoneInternalValue: "",
                nameZone: "",
                housingTypeId: "",
                housingType: "",
                housingTypeInternalValue: "",
                reference: "",
            },
            workAddress: {
                departmentError: false,
                provinceError: false,
                districtError: false,
                viaError: false,
                nameViaError: false,
                nameZoneError: false,
                housingTypeError: false,
                departmentLoading: false,
                provinceLoading: false,
                districtLoading: false,
                departmentDisabled: true,
                provinceDisabled: true,
                districtDisabled: true,
                nameZoneDisabled: true,

                id: 0,
                departmentId: "",
                department: "",
                provinceId: "",
                province: "",
                districtId: "",
                district: "",
                viaId: "",
                via: "",
                viaInteralValue: "",
                nameVia: "",
                number: "",
                building: "",
                inside: "",
                mz: "",
                lot: "",
                zoneId: "",
                zone: "",
                zoneInternalValue: "",
                nameZone: "",
                reference: "",
            },

            businessNameRequired: false,
            rucRequired: false,

            employmentSituationError: false,
            jobTitleError: false,
            economicActivityError: false,
            laborIncomeDateError: false,
            grossIncomeError: false,
            businessOptionError: false,
            businessNameError: false,
            rucError: false,
            rucErrorVal: false,
            bussNameError: false,
            workTelephoneError: false,
            jobTitleDisabled: true,
            economicActivityDisabled: true,
            businessNameDisabled: true,
            rucDisabled: true,

            employmentSituationId: "",
            employmentSituation: "",
            jobTitleId: "",
            jobTitle: "",
            economicActivityId: "",
            economicActivity: "",
            laborIncomeDate: "",
            businessOption: "",
            businessName: "",
            ruc: "",
            grossIncome: "",
            workTelephoneExtension: "",
            workTelephone: "",
            extra: {
                paymentDateError: false,
                sendCorrespondenceError: false,
                accoutStatusError: false,
                scannerCodeError: false,
                termError: false,
                promoterError: false,

                paymentDate: "",
                paymentDateInternalValue: "",
                paymentDateId: "",
                sendCorrespondence: "",
                sendCorrespondenceId: "",
                accountStatus: "",
                accountStatusId: "",
                promoterId: "",
                scannerCode: "",
                promoterName: "",
                term: false,

                internetConsumeAdvice: "0",
                internetConsumeAdviceError: false,
                foreignConsumeAdvice: "0",
                foreignConsumeAdviceError: false,
                effectiveDisposition: "0",
                effectiveDispositionError: false,
                overflipping: "0",
                overflippingError: false,
                mailAlerts: "1",
                mailAlertsError: false,
                minimumForAlerts: 0,
                minimumForAlertsError: false,
            }
        },
        open: false,
        anchorEl: null,

        openPreview: false,
        acceptModalButtonDisabled: false,
        acceptModalButtonLoading: false,
        cancelModalButtonDisabled: false,

        defaultMaxBirthday: "",
        defaultMinBirthday: "",

        maxBirthday: 0,
        minBirthday: 0,

        defaultMaxLaborIncomeDate: "",
        defaultMinLaborIncomeDate: "",

        maxLaborIncomeDate: 0,
        minLaborIncomeDate: 0,
        // Global
        disabled: true,
        loading: true,
        shrink: false
    }

    // Notistack
    getNotistack(message, variant = "default", duration = 6000) {
        let select = "default";
        switch (variant) {
            case "error":
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default:
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit" />
                </IconButton>
            ),
        });
    }

    componentWillMount = () => {
        let { client } = this.props;

        if (client.documentNumber) {
            let birthday = client.birthday ? client.birthday.split("T")[0] : "";
            let documentTypeReadOnly = false;
            let documentNumberReadOnly = false;

            if (client.documentTypeId !== "" && client.documentNumber !== "") {
                documentNumberReadOnly = true;
            }
            this.setState(state => ({
                ...state,
                client: {
                    ...state.client,
                    documentTypeId: client.documentTypeId,
                    documentNumber: client.documentNumber,
                    birthday: birthday,
                    documentTypeReadOnly: documentTypeReadOnly,
                    documentNumberReadOnly: documentNumberReadOnly,
                    cellphoneValidationSMS: {
                        ...state.client.cellphoneValidationSMS,
                        validate: this.props.isSAEProcess && !this.props.callCenter
                    }
                }
            }));
        }
    }

    componentDidMount() {
        // Get API's -  Sever ODC
        const { origination, isSAEProcess } = this.props
        const { client } = this.state
        Promise.all([
            this.props.getIdentificationDocumentType(),
            this.props.getGender(),
            this.props.getMaritalStatus(),
            this.props.getCountry(),
            this.props.getAcademicDegree(),
            this.props.getVia(),
            this.props.getZone(),
            this.props.getHousingType(),
            this.props.getEmploymentSituation(),
            this.props.getJobTitle(),
            this.props.getEconomicActivity(),
            this.props.getPaymentDate(),
            this.props.getSendCorrespondence(),
            this.props.getAccountStatus(),
            this.props.getConstantODC(),
            this.props.getDepartment({ type: 'DE', value: '' })
        ])
            .then(async result => {
                if (result) {
                    const sendDataPreloadClient = {
                        solicitudeCode: origination.number,
                        completeSolicitudeCode: origination.fullNumber,
                        documentType: client.documentTypeAux,
                        documentNumber: client.documentNumber,
                        activityName: MasterOrigination.preloadClientActivity.activityName,
                        phaseCode: MasterOrigination.preloadClientActivity.phaseCode,
                        masterStageCode: isSAEProcess
                            ? Constants.MasterConfigurationStage.masterCECStage
                            : Constants.MasterConfigurationStage.masterExpressStage,
                        clientTypeCode: Constants.ClientTypes.holderClientCode
                    }
                    const response = await this.props.preloadClientMaster(sendDataPreloadClient)
                    if (response?.success) {
                        const { precargaCliente } = response || { }
                        const homeAddress = precargaCliente.list_tt_ori_cliente_direccionEntity?.find(t => t.cod_tipo_direccion === 260001) || { }
                        const workAddress = precargaCliente.list_tt_ori_cliente_direccionEntity?.find(t => t.cod_tipo_direccion === 260002) || { }
                        this.setState(state => ({
                            ...state,
                            client: {
                                ...state.client,
                                id: precargaCliente.cod_cliente || 0,
                                documentTypeAux: precargaCliente.cod_tipo_documento || '',
                                documentTypeId: precargaCliente.cod_tipo_documento_2 || '',
                                documentType: precargaCliente.des_tipo_documento || '',
                                documentTypeInternalValue: precargaCliente.des_tipo_documento_valor_interno || '',
                                documentNumber: precargaCliente.des_nro_documento || '',
                                firstName: precargaCliente.des_primer_nom || '',
                                secondName: precargaCliente.des_segundo_nom || '',
                                lastName: precargaCliente.des_ape_paterno || '',
                                motherLastName: precargaCliente.des_ape_materno || '',
                                birthday: precargaCliente.fec_nacimiento?.split('T')[0] || '1900-01-01',
                                genderId: verifyValueIntoArray(this.props.gender.data, 'cod_valor', precargaCliente.cod_genero || 0),
                                gender: precargaCliente.des_genero || '',
                                genderInternalValue: precargaCliente.des_genero_valor_interno || '',
                                maritalStatusId: verifyValueIntoArray(this.props.maritalStatus.data, 'cod_valor', precargaCliente.cod_estado_civil || 0),
                                maritalStatus: precargaCliente.des_estado_civil || '',
                                maritalStatusInternalValue: precargaCliente.des_estado_civil_valor_interno || '',
                                nationalityId: verifyValueIntoArray(this.props.country.data, 'cod_valor', precargaCliente.cod_nacionalidad || 0),
                                nationality: precargaCliente.des_nacionalidad || '',
                                academicDegreeId: verifyValueIntoArray(this.props.academicDegree.data, 'cod_valor', precargaCliente.cod_gradoacademico || 0),
                                academicDegree: precargaCliente.des_gradoacademico || '',
                                academicDegreeInternalValue: precargaCliente.des_gradoacademico_valor_interno ||'',
                                email: precargaCliente.des_correo || '',
                                cellphone: precargaCliente.des_telef_celular || '',
                                telephone: precargaCliente.des_telef_fijo || '',
                                employmentSituationId: verifyValueIntoArray(this.props.employmentSituation.data, 'cod_valor', precargaCliente.cod_situacion_laboral || 0),
                                employmentSituation: precargaCliente.des_situacion_laboral || '',
                                jobTitleId: verifyValueIntoArray(this.props.jobTitle.data, 'cod_valor', precargaCliente.cod_cargo_profesion || 0),
                                jobTitle: precargaCliente.des_cargo_profesion || '',
                                economicActivityId: verifyValueIntoArray(this.props.economicActivity.data, 'cod_valor', precargaCliente.cod_actividad_economica || 0),
                                economicActivity: precargaCliente.des_actividad_economica || '',
                                laborIncomeDate: precargaCliente.fec_ingreso_laboral?.split("T")[0] || moment().format('YYYY-MM-DD'),
                                ruc: precargaCliente.des_ruc || '',
                                businessName: precargaCliente.des_razon_social || '',
                                businessOption: precargaCliente.des_ruc ? '0' : (precargaCliente.des_razon_social ? '1' : ''),
                                grossIncomeCall: precargaCliente.num_ingreso_bruto || '',
                                workTelephone: precargaCliente.des_telefono_laboral || '',
                                workTelephoneExtension: precargaCliente.des_anexo_laboral || '',
                                extra: {
                                    ...state.client.extra,
                                    term: precargaCliente.flg_autoriza_datos_per
                                },
                                homeAddress: {
                                    id: homeAddress.cod_direccion || 0,
                                    departmentId: homeAddress.cod_ubi_departamento || '',
                                    department: homeAddress.des_ubi_departamento || '',
                                    provinceId: homeAddress.cod_ubi_provincia || '',
                                    province: homeAddress.des_ubi_provicia || '',
                                    districtId: homeAddress.cod_ubi_distrito || '',
                                    district: homeAddress.des_ubi_distrito || '',
                                    viaId: verifyValueIntoArray(this.props.via.data, 'cod_valor', homeAddress.cod_via || 0),
                                    via: homeAddress.des_via || '',
                                    viaInternalValue: homeAddress.des_via_valor_interno || '',
                                    nameVia: homeAddress.des_nombre_via || '',
                                    number: homeAddress.des_nro || '',
                                    building: homeAddress.des_departamento || '',
                                    inside: homeAddress.des_interior || '',
                                    mz: homeAddress.des_manzana || '',
                                    lot: homeAddress.des_lote || '',
                                    zoneId: verifyValueIntoArray(this.props.zone.data, 'cod_valor', homeAddress.cod_zona || 0),
                                    zone: homeAddress.des_zona_valor_interno || '',
                                    nameZoneDisabled: !homeAddress.cod_zona,
                                    zoneInternalValue: homeAddress.des_zona_valor_interno || '',
                                    nameZone: homeAddress.des_zona  || '',
                                    housingTypeId: verifyValueIntoArray(this.props.housingType.data, 'cod_valor', homeAddress.cod_tipo_vivienda || 0),
                                    housingType: homeAddress.des_tipo_vivienda || '',
                                    housingTypeInternalValue: homeAddress.des_tipo_vivienda_valor_interno || '',
                                    reference: homeAddress.des_referencia_domicilio || ''
                                },
                                workAddress: {
                                    id: workAddress.cod_direccion || 0,
                                    departmentId: workAddress.cod_ubi_departamento || '',
                                    department: workAddress.des_ubi_departamento || '',
                                    provinceId: workAddress.cod_ubi_provincia || '',
                                    province: workAddress.des_ubi_provicia || '',
                                    districtId: workAddress.cod_ubi_distrito || '',
                                    district: workAddress.des_ubi_distrito || '',
                                    viaId: verifyValueIntoArray(this.props.via.data, 'cod_valor', workAddress.cod_via || 0),
                                    via: workAddress.des_via || '',
                                    viaInternalValue: workAddress.des_via_valor_interno || '',
                                    nameVia: workAddress.des_nombre_via || '',
                                    number: workAddress.des_nro || '',
                                    building: workAddress.des_departamento || '',
                                    inside: workAddress.des_interior || '',
                                    mz: workAddress.des_manzana || '',
                                    lot: workAddress.des_lote || '',
                                    zoneId: verifyValueIntoArray(this.props.zone.data, 'cod_valor', workAddress.cod_zona || 0),
                                    zone: workAddress.des_zona_valor_interno || '',
                                    nameZoneDisabled: !workAddress.cod_zona,
                                    zoneInternalValue: workAddress.des_zona_valor_interno || '',
                                    nameZone: workAddress.des_zona || '',
                                    reference: workAddress.des_referencia_domicilio || ''
                                }
                            }
                        }))

                        if (homeAddress.cod_ubi_departamento)
                            this.props.getHomeAddressProvince({ type: 'PR', value: homeAddress.cod_ubi_departamento || '' })
                        if (homeAddress.cod_ubi_provincia)
                            this.props.getHomeAddressDistrict({ type: 'DI', value: homeAddress.cod_ubi_provincia || '' })
                        
                        if (workAddress.cod_ubi_departamento)
                            this.props.getWorkAddressProvince({ type: 'PR', value: workAddress.cod_ubi_departamento || '' })
                        if (workAddress.cod_ubi_provincia)
                            this.props.getWorkAddressDistrict({ type: 'DI', value: workAddress.cod_ubi_provincia || '' })
                    }
                    this.setState(state => ({
                        ...state,
                        loading: false,
                        shrink: true,
                        client: {
                            ...state.client,
                            cellphoneValidationSMS: {
                                ...state.client.cellphoneValidationSMS,
                                visiblePopup: true
                            }
                        }
                    }))
                }
            })
    }

    componentDidUpdate(prevProps) {
        if (prevProps.constantODC !== this.props.constantODC) {
            // Gender - Success Service
            if (!this.props.constantODC.loading &&
                this.props.constantODC.response &&
                this.props.constantODC.success) {

                const minAge = this.props.constantODC.data.find(item => item.des_abv_constante === "CDA_EDAD_MINIMA_CLIENTE_TITULAR");
                const maxAge = this.props.constantODC.data.find(item => item.des_abv_constante === "CDA_EDAD_MAXIMA_CLIENTE_TITULAR");

                const minWorkingYears = this.props.constantODC.data.find(item => item.des_abv_constante === "CDA_ANO_LABORAL_MINIMA");
                const maxWorkingYears = this.props.constantODC.data.find(item => item.des_abv_constante === "CDA_ANO_LABORAL_MAXIMA");

                const defaultMinBirthday = moment().subtract(minAge.valor_numerico, "years").format('YYYY-MM-DD');
                const defaultMaxBirthday = moment().subtract(maxAge.valor_numerico, "years").format('YYYY-MM-DD');

                const defaultMinLaborIncomeDate = moment().subtract(minWorkingYears.valor_numerico, "years").format('YYYY-MM-DD');
                const defaultMaxLaborIncomeDate = moment().subtract(maxWorkingYears.valor_numerico, "years").format('YYYY-MM-DD');

                this.setState(state => ({
                    ...state,
                    client: {
                        ...state.client
                    },
                    defaultMinBirthday: defaultMinBirthday,
                    defaultMaxBirthday: defaultMaxBirthday,
                    minBirthday: minAge.valor_numerico,
                    maxBirthday: maxAge.valor_numerico,

                    defaultMaxLaborIncomeDate: defaultMaxLaborIncomeDate,
                    defaultMinLaborIncomeDate: defaultMinLaborIncomeDate,
                    minLaborIncomeDate: minWorkingYears.valor_numerico,
                    maxLaborIncomeDate: maxWorkingYears.valor_numerico
                }));
            }
            // Gender - Error Service
            else if (!this.props.constantODC.loading &&
                this.props.constantODC.response &&
                !this.props.constantODC.success) {
                this.getNotistack(`Constante ODC: ${this.props.gender.error}`, "error");
            }
            // Gender - Error Service Connectivity
            else if (!this.props.constantODC.loading &&
                !this.props.constantODC.response &&
                !this.props.constantODC.success) {
                this.getNotistack(`Constante ODC: ${this.props.gender.error}`, "error");
            }
        }
        // Identification Document Type - Before
        if (prevProps.idenfiticationDocumentType !== this.props.idenfiticationDocumentType) {
            // Identification Document Type - Success Service
            if (!this.props.idenfiticationDocumentType.loading &&
                this.props.idenfiticationDocumentType.response &&
                this.props.idenfiticationDocumentType.success) {

                const idenfiticationDocumentType = this.props.idenfiticationDocumentType.data.find(item => item.val_orden === 1);
                this.setState(state => ({
                    ...state,
                    client: {
                        ...state.client,
                        documentTypeId: idenfiticationDocumentType.cod_valor,
                        documentType: idenfiticationDocumentType.des_valor,
                        documentTypeAux: idenfiticationDocumentType.des_auxiliar,
                        documentTypeInternalValue: idenfiticationDocumentType.valor_interno,
                        documentTypeIdDisabled: false,
                        documentNumberDisabled: false
                    }
                }));
            }
            // Identification Document Type - Error Service
            if (!this.props.idenfiticationDocumentType.loading &&
                this.props.idenfiticationDocumentType.response &&
                !this.props.idenfiticationDocumentType.success) {

                this.getNotistack(`Tipo Documento: ${this.props.idenfiticationDocumentType.error}`, "error");
            }
            // Identification Document Type - Error Service Connectivity
            else if (!this.props.idenfiticationDocumentType.loading &&
                !this.props.idenfiticationDocumentType.response &&
                !this.props.idenfiticationDocumentType.success) {
                this.getNotistack(`Tipo Documento: ${this.props.idenfiticationDocumentType.error}`, "error");
            }
        }
        // Gender - Before
        if (prevProps.gender !== this.props.gender) {
            // Gender - Success Service
            if (!this.props.gender.loading &&
                this.props.gender.response &&
                this.props.gender.success) {
            }
            // Gender - Error Service
            if (!this.props.gender.loading &&
                this.props.gender.response &&
                !this.props.gender.success) {
                this.getNotistack(`Género: ${this.props.gender.error}`, "error");
            }
            // Gender - Error Service Connectivity
            else if (!this.props.gender.loading &&
                !this.props.gender.response &&
                !this.props.gender.success) {
                this.getNotistack(`Género: ${this.props.gender.error}`, "error");
            }
        }
        // Marital Status - Before
        if (prevProps.maritalStatus !== this.props.maritalStatus) {
            // Marital Status - Error Service
            if (!this.props.maritalStatus.loading &&
                this.props.maritalStatus.response &&
                this.props.maritalStatus.success) {
            }
            // Marital Status - Error Service
            if (!this.props.maritalStatus.loading &&
                this.props.maritalStatus.response &&
                !this.props.maritalStatus.success) {
                this.getNotistack(`Estado Civil: ${this.props.maritalStatus.error}`, "error");
            }
            // Marital Status - Error Service Connectivity
            else if (!this.props.maritalStatus.loading &&
                !this.props.maritalStatus.response &&
                !this.props.maritalStatus.success) {
                this.getNotistack(`Estado Civil: ${this.props.maritalStatus.error}`, "error");
            }
        }
        // Country - Before
        if (prevProps.country !== this.props.country) {
            // Country - Error Service
            if (!this.props.country.loading &&
                this.props.country.response &&
                this.props.country.success) {
            }
            // Country - Error Service
            if (!this.props.country.loading &&
                this.props.country.response &&
                !this.props.country.success) {

                this.getNotistack(`Nacionalidad: ${this.props.country.error}`, "error");
                this.setState({
                    ...this.state,
                    nationalityDisabled: true
                });
            }
            // Country - Error Service Connectivity
            else if (!this.props.country.loading &&
                !this.props.country.response &&
                !this.props.country.success) {
                this.getNotistack(`Nacionalidad: ${this.props.country.error}`, "error");
                this.setState({
                    ...this.state,
                    nationalityDisabled: true
                });
            }
        }
        // Academic Degree - Before
        if (prevProps.academicDegree !== this.props.academicDegree) {
            // Academic Degree - Error Service
            if (!this.props.academicDegree.loading &&
                this.props.academicDegree.response &&
                !this.props.academicDegree.success) {

                this.getNotistack(`Grado Académico: ${this.props.academicDegree.error}`, "error");
            }
            // Academic Degree - Error Service Connectivity
            else if (!this.props.academicDegree.loading &&
                !this.props.academicDegree.response &&
                !this.props.academicDegree.success) {
                this.getNotistack(`Grado Académico: ${this.props.academicDegree.error}`, "error");
            }
        }
        // Department - Before
        if (prevProps.department !== this.props.department) {
            if (this.props.department.loading) {
                this.setState(state => ({
                    ...state,
                    client: {
                        ...state.client,
                        homeAddress: {
                            ...state.client.homeAddress,
                            departmentDisabled: true,
                            departmentLoading: true
                        },
                        workAddress: {
                            ...state.client.workAddress,
                            departmentDisabled: true,
                            departmentLoading: true
                        }
                    }
                }));
            }
            if (!this.props.department.loading &&
                this.props.department.response &&
                this.props.department.success) {
                this.setState(state => ({
                    ...state,
                    client: {
                        ...state.client,
                        homeAddress: {
                            ...state.client.homeAddress,
                            departmentDisabled: false,
                            departmentLoading: false
                        },
                        workAddress: {
                            ...state.client.workAddress,
                            departmentDisabled: false,
                            departmentLoading: false
                        },
                    }
                }));
            }
            // Department - Error Service
            else if (!this.props.department.loading &&
                this.props.department.response &&
                !this.props.department.success) {
                this.getNotistack(`Departamento: ${this.props.department.error}`, "error");
                this.setState(state => ({
                    ...state,
                    client: {
                        ...state.client,
                        homeAddress: {
                            ...state.client.homeAddress,
                            departmentDisabled: true,
                            departmentLoading: false
                        },
                        workAddress: {
                            ...state.client.workAddress,
                            departmentDisabled: true,
                            departmentLoading: false
                        },
                    }
                }));
            }
            // Department- Error Service Connectivity
            else if (!this.props.department.loading &&
                !this.props.department.response &&
                !this.props.department.success) {
                this.getNotistack(`Departamento: ${this.props.department.error}`, "error");
                this.setState(state => ({
                    ...state,
                    client: {
                        ...state.client,
                        homeAddress: {
                            ...state.client.homeAddress,
                            departmentDisabled: true,
                            departmentLoading: false
                        },
                        workAddress: {
                            ...state.client.workAddress,
                            departmentDisabled: true,
                            departmentLoading: false
                        },
                    }
                }));
            }
        }
        // Province - Home - Before
        if (prevProps.provinceFilter !== this.props.provinceFilter) {
            // Province - Home - Before
            if (prevProps.provinceFilter.homeAddressProvince !== this.props.provinceFilter.homeAddressProvince) {
                // Home Address Province - Success Loading
                if (this.props.provinceFilter.homeAddressProvince.loading) {
                    this.setState(state => ({
                        client: {
                            ...state.client,
                            homeAddress: {
                                ...state.client.homeAddress,
                                provinceDisabled: true,
                                provinceLoading: true
                            }
                        }
                    }));
                }
                // Home Address Province - Success Service
                if (!this.props.provinceFilter.homeAddressProvince.loading &&
                    this.props.provinceFilter.homeAddressProvince.response &&
                    this.props.provinceFilter.homeAddressProvince.success) {
                    this.setState(state => ({
                        client: {
                            ...state.client,
                            homeAddress: {
                                ...state.client.homeAddress,
                                provinceDisabled: false,
                                provinceLoading: false
                            }
                        }
                    }));
                }
                // Home Address Province - Error Service
                else if (!this.props.provinceFilter.homeAddressProvince.loading &&
                    this.props.provinceFilter.homeAddressProvince.response &&
                    !this.props.provinceFilter.homeAddressProvince.success) {
                    if (this.props.provinceFilter.homeAddressProvince.error) {
                        this.getNotistack(`Dir. Personal - Provincia: ${this.props.provinceFilter.homeAddressProvince.error}`, "error");
                        this.setState(state => ({
                            client: {
                                ...state.client,
                                homeAddress: {
                                    ...state.client.homeAddress,
                                    provinceDisabled: true,
                                    provinceLoading: false
                                }
                            }
                        }));
                    }
                }
                // Home Address Province - Error Service Connectivity
                else if (!this.props.provinceFilter.homeAddressProvince.loading &&
                    !this.props.provinceFilter.homeAddressProvince.response &&
                    !this.props.provinceFilter.homeAddressProvince.success) {
                    if (this.props.provinceFilter.homeAddressProvince.error) {
                        this.getNotistack(`Dir. Personal - Provincia: ${this.props.provinceFilter.homeAddressProvince.error}`, "error");
                        this.setState(state => ({
                            client: {
                                ...state.client,
                                homeAddress: {
                                    ...state.client.homeAddress,
                                    provinceDisabled: true,
                                    provinceLoading: false
                                }
                            }
                        }));
                    }
                }
            }
            // Province - Work - Before
            if (prevProps.provinceFilter.workAddressProvince !== this.props.provinceFilter.workAddressProvince) {
                // Home Address Province - Success Loading
                if (this.props.provinceFilter.workAddressProvince.loading) {
                    this.setState(state => ({
                        client: {
                            ...state.client,
                            workAddress: {
                                ...state.client.workAddress,
                                provinceDisabled: true,
                                provinceLoading: true
                            }
                        }
                    }));
                }
                // Home Address Province - Success Service
                if (!this.props.provinceFilter.workAddressProvince.loading &&
                    this.props.provinceFilter.workAddressProvince.response &&
                    this.props.provinceFilter.workAddressProvince.success) {
                    this.setState(state => ({
                        client: {
                            ...state.client,
                            workAddress: {
                                ...state.client.workAddress,
                                provinceDisabled: false,
                                provinceLoading: false
                            }
                        }
                    }));
                }
                // Home Address Province - Error Service
                else if (!this.props.provinceFilter.workAddressProvince.loading &&
                    this.props.provinceFilter.workAddressProvince.response &&
                    !this.props.provinceFilter.workAddressProvince.success) {
                    if (this.props.provinceFilter.workAddressProvince.error) {
                        this.getNotistack(`Dir. Personal - Provincia: ${this.props.provinceFilter.workAddressProvince.error}`, "error");
                        this.setState(state => ({
                            client: {
                                ...state.client,
                                workAddress: {
                                    ...state.client.workAddress,
                                    provinceDisabled: false,
                                    provinceLoading: false
                                }
                            }
                        }));
                    }
                }
                // Home Address Province - Error Service Connectivity
                else if (!this.props.provinceFilter.workAddressProvince.loading &&
                    !this.props.provinceFilter.workAddressProvince.response &&
                    !this.props.provinceFilter.workAddressProvince.success) {
                    if (this.props.provinceFilter.workAddressProvince.error) {
                        this.getNotistack(`Dir. Personal - Provincia: ${this.props.provinceFilter.workAddressProvince.error}`, "error");
                        this.setState(state => ({
                            client: {
                                ...state.client,
                                workAddress: {
                                    ...state.client.workAddress,
                                    provinceDisabled: false,
                                    provinceLoading: false
                                }
                            }
                        }));
                    }
                }
            }
        }
        // District - Home - Before
        if (prevProps.districtFilter !== this.props.districtFilter) {
            // District - Home - Before
            if (prevProps.districtFilter.homeAddressDistrict !== this.props.districtFilter.homeAddressDistrict) {
                if (this.props.districtFilter.homeAddressDistrict.loading) {
                    this.setState(state => ({
                        client: {
                            ...state.client,
                            homeAddress: {
                                ...state.client.homeAddress,
                                districtDisabled: true,
                                districtLoading: true
                            }
                        }
                    }));
                }

                if (!this.props.districtFilter.homeAddressDistrict.loading &&
                    this.props.districtFilter.homeAddressDistrict.response &&
                    this.props.districtFilter.homeAddressDistrict.success) {
                    this.setState(state => ({
                        client: {
                            ...state.client,
                            homeAddress: {
                                ...state.client.homeAddress,
                                districtDisabled: false,
                                districtLoading: false
                            }
                        }
                    }));
                }
                // Home Address District - Error Service
                else if (!this.props.districtFilter.homeAddressDistrict.loading &&
                    this.props.districtFilter.homeAddressDistrict.response &&
                    !this.props.districtFilter.homeAddressDistrict.success) {
                    if (this.props.districtFilter.homeAddressDistrict.error) {
                        this.getNotistack(`Dir. Personal - Distrito: ${this.props.districtFilter.homeAddressDistrict.error}`, "error");
                        this.setState(state => ({
                            client: {
                                ...state.client,
                                homeAddress: {
                                    ...state.client.homeAddress,
                                    districtDisabled: true,
                                    districtLoading: false
                                }
                            }
                        }));
                    }
                }
                // Home Address District - Error Service Connectivity
                else if (!this.props.districtFilter.homeAddressDistrict.loading &&
                    !this.props.districtFilter.homeAddressDistrict.response &&
                    !this.props.districtFilter.homeAddressDistrict.success) {
                    if (this.props.districtFilter.homeAddressDistrict.error) {
                        this.getNotistack(`Dir. Personal - Distrito: ${this.props.districtFilter.homeAddressDistrict.error}`, "error");
                        this.setState(state => ({
                            client: {
                                ...state.client,
                                homeAddress: {
                                    ...state.client.homeAddress,
                                    districtDisabled: true,
                                    districtLoading: false
                                }
                            }
                        }));
                    }
                }
            }
            // District - Work - Before
            if (prevProps.districtFilter.workAddressDistrict !== this.props.districtFilter.workAddressDistrict) {
                if (this.props.districtFilter.workAddressDistrict.loading) {
                    this.setState(state => ({
                        client: {
                            ...state.client,
                            workAddress: {
                                ...state.client.workAddress,
                                districtDisabled: true,
                                districtLoading: true
                            }
                        }
                    }));
                }

                if (!this.props.districtFilter.workAddressDistrict.loading &&
                    this.props.districtFilter.workAddressDistrict.response &&
                    this.props.districtFilter.workAddressDistrict.success) {
                    this.setState(state => ({
                        client: {
                            ...state.client,
                            workAddress: {
                                ...state.client.workAddress,
                                districtDisabled: false,
                                districtLoading: false
                            }
                        }
                    }));
                }
                // Home Address District - Error Service
                else if (!this.props.districtFilter.workAddressDistrict.loading &&
                    this.props.districtFilter.workAddressDistrict.response &&
                    !this.props.districtFilter.workAddressDistrict.success) {
                    if (this.props.districtFilter.workAddressDistrict.error) {
                        this.getNotistack(`Dir. Personal - Distrito: ${this.props.districtFilter.workAddressDistrict.error}`, "error");
                        this.setState(state => ({
                            client: {
                                ...state.client,
                                workAddress: {
                                    ...state.client.workAddress,
                                    districtDisabled: true,
                                    districtLoading: false
                                }
                            }
                        }));
                    }
                }
                // Home Address District - Error Service Connectivity
                else if (!this.props.districtFilter.workAddressDistrict.loading &&
                    !this.props.districtFilter.workAddressDistrict.response &&
                    !this.props.districtFilter.workAddressDistrict.success) {
                    if (this.props.districtFilter.workAddressDistrict.error) {
                        this.getNotistack(`Dir. Personal - Distrito: ${this.props.districtFilter.workAddressDistrict.error}`, "error");
                        this.setState(state => ({
                            client: {
                                ...state.client,
                                workAddress: {
                                    ...state.client.workAddress,
                                    districtDisabled: true,
                                    districtLoading: false
                                }
                            }
                        }));
                    }
                }
            }
        }
        // Via - Before
        if (prevProps.via !== this.props.via) {
            // Via - Error Service
            if (!this.props.via.loading &&
                this.props.via.response &&
                !this.props.via.success) {

                this.getNotistack(`Vía: ${this.props.via.error}`, "error");
            }
            // Via - Error Service Connectivity
            else if (!this.props.via.loading &&
                !this.props.via.response &&
                !this.props.via.success) {
                this.getNotistack(`Vía: ${this.props.via.error}`, "error");
            }
        }
        // Zone - Before
        if (prevProps.zone !== this.props.zone) {
            // Zone - Error Service
            if (!this.props.zone.loading &&
                this.props.zone.response &&
                !this.props.zone.success) {

                this.getNotistack(`Zona: ${this.props.zone.error}`, "error");
            }
            // Zone - Error Service Connectivity
            else if (!this.props.zone.loading &&
                !this.props.zone.response &&
                !this.props.zone.success) {
                this.getNotistack(`Zona: ${this.props.zone.error}`, "error");
            }
        }
        // Housing Type - Before
        if (prevProps.housingType !== this.props.housingType) {
            // Housing Type - Error Service
            if (!this.props.housingType.loading &&
                this.props.housingType.response &&
                !this.props.housingType.success) {
                this.getNotistack(`Tipo Vivienda: ${this.props.housingType.error}`, "error");
            }
            // Housing Type - Error Service Connectivity
            else if (!this.props.housingType.loading &&
                !this.props.housingType.response &&
                !this.props.housingType.success) {
                this.getNotistack(`Tipo Vivienda: ${this.props.housingType.error}`, "error");
            }
        }
        // Employment Situation - Before
        if (prevProps.employmentSituation !== this.props.employmentSituation) {
            // Employment Situation - Error Service
            if (!this.props.employmentSituation.loading &&
                this.props.employmentSituation.response &&
                !this.props.employmentSituation.success) {
                this.getNotistack(`Situación Laboral: ${this.props.employmentSituation.error}`, "error");
            }
            // Employment Situation - Error Service Connectivity
            else if (!this.props.employmentSituation.loading &&
                !this.props.employmentSituation.response &&
                !this.props.employmentSituation.success) {
                this.getNotistack(`Situación Laboral: ${this.props.employmentSituation.error}`, "error");
            }
        }
        // Job Title - Before
        if (prevProps.jobTitle !== this.props.jobTitle) {
            // Job Title - Error Service
            if (!this.props.jobTitle.loading &&
                this.props.jobTitle.response &&
                !this.props.jobTitle.success) {

                this.getNotistack(`Cargo - Profesión: ${this.props.jobTitle.error}`, "error");
            }
            // Job Title - Error Service Connectivity
            else if (!this.props.jobTitle.loading &&
                !this.props.jobTitle.response &&
                !this.props.jobTitle.success) {
                this.getNotistack(`Cargo - Profesión: ${this.props.jobTitle.error}`, "error");
            }
        }
        // Economic Activity - Before
        if (prevProps.economicActivity !== this.props.economicActivity) {
            // Economic Activity - Error Service
            if (!this.props.economicActivity.loading &&
                this.props.economicActivity.response &&
                !this.props.economicActivity.success) {

                this.getNotistack(`Actividad Económica: ${this.props.economicActivity.error}`, "error");
            }
            // Economic Activity - Error Service Connectivity
            else if (!this.props.economicActivity.loading &&
                !this.props.economicActivity.response &&
                !this.props.economicActivity.success) {
                this.getNotistack(`Actividad Económica: ${this.props.economicActivity.error}`, "error");
            }
        }
        // Payment Date - Before
        if (prevProps.paymentDate !== this.props.paymentDate) {
            // Payment Date - Error Service
            if (!this.props.paymentDate.loading &&
                this.props.paymentDate.response &&
                !this.props.paymentDate.success) {

                this.getNotistack(`Fecha de Pago: ${this.props.paymentDate.error}`, "error");
            }
            // Payment Date - Error Service Connectivity
            else if (!this.props.paymentDate.loading &&
                !this.props.paymentDate.response &&
                !this.props.paymentDate.success) {
                this.getNotistack(`Fecha de Pago: ${this.props.paymentDate.error}`, "error");
            }
        }
        // Business - Before
        if (prevProps.businessData !== this.props.businessData) {
            // Business - Success Service
            if (!this.props.businessData.filteredBusiness.loading &&
                this.props.businessData.filteredBusiness.response &&
                this.props.businessData.filteredBusiness.success) {
                this.setState(state => ({ ...state, open: true }));
            }
            // Business - Error Service Connectivity
            else if (!this.props.businessData.filteredBusiness.loading &&
                !this.props.businessData.filteredBusiness.response &&
                !this.props.businessData.filteredBusiness.success) {
                this.setState(state => ({ ...state, open: false }));
            }
            // Business - Error Service
            if (!this.props.businessData.filteredBusiness.loading &&
                this.props.businessData.filteredBusiness.response &&
                !this.props.businessData.filteredBusiness.success) {
                this.setState(state => ({ ...state, open: false }));
            }
        }
        // Send Correspondence - Before
        if (prevProps.sendCorrespondence !== this.props.sendCorrespondence) {
            if (!this.props.sendCorrespondence.loading &&
                this.props.sendCorrespondence.response &&
                this.props.sendCorrespondence.success) {

                const sendCorrespondence = this.props.sendCorrespondence.data.find(item => item.val_orden === 1)
                this.setState(state => ({
                    ...state,
                    client: {
                        ...state.client,
                        extra: {
                            ...state.client.extra,
                            sendCorrespondence: sendCorrespondence.des_valor,
                            sendCorrespondenceId: sendCorrespondence.cod_valor,
                        }
                    }
                }));
            }
            // Send Correspondence - Error Service
            else if (!this.props.sendCorrespondence.loading &&
                this.props.sendCorrespondence.response &&
                !this.props.sendCorrespondence.success) {

                this.getNotistack(`Envío Correspondecia: ${this.props.sendCorrespondence.error}`, "error");
            }
            // Send Correspondence - Error Service Connectivity
            else if (!this.props.sendCorrespondence.loading &&
                !this.props.sendCorrespondence.response &&
                !this.props.sendCorrespondence.success) {
                this.getNotistack(`Envío Correspondecia: ${this.props.sendCorrespondence.error}`, "error");
            }
        }
        // Account Status - Before
        if (prevProps.accountStatus !== this.props.accountStatus) {
            // Account Status - Error Service
            if (!this.props.accountStatus.loading &&
                this.props.accountStatus.response &&
                this.props.accountStatus.success) {
                const accountStatus = this.props.accountStatus.data.find(item => item.val_orden === 1)
                this.setState(state => ({
                    ...state,
                    client: {
                        ...state.client,
                        extra: {
                            ...state.client.extra,
                            accountStatus: accountStatus.des_valor,
                            accountStatusId: accountStatus.cod_valor,
                        }
                    }
                }));
            }
            else if (!this.props.accountStatus.loading &&
                this.props.accountStatus.response &&
                !this.props.accountStatus.success) {

                this.getNotistack(`Estado de Cuenta: ${this.props.accountStatus.error}`, "error");
            }
            // Account Status - Error Service Connectivity
            else if (!this.props.accountStatus.loading &&
                !this.props.accountStatus.response &&
                !this.props.accountStatus.success) {
                this.getNotistack(`Estado de Cuenta: ${this.props.accountStatus.error}`, "error");
            }
        }
        // Promoter
        if (prevProps.promoter !== this.props.promoter) {
            if (!this.props.promoter.loading && this.props.promoter.response && this.props.promoter.success) {
                this.setState(state => ({
                    ...state,
                    client: {
                        ...state.client,
                        extra: {
                            ...state.client.extra,
                            scannerCode: this.props.promoter.data.cod_escaner,
                            promoterId: this.props.promoter.data.cod_promotor,
                            promoterName: this.props.promoter.data.des_nom_promotor,
                            scannerCodeError: false,
                        }
                    }
                }));
            }
            else if (!this.props.promoter.loading && this.props.promoter.response && !this.props.promoter.success) {
                this.setState(state => ({
                    ...state,
                    client: {
                        ...state.client,
                        extra: {
                            ...state.client.extra,
                            scannerCode: "",
                            promoterId: "",
                            promoterName: "Promotor No Identificado",
                            scannerCodeError: true,
                        }
                    }
                }));
            }
            else if (!this.props.promoter.loading && !this.props.promoter.response && !this.props.promoter.success) {
                this.setState(state => ({
                    ...state,
                    client: {
                        ...state.client,
                        extra: {
                            ...state.client.extra,
                            scannerCode: "",
                            promoterId: "",
                            promoterName: "Promotor No Identificado",
                            scannerCodeError: true,
                        }
                    }
                }));
            }
        }
        // Second Call
        if (prevProps.secondCall !== this.props.secondCall) {
            // Enabled Button - Validate Identity Client
            if (this.props.secondCall.loading) {
                this.setState({
                    ...this.state,
                    infoPreviewButtonDisabled: true,
                    cancelButtonDisabled: true,
                    cancelModalButtonDisabled: true,
                    acceptModalButtonDisabled: true,
                    acceptModalButtonLoading: true
                });
            }
            else if (!this.props.secondCall.loading && this.props.secondCall.response && !this.props.secondCall.success) {
                this.setState({
                    ...this.state,
                    cancelButtonDisabled: false,
                    infoPreviewButtonDisabled: false,
                    cancelModalButtonDisabled: false,
                    acceptModalButtonDisabled: false,
                    acceptModalButtonLoading: false
                })
                // Notistack
                this.getNotistack(this.props.secondCall.error, "error");
            }
            else if (!this.props.secondCall.loading && !this.props.secondCall.response && !this.props.secondCall.success) {
                this.setState({
                    ...this.state,
                    cancelButtonDisabled: false,
                    infoPreviewButtonDisabled: false,
                    cancelModalButtonDisabled: false,
                    acceptModalButtonDisabled: false,
                    acceptModalButtonLoading: false
                })
                // Notistack
                this.getNotistack(this.props.secondCall.error, "error");
            }
        }
        // Activity - Simple
        if (prevProps.simpleGenericActivity !== this.props.simpleGenericActivity) {
            if (this.props.simpleGenericActivity.loading) {
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: true,
                    infoPreviewButtonDisabled: true,
                    cancelModalButtonDisabled: true,
                    acceptModalButtonDisabled: true,
                    acceptModalButtonLoading: true
                }));
            }
            if (!this.props.simpleGenericActivity.loading && this.props.simpleGenericActivity.response && this.props.simpleGenericActivity.success) {
                this.getNotistack("Consulta Correcta, 3er Paso Ok!", "success");
                setTimeout(() => {
                    this.props.handleNext();
                }, 1500);
            }
            else if (!this.props.simpleGenericActivity.loading && this.props.simpleGenericActivity.response && !this.props.simpleGenericActivity.success) {
                // Notistack
                this.getNotistack(this.props.simpleGenericActivity.error, "error");
                this.setState({
                    ...this.state,
                    cancelButtonDisabled: false,
                    infoPreviewButtonDisabled: false,
                    cancelModalButtonDisabled: false,
                    acceptModalButtonDisabled: false,
                    acceptModalButtonLoading: false
                })
            }
            else if (!this.props.simpleGenericActivity.loading && !this.props.simpleGenericActivity.response && !this.props.simpleGenericActivity.success) {
                // Notistack
                this.getNotistack(this.props.simpleGenericActivity.error, "error");
                this.setState({
                    ...this.state,
                    cancelButtonDisabled: false,
                    infoPreviewButtonDisabled: false,
                    cancelModalButtonDisabled: false,
                    acceptModalButtonDisabled: false,
                    acceptModalButtonLoading: false
                })
            }
        }
        // Activity - Cancel
        if (prevProps.cancelActivity !== this.props.cancelActivity) {
            if (this.props.cancelActivity.loading) {
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: true,
                    infoPreviewButtonDisabled: true,
                    cancelModalButtonDisabled: true,
                    acceptModalButtonDisabled: true,
                    acceptModalButtonLoading: true
                }));
            }
            if (!this.props.cancelActivity.loading &&
                this.props.cancelActivity.response &&
                this.props.cancelActivity.success) {

                this.props.handleCancelExpress(); // Cancel Father

                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    infoPreviewButtonDisabled: false,
                    cancelModalButtonDisabled: false,
                    acceptModalButtonDisabled: false,
                    acceptModalButtonLoading: false
                }));
            }
            else if (
                !this.props.cancelActivity.loading &&
                this.props.cancelActivity.response &&
                !this.props.cancelActivity.success) {
                // Notistack
                this.getNotistack(this.props.cancelActivity.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    infoPreviewButtonDisabled: false,
                    cancelModalButtonDisabled: false,
                    acceptModalButtonDisabled: false,
                    acceptModalButtonLoading: false
                }));
            }
            else if (
                !this.props.cancelActivity.loading &&
                !this.props.cancelActivity.response &&
                !this.props.cancelActivity.success) {
                // Notistack
                this.getNotistack(this.props.cancelActivity.error, "error");
                this.setState(state => ({
                    ...state,
                    cancelButtonDisabled: false,
                    infoPreviewButtonDisabled: false,
                    cancelModalButtonDisabled: false,
                    acceptModalButtonDisabled: false,
                    acceptModalButtonLoading: false
                }));
            }
        }
        // SMS Validated
        resolveStateRedux(prevProps.verifyCellphone.verifySMSCodeService, this.props.verifyCellphone.verifySMSCodeService,
            null,
            _ => this.setState(state => ({
                ...state,
                client: {
                    ...state.client,
                    cellphoneValidationSMS: {
                        ...state.client.cellphoneValidationSMS,
                        confirmed: true,
                        visiblePopup: false
                    }
                }
            })), null, null
        )
        // Register - Update Client into Master
        resolveStateRedux(prevProps.odcMaster.clientRegistered, this.props.odcMaster.clientRegistered,
            _ => this.setState(state => ({
                    ...state,
                    infoPreviewButtonDisabled: true,
                    cancelButtonDisabled: true,
                    cancelModalButtonDisabled: true,
                    acceptModalButtonDisabled: true,
                    acceptModalButtonLoading: true
            })), null,
            warningMessage => {
                this.setState(state => ({
                    ...state,
                    infoPreviewButtonDisabled: false,
                    cancelButtonDisabled: false,
                    cancelModalButtonDisabled: false,
                    acceptModalButtonDisabled: false,
                    acceptModalButtonLoading: false
                }))
                this.getNotistack(warningMessage, 'warning')
            },
            errorMessage => {
                this.setState(state => ({
                    ...state,
                    infoPreviewButtonDisabled: false,
                    cancelButtonDisabled: false,
                    cancelModalButtonDisabled: false,
                    acceptModalButtonDisabled: false,
                    acceptModalButtonLoading: false
                }))
                this.getNotistack(errorMessage, 'error')
            }
        )
    }

    // Submit - Register Client
    handleClientRegisterForm = e => {
        e.preventDefault();
        let firstNameError = true;
        let lastNameError = true;

        let genderError = true;
        let birthdayError = true;

        let maritalStatusError = true;
        let nationalityError = true;
        let academicDegreeError = true;
        let telephoneError = true;
        let cellphoneError = true;

        let grossIncomeError = true;
        let laborIncomeDateError = true;
        let rucError = true;
        let rucErrorVal = true;
        let bussNameError = true;

        let homeAddressNameViaError = true;
        let homeAddressDepartmentError = true;
        let homeAddressProvinceError = true;
        let homeAddressDistrictError = true;
        let homeAddressViaError = true;
        let homeAddressHousingTypeError = true;

        let employmentSituationError = true;
        let jobTitleError = false;
        let economicActivityError = false;
        let workTelephoneError = true;

        let workAddressNameViaError = true;
        let workAddressDepartmentError = true;
        let workAddressProvinceError = true;
        let workAddressDistrictError = true;
        let workAddressViaError = true;

        let extraPaymentDateError = true;
        let extraSendCorrespondenceError = true;
        let extraAccountStatusError = true;
        let extraPromoterError = true;
        let businessOptionError = true;
        let cellPhoneSMSValidated = true

        const homeAddressReferenceError = this.state.client.homeAddress.referenceError || false
        const workAddressReferenceError = this.state.client.workAddress.referenceError || false

        let { client, defaultMaxBirthday, defaultMinBirthday, defaultMinLaborIncomeDate, defaultMaxLaborIncomeDate } = this.state;

        if (this.firstNameInput.current.value.trim()) {
            firstNameError = false;
        }
        if (this.lastNameInput.current.value.trim()) {
            lastNameError = false;
        }

        const endB = new Date(defaultMinBirthday);
        const startB = new Date(defaultMaxBirthday);
        const valueB = new Date(client.birthday);
        const rangeB = moment().range(startB, endB);

        const endLI = new Date(defaultMinLaborIncomeDate);
        const startLI = new Date(defaultMaxLaborIncomeDate);
        const valueLI = new Date(client.laborIncomeDate);
        const rangeLI = moment().range(startLI, endLI);

        if (rangeB.contains(valueB)) {
            birthdayError = false;
        }
        if (rangeLI.contains(valueLI)) {
            laborIncomeDateError = false;
        }
        if (this.homeAddressNameViaInput.current.value.trim()) {
            homeAddressNameViaError = false;
        }
        if (this.workAddressNameViaInput.current.value.trim()) {
            workAddressNameViaError = false;
        }
        if (this.grossIncomeInput.current.value.trim()) {
            grossIncomeError = false;
        }
        if (client.gender !== "") {
            genderError = false;
        }
        if (client.maritalStatus !== "") {
            maritalStatusError = false;
        }
        if (client.nationality !== "") {
            nationalityError = false;
        }
        if (client.academicDegree !== "") {
            academicDegreeError = false;
        }
        if (client.cellphone.length === 9 && client.cellphone[0] === "9") {
            cellphoneError = false;
        }
        if (client.ruc.length === 11) {
            rucError = false;
        }
        if (!this.props.businessData.filteredBusiness.error) {
            rucErrorVal = false;
        }
        if (client.businessName) {
            bussNameError = false
        }
        if (client.homeAddress.department !== "") {
            homeAddressDepartmentError = false;
        }
        if (client.homeAddress.province !== "") {
            homeAddressProvinceError = false;
        }
        if (client.homeAddress.district !== "") {
            homeAddressDistrictError = false;
        }
        if (client.homeAddress.via !== "") {
            homeAddressViaError = false;
        }
        if (client.homeAddress.housingType !== "") {
            homeAddressHousingTypeError = false;
        }
        if (client.jobTitleDisabled === false) {
            if (client.jobTitle !== "") {
                jobTitleError = false;
            } else {
                jobTitleError = true;
            }
        }
        if (client.economicActivityDisabled === false) {
            if (client.economicActivity !== "") {
                economicActivityError = false;
            } else {
                economicActivityError = true;
            }
        }
        if (client.employmentSituation !== "") {
            employmentSituationError = false;
        }
        if (client.workAddress.department !== "") {
            workAddressDepartmentError = false;
        }
        if (client.workAddress.province !== "") {
            workAddressProvinceError = false;
        }
        if (client.workAddress.district !== "") {
            workAddressDistrictError = false;
        }
        if (client.workAddress.via !== "") {
            workAddressViaError = false;
        }
        if (client.extra.paymentDate !== "") {
            extraPaymentDateError = false;
        }
        if (client.extra.sendCorrespondence !== "") {
            extraSendCorrespondenceError = false;
        }
        if (client.extra.accountStatus !== "") {
            extraAccountStatusError = false;
        }
        if (client.businessOption !== "") {
            businessOptionError = false;
        }
        if (client.extra.promoterId !== "") {
            extraPromoterError = false;
        }
        if (!client.telephoneError) {
            telephoneError = false;
        }
        if (!client.workTelephoneError) {
            workTelephoneError = false;
        }
        if (client.cellphoneValidationSMS.validate && !client.cellphoneValidationSMS.confirmed) {
            cellPhoneSMSValidated = false
        }
        if (
            !firstNameError &&
            !lastNameError &&
            !genderError &&
            !birthdayError &&
            !maritalStatusError &&
            !nationalityError &&
            !academicDegreeError &&
            !cellphoneError &&
            !rucError &&
            !rucErrorVal &&
            !bussNameError &&
            !grossIncomeError &&
            !laborIncomeDateError &&
            !homeAddressNameViaError &&
            !homeAddressDepartmentError &&
            !homeAddressProvinceError &&
            !homeAddressDistrictError &&
            !homeAddressViaError &&
            !homeAddressHousingTypeError &&
            !employmentSituationError &&
            !jobTitleError &&
            !economicActivityError &&

            !workAddressNameViaError &&
            !workAddressDepartmentError &&
            !workAddressProvinceError &&
            !workAddressDistrictError &&
            !workAddressViaError &&
            !extraPaymentDateError &&
            !extraSendCorrespondenceError &&
            !extraAccountStatusError &&
            !client.extra.internetConsumeAdviceError &&
            !client.extra.foreignConsumeAdviceError &&
            !client.extra.effectiveDispositionError &&
            !client.extra.overflippingError &&
            !client.extra.mailAlertsError &&
            !client.extra.minimumForAlertsError &&
            !businessOptionError &&
            !extraPromoterError &&
            !telephoneError &&
            !workTelephoneError &&

            !homeAddressReferenceError &&
            !workAddressReferenceError &&
            cellPhoneSMSValidated
        ) {
            let fullName = ` ${this.lastNameInput.current.value ? this.lastNameInput.current.value : ""} ${this.motherLastNameInput.current.value ? this.motherLastNameInput.current.value : ""} ${this.firstNameInput.current.value ? this.firstNameInput.current.value : ""} ${this.secondNameInput.current.value ? this.secondNameInput.current.value : ""}`;
            let shortName = `${this.lastNameInput.current.value ? this.lastNameInput.current.value : ""} ${this.firstNameInput.current.value ? this.firstNameInput.current.value : ""}`;

            let numbers = this.grossIncomeInput.current.value;
            let moneyFormat = numbers.replace('S/', '');
            let money = parseFloat(moneyFormat);
            let data = {
                client: {
                    documentType: client.documentType,
                    documentTypeAux: client.documentTypeAux,
                    documentTypeId: client.documentTypeId,
                    documentTypeInternalValue: client.documentTypeInternalValue,
                    documentNumber: client.documentNumber,
                    firstName: this.firstNameInput.current.value.trim(),
                    secondName: this.secondNameInput.current.value.trim(),
                    lastName: this.lastNameInput.current.value.trim(),
                    motherLastName: this.motherLastNameInput.current.value.trim(),
                    fullName: fullName.trim(),
                    shortName: shortName.trim(),
                    birthday: client.birthday,
                    genderId: client.genderId,
                    gender: client.gender.trim(),
                    genderInternalValue: client.genderInternalValue.trim(),
                    maritalStatusId: client.maritalStatusId,
                    maritalStatus: client.maritalStatus.trim(),
                    maritalStatusInternalValue: client.maritalStatusInternalValue.trim(),
                    nationalityId: client.nationalityId,
                    nationality: client.nationality,
                    academicDegreeId: client.academicDegreeId,
                    academicDegree: client.academicDegree,
                    academicDegreeInternalValue: client.academicDegreeInternalValue.trim(),
                    email: this.emailInput.current.value.trim(),
                    cellphone: client.cellphone,
                    telephone: filterMaskTelephone(this.telephoneInput.current.value),
                    homeAddress: {
                        id: client.homeAddress.id,
                        departmentId: client.homeAddress.departmentId,
                        department: client.homeAddress.department,
                        provinceId: client.homeAddress.provinceId,
                        province: client.homeAddress.province,
                        districtId: client.homeAddress.districtId,
                        district: client.homeAddress.district,
                        viaId: client.homeAddress.viaId,
                        via: client.homeAddress.via,
                        viaInternalValue: client.homeAddress.viaInternalValue.trim(),
                        nameVia: this.homeAddressNameViaInput.current.value.trim(),
                        number: this.homeAddressNumberInput.current.value.trim(),
                        building: this.homeAddressBuildingInput.current.value.trim(),
                        inside: this.homeAddressInsideInput.current.value.trim(),
                        mz: this.homeAddressMzInput.current.value.trim(),
                        lot: this.homeAddressLotInput.current.value.trim(),
                        reference: client.homeAddress.reference,
                        housingTypeId: client.homeAddress.housingTypeId,
                        housingType: client.homeAddress.housingType.trim(),
                        housingTypeInternalValue: client.homeAddress.housingTypeInternalValue.trim(),
                        zone: client.homeAddress.zone.trim(),
                        zoneId: client.homeAddress.zoneId,
                        zoneInternalValue: client.homeAddress.zoneInternalValue.trim(),
                        nameZone: client.homeAddress.nameZone.trim(),
                    },
                    workAddress: {
                        id: client.workAddress.id,
                        departmentId: client.workAddress.departmentId,
                        department: client.workAddress.department,
                        provinceId: client.workAddress.provinceId,
                        province: client.workAddress.province,
                        districtId: client.workAddress.districtId,
                        district: client.workAddress.district,
                        viaId: client.workAddress.viaId,
                        via: client.workAddress.via,
                        viaInternalValue: client.workAddress.viaInternalValue.trim(),
                        nameVia: this.workAddressNameViaInput.current.value.trim(),
                        number: this.workAddressNumberInput.current.value.trim(),
                        building: this.workAddressBuildingInput.current.value.trim(),
                        inside: this.workAddressInsideInput.current.value.trim(),
                        mz: this.workAddressMzInput.current.value.trim(),
                        lot: this.workAddressLotInput.current.value.trim(),
                        reference: client.workAddress.reference.trim(),
                        zone: client.workAddress.zone.trim(),
                        zoneId: client.workAddress.zoneId,
                        zoneInternalValue: client.workAddress.zoneInternalValue.trim(),
                        nameZone: client.workAddress.nameZone.trim(),
                    },
                    ruc: client.ruc,
                    businessName: client.businessName,
                    employmentSituationId: client.employmentSituationId,
                    employmentSituation: client.employmentSituation.trim(),
                    jobTitleId: client.jobTitleId,
                    jobTitle: client.jobTitle.trim(),
                    economicActivityId: client.economicActivityId,
                    economicActivity: client.economicActivity.trim(),
                    laborIncomeDate: client.laborIncomeDate,
                    grossIncome: money,
                    workTelephone: filterMaskTelephone(this.workTelephoneInput.current.value),
                    workTelephoneExtension: this.workTelephoneExtensionInput.current.value,
                    extra: {
                        paymentDateId: client.extra.paymentDateId,
                        paymentDateInternalValue: client.extra.paymentDateInternalValue,
                        paymentDate: client.extra.paymentDate.trim(),
                        sendCorrespondenceId: client.extra.sendCorrespondenceId,
                        sendCorrespondence: client.extra.sendCorrespondence.trim(),
                        accountStatusId: client.extra.accountStatusId,
                        accountStatus: client.extra.accountStatus.trim(),
                        promoterId: client.extra.promoterId,
                        scannerCode: client.extra.scannerCode,
                        promoterName: client.extra.promoterName ? client.extra.promoterName.trim() : "",
                        term: client.extra.term,
                        internetConsumeAdvice: client.extra.internetConsumeAdvice,
                        foreignConsumeAdvice: client.extra.foreignConsumeAdvice,
                        effectiveDisposition: client.extra.effectiveDisposition,
                        overflipping: client.extra.overflipping,
                        mailAlerts: client.extra.mailAlerts,
                        minimumForAlerts: client.extra.minimumForAlerts,
                    }
                }
            }

            this.setState(state => ({
                ...state,
                client: {
                    ...state.client,
                    ...data.client,
                    homeAddress: {
                        ...state.client.homeAddress,
                        ...data.client.homeAddress
                    },
                    workAddress: {
                        ...state.client.workAddress,
                        ...data.client.workAddress
                    },
                    extra: {
                        ...state.client.extra
                    }
                },
                openPreview: true
            }));
        }
        else {
            if (firstNameError) {
                this.getNotistack("Primer Nombre: Verificar porfavor", "warning");
                this.firstNameInput.current.value = "";
                this.firstNameInput.current.focus();
            }

            if (lastNameError) {
                this.getNotistack("Apellido Paterno: Verificar porfavor", "warning");
                this.lastNameInput.current.value = "";
                this.lastNameInput.current.focus();
            }

            if (birthdayError) {
                this.getNotistack("Fecha Nacimiento: Verificar porfavor", "warning");
            }

            if (laborIncomeDateError) {
                this.getNotistack("Fecha de Ingreso: Verificar porfavor", "warning");
            }

            if (homeAddressNameViaError) {
                this.getNotistack("Dir. Personal - Nombre via: Verificar porfavor", "warning");
                this.homeAddressNameViaInput.current.value = "";
                this.homeAddressNameViaInput.current.focus();
            }

            if (workAddressNameViaError) {
                this.getNotistack("Dir. Laboral - Nombre via: Verificar porfavor", "warning");
                this.workAddressNameViaInput.current.value = "";
                this.workAddressNameViaInput.current.focus();
            }
            if (grossIncomeError) {
                this.getNotistack("Ingreso Bruto: Verificar porfavor", "warning");
                this.grossIncomeInput.current.value = "";
                this.grossIncomeInput.current.focus();
            }
            if (genderError)
                this.getNotistack("Género: Verificar porfavor", "warning");
            if (maritalStatusError)
                this.getNotistack("Estado Civil: Verificar porfavor", "warning");
            if (academicDegreeError)
                this.getNotistack("Grado Académico: Verificar porfavor", "warning");
            if (nationalityError)
                this.getNotistack("Nacionalidad: Verificar porfavor", "warning");
            if (cellphoneError)
                this.getNotistack("Celular: Verificar porfavor", "warning");
            if (homeAddressDepartmentError)
                this.getNotistack("Dir. Personal - Departamento: Verificar porfavor", "warning");
            if (homeAddressProvinceError)
                this.getNotistack("Dir. Personal - Provincia: Verificar porfavor", "warning");
            if (homeAddressDistrictError)
                this.getNotistack("Dir. Personal - Distrito: Verificar porfavor", "warning");
            if (homeAddressViaError)
                this.getNotistack("Dir. Personal - Via: Verificar porfavor", "warning");
            if (homeAddressHousingTypeError)
                this.getNotistack("Dir. Personal - Tipo Vivienda: Verificar porfavor", "warning");
            if (workAddressDepartmentError)
                this.getNotistack("Dir. Laboral - Departamento: Verificar porfavor", "warning");
            if (workAddressProvinceError)
                this.getNotistack("Dir. Laboral - Provincia: Verificar porfavor", "warning");
            if (workAddressDistrictError)
                this.getNotistack("Dir. Laboral - Distrito: Verificar porfavor", "warning");
            if (workAddressViaError)
                this.getNotistack("Dir. Laboral - Via: Verificar porfavor", "warning");
            if (jobTitleError)
                this.getNotistack("Cargo - Profesión: Verificar porfavor", "warning");
            if (economicActivityError)
                this.getNotistack("Actividad Económica: Verificar porfavor", "warning");
            if (employmentSituationError)
                this.getNotistack("Situación Laboral: Verificar porfavor", "warning");
            if (businessOptionError)
                this.getNotistack("Ruc - Razón Social: Verificar porfavor", "warning");
            if (extraPaymentDateError)
                this.getNotistack("Fecha de Pago: Verificar porfavor", "warning");
            if (extraSendCorrespondenceError)
                this.getNotistack("Envío Correspondencia: Verificar porfavor", "warning");
            if (extraAccountStatusError)
                this.getNotistack("Estado de cuenta: Verificar porfavor", "warning");
            if (client.extra.internetConsumeAdviceError)
                this.getNotistack("Consumos por Internet: Verificar porfavor", "warning");
            if (client.extra.foreignConsumeAdviceError)
                this.getNotistack("Consumos en el Extranjero: Verificar porfavor", "warning");
            if (client.extra.effectiveDispositionError)
                this.getNotistack("Retirar Efectivo: Verificar porfavor", "warning");
            if (client.extra.overflippingError)
                this.getNotistack("Sobregirar Linea: Verificar porfavor", "warning");
            if (client.extra.mailAlertsError)
                this.getNotistack("Notificaciones de Consumo: Verificar porfavor", "warning");
            if (client.extra.minimumForAlertsError)
                this.getNotistack("Monto Minimo: Verificar porfavor", "warning");
            if (extraPromoterError)
                this.getNotistack("Código Escáner: Verificar porfavor", "warning");
            if (telephoneError)
                this.getNotistack("Teléfono: Verificar porfavor", "warning");
            if (workTelephoneError)
                this.getNotistack("Teléfono Laboral: Verificar porfavor", "warning");
            if (rucError)
                this.getNotistack("RUC invalido: Cant. digitos 11", "warning");
            if (bussNameError)
                this.getNotistack("Razón Social: Este campo no debe estar vacío.", "warning");
            if (rucErrorVal)
                this.getNotistack(`RUC / Razón Social Invalido: ${this.props.businessData.filteredBusiness.error}`, "warning");
            if (homeAddressReferenceError)
                this.getNotistack("Referencia - Dirección Personal: Solo es permitido caracteres alfanuméricos (A-Z;0-9).", "warning");
            if (workAddressReferenceError)
                this.getNotistack("Referencia - Dirección Laboral: Solo es permitido caracteres alfanuméricos (A-Z;0-9).", "warning");
            if (!cellPhoneSMSValidated)
                this.getNotistack('Celular: Verificar el código porfavor.', 'warning')
            this.setState(state => ({
                ...state,
                client: {
                    ...state.client,
                    firstNameError: firstNameError,

                    genderError: genderError,
                    maritalStatusError: maritalStatusError,
                    nationalityError: nationalityError,
                    academicDegreeError: academicDegreeError,
                    cellphoneError: cellphoneError,
                    birthdayError: birthdayError,
                    rucError: rucError,
                    homeAddress: {
                        ...state.client.homeAddress,
                        departmentError: homeAddressDepartmentError,
                        provinceError: homeAddressProvinceError,
                        districtError: homeAddressDistrictError,
                        viaError: homeAddressViaError,
                        housingTypeError: homeAddressHousingTypeError
                    },
                    workAddress: {
                        ...state.client.workAddress,
                        departmentError: workAddressDepartmentError,
                        provinceError: workAddressProvinceError,
                        districtError: workAddressDistrictError,
                        viaError: workAddressViaError,
                    },
                    jobTitleError: jobTitleError,
                    economicActivityError: economicActivityError,
                    employmentSituationError: employmentSituationError,
                    extra: {
                        ...state.client.extra,
                        paymentDateError: extraPaymentDateError,
                        sendCorrespondenceError: extraSendCorrespondenceError,
                        accountStatusError: extraAccountStatusError,
                        scannerCodeError: extraPromoterError,
                    },
                    businessOptionError: businessOptionError,
                    workTelephoneError: workTelephoneError,
                    telephoneError: telephoneError,

                }
            }));
        }
    }

    // Send Back
    handleSubmitRegisterClient = e => {
        e.preventDefault();
        let { client } = this.state;
        let sendData = {
            client: {
                ...client,
                extra: {
                    ...client.extra,
                    effectiveDisposition: this.props.isSAEProcess ? '1' : client.extra.effectiveDisposition
                }
            }
        }
        this.props.handleClientRegister({ data: sendData, reset: true });
    }
    // Submit - Origination
    handleCancelExpress = e => {
        e.preventDefault();
        this.props.handleOpenModalCancelExpress();
    }
    handleToggleInfoPreview = e => {
        this.setState(state => ({
            ...state,
            openPreview: !state.openPreview
        }));
    }
    // Client - Document Number - TextField Event Change
    handleChangeTextFieldDocumentNumber = name => e => {
        let { value } = e.target;
        let nameError = `${name}Error`;
        if (!isNaN(Number(value))) {
            this.setState(state => ({
                client: {
                    ...state.client,
                    [name]: value,
                    [nameError]: this.validateLengthDocumentType(value.length)
                }
            }));
        }
    }
    // Client - Employment Situation - Select Event Change
    handleChangeSelectEmploymentSituation = name => e => {
        let jobTitleDisabled = true;
        let economicActivityDisabled = true;
        let employmentSituationError = true;
        let jobTitleError = true;
        let economicActivityError = true;

        let employmentSituation = "";
        let employmentSituationId = "";
        let jobTitle = "";
        let jobTitleId = "";
        let economicActivity = "";
        let economicActivityId = "";

        if (e !== null) {
            employmentSituation = e.label;
            employmentSituationId = e.value;
            if (e.value === 160001) {
                jobTitleDisabled = false;
                economicActivityError = false;
                employmentSituationError = false;
            }
            else if (e.value === 160002) {
                employmentSituationError = false;
                jobTitleDisabled = false;
                economicActivityError = false;
            }
            else if (e.value === 160003) {
                employmentSituationError = false;
                jobTitleError = false;
                economicActivityDisabled = false;
            }
            else if (e.value === 160004) {
                employmentSituationError = false;
                jobTitleError = false;
                economicActivityDisabled = false;
            }
            else {
                employmentSituationError = false;
                jobTitleError = false;
                economicActivityError = false;
            }
        }
        else {
            jobTitleError = false;
            economicActivityError = false;
        }

        this.setState(state => ({
            ...state,
            client: {
                ...state.client,
                employmentSituation: employmentSituation,
                employmentSituationId: employmentSituationId,
                employmentSituationError: employmentSituationError,
                jobTitle: jobTitle,
                jobTitleId: jobTitleId,
                jobTitleError: jobTitleError,
                jobTitleDisabled: jobTitleDisabled,
                economicActivity: economicActivity,
                economicActivityId: economicActivityId,
                economicActivityError: economicActivityError,
                economicActivityDisabled: economicActivityDisabled
            }
        }));
    }

    handleInvalidBirthday = (objectName, subObjectName, name) => e => {
        // const {minBirthday,  maxBirthday, client} = this.state;
        // if(client.birthdayError){
        //     e.target.setCustomValidity(`CDA: La edad válida es entre ${minBirthday} a ${maxBirthday} año(s). `);
        // }
        // else{
        //     e.target.setCustomValidity("");
        // }
    }
    setCustomValidity = name => e => {
        // console.log(name, e);
    }
    handleInvalidTextFieldLaborIncomeDate = (objectName, subObjectName, name) => e => {
        //const {minLaborIncomeDate,  maxLaborIncomeDate/*, client*/} = this.state;
        //e.target.setCustomValidity(`CDA: Antigüedad válida es entre ${minLaborIncomeDate} a ${maxLaborIncomeDate} año(s).`);
    }
    // Client - Business Option - Select Event Change
    handleChangeSelectBusinessOption = (objectName, subObjectName, name) => e => {
        let businessOptionError = true;
        let businessOption = "";
        let businessNameRequired = false;
        let businessNameDisabled = true;
        let businessNameError = false;

        let rucRequired = false;
        let rucDisabled = true;
        let rucError = false;

        if (e !== null) {
            businessOption = e.value;
            businessOptionError = false;
            if (businessOption === "1") {
                businessNameDisabled = false;
                businessNameError = true;
                businessNameRequired = true;
            }
            else {
                rucDisabled = false;
                rucError = true;
                rucRequired = true;
            }
        }

        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        businessOption: businessOption,
                        businessOptionError: businessOptionError,
                        ruc: "",
                        rucError: rucError,
                        rucDisabled: rucDisabled,
                        rucRequired: rucRequired,
                        businessName: "",
                        businessNameError: businessNameError,
                        businessNameRequired: businessNameRequired,
                        businessNameDisabled: businessNameDisabled
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    businessOption: businessOption,
                    businessOptionError: businessOptionError,
                    ruc: "",
                    rucError: rucError,
                    rucDisabled: rucDisabled,
                    rucRequired: rucRequired,
                    businessName: "",
                    businessNameError: businessNameError,
                    businessNameRequired: businessNameRequired,
                    businessNameDisabled: businessNameDisabled
                }
            }));
        }
    }
    // Client - Ruc - TexField Event Change
    handleChangeTextFieldRUC = (objectName, subObjectName, name) => e => {
        let { currentTarget } = e;
        let nameError = `${name}Error`;
        let { value } = e.target;
        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [name]: value,
                        [nameError]: this.validateLengthRuc(value.length),
                        anchorEl: currentTarget.parentElement
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [name]: value,
                    [nameError]: this.validateLengthRuc(value.length),
                    anchorEl: currentTarget
                }
            }));
        }
    }
    // Client - Seach Business - TextField Event Change
    handleKeyUpTextFieldSearchBusiness = (isRucOrNameBusiness = true) => e => {
        let { value } = e.target;
        if (isRucOrNameBusiness) {
            this.props.getFilterByNameBusinessOrRuc("", value);
        } else {
            this.props.getFilterByNameBusinessOrRuc(value, "");
        }
    }
    // Client - Close Modal
    handleCloseModalBusiness = name => e => {
        this.setState(state => ({ open: false }));
    }
    // Client - Select Business - Event Click
    handleClickMenuBusiness = (item) => e => {
        if (item) {
            this.setState(state => ({
                ...state,
                client: {
                    ...state.client,
                    businessName: item.nom_emp,
                    ruc: item.ruc_emp,
                    rucError: false,
                    businessNameError: false,
                },
                open: false,
                anchorEl: null
            }));
        }
    }
    // Validate Length Document Type
    validateLengthDocumentType = (value) => {
        switch (value) {
            case 8: return false;
            case 9: return false;
            default: return true;
        }
    }
    // Validate Length Ruc
    validateLengthRuc = (value) => {
        switch (value) {
            case 11: return false;
            default: return true;
        }
    }
    // Client - Ubigeo Department - Select Event Change
    handleChangeSelectUbigeoDepartment = (objectName, subObjectName, name, isHomeOrWork = true) => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let department = "";
        let departmentId = "";
        let provinceError = false;

        if (e !== null) {
            error = false;
            provinceError = true;
            let { label, value } = e;
            department = label;
            departmentId = value;
            let data = {
                type: "PR",
                value: value
            }
            isHomeOrWork ? this.props.getHomeAddressProvince(data) : this.props.getWorkAddressProvince(data);
        }
        this.setState(state => ({
            ...state,
            [objectName]: {
                ...state[objectName],
                [subObjectName]: {
                    ...state[objectName][subObjectName],
                    [name]: department,
                    [nameId]: departmentId,
                    [nameError]: error,
                    provinceError: provinceError,
                    provinceDisabled: true,
                    provinceId: "",
                    province: "",
                    districtError: false,
                    districtDisabled: true,
                    districtId: "",
                    district: ""
                }
            }
        }));
    }
    // Client - Ubigeo Provice - Select Event Change
    handleChangeSelectUbigeoProvince = (objectName, subObjectName, name, isHomeOrWork = true) => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let province = "";
        let provinceId = "";
        let provinceError = false;

        if (e !== null) {
            error = false;
            provinceError = true;
            let { label, value } = e;
            province = label;
            provinceId = value;
            let data = {
                type: "DI",
                value: value
            }
            isHomeOrWork ? this.props.getHomeAddressDistrict(data) : this.props.getWorkAddressDistrict(data);
        }
        this.setState(state => ({
            ...state,
            [objectName]: {
                ...state[objectName],
                [subObjectName]: {
                    ...state[objectName][subObjectName],
                    [name]: province,
                    [nameId]: provinceId,
                    [nameError]: error,
                    districtError: provinceError,
                    districtDisabled: true,
                    districtId: "",
                    district: "",
                }
            }
        }));
    }
    // Client - Address - Zone - Select Event Change
    handleChangeSelectZone = (objectName, subObjectName, data = [], name) => e => {
        let nameZoneDisabled = true;
        let nameZone = "";
        let nameZoneError = true;
        if (e !== null) {
            if (this.state[objectName][subObjectName].nameZone !== "") {
                nameZoneError = false;
            }
            nameZoneDisabled = false;
            nameZone = this.state[objectName][subObjectName].nameZone;
            let object = data.find(option => option.cod_valor === e.value);
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        zone: e.label,
                        zoneId: e.value,
                        zoneAux: object && object.des_auxiliar && object.des_auxiliar,
                        zoneInternalValue: object && object.valor_interno && object.valor_interno,
                        nameZone: nameZone,
                        nameZoneError: nameZoneError,
                        nameZoneDisabled: nameZoneDisabled
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        zone: "",
                        zoneId: "",
                        zoneAux: "",
                        zoneInternalValue: "",
                        nameZone: "",
                        nameZoneError: nameZoneError,
                        nameZoneDisabled: nameZoneDisabled
                    }
                }
            }));
        }
    }
    // Client - Address - NameZone - TextField Event Change
    handleChangeTextFieldNameZone = (objectName, subObjectName, name) => e => {
        e.persist();
        let nameZoneError = true;
        if (!this.state[objectName][subObjectName].nameZoneDisabled) {
            if (e.target.value !== "") {
                nameZoneError = false
            }
        }
        else {
            nameZoneError = false;
        }
        this.setState(state => ({
            ...state,
            [objectName]: {
                ...state[objectName],
                [subObjectName]: {
                    ...state[objectName][subObjectName],
                    nameZone: e.target.value,
                    nameZoneError: nameZoneError
                }
            }
        }));
    }
    // Client - Search Promoter - TextField KeyPress Change
    handleKeyPressSearchScannerCode = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if (!checkInputKeyCode(code)) {
            e.preventDefault();
        }
        if (code === 13) {
            this.props.getPromoter(e.target.value);
            e.preventDefault();
        }
    }
    // Only Text
    handleKeyPressTextFieldOnlyText = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if (!onlyTextKeyCode(code)) {
            e.preventDefault();
        }
    }
    // Only Number
    handleKeyPressTextFieldOnlyNumber = (_, label) => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if (!onlyNumberKeyCode(code)) {
            e.preventDefault();
        }
        handleEnterKeyPress(e, _ => {
            if (label === 'cellphone') this.handleSendSMS()
        })
    }
    // Handle send SMS
    handleSendSMS = _ => {
        const { client } = this.state
        const { validate, confirmed } = client.cellphoneValidationSMS
        if (validate && !confirmed) {
            if (!client.cellphone && client.cellphone.length !== 9) {
                this.getNotistack('Enviar Código: Verificar número de celular', 'warning')
            }
            else {
                const smsData = {
                    cod_solicitud_completa: this.props.origination.fullNumber,
                    des_nro_documento: client.documentNumber,
                    des_telefono: client.cellphone
                }
                this.props.sendSMSCode(smsData)
            }
        }
    }
    // Check Input
    handleKeyPressTextFieldCheckInput = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if (!checkInputKeyCode(code)) {
            e.preventDefault();
        }
    }

    // Check OnBlur Telephone
    handleBlurTextFieldValidateTelephoneInput = (objectName, subObjectName, name) => e => {
        e.persist();
        let nameError = `${name}Error`;
        let { value } = e.target;
        let error = filterMaskTelephone(value).length !== 10 && filterMaskTelephone(value).length !== 0;

        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [nameError]: error
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [nameError]: error
                }
            }));
        }
    }
    // Check Email Input
    handleBlurTextFieldCheckEmailInput = (objectName, subObjectName, name) => e => {
        e.persist();
        let nameError = `${name}Error`;
        let { value } = e.target;
        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [nameError]: !checkEmailInputRegex(value)
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [nameError]: !checkEmailInputRegex(value)
                }
            }));
        }
    }
    // Client - Date - TextField Event Blur
    handleBlurTextFieldBirthday = (objectName, subObjectName, name) => e => {
        const { defaultMaxBirthday, defaultMinBirthday } = this.state;
        const nameError = `${name}Error`;
        const end = new Date(defaultMinBirthday);
        const start = new Date(defaultMaxBirthday);
        const value = new Date(e.target.value);
        const range = moment().range(start, end);
        let error = true;

        if (range.contains(value)) {
            error = false;
        }

        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [nameError]: error,
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [nameError]: error,
                }
            }));
        }
    }
    // Client - Labor Income Date - TextField Event Blur
    handleBlurTextFieldLaborIncomeDate = (objectName, subObjectName, name) => e => {
        const { defaultMaxLaborIncomeDate, defaultMinLaborIncomeDate } = this.state;

        const nameError = `${name}Error`;
        const end = new Date(defaultMinLaborIncomeDate);
        const start = new Date(defaultMaxLaborIncomeDate);
        const value = new Date(e.target.value);
        const range = moment().range(start, end);
        let error = true;

        if (range.contains(value)) {
            error = false;
        }

        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [nameError]: error,
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [nameError]: error,
                }
            }));
        }
    }
    // Client - TextField - Event Blur
    handleBlurTextFieldRequired = (objectName, subObjectName, name) => e => {
        e.persist();
        let nameError = `${name}Error`;
        let { value } = e.target;
        value = value.trim();
        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [nameError]: value ? false : true,
                        [name]: value ? value : ""
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [nameError]: value ? false : true,
                    [name]: value ? value : ""
                }
            }));
        }
    }
    // Client - TextField - Event Change - Required
    handleChangeTextFieldRequired = (objectName, subObjectName, name) => e => {
        e.persist();
        let { value } = e.target;
        let nameError = `${name}Error`;

        if (subObjectName) {
            this.setState({
                [objectName]: {
                    ...this.state[objectName],
                    [subObjectName]: {
                        ...this.state[objectName][subObjectName],
                        [name]: value ? value : "",
                        [nameError]: value !== "" ? false : true,

                    }
                }
            });
        } else {
            this.setState({
                [objectName]: {
                    ...this.state[objectName],
                    [name]: value ? value : "",
                    [nameError]: value !== "" ? false : true
                }
            });
        }
    }
    // Client - TextField - Event Change
    handleChangeTextField = (objectName, subObjectName, name) => e => {
        let { value } = e.target;
        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [name]: value
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [name]: value
                }
            }));
        }
    }
    handleChangeOnlyTextAndNumberField = (objectName, subObjectName, name) => e => {
        const { value } = e.target;
        let nameError = `${name}Error`;
        const textIsValid = checkInputRegex(value)
        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [name]: value,
                        [nameError]: !textIsValid
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [name]: value,
                    [nameError]: !textIsValid
                }
            }));
        }
    }
    // Client - Select - Event Change
    handleChangeSelectFullRequired = (objectName, subObjectName, data = [], name) => e => {

        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let nameAux = `${name}Aux`;
        let nameInternalValue = `${name}InternalValue`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        let object = null;
        if (e !== null) {
            error = false;
            let { label, value } = e;
            object = data.find(option => option.cod_valor === value);
            valueDefault = label;
            idDefault = value;
        }
        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [name]: valueDefault,
                        [nameId]: idDefault,
                        [nameAux]: object && object.des_auxiliar && object.des_auxiliar,
                        [nameInternalValue]: object && object.valor_interno && object.valor_interno,
                        [nameError]: error,
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [name]: valueDefault,
                    [nameId]: idDefault,
                    [nameAux]: object && object.des_auxiliar && object.des_auxiliar,
                    [nameInternalValue]: object && object.valor_interno && object.valor_interno,
                    [nameError]: error,
                }
            }));
        }

    }
    // Client - Select - Event Change
    handleChangeSelectRequired = (objectName, subObjectName, name) => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        if (e !== null) {
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }
        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [name]: valueDefault,
                        [nameId]: idDefault,
                        [nameError]: error,
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [name]: valueDefault,
                    [nameId]: idDefault,
                    [nameError]: error,
                }
            }));
        }
    }
    // Client - Default Select - Event Change
    handleChangeDefaultSelectFullRequired = (objectName, subObjectName, data = [], name) => e => {
        let object = data.find(option => option.cod_valor === e.target.value);
        let nameId = `${name}Id`;
        let nameAux = `${name}Aux`;
        let nameInternalValue = `${name}InternalValue`;
        let nameError = `${name}Error`;
        let { value } = e.target;
        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [name]: object && object.des_valor && object.des_valor,
                        [nameAux]: object && object.des_auxiliar && object.des_auxiliar,
                        [nameInternalValue]: object && object.valor_interno && object.valor_interno,
                        [nameId]: value,
                        [nameError]: value !== "" ? false : true
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [name]: object && object.des_valor && object.des_valor,
                    [nameAux]: object && object.des_auxiliar && object.des_auxiliar,
                    [nameInternalValue]: object && object.valor_interno && object.valor_interno,
                    [nameId]: value,
                    [nameError]: value !== "" ? false : true
                }
            }));
        }
    }
    // Client - Radio Button - Event Change
    handleChangeRadioButtonRequired = (objectName, subObjectName, name) => (e, value) => {
        let nameError = `${name}Error`;
        let error = true;
        if (value !== "") {
            error = false
        }
        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [name]: value,
                        [nameError]: error
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [name]: value,
                    [nameError]: error
                }
            }));
        }
    }
    // Client - Radio Button - Event Change =
    handleChangeCheckBoxRequired = (objectName, subObjectName, name) => (e) => {
        let { checked } = e.target;
        if (subObjectName) {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [subObjectName]: {
                        ...state[objectName][subObjectName],
                        [name]: checked
                    }
                }
            }));
        }
        else {
            this.setState(state => ({
                ...state,
                [objectName]: {
                    ...state[objectName],
                    [name]: checked
                }
            }));
        }
    }

    handleKeyPressTextFieldOnlyNumberDot = (name) => (e) => {
        var rgx = /^[0-9]*\.?[0-9]*$/;
        return e.toString().match(rgx);
    };

    handleChangeMinimumForAlerts = (objectName, subObjectName, name) => (e) => {
        const { value } = e.target;
        const nameError = `${name}Error`;
        this.setState(state => ({
            ...state,
            [objectName]: {
                ...state[objectName],
                [subObjectName]: {
                    ...state[objectName][subObjectName],
                    [name]: value,
                    [nameError]: value < 0 || value > 9999999.99
                }
            }
        }));
    };

    handleOpenSMSPopupValidator = _ => _ => {
        const { visiblePopup } = this.state.client.cellphoneValidationSMS
        this.setState(state => ({
            ...state,
            client: {
                ...state.client,
                cellphoneValidationSMS: {
                    ...state.client.cellphoneValidationSMS,
                    visiblePopup: !visiblePopup
                }
            }
        }))
    }

    render() {
        let { classes,
            idenfiticationDocumentType,
            gender,
            maritalStatus,
            country,
            academicDegree,
            department,
            provinceFilter,
            districtFilter,
            via,
            zone,
            housingType,
            employmentSituation,
            jobTitle,
            economicActivity,
            businessData,
            paymentDate,
            sendCorrespondence,
            accountStatus } = this.props;

        let { homeAddressProvince, workAddressProvince } = provinceFilter;
        let { homeAddressDistrict, workAddressDistrict } = districtFilter;

        let { client,
            /*defaultMinBirthday, defaultMaxBirthday, minBirthday, maxBirthday,*/
            /*defaultMaxLaborIncomeDate, defaultMinLaborIncomeDate,*/ maxLaborIncomeDate, minLaborIncomeDate, loading } = this.state;
        // Format Gender
        let genderData = gender.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor,
        }));
        // Format Marital Status
        let maritalData = maritalStatus.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor,
        }));
        // Format Nationality
        let countryData = country.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor,
        }));
        // Format Academic Degree
        let academicDegreeData = academicDegree.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor,
        }));
        // Format Home Address Department
        let homeAddressDepartmentData = department.data.map(item => ({
            value: item.cod_det_ubi, //cod_ubigeo
            label: item.val_ubigeo
        }));
        // Format Home Address Province
        let homeAddressProvinceData = homeAddressProvince.data.map(item => ({
            value: item.cod_det_ubi,
            label: item.val_ubigeo
        }));
        // Format Home Address District
        let homeAddressDistrictData = homeAddressDistrict.data.map(item => ({
            value: item.cod_det_ubi,
            label: item.val_ubigeo
        }));
        // Format Home Address Via
        let homeAddressViaData = via.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        }));
        // Format Home Address Zone
        let homeAddressZoneData = zone.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        }));
        // Format Home Address Housing Type
        let homeAddressHousingTypeData = housingType.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        }));
        // Employment Situation
        let employmentSituationData = employmentSituation.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        }));
        // Job Title
        let jobTitleData = jobTitle.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        }));
        // Economic Activity
        let economicActivityData = economicActivity.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        }));
        // Format Work Address Department
        let workAddressDepartmentData = department.data.map(item => ({
            value: item.cod_det_ubi,
            label: item.val_ubigeo
        }));
        // Format Work Address Province
        let workAddressProvinceData = workAddressProvince.data.map(item => ({
            value: item.cod_det_ubi,
            label: item.val_ubigeo
        }));
        // Format Work Address District
        let workAddressDistrictData = workAddressDistrict.data.map(item => ({
            value: item.cod_det_ubi,
            label: item.val_ubigeo
        }));
        // Format Work Address Via
        let workAddressViaData = via.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        }));
        // Format Work Address Zone
        let workAddressZoneData = zone.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        }));
        // Format Extra Payment Date
        let extraPaymentDateData = paymentDate.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        }));
        // Format Extra Business
        let businessOptionData = [
            { value: "0", label: "RUC" },
            { value: "1", label: "Razón Social" }
        ]
        // Format Extra Send Correspondence
        let extraSendCorrespondenceData = sendCorrespondence.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        }));
        // Format Extra Account Status
        let extraAccountStatusData = accountStatus.data.map(item => ({
            value: item.cod_valor,
            label: item.des_valor
        }));

        return (
            <React.Fragment>
                <div className="mb-2">
                    {
                        loading ?
                            <>
                                <Typography align="center"> Cargando información del cliente... </Typography>
                                <LinearProgress />
                            </>
                            :
                            <form method="post" onSubmit={this.handleClientRegisterForm} autoComplete="off" >
                                <Grid container spacing={8} className="mb-2">
                                    {/* Panel - Personal Info */}
                                    <Grid item xs={12}>
                                        <ExpansionPanel className={classes.root} defaultExpanded>
                                            <ExpansionPanelSummary
                                                className={classes.expansionPanel}
                                                expandIcon={<ExpandMoreIcon fontSize="small" />}>
                                                <Typography className={classes.heading}>
                                                    <AccountCircleIcon fontSize="small" className="mr-2" />
                                        Información Personal
                                    </Typography>
                                            </ExpansionPanelSummary>
                                            <ExpansionPanelDetails>
                                                <Grid container spacing={8}>
                                                    <Grid item xs={12}>
                                                        <Grid container spacing={8}>
                                                            {/* Client - Document Type */}
                                                            <Grid item xs={12} sm={6} md={6} lg={3}>
                                                                <FormControl
                                                                    required
                                                                    error={client.documentTypeError}
                                                                    margin="normal"
                                                                    fullWidth={true}>
                                                                    <InputLabel htmlFor="documentType">Tipo Documento</InputLabel>
                                                                    <Select
                                                                        value={client.documentTypeId}
                                                                        inputProps={{
                                                                            readOnly: client.documentTypeReadOnly,
                                                                            id: 'documentType'
                                                                        }}
                                                                        onChange={this.handleChangeDefaultSelectFullRequired("client", null, idenfiticationDocumentType.data, "documentType")}>
                                                                        {
                                                                            idenfiticationDocumentType.data.map((item, index) => (
                                                                                <MenuItem key={index} value={item.cod_valor}>{item.des_valor_corto}</MenuItem>
                                                                            ))
                                                                        }
                                                                    </Select>
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Document Number */}
                                                            <Grid item xs={12} sm={6} md={6} lg={3}>
                                                                <TextField
                                                                    error={client.documentNumberError}
                                                                    disabled={client.documentNumberDisabled}
                                                                    fullWidth={true}
                                                                    required
                                                                    label="Número Documento "
                                                                    type="text"
                                                                    margin="normal"
                                                                    color="default"
                                                                    name="documentNumber"
                                                                    value={client.documentNumber}
                                                                    onChange={this.handleChangeTextFieldDocumentNumber("documentNumber")}
                                                                    InputProps={{
                                                                        readOnly: client.documentNumberReadOnly,
                                                                        inputProps: {
                                                                            maxLength: client.documentTypeId === 100001 ? 8 : 9,
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <RecentActorsIcon color={client.documentNumberError ? "secondary" : "inherit"} fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                                <FormHelperText className="text-right">
                                                                    {client.documentNumber.toString().length}/{client.documentTypeId === 100001 ? 8 : 9}
                                                                </FormHelperText>
                                                            </Grid>
                                                        </Grid>
                                                        <Grid container spacing={8}>
                                                            {/* Client - First Name */}
                                                            <Grid item xs={12} sm={6} md={6} lg={3}>
                                                                <TextField
                                                                    error={client.firstNameError}
                                                                    fullWidth={true}
                                                                    required
                                                                    label="Primer Nombre"
                                                                    type="text"
                                                                    margin="normal"
                                                                    name="firstName"
                                                                    defaultValue={client.firstName}
                                                                    onKeyPress={this.handleKeyPressTextFieldOnlyText("client", "firstName")}
                                                                    onBlur={this.handleBlurTextFieldRequired("client", null, "firstName")}
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.firstNameInput,
                                                                            maxLength: defaultLengthInput,
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <Filter1Icon color={client.firstNameError ? "secondary" : "inherit"} fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Second Name */}
                                                            <Grid item xs={12} sm={6} md={6} lg={3}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    label="Segundo Nombre"
                                                                    type="text"
                                                                    margin="normal"
                                                                    color="default"
                                                                    name="secondName"
                                                                    defaultValue={client.secondName}
                                                                    onKeyPress={this.handleKeyPressTextFieldOnlyText("client", "secondName")}
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.secondNameInput,
                                                                            maxLength: defaultLengthInput,
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <Filter2Icon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Last Name */}
                                                            <Grid item xs={12} sm={6} md={6} lg={3}>
                                                                <TextField
                                                                    error={client.lastNameError}
                                                                    fullWidth={true}
                                                                    required
                                                                    label="Apellido Paterno"
                                                                    type="text"
                                                                    margin="normal"
                                                                    color="default"
                                                                    name="lastName"
                                                                    defaultValue={client.lastName}
                                                                    onKeyPress={this.handleKeyPressTextFieldOnlyText("client", "lastName")}
                                                                    onBlur={this.handleBlurTextFieldRequired("client", null, "lastName")}
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.lastNameInput,
                                                                            maxLength: defaultLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <PermIdentityIcon color={client.lastNameError ? "secondary" : "inherit"} fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Mother Last Name */}
                                                            <Grid item xs={12} sm={6} md={6} lg={3}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    label="Apellido Materno"
                                                                    type="text"
                                                                    margin="normal"
                                                                    color="default"
                                                                    name="motherLastName"
                                                                    defaultValue={client.motherLastName}
                                                                    onKeyPress={this.handleKeyPressTextFieldOnlyText("client", "motherLastName")}
                                                                    //onBlur={this.handleBlurTextFieldRequired("client", null, "motherLastName")}
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.motherLastNameInput,
                                                                            maxLength: defaultLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <PermIdentityIcon color="inherit" fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                        <Grid container spacing={8}>
                                                            {/* Client - Birthday*/}
                                                            <Grid item xs={6} sm={6} md={3} lg={3}>
                                                                <TextField
                                                                    error={client.birthdayError}
                                                                    fullWidth
                                                                    name="birthday"
                                                                    label="Fecha Nacimiento"
                                                                    type="date"
                                                                    margin="normal"
                                                                    required
                                                                    format="DD-MM-YYYY"
                                                                    value={client.birthday}
                                                                    onChange={this.handleChangeTextFieldRequired("client", null, "birthday")}
                                                                    onBlur={this.handleBlurTextFieldBirthday("client", null, "birthday")}
                                                                    onInvalid={this.handleInvalidBirthday("client", null, "birthday")}
                                                                    InputLabelProps={{
                                                                        shrink: true,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            title: 'Revisar fecha de nacimiento.'//`CDA: La edad válida es entre ${minBirthday} a ${maxBirthday} años. `,
                                                                            // min: defaultMaxBirthday,
                                                                            // max: defaultMinBirthday
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <CalendarTodayIcon color={client.birthdayError ? "secondary" : "inherit"} fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />

                                                                {
                                                                    client.birthdayError && <FormHelperText className="text-red">
                                                                        {
                                                                            'Revisar fecha de nacimiento.'
                                                                            //`CDA: La edad válida es entre ${minBirthday} a ${maxBirthday} año(s). `
                                                                        }
                                                                    </FormHelperText>
                                                                }
                                                            </Grid>
                                                            {/* Client - Gender */}
                                                            <Grid item xs={6} sm={6} md={3} lg={3}>
                                                                <FormControl fullWidth error={client.genderError} required={true} >
                                                                    <Autocomplete
                                                                        error={client.genderError}
                                                                        onChange={this.handleChangeSelectFullRequired("client", null, gender.data, "gender")}
                                                                        data={genderData}
                                                                        value={client.genderId}
                                                                        placeholder={"Género *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Marital Status*/}
                                                            <Grid item xs={6} sm={6} md={3} lg={3}>
                                                                <FormControl fullWidth error={client.maritalStatusError} required={true}>
                                                                    <Autocomplete
                                                                        error={client.maritalStatusError}
                                                                        onChange={this.handleChangeSelectFullRequired("client", null, maritalStatus.data, "maritalStatus")}
                                                                        data={maritalData}
                                                                        value={client.maritalStatusId}
                                                                        placeholder={"Estado Civil *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Nationality */}
                                                            <Grid item xs={6} sm={6} md={3} lg={3}>
                                                                <FormControl fullWidth error={client.nationalityError}>
                                                                    <Autocomplete
                                                                        //error={client.nationalityError}
                                                                        onChange={this.handleChangeSelectRequired("client", null, "nationality")}
                                                                        value={client.nationalityId}
                                                                        data={countryData}
                                                                        placeholder={"Nacionalidad *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                        </Grid>
                                                        <Grid container spacing={8}>
                                                            {/* Client - Academic Degree */}
                                                            <Grid item xs={12} sm={6} md={6} lg={3}>
                                                                <FormControl fullWidth error={client.academicDegreeError}>
                                                                    <Autocomplete
                                                                        error={client.academicDegreeError}
                                                                        onChange={this.handleChangeSelectFullRequired("client", null, academicDegree.data, "academicDegree")}
                                                                        value={client.academicDegreeId}
                                                                        data={academicDegreeData}
                                                                        placeholder={"Grado Académico *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - E-Mail */}
                                                            <Grid item xs={12} sm={6} md={6} lg={3}>
                                                                <TextField
                                                                    required
                                                                    error={client.emailError}
                                                                    fullWidth={true}
                                                                    label="Correo Electrónico"
                                                                    type="email"
                                                                    margin="normal"
                                                                    color="default"
                                                                    name="firstName"
                                                                    defaultValue={client.email}
                                                                    onBlur={this.handleBlurTextFieldCheckEmailInput("client", null, "email")}
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.emailInput,
                                                                            maxLength: defaultEmailLengthInput,
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <EmailIcon color={client.emailError ? "secondary" : "inherit"} fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Cellphone */}
                                                            <Grid item xs={6} sm={6} md={6} lg={3}>
                                                                <TextField
                                                                    error={client.cellphoneError}
                                                                    fullWidth={true}
                                                                    required
                                                                    label="Celular"
                                                                    type={ client.cellphoneValidationSMS.validate ? 'search' : 'text' }
                                                                    margin="normal"
                                                                    color="default"
                                                                    name="cellphone"
                                                                    value={client.cellphone}
                                                                    onKeyPress={this.handleKeyPressTextFieldOnlyNumber("client", "cellphone")}
                                                                    onBlur={this.handleBlurTextFieldRequired("client", null, "cellphone")}
                                                                    onChange={this.handleChangeTextField("client", null, "cellphone")}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            readOnly: client.cellphoneValidationSMS.validate && client.cellphoneValidationSMS.confirmed,
                                                                            maxLength: defaultCellphoneLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <StayPrimaryPortraitIcon color={client.cellphoneError ? "secondary" : "inherit"} fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                                <FormHelperText className="text-left">
                                                                    { client.cellphoneValidationSMS.validate && 'Presionar Enter para enviar SMS | ' }
                                                                    {client.cellphone.toString().length}/9
                                                                </FormHelperText>
                                                            </Grid>
                                                            {/* Client - Telephone */}
                                                            <Grid item xs={6} sm={6} md={6} lg={3}>
                                                                <TextField
                                                                    error={client.telephoneError}
                                                                    fullWidth={true}
                                                                    label="Teléfono Fijo"
                                                                    type="text"
                                                                    margin="normal"
                                                                    color="default"
                                                                    name="telephone"
                                                                    defaultValue={client.telephone}
                                                                    onBlur={this.handleBlurTextFieldValidateTelephoneInput("client", null, "telephone")}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.telephoneInput,
                                                                            //maxLength: 10
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <PhoneIcon color="inherit" fontSize="small" />
                                                                            </InputAdornment>
                                                                        ),
                                                                        inputComponent: TextMaskTelephone,
                                                                    }}
                                                                />
                                                                <FormHelperText>
                                                                    (Código Región) + Número Telefónico
                                                    </FormHelperText>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </ExpansionPanelDetails>
                                        </ExpansionPanel>
                                    </Grid>
                                    {/* Panel - Home Address */}
                                    <Grid item xs={12}>
                                        <ExpansionPanel className={classes.root}>
                                            <ExpansionPanelSummary
                                                className={classes.expansionPanel}
                                                expandIcon={<ExpandMoreIcon fontSize="small" />}>
                                                <Typography className={classes.heading}>
                                                    <PlaceIcon fontSize="small" className="mr-2" />
                                        Dirección Personal
                                    </Typography>
                                            </ExpansionPanelSummary>
                                            <ExpansionPanelDetails>
                                                <Grid container spacing={8}>
                                                    <Grid item xs={12}>
                                                        <Grid container spacing={8}>
                                                            {/* Client - Home Address - Department */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <FormControl fullWidth error={client.homeAddress.departmentError}>
                                                                    <Autocomplete
                                                                        loading={client.homeAddress.departmentLoading}
                                                                        disabled={client.homeAddress.departmentDisabled}
                                                                        error={client.homeAddress.departmentError}
                                                                        onChange={this.handleChangeSelectUbigeoDepartment("client", "homeAddress", "department", true)}
                                                                        value={client.homeAddress.departmentId}
                                                                        data={homeAddressDepartmentData}
                                                                        placeholder={"Departamento *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Home Address - Province */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <Autocomplete
                                                                    loading={client.homeAddress.provinceLoading}
                                                                    disabled={client.homeAddress.provinceDisabled}
                                                                    error={client.homeAddress.provinceError}
                                                                    onChange={this.handleChangeSelectUbigeoProvince("client", "homeAddress", "province", true)}
                                                                    data={homeAddressProvinceData}
                                                                    value={client.homeAddress.provinceId}
                                                                    placeholder={"Provincia *"}
                                                                />
                                                            </Grid>
                                                            {/* Client - Home Address - District */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <Autocomplete
                                                                    loading={client.homeAddress.districtLoading}
                                                                    disabled={client.homeAddress.districtDisabled}
                                                                    error={client.homeAddress.districtError}
                                                                    onChange={this.handleChangeSelectRequired("client", "homeAddress", "district")}
                                                                    value={client.homeAddress.districtId}
                                                                    data={homeAddressDistrictData}
                                                                    placeholder={"Distrito *"}
                                                                />
                                                            </Grid>
                                                            {/* Client - Home Address - Via */}
                                                            <Grid item xs={12} sm={6} md={4} lg={2}>
                                                                <FormControl fullWidth error={client.homeAddress.viaError}>
                                                                    <Autocomplete
                                                                        error={client.homeAddress.viaError}
                                                                        onChange={this.handleChangeSelectFullRequired("client", "homeAddress", via.data, "via")}
                                                                        value={client.homeAddress.viaId}
                                                                        data={homeAddressViaData}
                                                                        placeholder={"Vía *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Home Address - Name Via*/}
                                                            <Grid item xs={12} sm={12} md={8} lg={6}>
                                                                <TextField
                                                                    error={client.homeAddress.nameViaError}
                                                                    fullWidth={true}
                                                                    required
                                                                    label="Nombre Vía"
                                                                    type="text"
                                                                    margin="normal"
                                                                    color="default"
                                                                    name="nameVia"
                                                                    defaultValue={client.homeAddress.nameVia}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("homeAddress", "nameVia")}
                                                                    onBlur={this.handleBlurTextFieldRequired("client", "homeAddress", "nameVia")}
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.homeAddressNameViaInput,
                                                                            maxLength: defaultViaLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <PlaceIcon color={client.homeAddress.nameViaError ? "secondary" : "inherit"} fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                        <Grid container spacing={8}>
                                                            {/* Client - Home Address - Number */}
                                                            <Grid item xs={6} sm={4} md={4} lg={2}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    label="Número"
                                                                    type="text"
                                                                    name="number"
                                                                    defaultValue={client.homeAddress.number}
                                                                    onKeyPress={this.handleKeyPressTextFieldOnlyNumber("homeAddress", "number")}
                                                                    margin="normal"
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.homeAddressNumberInput,
                                                                            maxLength: defaultComplementaryLengthInput,
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <Filter9PlusIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Home Address - Department */}
                                                            <Grid item xs={6} sm={4} md={4} lg={2}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    label="Departamento"
                                                                    name="building"
                                                                    defaultValue={client.homeAddress.building}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("homeAddress", "building")}
                                                                    type="text"
                                                                    margin="normal"
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.homeAddressBuildingInput,
                                                                            maxLength: defaultComplementaryLengthInput,
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <HomeIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Home Address - Inside */}
                                                            <Grid item xs={6} sm={4} md={4} lg={2}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    label="Interior"
                                                                    name="inside"
                                                                    defaultValue={client.homeAddress.inside}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("homeAddress", "inside")}
                                                                    type="text"
                                                                    margin="normal"
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.homeAddressInsideInput,
                                                                            maxLength: defaultComplementaryLengthInput,
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <LastPageIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Home Address - MZ */}
                                                            <Grid item xs={6} sm={4} md={4} lg={2}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    label="Mz"
                                                                    name="mz"
                                                                    defaultValue={client.homeAddress.mz}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("homeAddress", "mz")}
                                                                    type="text"
                                                                    margin="normal"
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.homeAddressMzInput,
                                                                            maxLength: defaultComplementaryLengthInput,
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <CropDinIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Home Address - Lot */}
                                                            <Grid item xs={6} sm={4} md={4} lg={2}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    label="Lote"
                                                                    name="mz"
                                                                    defaultValue={client.homeAddress.lot}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("homeAddress", "lot")}
                                                                    type="text"
                                                                    margin="normal"
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.homeAddressLotInput,
                                                                            maxLength: defaultComplementaryLengthInput,
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <TabUnselectedIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                        <Grid container spacing={8}>
                                                            {/* Client - Home Address - Zone */}
                                                            <Grid item xs={12} sm={4} md={4} lg={2}>
                                                                <FormControl fullWidth error={client.homeAddress.zoneError}>
                                                                    <Autocomplete
                                                                        error={client.homeAddress.zoneError}
                                                                        onChange={this.handleChangeSelectZone("client", "homeAddress", zone.data, "zone")}
                                                                        data={homeAddressZoneData}
                                                                        value={client.homeAddress.zoneId}
                                                                        placeholder={!client.homeAddress.nameZoneDisabled ? "Zona *" : "Zona"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Home Address - Name Zone */}
                                                            <Grid item xs={12} sm={8} md={8} lg={6}>
                                                                <TextField
                                                                    error={client.homeAddress.nameZoneError}
                                                                    fullWidth={true}
                                                                    required={!client.homeAddress.nameZoneDisabled}
                                                                    disabled={client.homeAddress.nameZoneDisabled}
                                                                    label="Nombre Zona"
                                                                    type="text"
                                                                    margin="normal"
                                                                    color="default"
                                                                    name="nameZone"
                                                                    value={client.homeAddress.nameZone}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("client", "nameZone")}
                                                                    onBlur={this.handleBlurTextFieldRequired("client", "homeAddress", "nameZone")}
                                                                    onChange={this.handleChangeTextFieldRequired("client", "homeAddress", "nameZone")}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.homeAddressNameZoneInput,
                                                                            //maxLength: defaultLengthInput
                                                                            maxLength: 100
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <NearMeIcon fontSize="small" color={client.homeAddress.nameZoneError ? "secondary" : "inherit"} />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Home Address - Housing Type */}
                                                            <Grid item xs={12} sm={12} md={4} lg={4}>
                                                                <FormControl fullWidth error={client.homeAddress.housingTypeError}>
                                                                    <Autocomplete
                                                                        error={client.homeAddress.housingTypeError}
                                                                        onChange={this.handleChangeSelectFullRequired("client", "homeAddress", housingType.data, "housingType")}
                                                                        value={client.homeAddress.housingTypeId}
                                                                        data={homeAddressHousingTypeData}
                                                                        placeholder={"Tipo Vivienda *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Home Address - Reference */}
                                                            <Grid item xs={12} sm={12} md={8} lg={12}>
                                                                <TextField
                                                                    error={ client.homeAddress.referenceError }
                                                                    fullWidth={true}
                                                                    label="Referencia del Domicilio"
                                                                    multiline
                                                                    rows="1"
                                                                    rowsMax="3"
                                                                    name="reference"
                                                                    value={client.homeAddress.reference}
                                                                    onChange={this.handleChangeOnlyTextAndNumberField("client", "homeAddress", "reference")}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("homeAddress", "reference")}
                                                                    type="text"
                                                                    margin="normal"
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            maxLength: defaultReferenceLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <LocalLibraryIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                                <FormHelperText
                                                                    className={ 'd-flex justify-content-between small' } >
                                                                        <FormHelperText
                                                                            component='span'
                                                                            className={ client.homeAddress.referenceError
                                                                                ? 'text-red' : 'inherit' }>
                                                                                { client.homeAddress.referenceError && 'Solo es permitido caracteres alfanuméricos (A-Z;0-9).' }
                                                                        </FormHelperText>
                                                                        <FormHelperText
                                                                            component='span'
                                                                            className={ client.homeAddress.referenceError 
                                                                                ? 'text-red' : 'inherit' }>
                                                                                { client.homeAddress.reference && client.homeAddress.reference.length }/{ defaultReferenceLengthInput }
                                                                        </FormHelperText>
                                                                </FormHelperText>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </ExpansionPanelDetails>
                                        </ExpansionPanel>
                                    </Grid>
                                    {/* Panel - Working Information */}
                                    <Grid item xs={12}>
                                        <ExpansionPanel className={classes.root}>
                                            <ExpansionPanelSummary
                                                className={classes.expansionPanel}
                                                expandIcon={<ExpandMoreIcon fontSize="small" />}>
                                                <Typography className={classes.heading}>
                                                    <WorkIcon fontSize="small" className="mr-2" />
                                        Información Laboral
                                    </Typography>
                                            </ExpansionPanelSummary>
                                            <ExpansionPanelDetails>
                                                <Grid container spacing={8}>
                                                    {/* Client - Employment Situation */}
                                                    <Grid item xs={12} sm={6} md={4} lg={3}>
                                                        <FormControl fullWidth error={client.employmentSituationError}>
                                                            <Autocomplete
                                                                error={client.employmentSituationError}
                                                                onChange={this.handleChangeSelectEmploymentSituation("employmentSituation")}
                                                                //onChange={this.handleChangeSelectFullRequired("client", null, employmentSituation.data, "employmentSituation")}
                                                                value={client.employmentSituationId}
                                                                data={employmentSituationData}
                                                                placeholder={"Situación Laboral *"}
                                                            />
                                                        </FormControl>
                                                    </Grid>
                                                    {/* Client - Job Title */}
                                                    <Grid item xs={12} sm={6} md={4} lg={3}>
                                                        <FormControl fullWidth error={client.jobTitleError}>
                                                            <Autocomplete
                                                                disabled={client.jobTitleDisabled}
                                                                error={client.jobTitleError}
                                                                onChange={this.handleChangeSelectRequired("client", null, "jobTitle")}
                                                                value={client.jobTitleId}
                                                                data={jobTitleData}
                                                                placeholder={"Cargo Profesión *"}
                                                            />
                                                        </FormControl>
                                                    </Grid>
                                                    {/* Client - Economic Activity */}
                                                    <Grid item xs={12} sm={6} md={4} lg={3}>
                                                        <FormControl fullWidth error={client.economicActivityError}>
                                                            <Autocomplete
                                                                disabled={client.economicActivityDisabled}
                                                                error={client.economicActivityError}
                                                                onChange={this.handleChangeSelectRequired("client", null, "economicActivity")}
                                                                value={client.economicActivityId}
                                                                data={economicActivityData}
                                                                placeholder={"Actividad Económica *"}
                                                            />
                                                        </FormControl>
                                                    </Grid>
                                                    {/* Client - Labor Income Date*/}
                                                    <Grid item xs={12} sm={6} md={4} lg={3}>
                                                        <TextField
                                                            required
                                                            error={client.laborIncomeDateError}
                                                            fullWidth={true}
                                                            label="Fecha de Ingreso"
                                                            type="date"
                                                            margin="normal"
                                                            format="DD-MM-YYYY"
                                                            name="laborIncome"
                                                            value={client.laborIncomeDate}
                                                            onChange={this.handleChangeTextFieldRequired("client", null, "laborIncomeDate")}
                                                            onBlur={this.handleBlurTextFieldLaborIncomeDate("client", null, "laborIncomeDate")}
                                                            onInvalid={this.handleInvalidTextFieldLaborIncomeDate("client", null, "laborIncomeDate")}
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            InputProps={{
                                                                inputProps: {
                                                                    title: `CDA: Antigüedad válida es entre ${minLaborIncomeDate} a ${maxLaborIncomeDate} año(s). `,
                                                                    // min: defaultMaxLaborIncomeDate,
                                                                    // max: defaultMinLaborIncomeDate
                                                                },
                                                                endAdornment: (
                                                                    <InputAdornment position="end">
                                                                        <CalendarTodayIcon color={client.laborIncomeDateError ? "secondary" : "inherit"} fontSize="small" />
                                                                    </InputAdornment>
                                                                )
                                                            }}
                                                        />
                                                        {
                                                            client.laborIncomeDateError && <FormHelperText className="text-red">
                                                                {
                                                                    `CDA: Antigüedad válida es entre ${minLaborIncomeDate} a ${maxLaborIncomeDate} año(s). `
                                                                }
                                                            </FormHelperText>
                                                        }
                                                    </Grid>
                                                    {/* Client - Option Business */}
                                                    <Grid item xs={12} sm={6} md={4} lg={3}>
                                                        <FormControl fullWidth error={client.businessOptionError}>
                                                            <Autocomplete
                                                                error={client.businessOptionError}
                                                                onChange={this.handleChangeSelectBusinessOption("client", null, "businessOption")}
                                                                //onChange={this.handleChangeSelectFullRequired("client", null, businessOption.data, "businessOption")}
                                                                value={client.businessOption}
                                                                data={businessOptionData}
                                                                placeholder={"Seleccionar RUC | Razón Social *"}
                                                            />
                                                        </FormControl>
                                                    </Grid>
                                                    {/* Client - Business */}
                                                    <Grid item xs={12} sm={6} md={4} lg={3}>
                                                        <div style={{ display: 'flex', position: 'relative' }}>
                                                            <div style={{ width: 'calc(100% - 4.5em)' }}>
                                                                {
                                                                    client.businessOption === "0" ?
                                                                        <React.Fragment>
                                                                            <TextField
                                                                                error={client.rucError}
                                                                                required={client.rucRequired}
                                                                                disabled={client.rucDisabled}
                                                                                fullWidth={true}
                                                                                label="Búsqueda RUC"
                                                                                type="text"
                                                                                margin="normal"
                                                                                color="default"
                                                                                name="ruc"
                                                                                value={client.ruc}
                                                                                onKeyPress={this.handleKeyPressTextFieldOnlyNumber("client", "ruc")}
                                                                                onChange={this.handleChangeTextFieldRUC("client", null, "ruc")}
                                                                                onBlur={this.handleBlurTextFieldRequired("client", null, "businessName")}
                                                                                onKeyUp={this.handleKeyUpTextFieldSearchBusiness(true)}
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: 11
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment position="end">
                                                                                            <BusinessIcon color={client.rucError ? "secondary" : "inherit"} fontSize="small" />
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <FormHelperText
                                                                                className={classNames("d-flex justify-content-between small")} >
                                                                                <FormHelperText component="span" className={client.rucError ? "text-red" : "inherit"}>
                                                                                    {this.props.businessData.filteredBusiness.error}
                                                                                </FormHelperText>
                                                                                <FormHelperText component="span" className={client.rucError ? "text-red" : "inherit"}>
                                                                                    {client.ruc.toString().length}/11
                                                        </FormHelperText>
                                                                            </FormHelperText>
                                                                        </React.Fragment>
                                                                        :
                                                                        <React.Fragment>
                                                                            <TextField
                                                                                error={client.businessNameError}
                                                                                required={client.businessNameRequired}
                                                                                disabled={client.businessNameDisabled}
                                                                                fullWidth={true}
                                                                                label="Búsqueda Razón Social"
                                                                                type="text"
                                                                                margin="normal"
                                                                                color="default"
                                                                                value={client.businessName}
                                                                                onKeyPress={this.handleKeyPressTextFieldOnlyText("client", "ruc")}
                                                                                onChange={this.handleChangeTextFieldRequired("client", null, "businessName")}
                                                                                onBlur={this.handleBlurTextFieldRequired("client", null, "businessName")}
                                                                                onKeyUp={this.handleKeyUpTextFieldSearchBusiness(false)}
                                                                                // onClick={''}
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        maxLength: 10000,
                                                                                    },
                                                                                    endAdornment: (
                                                                                        <InputAdornment position="end">
                                                                                            <BusinessIcon color={client.businessNameError ? "secondary" : "inherit"} fontSize="small" />
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                            <FormHelperText component="span" className={client.businessNameError ? "text-red" : "text-red"}>
                                                                                {this.props.businessData.filteredBusiness.error}
                                                                            </FormHelperText>
                                                                        </React.Fragment>
                                                                }
                                                            </div>
                                                            <div style={{ width: '2em', position: 'absolute', bottom: '.5em', right: '2em' }}>
                                                                <RegisterModalButton title={false} />
                                                            </div>
                                                        </div>
                                                        <Popper
                                                            style={{
                                                                zIndex: 900, position: "absolute",
                                                                maxHeight: 48 * 4.5,
                                                                minWidth: 320,
                                                                maxWidth: 400
                                                            }}
                                                            anchorEl={this.state.anchorEl}
                                                            aria-haspopup="true"
                                                            open={this.state.open} transition disablePortal>
                                                            {({ TransitionProps, placement }) => (
                                                                <Grow
                                                                    {...TransitionProps}
                                                                    id="menu-list-grow"
                                                                    style={{
                                                                        transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',

                                                                    }}
                                                                >
                                                                    <Paper>
                                                                        <ClickAwayListener onClickAway={this.handleCloseModalBusiness("business")}>
                                                                            <MenuList className={classes.menuItem}>
                                                                                {
                                                                                    businessData.filteredBusiness.data.map((item, index) => (
                                                                                        <MenuItem
                                                                                            key={index}
                                                                                            onClick={this.handleClickMenuBusiness(item)}>
                                                                                            <Typography noWrap>
                                                                                                {item.nom_emp}
                                                                                            </Typography>
                                                                                        </MenuItem>
                                                                                    ))
                                                                                }
                                                                            </MenuList>
                                                                        </ClickAwayListener>
                                                                    </Paper>
                                                                </Grow>
                                                            )}
                                                        </Popper>

                                                    </Grid>
                                                    {/**Create Ruc */}
                                                    {/* Client - Gross Income */}
                                                    <Grid item xs={12} sm={6} md={4} lg={3}>
                                                        <TextField
                                                            error={client.grossIncomeError}
                                                            fullWidth={true}
                                                            required
                                                            label="Ingreso Bruto (S/)"
                                                            //type="number"
                                                            margin="normal"
                                                            color="default"
                                                            name="grossIncome"
                                                            defaultValue={client.grossIncomeCall}
                                                            placeholder="0.00"
                                                            //onBlur={this.handleBlurTextFieldRequired("client", null, "grossIncome")}
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            InputProps={{
                                                                inputProps: {
                                                                    ref: this.grossIncomeInput,
                                                                    // step:"0.01",
                                                                    // min:"0",
                                                                    // max:"100000",
                                                                    maxLength: 10
                                                                },
                                                                endAdornment: (
                                                                    <InputAdornment position="end">
                                                                        <MoneyIcon color={client.grossIncomeError ? "secondary" : "inherit"} fontSize="small" />
                                                                    </InputAdornment>
                                                                ),
                                                                inputComponent: NumberMaskMoney,
                                                            }}
                                                        />
                                                    </Grid>
                                                    {/* Client - Work Telephone */}
                                                    <Grid item xs={6} sm={6} md={4} lg={3}>
                                                        <TextField
                                                            error={client.workTelephoneError}
                                                            fullWidth={true}
                                                            label="Teléfono"
                                                            type="text"
                                                            margin="normal"
                                                            color="default"
                                                            name="workTelephone"
                                                            defaultValue={client.workTelephone}
                                                            onBlur={this.handleBlurTextFieldValidateTelephoneInput("client", null, "workTelephone")}
                                                            InputProps={{
                                                                inputProps: {
                                                                    ref: this.workTelephoneInput,
                                                                    //maxLength: 10
                                                                },
                                                                endAdornment: (
                                                                    <InputAdornment position="end">
                                                                        <PhoneIcon color="inherit" fontSize="small" />
                                                                    </InputAdornment>
                                                                ),
                                                                inputComponent: TextMaskTelephone,
                                                            }}
                                                        />
                                                        <FormHelperText>
                                                            (Código Región) + Número Telefónico
                                            </FormHelperText>
                                                    </Grid>
                                                    {/* Client - Work Telephone Annex */}
                                                    <Grid item xs={6} sm={6} md={4} lg={3}>
                                                        <TextField
                                                            fullWidth={true}
                                                            label="Anexo Telefónico"
                                                            type="text"
                                                            margin="normal"
                                                            color="default"
                                                            name="workTelephoneExtension"
                                                            defaultValue={client.workTelephoneExtension}
                                                            onKeyPress={this.handleKeyPressTextFieldOnlyNumber("client", "workTelephoneExtension")}

                                                            InputProps={{
                                                                inputProps: {
                                                                    ref: this.workTelephoneExtensionInput,
                                                                    maxLength: defaultTelephoneExtensionLengthInput
                                                                },
                                                                endAdornment: (
                                                                    <InputAdornment position="end">
                                                                        <DialpadIcon fontSize="small" />
                                                                    </InputAdornment>
                                                                )
                                                            }}
                                                        />
                                                    </Grid>
                                                </Grid>
                                            </ExpansionPanelDetails>
                                        </ExpansionPanel>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <ExpansionPanel className={classes.root}>
                                            <ExpansionPanelSummary
                                                className={classes.expansionPanel}
                                                expandIcon={<ExpandMoreIcon fontSize="small" />}>
                                                <Typography className={classes.heading}>
                                                    <BusinessIcon fontSize="small" className="mr-2" />
                                        Dirección Laboral
                                    </Typography>
                                            </ExpansionPanelSummary>
                                            <ExpansionPanelDetails>
                                                <Grid container spacing={8}>
                                                    <Grid item xs={12}>
                                                        <Grid container spacing={8}>
                                                            {/* Client - Work Address - Department */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <FormControl fullWidth error={client.workAddress.departmentError}>
                                                                    <Autocomplete
                                                                        loading={client.workAddress.departmentLoading}
                                                                        disabled={client.workAddress.departmentDisabled}
                                                                        error={client.workAddress.departmentError}
                                                                        onChange={this.handleChangeSelectUbigeoDepartment("client", "workAddress", "department", false)}
                                                                        value={client.workAddress.departmentId}
                                                                        data={workAddressDepartmentData}
                                                                        placeholder={"Departamento *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Work Address - Province */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <Autocomplete
                                                                    loading={client.workAddress.provinceLoading}
                                                                    disabled={client.workAddress.provinceDisabled}
                                                                    error={client.workAddress.provinceError}
                                                                    onChange={this.handleChangeSelectUbigeoProvince("client", "workAddress", "province", false)}
                                                                    data={workAddressProvinceData}
                                                                    value={client.workAddress.provinceId}
                                                                    placeholder={"Provincia *"}
                                                                />
                                                            </Grid>
                                                            {/* Client - Work Address - District */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <Autocomplete
                                                                    loading={client.workAddress.districtLoading}
                                                                    disabled={client.workAddress.districtDisabled}
                                                                    error={client.workAddress.districtError}
                                                                    onChange={this.handleChangeSelectRequired("client", "workAddress", "district")}
                                                                    value={client.workAddress.districtId}
                                                                    data={workAddressDistrictData}
                                                                    placeholder={"Distrito *"}
                                                                />
                                                            </Grid>
                                                            {/* Client - Work Address - Via */}
                                                            <Grid item xs={12} sm={6} md={4} lg={2}>
                                                                <FormControl fullWidth error={client.workAddress.viaError}>
                                                                    <Autocomplete
                                                                        error={client.workAddress.viaError}
                                                                        onChange={this.handleChangeSelectFullRequired("client", "workAddress", via.data, "via")}
                                                                        value={client.workAddress.viaId}
                                                                        data={workAddressViaData}
                                                                        placeholder={"Vía *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Work Address - Name Via*/}
                                                            <Grid item xs={12} sm={12} md={8} lg={6}>
                                                                <TextField
                                                                    error={client.workAddress.nameViaError}
                                                                    fullWidth={true}
                                                                    required
                                                                    label="Nombre Vía"
                                                                    type="text"
                                                                    margin="normal"
                                                                    color="default"
                                                                    name="nameVia"
                                                                    defaultValue={client.workAddress.nameVia}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("workAddress", "nameVia")}
                                                                    onBlur={this.handleBlurTextFieldRequired("client", "workAddress", "nameVia")}
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.workAddressNameViaInput,
                                                                            maxLength: defaultViaLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <PlaceIcon color={client.workAddress.nameViaError ? "secondary" : "inherit"} fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                        <Grid container spacing={8}>
                                                            {/* Client - Work Address - Number */}
                                                            <Grid item xs={6} sm={4} md={4} lg={2}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    label="Número"
                                                                    name="number"
                                                                    defaultValue={client.workAddress.number}
                                                                    onKeyPress={this.handleKeyPressTextFieldOnlyNumber("workAddress", "number")}
                                                                    type="text"
                                                                    margin="normal"
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.workAddressNumberInput,
                                                                            maxLength: defaultComplementaryLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <Filter9PlusIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Work Address - Department */}
                                                            <Grid item xs={6} sm={4} md={4} lg={2}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    label="Departamento"
                                                                    name="building"
                                                                    defaultValue={client.workAddress.building}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("workAddress", "building")}
                                                                    type="text"
                                                                    margin="normal"
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.workAddressBuildingInput,
                                                                            maxLength: defaultComplementaryLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <HomeIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Work Address - Inside */}
                                                            <Grid item xs={6} sm={4} md={4} lg={2}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    label="Interior"
                                                                    name="inside"
                                                                    defaultValue={client.workAddress.inside}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("workAddress", "inside")}
                                                                    type="text"
                                                                    margin="normal"
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.workAddressInsideInput,
                                                                            maxLength: defaultComplementaryLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <LastPageIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Work Address - MZ */}
                                                            <Grid item xs={6} sm={4} md={4} lg={2}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    label="Mz"
                                                                    name="mz"
                                                                    defaultValue={client.workAddress.mz}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("workAddress", "mz")}
                                                                    type="text"
                                                                    margin="normal"
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.workAddressMzInput,
                                                                            maxLength: defaultComplementaryLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <CropDinIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Work Address - Lot */}
                                                            <Grid item xs={6} sm={4} md={4} lg={2}>
                                                                <TextField
                                                                    fullWidth={true}
                                                                    id="clientLot"
                                                                    label="Lote"
                                                                    name="lot"
                                                                    defaultValue={client.workAddress.lot}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("workAddress", "lot")}
                                                                    type="text"
                                                                    margin="normal"
                                                                    InputLabelProps={{
                                                                        shrink: this.state.shrink,
                                                                    }}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.workAddressLotInput,
                                                                            maxLength: defaultComplementaryLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <TabUnselectedIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                        <Grid container spacing={8}>
                                                            {/* Client - Work Address - Zone */}
                                                            <Grid item xs={12} sm={4} md={4} lg={2}>
                                                                <FormControl fullWidth error={client.workAddress.zoneError}>
                                                                    <Autocomplete
                                                                        error={client.workAddress.zoneError}
                                                                        onChange={this.handleChangeSelectZone("client", "workAddress", zone.data, "zone")}
                                                                        data={workAddressZoneData}
                                                                        value={client.workAddress.zoneId}
                                                                        placeholder={!client.workAddress.nameZoneDisabled ? "Zona *" : "Zona"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Work Address - Name Zone */}
                                                            <Grid item xs={12} sm={8} md={8} lg={6}>
                                                                <TextField
                                                                    error={client.workAddress.nameZoneError}
                                                                    fullWidth={true}
                                                                    required={!client.workAddress.nameZoneDisabled}
                                                                    disabled={client.workAddress.nameZoneDisabled}
                                                                    label="Nombre Zona"
                                                                    type="text"
                                                                    margin="normal"
                                                                    color="default"
                                                                    name="nameZone"
                                                                    value={client.workAddress.nameZone}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("client", "nameZone")}
                                                                    onBlur={this.handleBlurTextFieldRequired("client", "workAddress", "nameZone")}
                                                                    onChange={this.handleChangeTextFieldRequired("client", "workAddress", "nameZone")}
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.workAddressNameZoneInput,
                                                                            maxLength: defaultReferenceLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <NearMeIcon fontSize="small" color={client.workAddress.nameZoneError ? "secondary" : "inherit"} />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                            {/* Client - Work Address - Reference */}
                                                            <Grid item xs={12} sm={12} md={12} lg={12}>
                                                                <TextField
                                                                    error={ client.workAddress.referenceError }
                                                                    fullWidth={true}
                                                                    label="Referencia Dir. Laboral"
                                                                    multiline
                                                                    rows="1"
                                                                    rowsMax="3"
                                                                    name="reference"
                                                                    value={client.workAddress.reference}
                                                                    onChange={this.handleChangeOnlyTextAndNumberField("client", "workAddress", "reference")}
                                                                    onKeyPress={this.handleKeyPressTextFieldCheckInput("workAddress", "reference")}
                                                                    type="text"
                                                                    margin="normal"
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            maxLength: defaultReferenceLengthInput
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <LocalLibraryIcon fontSize="small" />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                                <FormHelperText
                                                                    className={ 'd-flex justify-content-between small' } >
                                                                        <FormHelperText
                                                                            component='span'
                                                                            className={ client.workAddress.referenceError
                                                                                ? 'text-red' : 'inherit' }>
                                                                                { client.workAddress.referenceError && 'Solo es permitido caracteres alfanuméricos (A-Z;0-9).' }
                                                                        </FormHelperText>
                                                                        <FormHelperText
                                                                            component='span'
                                                                            className={ client.workAddress.referenceError 
                                                                                ? 'text-red' : 'inherit' }>
                                                                                { client.workAddress.reference && client.workAddress.reference.length }/{ defaultReferenceLengthInput }
                                                                        </FormHelperText>
                                                                </FormHelperText>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </ExpansionPanelDetails>
                                        </ExpansionPanel>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <ExpansionPanel className={classes.root}>
                                            <ExpansionPanelSummary
                                                className={classes.expansionPanel}
                                                expandIcon={<ExpandMoreIcon fontSize="small" />}>
                                                <Typography className={classes.heading}>
                                                    <LocalOfferIcon fontSize="small" className="mr-2" />
                                        Beneficios y Funcionalidades
                                    </Typography>
                                            </ExpansionPanelSummary>
                                            <ExpansionPanelDetails>
                                                <Grid container spacing={8}>
                                                    <Grid item xs={12}>
                                                        <Grid container spacing={8}>
                                                            {/* Client - Extra - Payment Date */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <FormControl fullWidth error={client.extra.paymentDateError}>
                                                                    <Autocomplete
                                                                        error={client.extra.paymentDateError}
                                                                        onChange={this.handleChangeSelectFullRequired("client", "extra", paymentDate.data, "paymentDate")}
                                                                        value={client.extra.paymentDateId}
                                                                        data={extraPaymentDateData}
                                                                        placeholder={"Fecha de Pago *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Extra - Send Correspondence */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <FormControl fullWidth error={client.extra.sendCorrespondenceError}>
                                                                    <Autocomplete
                                                                        error={client.extra.sendCorrespondenceError}
                                                                        onChange={this.handleChangeSelectRequired("client", "extra", "sendCorrespondence")}
                                                                        value={client.extra.sendCorrespondenceId}
                                                                        data={extraSendCorrespondenceData}
                                                                        placeholder={"Envío Correspondencia *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Extra - Account Status */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <FormControl fullWidth error={client.extra.accountStatusError}>
                                                                    <Autocomplete
                                                                        error={client.extra.accountStatusError}
                                                                        onChange={this.handleChangeSelectRequired("client", "extra", "accountStatus")}
                                                                        value={client.extra.accountStatusId}
                                                                        data={extraAccountStatusData}
                                                                        placeholder={"Estado de Cuenta *"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Term */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <FormControl error={client.extra.termError} component="fieldset">
                                                                    <FormLabel component="legend">Uso de Datos Personales.</FormLabel>
                                                                    <FormControlLabel
                                                                        control={
                                                                            <Checkbox
                                                                                name="term"
                                                                                checked={client.extra.term}
                                                                                onChange={this.handleChangeCheckBoxRequired("client", "extra", "term")}
                                                                                color="primary"
                                                                            />
                                                                        }
                                                                        label="Autorización del uso de datos personales."
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            {/* Client - Extra - Scanner Code */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <TextField
                                                                    error={client.extra.scannerCodeError}
                                                                    fullWidth={true}
                                                                    required
                                                                    label="Código de Escáner / DNI / Usuario de Red"
                                                                    name="scannerCode"
                                                                    onKeyPress={this.handleKeyPressSearchScannerCode("extra", "scannerCode")}
                                                                    onBlur={this.handleBlurTextFieldRequired("client", "extra", "scannerCode")}
                                                                    type="search"
                                                                    margin="normal"
                                                                    InputProps={{
                                                                        inputProps: {
                                                                            ref: this.scannerCodeInput,
                                                                            maxLength: 20,
                                                                        },
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <CodeIcon fontSize="small" color={client.extra.scannerCodeError ? "secondary" : "inherit"} />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                                <FormHelperText component="span" className="text-red">
                                                                    {
                                                                        this.props.promoter.error ?
                                                                            <FormHelperText component="span" className="text-red">
                                                                                {this.props.promoter.error}
                                                                            </FormHelperText>
                                                                            :
                                                                            <FormHelperText component="span">
                                                                                Presionar Enter para realizar la búsqueda
                                                        </FormHelperText>
                                                                    }
                                                                </FormHelperText>
                                                            </Grid>
                                                            {/* Client - Extra - Promoter Name */}
                                                            <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                <TextField
                                                                    error={client.extra.scannerCodeError}
                                                                    fullWidth={true}
                                                                    required
                                                                    label="Nombre Promotor"
                                                                    value={client.extra.promoterName || ''}
                                                                    type="text"
                                                                    margin="normal"
                                                                    placeholder="Promotor..."
                                                                    InputLabelProps={{
                                                                        shrink: true,
                                                                    }}
                                                                    InputProps={{
                                                                        readOnly: true,
                                                                        endAdornment: (
                                                                            <InputAdornment position="end">
                                                                                <PermIdentityIcon fontSize="small" color={client.extra.scannerCodeError ? "secondary" : "inherit"} />
                                                                            </InputAdornment>
                                                                        )
                                                                    }}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </ExpansionPanelDetails>
                                        </ExpansionPanel>
                                    </Grid>
                                    {
                                        !this.props.isSAEProcess ?
                                            <Grid item xs={12}>
                                                <ExpansionPanel className={classes.root}>
                                                    <ExpansionPanelSummary
                                                        className={classes.expansionPanel}
                                                        expandIcon={<ExpandMoreIcon fontSize="small" />}>
                                                        <Typography className={classes.heading}>
                                                            <HelpIcon fontSize="small" className="mr-2" />
                                        Preguntas Adicionales Tarjeta de Crédito
                                    </Typography>
                                                    </ExpansionPanelSummary>
                                                    <ExpansionPanelDetails>
                                                        <Grid container spacing={8}>
                                                            <Grid item xs={12}>
                                                                <Grid container spacing={8}>
                                                                    {/* Client - Extra - Effective Withdrawal */}
                                                                    <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                        <FormControl
                                                                            error={client.extra.internetConsumeAdviceError}
                                                                            required
                                                                            margin="normal"
                                                                            fullWidth={true}
                                                                            color="primary"
                                                                            component="fieldset">
                                                                            <FormLabel
                                                                                component="legend">
                                                                                ¿Desea habilitar la opción de consumos por internet?
                                                        </FormLabel>
                                                                            <RadioGroup
                                                                                row
                                                                                className="d-flex justify-content-between justify-content-md-start"
                                                                                aria-label="internetConsumeAdvice"
                                                                                name="internetConsumeAdvice"
                                                                                onChange={this.handleChangeRadioButtonRequired("client", "extra", "internetConsumeAdvice")}
                                                                                value={client.extra.internetConsumeAdvice}>
                                                                                <FormControlLabel
                                                                                    value={"1"}
                                                                                    control={<Radio className="d-flex" color="primary" />}
                                                                                    label="Si" />
                                                                                <FormControlLabel
                                                                                    value={"0"}
                                                                                    control={
                                                                                        <Radio className="d-flex" color="primary" />}
                                                                                    label="No"
                                                                                />
                                                                            </RadioGroup>
                                                                        </FormControl>
                                                                    </Grid>
                                                                    {/* Client - Extra - Alert */}
                                                                    <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                        <FormControl
                                                                            required
                                                                            error={client.extra.foreignConsumeAdviceError}
                                                                            margin="normal"
                                                                            fullWidth={true}
                                                                            color="primary"
                                                                            component="fieldset">
                                                                            <FormLabel
                                                                                component="legend">
                                                                                ¿Desea habilitar la opción de consumos en el extranjero?
                                                        </FormLabel>
                                                                            <RadioGroup
                                                                                row
                                                                                className="d-flex justify-content-between justify-content-md-start"
                                                                                aria-label="foreignConsumeAdvice"
                                                                                name="foreignConsumeAdvice"
                                                                                onChange={this.handleChangeRadioButtonRequired("client", "extra", "foreignConsumeAdvice")}
                                                                                value={this.state.client.extra.foreignConsumeAdvice}>
                                                                                <FormControlLabel
                                                                                    value={"1"}
                                                                                    control={<Radio className="d-flex" color="primary" />}
                                                                                    label="Si" />

                                                                                <FormControlLabel
                                                                                    value={"0"}
                                                                                    control={<Radio className="d-flex" color="primary" />}
                                                                                    label="No"
                                                                                />
                                                                            </RadioGroup>
                                                                        </FormControl>
                                                                    </Grid>
                                                                    {/* Client - Extra - Send Common Promotion */}
                                                                    <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                        <FormControl
                                                                            error={client.extra.effectiveDispositionError}
                                                                            required
                                                                            margin="normal"
                                                                            fullWidth={true}
                                                                            color="primary"
                                                                            component="fieldset">
                                                                            <FormLabel
                                                                                component="legend">
                                                                                ¿Desea contar con la posibilidad de retirar efectivo con cargo a su Tarjeta Cencosud?
                                                        </FormLabel>
                                                                            <RadioGroup
                                                                                row
                                                                                className="d-flex justify-content-between justify-content-md-start"
                                                                                aria-label="effectiveDisposition"
                                                                                name="effectiveDisposition"
                                                                                onChange={this.handleChangeRadioButtonRequired("client", "extra", "effectiveDisposition")}
                                                                                value={client.extra.effectiveDisposition}>
                                                                                <FormControlLabel
                                                                                    value={"1"}
                                                                                    control={<Radio className="d-flex" color="primary" />}
                                                                                    label="Si" />
                                                                                <FormControlLabel
                                                                                    value={"0"}
                                                                                    control={<Radio className="d-flex" color="primary" />}
                                                                                    label="No"
                                                                                />
                                                                            </RadioGroup>
                                                                        </FormControl>
                                                                    </Grid>
                                                                    <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                        <FormControl
                                                                            error={client.extra.overflippingError}
                                                                            required
                                                                            margin="normal"
                                                                            fullWidth={true}
                                                                            color="primary"
                                                                            component="fieldset">
                                                                            <FormLabel
                                                                                component="legend">
                                                                                ¿Desea contar con la posibilidad de sobregirar la línea de crédito de su Tarjeta Cencosud?
                                                        </FormLabel>
                                                                            <RadioGroup
                                                                                row
                                                                                className="d-flex justify-content-between justify-content-md-start"
                                                                                aria-label="overflipping"
                                                                                name="overflipping"
                                                                                onChange={this.handleChangeRadioButtonRequired("client", "extra", "overflipping")}
                                                                                value={client.extra.overflipping}>
                                                                                <FormControlLabel
                                                                                    value={"1"}
                                                                                    control={<Radio className="d-flex" color="primary" />}
                                                                                    label="Si" />
                                                                                <FormControlLabel
                                                                                    value={"0"}
                                                                                    control={<Radio className="d-flex" color="primary" />}
                                                                                    label="No"
                                                                                />
                                                                            </RadioGroup>
                                                                        </FormControl>
                                                                    </Grid>
                                                                    <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                        <FormControl
                                                                            error={client.extra.mailAlertsError}
                                                                            required
                                                                            margin="normal"
                                                                            fullWidth={true}
                                                                            color="primary"
                                                                            component="fieldset">
                                                                            <FormLabel
                                                                                component="legend">
                                                                                ¿Desea recibir notificaciones de consumo y/o retiros en efectivo por email?
                                                        </FormLabel>
                                                                            <RadioGroup
                                                                                row
                                                                                className="d-flex justify-content-between justify-content-md-start"
                                                                                aria-label="mailAlerts"
                                                                                name="mailAlerts"
                                                                                onChange={this.handleChangeRadioButtonRequired("client", "extra", "mailAlerts")}
                                                                                value={client.extra.mailAlerts}>
                                                                                <FormControlLabel
                                                                                    value={"1"}
                                                                                    control={<Radio className="d-flex" color="primary" />}
                                                                                    label="Si" />
                                                                                <FormControlLabel
                                                                                    value={"0"}
                                                                                    control={<Radio className="d-flex" color="primary" />}
                                                                                    label="No"
                                                                                />
                                                                            </RadioGroup>
                                                                        </FormControl>
                                                                    </Grid>
                                                                    <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                        <FormControl
                                                                            required
                                                                            margin="normal"
                                                                            fullWidth={true}
                                                                            color="primary"
                                                                            component="fieldset"
                                                                        >
                                                                            <FormLabel
                                                                                component="legend">
                                                                                Especificar el monto mínimo en Soles que debe consumir y/o retirar en efectivo para recibir notificaciones por email
                                                        </FormLabel>
                                                                            <TextField
                                                                                fullWidth={true}
                                                                                error={client.extra.minimumForAlertsError}
                                                                                required={client.extra.mailAlerts === "0"}
                                                                                name="minimumForAlerts"
                                                                                value={parseFloat(client.extra.minimumForAlerts).toFixed(0)}
                                                                                onKeyPress={this.handleKeyPressTextFieldOnlyNumberDot("minimumForAlerts")}
                                                                                onChange={this.handleChangeMinimumForAlerts("client", "extra", "minimumForAlerts")}
                                                                                onBlur={() => this.setState({
                                                                                    client: {
                                                                                        ...client,
                                                                                        extra: {
                                                                                            ...client.extra,
                                                                                            minimumForAlerts: parseFloat(client.extra.minimumForAlerts)
                                                                                                ? parseFloat(client.extra.minimumForAlerts).toFixed(2)
                                                                                                : client.extra.minimumForAlerts
                                                                                        }
                                                                                    }
                                                                                })}
                                                                                type="number"
                                                                                margin="normal"
                                                                                color="default"
                                                                                InputLabelProps={{
                                                                                    shrink: this.state.shrink,
                                                                                }}
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        ref: this.minimumForAlertsInput,
                                                                                        maxLength: defaultvalCuotasLength
                                                                                    },
                                                                                    startAdornment: (
                                                                                        <InputAdornment position="start">
                                                                                            S/.
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                        </FormControl>

                                                                    </Grid>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </ExpansionPanelDetails>
                                                </ExpansionPanel>
                                            </Grid> :
                                            <Grid item xs={12}>
                                                <ExpansionPanel className={classes.root}>
                                                    <ExpansionPanelSummary
                                                        className={classes.expansionPanel}
                                                        expandIcon={<ExpandMoreIcon fontSize="small" />}>
                                                        <Typography className={classes.heading}>
                                                            <HelpIcon fontSize="small" className="mr-2" />
                                        Preguntas Adicionales Efectivo Cencosud
                                    </Typography>
                                                    </ExpansionPanelSummary>
                                                    <ExpansionPanelDetails>
                                                        <Grid container spacing={8}>
                                                            <Grid item xs={12}>
                                                                <Grid container spacing={8}>
                                                                    {/* Client - Extra - Effective Withdrawal */}
                                                                    <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                        <FormControl
                                                                            error={client.extra.mailAlertsError}
                                                                            required
                                                                            margin="normal"
                                                                            fullWidth={true}
                                                                            color="primary"
                                                                            component="fieldset">
                                                                            <FormLabel
                                                                                component="legend">
                                                                                ¿Desea recibir notificaciones de consumo y/o retiros en efectivo por email?
                                                        </FormLabel>
                                                                            <RadioGroup
                                                                                row
                                                                                className="d-flex justify-content-between justify-content-md-start"
                                                                                aria-label="mailAlerts"
                                                                                name="mailAlerts"
                                                                                onChange={this.handleChangeRadioButtonRequired("client", "extra", "mailAlerts")}
                                                                                value={client.extra.mailAlerts}>
                                                                                <FormControlLabel
                                                                                    value={"1"}
                                                                                    control={<Radio className="d-flex" color="primary" />}
                                                                                    label="Si" />
                                                                                <FormControlLabel
                                                                                    value={"0"}
                                                                                    control={<Radio className="d-flex" color="primary" />}
                                                                                    label="No"
                                                                                />
                                                                            </RadioGroup>
                                                                        </FormControl>
                                                                    </Grid>
                                                                    {/* <Grid item xs={12} sm={6} md={4} lg={4}>
                                                                        <FormControl
                                                                            required
                                                                            margin="normal"
                                                                            fullWidth={true}
                                                                            color="primary"
                                                                            component="fieldset"
                                                                        >
                                                                            <FormLabel
                                                                                component="legend">
                                                                                Especificar el monto mínimo en Soles que debe consumir y/o retirar en efectivo para recibir notificaciones por email!
                                                        </FormLabel>
                                                                            <TextField
                                                                                fullWidth={true}
                                                                                error={client.extra.minimumForAlertsError}
                                                                                required={client.extra.mailAlerts === "0"}
                                                                                name="minimumForAlerts"
                                                                                value={parseFloat(client.extra.minimumForAlerts).toFixed(0)}
                                                                                onKeyPress={this.handleKeyPressTextFieldOnlyNumberDot("minimumForAlerts")}
                                                                                onChange={this.handleChangeMinimumForAlerts("client", "extra", "minimumForAlerts")}
                                                                                onBlur={() => this.setState({
                                                                                    client: {
                                                                                        ...client,
                                                                                        extra: {
                                                                                            ...client.extra,
                                                                                            minimumForAlerts: parseFloat(client.extra.minimumForAlerts)
                                                                                                ? parseFloat(client.extra.minimumForAlerts).toFixed(2)
                                                                                                : client.extra.minimumForAlerts
                                                                                        }
                                                                                    }
                                                                                })}
                                                                                type="number"
                                                                                margin="normal"
                                                                                color="default"
                                                                                InputLabelProps={{
                                                                                    shrink: this.state.shrink,
                                                                                }}
                                                                                InputProps={{
                                                                                    inputProps: {
                                                                                        ref: this.minimumForAlertsInput,
                                                                                        maxLength: defaultvalCuotasLength
                                                                                    },
                                                                                    startAdornment: (
                                                                                        <InputAdornment position="start">
                                                                                            S/.
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                            />
                                                                        </FormControl>
                                                                    </Grid> */}
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </ExpansionPanelDetails>
                                                </ExpansionPanel>
                                            </Grid>
                                    }
                                </Grid>
                                <Grid container spacing={8} className="d-flex justify-content-center">
                                    {/* Button - Cancel */}
                                    <Grid item xs={12} sm={12} md={6} lg={4}>
                                        <Tooltip TransitionComponent={Zoom} title="Cancelar Originación.">
                                            <div>
                                                <Bounce>
                                                    <Button
                                                        disabled={this.state.cancelButtonDisabled}
                                                        className={classes.button}
                                                        type="button"
                                                        variant="contained"
                                                        size="small"
                                                        margin="normal"
                                                        color="secondary"
                                                        onClick={this.handleCancelExpress}
                                                        fullWidth={true}>
                                                        Cancelar
                                            <CancelIcon fontSize="small" className="ml-2" />
                                                    </Button>
                                                </Bounce>
                                            </div>
                                        </Tooltip>
                                    </Grid>
                                    {/* Button - Continue */}
                                    <Grid item xs={12} sm={12} md={6} lg={4}>
                                        <Tooltip TransitionComponent={Zoom} title="Completar el formulario, para visualizar información.">
                                            <div>
                                                <Bounce>
                                                    <div className={classNames(classes.wrapper)}>
                                                        <Button
                                                            disabled={this.state.infoPreviewButtonDisabled}
                                                            variant="contained"
                                                            type="submit"
                                                            className={classNames(classes.button)}
                                                            color="primary"
                                                            size="small"
                                                            fullWidth={true}>
                                                            Previsualizar Información
                                                <SendIcon fontSize="small" className="ml-2" />
                                                        </Button>
                                                    </div>
                                                </Bounce>
                                            </div>
                                        </Tooltip>
                                    </Grid>
                                </Grid>
                            </form>
                    }
                </div>
                {
                    client.cellphoneValidationSMS.validate && 
                        <VerifyCellphone
                            client={ client }
                            origination={ this.props.origination }
                            onClick={ this.handleOpenSMSPopupValidator() }
                            onKeyPress={ this.handleKeyPressTextFieldOnlyNumber('client', 'cellphone') }
                            onBlur={ this.handleBlurTextFieldRequired('client', null, 'cellphone') }
                            onChange={ this.handleChangeTextField('client', null, 'cellphone') }
                            showVerifyCellphone={ client.cellphoneValidationSMS.visiblePopup } />
                }
                {/*  Modal Register Cliente Additional */}
                <Dialog
                    fullWidth
                    maxWidth={"md"}
                    open={this.state.openPreview}
                    //onClose={this.handleToggleInfoPreview}
                    aria-labelledby="form-dialog"
                >
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-dialog" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                Información Previa del Cliente
                        </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form
                        onSubmit={this.handleSubmitRegisterClient}
                        className={classes.modalRoot}>
                        <DialogContent>
                            <Grid container spacing={8} className={classes.item}>
                                <Grid item xs={12}>
                                    <ClientPreview data={this.state.client} />
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            className={classNames(classes.button)}
                                            disabled={this.state.cancelModalButtonDisabled}
                                            fullWidth={true}
                                            color="secondary"
                                            size="small"
                                            onClick={this.handleToggleInfoPreview}
                                            margin="normal">
                                            Cancelar
                                    </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            className={classNames(classes.button)}
                                            disabled={this.state.acceptModalButtonDisabled}
                                            type="submit"
                                            margin="normal"
                                            color="primary"
                                            size="small"
                                            fullWidth={true}>
                                            Aceptar
                                        <SendIcon fontSize="small" className="ml-2" />
                                            {this.state.acceptModalButtonDisabled && <CircularProgress size={24} className={classes.buttonProgress} />}
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>
            </React.Fragment>
        )
    }
}
export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ClientRegister)));
