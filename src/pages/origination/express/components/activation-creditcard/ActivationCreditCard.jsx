import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
// React Router
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components
import {
    AppBar,
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    IconButton,
    Fab,
    Grid,
    Tooltip,
    Typography,
    CircularProgress,
} from '@material-ui/core';
// Color
import green from '@material-ui/core/colors/green';
import yellow from '@material-ui/core/colors/yellow';
import red from '@material-ui/core/colors/red';
// Effects
import Zoom from 'react-reveal/Zoom';
import Bounce from 'react-reveal/Bounce';
// Icons
import WarningIcon from '@material-ui/icons/Warning';
import CloseIcon from '@material-ui/icons/Close';
import FingerprintIcon from '@material-ui/icons/Fingerprint';
import SendIcon from '@material-ui/icons/Send';
import DoneIcon from '@material-ui/icons/Done';
import TouchAppIcon from '@material-ui/icons/TouchApp';
// Actions
import { getDocuments, downloadDocument } from '../../../../../actions/odc-document/odc-document';
import { biometricActivity, validateBiometric } from '../../../../../actions/generic/biometric';
import { validateBiometricZytrustService } from '../../../../../actions/generic/zytrust';
import { URL_VALIDATE_ZYTRUST } from '../../../../../actions/config';
// Utils
import { resolveStateRedux } from '../../../../../utils/Utils';

const styles = theme => ({
    rootWrapper: {
        position: "relative",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    root: {
        minWidth: 300,
        maxWidth: 700,
        margin: "0 auto",
    },
    button: {
        textTransform: 'none',
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    contingencyFabWrapper: {
        zIndex: 900,
        [theme.breakpoints.up('md')]: {
            position: "absolute",
            bottom: 20,
            right: 20
        }
    },
    FileFabWrapper: {
        zIndex: 900,
        [theme.breakpoints.up('md')]: {
            position: "absolute",
            bottom: 80,
            right: 20
        }
    },
    filefab: {
        backgroundColor: red[700],
        transition: ".3s",
        '&:hover': {
            backgroundColor: red[900],
        },
    },
    contingencyfab: {
        backgroundColor: "#ffa000",
        transition: ".3s",
        '&:hover': {
            backgroundColor: yellow[900],
        },
    },
    activationCreditCard: {
        backgroundColor: green[700],
        transition: ".3s",
        '&:hover': {
            backgroundColor: green[900],
        },
    },
    buttonActions: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    buttonActionsOpenModal: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    stepWrapper: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    diplayFlexFabCenter: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    appBar: {
        position: 'relative',
    }
});

const mapStateToProps = state => {
    return {
        odcDocument: state.odcDocumentReducer,
        biometric: state.biometricReducer,
        validateBioZytrustService: state.zytrustReducer.validateBioZytrustService,
        pci: state.pciReducer,
        odcMaster: state.odcMasterReducer
    }
}

const mapDispatchToProps = dispatch => {
    const actions = {
        getDocuments: bindActionCreators(getDocuments, dispatch),
        downloadDocument: bindActionCreators(downloadDocument, dispatch),
        biometricActivity: bindActionCreators(biometricActivity, dispatch),
        validateBiometric: bindActionCreators(validateBiometric, dispatch),
        validateBiometricZytrustService: bindActionCreators(validateBiometricZytrustService, dispatch),
    };
    return actions;
}

class ActivationCreditCard extends Component {

    state = {
        origination: {
            number: 2280,
            activityNumber: 0,
            fullNumber: "190731170002",
            previousStatePhaseFlow: 0,
            nextStatePhaseFlow: 0
        },
        countConnectionZytrust: 1,
        client: {
            id: 190,
            documentTypeAux: "1",
            documentTypeId: 100001,
            documentType: "DNI",
            documentTypeInternalValue: "DU",
            documentNumber: "02781480",
            names: "ROBERTO GIAN FRANCO",
            surnames: "CARBONELL SARAVIA",
            shortName: "ROBERTO CARBONELL",
            fullName: "ROBERTO GIAN FRANCO CARBONELL SARAVIA"
        },
        creditCard: {
            type: 1,
            brand: 0,
            color: "",
            cardNumber: "0005292060099993719",
            colorDisabled: false
        },
        responseBioZytrustService: {
            messageCode: "",
            message: "",
            inputXML: "",
            outputXML: ""
        },
        number_attempts: {
            number: 0
        },
        validateIdentityClientButtonDisabled: false,
        validateIdentityClientButtonLoading: false,
        activationCreditCardButtonDisabled: true,
        contingencyButtonDisabled: true,
        pendingActivationModal: false,

        acceptPendingCreditCardButtonDisabled: false,
        cancelPendingCreditCardButtonDisabled: false,

        isBiometricOk: false,
        pendingOk: false,
    }

    getNotistack(message, variant = "default", duration = 6000) {
        let select = "default";
        switch (variant) {
            case "error":
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default:
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit" />
                </IconButton>
            ),
        });
    }

    componentWillMount = () => {
        let { client, origination, isBiometricOk, isSAEProcess } = this.props;
        let { activationCreditCardButtonDisabled, validateIdentityClientButtonDisabled } = this.state;
        if (origination) {
            if ((origination.previousStatePhaseFlow >= 10804 && !isSAEProcess) || 
                (origination.previousStatePhaseFlow >= 90704 && isSAEProcess) ||
                isBiometricOk) {
                activationCreditCardButtonDisabled = false;
                validateIdentityClientButtonDisabled = true;
            }

            this.setState(state => ({
                ...state,
                client: {
                    ...state.client,
                    ...client
                },
                origination: {
                    ...state.origination,
                    ...origination
                },
                activationCreditCardButtonDisabled: activationCreditCardButtonDisabled,
                validateIdentityClientButtonDisabled: validateIdentityClientButtonDisabled,
                isBiometricOk: isBiometricOk
            }));
        }
    }

    componentDidMount = () => {
        this.props.validateBiometricZytrustService();
        window.addEventListener("focus", this.onWindowsValidateBiometricZytrustService);
        window.addEventListener("blur", this.onWindowsValidateBiometricZytrustService);
    }

    onWindowsValidateBiometricZytrustService = () => {
        this.props.validateBiometricZytrustService();
    }

    componentDidUpdate = (prevProps) => {
        // Biometric
        if (prevProps.biometric.biometricActivity !== this.props.biometric.biometricActivity) {
            if (!this.props.biometric.biometricActivity.loading &&
                this.props.biometric.biometricActivity.response &&
                this.props.biometric.biometricActivity.success) {
                let { data } = this.props.biometric.biometricValidation
                let { des_error, color, btnActivar, btnContingencia, btnValidarIdentidad } = data
                let { countConnectionZytrust } = this.state

                if (color === "warning") {
                    countConnectionZytrust++
                }

                // Success
                this.setState(state => ({
                    ...state,
                    countConnectionZytrust: countConnectionZytrust,
                    isBiometricOk: btnActivar,
                    contingencyButtonDisabled: !btnContingencia,
                    validateIdentityClientButtonDisabled: !btnValidarIdentidad,
                    activationCreditCardButtonDisabled: !btnActivar,
                    validateIdentityClientButtonLoading: false,
                }));
                // Notistack
                if (des_error) {
                    let messages = des_error.split("||")
                    this.getNotistack(`Biometría: ${messages[0]}`, color);

                    if (btnContingencia && messages[1]) {
                        this.getNotistack(messages[1], 'warning', 10000);
                    }
                }
            }
            else if (!this.props.biometric.biometricActivity.loading &&
                this.props.biometric.biometricActivity.response &&
                !this.props.biometric.biometricActivity.success) {
                this.getNotistack(`Biometría: ${this.props.biometric.biometricActivity.error}`, "error");
            }
            else if (!this.props.biometric.biometricActivity.loading &&
                !this.props.biometric.biometricActivity.response &&
                !this.props.biometric.biometricActivity.success) {
                this.getNotistack(`Biometría: ${this.props.biometric.biometricActivity.error}`, "error");
            }

        }
        // Zytrust - Response
        if (prevProps.responseBioZytrustService !== this.props.responseBioZytrustService) {
            // Enabled Button - Validate Identity Client
            if (this.props.responseBioZytrustService.loading) {
                this.setState(state => ({
                    ...state,
                    contingencyButtonDisabled: true,
                    validateIdentityClientButtonDisabled: true,
                    validateIdentityClientButtonLoading: true
                }));
            } else {
                let { data, error } = this.props.responseBioZytrustService;

                let message = data ? data : error

                let sendData = {
                    solicitudeCode: this.state.origination.number,
                    completeSolicitudeCode: this.state.origination.fullNumber,
                    intentos: this.state.countConnectionZytrust,
                    codeError: message.messageCode ? message.messageCode : 0,
                    resultCode: message.resultCode ? message.resultCode : 0,
                    message: message.message,
                    restriccion: message.restriccion,
                    validateXML: message.validateXML
                }
                this.props.validateBiometric(sendData);

            }
        }

        // ODC Biometrico Response
        if (prevProps.biometric.biometricValidation !== this.props.biometric.biometricValidation) {
            if (this.props.biometric.biometricValidation.loading) {
                this.setState(state => ({
                    ...state,
                    contingencyButtonDisabled: true,
                    validateIdentityClientButtonDisabled: true,
                    validateIdentityClientButtonLoading: true
                }));
            }
            if (!this.props.biometric.biometricValidation.loading &&
                this.props.biometric.biometricValidation.response &&
                this.props.biometric.biometricValidation.success) {

                let { client, origination } = this.state;

                let message = this.props.responseBioZytrustService.data ? this.props.responseBioZytrustService.data : this.props.responseBioZytrustService.error

                let sendData = {
                    tipo_doc: client.documentTypeAux,
                    nro_doc: client.documentNumber,
                    tipo_doc_letra: client.documentType,
                    inputXml: message.inputXML,
                    outputXml: message.outputXML,
                    errorCodeBiometrico: message.messageCode,
                    errorMessageBiometrico: message.message,
                    nom_actividad: this.props.isSAEProcess ? 'Activación Efectivo: Biometría' : "Activación Tarjeta: Biometría",
                    cod_flujo_fase_estado_actual: this.props.isSAEProcess ? 90702 : 10802,
                    cod_flujo_fase_estado_anterior: this.props.isSAEProcess ? 90701 : 10801,
                    cod_solicitud: origination.number,
                    cod_solicitud_completa: origination.fullNumber,
                    cod_cliente: client.id
                }

                if (this.props.biometric.biometricValidation.data.color === "success") {
                    sendData.errorCodeBiometrico = ""
                    sendData.errorMessageBiometrico = ""
                }

                this.props.biometricActivity(sendData);
            }
            else if (!this.props.biometric.biometricValidation.loading &&
                this.props.biometric.biometricValidation.response &&
                !this.props.biometric.biometricValidation.success) {
                this.setState(state => ({
                    ...state,
                    validateIdentityClientButtonLoading: false,
                    validateIdentityClientButtonDisabled: false,
                    isBiometricOk: false,
                }));
                this.getNotistack(`Biometría: ${this.props.biometric.biometricValidation.error}`, "error");
            }
            else if (!this.props.biometric.biometricValidation.loading &&
                !this.props.biometric.biometricValidation.response &&
                !this.props.biometric.biometricValidation.success) {
                this.setState(state => ({
                    ...state,
                    validateIdentityClientButtonLoading: false,
                    validateIdentityClientButtonDisabled: false,
                    isBiometricOk: false,
                }));
                this.getNotistack(`Biometría: ${this.props.biometric.biometricValidation.error}`, "error");
            }
        }

        // block Type Credit Card
        if (prevProps.blockTypeCreditCard !== this.props.blockTypeCreditCard) {
            if (this.props.blockTypeCreditCard.loading) {
                this.setState(state => ({
                    ...state,
                    activationCreditCardButtonDisabled: true
                }));
            }
            if (!this.props.blockTypeCreditCard.loading && this.props.blockTypeCreditCard.response && this.props.blockTypeCreditCard.success) {
                if (this.props.blockTypeCreditCard.extra) {
                    const { isSAE } = this.props.blockTypeCreditCard.extra;
                    if (isSAE) {
                        this.getNotistack("Activación TC: PMP Activar Credito Efectivo!", "success");
                    }
                    else {
                        this.getNotistack("Activación TC: PMP Activar Tarjeta Ok!", "success");
                    }
                }
            }
            else if (!this.props.blockTypeCreditCard.loading && this.props.blockTypeCreditCard.response && !this.props.blockTypeCreditCard.success) {
                // Notistack
                this.getNotistack(this.props.blockTypeCreditCard.error, "error");
                this.setState(state => ({
                    ...state,
                    activationCreditCardButtonDisabled: false
                }));
            }
            else if (!this.props.blockTypeCreditCard.loading && !this.props.blockTypeCreditCard.response && !this.props.blockTypeCreditCard.success) {
                // Notistack
                this.getNotistack(this.props.blockTypeCreditCard.error, "error");
                this.setState(state => ({
                    ...state,
                    activationCreditCardButtonDisabled: false
                }));
            }
        }
        // Generic Activity
        if (prevProps.genericActivity !== this.props.genericActivity) {
            if (this.props.genericActivity.loading) {
                this.setState(state => ({
                    ...state,
                    activationCreditCardButtonDisabled: true,
                }));
            }
            if (!this.props.genericActivity.loading && this.props.genericActivity.response && this.props.genericActivity.success) {
                const { extra } = this.props.genericActivity;
                if (extra) {
                    if (extra.finishProcess) {
                        this.getNotistack("Consulta Correcta, 7mo Paso Ok!", "success");
                        setTimeout(() => this.props.handleNext(), 2500);
                    }
                }
            }
            else if (!this.props.genericActivity.loading && this.props.genericActivity.response && !this.props.genericActivity.success) {
                // Notistack
                this.getNotistack(this.props.genericActivity.error, "error");
                this.setState(state => ({
                    ...state,
                    activationCreditCardButtonDisabled: false
                }));
            }
            else if (!this.props.genericActivity.loading && !this.props.genericActivity.response && !this.props.genericActivity.success) {
                // Notistack
                this.getNotistack(this.props.genericActivity.error, "error");
                this.setState(state => ({
                    ...state,
                    activationCreditCardButtonDisabled: false
                }));
            }
        }
        // Simple Generic Activity
        if (prevProps.simpleGenericActivity !== this.props.simpleGenericActivity) {
            if (this.props.simpleGenericActivity.loading) {
                this.setState(state => ({
                    ...state,
                    acceptPendingCreditCardButtonDisabled: true,
                    cancelPendingCreditCardButtonDisabled: true,
                }));
            }
            if (!this.props.simpleGenericActivity.loading &&
                this.props.simpleGenericActivity.response &&
                this.props.simpleGenericActivity.success) {
                const { extra } = this.props.simpleGenericActivity;
                if (extra) {
                    if (extra.pendingTC) {
                        this.getNotistack("Pendiente de Activación de TC.", "info");
                        setTimeout(() => this.props.handleReset(), 2500);
                    }
                    else if (extra.finishProcess) {
                        this.getNotistack("Fin Proceso Efectivo Cencosud", "success");
                        setTimeout(() => this.props.handleNext(), 2500);
                    }
                }
            }
            else if (!this.props.simpleGenericActivity.loading && this.props.simpleGenericActivity.response && !this.props.simpleGenericActivity.success) {
                // Notistack
                this.getNotistack(this.props.simpleGenericActivity.error, "error");
                this.setState(state => ({
                    ...state,
                    acceptPendingCreditCardButtonDisabled: false,
                    cancelPendingCreditCardButtonDisabled: false,
                }));
            }
            else if (!this.props.simpleGenericActivity.loading && !this.props.simpleGenericActivity.response && !this.props.simpleGenericActivity.success) {
                // Notistack
                this.getNotistack(this.props.simpleGenericActivity.error, "error");
                this.setState(state => ({
                    ...state,
                    acceptPendingCreditCardButtonDisabled: false,
                    cancelPendingCreditCardButtonDisabled: false,
                }));
            }
        }
        resolveStateRedux(prevProps.pci.cardnumberDecrypted, this.props.pci.cardnumberDecrypted, 
            _ => this.setState(state => ({
                ...state,
                activationCreditCardButtonDisabled: true
            })),
            _ => this.getNotistack('Activación TC: PCI Desencriptar OK!', 'success'),
            warningMessage => {
                this.getNotistack(`Activación TC: PCI Desencriptar - ${ warningMessage }`, 'warning')
                this.setState(state => ({
                    ...state,
                    activationCreditCardButtonDisabled: false
                }))
            },
            errorMessage => {
                this.getNotistack(`Activación TC: PCI Desencriptar - ${ errorMessage }`, 'error')
                this.setState(state => ({
                    ...state,
                    activationCreditCardButtonDisabled: false
                }))
            }
        )
        resolveStateRedux(prevProps.odcMaster.blockingCodeCreditCardUpdated, this.props.odcMaster.blockingCodeCreditCardUpdated, 
            _ => this.setState(state => ({
                ...state,
                activationCreditCardButtonDisabled: true
            })),
            _ => this.getNotistack('Activación TC: Maestro Cambio Bloqueo OK!', 'success'),
            warningMessage => {
                this.getNotistack(`Activación TC: Maestro Cambio Bloqueo - ${ warningMessage }`, 'warning')
                this.setState(state => ({
                    ...state,
                    activationCreditCardButtonDisabled: false
                }))
            },
            errorMessage => {
                this.getNotistack(`Activación TC: Maestro Cambio Bloqueo - ${ errorMessage }`, 'error')
                this.setState(state => ({
                    ...state,
                    activationCreditCardButtonDisabled: false
                }))
            }
        )
    }
    componentWillUnmount = () => {
        window.removeEventListener("focus", this.onWindowsValidateBiometricZytrustService);
        window.removeEventListener("blur", this.onWindowsValidateBiometricZytrustService);
    }
    // Close Modal
    handleClickCloseModalContigency = e => {
        this.setState(state => ({
            ...state,
            pendingActivationModal: false
        }));
    }

    // Open Modal
    handleClickOpenModalContigency = e => {
        this.setState(state => ({
            ...state,
            pendingActivationModal: true
        }));
    }
    // Send - Zytrust Biometric
    handleBiometricZytrustServiceForm = e => {
        e.preventDefault();
        this.props.handleBiometricZytrustService();
    }
    // Send - Pending Activation
    handleSubmitPendingActivationForm = e => {
        e.preventDefault();
        this.props.handlePendingActivationCredit();
    }
    // Send - Activation CreditCard
    handleSubmitActivationCreditCardForm = e => {
        e.preventDefault();
        if (this.props.disbursementTypeId === 380001) {
            this.getNotistack(`Se ha iniciado el proceso de desembolso Efectivo Cencosud`, "success");
        }
        this.props.handleActivationCreditCard();
    }

    render() {
        let { classes,
            blockTypeCreditCard,
            validateBioZytrustService,
            isSAEProcess
        } = this.props;
        return (
            <Grid container spacing={8}>
                <Grid item xs={12}>
                    <form method="post" onSubmit={this.handleSubmitActivationCreditCardForm}>
                        <Grid container spacing={8} className={classNames(classes.rootWrapper, "p-sm-5")}>
                            <Grid item xs={12}>
                                <Grid container spacing={8} className={classes.root}>
                                    {/* Header */}
                                    <Grid item xs={12} className="mb-2">
                                        <Typography align="center" component="h2" variant="h6">
                                            {isSAEProcess ? 'Activación de Efectivo Cencosud' : 'Activación de Tarjeta'}
                                        </Typography>
                                    </Grid>
                                    {/* Step - Bio */}
                                    <Grid item xs={12} className="mb-2">
                                        <Grid container spacing={8}>
                                            <Grid item xs={12}>
                                                <Typography className={classes.text}>
                                                    <DoneIcon className="text-green mr-2" fontSize="small" />
                                                    Realizar el proceso de generación de clave en el Sistema
                                                    <span className="font-weight-bold"> MIG</span>
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Typography>
                                                    <DoneIcon className="text-green mr-2" fontSize="small" />
                                                    Validación Biométrica al Cliente
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Typography>
                                                    <DoneIcon className="text-green mr-2" fontSize="small" />
                                                    Si luego de 3 intentos el cliente no ha podido ser validado, realizar la
                                                    <span className="font-weight-bold"> Modalidad de Contingencia</span>.
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    {/* Form Button - Contigency - Files */}
                                    <Grid item xs={12} className="mb-2">
                                        <Grid container spacing={8} className={classes.buttonActionsOpenModal}>
                                            {/* Form - Button - Contingency */}
                                            <Grid item xs={12} sm={6} className={classes.contingencyFabWrapper}>
                                                <Bounce>
                                                    <div className={classes.diplayFlexFabCenter}>
                                                        <Fab
                                                            disabled={this.state.contingencyButtonDisabled}
                                                            onClick={this.handleClickOpenModalContigency}
                                                            variant="extended"
                                                            className={classes.contingencyfab}>
                                                            <WarningIcon
                                                                className="text-white"
                                                                fontSize="small"
                                                                color="inherit" />
                                                        </Fab>
                                                        <Typography className="font-weight-bolder m-2 d-lg-none">
                                                            Modalidad de Contingencia
                                                        </Typography>
                                                    </div>
                                                </Bounce>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        {/* Form - Button */}
                        <Grid container spacing={8} className={classes.buttonActions}>
                            <Grid item xs={12}>
                                <Typography align="center">
                                    {
                                        validateBioZytrustService.data ?
                                            validateBioZytrustService.data.online ?
                                                <Typography component="span" className="font-weight-bolder text-green">
                                                    {validateBioZytrustService.data.message}
                                                    <TouchAppIcon fontSize="small" className="ml-2" />
                                                </ Typography>
                                                :
                                                <Typography color="secondary" component="span" className="font-weight-bolder">
                                                    {validateBioZytrustService.data.message ?
                                                        <React.Fragment>
                                                            {validateBioZytrustService.data.message}
                                                            <a href={URL_VALIDATE_ZYTRUST}
                                                                className="ml-2"
                                                                target='_blank'
                                                                rel="noopener noreferrer" >
                                                                Click Aqui
                                                        </a>
                                                        </React.Fragment> : ""
                                                    }
                                                </ Typography>
                                            :
                                            <Typography color="error" component="span" className="font-weight-bolder">
                                                {validateBioZytrustService.data.message}
                                                Click Aqui
                                            </ Typography>
                                    }
                                </Typography>
                            </Grid>
                            {/* Button - Validate Identity */}
                            <Grid item xs={12} sm={12} md={4} lg={3}>
                                <Tooltip TransitionComponent={Zoom} title="Click para validar la identidad del cliente.">
                                    <div>
                                        <Bounce>
                                            <Button
                                                disabled={this.state.validateIdentityClientButtonDisabled}
                                                variant="contained"
                                                type="submit"
                                                className={classNames(classes.button)}
                                                color="primary"
                                                size="small"
                                                onClick={this.handleBiometricZytrustServiceForm}
                                                fullWidth={true}>
                                                Validar Identidad
                                                <FingerprintIcon fontSize="small" className="ml-2" />
                                                {
                                                    this.state.validateIdentityClientButtonLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                                                }
                                            </Button>
                                        </Bounce>
                                    </div>
                                </Tooltip>
                            </Grid>
                            {/* Button - Activation */}
                            <Grid item xs={12} sm={12} md={4} lg={3}>
                                <Tooltip TransitionComponent={Zoom} title="Click para la activación de la tarjeta.">
                                    <div>
                                        <Bounce>
                                            <div className={classNames(classes.wrapper)}>
                                                <Button
                                                    disabled={this.state.activationCreditCardButtonDisabled}
                                                    variant="contained"
                                                    type="submit"
                                                    className={classNames(classes.button, classes.activationCreditCard)}
                                                    color="primary"
                                                    fullWidth={true}
                                                    autoFocus
                                                    size="small">
                                                    {isSAEProcess ? 'Activar Efectivo Cencosud' : 'Activar Tarjeta'}
                                                    <SendIcon fontSize="small" className="ml-2" />
                                                    {
                                                        blockTypeCreditCard.loading && <CircularProgress size={24} className={classes.buttonProgress} />
                                                    }
                                                </Button>
                                            </div>
                                        </Bounce>
                                    </div>
                                </Tooltip>
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
                {/* Modal Contingecy */}
                <Dialog
                    onClose={this.handleClickCloseModalContigency}
                    open={this.state.pendingActivationModal}
                    maxWidth='xs'
                    aria-labelledby="form-dialog-contingency">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-dialog" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                Pendiente de Activación
                            </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form onSubmit={this.handleSubmitPendingActivationForm}>
                        <DialogContent>
                            <Typography align="center">
                                La tarjeta estará en la bandeja de pendientes por activar.
                            </Typography>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={this.state.cancelPendingCreditCardButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth={true}
                                            color="secondary"
                                            size="small"
                                            onClick={this.handleClickCloseModalContigency}
                                            margin="normal">
                                            Cancelar
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={this.state.acceptPendingCreditCardButtonDisabled}
                                            className={classNames(classes.button)}
                                            type="submit"
                                            margin="normal"
                                            color="primary"
                                            size="small"
                                            fullWidth={true}>
                                            Aceptar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                                this.state.acceptPendingCreditCardButtonDisabled && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>
            </Grid>
        )
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ActivationCreditCard)));