import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// Components
import Typography from '@material-ui/core/Typography';
// Effects
import Fade from 'react-reveal/Fade';
//Icons 
import LensIcon from '@material-ui/icons/Lens';

const styles = theme => ({
    root:{
        display:"flex", 
        justifyContent:"space-between", 
        alignItems:"center"
    },
    valueWrapper:{
        display:"flex", 
        justifyContent:"space-between", 
        alignItems:"center"
    }
});

const ItemDetailPreEvaluated = ({data, classes, ...props}) => (
    <div className={classNames(classes.root, "mb-2")}>
        <Fade left>
            <Typography>
                {data.name}
            </Typography>
        </Fade>
        <Fade right>
            <Typography className={classes.valueWrapper} color="textSecondary">
                {
                    data.value === 1?  
                    <LensIcon className="text-green" /> : 
                    data.value === 0? <LensIcon className="text-red" />: <LensIcon className="text-amber" />
                }
            </Typography>
        </Fade>
    </div>
);

ItemDetailPreEvaluated.propTypes = {
    data: PropTypes.object.isRequired,
};

ItemDetailPreEvaluated.defaultProps = {
    data: {
        initials:"",
        name: "",
        value: "", 
    }
};

export default withStyles(styles)(ItemDetailPreEvaluated);



