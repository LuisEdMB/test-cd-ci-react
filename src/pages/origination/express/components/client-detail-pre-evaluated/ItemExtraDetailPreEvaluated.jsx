import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
// Components
import Typography from '@material-ui/core/Typography';
// Effects
import Fade from 'react-reveal/Fade';

const styles = theme => ({
    root:{
        display:"flex", 
        flexDirection:"column", 
        justifyContent:"start", 
        [theme.breakpoints.down('sm')]: {
            flexDirection:"row", 
            justifyContent:"space-between", 
            alignItems:"center",
        },
        [theme.breakpoints.up('md')]: {
            flexDirection:"row", 
            justifyContent:"space-between", 
            alignItems:"center",
        }
    },
    valueWrapper:{
        display:"flex", 
        justifyContent:"space-between", 
        alignItems:"center"
    }
});

const ItemExtraDetailPreEvaluated = ({data, classes, ...props}) => (
    <div className={classNames(classes.root, "mb-2")}>
        <Fade left>
            <Typography >
                { data.name }
            </Typography>
        </Fade>
        <Fade right>
            <Typography className={classes.valueWrapper} color="textSecondary">
                { data.value }
            </Typography>
        </Fade>
    </div>
);
  
ItemExtraDetailPreEvaluated.propTypes = {
    data: PropTypes.object.isRequired,
};

ItemExtraDetailPreEvaluated.defaultProps = {
    data: {
        name:"",
        value: "", 
    }
};

export default withStyles(styles)(ItemExtraDetailPreEvaluated);



