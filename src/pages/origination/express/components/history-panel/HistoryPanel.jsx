import React from "react";
import classNames from "classnames";
import HistoryIcon from "@material-ui/icons/History";
import { withStyles } from "@material-ui/core/styles";
// Components
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
// Effects
// Icons
import LensIcon from "@material-ui/icons/Lens";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
// Colors
import orange from "@material-ui/core/colors/orange";

// Utils
import { getMoneyFormat, maskCreditCard } from "../../../../../utils/Utils";

const styles = _ => ({
  title: {
    color: "white",
    textShadow: "1px 1px 6px black",
  },
  onHistory: {
    right: 0,
  },
  offHistory: {
    right: -290,
  },
  fontSize: {
    fontSize: 12,
  },
  IconButton: {
    top: -10,
    left: -50,
    position: "absolute",
    zIndex: 950,
    backgroundColor: orange[200],
    color: "white",
    transition: ".3s",
    "&:hover": {
      backgroundColor: orange[300],
      transform: "rotate(180deg) scale(1.05)",
    },
  },
  icon: {
    fontSize: 20,
  },
  creditCardColor: {
    fontSize: 11.5,
  },
  historyContainer: {
    position: "fixed",
    top: "33.3vh",
    transition: ".3s",
    width: 290,
    boxShadow: "1px 1px 4px black",
    height: 330,
    backgroundColor: "white",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "rgba(0,0,0,0.23)",
    borderRadius: "0 0 0 .5em",
    zIndex: 950,
  },
  container: {
    overflowY: "auto",
    height: 300,
  },
});

const HistoryPanel = (props) => {
  const {
    origination,
    lineAvailable,
    effectiveProvision,
    finalLineAvailable,
    finalEffectiveProvision,
    client,
    creditCard,
    showHistory,
    numero_cuotas,
    monto_cuota,
    tcea,
    lineSAE,
    onClick,
    noWrap = false,
    classes,
  } = props;
  let showColor = creditCard ? creditCard.productId !== 10008 : false;
  let fullName = null;
  if (client.firsName && client.lastName) {
    fullName = `${client.firsName} ${client.secondName} ${client.lastName} ${client.motherLastName}`.trim();
  }
  return (
    <div
      className={classNames(classes.historyContainer, {
        [classes.onHistory]: showHistory,
        [classes.offHistory]: !showHistory,
      })}
    >
      <div>
        <IconButton
          onClick={onClick}
          className={classNames(classes.IconButton)}
        >
          {showHistory ? (
            <KeyboardArrowLeftIcon className={classNames(classes.icon)} />
          ) : (
            <KeyboardArrowRightIcon className={classNames(classes.icon)} />
          )}
        </IconButton>
      </div>
      <Grid container spacing={8}>
        <Grid item xs={12}>
          <Typography
            align="center"
            component="span"
            className={classNames(
              "bg-metal-blue w-100 d-flex align-items-center justify-content-center text-white text-shadow-black py-1"
            )}
          >
            <Typography
              component="span"
              color="inherit"
              className={classes.fontSize}
            >
              FLUJO DE INFORMACIÓN
            </Typography>
            <HistoryIcon fontSize="small" className="ml-2" />
          </Typography>
        </Grid>
        <Grid item xs={12} className={classNames(classes.container, "px-3")}>
          {/* Origination - Full Number */}
          <Grid container spacing={8}>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
                noWrap={noWrap}
              >
                {origination
                  ? origination.fullNumber
                    ? "NRO. SOLICITUD:"
                    : ""
                  : ""}
              </Typography>
            </Grid>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {origination
                  ? origination.fullNumber
                    ? origination.fullNumber
                    : ""
                  : ""}
              </Typography>
            </Grid>
          </Grid>
          {/* Client - Document Type */}
          <Grid container spacing={8}>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {client
                  ? client.documentType
                    ? `${client.documentType.toUpperCase()}:`
                    : ""
                  : ""}
              </Typography>
            </Grid>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {client
                  ? client.documentNumber
                    ? client.documentNumber
                    : ""
                  : ""}
              </Typography>
            </Grid>
          </Grid>
          {/* Client - FullName */}
          <Grid container spacing={8}>
            <Grid item xs={12}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {fullName && fullName.toUpperCase()}
              </Typography>
            </Grid>
          </Grid>
          {/* Client - E-Mail */}
          <Grid container spacing={8}>
            <Grid item xs={12}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {client ? (client.email ? client.email.toUpperCase() : "") : ""}
              </Typography>
            </Grid>
          </Grid>
          {/* Client - Birthday */}
          <Grid container spacing={8}>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {client.birthday && "FEC. NACIMIENTO:"}
              </Typography>
            </Grid>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {client.birthday && client.birthday.substring(0, 10)}
              </Typography>
            </Grid>
          </Grid>
          {/* Credit Card - Brand */}
          <Grid container spacing={8}>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {creditCard ? (creditCard.brand ? "MARCA:" : "") : ""}
              </Typography>
            </Grid>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {creditCard
                  ? creditCard.brand
                    ? creditCard.brand.toUpperCase()
                    : ""
                  : ""}
              </Typography>
            </Grid>
          </Grid>
          {/* Credit Card - Card Number */}
          <Grid container spacing={8}>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {creditCard
                  ? creditCard.cardNumber
                    ? "NÚMERO TARJETA:"
                    : ""
                  : ""}
              </Typography>
            </Grid>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {creditCard
                  ? creditCard.cardNumber
                    ? maskCreditCard(creditCard.cardNumber)
                    : ""
                  : ""}
              </Typography>
            </Grid>
          </Grid>
          {/* Credit Card - Color */}
          {showColor && (
            <Grid container spacing={8}>
              <Grid item sm={6}>
                <Typography
                  color="default"
                  align="left"
                  className={classes.fontSize}
                >
                  {creditCard ? (creditCard.colorAux ? "COLOR:" : "COLOR:") : ""}
                </Typography>
              </Grid>
              <Grid item sm={6}>
                <Typography
                  color="default"
                  align="left"
                  className={classes.fontSize}
                >
                  {creditCard ? (
                    creditCard.colorAux ? (
                      <LensIcon
                        className={classes.creditCardColor}
                        style={{ color: creditCard.colorAux }}
                      />
                    ) : (
                      "NO APLICA"
                    )
                  ) : (
                    ""
                  )}
                </Typography>
              </Grid>
            </Grid>
          )}
          {/* Effective Provision */}
          <Grid container spacing={8}>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {effectiveProvision ? "D.E. INICIAL:" : ""}
              </Typography>
            </Grid>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {effectiveProvision
                  ? `S/ ${getMoneyFormat(
                    (lineAvailable.value * effectiveProvision) / 100
                  )} (${finalEffectiveProvision}%)`
                  : ""}
              </Typography>
            </Grid>
          </Grid>
          {/* Final Avaiable Line*/}
          <Grid container spacing={8}>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {finalLineAvailable > -1 ? "L.C (TC) OTORGADO:" : ""}
              </Typography>
            </Grid>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {finalLineAvailable > -1
                  ? `S/ ${getMoneyFormat(finalLineAvailable)}`
                  : ""}
              </Typography>
            </Grid>
          </Grid>
          {/* Effective Provision */}
          <Grid container spacing={8}>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {lineSAE > -1 ? "EC OTORGADO:" : ""}
              </Typography>
            </Grid>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {lineSAE > -1 ? `S/ ${getMoneyFormat(lineSAE)} ` : ""}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={8}>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {tcea ? "TCEA:" : ""}
              </Typography>
            </Grid>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {tcea ? `(${parseFloat(tcea).toFixed(2)}%)` : ""}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={8}>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {numero_cuotas ? "NUM. CUOTAS:" : ""}
              </Typography>
            </Grid>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {numero_cuotas ? numero_cuotas : ""}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={8}>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {monto_cuota ? "V.A. CUOTAS:" : ""}
              </Typography>
            </Grid>
            <Grid item sm={6}>
              <Typography
                color="default"
                align="left"
                className={classes.fontSize}
              >
                {monto_cuota ? `S/ ${getMoneyFormat(monto_cuota)}` : ""}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

HistoryPanel.defaultProps = {
  client: {
    birthday: "01-01-1900",
  }
};

export default withStyles(styles)(HistoryPanel);
