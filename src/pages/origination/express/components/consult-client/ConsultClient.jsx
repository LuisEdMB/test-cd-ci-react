// React
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components
import {
    Button,
    CircularProgress,
    FormControl,
    FormHelperText, 
    Grid,  
    IconButton, 
    InputAdornment,
    InputLabel, 
    MenuItem,
    Select,
    TextField
} from '@material-ui/core';
import green from '@material-ui/core/colors/green';
// Icons
import CloseIcon from '@material-ui/icons/Close'; 
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import SendIcon from '@material-ui/icons/Send'; 
// Effects
import Fade from 'react-reveal/Fade';
import Bounce from 'react-reveal/Bounce';
// Actions 
import { getIdentificationDocumentType } from '../../../../../actions/value-list/identification-document-type';
// Utils 
import { Typography } from '@material-ui/core';

const styles = theme => ({
    root:{
        display:"flex",
        [theme.breakpoints.up('md')]: {
            justifyContent:"center", 
        }
    },
    buttonWrapper:{
        display:"flex",
        justifyContent:"center",
    },
    button: {
        textTransform: 'none',
    },
    wrapper: {
        position: 'relative',
    }, 
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    }, 
    documentNumberFormHelperText:{
        display:"flex",
        justifyContent:"space-between", 
        alignItems:"center"
    },
    fontSizeError: {
        fontSize:"0.75rem"
    }
});


const mapStateToProps = (state, props) => {
    return {
        idenfiticationDocumentType : state.identificationDocumentTypeReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getIdentificationDocumentType: bindActionCreators(getIdentificationDocumentType, dispatch),
    };
  return actions;
}

class ConsultClient extends Component {
    state = {
        client:{
            documentTypeId:100001, // Value Default Id DNI
            documentTypeAux:"1",
            documentTypeInternalValue:"DU", // Value Default SIEBEL 
            documentType:"DNI", // Value Default (DNI) Select Client Document Type
            documentTypeSelectDisabled:true, // Disabled Select Client Document Type
            documentNumber:"",  // Value Default Select Client Document Type
            documentNumberOk:false, // Value Ok Client Document Number
            documentNumberError:false, // Change Color TextField
            documentNumberShowMessageError:false, // Error Message Client Document Number
            validateClientButtonDisabled:false, // Valid;ate Button Cliente Active  
        },
    }

    componentDidMount(){
        this.props.getIdentificationDocumentType();
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(prevProps.idenfiticationDocumentType !== this.props.idenfiticationDocumentType){
            // Enabled Button - Validate Identity Client	
            if(this.props.idenfiticationDocumentType.loading){
                this.setState(state => ({
                    client:{
                        ...state.client,
                        documentTypeSelectDisabled:true
                    }
                }));
            }
            if(!this.props.idenfiticationDocumentType.loading && this.props.idenfiticationDocumentType.response && this.props.idenfiticationDocumentType.success){
                this.setState(state => ({
                    client:{
                        ...state.client,
                        documentTypeSelectDisabled:false,
                        validateClientButtonDisabled:false
                    }
                }));
            }
            else if(!this.props.idenfiticationDocumentType.loading && this.props.idenfiticationDocumentType.response && !this.props.idenfiticationDocumentType.success){
                this.setState(state => ({
                    client:{
                        ...state.client,
                        documentTypeSelectDisabled:true,
                        validateClientButtonDisabled:true
                    }
                }));
                // Notistack
                this.getNotistack(this.props.idenfiticationDocumentType.error, "error");
            }
            else if(!this.props.idenfiticationDocumentType.loading && !this.props.idenfiticationDocumentType.response && !this.props.idenfiticationDocumentType.success){
                this.setState(state => ({
                    client:{
                        ...state.client,
                        documentTypeSelectDisabled:true,
                        validateClientButtonDisabled:true
                    }
                }));
                // Notistack
                this.getNotistack(this.props.idenfiticationDocumentType.error, "error");
            }
        }
        if (prevProps.firstCall !== this.props.firstCall) {
            // Enabled Button - Validate Identity Client	
            if(this.props.firstCall.loading){
                this.setState(state => ({
                    validateClientButtonDisabled:true
                }));
            }
            if(!this.props.firstCall.loading && this.props.firstCall.response && this.props.firstCall.success){
                this.getNotistack("Consulta Correcta, 1er Paso Ok!", "success");
                setTimeout(()=>{
                   this.props.handleNext();
                }, 1500) 
            }
            else if(!this.props.firstCall.loading && this.props.firstCall.response && !this.props.firstCall.success){
                this.setState(state => ({
                    validateClientButtonDisabled:false
                }));
                // Notistack
                this.getNotistack(this.props.firstCall.error, "error");
            }
            else if(!this.props.firstCall.loading && !this.props.firstCall.response && !this.props.firstCall.success){
                this.setState(state => ({
                    validateClientButtonDisabled:false
                }));
                // Notistack
                this.getNotistack(this.props.firstCall.error, "error");
            }
        }
    }
    // Client - Document Type - Select Event Change
    handleChangeSelectDocumentType = name => e => {
        let object = this.props.idenfiticationDocumentType.data.find(option => option.cod_valor === e.target.value);
        this.setState(state => ({
            client:{
                ...state.client,
                documentType:object.des_valor,
                documentTypeId:object.cod_valor,
                documentTypeInternalValue:object.valor_interno, 
                documentNumber:"", 
                documentNumberError:false, 
                documentNumberOk:false, 
                documentNumberShowMessageError:false
           }
        }));
    } 
    // Validate Length Document Type
    validateLengthDocumentType = (value) => {
        switch(value){
            case 8 : return false;
            case 9 : return false;
            default : return true;
        }
    }
    // Client - Document Number - TextFiled Event Change
    handleChangeTextFieldDocumentNumber = name => e =>{
        let { value, name } = e.target;
        let nameError = `${name}Error`;
        if(!isNaN(Number(value))){
            this.setState(state => ({
                client:{
                    ...state.client,
                    [name]:value,
                    [nameError]: this.validateLengthDocumentType(value.length)
                }
            }));        
        }
    }
    // Client - Data - Button Event Submit
    handleSubmitValidateClient = e => {
        e.preventDefault(); 
        this.setState(state => ({
            client:{
                ...state.client, 
                documentNumberShowMessageError:false
            }
        }));
        // Validate Value Data
        let { client } = this.state;
        if(
            (client.documentTypeId === 100001 && client.documentNumber.trim().length === 8) || 
            (client.documentTypeId === 100002 && client.documentNumber.trim().length === 9) 
        ){
            // Set Value
            let data = {
                client:{
                    documentType: client.documentType,
                    documentTypeAux: client.documentNumber.length === 8 ? "1":"2",
                    documentTypeId: client.documentTypeId,
                    documentTypeInternalValue: client.documentTypeInternalValue,
                    documentNumber: client.documentNumber
                }
            }
            // Send Data
            this.props.handleClientValidationForm(data);
        }
        else{
            this.setState(state => ({
                client:{
                    ...state.client, 
                    documentNumberShowMessageError:true
                }
            }));
        }
    }   
    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render() {
        let { classes, idenfiticationDocumentType} = this.props;
        let { client } = this.state;
        return (
            <form method="post" onSubmit={this.handleSubmitValidateClient} autoComplete="off">
                <Grid container spacing={8} className={classNames(classes.root, "mb-2")}>
                    {/* Client - Identification Document Type*/}
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                        <FormControl 
                            disabled={client.documentTypeSelectDisabled}
                            required
                            margin="normal"
                            fullWidth={true}>
                            <InputLabel htmlFor="documentType">Tipo Documento</InputLabel>
                            <Select
                                value={client.documentTypeId}
                                onChange={this.handleChangeSelectDocumentType("documentType")}
                                inputProps={{
                                    name: "documentType",
                                    id: "documentType",
                                }}
                                >
                                {
                                    idenfiticationDocumentType.data.map((item, index) => (
                                        <MenuItem key={index} value={item.cod_valor}>
                                            {item.des_valor_corto}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>                     
                    </Grid>
                    {/* Client - Identification Document Number*/}
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                        <TextField
                            error={client.documentNumberError}
                            fullWidth={true}
                            required
                            label="Número Documento"
                            type="text"
                            margin="normal"
                            color="default"
                            id="documentNumber"
                            name="documentNumber"
                            value={client.documentNumber}
                            onChange={this.handleChangeTextFieldDocumentNumber("documentNumber")}
                            autoFocus
                            InputProps={{
                                inputProps:{
                                    maxLength: client.documentTypeId === 100001 ? 8:9,
                                },
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <RecentActorsIcon color={client.documentNumberError? "secondary": "inherit"}  fontSize="small"/>
                                    </InputAdornment>
                                )
                            }}
                        />
                        <Fade>
                            <FormHelperText className={classes.documentNumberFormHelperText}>
                                <Typography 
                                    component="span"  
                                    variant="inherit"
                                    color={client.documentNumberShowMessageError? "secondary":"default"}
                                    >
                                    {
                                        client.documentNumberShowMessageError && 
                                            "La cantidad de dígitos no son los correctos" 
                                    }
                                </Typography>
                                <Typography 
                                    component="span" 
                                    variant="inherit"
                                    color={client.documentNumberShowMessageError? "secondary":"default"}
                                    >
                                    {client.documentNumber.toString().length}/{this.state.client.documentTypeId === 100001 ? 8 : 9}
                                </Typography>
                            </FormHelperText>
                        </Fade>
                    </Grid>
                </Grid>
                <Grid container spacing={8} className={classes.buttonWrapper}>
                    {/* Validate Client */}    
                    <Grid item xs={12} sm={12} md={4} lg={3}>
                        <Bounce>
                            <div className={classNames(classes.wrapper)}>
                                <Button
                                    variant="contained"
                                    type="submit" 
                                    className={classNames(classes.button)}
                                    disabled={this.state.validateClientButtonDisabled}
                                    color="primary" 
                                    size="small"
                                    fullWidth={true}>
                                    Continuar
                                    <SendIcon fontSize="small" className="ml-2" />
                                </Button>
                                { this.state.validateClientButtonDisabled && <CircularProgress size={24} className={classes.buttonProgress} />}
                            </div>
                        </Bounce>
                    </Grid>
                </Grid>
            </form>
        )
    }
}

const { func } = PropTypes;

ConsultClient.propTypes = {
    handleClientValidationForm: func.isRequired,
};

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ConsultClient)));

