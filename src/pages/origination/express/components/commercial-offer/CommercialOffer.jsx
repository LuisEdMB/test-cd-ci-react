import React, { Component } from "react";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";
// React Router
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// Components
import Slider from "@material-ui/lab/Slider";
import {
  Button,
  IconButton,
  Typography,
  Grid,
  FormControl,
  FormLabel,
  InputLabel,
  Select,
  MenuItem,
  CircularProgress,
  TextField,
  Tooltip,
  FormHelperText,
  FormGroup,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
// Colors
import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";
// Icons
import Label from "@material-ui/icons/Label";
import CancelIcon from "@material-ui/icons/Cancel";
import CloseIcon from "@material-ui/icons/Close";
import SendIcon from "@material-ui/icons/Send";
import CreditCardIcon from "@material-ui/icons/CreditCard";
import BookmarkBorderIcon from "@material-ui/icons/BookmarkBorder";
//import TrendingUpIcon from '@material-ui/icons/TrendingUp';
// Images
import CreditCard from "../../../../../components/CreditCard";
// Effects
import Fade from "react-reveal/Fade";
import Zoom from "react-reveal/Zoom";
import Bounce from "react-reveal/Bounce";
// Utils
import { getMoneyFormat } from "../../../../../utils/Utils";
// Actions
import { getConstantODC } from "../../../../../actions/generic/constant";
import { getCreditCardAssignment } from "../../../../../actions/generic/assign-credit-card";
import { getCreditCardColor } from "../../../../../actions/value-list/credit-card-color";
import { getCreditCardBrand } from "../../../../../actions/value-list/credit-card-brand";
import { getCreditCardBin } from "../../../../../actions/value-list/credit-card-bin";
import { getDisbursementType } from "../../../../../actions/value-list/disbursement-type";
import { getEntidadFinanciera } from "../../../../../actions/generic/entidad-financiera";
import { getPCTCommercialOffer } from "../../../../../actions/generic/pct";
//Imagenes
import logo from '../../../../../assets/media/images/jpg/CEC-logo-1.jpg'
// Utils
import {
  onlyNumberKeyCode,
  onlyTextKeyCode,
  defaultCCILengthInput,
  defaultvalCuotasLength, //, defaultFullMaxLengthInput
} from "../../../../../utils/Utils";
import { valueReducer } from "../../../../../utils/ValueReducer";
const styles = _ => ({
  root: {
    maxWidth: 900,
  },
  button: {
    textTransform: "none",
  },
  wrapper: {
    position: "relative",
  },
  cancelButtonProgress: {
    color: red[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
    textTransform: "none",
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
    textTransform: "none",
  },
  textWhite: {
    color: "white",
  },
  buttonActions: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
});

const mapStateToProps = (state) => {
  return {
    constantODC: state.constantODCReducer,
    creditCardColor: state.creditCardColorReducer,
    creditCardBrand: state.creditCardBrandReducer,
    creditCardBin: state.creditCardBinReducer,
    assignCreditCard: state.assignCreditCardReducer,
    disbursementType: state.disbursementTypeReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  const actions = {
    getCreditCardColor: bindActionCreators(getCreditCardColor, dispatch),
    getCreditCardBrand: bindActionCreators(getCreditCardBrand, dispatch),
    getCreditCardBin: bindActionCreators(getCreditCardBin, dispatch),
    getConstantODC: bindActionCreators(getConstantODC, dispatch),
    getEntidadFinanciera: bindActionCreators(getEntidadFinanciera, dispatch),
    getPCTCommercialOffer: bindActionCreators(getPCTCommercialOffer, dispatch),
    getCreditCardAssignment: bindActionCreators(
      getCreditCardAssignment,
      dispatch
    ),
    getDisbursementType: bindActionCreators(getDisbursementType, dispatch),
  };
  return actions;
};

class CommercialOffer extends Component {
  state = {
    client: {
      documentTypeId: 100001,
      documentType: "DNI", // 100001 DNI | 100002 CE
      documentTypeInternalValue: "DU",
      documentNumber: "",
      firstName: "",
      lastName: "",
      gender: "",
      maritalStatusInternalValue: "",
      nationality: "",
      email: "",
      cellphone: "",
      birthday: "",
      businessName: "",
      names: "",
      surnames: "",
      shortName: "",
      fullName: "",
      telephone: "(   )    -   ",
      homeAddress: {
        department: "",
        departmentId: "",
        provinceId: "",
        districtId: "",
        via: "",
        nameVia: "",
        number: "",
        building: "",
        inside: "",
        mz: "",
        lot: "",
        zone: "",
        nameZone: "",
        reference: "",
      },
      workAddress: {
        department: "",
        departmentId: "",
        provinceId: "",
        districtId: "",
        via: "",
        nameVia: "",
        number: "",
        building: "",
        inside: "",
        mz: "",
        lot: "",
        zone: "",
        nameZone: "",
        reference: "",
      },
      jobTitle: "",
      workTelephone: "(   )    -   ",
      extra: {
        effectiveWithdrawal: "",
        paymentDateInternalValue: "",
      },
    },
    creditCard: {
      type: "Clásica",
      brandId: 200001,
      brand: "MasterCard", //MasterCard | Visa
      colorId: "",
      colorAux: "",
      color: "",
      productId: 10001,
      bin: "",
      name: "CENCOSUD MASTERCARD",
      colorDisabled: false,
      colorError: false,
    },
    lineAvailable: {
      value: 1200,
      min: 0,
      max: 3000,
    },
    segment: "",
    effectiveProvision: 77.7,
    readOnly: true,
    commercialOfferButtonDisabled: false,

    lineSAE: {
      value: 0,
      min: 0,
      max: 500,
      saeError: false,
    },
    maxAmount: 3000,
    pctSAE1: "",
    pctSAE2: "",
    pctSAE: "",

    isWithoutMembresy: false,

    isSAEShow: true,
    isSAE: false,
    isSAEDisabled: false,
    isSAESliderDisabled: true,

    disbursementType: "",
    disbursementTypeId: 0,
    disbursementTypeDisabled: false,
    CCI: "",
    CCIError: false,
    CCIRequired: true,
    CCIDisabled: false,
    findedEntidadFinanciera: false,
    nameEntidadFinanciera: "",
    errorEntidadFinanciera: false,
    errorMessageEntidadFinanciera: "",

    numberFees: "",
    numberFeesError: false,
    numberFeesRequired: true,
    numberFeesDisabled: false,
    numberMaxFees: 36,
    numberMinFees: 12,

    tcea: "",
    tceaError: false,

    feeAmount: "",
    feeAmountError: false,
    feeAmountRequired: true,
    feeAmountDisabled: false,

    percentageAmounts: 0.5,
  };
  // Notistack
  getNotistack(message, variant = "default", duration = 6000) {
    let select = "default";
    switch (variant) {
      case "error":
        select = variant;
        break;
      case "success":
        select = variant;
        break;
      case "warning":
        select = variant;
        break;
      case "info":
        select = variant;
        break;
      default:
        select = variant;
        break;
    }
    // Notistack
    this.props.enqueueSnackbar(message, {
      variant: select,
      autoHideDuration: duration,
      action: (
        <IconButton>
          <CloseIcon size="small" className="text-white" color="inherit" />
        </IconButton>
      ),
    });
  }

  componentWillMount() {
    let { cda, client, sae, isSAEProcess, lineAvailable } = this.props;
    if (cda && client) {
      // Set Data Client
      let fullName = `${client.fullName}`;
      let segment = cda.segmento ? cda.segmento : "";
      let effectiveProvision =
        client.extra.effectiveWithdrawal === "0"
          ? 0
          : parseFloat(cda.porcentajeDisponibleEfectivo.toString());
      // SAE
      let pctSAE1 = "";
      let pctSAE2 = "";
      let CCIDisabled = true;
      let numberFeesDisabled = true;
      let disbursementTypeDisabled = true;
      let tcea = "";

      if (isSAEProcess) {
        if (sae && Object.keys(sae).length > 0) {
          pctSAE1 = sae.pctSAE1 ? sae.pctSAE1 : "";
          pctSAE2 = sae.pctSAE2 ? sae.pctSAE2 : "";
          disbursementTypeDisabled = false;
          CCIDisabled = false;
          numberFeesDisabled = false;
          tcea = "";
        }
      }

      this.setState((state) => ({
        ...state,
        client: {
          ...client,
          fullName: fullName,
        },
        lineAvailable: { ...lineAvailable },
        effectiveProvision: effectiveProvision,
        segment: segment,
        isSAE: isSAEProcess,
        isSAEDisabled: !isSAEProcess,
        isSAESliderDisabled: !isSAEProcess,
        isSAEShow: isSAEProcess,
        disbursementTypeDisabled: disbursementTypeDisabled,
        CCIDisabled: CCIDisabled,
        numberFeesDisabled: numberFeesDisabled,
        //descriptionDisabled: descriptionDisabled,
        pctSAE1: pctSAE1,
        pctSAE2: pctSAE2,
        tcea,
      }));
    }
  }
  componentDidMount() {
    // Get Service
    this.props.getCreditCardColor();
    this.props.getCreditCardBrand();
    this.props.getCreditCardBin();
    this.props.getConstantODC();
    if (!this.props.isSAEProcess) {
      this.props.getCreditCardAssignment();
    }
    this.props.getDisbursementType();
  }
  componentDidUpdate = (prevProps, prevState) => {
    // Identification Document Type - Before
    if (
      prevProps.idenfiticationDocumentType !==
      this.props.idenfiticationDocumentType
    ) {
      // Identification Document Type - Error Service
      if (
        !this.props.idenfiticationDocumentType.loading &&
        this.props.idenfiticationDocumentType.response &&
        !this.props.idenfiticationDocumentType.success
      ) {
        this.getNotistack(
          `Tipo Documento: ${this.props.idenfiticationDocumentType.error}`,
          "error"
        );
      }
      // Identification Document Type - Error Service Connectivity
      else if (
        !this.props.idenfiticationDocumentType.loading &&
        !this.props.idenfiticationDocumentType.response &&
        !this.props.idenfiticationDocumentType.success
      ) {
        this.getNotistack(
          `Tipo Documento: ${this.props.idenfiticationDocumentType.error}`,
          "error"
        );
      }
    }
    // Credit Card Constant - Before
    if (prevProps.constantODC !== this.props.constantODC) {
      // Credit Card Constant - Error Service
      if (
        !this.props.constantODC.loading &&
        this.props.constantODC.response &&
        this.props.constantODC.success
      ) {
        let { data } = this.props.constantODC;
        let lineaMinTC = data.find(
          (item) => item.des_abv_constante === "LINEA_MIN_TC"
        );
        let montoMinSae = data.find(
          (item) => item.des_abv_constante === "MONTO_MIN_SAE"
        );
        let numberMaxFees = data.find(
          (item) => item.des_abv_constante === "NRO_MAX_CUOTAS"
        );
        let numberMinFees = data.find(
          (item) => item.des_abv_constante === "NRO_MIN_CUOTAS"
        );
        this.setState((state) => ({
          ...state,
          lineAvailable: {
            ...state.lineAvailable,
            min: this.props.isSAEProcess ? montoMinSae.valor_numerico : lineaMinTC.valor_numerico,
          },
          numberMaxFees: numberMaxFees.valor_numerico,
          numberMinFees: numberMinFees.valor_numerico,
        }));
      }
      // Credit Card Constant - Error Service
      else if (
        !this.props.constantODC.loading &&
        this.props.constantODC.response &&
        !this.props.constantODC.success
      ) {
        this.getNotistack(
          `Constante: ${this.props.constantODC.error}`,
          "error"
        );
      }
      // Credit Card Constant - Error Service Connectivity
      else if (
        !this.props.constantODC.loading &&
        !this.props.constantODC.response &&
        !this.props.constantODC.success
      ) {
        this.getNotistack(
          `Constante: ${this.props.constantODC.error}`,
          "error"
        );
      }
    }

    // Credit Card Color - Before
    if (prevProps.creditCardColor !== this.props.creditCardColor) {
      if (
        !this.props.creditCardColor.loading &&
        this.props.creditCardColor.response &&
        this.props.creditCardColor.success
      ) {
        // Enabled Color
        // const color = this.props.creditCardColor.data.find(item => item.val_orden === 1);
        // if(!this.selectProduct(this.state.creditCard.productId)){
        //     this.setState(state => ({
        //         ...state,
        //         creditCard:{
        //             ...state.creditCard,
        //             colorId: color.cod_valor,
        //             colorAux: color.des_auxiliar,
        //             color: color.des_valor
        //         }
        //     }));
        // }
      }
      // Credit Card Color - Error Service
      else if (
        !this.props.creditCardColor.loading &&
        this.props.creditCardColor.response &&
        !this.props.creditCardColor.success
      ) {
        this.getNotistack("Color: Error de Servicio", "error");
      }
      // Credit Card Color - Error Service Connectivity
      else if (
        !this.props.creditCardColor.loading &&
        !this.props.creditCardColor.response &&
        !this.props.creditCardColor.success
      ) {
        this.getNotistack(
          `Color: ${this.props.creditCardColor.error}`, "error");
      }
    }
    // Credit Card Brand - Before
    if (prevProps.creditCardBrand !== this.props.creditCardBrand) {
      // Credit Card Brand - Error Service
      if (
        !this.props.creditCardBrand.loading &&
        this.props.creditCardBrand.response &&
        !this.props.creditCardBrand.success
      ) {
        this.getNotistack("Marca: Error de Servicio", "error");
      }
      // Credit Card Brand - Error Service Connectivity
      else if (
        !this.props.creditCardBrand.loading &&
        !this.props.creditCardBrand.response &&
        !this.props.creditCardBrand.success
      ) {
        this.getNotistack(
          `Marca: ${this.props.creditCardBrand.error}`,
          "error"
        );
      }
    }
    // Credit Card Bin - Before
    if (prevProps.creditCardBin !== this.props.creditCardBin) {
      // Credit Card Bin - Success Service
      if (
        !this.props.creditCardBin.loading &&
        this.props.creditCardBin.response &&
        this.props.creditCardBin.success
      ) {
        let { lineAvailable, creditCard } = this.state;
        let creditCardProduct = this.getCreditCardProduct(
          lineAvailable.value,
          creditCard.brandId
        );
        this.validateCreditCard(creditCardProduct, lineAvailable.value);
      }
      // Credit Card Bin - Error Service
      else if (
        !this.props.creditCardBin.loading &&
        this.props.creditCardBin.response &&
        !this.props.creditCardBin.success
      ) {
        this.getNotistack("Bin: Error de Servicio", "error");
      }
      // Credit Card Bin - Error Service Connectivity
      else if (
        !this.props.creditCardBin.loading &&
        !this.props.creditCardBin.response &&
        !this.props.creditCardBin.success
      ) {
        this.getNotistack(`Bin: ${this.props.creditCardBin.error}`, "error");
      }
    }
    // Commercial Offer
    if (prevProps.commercialOffer !== this.props.commercialOffer) {
      // Enabled Button - Validate Identity Client
      if (this.props.commercialOffer.loading) {
        this.setState({
          ...this.state,
          commercialOfferButtonDisabled: true,
          cancelButtonDisabled: true,
        });
      }
      if (
        !this.props.commercialOffer.loading &&
        this.props.commercialOffer.response &&
        this.props.commercialOffer.success
      ) {
        this.getNotistack("Consultar Correcta, 4to Paso Ok!", "success");
        setTimeout(() => {
          this.props.handleNext();
        }, 1500);
      } else if (
        !this.props.commercialOffer.loading &&
        this.props.commercialOffer.response &&
        !this.props.commercialOffer.success
      ) {
        this.setState({
          ...this.state,
          commercialOfferButtonDisabled: false,
          cancelButtonDisabled: false,
        });
        // Notistack
        this.getNotistack(this.props.commercialOffer.error, "error");
      } else if (
        !this.props.commercialOffer.loading &&
        !this.props.commercialOffer.response &&
        !this.props.commercialOffer.success
      ) {
        this.setState({
          ...this.state,
          commercialOfferButtonDisabled: false,
          cancelButtonDisabled: false,
        });
        // Notistack
        this.getNotistack(this.props.commercialOffer.error, "error");
      }
    }
    // Activity - Cancel
    if (prevProps.cancelActivity !== this.props.cancelActivity) {
      if (this.props.cancelActivity.loading) {
        this.setState((state) => ({
          ...state,
          cancelButtonDisabled: true,
          commercialOfferButtonDisabled: true,
        }));
      }
      if (
        !this.props.cancelActivity.loading &&
        this.props.cancelActivity.response &&
        this.props.cancelActivity.success
      ) {
        this.props.handleCancelExpress(); // Cancel Father

        this.setState((state) => ({
          ...state,
          cancelButtonDisabled: false,
          commercialOfferButtonDisabled: false,
        }));
      } else if (
        !this.props.cancelActivity.loading &&
        this.props.cancelActivity.response &&
        !this.props.cancelActivity.success
      ) {
        // Notistack
        this.getNotistack(this.props.cancelActivity.error, "error");
        this.setState((state) => ({
          ...state,
          cancelButtonDisabled: false,
          commercialOfferButtonDisabled: false,
        }));
      } else if (
        !this.props.cancelActivity.loading &&
        !this.props.cancelActivity.response &&
        !this.props.cancelActivity.success
      ) {
        // Notistack
        this.getNotistack(this.props.cancelActivity.error, "error");
        this.setState((state) => ({
          ...state,
          cancelButtonDisabled: false,
          commercialOfferButtonDisabled: false,
        }));
      }
    }
    // Assign Credit Card
    if (prevProps.assignCreditCard !== this.props.assignCreditCard) {
      if (
        !this.props.assignCreditCard.loading &&
        this.props.assignCreditCard.response &&
        this.props.assignCreditCard.success
      ) {
        let { data } = this.props.assignCreditCard;
        this.setState((state) => ({
          ...state,
          creditCard: {
            ...state.creditCard,
            brand: data.des_valor,
            brandId: data.cod_valor,
          },
        }));
      }
      // Credit Card Brand - Error Service
      else if (
        !this.props.assignCreditCard.loading &&
        this.props.assignCreditCard.response &&
        !this.props.assignCreditCard.success
      ) {
        this.getNotistack(
          `Marca: ${this.props.assignCreditCard.error}`,
          "error"
        );
      }
      // Credit Card Brand - Error Service Connectivity
      else if (
        !this.props.assignCreditCard.loading &&
        !this.props.assignCreditCard.response &&
        !this.props.assignCreditCard.success
      ) {
        this.getNotistack(
          `Marca: ${this.props.assignCreditCard.error}`,
          "error"
        );
      }
    }

    if (prevProps.disbursementType !== this.props.disbursementType) {
      if (
        !this.props.disbursementType.loading &&
        this.props.disbursementType.response &&
        this.props.disbursementType.success
      ) {
        const disbursementType = this.props.disbursementType.data.find(
          (item) => item.val_orden === 1
        );
        this.setState((state) => ({
          ...state,
          disbursementTypeId: disbursementType.cod_valor,
        }));
      }
      // Credit Card Brand - Error Service
      else if (
        !this.props.disbursementType.loading &&
        this.props.disbursementType.response &&
        !this.props.disbursementType.success
      ) {
        this.getNotistack(
          `Tipo Desembolso: ${this.props.disbursementType.error}`,
          "error"
        );
      }
      // Credit Card Brand - Error Service Connectivity
      else if (
        !this.props.disbursementType.loading &&
        !this.props.disbursementType.response &&
        !this.props.disbursementType.success
      ) {
        this.getNotistack(
          `Tipo Desembolso: ${this.props.disbursementType.error}`,
          "error"
        );
      }
    }
  };
  // Client Full Name - Event Change Select
  handleChangeTextFieldClient = (e) => {
    let { value, name } = e.target;
    this.setState((state) => ({
      client: {
        ...state.client,
        [name]: value,
      },
    }));
  };
  // CreditCard - Brand - Event Change Select
  handleChangeSelectCreditCardBrand = (e) => {
    const { creditCard, lineAvailable, isWithoutMembresy } = this.state;
    const { value } = e?.target || e;
    const product = this.getCreditCardProduct(lineAvailable.value, value);
    if (!this.selectProduct(product.productId)) {
      this.setState((state) => ({
        ...state,
        creditCard: {
          ...creditCard,
          ...product,
          colorId: isWithoutMembresy ? product.colorId : '',
          colorAux: isWithoutMembresy ? product.colorAux : '',
          color: isWithoutMembresy ? product.color : ''
        },
      }), _ => this.validateCreditCard(this.state.creditCard, this.state.lineAvailable.value));
    }
    else{
      this.setState(state => ({
        ...state,
        creditCard:{
          ...creditCard,
          ...product,
          color: product.color,
          colorAux: product.colorAux,
          colorId: product.colorId,
          brandId: value
        }
      }), _ => this.validateCreditCard(this.state.creditCard, this.state.lineAvailable.value));
    }
  };

  // CreditCard - Color - Event Change Select
  handleChangeSelectCreditCardColor = (objectName, name) => (e) => {
    let object = this.props.creditCardColor.data.find(
      (option) => option.cod_valor === e.target.value
    );
    let nameId = `${name}Id`;
    let nameAux = `${name}Aux`;
    let nameError = `${name}Error`;
    let { value } = e.target;

    if (object) {
      this.setState((state) => ({
        [objectName]: {
          ...state[objectName],
          [name]: object.des_valor ? object.des_valor : 0,
          [nameAux]: object.des_auxiliar ? object.des_auxiliar : "",
          [nameId]: value ? value : 0,
          [nameError]: value === "",
        },
      }));
    }
  };

  // CreditCard - Effective Provision - Event Change TextField
  handleChangeTextFieldEffectiveProvision = (e) => {
    this.setState((state) => ({
      effectiveProvision: e.target.value,
    }));
  };

  // CreditCard - Line Available - Event Change Slider
  handleChangeLineAvailable = (e, value) => {
    let creditCard = this.getCreditCardProduct(
      value,
      this.state.creditCard.brandId
    );
    this.validateCreditCard(creditCard, value);
  };

  // CreditCard - Line Available - Event Change TextField
  handleChangeTextFieldLineAvailable = (e) => {
    let value = e.target.value ? parseInt(e.target.value) : 0;
    if (value <= this.state.lineAvailable.max) {
      let creditCard = this.getCreditCardProduct(
        value,
        this.state.creditCard.brandId
      );
      this.validateCreditCard(creditCard, value);
    }
  };
  handleBlurTextFieldLineAvailable = (e) => {
    let value = e.target.value !== "" ? parseInt(e.target.value) : 0;
    let { min } = this.state.lineAvailable;
    if (value < min) {
      let creditCard = this.getCreditCardProduct(
        min,
        this.state.creditCard.brandId
      );
      this.validateCreditCard(creditCard, min);
    }
  };

  // Send - Event Submit
  handleSubmitCommercialOfferForm = (e) => {
    e.preventDefault();
    const { isSAEProcess } = this.props;
    let {
      lineAvailable,
      pctSAE1,
      CCI,
      disbursementTypeId,
      nameEntidadFinanciera,
      numberFees,
      feeAmount,
      tcea,

      numberFeesError,
      feeAmountError,
      errorEntidadFinanciera,
      CCIError,
      tceaError,
    } = this.state;
    let colorError = false;
    let saeError = false;

    if (lineAvailable.value >= lineAvailable.min) {
      let { client, creditCard, effectiveProvision, segment } = this.state;

      //  Validation Color
      colorError = !creditCard.colorId && !this.selectProduct(creditCard.productId);
      let finalMaxAmount = lineAvailable.max;
      // Set data
      let pctSAE = "";
      if (isSAEProcess) {
        // Lines
        pctSAE = pctSAE1;
        // Validate CCI - Both
        CCIError = CCIError || (disbursementTypeId !== 380001 && CCI.length === defaultCCILengthInput);
        numberFeesError = numberFeesError || numberFees < this.state.numberMinFees || numberFees > this.state.numberMaxFees;
        tceaError = tceaError || tcea < 1 || tcea > 200;
        feeAmountError = feeAmountError || feeAmount < 1 || feeAmount > 9999999.99;
        errorEntidadFinanciera = errorEntidadFinanciera || (nameEntidadFinanciera !== "" && disbursementTypeId !== 380001);
        saeError = lineAvailable.value < lineAvailable.min;
      }

      if (
        !CCIError &&
        !colorError &&
        !saeError &&
        !numberFeesError &&
        !feeAmountError &&
        !errorEntidadFinanciera &&
        !tceaError
      ) {
        let data = {
          finalLineAvailable: lineAvailable.value,
          finalLineSAE: isSAEProcess ? lineAvailable.value : 0,
          finalMaxAmount: finalMaxAmount,
          pctSAE: pctSAE,
          isSAE: isSAEProcess,
          client: {
            ...client,
          },
          creditCard: {
            name: creditCard.name,
            brandId: creditCard.brandId,
            brand: creditCard.brand,
            productId: creditCard.productId,
            colorId: creditCard.colorId !== "" ? creditCard.colorId : 0,
            colorAux: creditCard.colorAux,
            color: creditCard.color,
            type: creditCard.type,
            bin: creditCard.bin,
          },
          finalEffectiveProvision: effectiveProvision,
          segment: segment,
          CCI: CCI,
          disbursementTypeId: disbursementTypeId,
          description: "",
          transferAmount: CCI ? lineAvailable.value : 0,
          effectiveAmount: CCI ? 0 : lineAvailable.value,
          numberFees: numberFees,
          feeAmount: feeAmount,
          tcea: tcea,
        };
        this.props.handleCommercialOffer(data);
      } else {
        if (colorError) {
          this.getNotistack("Color: Verificar porfavor", "warning");
        }
        if (CCIError) {
          this.getNotistack("CCI: Verificar porfavor", "warning");
        }
        if (saeError) {
          this.getNotistack(
            `SAE: El monto debe ser mayor o igual a ${lineAvailable.min}`,
            "warning"
          );
        }
        if (numberFeesError) {
          this.getNotistack(
            `Número de Cuotas: El número de cuotas debe de ser mayor o igual a ${this.state.numberMinFees} y menor o igual a ${this.state.numberMaxFees}`,
            "warning"
          );
        }
        if (feeAmountError) {
          this.getNotistack(
            `Número de Cuotas: El monto de cada cuota debe de ser como máximo S/. 9 999 999.99`,
            "warning"
          );
        }
        if (errorEntidadFinanciera) {
          this.getNotistack(
            `Banco no válido para el desembolso del Crédito Efectivo Cencosud`,
            "warning"
          );
        }
        if (tceaError) {
          this.getNotistack(
            "TCEA: El valor de % debe de ser mayor o igual 1 y menor o igual a 200",
            "warning"
          );
        }

        this.setState((state) => ({
          ...state,
          creditCard: {
            ...state.creditCard,
            colorError: colorError,
            // CCIError: CCIError
          },
          lineSAE: {
            ...state.lineSAE,
            saeError: saeError,
          },
          CCIError,
          numberFeesError,
          feeAmountError,
          errorEntidadFinanciera,
          tceaError,
        }));
      }
    }
  };
  // Validate Credit Card
  validateCreditCard = (data, value) => {
    const { creditCard, isWithoutMembresy } = this.state;
    let { colorId, colorAux, color } = creditCard;
    let colorDisabled = true;

    if (!this.selectProduct(data.productId)) {
      colorDisabled = false;
      colorAux = colorId ? colorAux : "";
      if (this.props.isSAEProcess) {
        colorId = data.colorId;
        colorAux = data.colorAux;
        color = data.color;
      }
    } else {
      colorId = "";
      colorAux = data.colorAux;
    }

    this.setState((state) => ({
      lineAvailable: {
        ...state.lineAvailable,
        value: value,
      },
      creditCard: {
        ...state.creditCard,
        ...data,
        colorId: colorId,
        colorAux: colorAux,
        color: color,
        colorDisabled: colorDisabled || isWithoutMembresy,
      },
    }));
  };
  // Select Product Id - Table
  selectProduct(productId) {
    switch (productId) {
      case 10001:
        return false; // Clasic Visa
      case 10004:
        return false; // Clasic MasterCard
      case 10008:
        return false; // Simple Visa
      case 10009:
        return false; // Efectivo Cencosud
      default:
        return true;
    }
  }
  // Get Credit Card
  getCreditCardProduct = (value, brandId = 200001) => {
    const { isSAEProcess } = this.props;
    const { creditCard, isWithoutMembresy } = this.state;
    let color = "", colorAux = "", colorId = "";
    let product;
    if (isSAEProcess) {
      brandId = 200003;
      colorId = 250005;
      product = this.props.creditCardBin.data.find((item) => {
        return (
          value >= item.limite_min &&
          value <= item.limite_max &&
          item.cod_producto === 10009
        );
      });
    }
    else if (isWithoutMembresy) {
      colorId = 250004;
      colorAux = "#E9E9E9";
      color = "Sin Color";
      product = this.props.creditCardBin.data.find((item) => {
        return (
          value >= item.limite_min &&
          value <= item.limite_max &&
          item.cod_valor_marca === brandId.toString() &&
          item.cod_producto === 10008
        );
      });
    } else {
      product = this.props.creditCardBin.data.find((item) => {
        return (
          value >= item.limite_min &&
          value <= item.limite_max &&
          item.cod_valor_marca === brandId.toString() &&
          item.cod_producto !== 10008
        );
      });
    }
    if (product) {
      return {
        brandId: brandId,
        colorId: colorId,
        colorAux: !this.selectProduct(product.cod_producto)
          ? colorAux
          : product.cod_color,
        color: color,
        brand: product.des_marca_pro,
        productId: product.cod_producto,
        name: product.des_nom_prod,
        bin: product.cod_bin_pro,
        type: product.des_aux_pro,
        minLimit: product.limite_min,
        maxLimit: product.limite_max,
      };
    }
    return creditCard;
  };
  // Submit - Origination
  handleCancelExpress = (e) => {
    e.preventDefault();
    this.props.handleOpenModalCancelExpress();
  };
  // Only Text
  handleKeyPressTextFieldOnlyText = (name) => (e) => {
    let code = e.which ? e.which : e.keyCode;
    if (!onlyTextKeyCode(code)) {
      e.preventDefault();
    }
  };
  // Only Number
  handleKeyPressTextFieldOnlyNumber = (name) => (e) => {
    let code = e.which ? e.which : e.keyCode;
    if (!onlyNumberKeyCode(code)) {
      e.preventDefault();
    }
  };
  handleOPasteTextFieldOnlyNumber = (name) => (e) => {
    var clipboardData, pastedData;
    const nameError = `${name}Error`;

    // Stop data actually being pasted into div
    e.stopPropagation();
    e.preventDefault();

    // Get pasted data via clipboard API
    clipboardData = e.clipboardData || window.clipboardData;
    pastedData = clipboardData.getData('Text');

    const value = pastedData.match(/[0-9]+/sgui).join('').replace(/\s+/gi, ' ')

    if (
      value.length >= 3 &&
      value.substring(0, 3) !== this.state[name].substring(0, 3)
    ) {
      this.props.getEntidadFinanciera(value).then((response) => {
        if (!response.response) {
          const { data } = response;
          if (data && data.success) {
            const { entidadFinanciera } = data;
            this.setState({
              nameEntidadFinanciera: `${entidadFinanciera.des_entidad_financiera_detalle
                } ${entidadFinanciera.des_abv_entidad
                  ? `(${entidadFinanciera.des_abv_entidad})`
                  : ""
                }`,
              findedEntidadFinanciera: true,
              errorMessageEntidadFinanciera: `${entidadFinanciera.flg_afiliado
                ? "Banco válido para el desembolso del SAE"
                : "Banco no válido para el desembolso del SAE"
                }`,
              errorEntidadFinanciera: !entidadFinanciera.flg_afiliado,
            });
          } else {
            this.setState({
              nameEntidadFinanciera: "",
              findedEntidadFinanciera: false,
              errorMessageEntidadFinanciera:
                "Banco no válido para el desembolso del SAE", //data.errorMessage,
              errorEntidadFinanciera: true,
            });
          }
        } else {
          this.setState({
            nameEntidadFinanciera: "",
            findedEntidadFinanciera: false,
            errorMessageEntidadFinanciera:
              "Banco no válido para el desembolso del SAE", //response.response.data.errorMessage,
            errorEntidadFinanciera: true,
          });
        }
      });
    }

    this.setState((state) => ({
      ...state,
      [name]: value,
      [nameError]: value.length !== defaultCCILengthInput,
    }));
  }
  // number and dot
  handleKeyPressTextFieldOnlyNumberDot = (name) => (e) => {
    var rgx = /^[0-9]*\.?[0-9]*$/;
    return e.toString().match(rgx);
  };

  // SAE - Disbursement Type
  handleChangeSelectDisbursementType = (name) => (e) => {
    const { value } = e.target;
    const { innerText } = e.currentTarget;
    const nameId = `${name}Id`;
    const nameError = `${name}Error`;

    this.setState((state) => ({
      ...state,
      [nameId]: value,
      [name]: innerText,
      [nameError]: value !== "" ? false : true,
      CCI: value === 380001 ? state.CCI : "",
      CCIDisabled: value === 380001 || value === 380003 ? false : true,
      CCIRequired: value === 380001 || value === 380003 ? true : false,
      CCIError: value === 380001,

      findedEntidadFinanciera: false,
      nameEntidadFinanciera:
        value === 380001 ? state.nameEntidadFinanciera : "",
      errorEntidadFinanciera: value === 380001,
      errorMessageEntidadFinanciera: "",
    }));
  };
  changeWithoutMembresy = (checked) => {
    const { creditCard } = this.state;
    let { brandId } = creditCard;
    if (checked) {
      brandId = 200002;
    }
    this.setState(
      {
        isWithoutMembresy: checked,
      }, _ => this.handleChangeSelectCreditCardBrand({ value: brandId })
    );
  }
  // Sin Membresia - CheckBox
  handleChangeWithoutMembresy = (e) => {
    const { checked } = e.target;
    this.changeWithoutMembresy(checked);
  };

  // SAE CCI
  handleChangeCCI = (name) => (e) => {
    const { value } = e.target;
    const nameError = `${name}Error`;
    // search
    if (
      value.length >= 3 &&
      value.substring(0, 3) !== this.state[name].substring(0, 3)
    ) {
      this.props.getEntidadFinanciera(value).then((response) => {
        if (!response.response) {
          const { data } = response;
          if (data && data.success) {
            const { entidadFinanciera } = data;
            this.setState({
              nameEntidadFinanciera: `${entidadFinanciera.des_entidad_financiera_detalle
                } ${entidadFinanciera.des_abv_entidad
                  ? `(${entidadFinanciera.des_abv_entidad})`
                  : ""
                }`,
              findedEntidadFinanciera: true,
              errorMessageEntidadFinanciera: `${entidadFinanciera.flg_afiliado
                ? "Banco válido para el desembolso de EC"
                : "Banco no válido para el desembolso de EC"
                }`,
              errorEntidadFinanciera: !entidadFinanciera.flg_afiliado,
            });
          } else {
            this.setState({
              nameEntidadFinanciera: "",
              findedEntidadFinanciera: false,
              errorMessageEntidadFinanciera:
                "Banco no válido para el desembolso de EC",
              errorEntidadFinanciera: true,
            });
          }
        } else {
          this.setState({
            nameEntidadFinanciera: "",
            findedEntidadFinanciera: false,
            errorMessageEntidadFinanciera:
              "Banco no válido para el desembolso de EC",
            errorEntidadFinanciera: true,
          });
        }
      });
    }

    this.setState((state) => ({
      ...state,
      [name]: value,
      [nameError]: value.length !== defaultCCILengthInput,
    }));
  };

  handleChangeNumberFees = (name) => (e) => {
    const { value } = e.target;
    const nameError = `${name}Error`;

    this.setState({
      [name]: value,

      [nameError]: value < this.state.numberMinFees || value > this.state.numberMaxFees,
    });
  };

  handleChangeTcea = (name) => (e) => {
    const { value } = e.target;
    const nameError = `${name}Error`;

    //this.value=this.value.toString().split(".").map((el,i)=>i?el.split("").slice(0,2).join(""):el).join(".");

    this.setState({
      [name]: value,
      [nameError]: value < 0 || value > 200,
    });
  };

  handleChangeFeeAmount = (name) => (e) => {
    const { value } = e.target;
    const nameError = `${name}Error`;
    this.setState({
      [name]: value, //parseFloat(value).toFixed(2),
      [nameError]: value < 1 || value > 9999999.99,
    });
  };

  render() {
    let {
      classes,
      creditCardColor,
      creditCardBrand,
      disbursementType,
      isSAEProcess
    } = this.props;

    let {
      creditCard,
      client,
      lineAvailable,
      readOnly,
      lineSAE,
      disbursementTypeId,
    } = this.state;
    const pctSae = this.state.isSAE
      ? lineSAE.value > 0.5 * lineSAE.max
        ? this.state.pctSAE2
        : this.state.pctSAE1
      : "No Aplica";
    //const tceaSae = this.state.tcea;
    return (
      <Grid container spacing={8} className="p-3">
        <form
          method="post"
          className="w-100"
          onSubmit={this.handleSubmitCommercialOfferForm}
          autoComplete="off"
        >
          {/* Form - CreditCard */}
          <Grid container spacing={8}>
            <Grid item xs={12}>
              <Grid
                container
                spacing={32}
                className={classNames(
                  "d-flex justify-content-center p-2 p-sm-5 mb-2 rounded"
                )}
              >
                {/* border border-light-gray  */}
                <Grid
                  container
                  spacing={32}
                  className={classNames(classes.root, "m-0 m-sm-3")}
                >
                  {/* Form - Data */}
                  <Grid container spacing={8}>
                    {/* Document Type */}
                    <Grid item xs={12} sm={12} md={3} lg={3}>
                      <TextField
                        fullWidth={true}
                        required
                        label="Tipo Documento"
                        type="text"
                        margin="normal"
                        color="default"
                        value={client.documentType}
                        name="documentType"
                        onChange={this.handleChangeTextFieldClient}
                        onKeyPress={this.handleKeyPressTextFieldOnlyText(
                          "documentType"
                        )}
                        autoFocus
                        InputProps={{
                          readOnly: readOnly,
                        }}
                      />
                    </Grid>
                    {/* Document Number */}
                    <Grid item xs={12} sm={12} md={3} lg={3}>
                      <TextField
                        fullWidth={true}
                        required
                        label="Número Documento"
                        type="text"
                        margin="normal"
                        color="default"
                        value={client.documentNumber}
                        onChange={this.handleChangeTextFieldClient}
                        name="documentNumber"
                        InputProps={{
                          readOnly: readOnly,
                        }}
                      />
                    </Grid>
                    {/* FullName */}
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      <TextField
                        fullWidth={true}
                        required
                        label="Nombre Completo"
                        type="text"
                        margin="normal"
                        color="default"
                        value={client.fullName}
                        name="fullName"
                        onChange={this.handleChangeTextFieldClient}
                        onKeyPress={this.handleKeyPressTextFieldOnlyText(
                          "fullName"
                        )}
                        InputProps={{
                          readOnly: readOnly,
                        }}
                      />
                    </Grid>
                  </Grid>
                  {isSAEProcess && (
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      {/* Credit Card */}
                      <Grid
                        item
                        xs={12}
                        className="d-flex justify-content-center"
                      >
                        <div style={{ minHeight: 230 }}>
                          <div>
                            <img alt="logo" src={logo} />
                          </div>
                        </div>
                      </Grid>
                    </Grid>)}
                  {!isSAEProcess && (
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      {/* Credit Card */}
                      <Grid
                        item
                        xs={12}
                        className="d-flex justify-content-center"
                      >
                        <div style={{ minHeight: 230 }}>
                          {!this.props.creditCardBin.loading ? (
                            <Fade>
                              <CreditCard
                                client={client.shortName}
                                type={creditCard.type}
                                customColor={creditCard.colorAux}
                                bin={creditCard.bin}
                                image={creditCard.productId !== 10008}
                                brand={creditCard.brandId === 200001 ? 0 : 1}
                              />
                            </Fade>
                          ) : (
                            <div>
                              <CircularProgress />
                            </div>
                          )}
                        </div>
                      </Grid>
                    </Grid>)}
                  <Grid item xs={12} sm={12} md={6} lg={6}>
                    {!isSAEProcess ?
                      (<Grid container spacing={8}>
                        {/* Effective Provision */}
                        <Grid item xs={12} sm={4}>
                          <TextField
                            fullWidth={true}
                            required
                            label="Disp. Efectivo"
                            type="text"
                            margin="normal"
                            color="default"
                            name="effectiveProvision"
                            value={`${this.state.effectiveProvision.toFixed(2)}%`}
                            onChange={
                              this.handleChangeTextFieldEffectiveProvision
                            }
                            onKeyPress={this.handleKeyPressTextFieldOnlyNumber(
                              "effectiveProvision"
                            )}
                            inputProps={{
                              style: { textAlign: "right" },
                              readOnly: readOnly,
                            }}
                          />
                        </Grid>
                        {/* Brand */}
                        <Grid item xs={12} sm={4}>
                          <FormControl required margin="normal" fullWidth={true}>
                            <InputLabel htmlFor="brand">Marca Tarjeta</InputLabel>
                            <Select
                              value={creditCard.brandId}
                              disabled={this.state.isWithoutMembresy}
                              inputProps={{ id: "brand" }}
                              onChange={this.handleChangeSelectCreditCardBrand}
                            >
                              {creditCardBrand.data.map((item, index) => {
                                return (
                                  <MenuItem key={index} value={item.cod_valor}>
                                    {item.des_valor}
                                  </MenuItem>
                                );
                              })}
                            </Select>
                          </FormControl>
                        </Grid>
                        {/* Color */}
                        <Grid item xs={12} sm={4}>
                          <FormControl
                            required={!this.selectProduct(creditCard.productId)}
                            disabled={creditCard.colorDisabled}
                            margin="normal"
                            fullWidth={true}
                          >
                            <InputLabel htmlFor="color">Color Tarjeta</InputLabel>
                            <Select
                              value={creditCard.colorId}
                              inputProps={{ id: "color" }}
                              onChange={this.handleChangeSelectCreditCardColor(
                                "creditCard",
                                "color"
                              )}
                            >
                              <MenuItem disabled>Seleccionar Color</MenuItem>
                              {creditCardColor.data.map((item, index) => {
                                if (
                                  creditCard.brandId === 200002 &&
                                  item.cod_valor === 250003
                                ) {
                                  return null;
                                }
                                if (isSAEProcess) {
                                  if (item.cod_valor !== 250005) return null;
                                } else {
                                  if (this.state.isWithoutMembresy) {
                                    if (item.cod_valor !== 250004) return null;
                                  } else {
                                    if (item.cod_valor === 250004 || item.cod_valor === 250005) return null;
                                  }
                                }
                                return (
                                  <MenuItem
                                    selected={index === 0}
                                    key={index}
                                    value={item.cod_valor}
                                    className="d-flex justify-content-between"
                                  >
                                    <Label
                                      style={{
                                        color: item.des_auxiliar,
                                        fontSize: 12,
                                      }}
                                    />
                                    <span className="px-2">
                                      {" "}
                                      {item.des_valor}{" "}
                                    </span>
                                  </MenuItem>
                                );
                              })}
                            </Select>
                          </FormControl>
                        </Grid>
                        {/* Sin Membresia */}
                        <Grid item xs={12}>
                          <FormGroup row>
                            <FormControlLabel
                              control={
                                <Checkbox
                                  checked={this.state.isWithoutMembresy}
                                  onChange={this.handleChangeWithoutMembresy}
                                  value={this.state.isWithoutMembresy.toString()}
                                  color="primary"
                                />
                              }
                              label="Sin Membresía"
                            />
                          </FormGroup>
                        </Grid>
                        {/* Line Available TC */}
                        <Grid item xs={12}>
                          <FormLabel id="offert">Línea de Crédito</FormLabel>
                          <Slider
                            className="p-2 w-100"
                            style={{ overflow: "hidden" }}
                            aria-labelledby="offert"
                            valueReducer={valueReducer}
                            value={lineAvailable.value}
                            min={lineAvailable.min}
                            max={lineAvailable.max}
                            step={100}
                            onChange={this.handleChangeLineAvailable}
                          />
                          <div className="d-flex justify-content-between">
                            <FormHelperText className="font-weight-bold">
                              S/ {getMoneyFormat(lineAvailable.min)}
                            </FormHelperText>
                            <div className="d-flex justify-content-center align-items-center">
                              <Typography className="mr-2" fontSize="large">
                                S/
                              </Typography>
                              <TextField
                                style={{ width: 90 }}
                                required
                                label=""
                                variant="outlined"
                                type="text"
                                margin="normal"
                                color="default"
                                value={lineAvailable.value}
                                onChange={this.handleChangeTextFieldLineAvailable}
                                onBlur={this.handleBlurTextFieldLineAvailable}
                              />
                            </div>
                            <FormHelperText className="font-weight-bold">
                              S/ {getMoneyFormat(lineAvailable.max)}
                            </FormHelperText>
                          </div>
                        </Grid>
                      </Grid>) :
                      (<Grid container spacing={8}>
                        {/* Disbursement Type */}
                        <Grid item xs={12} sm={6} md={6} lg={6}>
                          <FormControl
                            required
                            disabled={this.state.disbursementTypeDisabled}
                            margin="normal"
                            fullWidth={true}
                          >
                            <InputLabel htmlFor="disbursement-type">
                              Tipo Desembolso
                            </InputLabel>
                            <Select
                              value={disbursementTypeId}
                              inputProps={{ id: "disbursement-type" }}
                              onChange={this.handleChangeSelectDisbursementType(
                                "disbursementType"
                              )}
                            >
                              {disbursementType.data.map((item, index) => {
                                return (
                                  <MenuItem key={index} value={item.cod_valor}>
                                    {item.des_valor}
                                  </MenuItem>
                                );
                              })}
                            </Select>
                          </FormControl>
                        </Grid>
                        {/* CCI */}
                        <Grid item xs={12} sm={6} md={6} lg={6}>
                          <TextField
                            required={this.state.CCIRequired}
                            error={this.state.CCIError}
                            disabled={this.state.CCIDisabled}
                            fullWidth
                            label="C.C.I."
                            type="text"
                            margin="normal"
                            color="default"
                            value={this.state.CCI}
                            name="CCI"
                            onChange={this.handleChangeCCI("CCI")}
                            onKeyPress={this.handleKeyPressTextFieldOnlyNumber(
                              "CCI"
                            )}
                            onPaste={this.handleOPasteTextFieldOnlyNumber("CCI")}
                            InputProps={{
                              inputProps: {
                                maxLength: defaultCCILengthInput,
                              },
                            }}
                          />
                        </Grid>
                        {/* CCI Bank */}
                        <Grid item xs={12} sm={12}>
                          <TextField
                            required={this.state.CCIRequired}
                            error={this.state.errorEntidadFinanciera}
                            disabled={true}
                            fullWidth
                            label="Nombre Banco"
                            type="text"
                            margin="normal"
                            color="default"
                            value={this.state.nameEntidadFinanciera}
                            name="nameEntidadFinanciera"
                            helperText={
                              this.state.errorMessageEntidadFinanciera
                            }
                            InputProps={{
                              readOnly: readOnly,
                            }}
                          />
                        </Grid>
                        {/* Number of Fees */}
                        <Grid item xs={12} sm={6} md={6} lg={4}>
                          <TextField
                            fullWidth={true}
                            required={this.state.numberFeesRequired}
                            disabled={this.state.numberFeesDisabled}
                            error={this.state.numberFeesError}
                            label="Número de Cuotas"
                            type="number"
                            margin="normal"
                            color="default"
                            value={this.state.numberFees}
                            onKeyPress={this.handleKeyPressTextFieldOnlyNumber(
                              "numberFees"
                            )}
                            onChange={this.handleChangeNumberFees("numberFees")}
                            name="numberFees"
                            helperText={
                              this.state.numberFeesError
                                ? `Número de Cuotas: El número de cuotas debe de ser mayor o igual a ${this.state.numberMinFees} 
                                  y menor o igual a ${this.state.numberMaxFees}`
                                : ""
                            }
                            InputProps={{
                              inputProps: {
                                min: 0,
                                max: this.state.numberMaxFees,
                              },
                            }}
                          />
                        </Grid>
                        {/* Fee Amount */}
                        <Grid item xs={12} sm={6} md={6} lg={4}>
                          <TextField
                            fullWidth={true}
                            required={this.state.feeAmountRequired}
                            disabled={this.state.numberFeesDisabled}
                            error={this.state.feeAmountError}
                            label="Valor Aprox. Cuotas"
                            type="number"
                            margin="normal"
                            color="default"
                            value={this.state.feeAmount}
                            onKeyPress={this.handleKeyPressTextFieldOnlyNumberDot(
                              "feeAmount"
                            )}
                            onChange={this.handleChangeFeeAmount("feeAmount")}
                            onBlur={() =>
                              this.setState({
                                feeAmount: parseFloat(this.state.feeAmount)
                                  ? parseFloat(this.state.feeAmount).toFixed(2)
                                  : this.state.feeAmount,
                              })
                            }
                            name="feeAmount"
                            helperText={
                              this.state.feeAmountError
                                ? `Número de Cuotas: El monto de cada cuota debe de ser como máximo S/. 9 999 999.99`
                                : ""
                            }
                            InputProps={{
                              inputProps: {
                                maxLength: defaultvalCuotasLength,
                              },
                            }}
                          />
                        </Grid>
                        {/* TCEA */}
                        <Grid item xs={12} sm={6} md={6} lg={4}>
                          <Typography
                            component="h6"
                            color="textSecondary"
                            className="d-flex align-items-center"
                          >
                            <TextField
                              fullWidth={true}
                              //required={this.state.numberFeesRequired}
                              disabled={this.state.numberFeesDisabled}
                              error={this.state.tceaError}
                              label="TCEA"
                              type="number"
                              margin="normal"
                              color="default"
                              value={this.state.tcea}
                              onChange={this.handleChangeTcea("tcea")}
                              onKeyPress={this.handleKeyPressTextFieldOnlyNumberDot(
                                "tcea"
                              )}
                              onBlur={() =>
                                this.setState({
                                  tcea: parseFloat(this.state.tcea)
                                    ? parseFloat(this.state.tcea).toFixed(2)
                                    : this.state.tcea,
                                })
                              }
                              name="tcea"
                              step=".01"
                              helperText={
                                this.state.tceaError
                                  ? `TCEA: El valor de % debe de ser mayor o igual 1 y menor o igual a 200`
                                  : ""
                              }
                              InputProps={{
                                inputProps: {
                                  //min: 0,
                                  //max: 200
                                  //maxLength: 10,
                                  //step: 0.01
                                },
                              }}
                            />
                          </Typography>
                        </Grid>
                      </Grid>)}
                  </Grid>
                  {isSAEProcess && (
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      {/* Line Available CEC*/}
                      <Grid item xs={12} sm={12}>
                        <FormLabel id="offert">Línea de Crédito Efectivo Cencosud</FormLabel>
                        <Slider
                          className="p-2 w-100"
                          style={{ overflow: "hidden" }}
                          aria-labelledby="offert"
                          valueReducer={valueReducer}
                          value={lineAvailable.value}
                          min={lineAvailable.min}
                          max={lineAvailable.max}
                          step={100}
                          onChange={this.handleChangeLineAvailable}
                        />
                        <div className="d-flex justify-content-between">
                          <FormHelperText className="font-weight-bold">
                            S/ {getMoneyFormat(lineAvailable.min)}
                          </FormHelperText>
                          <div className="d-flex justify-content-center align-items-center">
                            <Typography className="mr-2" fontSize="large">
                              S/
                            </Typography>
                            <TextField
                              style={{ width: 90 }}
                              required
                              label=""
                              variant="outlined"
                              type="text"
                              margin="normal"
                              color="default"
                              value={lineAvailable.value}
                              onChange={this.handleChangeTextFieldLineAvailable}
                              onBlur={this.handleBlurTextFieldLineAvailable}
                            />
                          </div>
                          <FormHelperText className="font-weight-bold">
                            S/ {getMoneyFormat(lineAvailable.max)}
                          </FormHelperText>
                        </div>
                      </Grid>
                      {/* Line Global */}
                      <Grid item xs={12}>
                        <Typography
                          component="h6"
                          color="textSecondary"
                          className="d-flex align-items-center"
                        >
                          <CreditCardIcon fontSize="small" className=" mr-2" />
                          <Typography component="span" color="inherit">
                            Línea de Crédito Efectivo Cencosud={" "}
                            <strong>
                              {`S/. ${getMoneyFormat(lineAvailable.value)}`}
                            </strong>
                          </Typography>
                        </Typography>
                        {/* <Typography
                          component="h6"
                          color="textSecondary"
                          className="d-flex align-items-center"
                        >
                          <CardMembershipIcon
                            fontSize="small"
                            className="mr-2"
                          />
                          <Typography component="span" color="inherit">
                            Línea Global: <strong>S/ {globalLine}</strong>
                          </Typography>
                        </Typography> */}
                        <Typography
                          component="h6"
                          color="textSecondary"
                          className="d-flex align-items-center"
                        >
                          <BookmarkBorderIcon
                            fontSize="small"
                            className=" mr-2"
                          />
                          <Typography component="span" color="inherit">
                            PCT EC: <strong>{pctSae}</strong>
                          </Typography>
                        </Typography>
                        {/*<Typography component="h6" color="textSecondary" className="d-flex align-items-center">
                            <TrendingUpIcon fontSize="small" className=" mr-2"/>
                            <Typography component="span" color="inherit">
                                TCEA: <strong>{tceaSae ? `${parseFloat(tceaSae).toFixed(2)} %` : 'No Aplica' }</strong>
                            </Typography>
                        </Typography>*/}
                      </Grid>
                    </Grid>
                  )}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          {/* Form - Button */}
          <Grid container spacing={8} className={classes.buttonActions}>
            {/* Button - Cancel */}
            <Grid item xs={12} sm={12} md={6} lg={3}>
              <Tooltip TransitionComponent={Zoom} title="Cancelar Originación.">
                <div>
                  <Bounce>
                    <div className={classNames(classes.wrapper)}>
                      <Button
                        disabled={this.state.cancelButtonDisabled}
                        className={classes.button}
                        type="button"
                        variant="contained"
                        size="small"
                        margin="normal"
                        color="secondary"
                        onClick={this.handleCancelExpress}
                        fullWidth={true}
                      >
                        Cancelar
                        <CancelIcon fontSize="small" className="ml-2" />
                      </Button>
                    </div>
                  </Bounce>
                </div>
              </Tooltip>
            </Grid>
            {/* Button - Ok */}
            <Grid item xs={12} sm={12} md={6} lg={3}>
              <Tooltip
                TransitionComponent={Zoom}
                title="Continuar con el proceso de originación."
              >
                <div>
                  <Bounce>
                    <div className={classNames(classes.wrapper)}>
                      <Button
                        variant="contained"
                        type="submit"
                        className={classNames(classes.button)}
                        color="primary"
                        disabled={this.state.commercialOfferButtonDisabled}
                        size="small"
                        fullWidth={true}
                      >
                        Continuar
                        <SendIcon fontSize="small" className="ml-2" />
                      </Button>
                      {this.state.commercialOfferButtonDisabled && (
                        <CircularProgress
                          size={24}
                          className={classes.buttonProgress}
                        />
                      )}
                    </div>
                  </Bounce>
                </div>
              </Tooltip>
            </Grid>
          </Grid>
        </form>
      </Grid>
    );
  }
}

export default withStyles(styles)(
  withSnackbar(connect(mapStateToProps, mapDispatchToProps)(CommercialOffer))
);
