import React from "react"
import { Paper, withStyles, Tooltip } from "@material-ui/core"
import { Zoom, Bounce, Fade } from "react-reveal"
import CencosudScotiaBank from "../../../../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg"
// Icons
import Title from "../../../../../components/title/Title"
import ExportExcel from "../../../../../components/file/ExportExcel"
import ExportCSV from "../../../../../components/file/ExportCSV"

const styles = (theme) => ({
  avatar: {
    [theme.breakpoints.down("md")]: {
      width: 60,
      height: 32,
    },
    width: 80,
    height: 48,
  },
  root: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  actionRoot: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  green: {
    background: `radial-gradient(circle at center, red 0, blue, green 100%)`,
  },
  title: {
    [theme.breakpoints.down("md")]: {
      fontSize: 14,
    },
  },
});

const Header = ({
  title,
  dataExcel,
  dataCsv,
  columnExcel,
  columnCsv,
  nameSheet,
  nameFile,
  nameFileCsv,
  classes,
}) => (
  <Paper className="p-1" color="primary">
    <figure className={classes.root}>
      <Bounce>
        <img
          className={classes.avatar}
          src={CencosudScotiaBank}
          alt="Cencosud ScotiaBank"
        />
      </Bounce>
      <figcaption className="w-100">
        <Fade>
          <Title align="center" title={title} />
        </Fade>
      </figcaption>
      <div className={classes.actionRoot}>
        <Bounce>
          <div>
            <Tooltip TransitionComponent={Zoom} title="Exportar a Excel.">
              <div className="pr-2">
                <ExportExcel
                  nameFile={nameFile}
                  workBook={[
                    {
                      sheet: {
                        data: dataExcel,
                        columns: columnExcel,
                        nameSheet: nameSheet,
                      },
                      nameFile: nameFile,
                    },
                  ]}
                />
              </div>
            </Tooltip>
          </div>
        </Bounce>
      </div>
      <div className={classes.actionRoot}>
        <Bounce>
          <div className="pr-2">
            <Tooltip TransitionComponent={Zoom} title="Exportar a CSV.">
              <div>
                <ExportCSV nameFile={nameFileCsv} data={dataCsv} />
              </div>
            </Tooltip>
          </div>
        </Bounce>
      </div>
    </figure>
  </Paper>
);

ExportExcel.defaultProps = {
  title: "",
  dataExcel: [],
  columnExcel: [],
  nameSheet: "Excel",
  nameFile: "Excel Default",
};

ExportCSV.defaultProps = {
  title: "",
  dataExcel: [],
  dataCsv: [],
  columnExcel: [],
  columnCsv: [],
  nameSheet: "Excel",
  nameFile: "Excel Default",
  nameFileCsv: "CSV Defaut",
};

export default withStyles(styles)(Header);
