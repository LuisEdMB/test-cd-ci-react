import React, { Component } from "react";
import * as moment from 'moment';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Autocomplete from '../../../../../components/Autocomplete';
import {  
    Grid,
    Button,
    TextField, 
    InputAdornment,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Typography,
    //FormHelperText,
    withStyles
} from '@material-ui/core';

import { getIdentificationDocumentType } from '../../../../../actions/value-list/identification-document-type';
import { getFlowPhaseState } from '../../../../../actions/generic/flow-phase-state'; 
import { getProduct } from '../../../../../actions/value-list/product';
import { getAgency } from '../../../../../actions/generic/agency';
import { getProcess, getSubProcess, getProcessFlowPhaseState } from '../../../../../actions/generic/process-subProcess';
// Icons
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FilterListIcon from '@material-ui/icons/FilterList';
import SearchIcon from '@material-ui/icons/Search';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
// Utils
import { onlyNumberKeyCode, defaultMinDate, validateLengthDocumentType, defaultEstadosSAE } from '../../../../../utils/Utils';
import { getNotistack } from '../../../../../utils/Notistack';
import { Bounce } from 'react-reveal';
// import MaterialUIPickers from './action-button/MaterialUIPickers'

const styles = theme => ({
    root:{
        width: "100%",
        //borderRadius: "0 0 .5em .5em",
        boxShadow: 'none',
        borderBottom: "1px solid #80808033",
    },
    paper:{
        padding: 20
    },
    buttonWrapper:{
        display: "center",
        alignItems: "center"
    }, 
    heading:{
        display: "flex",
        alignItem: "center",
        [theme.breakpoints.down('sm')]: {
            fontSize:14, 
        },
        [theme.breakpoints.up('md')]: {
            fontSize:16, 
        }
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        idenfiticationDocumentType: state.identificationDocumentTypeReducer, 
        flowPhaseState: state.flowPhaseStateReducer, 
        product: state.productReducer, 
        agency: state.agencyReducer, 
        processSubProcess: state.processSubProcessReducer,
        odcReport: state.odcReportReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getIdentificationDocumentType: bindActionCreators(getIdentificationDocumentType, dispatch), 
        getFlowPhaseState:  bindActionCreators(getFlowPhaseState, dispatch), 
        getProduct: bindActionCreators(getProduct, dispatch), 
        getAgency: bindActionCreators(getAgency, dispatch), 
        getProcess: bindActionCreators(getProcess, dispatch), 
        getSubProcess: bindActionCreators(getSubProcess, dispatch), 
        getProcessFlowPhaseState: bindActionCreators(getProcessFlowPhaseState, dispatch) 
    };
  return actions;
}

class FormPanel extends Component {
    state = {
        documentTypeError: false,
        documentTypeId: 0,
        documentType:"",
        documentTypeAux: "",
        documentTypeInternalValue: "",

        documentNumberError: false,
        documentNumber: "",

        flowPhaseStateError: false,
        flowPhaseStateId: "", 
        flowPhaseState: "",

        estadoSAEError: false,
        estadoSAE: '',
        estadoSAEId: '',

        registerAgencyError: false,
        registerAgency: "",
        registerAgencyId: "",

        registerProcesoError: false,
        registerProceso: "",
        registerProcesoId: "",
        
        embossingAgencyError: false,
        embossingAgencyId: "",
        embossingAgency: "",

        initialRegistrationDateError: false,
        initialRegistrationDate: "",

        initialRegistrationHour: '08:00',
        initialRegistrationHourError: false,

        finalRegistrationDateError: false,
        finalRegistrationDate: "", 

        finalRegistrationHour: '18:00',
        finalRegistrationHourError: false,

        initialEmbossingDateError: false,
        initialEmbossingDate: "",

        finalEmbossingDateError: false,
        finalEmbossingDate: "",

        usernameError: false,
        username: "",

        processError: false,
        processId: "",
        process: "",

        subProcessDisabled: true,
        subProcessLoading: false,
        subProcessError: false,
        subProcessId: "",
        subProcess: "",

        flowPhaseStateDisabled: true,
        flowPhaseStateLoading: false,
        //flowPhaseStateError: false,
        //flowPhaseStateId: "",
        //flowPhaseState: ""
    }

    componentWillMount = () => {
        let  initialRegistrationDate = moment().format('YYYY-MM-DD'); //moment().subtract(7, "day").format('YYYY-MM-DD');
        let  finalRegistrationDate = moment().format('YYYY-MM-DD');

        this.setState(state => ({
            ...state, 
            initialRegistrationDate: initialRegistrationDate, 
            finalRegistrationDate: finalRegistrationDate
        }));
    }

    componentDidMount = () => {
        // Get Api's Services
        this.props.getIdentificationDocumentType();
        this.props.getFlowPhaseState("REP002");
        this.props.getProduct();
        this.props.getAgency();
        this.props.getProcess(1, 0);
    }

    componentDidUpdate(prevProps, prevState) {
        // Identification Document Type - Before
        if (prevProps.idenfiticationDocumentType !== this.props.idenfiticationDocumentType){
            // Identification Document Type - Success Service 
            if(!this.props.idenfiticationDocumentType.loading &&
                this.props.idenfiticationDocumentType.response && 
                this.props.idenfiticationDocumentType.success){
                const idenfiticationDocumentType = this.props.idenfiticationDocumentType.data.find(item => item.val_orden === 1);
                this.setState(state => ({
                    ...state, 
                    documentTypeId: idenfiticationDocumentType.cod_valor,
                    documentType: idenfiticationDocumentType.des_valor,
                    documentTypeAux: idenfiticationDocumentType.des_auxiliar,
                    documentTypeInternalValue: idenfiticationDocumentType.valor_interno
                }));
            }
            // Identification Document Type - Error Service 
            else if(!this.props.idenfiticationDocumentType.loading &&
                     this.props.idenfiticationDocumentType.response && 
                    !this.props.idenfiticationDocumentType.success){
                    
                getNotistack(`Tipo Documento: ${this.props.idenfiticationDocumentType.error}`, this.props.enqueueSnackbar, "error");
            }
            // Identification Document Type - Error Service Connectivity
            else if(!this.props.idenfiticationDocumentType.loading && 
                    !this.props.idenfiticationDocumentType.response && 
                    !this.props.idenfiticationDocumentType.success){
                getNotistack(`Tipo Documento: ${this.props.idenfiticationDocumentType.error}`, this.props.enqueueSnackbar, "error");
            }
        }

        if (prevProps.agency !== this.props.agency){
            // Identification Document Type - Error Service 
            if(!this.props.agency.loading &&
                this.props.agency.response && 
               !this.props.agency.success){
                    
                getNotistack(`Agencia: ${this.props.agency.error}`, this.props.enqueueSnackbar, "error");
            }
            // Identification Document Type - Error Service Connectivity
            else if(!this.props.agency.loading && 
                    !this.props.agency.response && 
                    !this.props.agency.success){
                
                getNotistack(`Agencia: ${this.props.agency.error}`, this.props.enqueueSnackbar, "error");
            }
        }
    }

    handleSubmitSearch = e => {
        e.preventDefault();
        
        const { 
                estadoSAEId,
                registerAgencyId,
                initialRegistrationDate, 
                finalRegistrationDate,
                initialRegistrationHour,
                finalRegistrationHour
            } = this.state;
        
        let sendData = {
            estadoSAEId,
            registerAgencyId,
            initialRegistrationDate, 
            finalRegistrationDate, 
            initialRegistrationHour,
            finalRegistrationHour
            
        }
        this.props.handleSubmitFilter(sendData);
    }

    // Client - Select - Event Change
    handleChangeSelectRequired = (name) => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }

        this.setState(state => ({
            ...state, 
            [name]: valueDefault, 
            [nameId]: idDefault,
            [nameError]: error,
        }));
    }

    handleChangeProcess = name => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";

        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }

        this.props.getSubProcess(2, idDefault);

        this.setState(state => ({
            ...state, 
            [name]: valueDefault, 
            [nameId]: idDefault,
            [nameError]: error,
            subProcessDisabled: !idDefault? true: false,
            subProcessId: "",
            subProcess: "",
            flowPhaseStateDisabled: true,
            flowPhaseStateId: "",
            flowPhaseState: ""
        }));
    }

    handleChangeSubProcess = name => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        if(e !== null){
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }

        this.props.getProcessFlowPhaseState(3, idDefault);

        this.setState(state => ({
            ...state, 
            [name]: valueDefault, 
            [nameId]: idDefault,
            [nameError]: error,
            flowPhaseStateDisabled: !idDefault? true: false,
            flowPhaseStateId: "",
            flowPhaseState: ""
        }));
    }

    handleChangeTextFieldRequired = name => e => {
        e.persist();
        let { value } = e.target;
        let nameError = `${name}Error`;

        this.setState(state => ({
            ...state,
            [name]: value, 
            [nameError]: value !== "" ? false : true
        }));
    }

    // Client - Document Number - TextField Event Change
    handleChangeTextFieldDocumentNumber = name => e =>{
        let { value } = e.target;
        let nameError = `${name}Error`;
        if(!isNaN(Number(value))){
            this.setState(state => ({
                [name]:value,
                [nameError]: !validateLengthDocumentType(value.length)
            }));        
        }
    }

    // Only Number
    handleKeyPressTextFieldOnlyNumber = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!onlyNumberKeyCode(code)){
            e.preventDefault();
        }   
    }

    render = () => {
        const { //idenfiticationDocumentType, 
                classes, 
                title, 
                agency,
                //processSubProcess
            } = this.props;

        const agencyData = agency.data.map((item, index) => ({
            value: item.ide_agencia, //item.cod_agencia, 
            label: item.des_agencia
        }));
     


        return( <div>
                    <ExpansionPanel className={classes.root}  defaultExpanded>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel-content"
                            id="panel-header"
                        >
                            <Typography variant="h6" className={classes.heading}>
                                <FilterListIcon className="mr-2" />
                                {title}
                            </Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <div className="w-100">
                                <form method="post" autoComplete="off" onSubmit={this.handleSubmitSearch}>
                                    <Grid container spacing={8} className="mb-2">
                                         {/**Estado */}
                                         <Grid item xs={12} sm={6} md={4} lg={2}>
                                            <Autocomplete 
                                                onChange={this.handleChangeSelectRequired("estadoSAE")}
                                                value={this.state.estadoSAEId}
                                                data={defaultEstadosSAE}
                                                placeholder={"Estado"}
                                            />
                                        </Grid>
                                        {/* Register Agency */}
                                        <Grid item xs={12} sm={6} md={4} lg={2}>
                                            <Autocomplete 
                                                onChange={this.handleChangeSelectRequired("registerAgency")}
                                                value={this.state.registerAgencyId}
                                                data={agencyData}
                                                placeholder={"Agencia Atención"}
                                            />
                                        </Grid>
                                        {/* Initial Registration Date */}
                                        <Grid item xs={6} sm={6} md={4} lg={2}>
                                            <TextField
                                                error={this.state.initialRegistrationDateError}
                                                fullWidth={true}
                                                name="initialDate"
                                                label="Fecha Atención Inicial"
                                                type="date"
                                                margin="normal"
                                                format="DD-MM-YYYY"
                                                defaultValue={this.state.initialRegistrationDate}
                                                onChange={this.handleChangeTextFieldRequired("initialRegistrationDate")}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        min: defaultMinDate,
                                                        max: moment().format('YYYY-MM-DD')
                                                    },
                                                    endAdornment: (
                                                        <InputAdornment position="end">
                                                            <CalendarTodayIcon color={this.state.initialRegistrationDateError? "secondary": "inherit"} fontSize="small" />
                                                        </InputAdornment>
                                                    )
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={6} sm={6} md={4} lg={2}>
                                            <TextField
                                                error={this.state.initialRegistrationHourError}
                                                fullWidth={true}
                                                name="initialRegistrationHour"
                                                label="Hora Atención Inicial"
                                                type="time"
                                                margin="normal"
                                                defaultValue={this.state.initialRegistrationHour}
                                                onChange={this.handleChangeTextFieldRequired("initialRegistrationHour")}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        //step: 300, //5 min comment
                                                    },
                                                    endAdornment: (
                                                        <InputAdornment position="end">
                                                            <QueryBuilderIcon color={this.state.initialRegistrationHourError? "secondary": "inherit"} fontSize="small" />
                                                        </InputAdornment>
                                                    )
                                                }}
                                            />
                                        </Grid>
                                        {/* Final Registration Date */}
                                        <Grid item xs={6} sm={6} md={4} lg={2}>
                                            <TextField
                                                error={this.state.finalRegistrationDateError}
                                                fullWidth={true}
                                                name="finalDate"
                                                label="Fecha Atención Final"
                                                type="date"
                                                margin="normal"
                                                format="DD-MM-YYYY"
                                                defaultValue={this.state.finalRegistrationDate}
                                                onChange={this.handleChangeTextFieldRequired("finalRegistrationDate")}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        min: defaultMinDate,
                                                        max: moment().format('YYYY-MM-DD')
                                                    },
                                                    endAdornment: (
                                                        <InputAdornment position="end">
                                                            <CalendarTodayIcon color={this.state.finalRegistrationDateError? "secondary": "inherit"} fontSize="small" />
                                                        </InputAdornment>
                                                    )
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={6} sm={6} md={4} lg={2}>
                                            <TextField
                                                error={this.state.finalRegistrationDateError}
                                                fullWidth={true}
                                                name="finalRegistrationHour"
                                                label="Hora Atención Final"
                                                type="time"
                                                margin="normal"
                                                defaultValue={this.state.finalRegistrationHour}
                                                onChange={this.handleChangeTextFieldRequired("finalRegistrationHour")}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                InputProps={{
                                                    inputProps:{
                                                        //step: 300,//5 min comment 
                                                    },
                                                    endAdornment: (
                                                        <InputAdornment position="end">
                                                            <QueryBuilderIcon color={this.state.finalRegistrationDateError? "secondary": "inherit"} fontSize="small" />
                                                        </InputAdornment>
                                                    )
                                                }}
                                            />
                                        </Grid>
                                        {/* Boton consultar */}
                                        <Grid item xs={12} sm={12} md={4} lg={2} className="d-flex justify-content-center align-items-center">
                                            <div className="w-100">
                                                <Bounce> 
                                                    <div>
                                                        <Button 
                                                            className={classes.button} fullWidth variant="contained" type="submit"  color="primary"
                                                            disabled={this.props.odcReport.originacionOperacionesSae.loading}>
                                                            Consultar
                                                            <SearchIcon  className="ml-2" size="small" />
                                                        </Button>
                                                    </div>
                                                </Bounce>
                                            </div>
                                        </Grid>
                                    </Grid>
                            </form>
                            </div>
                            
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </div>
            )
    }
}


FormPanel.defaultProps = {
    form:{
        agencyId:0,
        initialDateError:false,
        finalDateError:false,
        initialDate:"",
        finalDate:""
    }
};

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(FormPanel)));
