import React from "react";
import * as moment from 'moment';
import { Table } from '@devexpress/dx-react-grid-material-ui';
import { getMoneyFormat } from '../../../../../../utils/Utils'
import NewCommentForm from '../../../../../../components/NewCommentForm/NewCommentForm';

const MyField = (props) => (
    <span style={{color: props.fontColor , padding: '.2em .5em', backgroundColor: props.bg, borderRadius: '.2em'}}>
        {props.value}
    </span>
)

const  Cell = ({...props}) => {
    if(props.column.name === "monto_cci"){
        return <Table.Cell {...props} value={props.value? `S/. ${getMoneyFormat(props.value)}`: '-'} />
    }
    // if(props.column.name === "fec_reg"){
    //     const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY') : "";
    //     return <Table.Cell {...props} value={value} />
    // }
    // if(props.column.name === "hora_reg"){
    //     const value = props.row.fec_reg ? moment(props.row.fec_reg.replace("T", " ")).format('HH:mm') : "";
    //     return <Table.Cell {...props} value={value} />
    // }
    if(props.column.name === "fec_reg_emboce"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    if(props.column.name === "des_nombre_flujo_fase_estado_sae"){
        return <Table.Cell {...props} value={<MyField  bg={props.row.des_color_flujo_fase_estado_sae} value={props.value} fontColor={props.row.cod_flujo_fase_estado_sae === 11010 ?  '#363636' : '#ffffff'} />} />
    }
    if(props.column.name === "observation"){
        return <Table.Cell {...props}>
           <NewCommentForm {...props}/>
        </Table.Cell>
    }
    return <Table.Cell {...props} />
}

export default Cell;
