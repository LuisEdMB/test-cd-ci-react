import React, { Component } from "react";
import * as moment from 'moment';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
import { bindActionCreators } from "redux";
// React Router 
import { connect } from 'react-redux';
// Component
import {
    IconButton,
    Grid,
    LinearProgress,
    withStyles,
    Divider
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close';
// Components Custom 
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
// import HeaderReport from '../../../../components/header-report/HeaderReport';
import Header from './components/Header';
import FormPanel from './components/FormPanel';

// Actions 
import { getReportOriginationsOperationsSae, getReportOriginationOperationsIndicators } from '../../../../actions/odc-report/odc-report';

const styles = theme => ({
    avatar: {
        width: 80,
        height: 48
    },
    modalRoot: {

    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        odcReport: state.odcReportReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getReportOriginationsOperationsSae: bindActionCreators(getReportOriginationsOperationsSae, dispatch),
        getReportOriginationOperationsIndicators: bindActionCreators(getReportOriginationOperationsIndicators, dispatch)
    };
    return actions;
}

class ActivationOperationsSae extends Component {

    state = {
        defaultColumnWidths: [
            { columnName: 'cod_solicitud_completa', width: 130 },
            { columnName: 'fec_reg', width: 150 },
            { columnName: 'hora_reg', width: 150 },
            { columnName: 'des_nombre_producto', width: 150 },
            { columnName: 'des_tipo_documento', width: 100 },
            { columnName: 'des_nro_documento', width: 100 },
            { columnName: 'des_nombre_completo', width: 200 },
            { columnName: 'num_tarjeta_lp', width: 180 },
            { columnName: 'des_cci', width: 180 },
            { columnName: 'des_entidad_financiera', width: 180 },
            { columnName: 'monto_cci', width: 180 },
            { columnName: 'cuota', width: 180 }, //numero_cuotas
            { columnName: 'monto_cuota', width: 150 },
            { columnName: 'des_usu_reg', width: 180 },
            { columnName: 'tip_canal', width: 120 },
            { columnName: 'des_agencia', width: 200 },
            { columnName: 'cod_comercio', width: 150 },
            { columnName: 'cod_usuario_act_sol', width: 180 },
            { columnName: 'fech_aten_Act', width: 150 },
            { columnName: 'hora_aten_Act', width: 150 },
            { columnName: 'des_nombre_flujo_fase_estado_sae', width: 200 },
            { columnName: 'observation', width: 150 },
        ],
        columns: [],
        tableColumnExtensions: [
            { columnName: 'authorize', align: 'center' },
            { columnName: 'monto_cci', align: 'center' },
            { columnName: 'cuota', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        rowsExcel: [],
        rowsCsv: [],
        additional: 0,
        reprint: 0,
        express: 0,
        pageSizes: [],
        columnExcel: [],
        columnCsv: [],

        dateDiffDays: 0
    };

    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }
        if (prevProps.odcReport !== this.props.odcReport) {
            if (prevProps.odcReport.originacionOperacionesSae !== this.props.odcReport.originacionOperacionesSae) {

                if (this.props.odcReport.originacionOperacionesSae.loading) {
                    this.setState(state => ({
                        ...state,
                        rows: [...state.rows],
                        rowsExcel: [...state.rowsExcel]
                    }));
                }

                if (!this.props.odcReport.originacionOperacionesSae.loading &&
                    this.props.odcReport.originacionOperacionesSae.response &&
                    this.props.odcReport.originacionOperacionesSae.success) {

                    let { data } = this.props.odcReport.originacionOperacionesSae;
                    let { solicitudesReporteActivacionSae, solicitudesReporteActivacionesSaeExcel, solicitudesReporteActivacionesSaeCsv } = data;
                    let rows = solicitudesReporteActivacionSae
                    if (solicitudesReporteActivacionSae && solicitudesReporteActivacionesSaeExcel && solicitudesReporteActivacionesSaeCsv) {

                        let columnExcel = Object.keys(solicitudesReporteActivacionesSaeExcel.length > 0 && solicitudesReporteActivacionesSaeExcel[0])
                        columnExcel = columnExcel.map((item, index) => {
                            if (item === "Estado SAE") {
                                item = "Estado EC"
                            }
                            if (item === "Número de Tarjeta LP") {
                                item = "Número de Tarjeta CEC"
                            }
                            return { value: item, label: item }
                        })

                        let columnCsv = Object.keys(solicitudesReporteActivacionesSaeCsv.length > 0 && solicitudesReporteActivacionesSaeCsv[0])
                        columnCsv = columnCsv.map((item, index) => (
                            { value: item, label: item }
                        ))

                        let rowsExcel = solicitudesReporteActivacionesSaeExcel.map((e, i) => {
                            let json = { ...e }
                            for (var v in json) {
                                let newKey = v
                                if (newKey === "Estado SAE") {
                                    newKey = "Estado EC"
                                }
                                if (newKey === "Número de Tarjeta LP") {
                                    newKey = "Número de Tarjeta CEC"
                                }
                                json[newKey] = (json[v] || '').toString()
                            }
                            return json
                        })

                        this.setState(state => ({
                            ...state,
                            rows: rows,
                            columnExcel: columnExcel,
                            columnCsv: columnCsv,
                            rowsExcel: rowsExcel,
                            rowsCsv: solicitudesReporteActivacionesSaeCsv
                        }));
                    }
                }
                else if (!this.props.odcReport.originacionOperacionesSae.loading &&
                    this.props.odcReport.originacionOperacionesSae.response &&
                    !this.props.odcReport.originacionOperacionesSae.success) {
                    // Notistack
                    this.getNotistack(this.props.odcReport.originacionOperacionesSae.error, "error");
                    this.setState(state => ({ ...state, rows: [], rowsExcel: [], columnCsv: [], columnExcel: [], rowsCsv: [] }));
                }
                else if (!this.props.odcReport.originacionOperacionesSae.loading &&
                    !this.props.odcReport.originacionOperacionesSae.response &&
                    !this.props.odcReport.originacionOperacionesSae.success) {
                    // Notistack
                    this.getNotistack(this.props.odcReport.originacionOperacionesSae.error, "error");
                    this.setState(state => ({ ...state, rows: [], rowsExcel: [], columnCsv: [], columnExcel: [], rowsCsv: [] }));
                }
            }
        }
    }

    componentWillMount = () => {
        const columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns: columns
        }));
    }

    getColumnsRender() {
        let columns = [];
        if (this.props.width === "xs") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'fec_reg', title: 'Fecha de Atención' },
                { name: 'hora_reg', title: 'Hora de Atención' },
            ]
        }
        else if (this.props.width === "sm") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'fec_reg', title: 'Fecha de Atención' },
                { name: 'hora_reg', title: 'Hora de Atención' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'num_tarjeta_lp', title: 'Número de Tarjeta CEC' },
                { name: 'des_cci', title: 'Número CCI' }
            ]
        }
        else if (this.props.width === "md") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'fec_reg', title: 'Fecha de Atención' },
                { name: 'hora_reg', title: 'Hora de Atención' },
                { name: 'des_nombre_producto', title: 'Tipo Producto' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'num_tarjeta_lp', title: 'Número de Tarjeta CEC' },
                { name: 'des_cci', title: 'Número CCI' },
                { name: 'des_entidad_financiera', title: 'Nombre del otro Banco' },
                { name: 'monto_cci', title: 'Importe de desembolso' },
                { name: 'cuota', title: 'Número de cuotas' },
                // { name: 'monto_cuota', title: 'Monto de cuotas' },
                { name: 'des_usu_reg', title: 'Cód. Usuario registro' },
                { name: 'tip_canal', title: 'Cód. Agencia' },
                { name: 'des_agencia', title: 'Nombre de la Agencia' },
                { name: 'cod_comercio', title: 'Cód. Comercio' },
                { name: 'cod_usuario_act_sol', title: 'Cód. Usuario Activación' },
                { name: 'fech_aten_Act', title: 'Fecha Activación' },
                { name: 'hora_aten_Act', title: 'Hora Activación' },
                { name: 'des_nombre_flujo_fase_estado_sae', title: 'Estado EC' },
            ]
        }
        else if (this.props.width === "lg") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'fec_reg', title: 'Fecha de Atención' },
                { name: 'hora_reg', title: 'Hora de Atención' },
                { name: 'des_nombre_producto', title: 'Tipo Producto' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'num_tarjeta_lp', title: 'Número de Tarjeta CEC' },
                { name: 'des_cci', title: 'Número CCI' },
                { name: 'des_entidad_financiera', title: 'Nombre del otro Banco' },
                { name: 'monto_cci', title: 'Importe de desembolso' },
                { name: 'cuota', title: 'Número de cuotas' },
                // { name: 'monto_cuota', title: 'Monto de cuotas' },
                { name: 'des_usu_reg', title: 'Cód. Usuario registro' },
                { name: 'tip_canal', title: 'Cód. Agencia' },
                { name: 'des_agencia', title: 'Nombre de la Agencia' },
                { name: 'cod_comercio', title: 'Cód. Comercio' },
                { name: 'cod_usuario_act_sol', title: 'Cód. Usuario Activación' },
                { name: 'fech_aten_Act', title: 'Fecha Activación' },
                { name: 'hora_aten_Act', title: 'Hora Activación' },
                { name: 'des_nombre_flujo_fase_estado_sae', title: 'Estado EC' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        else if (this.props.width === "xl") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'fec_reg', title: 'Fecha de Atención' },
                { name: 'hora_reg', title: 'Hora de Atención' },
                { name: 'des_nombre_producto', title: 'Tipo Producto' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'num_tarjeta_lp', title: 'Número de Tarjeta CEC' },
                { name: 'des_cci', title: 'Número CCI' },
                { name: 'des_entidad_financiera', title: 'Nombre del otro Banco' },
                { name: 'monto_cci', title: 'Importe de desembolso' },
                { name: 'cuota', title: 'Número de cuotas' },
                // { name: 'monto_cuota', title: 'Monto de cuotas' },
                { name: 'des_usu_reg', title: 'Cód. Usuario registro' },
                { name: 'tip_canal', title: 'Cód. Agencia' },
                { name: 'des_agencia', title: 'Nombre de la Agencia' },
                { name: 'cod_comercio', title: 'Cód. Comercio' },
                { name: 'cod_usuario_act_sol', title: 'Cód. Usuario Activación' },
                { name: 'fech_aten_Act', title: 'Fecha Activación' },
                { name: 'hora_aten_Act', title: 'Hora Activación' },
                { name: 'des_nombre_flujo_fase_estado_sae', title: 'Estado EC' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        return columns;
    }

    handleSubmitFilter = data => {
        if (data) {
            let sendData = {
                cod_estado_sae: data.estadoSAEId,
                cod_agencia: data.registerAgencyId,
                fec_inicial: data.initialRegistrationDate,
                hora_inicial: data.initialRegistrationHour,
                fec_final: data.finalRegistrationDate,
                hora_final: data.finalRegistrationHour,
            }
            this.props.getReportOriginationsOperationsSae(sendData)
        }
    }

    // Notistack 
    getNotistack(message, variant = "default", duration = 6000) {
        let select = "default";
        switch (variant) {
            case "error":
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default:
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit" />
                </IconButton>
            ),
        });
    }
    render() {
        const { odcReport, width, } = this.props;
        const {
            columns,
            defaultColumnWidths,
            defaultHiddenColumnNames,
            rows,
            rowsExcel,
            rowsCsv,
            columnExcel,
            columnCsv,
            tableColumnExtensions
        } = this.state;
        return (
            <Grid container className="p-1">
                <Grid item xs={12}>
                    <div style={{ height: 12 }}>
                        {
                            odcReport.originacionOperacionesSae.loading ? <LinearProgress /> : ""
                        }
                    </div>
                </Grid>
                <Grid item xs={12}>
                    <Header
                        title={"Reporte de Registro Efectivo Cencosud"}
                        columnExcel={columnExcel}
                        columnCsv={columnCsv}
                        dataExcel={rowsExcel}
                        dataCsv={rowsCsv}
                        data={rows}
                        nameSheet={"Reporte de Act. de CEC"}
                        nameFile={`Reporte_Activaciones_CEC_${moment().format('DDMMYYYY')}`}
                        nameFileCsv={`Desembolso_Red_Agencia_${moment().format('DDMMYYYY')}`}
                    />
                </Grid>
                <Grid item xs={12}>
                    <Divider className="mb-2" />
                </Grid>
                <Grid item xs={12} className="mb-2">
                    <FormPanel title={"Filtros de búsqueda"}
                        handleSubmitFilter={this.handleSubmitFilter}
                    />
                </Grid>

                <Grid item xs={12}>
                    <DevGridComponent
                        rows={rows}
                        columns={columns}
                        width={width}
                        tableColumnExtensions={tableColumnExtensions}
                        defaultColumnWidths={defaultColumnWidths}
                        defaultHiddenColumnNames={defaultHiddenColumnNames}
                        RowDetailComponent={RowDetail}
                        CellComponent={Cell} />
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(ActivationOperationsSae))));

