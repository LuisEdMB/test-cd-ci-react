import React from "react";
import { withStyles, Typography } from "@material-ui/core";

const styles = theme => ({
    fontSizeDefault: {
        fontSize:12
    },
});

const RowDetail = ({ row, classes }) => (
    <ul>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Solicitud:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.cod_solicitud_completa }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Tipo Documento:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_tipo_documento }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Número Documento:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_nro_documento }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Cliente:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.des_nombre_completo }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Producto:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_nombre_producto }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Promotor:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_resp_promotor_nombre_completo }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Celular:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_telef_celular }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Teléfono:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_telef_fijo }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Usuario:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_usu_reg }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.fec_reg }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Flujo - Fase - Estado:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_jerarquias_flujo_fase_estado }
            </Typography>
        </li>
    </ul>
);

export default withStyles(styles)(RowDetail);

