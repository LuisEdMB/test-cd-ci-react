import React, { Component } from "react";
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
import { bindActionCreators } from "redux";
// React Router 
import { connect } from 'react-redux';
// Component
import{ 
    IconButton,
    Grid, 
    LinearProgress,
    withStyles,
    Divider
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close'; 
// Components Custom 
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
import HeaderReport from '../../../../components/header-report/HeaderReport';
import FormPanel from './components/FormPanel';
// Actions 
import { getReportClienteConsult } from '../../../../actions/odc-report/odc-report';
// Utils 
import { defaultPageSizes } from '../../../../utils/Utils';

const styles = theme => ({
    avatar:{
        width:80, 
        height:48
    },
    modalRoot:{
       
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        odcReport: state.odcReportReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getReportClienteConsult: bindActionCreators(getReportClienteConsult, dispatch), 
    };
    return actions;
}

class ClientConsult extends Component {

    state = {
        columnExcel:[],
        defaultColumnWidths: [
            { columnName: 'cod_solicitud_completa', width: 125 },
            { columnName: 'des_tipo_documento', width: 100},
            { columnName: 'des_nro_documento', width: 100 },
            { columnName: 'des_nombre_completo', width: 300 },
            { columnName: 'des_jerarquias_flujo_fase_estado', width: 380 },
            { columnName: 'des_nombre_producto', width: 200 },
            { columnName: 'des_resp_promotor_nombre_completo', width: 200 },
            { columnName: 'des_telef_celular', width: 130 },
            { columnName: 'des_telef_fijo', width: 130 },
            { columnName: 'des_usu_reg', width: 130 },
            { columnName: 'fec_reg', width: 140 },
        ],
        columns:[],
        tableColumnExtensions: [
            { columnName: 'authorize', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        rowsExcel:[],
        additional: 0, 
        reprint: 0, 
        express: 0,
        pageSizes: defaultPageSizes,
        dateDiffDays: 0
        
    };
    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }
        if (prevProps.odcReport.clientConsult !== this.props.odcReport.clientConsult) {	
            if(this.props.odcReport.clientConsult.loading){
                this.setState(state => ({
                    ...state, 
                    rows: [...state.rows], 
                    rowsExcel: [...state.rowsExcel]
                }));
            }
           
            if(!this.props.odcReport.clientConsult.loading && 
                this.props.odcReport.clientConsult.response && 
                this.props.odcReport.clientConsult.success){
                let { data } = this.props.odcReport.clientConsult;  
                
                
                let { tarjetasOriginadas, tarjetasOriginadasExcel } = data;

                if(tarjetasOriginadas && tarjetasOriginadasExcel){

                    let columnExcel = Object.keys(tarjetasOriginadasExcel.length > 0 && tarjetasOriginadasExcel[0]);
                    columnExcel = columnExcel.map((item, index) => (
                        {value: item, label: item}
                    ));
    
                    this.setState(state => ({
                        ...state, 
                        rows: tarjetasOriginadas,
                        columnExcel: columnExcel,
                        rowsExcel: tarjetasOriginadasExcel
                    }));
                }
              
            }
            else if(!this.props.odcReport.clientConsult.loading && 
                     this.props.odcReport.clientConsult.response && 
                    !this.props.odcReport.clientConsult.success){
                // Notistack
                this.getNotistack(this.props.odcReport.clientConsult.error, "error");
                this.setState(state => ({  ...state, rows: [],  rowsExcel:[], columnExcel: []}));
            }
            else if(!this.props.odcReport.clientConsult.loading && 
                    !this.props.odcReport.clientConsult.response && 
                    !this.props.odcReport.clientConsult.success){
                // Notistack
                this.getNotistack(this.props.odcReport.clientConsult.error, "error");
                this.setState(state => ({  ...state, rows: [],  rowsExcel:[], columnExcel: []}));
            }
        }
    }

    componentWillMount = () => {
        const columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns:columns
        }));
    }
    
    getColumnsRender(){
        let columns = [];
        if(this.props.width === "xs"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
            ]
        }
        else if(this.props.width === "sm"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
            ]
        }
        else if(this.props.width === "md"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento ' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
                { name: 'fec_reg', title: 'Fecha Registro' },
            ]
        }
        else if(this.props.width === "lg"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado'},
                { name: 'des_nombre_producto', title: "Producto" },
                { name: 'des_usu_reg', title: "Usuario Creador" },
                { name: 'fec_reg', title: "Fecha Registro" },
                { name: 'des_resp_promotor_nombre_completo', title: "Promotor" },
                { name: 'des_telef_celular', title: "Nro. Celular" },
                { name: 'des_telef_fijo', title: "Nro. Tel. Casa" }
            ]
        }
        else if(this.props.width === "xl"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado'},
                { name: 'des_nombre_producto', title: "Producto" },
                { name: 'des_usu_reg', title: "Usuario Creador" },
                { name: 'fec_reg', title: "Fecha Registro" },
                { name: 'des_resp_promotor_nombre_completo', title: "Promotor" },
                { name: 'des_telef_celular', title: "Nro. Celular" },
                { name: 'des_telef_fijo', title: "Nro. Tel. Casa" }
            ]
        }
        return columns;
    }
   
    handleSubmitFilter = data => {
        if(data){
            let sendData = {
                tarjetasOriginadas:{
                    cod_tipo_documento: data.documentTypeAux,
                    des_nro_documento: data.documentNumber,
                    cod_producto: data.productId? data.productId: 0,
                    des_siglas_flujo_fase_estado: data.flowPhaseStateId,
                    des_usu_reg: data.username,
                    fec_inicial: data.initialRegistrationDate,
                    fec_final: data.finalRegistrationDate,
                    cod_agencia: 0
                }
            }
            this.props.getReportClienteConsult(sendData);
        }
    }
    handleShowHideSearchInput = e => {
        this.setState(state => ({
            showHideSearchInput: !state.showHideSearchInput
        }));
    }
    handleClickOpenSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: true
        }));
    }
    handleClickCloseSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: false
        }));
    }

    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render() {
        const { width, odcReport } = this.props;
        const { 
                columns,
                defaultColumnWidths, 
                defaultHiddenColumnNames, 
                rows, 
                rowsExcel,
                tableColumnExtensions,
                columnExcel, 
            } = this.state;
        return (
                <Grid container className="p-1">
                    <Grid  item xs={12} style={{height: 12}}>
                        {
                            odcReport.originatedCards.loading ?  <LinearProgress /> : ""
                        }
                    </Grid>
                    <Grid item xs={12}>
                        <HeaderReport 
                            title={"Reporte de Consulta Cliente" }
                            columnExcel={columnExcel} 
                            dataExcel={rowsExcel}
                            data={rows}
                            nameSheet={"Reporte de Consulta Cliente"} 
                            nameFile={"Datos exportados"}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Divider className="mb-2"/>  
                    </Grid>
                    <Grid item xs={12}  className="mb-2">
                        <FormPanel title={"Filtros de búsqueda"} 
                            handleSubmitFilter={this.handleSubmitFilter}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <DevGridComponent 
                            rows={rows}
                            columns={columns}
                            width={width}
                            tableColumnExtensions={tableColumnExtensions}
                            defaultColumnWidths={defaultColumnWidths}
                            defaultHiddenColumnNames={defaultHiddenColumnNames}
                            RowDetailComponent={RowDetail}
                            CellComponent={Cell}/>
                    </Grid>
                </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(ClientConsult))));

