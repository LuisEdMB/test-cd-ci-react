import React, { Component } from "react";
import * as moment from 'moment';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
import { bindActionCreators } from "redux";
// React Router 
import { connect } from 'react-redux';
// Component
import{ 
    IconButton,
    Grid, 
    LinearProgress,
    withStyles,
    Divider
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close'; 
// Components Custom 
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
import HeaderReport from '../../../../components/header-report/HeaderReport';
import FormPanel from './components/FormPanel';

// Actions 
import { getReportOriginationOperationsSae, getReportOriginationOperationsIndicators } from '../../../../actions/odc-report/odc-report';

const styles = theme => ({
    avatar:{
        width:80, 
        height:48
    },
    modalRoot:{
       
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        odcReport: state.odcReportReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getReportOriginationOperationsSae: bindActionCreators(getReportOriginationOperationsSae, dispatch), 
        getReportOriginationOperationsIndicators: bindActionCreators(getReportOriginationOperationsIndicators, dispatch)
    };
    return actions;
}

class OriginationOperationsSae extends Component {

    state = {
        defaultColumnWidths: [
            { columnName: 'cod_solicitud_completa', width: 130 },
            { columnName: 'fec_reg', width: 150 },
            { columnName: 'hora_reg', width: 150 },
            { columnName: 'des_tipo_documento', width: 100},
            { columnName: 'des_nro_documento', width: 100 },
            { columnName: 'des_nombre_completo', width: 200 },
            { columnName: 'num_tarjeta_pmp', width: 180 },
            { columnName: 'des_cci', width: 180 },
            { columnName: 'des_entidad_financiera', width: 180 },
            { columnName: 'monto_cci', width: 180 },
            { columnName: 'cuota', width: 150 },
            { columnName: 'cod_responsable', width: 180 },
            { columnName: 'cod_agencia', width: 120 },
            { columnName: 'des_agencia', width: 150 },
            { columnName: 'cod_comercio', width: 150 },
            { columnName: 'cod_usuario_act_sol', width: 180 },
            { columnName: 'fech_aten_Act', width: 150 },
            { columnName: 'hora_aten_Act', width: 150 },


        ],
        columns:[],
        tableColumnExtensions: [
            { columnName: 'authorize', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        rowsExcel: [],
        additional: 0, 
        reprint: 0, 
        express: 0,
        pageSizes: [],
        columnExcel: [],
        
        dateDiffDays: 0        
    };

    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }
        if(prevProps.odcReport !== this.props.odcReport){
            if (prevProps.odcReport.originacionOperacionesSae !== this.props.odcReport.originacionOperacionesSae) {	
               
                if(this.props.odcReport.originacionOperacionesSae.loading){
                    this.setState(state => ({
                        ...state, 
                        rows: [...state.rows],
                        rowsExcel: [...state.rowsExcel]
                    }));
                }
               
                if(!this.props.odcReport.originacionOperacionesSae.loading && 
                    this.props.odcReport.originacionOperacionesSae.response && 
                    this.props.odcReport.originacionOperacionesSae.success){
           
                    let { data } = this.props.odcReport.originacionOperacionesSae;  
                    let { solicitudesReporteActivacionSae, solicitudesReporteActivacionSaeExcel } = data;
                    let rows = solicitudesReporteActivacionSae

                    if(solicitudesReporteActivacionSae && solicitudesReporteActivacionSaeExcel){

                        let columnExcel = Object.keys(solicitudesReporteActivacionSaeExcel.length > 0 && solicitudesReporteActivacionSaeExcel[0])
                        columnExcel = columnExcel.map((item, index)=>(
                            {value: item, label: item}
                        ))
                                            
                        this.setState(state => ({
                            ...state, 
                            rows: rows,
                            columnExcel: columnExcel,
                            rowsExcel: solicitudesReporteActivacionSaeExcel
                        }));
                    }
                }
                else if(!this.props.odcReport.originacionOperacionesSae.loading && 
                        this.props.odcReport.originacionOperacionesSae.response && 
                        !this.props.odcReport.originacionOperacionesSae.success){
                    // Notistack
                    this.getNotistack(this.props.odcReport.originacionOperacionesSae.error, "error");
                    this.setState(state => ({  ...state, rows: [],  rowsExcel:[], columnExcel: []}));
                }
                else if(!this.props.odcReport.originacionOperacionesSae.loading && 
                        !this.props.odcReport.originacionOperacionesSae.response && 
                        !this.props.odcReport.originacionOperacionesSae.success){
                    // Notistack
                    this.getNotistack(this.props.odcReport.originacionOperacionesSae.error, "error");
                    this.setState(state => ({  ...state, rows: [],  rowsExcel:[], columnExcel: []}));
                }
            }
        }
    }

    componentWillMount = () => {
        const columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns:columns
        }));
    }

    getColumnsRender(){
        let columns = [];
        if(this.props.width === "xs"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'fec_reg', title: 'Fecha de Registro'},
                { name: 'hora_reg', title: 'Hora de Registro' },
            ]
        }
        else if(this.props.width === "sm"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'fec_reg', title: 'Fecha de Registro'},
                { name: 'hora_reg', title: 'Hora de Registro' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                {name: 'num_tarjeta_pmp', title: 'Número de Tarjeta LP'},
                {name: 'des_cci', title: 'Número CCI'}
            ]
        }
        else if(this.props.width === "md"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'fec_reg', title: 'Fecha de Registro'},
                { name: 'hora_reg', title: 'Hora de Registro' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                {name: 'num_tarjeta_pmp', title: 'Número de Tarjeta LP'},
                {name: 'des_cci', title: 'Número CCI'},
                { name: 'des_entidad_financiera', title: 'Nombre del otro Banco' },
                { name: 'monto_cci', title: 'Importe de desembolso' },
                { name: 'cuota', title: 'Número de cuotas' },
                { name: 'cod_responsable', title: 'Cód. Usuario registro'},
                { name: 'cod_agencia', title: 'Cód. Agencia'},
                { name: 'des_agencia', title: 'Nombre de la Agencia'},
                { name: 'cod_comercio', title: 'Cód. Comercio'},
                { name: 'cod_responsable', title: 'Cód. Usuario Activación'},
                { name: 'fec_reg', title: 'Fecha Activación'},
                { name: 'hora_reg', title: 'Hora Activación'}
            ]
        }
        else if(this.props.width === "lg"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'fec_reg', title: 'Fecha de Registro'},
                { name: 'hora_reg', title: 'Hora de Registro' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                {name: 'num_tarjeta_pmp', title: 'Número de Tarjeta LP'},
                {name: 'des_cci', title: 'Número CCI'},
                { name: 'des_entidad_financiera', title: 'Nombre del otro Banco' },
                { name: 'monto_cci', title: 'Importe de desembolso' },
                { name: 'cuota', title: 'Número de cuotas' },
                { name: 'cod_responsable', title: 'Cód. Usuario registro'},
                { name: 'cod_agencia', title: 'Cód. Agencia'},
                { name: 'des_agencia', title: 'Nombre de la Agencia'},
                { name: 'cod_comercio', title: 'Cód. Comercio'},
                { name: 'cod_responsable', title: 'Cód. Usuario Activación'},
                { name: 'fec_reg', title: 'Fecha Activación'},
                { name: 'hora_reg', title: 'Hora Activación'}
            ]
        }
        else if(this.props.width === "xl"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'fec_reg', title: 'Fecha de Registro'},
                { name: 'hora_reg', title: 'Hora de Registro' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                {name: 'num_tarjeta_pmp', title: 'Número de Tarjeta LP'},
                {name: 'des_cci', title: 'Número CCI'},
                { name: 'des_entidad_financiera', title: 'Nombre del otro Banco' },
                { name: 'monto_cci', title: 'Importe de desembolso' },
                { name: 'cuota', title: 'Número de cuotas' },
                { name: 'cod_responsable', title: 'Cód. Usuario registro'},
                { name: 'cod_agencia', title: 'Cód. Agencia'},
                { name: 'des_agencia', title: 'Nombre de la Agencia'},
                { name: 'cod_comercio', title: 'Cód. Comercio'},
                { name: 'cod_responsable', title: 'Cód. Usuario Activación'},
                { name: 'fec_reg', title: 'Fecha Activación'},
                { name: 'hora_reg', title: 'Hora Activación'}
            ]
        }
        return columns;
    }
    
    handleSubmitFilter = data => {
        if(data){
            let sendData = {
                originacionOperacionesSae:{
                    cod_agencia: data.registerAgencyId,
                    fec_inicial: data.initialRegistrationDate,
                    fec_final: data.finalRegistrationDate,
                }
            }     
            this.props.getReportOriginationOperationsSae(sendData)
        }
    }
    handleShowHideSearchInput = e => {
        this.setState(state => ({
            showHideSearchInput: !state.showHideSearchInput
        }));
    }
    handleClickOpenSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: true
        }));
    }
    handleClickCloseSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: false
        }));
    }
    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render() {
        const { odcReport, width,  } = this.props;
        const { 
                columns,
                defaultColumnWidths,
                defaultHiddenColumnNames,
                rows,
                rowsExcel,
                columnExcel,
                tableColumnExtensions
            } = this.state;
            return (
                <Grid container className="p-1">
                    <Grid  item xs={12}>
                        <div style={{height: 12}}>
                            {
                                odcReport.originacionOperacionesSae.loading ?  <LinearProgress /> : ""
                            }
                        </div>
                    </Grid>
                    <Grid item xs={12}>
                        <HeaderReport 
                            title={"Reporte de Solicitud de Tarjetas SAE" }
                            columnExcel={columnExcel} 
                            dataExcel={rowsExcel}
                            data={rows}
                            nameSheet={"Reporte de Solicitud de Tarj."} 
                            nameFile={`Reporte_Solicitudes_Tarjetas_sae_${moment().format('DDMMYYYY')}`}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Divider className="mb-2"/>  
                    </Grid>
                    <Grid item xs={12} className="mb-2">
                        <FormPanel title={"Filtros de búsqueda"} 
                            handleSubmitFilter={this.handleSubmitFilter}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <DevGridComponent 
                            rows={rows}
                            columns={columns}
                            width={width}
                            tableColumnExtensions={tableColumnExtensions}
                            defaultColumnWidths={defaultColumnWidths}
                            defaultHiddenColumnNames={defaultHiddenColumnNames}
                            RowDetailComponent={RowDetail}
                            CellComponent={Cell}/>
                    </Grid>
                </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(OriginationOperationsSae))));

