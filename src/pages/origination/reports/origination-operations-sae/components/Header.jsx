import React from 'react';
import ClassNames from 'classnames';
import { 
    Paper,
    Typography,
    withStyles,
    Tooltip
} from '@material-ui/core';
import {Zoom} from 'react-reveal';
import CencosudScotiaBank from '../../../../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg';
// Icons
import ExportExcel from '../../../../../components/file/ExportExcel';

const styles = theme =>({
    rootWrapper:{
       borderBottom:"1px solid #f7f7f7" 
    },
    avatar:{
        [theme.breakpoints.down('md')]: {
            width: 68, 
            height: 36
        },
        width:80, 
        height:48
    },
    root:{
        display: "flex", 
        justifyContent: "space-between", 
        alignItems: "center",
        padding: "0 .5em"
       
    },
    actionRoot:{
        display: "flex", 
        justifyContent: "center", 
        alignItems: "center"
    }, 
    title:{
        fontWeight: "bold",
        [theme.breakpoints.down('md')]: {
            fontSize:14
        }
    }
});


const Header = ({title="", express = 0, reprint = 0, additional = 0, max = 999, excel, classes}) => (
    <div className={ClassNames(classes.rootWrapper, "py-1")} color="primary">
        <figure className={classes.root}>
            <img
                className={classes.avatar}
                src={CencosudScotiaBank}
                alt="Cencosud ScotiaBank" 
                />
            <figcaption className="px-2">
                <Typography align="left" variant="h5" component="h4" className={classes.title}>
                    {title}
                </Typography>
            </figcaption>
            <div className={classes.actionRoot}>
                <Tooltip TransitionComponent={Zoom} title="Exportar a Excel.">
                    <div className="border-right pr-2">
                        <ExportExcel nameFile={excel.nameFile} workBook={excel.workBook}/>
                    </div>
                </Tooltip>
            </div>
        </figure>
    </div>
)



Header.defaultProps = {
    excel: {
        workBook: [
            {   sheet:{
                    data: [],
                    columns: [],
                    nameSheet: "ODC - Reportes"
                }
            }
        ],
        nameFile: "Datos exportados"
    }
};


export default withStyles(styles)(Header);