import React from 'react';
import { 
    Paper,
    withStyles
} from '@material-ui/core';
import Header from './Header';

const styles = theme => ({
    root:{
        width: "100%",
        boxShadow: 'none',
        borderBottom: "1px solid #80808033",
    }
});

const HeaderIndicator = ({ title, data = [], columnExcel = [], indicator=[], dateDiffDays=0, nameSheet, nameFile, classes, ...props }) => {
    return (
            <Paper className="w-100">
                <Header 
                    title={title} 
                    excel={{
                        workBook: [
                            {   sheet:{
                                    data: data,
                                    columns: columnExcel,
                                    nameSheet: nameSheet
                                }
                            }
                        ],
                        nameFile: nameFile
                    }}
                />
            </Paper>
    )
}

export default withStyles(styles)(HeaderIndicator);
