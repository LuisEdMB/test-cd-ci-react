import React from "react";
import { withStyles, Typography } from "@material-ui/core";

const styles = theme => ({
    fontSizeDefault: {
        fontSize:12
    },
});

const RowDetail = ({ row, classes }) => (
    <ul>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Solicitud:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.cod_solicitud_completa }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Tipo Proceso:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_tipo_operacion }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Tipo Documento:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_tipo_documento }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Número Documento:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_nro_documento }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Apellidos y Nombres:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.des_nombre_completo }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Flujo - Fase - Estado:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_jerarquias_flujo_fase_estado }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Nro Cuenta:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.num_cuenta_pmp }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Nro Tarjeta:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.num_tarjeta_pmp }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Producto:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_nombre_producto }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Validación:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_tipo_validacion }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Tipo Relación:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
               { row.des_tipo_relacion }
            </Typography>
        </li>
       
        
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Agencia Registro:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_agencia }
            </Typography>
        </li>
       
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Usuario Creador:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_usu_reg }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha Registro:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.fec_reg }
            </Typography>
        </li>

        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Agencia Emboce:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_agencia_emboce }
            </Typography>
        </li>
       
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Usuario Emboce:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_usu_reg_emboce }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha Emboce:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.fec_reg_emboce }
            </Typography>
        </li>
        
    </ul>
);

export default withStyles(styles)(RowDetail);

