import React from "react";
import * as moment from 'moment';
import { Table } from '@devexpress/dx-react-grid-material-ui';

const  Cell = ({...props}) => {
    /*if(props.column.name === "fec_reg"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY') : "";
        return <Table.Cell {...props} value={value} />
    }
    else*/ if(props.column.name === "fec_reg_emboce"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    return <Table.Cell {...props} />
}

export default Cell;
