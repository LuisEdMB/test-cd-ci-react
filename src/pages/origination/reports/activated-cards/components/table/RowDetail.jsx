import React from "react";
import { withStyles, Typography } from "@material-ui/core";

const styles = theme => ({
    fontSizeDefault: {
        fontSize: 12
    },
});

const RowDetail = ({ row, classes }) => (
    <ul>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Solicitud:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.cod_solicitud_completa}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Nro. Doc Titular:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.des_nro_documento_titular}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Nro. Doc. Adicional:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.des_nro_documento_adicional}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Código Biometrico (Titular):
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.codigo_biometrico}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Cliente titular:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.nombre_completo_titular}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Producto:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.des_nombre_producto}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Promotor:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.cod_escaner}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Agencia Activación:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.nom_age_activa}
            </Typography>
        </li>

        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Usuario de Activacion:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.des_usu_activa_tc}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha y Hora de Activación:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.fecha_activacion_tc_texto}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Agencia Emboce:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.des_agencia}
            </Typography>
        </li>

        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Usuario Emboce:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.des_usu_emboce}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha y Hora de Emboce:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.fecha_emboce_texto}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Usuario Creador:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.des_usu_reg_solicitud}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha y Hora de Registro:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.fecha_registro_solicitud_texto}
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Agencia Registro:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {row.nom_age_solicitud}
            </Typography>
        </li>

    </ul>
);

export default withStyles(styles)(RowDetail);

