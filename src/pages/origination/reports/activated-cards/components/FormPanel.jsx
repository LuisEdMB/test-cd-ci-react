import React, { Component } from "react";
import * as moment from 'moment';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Autocomplete from '../../../../../components/Autocomplete';
import {
    Grid,
    Button,
    TextField,
    InputAdornment,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Typography,
    withStyles
} from '@material-ui/core';
import { getProcess, getSubProcess } from '../../../../../actions/generic/process-subProcess';
import { getAgency } from '../../../../../actions/generic/agency';
// Icons
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FilterListIcon from '@material-ui/icons/FilterList';
import SearchIcon from '@material-ui/icons/Search';
// Utils
import { getNotistack } from '../../../../../utils/Notistack';
import { defaultMinDate } from '../../../../../utils/Utils';
import { Bounce } from 'react-reveal';

const styles = theme => ({
    root: {
        width: "100%",
        //borderRadius: "0 0 .5em .5em",
        boxShadow: 'none',
        borderBottom: "1px solid #80808033",
    },
    paper: {
        padding: 20
    },
    buttonWrapper: {
        display: "center",
        alignItems: "center"
    },
    heading: {
        display: "flex",
        alignItem: "center",
        [theme.breakpoints.down('sm')]: {
            fontSize: 14,
        },
        [theme.breakpoints.up('md')]: {
            fontSize: 16,
        }
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        agency: state.agencyReducer,
        processSubProcess: state.processSubProcessReducer,
        odcReport: state.odcReportReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getAgency: bindActionCreators(getAgency, dispatch),
        getProcess: bindActionCreators(getProcess, dispatch),
        getSubProcess: bindActionCreators(getSubProcess, dispatch),
    };
    return actions;
}

class FormPanel extends Component {
    state = {

        enableRegisterAgency: true,

        registerAgencyError: false,
        registerAgency: "",
        registerAgencyId: "",

        initialRegistrationDateError: false,
        initialRegistrationDate: "",

        finalRegistrationDateError: false,
        finalRegistrationDate: "",

        processError: false,
        processId: "",
        process: "",

        subProcessDisabled: true,
        subProcessLoading: false,
        subProcessError: false,
        subProcessId: "",
        subProcess: "",
    }

    componentWillMount = () => {
        let initialRegistrationDate = moment().format('YYYY-MM-DD'); //moment().subtract(7, "day").format('YYYY-MM-DD');
        let finalRegistrationDate = moment().format('YYYY-MM-DD');

        this.setState(state => ({
            ...state,
            initialRegistrationDate: initialRegistrationDate,
            finalRegistrationDate: finalRegistrationDate
        }));
    }

    componentDidMount = () => {
        // Get Api's Services
        this.props.getAgency();
        this.props.getProcess(1, 0);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.agency !== this.props.agency) {
            // Identification Document Type - Error Service 
            if (!this.props.agency.loading &&
                this.props.agency.response &&
                !this.props.agency.success) {

                getNotistack(`Agencia: ${this.props.agency.error}`, this.props.enqueueSnackbar, "error");
            }
            // Identification Document Type - Error Service Connectivity
            else if (!this.props.agency.loading &&
                !this.props.agency.response &&
                !this.props.agency.success) {

                getNotistack(`Agencia: ${this.props.agency.error}`, this.props.enqueueSnackbar, "error");
            }
        }
    }

    handleSubmitSearch = e => {
        e.preventDefault();

        const {
            registerAgencyId,
            processId,
            subProcessId,
            initialRegistrationDate,
            finalRegistrationDate,
        } = this.state;

        let sendData = {
            registerAgencyId: this.props.enableRegisterAgency ? registerAgencyId : this.props.registerAgencyId,
            processId,
            subProcessId,
            initialRegistrationDate,
            finalRegistrationDate,
        }
        this.props.handleSubmitFilter(sendData);
    }

    // Client - Select - Event Change
    handleChangeSelectRequired = (name) => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        if (e !== null) {
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }

        this.setState(state => ({
            ...state,
            [name]: valueDefault,
            [nameId]: idDefault,
            [nameError]: error,
        }));
    }

    handleChangeProcess = name => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";

        if (e !== null) {
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }

        this.props.getSubProcess(2, idDefault);

        this.setState(state => ({
            ...state,
            [name]: valueDefault,
            [nameId]: idDefault,
            [nameError]: error,
            subProcessDisabled: !idDefault ? true : false,
            subProcessId: "",
            subProcess: "",
        }));
    }

    handleChangeSubProcess = name => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        if (e !== null) {
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }

        this.setState(state => ({
            ...state,
            [name]: valueDefault,
            [nameId]: idDefault,
            [nameError]: error,
        }));
    }

    handleChangeTextFieldRequired = name => e => {
        e.persist();
        let { value } = e.target;
        let nameError = `${name}Error`;

        this.setState(state => ({
            ...state,
            [name]: value,
            [nameError]: value !== "" ? false : true
        }));
    }


    render = () => {
        const { classes,
            title,
            form,
            agency,
            processSubProcess } = this.props;

        const agencyData = agency.data.map((item, index) => ({
            value: item.ide_agencia, //item.cod_agencia, 
            label: item.des_agencia
        }));

        const processData = processSubProcess.process.data.map((item, index) => ({
            value: item.cod_valor,
            label: item.des_valor
        }));

        const subProcessData = processSubProcess.subProcess.data.map((item, index) => ({
            value: item.cod_valor,
            label: item.des_valor
        }));

        return (<div>
            <ExpansionPanel className={classes.root} defaultExpanded>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel-content"
                    id="panel-header"
                >
                    <Typography variant="h6" className={classes.heading}>
                        <FilterListIcon className="mr-2" />
                        {title}
                    </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="w-100">
                        <form method="post" autoComplete="off" onSubmit={this.handleSubmitSearch}>
                            <Grid container spacing={8} className="mb-2">

                                {/* Register Agency */}
                                {
                                    this.props.enableRegisterAgency &&
                                    <Grid item xs={12} sm={6} md={4} lg={2}>
                                        <Autocomplete
                                            onChange={this.handleChangeSelectRequired("registerAgency")}
                                            value={this.state.registerAgencyId}
                                            data={agencyData}
                                            placeholder={"Agencia de Activación"}
                                        />
                                    </Grid>
                                }

                                {/* Procesos */}
                                <Grid item xs={12} sm={6} md={4} lg={2}>
                                    <Autocomplete
                                        onChange={this.handleChangeProcess("process")}
                                        value={this.state.processId}
                                        data={processData}
                                        placeholder={"Proceso"}
                                    />
                                </Grid>

                                {/* SubProcesos */}
                                <Grid item xs={12} sm={6} md={4} lg={2}>
                                    <Autocomplete
                                        loading={this.state.subProcessLoading}
                                        disabled={this.state.subProcessDisabled}
                                        onChange={this.handleChangeSubProcess("subProcess")}
                                        value={this.state.subProcessId}
                                        data={subProcessData}
                                        placeholder={"Sub Proceso"}
                                    />
                                </Grid>
                                {/* Initial Registration Date */}
                                <Grid item xs={6} sm={6} md={4} lg={2}>
                                    <TextField
                                        error={form.finalDateError}
                                        fullWidth={true}
                                        name="finalDate"
                                        label="Fecha Activación Inicial"
                                        type="date"
                                        margin="normal"
                                        format="DD-MM-YYYY"
                                        defaultValue={this.state.initialRegistrationDate}
                                        onChange={this.handleChangeTextFieldRequired("initialRegistrationDate")}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        InputProps={{
                                            inputProps: {
                                                min: defaultMinDate,
                                                max: moment().format('YYYY-MM-DD')
                                            },
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <CalendarTodayIcon color={form.startDateError ? "secondary" : "inherit"} fontSize="small" />
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                </Grid>
                                {/* Final Registration Date */}
                                <Grid item xs={6} sm={6} md={4} lg={2}>
                                    <TextField
                                        error={form.initialDateError}
                                        fullWidth={true}
                                        name="initialDate"
                                        label="Fecha Activación Final"
                                        type="date"
                                        margin="normal"
                                        format="DD-MM-YYYY"
                                        defaultValue={this.state.finalRegistrationDate}
                                        onChange={this.handleChangeTextFieldRequired("finalRegistrationDate")}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        InputProps={{
                                            inputProps: {
                                                min: defaultMinDate,
                                                max: moment().format('YYYY-MM-DD')
                                            },
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <CalendarTodayIcon color={form.finalDateError ? "secondary" : "inherit"} fontSize="small" />
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                </Grid>

                                <Grid item xs={12} sm={12} md={4} lg={1} className="d-flex justify-content-center align-items-center">
                                    <div className="w-100">
                                        <Bounce>
                                            <div>
                                                <Button 
                                                    className={classes.button} fullWidth variant="contained" type="submit" color="primary"
                                                    disabled={this.props.odcReport.activatedCards.loading}>
                                                    Consultar
                                                    <SearchIcon className="ml-2" size="small" />
                                                </Button>
                                            </div>
                                        </Bounce>
                                    </div>
                                </Grid>
                            </Grid>
                        </form>
                    </div>

                </ExpansionPanelDetails>
            </ExpansionPanel>
        </div>
        )
    }
}


FormPanel.defaultProps = {
    form: {
        agencyId: 0,
        initialDateError: false,
        finalDateError: false,
        initialDate: "",
        finalDate: ""
    }
};

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(FormPanel)));
