import React, { Component } from "react";
import * as moment from "moment";
import { withSnackbar } from "notistack";
import withWidth from "@material-ui/core/withWidth";
import { bindActionCreators } from "redux";
// React Router
import { connect } from "react-redux";
// Component
import {
  IconButton,
  Grid,
  LinearProgress,
  withStyles,
  Divider,
} from "@material-ui/core";
// Icons
import CloseIcon from "@material-ui/icons/Close";
// Components Custom
import DevGridComponent from "../../../../components/dev-grid/DevGridComponent";
import Cell from "./components/table/Cell";
import RowDetail from "./components/table/RowDetail";
import HeaderReportDownloadExcel from "../../../../components/header-report/HeaderReportDownloadExcel";
import FormPanel from "./components/FormPanel";

// Actions
import { getReportActivatedCards, getReportActivatedCardsExcel } from "../../../../actions/odc-report/odc-report";
// Utils
import { decrypt } from "../../../../utils/Utils";

const styles = _ => ({
  avatar: {
    width: 80,
    height: 48,
  },
  button: {
    textTransform: "none",
  },
});

const mapStateToProps = (state) => {
  return {
    odcReport: state.odcReportReducer,
  };
};
const mapDispatchToProps = (dispatch) => {
  const actions = {
    getReportActivatedCards: bindActionCreators(getReportActivatedCards, dispatch),
  };
  return actions;
};

class ActivatedCards extends Component {
  state = {
    columnExcel: [],
    defaultColumnWidths: [
      { columnName: "cod_solicitud_completa", width: 125 },
      { columnName: "des_nro_documento_titular", width: 150 },
      { columnName: "des_nro_documento_adicional", width: 150 },
      { columnName: "codigo_biometrico", width: 150 },
      { columnName: "nombre_completo_titular", width: 300 },
      { columnName: "motivo_reimpresion", width: 150 },
      { columnName: "num_tarjeta_tc", width: 150 },
      { columnName: "num_cuenta", width: 150 },
      { columnName: "des_nombre_producto", width: 200 },
      { columnName: "tipo_proceso", width: 200 },
      { columnName: "nom_age_activa", width: 200 },
      { columnName: "fecha_activacion_tc_texto", width: 200 },
      { columnName: "des_usu_activa_tc", width: 200 },
      { columnName: "des_agencia", width: 200 },
      { columnName: "des_usu_emboce", width: 200 },
      { columnName: "fecha_emboce_texto", width: 200 },
      { columnName: "fecha_validacion_biometrico_texto", width: 200 },
      { columnName: "des_usu_valida_biometrico", width: 200 },
      { columnName: "nom_age_solicitud", width: 200 },
      { columnName: "des_usu_reg_solicitud", width: 200 },
      { columnName: "fecha_registro_solicitud_texto", width: 200 },
      { columnName: "cod_escaner", width: 125 },
    ],
    columns: [],
    tableColumnExtensions: [],
    defaultHiddenColumnNames: [],
    rows: [],
    rowsExcel: [],
    additional: 0,
    reprint: 0,
    express: 0,

    dateDiffDays: 0,

    enableRegisterAgency: true,
    registerAgencyId: 0,

    initialRegistrationDate: moment().format("YYYY-MM-DD"),
    finalRegistrationDate: moment().format("YYYY-MM-DD")
  };
  componentDidUpdate = (prevProps) => {
    if (prevProps.width !== this.props.width) {
      let columns = this.getColumnsRender();
      this.setState((state) => ({
        ...state,
        columns: columns,
      }));
    }
    if (
      prevProps.odcReport.activatedCards !== this.props.odcReport.activatedCards
    ) {
      if (this.props.odcReport.activatedCards.loading) {
        this.setState((state) => ({
          ...state,
          rows: [...state.rows],
          rowsExcel: [...state.rowsExcel]
        }));
      }

      if (
        !this.props.odcReport.activatedCards.loading &&
        this.props.odcReport.activatedCards.response &&
        this.props.odcReport.activatedCards.success
      ) {
        let { data } = this.props.odcReport.activatedCards;

        this.setState(state => ({
          ...state,
          rows: data.tarjetasActivadas || [],
          rowsExcel: [],
          columnExcel: []
        }))
      } else if (
        !this.props.odcReport.activatedCards.loading &&
        this.props.odcReport.activatedCards.response &&
        !this.props.odcReport.activatedCards.success
      ) {
        // Notistack
        this.getNotistack(this.props.odcReport.activatedCards.error, "error");
        this.setState((state) => ({
          ...state,
          rows: [],
          rowsExcel: [],
          columnExcel: []
        }));
      } else if (
        !this.props.odcReport.activatedCards.loading &&
        !this.props.odcReport.activatedCards.response &&
        !this.props.odcReport.activatedCards.success
      ) {
        // Notistack
        this.getNotistack(this.props.odcReport.activatedCards.error, "error");
        this.setState((state) => ({
          ...state,
          rows: [],
          rowsExcel: [],
          columnExcel: []
        }));
      }
    }
  };

  componentWillMount = () => {
    const columns = this.getColumnsRender();
    this.setState((state) => ({
      ...state,
      columns: columns,
    }));
  };

  componentDidMount = () => {
    const dataEncrypt = sessionStorage.getItem("data");
    const decryptData = decrypt(dataEncrypt);
    let enableRegisterAgency = decryptData.profiles
      ? decryptData.profiles.id !== 4 && decryptData.profiles.id !== 5
      : true;
    let registerAgencyId = decryptData.agency
      ? enableRegisterAgency ? 0 : decryptData.agency.ide_agencia
      : 0;
    this.setState({
      enableRegisterAgency: enableRegisterAgency,
      registerAgencyId: registerAgencyId,
    });
  };

  getColumnsRender() {
    let columns = [];
    if (this.props.width === "xs") {
      columns = [
        { name: "cod_solicitud_completa", title: "Solicitud" },
        { name: "des_nro_documento_titular", title: "Nro. Doc Titular" },
        { name: "des_nro_documento_adicional", title: "Nro. Doc Adicional" },
      ];
    } else if (this.props.width === "sm") {
      columns = [
        { name: "cod_solicitud_completa", title: "Solicitud" },
        { name: "des_nro_documento_titular", title: "Nro. Doc Titular" },
        { name: "des_nro_documento_adicional", title: "Nro. Doc Adicional" },
      ];
    } else if (this.props.width === "md") {
      columns = [
        { name: "cod_solicitud_completa", title: "Solicitud" },
        { name: "des_nro_documento_titular", title: "Nro. Doc Titular " },
        { name: "des_nro_documento_adicional", title: "Nro. Doc Adicional" },
        { name: "fecha_registro_solicitud_texto", title: "Fecha Registro" },
      ];
    } else if (this.props.width === "lg") {
      columns = [
        { name: "cod_solicitud_completa", title: "Solicitud" },
        { name: "des_nro_documento_titular", title: "Nro. Doc. Titular" },
        { name: "des_nro_documento_adicional", title: "Nro. Doc. Adicional" },
        { name: "codigo_biometrico", title: "Código Biometrico (Titular)" },
        {
          name: "nombre_completo_titular",
          title: "Apellidos y Nombres del titular",
        },
        { name: "motivo_reimpresion", title: "Motivo Reimpresión" },
        { name: "num_tarjeta_tc", title: "Nro Tarjeta" },
        { name: "num_cuenta", title: "Nro Cuenta" },
        { name: "des_nombre_producto", title: "Tipo Tarjeta" },
        { name: "tipo_proceso", title: "Validación" },
        { name: "nom_age_activa", title: "Agencia Activación" },
        {
          name: "fecha_activacion_tc_texto",
          title: "Fecha y Hora de Activacion",
        },
        { name: "des_usu_activa_tc", title: "Usuario de Activacion" },
        { name: "des_agencia", title: "Agencia Emboce" },
        { name: "des_usu_emboce", title: "Usuario Emboce" },
        { name: "fecha_emboce_texto", title: "Fecha y Hora de Emboce" },
        {
          name: "fecha_validacion_biometrico_texto",
          title: "Fecha y Hora de Validación Biométrica",
        },
        {
          name: "des_usu_valida_biometrico",
          title: "Usuario Validación Biométrica",
        },
        { name: "nom_age_solicitud", title: "Agencia Registro" },
        { name: "des_usu_reg_solicitud", title: "Usuario Creador" },
        {
          name: "fecha_registro_solicitud_texto",
          title: "Fecha y Hora de Registro",
        },
        { name: "cod_escaner", title: "Cod. Promotor" },
      ];
    } else if (this.props.width === "xl") {
      columns = [
        { name: "cod_solicitud_completa", title: "Solicitud" },
        { name: "des_nro_documento_titular", title: "Nro. Doc. Titular" },
        { name: "des_nro_documento_adicional", title: "Nro. Doc. Adicional" },
        { name: "codigo_biometrico", title: "Código Biometrico (Titular)" },
        {
          name: "nombre_completo_titular",
          title: "Apellidos y Nombres del titular",
        },
        { name: "motivo_reimpresion", title: "Motivo Reimpresión" },
        { name: "num_tarjeta_tc", title: "Nro Tarjeta" },
        { name: "num_cuenta", title: "Nro Cuenta" },
        { name: "des_nombre_producto", title: "Tipo Tarjeta" },
        { name: "tipo_proceso", title: "Validación" },
        { name: "nom_age_activa", title: "Agencia Activación" },
        {
          name: "fecha_activacion_tc_texto",
          title: "Fecha y Hora de Activación",
        },
        { name: "des_usu_activa_tc", title: "Usuario de Activacion" },
        { name: "des_agencia", title: "Agencia Emboce" },
        { name: "des_usu_emboce", title: "Usuario Emboce" },
        { name: "fecha_emboce_texto", title: "Fecha y Hora de Emboce" },
        {
          name: "fecha_validacion_biometrico_texto",
          title: "Fecha y Hora de Validación Biométrica",
        },
        {
          name: "des_usu_valida_biometrico",
          title: "Usuario Validación Biométrica",
        },
        { name: "nom_age_solicitud", title: "Agencia Registro" },
        { name: "des_usu_reg_solicitud", title: "Usuario Creador" },
        {
          name: "fecha_registro_solicitud_texto",
          title: "Fecha y Hora de Registro",
        },
        { name: "cod_escaner", title: "Cod. Promotor" },
      ];
    }
    return columns;
  }

  handleSubmitFilter = (data) => {
    if (data) {
      let sendData = {
        tarjetasActivadas: {
          cod_agencia: data.registerAgencyId ? data.registerAgencyId : 0,
          cod_proceso: data.processId ? data.processId : 0,
          cod_subproceso: data.subProcessId ? data.subProcessId : 0,
          fec_inicial: data.initialRegistrationDate,
          fec_final: data.finalRegistrationDate
        },
      };
      this.props.getReportActivatedCards(sendData);
      this.setState(state => ({
        ...state,
        rows: [],
        rowsExcel: [],
        columnExcel: [],
        registerAgencyId: data.registerAgencyId,
        processId: data.processId,
        subProcessId: data.subProcessId,
        initialRegistrationDate: data.initialRegistrationDate,
        finalRegistrationDate: data.finalRegistrationDate
      }))
    }
  };

  handleShowHideSearchInput = _ => {
    this.setState((state) => ({
      ...state,
      showHideSearchInput: !state.showHideSearchInput,
    }));
  };
  handleClickOpenSearchModal = _ => {
    this.setState((state) => ({
      ...state,
      openSearchModal: true,
    }));
  };
  handleClickCloseSearchModal = _ => {
    this.setState((state) => ({
      ...state,
      openSearchModal: false,
    }));
  };

  // Notistack
  getNotistack(message, variant = "default", duration = 6000) {
    let select = "default";
    switch (variant) {
      case "error":
        select = variant;
        break;
      case "success":
        select = variant;
        break;
      case "warning":
        select = variant;
        break;
      case "info":
        select = variant;
        break;
      default:
        select = variant;
        break;
    }
    // Notistack
    this.props.enqueueSnackbar(message, {
      variant: select,
      autoHideDuration: duration,
      action: (
        <IconButton>
          <CloseIcon size="small" className="text-white" color="inherit" />
        </IconButton>
      ),
    });
  }
  downloadDocument = _ => {
    const {
      registerAgencyId,
      processId,
      subProcessId,
      initialRegistrationDate,
      finalRegistrationDate,
    } = this.state;
    let sendData = {
      exportarReporteTarjetasActivadas: {
        cod_agencia: registerAgencyId,
        cod_proceso: processId,
        cod_subproceso: subProcessId,
        fec_inicial: initialRegistrationDate,
        fec_final: finalRegistrationDate,
        nameFile: `Reporte_Tarjetas_Activadas_${moment().format(
          "DDMMYYYY"
        )}`
      },
    };
    getReportActivatedCardsExcel(sendData);

  }
  render() {
    const { width, odcReport } = this.props;
    const {
      columns,
      defaultColumnWidths,
      defaultHiddenColumnNames,
      rows,
      rowsExcel,
      tableColumnExtensions,
      columnExcel,
    } = this.state;
    return (
      <Grid container className="p-1">
        <Grid item xs={12} style={{ height: 12 }}>
          {odcReport.activatedCards.loading ? <LinearProgress /> : ""}
        </Grid>
        <Grid item xs={12}>
          <HeaderReportDownloadExcel
            title={"Reporte de Tarjetas Activadas"}
            columnExcel={columnExcel}
            dataExcel={rowsExcel}
            downloadDocument={this.downloadDocument}
            data={rows}
            nameSheet={"Reporte de Tarjetas Activadas"}
            nameFile={`Reporte_Tarjetas_Activadas_${moment().format(
              "DDMMYYYY"
            )}`}
          />
        </Grid>
        <Grid item xs={12}>
          <Divider className="mb-2" />
        </Grid>
        <Grid item xs={12} className="mb-2">
          <FormPanel
            title={"Filtros de búsqueda"}
            enableRegisterAgency={this.state.enableRegisterAgency}
            registerAgencyId={this.state.registerAgencyId}
            handleSubmitFilter={this.handleSubmitFilter}
          />
        </Grid>

        <Grid item xs={12}>
          <DevGridComponent
            rows={rows}
            columns={columns}
            width={width}
            tableColumnExtensions={tableColumnExtensions}
            defaultColumnWidths={defaultColumnWidths}
            defaultHiddenColumnNames={defaultHiddenColumnNames}
            RowDetailComponent={RowDetail}
            CellComponent={Cell}
          />
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(
  withSnackbar(
    connect(mapStateToProps, mapDispatchToProps)(withWidth()(ActivatedCards))
  )
);
