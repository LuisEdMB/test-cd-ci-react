import React, { Component } from "react";
import * as moment from 'moment';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
import { bindActionCreators } from "redux";
// React Router
import { connect } from 'react-redux';
// Component
import {
    IconButton,
    Grid,
    LinearProgress,
    withStyles,
    Divider
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close';
// Components Custom
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
import HeaderReport from '../../../../components/header-report/HeaderReport';
import FormPanel from './components/FormPanel';

// Actions
import { getReportOriginationOperations, getReportOriginationOperationsIndicators } from '../../../../actions/odc-report/odc-report';

const styles = theme => ({
    avatar: {
        width: 80,
        height: 48
    },
    modalRoot: {

    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        odcReport: state.odcReportReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getReportOriginationOperations: bindActionCreators(getReportOriginationOperations, dispatch),
        getReportOriginationOperationsIndicators: bindActionCreators(getReportOriginationOperationsIndicators, dispatch)
    };
    return actions;
}

const formatDate = (date) => date.split(' ')[0] || ''

const formatTime = (date) => date.split(' ')[1] || ''

class OriginationOperations extends Component {

    state = {
        defaultColumnWidths: [
            { columnName: 'cod_solicitud_completa', width: 125 },
            { columnName: 'des_tipo_operacion', width: 120 },

            { columnName: 'des_tipo_documento', width: 100 },
            { columnName: 'des_nro_documento', width: 100 },

            { columnName: 'des_nombre_completo', width: 300 },

            { columnName: 'des_proceso', width: 150 },
            { columnName: 'des_subproceso', width: 150 },
            { columnName: 'des_jerarquias_flujo_fase_estado', width: 380 },
            { columnName: 'des_relacion', width: 380 },

            { columnName: 'des_motivo_reimpresion', width: 150 },
            { columnName: 'des_observacion', width: 150 },

            { columnName: 'num_cuenta_pmp', width: 200 },
            { columnName: 'num_tarjeta_pmp', width: 200 },
            { columnName: 'num_cuenta_paralela', width: 200 },
            { columnName: 'des_nombre_producto', width: 200 },

            { columnName: 'des_tipo_validacion', width: 120 },
            { columnName: 'fecha_tipo_validacion', width: 200 },
            { columnName: 'hora_tipo_validacion', width: 200 },
            { columnName: 'des_usu_reg_validacion', width: 200 },

            { columnName: 'des_tipo_relacion', width: 120 },

            { columnName: 'des_agencia', width: 140 },
            { columnName: 'des_usu_reg', width: 130 },
            { columnName: 'fec_reg_texto', width: 140 },
            { columnName: 'fecha_reg_texto', width: 140 },
            { columnName: 'hora_reg_texto', width: 140 },

            { columnName: 'des_agencia_emboce', width: 140 },
            { columnName: 'des_usu_reg_emboce', width: 130 },
            { columnName: 'fec_reg_emboce_texto', width: 200 },
            { columnName: 'fecha_reg_emboce_texto', width: 200 },
            { columnName: 'hora_reg_emboce_texto', width: 200 },
            { columnName: 'fecha_reg_activacion_texto', width: 200 },
            { columnName: 'hora_reg_activacion_texto', width: 200 },
            { columnName: 'des_usu_reg_pmp', width: 200 },
            { columnName: 'fecha_reg_activacion_texto_sae', width: 200 },
            { columnName: 'hora_reg_activacion_texto_sae', width: 200 },
            { columnName: 'des_usu_reg_pmp_sae', width: 200 },
            { columnName: 'observation', width: 150 },
        ],
        columns: [],
        tableColumnExtensions: [
            { columnName: 'authorize', align: 'center' },
            { columnName: 'observation', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        rowsExcel: [],
        additional: 0,
        reprint: 0,
        express: 0,
        pageSizes: [],
        columnExcel: [],

        dateDiffDays: 0
    };

    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }

        if (prevProps.odcReport !== this.props.odcReport) {
            if (prevProps.odcReport.originationOperations !== this.props.odcReport.originationOperations) {

                if (this.props.odcReport.originationOperations.loading) {
                    this.setState(state => ({
                        ...state,
                        rows: [...state.rows],
                    }));
                }

                if (!this.props.odcReport.originationOperations.loading &&
                    this.props.odcReport.originationOperations.response &&
                    this.props.odcReport.originationOperations.success) {

                    let { data } = this.props.odcReport.originationOperations;
                    let { originacionOperaciones, originacionOperacionesExcel } = data;

                    if (originacionOperaciones && originacionOperacionesExcel) {

                        let columnExcel = Object.keys(originacionOperacionesExcel.length > 0 && originacionOperacionesExcel[0]);
                        columnExcel = columnExcel.filter((item) => item !== "Nro Cuenta LP" && item !== "Fecha y Hora de Activacion LP" && item !== "Usuario de Activacion LP").map((item) => {
                            return { value: item, label: item }
                        })
                        let rows = originacionOperaciones.map((value, index) => {
                            value.fecha_tipo_validacion = value.fec_reg_validacion_texto ? formatDate(value.fec_reg_validacion_texto) : ''
                            value.hora_tipo_validacion = value.fec_reg_validacion_texto ? formatTime(value.fec_reg_validacion_texto) : ''
                            value.fecha_reg_activacion_texto = value.fec_reg_pmp_texto ? formatDate(value.fec_reg_pmp_texto) : ''
                            value.hora_reg_activacion_texto = value.fec_reg_pmp_texto ? formatTime(value.fec_reg_pmp_texto) : ''
                            value.fecha_reg_activacion_texto_sae = value.fec_reg_pmp_sae_texto ? formatDate(value.fec_reg_pmp_sae_texto) : ''
                            value.hora_reg_activacion_texto_sae = value.fec_reg_pmp_sae_texto ? formatTime(value.fec_reg_pmp_sae_texto) : ''
                            value.fecha_reg_texto = value.fec_reg_texto ? formatDate(value.fec_reg_texto) : ''
                            value.hora_reg_texto = value.fec_reg_texto ? formatTime(value.fec_reg_texto) : ''
                            value.fecha_reg_emboce_texto = value.fec_reg_emboce_texto ? formatDate(value.fec_reg_emboce_texto) : ''
                            value.hora_reg_emboce_texto = value.fec_reg_emboce_texto ? formatTime(value.fec_reg_emboce_texto) : ''
                            return value
                        })

                        this.setState(state => ({
                            ...state,
                            rows: rows,
                            columnExcel: columnExcel,
                            rowsExcel: originacionOperacionesExcel
                        }));
                    }
                }
                else if (!this.props.odcReport.originationOperations.loading &&
                    this.props.odcReport.originationOperations.response &&
                    !this.props.odcReport.originationOperations.success) {
                    // Notistack
                    this.getNotistack(this.props.odcReport.originationOperations.error, "error");
                    this.setState(state => ({ ...state, rows: [], rowsExcel: [], columnExcel: [] }));
                }
                else if (!this.props.odcReport.originationOperations.loading &&
                    !this.props.odcReport.originationOperations.response &&
                    !this.props.odcReport.originationOperations.success) {
                    // Notistack
                    this.getNotistack(this.props.odcReport.originationOperations.error, "error");
                    this.setState(state => ({ ...state, rows: [], rowsExcel: [], columnExcel: [] }));
                }
            }
        }
    }

    componentWillMount = () => {
        const columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns: columns
        }));
    }

    getColumnsRender() {
        let columns = [];
        if (this.props.width === "xs") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
            ]
        }
        else if (this.props.width === "sm") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
            ]
        }
        else if (this.props.width === "md") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'des_proceso', title: 'Proceso' },
                { name: 'des_subproceso', title: 'SubProceso' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo-Fase-Estado' },
                { name: 'des_motivo_reimpresion', title: "Motivo Reimpresión" },
                { name: 'num_tarjeta_pmp', title: "Nro. Tarjeta" },
                { name: 'des_tipo_validacion', title: "Validación" },
                { name: 'des_tipo_relacion', title: "Relación" },
                { name: 'des_agencia', title: "Agencia Registro" },
                { name: 'des_usu_reg', title: "Usuario Creador" },
                { name: 'fec_reg_texto', title: "Fecha y Hora de Registro" },
                { name: 'des_agencia_emboce', title: "Agencia Emboce" },
                { name: 'des_usu_reg_emboce', title: "Usuario Emboce" },
                { name: 'fec_reg_emboce_texto', title: "Fecha y Hora de Emboce" },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        else if (this.props.width === "lg") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'des_proceso', title: 'Proceso' },
                { name: 'des_subproceso', title: 'SubProceso' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo-Fase-Estado' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_motivo_reimpresion', title: "Motivo Reimpresión" },
                { name: 'des_observacion', title: "Observaciones" },
                { name: 'num_tarjeta_pmp', title: "Nro. Tarjeta" },
                { name: 'num_cuenta_pmp', title: "Nro. Cta" },
                { name: 'des_nombre_producto', title: "Tipo Tarjeta" },
                { name: 'des_tipo_validacion', title: "Validación" },
                { name: 'fecha_tipo_validacion', title: "Fecha de validación de Identidad" },
                { name: 'hora_tipo_validacion', title: "Hora de validación Identidad" },
                { name: 'des_usu_reg_validacion', title: "Usuario de Validación de Identidad" },
                { name: 'des_agencia', title: "Agencia Registro" },
                { name: 'des_usu_reg', title: "Usuario Creador" },
                { name: 'fecha_reg_texto', title: "Fecha de Registro" },
                { name: 'hora_reg_texto', title: "Hora de Registro" },
                { name: 'des_agencia_emboce', title: "Agencia Emboce" },
                { name: 'des_usu_reg_emboce', title: "Usuario Emboce" },
                { name: 'fecha_reg_emboce_texto', title: "Fecha de Emboce" },
                { name: 'hora_reg_emboce_texto', title: "Hora de Emboce" },
                { name: 'fecha_reg_activacion_texto', title: "Fecha  de activación" },
                { name: 'hora_reg_activacion_texto', title: "Hora de activación" },
                { name: 'des_usu_reg_pmp', title: "Usuario de Activación" },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        else if (this.props.width === "xl") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'des_proceso', title: 'Proceso' },
                { name: 'des_subproceso', title: 'SubProceso' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo-Fase-Estado' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_motivo_reimpresion', title: "Motivo Reimpresión" },
                { name: 'des_observacion', title: "Observaciones" },
                { name: 'num_tarjeta_pmp', title: "Nro. Tarjeta" },
                { name: 'num_cuenta_pmp', title: "Nro. Cta" },
                { name: 'des_nombre_producto', title: "Tipo Tarjeta" },
                { name: 'des_tipo_validacion', title: "Validación" },
                { name: 'fecha_tipo_validacion', title: "Fecha de validación de Identidad" },
                { name: 'hora_tipo_validacion', title: "Hora de validación Identidad" },
                { name: 'des_usu_reg_validacion', title: "Usuario de Validación de Identidad" },
                { name: 'des_agencia', title: "Agencia Registro" },
                { name: 'des_usu_reg', title: "Usuario Creador" },
                { name: 'fecha_reg_texto', title: "Fecha de Registro" },
                { name: 'hora_reg_texto', title: "Hora de Registro" },
                { name: 'des_agencia_emboce', title: "Agencia Emboce" },
                { name: 'des_usu_reg_emboce', title: "Usuario Emboce" },
                { name: 'fecha_reg_emboce_texto', title: "Fecha de Emboce" },
                { name: 'hora_reg_emboce_texto', title: "Hora de Emboce" },
                { name: 'fecha_reg_activacion_texto', title: "Fecha  de activación" },
                { name: 'hora_reg_activacion_texto', title: "Hora de activación" },
                { name: 'des_usu_reg_pmp', title: "Usuario de Activación" },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        return columns;
    }

    handleSubmitFilter = data => {
        if (data) {
            let sendData = {
                originacionOperaciones: {
                    cod_agencia: data.registerAgencyId,
                    cod_agencia_emboce: data.embossingAgencyId,
                    cod_proceso: data.processId,
                    cod_subproceso: data.subProcessId,
                    cod_flujo_fase_estado: data.flowPhaseStateId,
                    des_siglas_flujo_fase_estado: data.flowPhaseStateId,
                    cod_tipo_documento: data.documentTypeAux,
                    des_nro_documento: data.documentNumber,
                    fec_inicial: data.initialRegistrationDate,
                    fec_final: data.finalRegistrationDate,
                }
            }
            this.props.getReportOriginationOperations(sendData);
        }
    }
    handleShowHideSearchInput = e => {
        this.setState(state => ({
            showHideSearchInput: !state.showHideSearchInput
        }));
    }
    handleClickOpenSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: true
        }));
    }
    handleClickCloseSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: false
        }));
    }
    // Notistack
    getNotistack(message, variant = "default", duration = 6000) {
        let select = "default";
        switch (variant) {
            case "error":
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default:
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit" />
                </IconButton>
            ),
        });
    }
    render() {
        const { odcReport, width, } = this.props;
        const {
            columns,
            defaultColumnWidths,
            defaultHiddenColumnNames,
            rows,
            rowsExcel,
            columnExcel,
            tableColumnExtensions
        } = this.state;
        return (
            <Grid container className="p-1">
                <Grid item xs={12}>
                    <div style={{ height: 12 }}>
                        {
                            odcReport.originationOperations.loading ? <LinearProgress /> : ""
                        }
                    </div>
                </Grid>
                <Grid item xs={12}>
                    <HeaderReport
                        title={"Reporte de Solicitud de Tarjetas"}
                        columnExcel={columnExcel}
                        dataExcel={rowsExcel}
                        data={rows}
                        nameSheet={"Reporte de Solicitud de Tarj."}
                        nameFile={`Reporte_Solicitudes_Tarjetas_${moment().format('DDMMYYYY')}`}
                    />
                </Grid>
                <Grid item xs={12}>
                    <Divider className="mb-2" />
                </Grid>
                <Grid item xs={12} className="mb-2">
                    <FormPanel title={"Filtros de búsqueda"}
                        handleSubmitFilter={this.handleSubmitFilter}
                    />
                </Grid>

                <Grid item xs={12}>
                    <DevGridComponent
                        rows={rows}
                        columns={columns}
                        width={width}
                        tableColumnExtensions={tableColumnExtensions}
                        defaultColumnWidths={defaultColumnWidths}
                        defaultHiddenColumnNames={defaultHiddenColumnNames}
                        RowDetailComponent={RowDetail}
                        CellComponent={Cell} />
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(OriginationOperations))));
