import React, { Component } from "react";
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Draggable from 'react-draggable';

// Components 
import { 
    Fab, 
    Tooltip, 
    Button,
    withStyles, 
    Dialog,
    DialogTitle,
    Typography,
    DialogContent,
    Grid,
    DialogActions,
    CircularProgress, 
    Paper, 
    TextField, 
    IconButton,
    FormHelperText
 } from "@material-ui/core";
// Colors
import green from '@material-ui/core/colors/green';
// Icons 
import CheckCircleOutlineIcon from '@material-ui/icons/VerifiedUser'; 
import SendIcon from '@material-ui/icons/Send';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close'; 
// Actions 
import { authorizeActivationProcess } from '../../../../../../actions/odc-reprint/odc-reprint';
const styles = theme => ({
    modalRoot:{
        minWidth:280, 
        width:350,
        maxWidth:400,
    },
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: green[700],
        '&:hover': {
            backgroundColor: green[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    buttonProgressWrapper:{
        position: 'relative'
    },
    button:{
        textTransform: 'none',
    },
    buttonCustom:{
        color: theme.palette.getContrastText(green[500]),
        '&:hover': {
            backgroundColor: green[700],
        },
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    }
});

function PaperComponent(props) {
    return (
      <Draggable>
        <Paper {...props} />
      </Draggable>
    );
}

const mapStateToProps = (state, props) => {
    return {
        odcReprint: state.odcReprintReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        authorizeActivationProcess: bindActionCreators(authorizeActivationProcess, dispatch)
    };
  return actions;
}

class AuthorizeActionButton extends Component {
    state = {
        openAuthorizeModal: false,
        authorizeButtonLoading: false, 
        authorizeButtonDisabled: false, 
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.odcReprint.autorizeActivationProcess !== this.props.odcReprint.autorizeActivationProcess) {	
            if(this.props.odcReprint.autorizeActivationProcess.loading){
                this.setState(state => ({
                    ...state,
                    authorizeButtonLoading: true, 
                    authorizeButtonDisabled: true
                }));
            }
            if(!this.props.odcReprint.autorizeActivationProcess.loading && 
                this.props.odcReprint.autorizeActivationProcess.response && 
                this.props.odcReprint.autorizeActivationProcess.success){
                this.setState(state => ({
                    ...state,
                    authorizeButtonLoading: false, 
                    authorizeButtonDisabled: false, 
                    openAuthorizeModal: false
                }));
            }
            else if(!this.props.odcReprint.autorizeActivationProcess.loading && 
                     this.props.odcReprint.autorizeActivationProcess.response && 
                    !this.props.odcReprint.autorizeActivationProcess.success){
                this.setState(state => ({
                    ...state,
                    authorizeButtonLoading: false, 
                    authorizeButtonDisabled: false
                }));
            }
            else if(!this.props.odcReprint.autorizeActivationProcess.loading && 
                    !this.props.odcReprint.autorizeActivationProcess.response && 
                    !this.props.odcReprint.autorizeActivationProcess.success){
                this.setState(state => ({
                    ...state,
                    authorizeButtonLoading: false, 
                    authorizeButtonDisabled: false
                }));
            }
        }
    }
    handleSubmitAuthorizeCreditCardActivation = row => e => {
        e.preventDefault();
        let sendData = {
            cod_requerimiento: row.cod_requerimiento,
            nom_actividad:"Activación Tarjeta: Autorizado",
            cod_flujo_fase_estado_actual: 30904,
            cod_flujo_fase_estado_anterior: 30903,
            cod_solicitud: row.cod_solicitud,
            cod_solicitud_completa: row.cod_solicitud_completa,
        }
       this.props.authorizeActivationProcess(sendData);
    }

    handleClickAuthorizeCreditCardActivationToggleModal = e => {
        this.setState(state => ({
            ...state,
            openAuthorizeModal: !state.openAuthorizeModal
        }));
    }

    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render = () => {
        const { classes, row } = this.props;
        let { openAuthorizeModal, authorizeButtonLoading, authorizeButtonDisabled } = this.state;
        return  (
            <React.Fragment>
                <Tooltip title={`Autorizar Solicitud: ${row.cod_solicitud_completa}`} placement="left">
                    <div>
                        <Fab className={classes.fab} onClick={this.handleClickAuthorizeCreditCardActivationToggleModal}>
                            <CheckCircleOutlineIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>  
                            
                <Dialog
                    onClose={this.handleClickAuthorizeCreditCardActivationToggleModal}
                    open={openAuthorizeModal}
                    PaperComponent={PaperComponent}
                    aria-labelledby="form-authorize">
                    <DialogTitle 
                        id="form-authorize" 
                        className="bg-metal-blue">
                       <Typography align="center" component="span" variant="h6" className="text-white text-shadow-black">
                           Autorizar Activación TC.
                        </Typography>
                    </DialogTitle>
                    <form  onSubmit={this.handleSubmitAuthorizeCreditCardActivation(row)} className={classes.modalRoot}>
                        <DialogContent>
                            <Typography align="center">
                                ¿Deseas autorizar la activación de la Tarjeta de Crédito para la solicitud: <strong> {row.cod_solicitud_completa}</strong>?
                            </Typography>
                            <br />
                            <Typography align="justify">
                                Al autorizar correctamente, la solicitud volverá a la bandeja <strong>"En Proceso"</strong>, para la activación correspondiente.
                            </Typography>
                        </DialogContent>
                        <DialogActions>
                            <Grid container >
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            disabled={authorizeButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickAuthorizeCreditCardActivationToggleModal}
                                            margin="normal">
                                            Cancelar
                                            <CancelIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={authorizeButtonDisabled}
                                            className={classNames(classes.button)}
                                            type="submit" 
                                            margin="normal"
                                            color="primary" 
                                            size="small"
                                            fullWidth>
                                            Autorizar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                               authorizeButtonLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  
            </React.Fragment>
        )        
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(AuthorizeActionButton)));
