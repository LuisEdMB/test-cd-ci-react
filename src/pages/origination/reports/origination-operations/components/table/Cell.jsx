import React from "react";
import { Table } from '@devexpress/dx-react-grid-material-ui';
import NewCommentForm from '../../../../../../components/NewCommentForm/NewCommentForm';


const  Cell = ({...props}) => {
    // if(props.column.name === "fec_reg"){
    //     const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
    //     return <Table.Cell {...props} value={value} />
    // }
    // else if(props.column.name === "fec_reg_emboce"){
    //     const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
    //     return <Table.Cell {...props} value={value} />
    // }
    if(props.column.name === "observation"){
        return <Table.Cell {...props}>
           <NewCommentForm {...props}/>
        </Table.Cell>
    }
    return <Table.Cell {...props} />
}

export default Cell;
