import React, { Component } from 'react'
import * as moment from "moment";
import { withSnackbar } from 'notistack'
import { bindActionCreators } from "redux";

// React Router
import { connect } from "react-redux";

import { IconButton, Grid, LinearProgress, withStyles, Divider } from "@material-ui/core";

// Components Custom
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
import HeaderReport from '../../../../components/header-report/HeaderReport';
import FormPanel from './components/FormPanel';
import CloseIcon from "@material-ui/icons/Close";
import PieChartCount from "./components/PieChartCount";

// Actions
import { getReportDailyDocument } from '../../../../actions/odc-report/odc-report';

const styles = theme => ({
    avatar: {
        width: 80,
        height: 48
    },
    modalRoot: {

    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        odcReport: state.odcReportReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getReportDailyDocument: bindActionCreators(getReportDailyDocument, dispatch),
    };
    return actions;
}

const columns = [
    { name: 'fec_ori_final', title: 'Fecha' },
    { name: 'cantidad_ori_titulares_tc', title: 'Originaciones TC' },
    { name: 'cantidad_ori_titulares_tc_mas_sae', title: 'Originaciones EC' },
    { name: 'cantidad_req_reimpresiones', title: 'Reimpresiones' },
    { name: 'cantidad_ori_adicionales', title: 'Adicionales' },
]

class DigitalDocumentDaily extends Component {

    state = {
        defaultColumnWidths: [
            { columnName: 'fec_ori_final', width: 150 },
            { columnName: 'cantidad_ori_titulares_tc', width: 150 },
            { columnName: 'cantidad_ori_titulares_tc_mas_sae', width: 180 },
            { columnName: 'cantidad_ori_adicionales', width: 150 },
            { columnName: 'cantidad_req_reimpresiones', width: 150 },
        ],
        tableColumnExtensions: [
            { columnName: 'authorize', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        dataTotal: '',
        rowsExcel: [],
        additional: 0,
        reprint: 0,
        express: 0,
        pageSizes: [],
        columnExcel: [],
        withData: false,
        dateDiffDays: 0

    }

    handleSubmitFilter = data => {
        this.setState({ withData: false }, () => {
            if (data) {
                let sendData = {
                    originacionCuadreDocDigitales: {
                        ...data
                    }
                }
                this.props.getReportDailyDocument(sendData)
                    .then(response => {
                        if (response.data.success) {
                            const { tarjetasOriginadasBiometria } = response.data
                            const {
                                cuadreDocumentosBiometria,
                                cuadreDocumentosBiometriaSuma,
                                cuadreDocumentosBiometriaExcel
                            } = tarjetasOriginadasBiometria
                            let rows = cuadreDocumentosBiometria
                            let dataTotal = {
                                fec_ori_final: 'TOTAL',
                                "Fecha": 'TOTAL',
                                cantidad_ori_titulares_tc: cuadreDocumentosBiometriaSuma.cantidad_ori_titulares_tc_suma,
                                cantidad_ori_titulares_tc_mas_sae: cuadreDocumentosBiometriaSuma.cantidad_ori_titulares_tc_mas_sae_suma,
                                cantidad_ori_adicionales: cuadreDocumentosBiometriaSuma.cantidad_ori_adicionales_suma,
                                cantidad_req_reimpresiones: cuadreDocumentosBiometriaSuma.cantidad_req_reimpresiones_suma,
                                "Originaciones TC": cuadreDocumentosBiometriaSuma.cantidad_ori_titulares_tc_suma,
                                "Originaciones EC": cuadreDocumentosBiometriaSuma.cantidad_ori_titulares_tc_mas_sae_suma,
                                "Adicionales": cuadreDocumentosBiometriaSuma.cantidad_ori_adicionales_suma,
                                "Reimpresiones": cuadreDocumentosBiometriaSuma.cantidad_req_reimpresiones_suma,
                            }

                            rows.push(dataTotal)
                            let columnExcel = Object.keys(cuadreDocumentosBiometriaExcel.length > 0 && cuadreDocumentosBiometriaExcel[0])

                            columnExcel = columnExcel.map((item, index) => {
                                if (item === "Originaciones TC+SAE") {
                                    item = "Originaciones EC"
                                }
                                return { value: item, label: item }
                            })
                            let rowsExcel = cuadreDocumentosBiometriaExcel
                            rowsExcel.push(dataTotal)
                            rowsExcel = rowsExcel.map((e, i) => {
                                let json = { ...e }
                                for (var v in json) {
                                    let newKey = v
                                    if (newKey === "Originaciones TC+SAE") {
                                        newKey = "Originaciones EC"
                                    }
                                    json[newKey] = (json[v] || '').toString()
                                }
                                return json
                            })
                            this.setState({
                                rows: rows,
                                dataTotal: dataTotal,
                                rowsExcel: rowsExcel,
                                columnExcel: columnExcel,
                                withData: true
                            }, () => {
                                this.getNotistack('Búsqueda exitosa', 'success')

                            })
                        } else {
                            this.getNotistack('Vuelva a realizar la búsqueda', 'warning')
                        }
                    })
                    .catch(error => {
                        this.getNotistack(error.data.errorMessage ? error.data.errorMessage : 'error', 'error')
                    });
            }
        })

    }

    getNotistack(message, variant = "default", duration = 6000) {
        let select = "default";
        switch (variant) {
            case "error":
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default:
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit" />
                </IconButton>
            ),
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

    }

    render() {
        const {
            defaultColumnWidths,
            defaultHiddenColumnNames,
            rows,
            dataTotal,
            rowsExcel,
            columnExcel,
            tableColumnExtensions,
            withData
        } = this.state;

        return (
            <Grid container className="p-1" style={{ marginBottom: '2em' }}>
                <Grid item xs={12}>
                    <div style={{ height: 12 }}>
                        {
                            this.props.odcReport.digitalDocumentDayli.loading ? <LinearProgress /> : null
                        }
                    </div>
                </Grid>
                <Grid item xs={12}>
                    <HeaderReport
                        title={"Reporte de Cuadre Diario - Documentos Digitales"}
                        columnExcel={columnExcel}
                        dataExcel={rowsExcel}
                        nameSheet={"Reporte de Solicitud de Tarj."}
                        nameFile={`Reporte_Cuadre_Digitales_${moment().format('DDMMYYYY')}`}
                    />
                </Grid>
                <Grid item xs={12} className="mb-2">
                    <FormPanel
                        title={"Filtros de búsqueda"}
                        handleSubmitFilter={this.handleSubmitFilter}
                    />
                </Grid>
                <Grid item xs={12}>
                    <Divider className="mb-2" />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={8} >

                    <Grid item xs={12} style={{ maxWidth: '810px', margin: '0 auto' }}>
                        {
                            withData &&
                            <DevGridComponent
                                rows={rows}
                                columns={columns}
                                tableColumnExtensions={tableColumnExtensions}
                                defaultColumnWidths={defaultColumnWidths}
                                defaultHiddenColumnNames={defaultHiddenColumnNames}
                                RowDetailComponent={RowDetail}
                                CellComponent={Cell}
                                pagination={false}
                            />
                        }
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={4} className="mb-2">
                    {
                        withData && <PieChartCount data={dataTotal} />
                    }
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(DigitalDocumentDaily)));