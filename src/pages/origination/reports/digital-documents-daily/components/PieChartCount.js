import React, { Component } from 'react';
import { Paper, Typography } from '@material-ui/core';
import {
    Chart,
    PieSeries,
} from '@devexpress/dx-react-chart-material-ui';
import { Animation } from '@devexpress/dx-react-chart';
import { withStyles } from '@material-ui/core/styles';

const getData = data => {
    if (!data) return []

    return [
        { title: 'Originaciones TC', count: data.cantidad_ori_titulares_tc },
        { title: 'Originaciones EC', count: data.cantidad_ori_titulares_tc_mas_sae },
        { title: 'Adicionales', count: data.cantidad_ori_adicionales },
        { title: 'Reimpresiones', count: data.cantidad_req_reimpresiones },
    ]
}

const sum = data => data.cantidad_ori_titulares_tc + data.cantidad_ori_titulares_tc_mas_sae + data.cantidad_ori_adicionales + data.cantidad_req_reimpresiones

const PieWithLabel = withStyles({
    label: {
        fill: '#ffffff',
        fontSize: '10px',
        zIndex: 10,
        //animation: `${getLabelAnimationName()} 1s`,
    },
})(({ classes, value, ...restProps }) => (
    <>
        <PieSeries.Point {...restProps} />
        <Chart.Label
            x={restProps.arg - restProps.maxRadius * (Math.cos((restProps.startAngle + restProps.endAngle + Math.PI) / 2))}
            y={restProps.val - restProps.maxRadius * (Math.sin((restProps.startAngle + restProps.endAngle + Math.PI) / 2))}
            dominantBaseline="middle"
            textAnchor="middle"
            className={classes.value}
            style={{ color: 'darkgray', fontWeight: '600', fontSize: '12px', zIndex: 10 }}
        >
            {!Number.isNaN(value) && value > 0 ? `${restProps.argument}(${value})` : ''}
        </Chart.Label>
    </>
));

export default class PieChartCount extends Component {


    render() {
        const { data } = this.props;
        const dataChart = getData(data)
        return (
            <Paper style={{ backgroundColor: 'inherit', padding: '1em' }}>
                <Chart
                    data={dataChart}
                    width={300}
                    height={300}
                >
                    <PieSeries
                        valueField="count"
                        argumentField="title"
                        pointComponent={PieWithLabel}
                    />
                    <Animation />
                </Chart>
                <Typography style={{ fontWeight: '750' }}>Total: {sum(data)}</Typography>
            </Paper>
        );
    }
}
