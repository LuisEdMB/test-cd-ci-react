import React from "react";
import { withStyles, Typography } from "@material-ui/core";

const styles = theme => ({
    fontSizeDefault: {
        fontSize:12
    },
});

const RowDetail = ({ row, classes }) => (
    <ul>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.fec_ori_final }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Documentos Titulares:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.cantidad_ori_titulares_tc }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Documentos Titulares SAE:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.cantidad_ori_titulares_tc_mas_sae }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Documentos Adicionales:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.cantidad_ori_adicionales }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Documentos Reimpresiones:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.cantidad_req_reimpresiones }
            </Typography>
        </li>
    </ul>
);

export default withStyles(styles)(RowDetail);

