import React from "react";
import * as moment from 'moment';
import { Table } from '@devexpress/dx-react-grid-material-ui';

/*const MyField = (props) => (
    <span style={{ padding: '.2em .5em'}}>
        { props.value ? 'Contingencia' : '7' }
    </span>
)*/

const  Cell = ({...props}) => {
    if(props.column.name === "fec_reg"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    else if(props.column.name === "fec_reg_emboce"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    else if(props.column.name === 'tdc_sae') {
        //return <Table.Cell {...props} value={<MyField /*value={props.value}*/ />} />
    }

    return <Table.Cell {...props} />
}

export default Cell;
