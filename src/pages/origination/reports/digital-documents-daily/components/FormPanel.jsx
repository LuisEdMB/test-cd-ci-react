import React, { Component } from "react";
import * as moment from 'moment';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

//import Autocomplete from '../../../../../components/Autocomplete';
import {
    Grid,
    Button,
    TextField,
    InputAdornment,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Typography,
    //FormHelperText,
    withStyles, IconButton
} from '@material-ui/core';

import { getIdentificationDocumentType } from '../../../../../actions/value-list/identification-document-type';
import { getFlowPhaseState } from '../../../../../actions/generic/flow-phase-state'; 
import { getProduct } from '../../../../../actions/value-list/product';
import { getAgency } from '../../../../../actions/generic/agency';
import { getProcess, getSubProcess, getProcessFlowPhaseState } from '../../../../../actions/generic/process-subProcess';
// Icons
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FilterListIcon from '@material-ui/icons/FilterList';
import SearchIcon from '@material-ui/icons/Search';
// Utils
import { defaultMinDate } from '../../../../../utils/Utils';
import { Bounce } from 'react-reveal';
import CloseIcon from "@material-ui/icons/Close";

const mapStateToProps = (state, props) => {
    return {
        idenfiticationDocumentType: state.identificationDocumentTypeReducer,
        flowPhaseState: state.flowPhaseStateReducer,
        product: state.productReducer,
        agency: state.agencyReducer,
        processSubProcess: state.processSubProcessReducer,
        odcReport: state.odcReportReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getIdentificationDocumentType: bindActionCreators(getIdentificationDocumentType, dispatch),
        getFlowPhaseState:  bindActionCreators(getFlowPhaseState, dispatch),
        getProduct: bindActionCreators(getProduct, dispatch),
        getAgency: bindActionCreators(getAgency, dispatch),
        getProcess: bindActionCreators(getProcess, dispatch),
        getSubProcess: bindActionCreators(getSubProcess, dispatch),
        getProcessFlowPhaseState: bindActionCreators(getProcessFlowPhaseState, dispatch)
    };
  return actions;
}

const styles = theme => ({
    root:{
        width: "100%",
        //borderRadius: "0 0 .5em .5em",
        boxShadow: 'none',
        borderBottom: "1px solid #80808033",
    },
    paper:{
        padding: 20
    },
    buttonWrapper:{
        display: "center",
        alignItems: "center"
    },
    heading:{
        display: "flex",
        alignItem: "center",
        [theme.breakpoints.down('sm')]: {
            fontSize:14,
        },
        [theme.breakpoints.up('md')]: {
            fontSize:16,
        }
    },
    button: {
        textTransform: 'none',
    },
});

class FormPanel extends Component {
    state = {
        initialDate : {
            error: false,
            value: moment().format('YYYY-MM-DD')
        },
        finalDate : {
            error:false,
            value: moment().format('YYYY-MM-DD')
        }
    }

    handleSubmitSearch = (e) => {
        e.preventDefault()
        const {initialDate, finalDate} = this.state
        if(initialDate.error)
            this.getNotistack('Verifique la Fecha Activación Inicial', 'warning')
        else if(finalDate.error)
            this.getNotistack('Verifique la Fecha Activación Final', 'warning')
        else{
            let sendData = {
                fec_inicial: initialDate.value,
                fec_final: finalDate.value
            }
            this.props.handleSubmitFilter(sendData)
        }
    }

    handleOnChange = variable => e => {
        e.persist()
        let { value } = e.target

        this.setState({
            [variable] : {
                value: value,
                error: value === ""
            }
        })
    }

    handleOnBlurInitialDate = e => {
        e.persist()
        //let {value} = e.target
        //let error = value.
    }

    handleOnBlurFinalDate = e => {
        e.persist()

    }

    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error":
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default:
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }

    render = () => {
        const {
            classes,
            title
        } = this.props;

        const {
            initialDate,
            finalDate
        } = this.state
        return(
            <div>
                <ExpansionPanel className={classes.root}  defaultExpanded>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel-content"
                        id="panel-header"
                    >
                        <Typography variant="h6" className={classes.heading}>
                            <FilterListIcon className="mr-2" />
                            {title}
                        </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div className="w-100">
                            <form method="post" autoComplete="off" onSubmit={this.handleSubmitSearch}>
                                <Grid container spacing={8} className="mb-2">
                                    {/* Initial Filter Date */}
                                    <Grid item xs={12} sm={6} md={4} lg={2}>
                                        <TextField
                                            error={initialDate.error}
                                            fullWidth={true}
                                            name="initialDate"
                                            label="Fecha Activación Inicial"
                                            type="date"
                                            margin="normal"
                                            format="DD/MM/YYYY"
                                            defaultValue={initialDate.value}
                                            onChange={this.handleOnChange("initialDate")}
                                            onBlur={this.handleOnBlurInitialDate}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            InputProps={{
                                                inputProps:{
                                                    min: defaultMinDate,
                                                    max: finalDate.value
                                                },
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <CalendarTodayIcon
                                                            color={ initialDate.error ? "secondary" : "inherit"}
                                                            fontSize="small"
                                                        />
                                                    </InputAdornment>
                                                )
                                            }}
                                        />
                                    </Grid>
                                    {/* Final Filter Date */}
                                    <Grid item xs={12} sm={6} md={4} lg={2}>
                                        <TextField
                                            error={finalDate.error}
                                            fullWidth={true}
                                            name="finalDate"
                                            label="Fecha Activación Final"
                                            type="date"
                                            margin="normal"
                                            format="DD/MM/YYYY"
                                            defaultValue={finalDate.value}
                                            onChange={this.handleOnChange("finalDate")}
                                            onBlur={this.handleOnBlurFinalDate}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            InputProps={{
                                                inputProps:{
                                                    min: initialDate.value,
                                                    max: moment().format('YYYY-MM-DD')
                                                },
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <CalendarTodayIcon
                                                            color={ finalDate.error ? "secondary" : "inherit"}
                                                            fontSize="small"
                                                        />
                                                    </InputAdornment>
                                                )
                                            }}
                                        />
                                    </Grid>
                                    {/* Boton consultar */}
                                    <Grid item xs={12} sm={6} md={4} lg={2} className="d-flex justify-content-center align-items-center">
                                        <div className="w-100">
                                            <Bounce>
                                                <div>
                                                    <Button 
                                                        className={classes.button} fullWidth variant="contained" type="submit"  color="primary"
                                                        disabled={this.props.odcReport.digitalDocumentDayli.loading}>
                                                        Consultar
                                                        <SearchIcon  className="ml-2" size="small" />
                                                    </Button>
                                                </div>
                                            </Bounce>
                                        </div>
                                    </Grid>
                                </Grid>
                        </form>
                        </div>

                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        )
    }
}


/*
FormPanel.defaultProps = {
    form:{
        agencyId:0,
        initialDateError:false,
        finalDateError:false,
        initialDate:"",
        finalDate:""
    }
};
*/

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(FormPanel)));
