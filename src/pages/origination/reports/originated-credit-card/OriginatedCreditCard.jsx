import React, { Component } from "react";
import * as moment from 'moment';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
import { bindActionCreators } from "redux";
// React Router
import { connect } from 'react-redux';
// Component
import {
    IconButton,
    Grid,
    LinearProgress,
    withStyles,
    Divider
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close';
// Components Custom
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
import HeaderReport from '../../../../components/header-report/HeaderReport';
import FormPanel from './components/FormPanel';

// Actions
import { getReportOriginatedCards } from '../../../../actions/odc-report/odc-report';
// Utils
import { defaultPageSizes } from '../../../../utils/Utils';

const styles = theme => ({
    avatar: {
        width: 80,
        height: 48
    },
    modalRoot: {

    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        odcReport: state.odcReportReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getReportOriginatedCards: bindActionCreators(getReportOriginatedCards, dispatch),
    };
    return actions;
}

class OriginatedCreditCard extends Component {

    state = {
        columnExcel: [],
        defaultColumnWidths: [
            { columnName: 'cod_solicitud_completa', width: 125 },
            { columnName: 'des_tipo_documento', width: 100 },
            { columnName: 'des_nro_documento', width: 100 },
            { columnName: 'des_nombre_completo', width: 300 },
            { columnName: 'des_proceso', width: 100 },
            { columnName: 'des_tipo_relacion', width: 150 },
            { columnName: 'des_subproceso', width: 100 },
            { columnName: 'des_jerarquias_flujo_fase_estado', width: 300 },
            { columnName: 'des_observacion', width: 350 },
            { columnName: 'num_tarjeta_pmp', width: 180 },
            { columnName: 'num_cuenta_pmp', width: 180 },
            { columnName: 'num_cuenta_paralela', width: 180 },
            { columnName: 'des_nombre_producto', width: 200 },
            { columnName: 'des_resp_promotor_nombre_completo', width: 200 },
            { columnName: 'des_telef_celular', width: 130 },
            { columnName: 'des_telef_fijo', width: 130 },
            { columnName: 'des_usu_reg', width: 130 },
            { columnName: 'fec_reg', width: 140 },
            { columnName: 'des_nombre_flujo_fase_estado_sae', width: 140 },
            { columnName: 'observation', width: 150 },
        ],
        columns: [],
        tableColumnExtensions: [
            { columnName: 'authorize', align: 'center' },
            { columnName: 'observation', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        rowsExcel: [],
        additional: 0,
        reprint: 0,
        express: 0,
        pageSizes: defaultPageSizes,
        dateDiffDays: 0

    };
    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }
        if (prevProps.odcReport.originatedCards !== this.props.odcReport.originatedCards) {
            if (this.props.odcReport.originatedCards.loading) {
                this.setState(state => ({
                    ...state,
                    rows: [...state.rows],
                    rowsExcel: [...state.rowsExcel]
                }));
            }

            if (!this.props.odcReport.originatedCards.loading &&
                this.props.odcReport.originatedCards.response &&
                this.props.odcReport.originatedCards.success) {

                let { data } = this.props.odcReport.originatedCards;
                let { tarjetasOriginadas, tarjetasOriginadasExcel } = data;

                if (tarjetasOriginadas && tarjetasOriginadasExcel) {

                    let columnExcel = Object.keys(tarjetasOriginadasExcel.length > 0 && tarjetasOriginadasExcel[0]);
                    // columnExcel = columnExcel.map((item, index) => (
                    //     { value: item, label: item }
                    // ));
                    columnExcel = columnExcel.filter((item) => item !== "Nro. Cta. LP").map((item) => {
                        if (item === "Estado SAE") {
                            item = "Estado EC"
                        }
                        return { value: item, label: item }
                    })

                    const rowsExcel = tarjetasOriginadasExcel.map((e, i) => {
                        let json = { ...e }
                        for (var v in json) {
                            let newKey = v
                            if (newKey === "Estado SAE") {
                                newKey = "Estado EC"
                            }
                            json[newKey] = (json[v] || '').toString()
                        }
                        return json
                    })

                    this.setState(state => ({
                        ...state,
                        rows: tarjetasOriginadas,
                        columnExcel: columnExcel,
                        rowsExcel: rowsExcel
                    }));
                }

            }
            else if (!this.props.odcReport.originatedCards.loading &&
                this.props.odcReport.originatedCards.response &&
                !this.props.odcReport.originatedCards.success) {
                // Notistack
                this.getNotistack(this.props.odcReport.originatedCards.error, "error");
                this.setState(state => ({ ...state, rows: [], rowsExcel: [], columnExcel: [] }));
            }
            else if (!this.props.odcReport.originatedCards.loading &&
                !this.props.odcReport.originatedCards.response &&
                !this.props.odcReport.originatedCards.success) {
                // Notistack
                this.getNotistack(this.props.odcReport.originatedCards.error, "error");
                this.setState(state => ({ ...state, rows: [], rowsExcel: [], columnExcel: [] }));
            }
        }
    }

    componentWillMount = () => {
        const columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns: columns
        }));
    }

    getColumnsRender() {
        let columns = [];
        if (this.props.width === "xs") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
            ]
        }
        else if (this.props.width === "sm") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
            ]
        }
        else if (this.props.width === "md") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Nro Solicitud' },
                { name: 'des_nro_documento', title: 'Nro Documento ' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo - Fase - Estado' },
                { name: 'fec_reg', title: 'Fecha Registro' },
            ]
        }
        else if (this.props.width === "lg") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'des_proceso', title: 'Proceso' },
                { name: 'des_subproceso', title: 'SubProceso' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo-Fase-Estado' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_observacion', title: 'Observaciones' },
                { name: 'num_tarjeta_pmp', title: "Nro. Tarjeta" },
                { name: 'num_cuenta_pmp', title: "Nro. Cta." },
                { name: 'des_nombre_flujo_fase_estado_sae', title: "Estado EC" },
                { name: 'des_nombre_producto', title: "Tipo Tarjeta" },
                { name: 'des_usu_reg', title: "Usuario Creador" },
                { name: 'fec_reg', title: "Fecha y Hora del Registro" },
                { name: 'des_resp_promotor_nombre_completo', title: "Promotor" },
                { name: 'des_telef_celular', title: "Nro. Celular" },
                { name: 'des_telef_fijo', title: "Nro. Tel. Casa" },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        else if (this.props.width === "xl") {
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Doc.' },
                { name: 'des_nro_documento', title: 'Nro. Doc.' },
                { name: 'des_nombre_completo', title: 'Apellidos y Nombres' },
                { name: 'des_proceso', title: 'Proceso' },
                { name: 'des_subproceso', title: 'SubProceso' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo-Fase-Estado' },
                { name: 'des_tipo_relacion', title: 'Relación' },
                { name: 'des_observacion', title: 'Observaciones' },
                { name: 'num_tarjeta_pmp', title: "Nro. Tarjeta" },
                { name: 'num_cuenta_pmp', title: "Nro. Cta." },
                { name: 'des_nombre_flujo_fase_estado_sae', title: "Estado EC" },
                { name: 'des_nombre_producto', title: "Tipo Tarjeta" },
                { name: 'des_usu_reg', title: "Usuario Creador" },
                { name: 'fec_reg', title: "Fecha y Hora del Registro" },
                { name: 'des_resp_promotor_nombre_completo', title: "Promotor" },
                { name: 'des_telef_celular', title: "Nro. Celular" },
                { name: 'des_telef_fijo', title: "Nro. Tel. Casa" },
                { name: 'observation', title: 'Ver Comentario' },


            ]
        }
        return columns;
    }

    handleSubmitFilter = data => {
        if (data) {
            let sendData = {
                tarjetasOriginadas: {
                    cod_agencia: 0,
                    cod_tipo_documento: data.documentTypeAux,
                    des_nro_documento: data.documentNumber,
                    cod_producto: data.productId ? data.productId : 0,
                    cod_proceso: data.processId ? data.processId : 0,
                    cod_subproceso: data.subProcessId ? data.subProcessId : 0,
                    cod_flujo_fase_estado: data.flowPhaseStateId,
                    des_siglas_flujo_fase_estado: data.flowPhaseStateId,
                    des_usu_reg: data.username,
                    fec_inicial: data.initialRegistrationDate,
                    fec_final: data.finalRegistrationDate,

                }
            }

            this.props.getReportOriginatedCards(sendData);
        }
    }
    handleShowHideSearchInput = e => {
        this.setState(state => ({
            showHideSearchInput: !state.showHideSearchInput
        }));
    }
    handleClickOpenSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: true
        }));
    }
    handleClickCloseSearchModal = e => {
        this.setState(state => ({
            ...state,
            openSearchModal: false
        }));
    }

    // Notistack
    getNotistack(message, variant = "default", duration = 6000) {
        let select = "default";
        switch (variant) {
            case "error":
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default:
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit" />
                </IconButton>
            ),
        });
    }
    render() {
        const { width, odcReport } = this.props;
        const {
            columns,
            defaultColumnWidths,
            defaultHiddenColumnNames,
            rows,
            rowsExcel,
            tableColumnExtensions,
            columnExcel,
        } = this.state;
        return (
            <Grid container className="p-1">
                <Grid item xs={12} style={{ height: 12 }}>
                    {
                        odcReport.originatedCards.loading ? <LinearProgress /> : ""
                    }
                </Grid>
                <Grid item xs={12}>
                    <HeaderReport
                        title={"Reporte de Solicitud de Tarjetas"}
                        columnExcel={columnExcel}
                        dataExcel={rowsExcel}
                        data={rows}
                        nameSheet={"Reporte de Solicitud de Tarj."}
                        nameFile={`Reporte_Solicitudes_Tarjetas_${moment().format('DDMMYYYY')}`}
                    />
                </Grid>
                <Grid item xs={12}>
                    <Divider className="mb-2" />
                </Grid>
                <Grid item xs={12} className="mb-2">
                    <FormPanel title={"Filtros de búsqueda"}
                        handleSubmitFilter={this.handleSubmitFilter}
                    />
                </Grid>

                <Grid item xs={12}>
                    <DevGridComponent
                        rows={rows}
                        columns={columns}
                        width={width}
                        tableColumnExtensions={tableColumnExtensions}
                        defaultColumnWidths={defaultColumnWidths}
                        defaultHiddenColumnNames={defaultHiddenColumnNames}
                        RowDetailComponent={RowDetail}
                        CellComponent={Cell} />
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(OriginatedCreditCard))));
