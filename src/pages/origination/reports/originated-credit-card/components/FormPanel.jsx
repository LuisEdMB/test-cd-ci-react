import React, { Component } from "react";
import * as moment from 'moment';
import { withSnackbar } from 'notistack';
// React Router
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Autocomplete from '../../../../../components/Autocomplete';
import {
    Grid,
    Button,
    TextField,
    InputAdornment,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Typography,
    FormHelperText,
    withStyles
} from '@material-ui/core';
import { getIdentificationDocumentType } from '../../../../../actions/value-list/identification-document-type';
import { getFlowPhaseState } from '../../../../../actions/generic/flow-phase-state';
import { getProduct } from '../../../../../actions/value-list/product';
import { getProcess, getSubProcess, getProcessFlowPhaseState } from '../../../../../actions/generic/process-subProcess';
// Icons
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FilterListIcon from '@material-ui/icons/FilterList';
import SearchIcon from '@material-ui/icons/Search';
// Utils
import { getNotistack } from '../../../../../utils/Notistack';
import { defaultMinDate, validateLengthDocumentType, checkInputKeyCode, onlyNumberKeyCode } from '../../../../../utils/Utils';
import { Bounce } from 'react-reveal';

const styles = theme => ({
    root: {
        width: "100%",
        //borderRadius: "0 0 .5em .5em",
        boxShadow: 'none',
        borderBottom: "1px solid #80808033",
    },
    paper: {
        padding: 20
    },
    buttonWrapper: {
        display: "center",
        alignItems: "center"
    },
    heading: {
        display: "flex",
        alignItem: "center",
        [theme.breakpoints.down('sm')]: {
            fontSize: 14,
        },
        [theme.breakpoints.up('md')]: {
            fontSize: 16,
        }
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        idenfiticationDocumentType: state.identificationDocumentTypeReducer,
        flowPhaseState: state.flowPhaseStateReducer,
        product: state.productReducer,
        processSubProcess: state.processSubProcessReducer,
        odcReport: state.odcReportReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        getIdentificationDocumentType: bindActionCreators(getIdentificationDocumentType, dispatch),
        getFlowPhaseState: bindActionCreators(getFlowPhaseState, dispatch),
        getProduct: bindActionCreators(getProduct, dispatch),
        getProcess: bindActionCreators(getProcess, dispatch),
        getSubProcess: bindActionCreators(getSubProcess, dispatch),
        getProcessFlowPhaseState: bindActionCreators(getProcessFlowPhaseState, dispatch)
    };
    return actions;
}

class FormPanel extends Component {
    state = {
        documentTypeError: false,
        documentTypeId: 0,
        documentType: "",
        documentTypeAux: "",
        documentTypeInternalValue: "",

        documentNumberError: false,
        documentNumber: "",

        productError: false,
        productId: "",
        product: "",

        flowPhaseStateError: false,
        flowPhaseStateId: "",
        flowPhaseState: "",

        registerAgencyError: false,
        registerAgency: "",
        registerAgencyId: "",

        embossingAgencyError: false,
        embossingAgencyId: "",
        embossingAgency: "",

        initialRegistrationDateError: false,
        initialRegistrationDate: "",

        finalRegistrationDateError: false,
        finalRegistrationDate: "",

        usernameError: false,
        username: "",

        processError: false,
        processId: "",
        process: "",

        subProcessDisabled: true,
        subProcessLoading: false,
        subProcessError: false,
        subProcessId: "",
        subProcess: "",

        flowPhaseStateDisabled: true,
        flowPhaseStateLoading: false,
    }

    componentWillMount = () => {
        let initialRegistrationDate = moment().format('YYYY-MM-DD'); //moment().subtract(7, "day").format('YYYY-MM-DD');
        let finalRegistrationDate = moment().format('YYYY-MM-DD');

        this.setState(state => ({
            ...state,
            initialRegistrationDate: initialRegistrationDate,
            finalRegistrationDate: finalRegistrationDate
        }));
    }

    componentDidMount = () => {
        // Get Api's Services
        this.props.getIdentificationDocumentType();
        this.props.getFlowPhaseState("REP001");
        this.props.getProduct(); //tipo tarjeta
        this.props.getProcess(1, 0);
    }

    componentDidUpdate(prevProps, prevState) {
        // Identification Document Type - Before
        if (prevProps.idenfiticationDocumentType !== this.props.idenfiticationDocumentType) {
            // Identification Document Type - Success Service
            if (!this.props.idenfiticationDocumentType.loading &&
                this.props.idenfiticationDocumentType.response &&
                this.props.idenfiticationDocumentType.success) {
                const idenfiticationDocumentType = this.props.idenfiticationDocumentType.data.find(item => item.val_orden === 1);
                this.setState(state => ({
                    ...state,
                    documentTypeId: idenfiticationDocumentType.cod_valor,
                    documentType: idenfiticationDocumentType.des_valor,
                    documentTypeAux: idenfiticationDocumentType.des_auxiliar,
                    documentTypeInternalValue: idenfiticationDocumentType.valor_interno
                }));
            }
            // Identification Document Type - Error Service
            else if (!this.props.idenfiticationDocumentType.loading &&
                this.props.idenfiticationDocumentType.response &&
                !this.props.idenfiticationDocumentType.success) {
                getNotistack(`Tipo Documento: ${this.props.idenfiticationDocumentType.error}`, this.props.enqueueSnackbar, "error");
            }
            // Identification Document Type - Error Service Connectivity
            else if (!this.props.idenfiticationDocumentType.loading &&
                !this.props.idenfiticationDocumentType.response &&
                !this.props.idenfiticationDocumentType.success) {
                getNotistack(`Tipo Documento: ${this.props.idenfiticationDocumentType.error}`, this.props.enqueueSnackbar, "error");
            }
        }

        if (prevProps.getFlowPhaseState !== this.props.getFlowPhaseState) {
            if (!this.props.getFlowPhaseState.loading &&
                this.props.getFlowPhaseState.response &&
                !this.props.getFlowPhaseState.success) {
                getNotistack(`Flujo - Fase - Estado: ${this.props.getFlowPhaseState.error}`, this.props.enqueueSnackbar, "error");
            }
            else if (!this.props.getFlowPhaseState.loading &&
                !this.props.getFlowPhaseState.response &&
                !this.props.getFlowPhaseState.success) {
                getNotistack(`Flujo - Fase - Estado: ${this.props.getFlowPhaseState.error}`, this.props.enqueueSnackbar, "error");
            }
        }

        if (prevProps.product !== this.props.product) {
            // Identification Document Type - Error Service
            if (!this.props.product.loading &&
                this.props.product.response &&
                !this.props.product.success) {
                getNotistack(`Flujo - Fase - Estado: ${this.props.product.error}`, this.props.enqueueSnackbar, "error");
            }
            // Identification Document Type - Error Service Connectivity
            else if (!this.props.product.loading &&
                !this.props.product.response &&
                !this.props.product.success) {
                getNotistack(`Producto: ${this.props.product.error}`, this.props.enqueueSnackbar, "error");
            }
        }
    }

    handleSubmitSearch = e => {
        e.preventDefault();

        const { documentTypeAux,
            documentNumber,
            productId,
            processId,
            subProcessId,
            flowPhaseStateId,
            initialRegistrationDate,
            finalRegistrationDate,
            username
        } = this.state;

        // // Get Values State
        // if(documentNumber.length === 0){
        //     let sendData  = {
        //         documentTypeAux,
        //         documentNumber,
        //         productId,
        //         flowPhaseStateId,
        //         initialRegistrationDate,
        //         finalRegistrationDate,
        //         username
        //     }
        //     this.props.handleSubmitFilter(sendData);
        // }
        // else if(validateLengthDocumentType(documentNumber.length)){
        //     let sendData  = {
        //         documentTypeAux,
        //         documentNumber,
        //         productId,
        //         flowPhaseStateId,
        //         initialRegistrationDate,
        //         finalRegistrationDate,
        //         username
        //     }
        //     this.props.handleSubmitFilter(sendData);
        // }

        let sendData = {
            documentTypeAux,
            documentNumber,
            productId,
            processId,
            subProcessId,
            flowPhaseStateId,
            initialRegistrationDate,
            finalRegistrationDate,
            username
        }
        this.props.handleSubmitFilter(sendData);
    }

    handleChangeProcess = name => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";

        if (e !== null) {
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }

        this.props.getSubProcess(2, idDefault);

        this.setState(state => ({
            ...state,
            [name]: valueDefault,
            [nameId]: idDefault,
            [nameError]: error,
            subProcessDisabled: !idDefault ? true : false,
            subProcessId: "",
            subProcess: "",
            flowPhaseStateDisabled: true,
            flowPhaseStateId: "",
            flowPhaseState: ""
        }));
    }

    handleChangeSubProcess = name => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";
        if (e !== null) {
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }

        this.props.getProcessFlowPhaseState(3, idDefault);

        this.setState(state => ({
            ...state,
            [name]: valueDefault,
            [nameId]: idDefault,
            [nameError]: error,
            flowPhaseStateDisabled: !idDefault ? true : false,
            flowPhaseStateId: "",
            flowPhaseState: ""
        }));
    }

    // Client - Select - Event Change
    handleChangeSelectRequired = (name) => e => {
        let nameError = `${name}Error`;
        let nameId = `${name}Id`;
        let error = true;
        let valueDefault = "";
        let idDefault = "";

        if (e !== null) {
            error = false;
            let { label, value } = e;
            valueDefault = label;
            idDefault = value;
        }

        this.setState(state => ({
            ...state,
            [name]: valueDefault,
            [nameId]: idDefault,
            [nameError]: error,
        }));
    }

    handleChangeTextFieldRequired = name => e => {
        e.persist();
        let { value } = e.target;
        let nameError = `${name}Error`;

        this.setState(state => ({
            ...state,
            [name]: value,
            [nameError]: value !== "" ? false : true
        }));
    }

    // Client - Document Number - TextField Event Change
    handleChangeTextFieldDocumentNumber = name => e => {
        let { value } = e.target;
        let nameError = `${name}Error`;
        if (!isNaN(Number(value))) {
            this.setState(state => ({
                [name]: value,
                [nameError]: !validateLengthDocumentType(value.length)
            }));
        }
    }

    // Only Number
    handleKeyPressTextFieldOnlyNumber = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if (!onlyNumberKeyCode(code)) {
            e.preventDefault();
        }
    }

    // Check Input
    handleKeyPressTextFieldCheckInput = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if (!checkInputKeyCode(code)) {
            e.preventDefault();
        }
    }

    render = () => {
        const { idenfiticationDocumentType,
            //flowPhaseState,
            product,
            classes,
            title,
            form,
            processSubProcess } = this.props;

        const idenfiticationDocumentTypeData = idenfiticationDocumentType.data.map((item, index) => ({
            value: item.cod_valor,
            label: item.des_valor_corto
        }
        ));

        // const flowPhaseStateData =  flowPhaseState.data.map((item, index) => ({
        //      value: item.des_siglas_flujo_fase_estado,
        //      label: item.des_flujo_fase_estado
        // }));

        const productData = product.data.map((item, index) => ({
            value: item.cod_producto,
            label: item.des_nom_prod
        }));

        const processData = processSubProcess.process.data.map((item, index) => ({
            value: item.cod_valor,
            label: item.des_valor
        }));

        const subProcessData = processSubProcess.subProcess.data.map((item, index) => ({
            value: item.cod_valor,
            label: item.des_valor
        }));

        const flowPhaseStateData = processSubProcess.flowPhaseState.data.map((item, index) => ({
            value: item.cod_valor,
            label: item.des_valor
        }));

        return (<div>
            <ExpansionPanel className={classes.root} defaultExpanded>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel-content"
                    id="panel-header"
                >
                    <Typography variant="h6" className={classes.heading}>
                        <FilterListIcon className="mr-2" />
                        {title}
                    </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="w-100">
                        <form method="post" autoComplete="off" onSubmit={this.handleSubmitSearch}>
                            <Grid container spacing={8} className="mb-2">
                                {/* Document Type */}
                                <Grid item xs={12} sm={6} md={4} lg={2}>
                                    <Autocomplete
                                        error={this.state.documentTypeError}
                                        onChange={this.handleChangeSelectRequired("documentType")}
                                        value={this.state.documentTypeId}
                                        data={idenfiticationDocumentTypeData}
                                        placeholder={"Tipo Documento"}
                                    />
                                </Grid>
                                {/* Document Number */}
                                <Grid item xs={12} sm={6} md={4} lg={2}>
                                    <TextField
                                        error={this.state.documentNumberError}
                                        fullWidth={true}
                                        label="Nro Documento"
                                        type="text"
                                        margin="normal"
                                        color="default"
                                        id="documentNumber"
                                        name="documentNumber"
                                        value={this.state.documentNumber}
                                        onKeyPress={this.handleKeyPressTextFieldOnlyNumber("documentNumber")}
                                        onChange={this.handleChangeTextFieldDocumentNumber("documentNumber")}
                                        placeholder="Ingresar nro. documento."
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        InputProps={{
                                            inputProps: {
                                                maxLength: this.state.documentTypeId === 100001 ? 8 : 9,
                                            }
                                        }}
                                    />
                                    <FormHelperText className="text-right">
                                        {this.state.documentNumber.toString().length}/{this.state.documentTypeId === 100001 ? 8 : 9}
                                    </FormHelperText>
                                </Grid>

                                {/* Procesos */}
                                <Grid item xs={12} sm={6} md={4} lg={2}>
                                    <Autocomplete
                                        onChange={this.handleChangeProcess("process")}
                                        value={this.state.processId}
                                        data={processData}
                                        placeholder={"Proceso"}
                                    />
                                </Grid>

                                {/* SubProcesos */}
                                <Grid item xs={12} sm={6} md={4} lg={2}>
                                    <Autocomplete
                                        loading={this.state.subProcessLoading}
                                        disabled={this.state.subProcessDisabled}
                                        onChange={this.handleChangeSubProcess("subProcess")}
                                        value={this.state.subProcessId}
                                        data={subProcessData}
                                        placeholder={"Sub Proceso"}
                                    />
                                </Grid>

                                {/* Flow-Phase-State */}
                                <Grid item xs={12} sm={6} md={8} lg={4}>
                                    <Autocomplete
                                        loading={this.state.flowPhaseStateLoading}
                                        disabled={this.state.flowPhaseStateDisabled}
                                        onChange={this.handleChangeSelectRequired("flowPhaseState")}
                                        value={this.state.flowPhaseStateId}
                                        data={flowPhaseStateData}
                                        placeholder={"Flujo-Fase-Estado"}
                                    />
                                </Grid>

                                {/* Product */}
                                <Grid item xs={12} sm={6} md={4} lg={3}>
                                    <Autocomplete
                                        onChange={this.handleChangeSelectRequired("product")}
                                        value={this.state.productId}
                                        data={productData}
                                        placeholder={"Tipo Tarjeta"}
                                    />
                                </Grid>
                                {/* Flow Phase State */}
                                {/* <Grid item xs={12} sm={6} md={4} lg={5}>
                                            <Autocomplete
                                                onChange={this.handleChangeSelectRequired("flowPhaseState")}
                                                value={this.state.flowPhaseStateId}
                                                data={flowPhaseStateData}
                                                placeholder={"Flujo-Fase-Estado"}
                                            />
                                        </Grid> */}
                                {/* User */}
                                <Grid item xs={12} sm={6} md={4} lg={2}>
                                    <TextField
                                        fullWidth={true}
                                        label="Usuario Creador"
                                        type="text"
                                        margin="normal"
                                        color="default"
                                        name="username"
                                        onKeyPress={this.handleKeyPressTextFieldCheckInput("username")}
                                        onChange={this.handleChangeTextFieldRequired("username")}
                                        value={this.state.username}
                                        placeholder="Ingresar nombre de usuario."
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        InputProps={{
                                            inputProps: {
                                                maxLength: 10
                                            }
                                        }}
                                    />
                                </Grid>
                                {/* Initial Registration Date */}
                                <Grid item xs={6} sm={6} md={4} lg={2}>
                                    <TextField
                                        error={form.finalDateError}
                                        fullWidth={true}
                                        name="finalDate"
                                        label="Fecha Registro Inicial"
                                        type="date"
                                        margin="normal"
                                        format="DD-MM-YYYY"
                                        defaultValue={this.state.initialRegistrationDate}
                                        onChange={this.handleChangeTextFieldRequired("initialRegistrationDate")}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        InputProps={{
                                            inputProps: {
                                                min: defaultMinDate,
                                                max: moment().format('YYYY-MM-DD')
                                            },
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <CalendarTodayIcon color={form.finalDateError ? "secondary" : "inherit"} fontSize="small" />
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                    {
                                        this.state.finalDateError && <FormHelperText className="text-red">
                                            {"Fecha incorrecta, edad mínima 21 años."}
                                        </FormHelperText>
                                    }
                                </Grid>
                                {/* Final Registration Date */}
                                <Grid item xs={6} sm={6} md={4} lg={2}>
                                    <TextField
                                        error={form.initialDateError}
                                        fullWidth={true}
                                        name="initialDate"
                                        label="Fecha Registro Final"
                                        type="date"
                                        margin="normal"
                                        format="DD-MM-YYYY"
                                        defaultValue={this.state.finalRegistrationDate}
                                        onChange={this.handleChangeTextFieldRequired("finalRegistrationDate")}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        InputProps={{
                                            inputProps: {
                                                min: defaultMinDate,
                                                max: moment().format('YYYY-MM-DD')
                                            },
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <CalendarTodayIcon color={form.startDateError ? "secondary" : "inherit"} fontSize="small" />
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                </Grid>

                                <Grid item xs={12} sm={12} md={4} lg={1} className="d-flex justify-content-center align-items-center">
                                    <div className="w-100">
                                        <Bounce>
                                            <div>
                                                <Button
                                                    className={classes.button} fullWidth variant="contained" type="submit" color="primary"
                                                    disabled={this.props.odcReport.originatedCards.loading}>
                                                    Consultar
                                                            <SearchIcon className="ml-2" size="small" />
                                                </Button>
                                            </div>
                                        </Bounce>
                                    </div>
                                </Grid>
                            </Grid>
                        </form>
                    </div>

                </ExpansionPanelDetails>
            </ExpansionPanel>
        </div>
        )
    }
}


FormPanel.defaultProps = {
    form: {
        agencyId: 0,
        initialDateError: false,
        finalDateError: false,
        initialDate: "",
        finalDate: ""
    }
};

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(FormPanel)));
