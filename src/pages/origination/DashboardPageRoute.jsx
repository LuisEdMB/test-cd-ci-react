import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Switch, Redirect } from 'react-router-dom';
import PrivateRoute from '../../utils/PrivateRoute';
import { SnackbarProvider } from 'notistack';
import DashboardPage from './dashboard/DashboardPage';

const mapStateToProps = (state, props) => {
    return {
        session: state.sessionReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {

    };
    return actions;
}

const DashboardPageRoute = (props) => {
    let { match, session } = props;
    return( <Switch>
                <Redirect exact from={`${match.path}`} to={`${match.path}/`}/>
                <PrivateRoute 
                    exact
                    path={`${match.path}`} 
                    component={(props) => <SnackbarProvider max={3} > <DashboardPage {...props}/> </SnackbarProvider>}
                    authenticated={session.authenticated}
                />
        </Switch>
    )
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DashboardPageRoute));
