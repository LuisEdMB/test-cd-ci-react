import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
// React Router 
import { connect } from 'react-redux';
// Component
import{ 
    IconButton,
    Grid, 
    LinearProgress,
    Divider
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close';
//import VisibilityIcon from '@material-ui/icons/Visibility'
// Components Custom 
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
import HeaderReport from '../../../../components/header-report/HeaderReport';
// Actions 
import { inboxSend } from '../../../../actions/odc-digitization/odc-digitization';
import { bindActionCreators } from "redux";
const styles = theme => ({
    avatar:{
        width:80, 
        height:48
    },
    modalRoot:{
       
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        odcDigitization : state.odcDigitizationReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        inboxSend: bindActionCreators(inboxSend, dispatch)
    };
    return actions;
}

class ReceiveLegajoPage extends Component {

    state = {
        defaultColumnWidths: [
            { columnName: 'cod_solicitud_completa', width: 125 },
            { columnName: 'des_tipo_documento', width: 130 },
            { columnName: 'des_nro_documento', width: 125 },
            { columnName: 'des_nombre_completo', width: 150 },
            { columnName: 'des_tipo_operacion', width: 130 },
            { columnName: 'num_tarjeta_pmp', width: 130 },
            { columnName: 'num_cuenta_pmp', width: 130 },
            { columnName: 'num_cuenta_paralela', width: 130 },
            { columnName: 'des_usu_reg', width: 135 },
            { columnName: 'des_agencia', width: 135 },
            { columnName: 'fec_reg', width: 130 },
            { columnName: 'des_usu_reg_emboce', width: 135 },
            { columnName: 'des_agencia_emboce', width: 135 },
            { columnName: 'fec_reg_emboce', width: 130 },
            { columnName: 'des_flujo_fase_estado_dig', width: 130 },
            { columnName: 'des_observacion_legajo', width: 150 },
            { columnName: 'comentario', width: 150 },
            

            { columnName: 'send', width: 130 },
            { columnName: 'observation', width: 130 },
            { columnName: 'finish', width: 130 }
        ],
        columns:[],
        tableColumnExtensions: [
            { columnName: 'des_tipo_documento', align: 'center' },
            { columnName: 'send', align: 'center' },
            { columnName: 'observation', align: 'center' },
            { columnName: 'finish', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        rowsExcel: [],
        additional: 0, 
        reprint: 0, 
        express: 0,
        pageSizes: [5, 10, 15, 20, 0],
        columnExcel: [],
        showHideSearchInput: false, 
    };

    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }


        if (prevProps.odcDigitization.inboxSend !== this.props.odcDigitization.inboxSend) {	
            
            if(this.props.odcDigitization.inboxSend.loading){
                this.setState(state => ({
                    ...state, 
                    rows: [], 
                    columnExcel: [],
                    rowsExcel: []
                }));
            }
            
            if(!this.props.odcDigitization.inboxSend.loading && 
                this.props.odcDigitization.inboxSend.response && 
                this.props.odcDigitization.inboxSend.success){
                
                let { data } = this.props.odcDigitization.inboxSend;  
                let { solicitudesEnviarLegajo, solicitudesEnviarLegajoExcel } = data;

                if(solicitudesEnviarLegajo && solicitudesEnviarLegajoExcel){
                    // Label                
                    let columnExcel = Object.keys(solicitudesEnviarLegajoExcel.length > 0 ? solicitudesEnviarLegajoExcel[0]: []);
                    columnExcel = columnExcel.filter(item => item !== 'Nro. Cuenta LP' ).map((item, index) => {
                        return {value: item, label: item}
                    });

                    let rowsExcel = solicitudesEnviarLegajoExcel.length > 0 ? solicitudesEnviarLegajoExcel : [];

                    this.setState(state => ({
                        ...state, 
                        rows: solicitudesEnviarLegajo? solicitudesEnviarLegajo: [], 
                        additional: this.props.odcDigitization.inboxSend.data.contador_additional, 
                        reprint: this.props.odcDigitization.inboxSend.data.contador_reimpresiones, 
                        express: this.props.odcDigitization.inboxSend.data.contador_express,
                        columnExcel: columnExcel,
                        rowsExcel: rowsExcel 
                    }));
                }
            }
            else if(!this.props.odcDigitization.inboxSend.loading && 
                     this.props.odcDigitization.inboxSend.response && 
                    !this.props.odcDigitization.inboxSend.success){
                // Notistack
                this.getNotistack(this.props.odcDigitization.inboxSend.error, "error");
                this.setState(state => ({  ...state, rows: [],  rowsExcel:[], columnExcel: []}));
            }
            else if(!this.props.odcDigitization.inboxSend.loading && 
                    !this.props.odcDigitization.inboxSend.response && 
                    !this.props.odcDigitization.inboxSend.success){
                // Notistack
                this.getNotistack(this.props.odcDigitization.inboxSend.error, "error");
                this.setState(state => ({  ...state, rows: [],  rowsExcel:[], columnExcel: []}));
            }

        }

        if(prevProps.odcDigitization.sendLegajo !== this.props.odcDigitization.sendLegajo){

            if(!this.props.odcDigitization.sendLegajo.loading && 
                this.props.odcDigitization.sendLegajo.response && 
                this.props.odcDigitization.sendLegajo.success){
                this.props.inboxSend("", "", "");
            }
            else if (!this.props.odcDigitization.sendLegajo.loading &&
                      this.props.odcDigitization.sendLegajo.response &&
                     !this.props.odcDigitization.sendLegajo.success) {
                this.getNotistack(this.props.odcDigitization.sendLegajo.error, "error");
            }
            else if (!this.props.odcDigitization.sendLegajo.loading &&
                     !this.props.odcDigitization.sendLegajo.response &&
                     !this.props.odcDigitization.sendLegajo.success) {
                this.getNotistack(this.props.odcDigitization.sendLegajo.error, "error");
            }
        }

        if(prevProps.odcDigitization.regularizeLegajo !== this.props.odcDigitization.regularizeLegajo){

            if(!this.props.odcDigitization.regularizeLegajo.loading && 
                this.props.odcDigitization.regularizeLegajo.response && 
                this.props.odcDigitization.regularizeLegajo.success){
                this.props.inboxSend("", "", "");
            }
            else if (!this.props.odcDigitization.regularizeLegajo.loading &&
                      this.props.odcDigitization.regularizeLegajo.response &&
                      !this.props.odcDigitization.regularizeLegajo.success) {
                this.getNotistack(this.props.odcDigitization.sendLegajo.error, "error");
            }
            else if (!this.props.odcDigitization.regularizeLegajo.loading &&
                     !this.props.odcDigitization.regularizeLegajo.response &&
                     !this.props.odcDigitization.regularizeLegajo.success) {
                this.getNotistack(this.props.odcDigitization.regularizeLegajo.error, "error");
            }
        }
    }

    componentWillMount(){
        let columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns:columns
        }));
    }
    componentDidMount(){
        this.props.inboxSend("", "", "");
    }
    
    getColumnsRender(){
        let columns = [];
        if(this.props.width === "xs"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },

                { name: 'send', title: 'Enviar Legajo' },
                { name: 'observation', title: 'Ver Observación' },
                //{ name: 'finish', title: 'Cerrado' }
            ];
        }
        else if(this.props.width === "sm"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
               
                { name: 'send', title: 'Enviar' },
                { name: 'observation', title: 'Ver Observación' },
                //{ name: 'finish', title: 'Cerrado' }
            ]
        }
        else if(this.props.width === "md"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                
                { name: 'send', title: 'Enviar' },
                { name: 'observation', title: 'Ver Observación' },
                { name: 'comentario', title: 'Ver Comentario' },
                //{ name: 'finish', title: 'Cerrado' }
            ]
        }
        else if(this.props.width === "lg"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'des_tipo_operacion', title: 'Tipo Operación' },

                { name: 'des_usu_reg', title: 'Usuario Creador' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_usu_reg_emboce', title: 'Usuario Emboce' },
                { name: 'des_agencia_emboce', title: 'Agencia Emboce' },
                { name: 'fec_reg_emboce', title: 'Fecha Emboce' },
                { name: 'des_observacion_legajo', title: "Observación" },
                { name: 'num_tarjeta_pmp', title: "Nro. Tarjeta" },
                { name: 'num_cuenta_pmp', title: "Nro. Cuenta" },
                { name: 'des_flujo_fase_estado_dig', title: 'Estado' },
                
                
                { name: 'send', title: 'Enviar' },
                { name: 'observation', title: 'Observación' },
                { name: 'comentario', title: 'Ver Comentario' },
                //{ name: 'finish', title: 'Cerrado' }
            ]
        }
        else if(this.props.width === "xl"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'des_tipo_operacion', title: 'Tipo Operación' },

                { name: 'des_usu_reg', title: 'Usuario Creador' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_usu_reg_emboce', title: 'Usuario Emboce' },
                { name: 'des_agencia_emboce', title: 'Agencia Emboce' },
                { name: 'fec_reg_emboce', title: 'Fecha Emboce' },
                { name: 'des_observacion_legajo', title: "Observación" },
                { name: 'num_tarjeta_pmp', title: "Nro. Tarjeta" },
                { name: 'num_cuenta_pmp', title: "Nro. Cuenta" },
                { name: 'des_flujo_fase_estado_dig', title: 'Estado' },
                
               
                { name: 'send', title: 'Enviar Legajo' },
                { name: 'observation', title: 'Ver Observación' },
                { name: 'comentario', title: 'Ver Comentario' },
                //{ name: 'finish', title: 'Cerrado' }
            ]
        }
        return columns;
    }
   
    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render() {
        const { width, odcDigitization } = this.props;
        const { tableColumnExtensions,
                columns,
                defaultColumnWidths, 
                defaultHiddenColumnNames, 
                rows, 
                columnExcel,
                rowsExcel
            } = this.state;
        
        return (
                <Grid container className="p-1">
                    <Grid item xs={12}>
                        {
                            odcDigitization.inboxSend.loading ?  <LinearProgress /> : ""
                        }
                    </Grid>
                    <Grid item xs={12}>
                        
                        {/* <Header 
                            title="Bandeja de Envio de Legajo" 
                            additional={additional}
                            reprint={reprint}
                            express={express}
                            excel={{
                                workBook: [
                                    {   sheet:{
                                            data: rowsExcel,
                                            columns: columnExcel,
                                            nameSheet: "ODC - Solicitudes"
                                        }
                                    }
                                ],
                                nameFile: "Datos exportados"
                            }}
                        /> */}
                        <HeaderReport 
                            title={"Bandeja de Envío de Legajo" }
                            columnExcel={columnExcel} 
                            dataExcel={rowsExcel}
                            data={rows}
                            nameSheet={"Reporte de Envio de Legajo"} 
                            nameFile={"Datos exportados"}
                        
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Divider className="mb-2"/>  
                    </Grid>
                    <Grid item xs={12}>
                        <DevGridComponent 
                            rows={rows}
                            columns={columns}
                            width={width}
                            search={true}
                            columnExtensions={tableColumnExtensions}
                            defaultColumnWidths={defaultColumnWidths}
                            defaultHiddenColumnNames={defaultHiddenColumnNames}
                            RowDetailComponent={RowDetail}
                            CellComponent={Cell}/>
                    </Grid>
                </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(ReceiveLegajoPage))));

