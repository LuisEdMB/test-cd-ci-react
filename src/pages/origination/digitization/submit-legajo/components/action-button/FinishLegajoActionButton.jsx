import React, { Component } from 'react';
import classNames from 'classnames';
// Components 
import { 
    Fab, 
    IconButton,
    Tooltip, 
    Button,
    withStyles, 
    Dialog,
    DialogTitle,
    Typography,
    DialogContent,
    Grid,
    DialogActions,
    CircularProgress
 } from "@material-ui/core";
// Colors
import green from '@material-ui/core/colors/green';
// Icons 
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import SendIcon from '@material-ui/icons/Send';
import CloseIcon from '@material-ui/icons/Close'; 

const styles = theme => ({
    modalRoot:{
        minWidth:280, 
        width:350,
        maxWidth:400,
    },
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: green[700],
        '&:hover': {
            backgroundColor: green[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    buttonProgressWrapper:{
        position: 'relative'
    },
    button:{
        textTransform: 'none',
    }
});

class FinishLegajoActionButton extends Component {
    state = {
        openModal: false,
        loading: false, 
        disabled: false, 
        buttonDisabled: false
    }
    componentWillMount = () => {
        this.setState(state => ({...state, buttonDisabled: this.props.disabled}));
    }
    handleSubmitFinishLegajo = e => {
        e.preventDefault();
    }
    handleClickToggleModal = e => {
        this.setState(state => ({
            openModal: !state.openModal
        }));
    }
    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render = () => {
        const { classes, row } = this.props;
        let { openModal, loading, disabled, buttonDisabled } = this.state;
        return  (
            <React.Fragment>
                <Tooltip title={`Legajo Finalizado: ${row.cod_solicitud_completa}`} placement="left">
                    <div>
                        <Fab className={classes.fab} disabled={buttonDisabled}>
                            <CheckCircleIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>  
                            
                <Dialog
                    onClose={this.handleClickToggleModal}
                    open={openModal}
                    aria-labelledby="form-finish-legajo">
                    <DialogTitle 
                        id="form-finish-legajo" 
                        className="bg-metal-blue">
                        <Typography align="center" component="span" variant="h6" className="text-white text-shadow-black">
                            Proceso de Digitalización                        
                        </Typography>
                    </DialogTitle>
                
                    <form  onSubmit={this.handleSubmitFinishLegajo} className={classes.modalRoot}>
                        <DialogContent>
                            <Typography align="center">
                                ¿Desea finalizar el <strong>Proceso de Digitalización</strong> para la solicitud:
                                <strong> {row.Solicitud}</strong>?
                            </Typography>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            disabled={disabled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickToggleModal}
                                            margin="normal">
                                            Cancelar
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={disabled}
                                            className={classNames(classes.button)}
                                            type="submit" 
                                            margin="normal"
                                            color="primary" 
                                            size="small"
                                            fullWidth>
                                            Confirmar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                               loading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  
            </React.Fragment>
        )        
    }
}

export default withStyles(styles)(FinishLegajoActionButton);