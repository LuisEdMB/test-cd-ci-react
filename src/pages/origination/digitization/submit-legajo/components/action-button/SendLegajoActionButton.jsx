import React, { Component } from "react";
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components 
import { 
    AppBar,
    Fab, 
    IconButton,
    Tooltip, 
    Button,
    withStyles, 
    Dialog,
    DialogTitle,
    Typography,
    DialogContent,
    Grid,
    DialogActions,
    CircularProgress
 } from "@material-ui/core";
// Colors
import { green, blue } from "@material-ui/core/colors";
// Icons 
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight'
import SendIcon from '@material-ui/icons/Send';
import CloseIcon from '@material-ui/icons/Close'; 
// Actions 
import { sendLegajo } from '../../../../../../actions/odc-digitization/odc-digitization';

const styles = theme => ({
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: blue[700],
        '&:hover': {
            backgroundColor: blue[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    buttonProgressWrapper:{
        position: 'relative'
    },
    button:{
        textTransform: 'none',
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    appBar: {
        position: 'relative',
    }
});

const mapStateToProps = (state, props) => {
    return {
        odcDigitization: state.odcDigitizationReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        sendLegajo: bindActionCreators(sendLegajo, dispatch),
    };
  return actions;
}

class SendLegajoActionButton extends Component {
    state = {
        openModal: false,
        loading: false, 
        disabled: false,
        buttonDisabled: false
    }
    componentWillMount = () => {
        this.setState(state => ({buttonDisabled: this.props.disabled}));
    }
    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.odcDigitization.sendLegajo !== this.props.odcDigitization.sendLegajo) {	
            if(this.props.odcDigitization.sendLegajo.loading){
                this.setState(state => ({
                    ...state,
                    loading: true, 
                    disabled: true
                }));
            }
            
            if(!this.props.odcDigitization.sendLegajo.loading && 
                this.props.odcDigitization.sendLegajo.response && 
                this.props.odcDigitization.sendLegajo.success){
                this.setState(state => ({
                    ...state,
                    openModal: false,
                    loading: false, 
                    disabled: false
                }));
            }
            else if(!this.props.odcDigitization.sendLegajo.loading && 
                this.props.odcDigitization.sendLegajo.response && 
               !this.props.odcDigitization.sendLegajo.success){
                // Notistack
                this.setState(state => ({
                    ...state,
                    loading: false, 
                    disabled: false
                }));
            }
            else if(!this.props.odcDigitization.sendLegajo.loading && 
                    !this.props.odcDigitization.sendLegajo.response && 
                    !this.props.odcDigitization.sendLegajo.success){
                // Notistack
                this.setState(state => ({
                    ...state,
                    loading: false, 
                    disabled: false
                }));
            }
        }
    }
    handleSubmitSendLegajo = row => e => {
        e.preventDefault();
        let sendData = {
            tipo_doc: row.cod_tipo_documento? row.cod_tipo_documento: 1,
            nro_doc: row.des_nro_documento? row.des_nro_documento: 1,
            tipo_doc_letra: row.des_tipo_documento? row.des_tipo_documento: "DNI",
            cod_solicitud: row.cod_solicitud? row.cod_solicitud: 0,
            cod_solicitud_completa: row.cod_solicitud_completa? row.cod_solicitud_completa: "",
            cod_solicitud_digitalizacion: 0,
            digitalizacionActividad: {
                nom_actividad: "Legajo Enviado",
                cod_flujo_fase_estado_actual: 50002,
                cod_flujo_fase_estado_anterior: 50001,
                des_observacion_legajo: null,
            }
        }
        this.props.sendLegajo(sendData)
        .then(response => {
            if(response){
                if(response.data){
                    if(response.data.success){
                        this.getNotistack(`Digitalización: Solicitud ${row.cod_solicitud_completa} - Legajo Enviado.`, "success");
                    }
                }
            }
        });
    }
    handleClickToggleModal = e => {
        this.setState(state => ({
            openModal: !state.openModal
        }));
    }
    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render = () => {
        const { classes, row } = this.props;
        let { openModal, loading, disabled, buttonDisabled } = this.state;
        return  (
            <React.Fragment>
                <Tooltip title={`Enviar Legajo: ${row.cod_solicitud_completa}`} placement="left">
                    <div>
                        <Fab className={classes.fab} onClick={this.handleClickToggleModal} disabled={buttonDisabled}>
                            <SubdirectoryArrowRightIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>  
                            
                <Dialog
                    onClose={this.handleClickToggleModal}
                    open={openModal}
                    maxWidth='xs'
                    aria-labelledby="form-send-legajo">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-send-legajo" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                            Envío de Legajo
                            </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form  onSubmit={this.handleSubmitSendLegajo(row)}>
                        <DialogContent>
                            <Typography align="justify">
                                Si ha realizado el envío del legajo del cliente al proveedor, 
                                por favor Click el botón <strong> "Confirmar" </strong>
                            </Typography>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            disabled={disabled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickToggleModal}
                                            margin="normal">
                                            Cancelar
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={disabled}
                                            className={classNames(classes.button)}
                                            type="submit" 
                                            margin="normal"
                                            color="primary" 
                                            size="small"
                                            fullWidth>
                                            Confirmar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                               loading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  
            </React.Fragment>
        )        
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(SendLegajoActionButton)));
