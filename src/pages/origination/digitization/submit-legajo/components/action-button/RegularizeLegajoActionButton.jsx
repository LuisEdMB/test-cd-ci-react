import React, { Component } from "react";
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// Components 
import { 
    AppBar,
    Fab, 
    IconButton,
    Tooltip, 
    Button,
    withStyles, 
    Dialog,
    DialogTitle,
    Typography,
    DialogContent,
    Grid,
    DialogActions,
    CircularProgress,
    TextField
 } from "@material-ui/core";
// Colors
import { orange, green } from '@material-ui/core/colors';
// Icons 
import VisibilityIcon from '@material-ui/icons/Visibility'
import SendIcon from '@material-ui/icons/Send';
import CloseIcon from '@material-ui/icons/Close'; 
// Utils
import { onlyTextKeyCode } from '../../../../../../utils/Utils';
// Actions 
import { regularizeLegajo, sendLegajo } from '../../../../../../actions/odc-digitization/odc-digitization';

const styles = theme => ({
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: orange[700],
        '&:hover': {
            backgroundColor: orange[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    buttonProgressWrapper:{
        position: 'relative'
    },
    button:{
        textTransform: 'none',
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    appBar: {
        position: 'relative',
    }
});

const mapStateToProps = (state, props) => {
    return {
        odcDigitization: state.odcDigitizationReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        regularizeLegajo: bindActionCreators(regularizeLegajo, dispatch),
        sendLegajo: bindActionCreators(sendLegajo, dispatch),
    };
  return actions;
}
class RegularizeLegajoActionButton extends Component {
    state = {
        openObservationModal: false,
        openRegularizeModal: false,
        observationButtonLoading: false, 
        observationButtonDisabled: false, 
        regularizeButtonLoading: false, 
        regularizeButtonDisabled: false, 
        observation: "",
        buttonDisabled: false
    }
    componentWillMount = () => {
        let { row } = this.props;
        if(row){
            this.setState(state => ({...state, buttonDisabled: this.props.disabled, observation: this.props.row.des_observacion_legajo}));
        }
    }
    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.odcDigitization.regularizeLegajo !== this.props.odcDigitization.regularizeLegajo) {	
            if(this.props.odcDigitization.regularizeLegajo.loading){
                this.setState(state => ({
                    ...state,
                    regularizeButtonLoading: true, 
                    regularizeButtonDisabled: true, 
                }));
            }
            
            if(!this.props.odcDigitization.regularizeLegajo.loading && 
                this.props.odcDigitization.regularizeLegajo.response && 
                this.props.odcDigitization.regularizeLegajo.success){

                this.setState(state => ({
                    ...state,
                    regularizeButtonLoading: false, 
                    regularizeButtonDisabled: false, 
                    openObservationModal: false,
                    openRegularizeModal: false,
                }));
            }
            else if(!this.props.odcDigitization.regularizeLegajo.loading && 
                     this.props.odcDigitization.regularizeLegajo.response && 
                    !this.props.odcDigitization.regularizeLegajo.success){
                // Notistack
                this.setState(state => ({
                    ...state,
                    regularizeButtonLoading: false, 
                    regularizeButtonDisabled: false, 
                }));
            }
            else if(!this.props.odcDigitization.regularizeLegajo.loading && 
                    !this.props.odcDigitization.regularizeLegajo.response && 
                    !this.props.odcDigitization.regularizeLegajo.success){
                // Notistack
                this.setState(state => ({
                    ...state,
                    regularizeButtonLoading: false, 
                    regularizeButtonDisabled: false, 
                }));
            }
        }
    }
    handleSubmitSendLegajo = e => {
        e.preventDefault();
        this.handleClickRegularizeToggleModal();
    }
    handleSubmitRegularizeLegajo = row => e => {
        e.preventDefault();
        let sendData = {
            tipo_doc: row.cod_tipo_documento? row.cod_tipo_documento: 1,
            nro_doc: row.des_nro_documento? row.des_nro_documento: 1,
            tipo_doc_letra: row.des_tipo_documento? row.des_tipo_documento: "DNI",
            cod_solicitud: row.cod_solicitud? row.cod_solicitud: 0,
            cod_solicitud_completa: row.cod_solicitud_completa? row.cod_solicitud_completa: "",
            cod_solicitud_digitalizacion: 0,
            digitalizacionActividad: {
                nom_actividad: "Digitalización: Legajo Regularizado",
                cod_flujo_fase_estado_actual: 50003,
                cod_flujo_fase_estado_anterior: 50002,
                des_observacion_legajo: null,
            }
        }
        this.props.regularizeLegajo(sendData)
        .then(response => {
            if(response){
                if(response.data){
                    if(response.data.success){
                        this.getNotistack(`Digitalización: Solicitud ${row.cod_solicitud_completa} - Legajo Regularizado.`, "success");
                    }
                }
            }
        });
    }
    handleClickObservationToggleModal = e => {
        this.setState(state => ({
            openObservationModal: !state.openObservationModal
        }));
    }

    handleClickRegularizeToggleModal = e => {
        this.setState(state => ({
            openRegularizeModal: !state.openRegularizeModal
        }));
    }
    // Client - TextField - Event Change - Required    
    handleChangeTextFieldRequired = name => e => {
        e.persist();
        let { value } = e.target;
        this.setState(state => ({
            ...state,
            [name]: value, 
        }));
    }
    // Only Text
    handleKeyPressTextFieldOnlyText = name => e =>{
        let code = (e.which) ? e.which : e.keyCode;
        if(!onlyTextKeyCode(code)){
            e.preventDefault();
        }       
    }
    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render = () => {
        const { classes, row } = this.props;
        let { openObservationModal, openRegularizeModal, 
            observationButtonDisabled, 
            regularizeButtonLoading, regularizeButtonDisabled, buttonDisabled } = this.state;
        return  (
            <React.Fragment>
                <Tooltip title={`Observación Legajo: ${row.cod_solicitud_completa}`} placement="left">
                    <div>
                        <Fab className={classes.fab} onClick={this.handleClickObservationToggleModal} disabled={buttonDisabled}>
                            <VisibilityIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>  
                            
                <Dialog
                    onClose={this.handleClickObservationToggleModal}
                    open={openObservationModal}
                    maxWidth='xs'
                    aria-labelledby="form-observe-legajo">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-observe-legajo" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                Observaciones del Legajo aqui
                            </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form onSubmit={this.handleSubmitSendLegajo}>
                        <DialogContent>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    multiline
                                    rows="4"
                                    label="Observaciones"
                                    type="text"
                                    margin="normal"
                                    color="default"
                                    name="observation"
                                    variant="outlined"
                                    value={this.state.observation}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                    onChange={this.handleChangeTextFieldRequired("observation")}
                                />
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={12}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            disabled={observationButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickObservationToggleModal}
                                            margin="normal">
                                            Cancelar
                                            <CloseIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  


                <Dialog
                    open={openRegularizeModal}
                    maxWidth='xs'
                    aria-labelledby="form-send-legajo">
                    <DialogTitle 
                        id="form-send-legajo" 
                        className="bg-metal-blue">
                       <Typography align="center" component="span" variant="h6" className="text-white text-shadow-black">
                           Regularizar Legajo
                        </Typography>
                    </DialogTitle>
                    <form onSubmit={this.handleSubmitRegularizeLegajo(row)}>
                        <DialogContent>
                            <Typography align="center">
                                ¿Se regularizó las observaciones reportadas por el proveedor, por favor, 
                                seleccionar el boton <strong>"Confirmar"</strong>,  para la solicitud:
                                <strong> {row.cod_solicitud_completa}</strong>?
                            </Typography>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            disabled={regularizeButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickRegularizeToggleModal}
                                            margin="normal">
                                            Cancelar
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={regularizeButtonDisabled}
                                            className={classNames(classes.button)}
                                            type="submit" 
                                            margin="normal"
                                            color="primary" 
                                            size="small"
                                            fullWidth>
                                            Confirmar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                               regularizeButtonLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  

            </React.Fragment>
        )        
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(RegularizeLegajoActionButton)));
