import React from "react";
import * as moment from "moment";
import { Table } from '@devexpress/dx-react-grid-material-ui';
import SendLegajoActionButton from '../action-button/SendLegajoActionButton';
import RegularizeLegajoActionButton from '../action-button/RegularizeLegajoActionButton';
import NewCommentForm from '../../../../../../components/NewCommentForm/NewCommentForm';

// import FinishLegajoActionButton from '../action-button/FinishLegajoActionButton';

const MyField = (props) => (
    <span style={{color: props.fontColor , padding: '.2em .5em', backgroundColor: props.bg, borderRadius: '.2em'}}>
        {props.value}
    </span>
)


const  Cell = ({...props}) => {

    let { row } = props;

    if(props.column.name === "fec_reg"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    if(props.column.name === "fec_reg_emboce"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    if(props.column.name === 'des_flujo_fase_estado_dig'){
        return <Table.Cell {...props} value={<MyField  bg={props.row.des_color_dig} value={props.value} fontColor={row.cod_flujo_fase_estado_dig === 50002 || row.cod_flujo_fase_estado_dig === 80002 ?  '#ffffff' : '#363636'} />} />
    }
    if(props.column.name === "send"){
        return <Table.Cell {...props}>
            <SendLegajoActionButton {...props}  disabled={!(row.cod_flujo_fase_estado_dig === 50001 || row.cod_flujo_fase_estado_dig === 80001) }/>
        </Table.Cell>
    }
    if(props.column.name === "observation"){
        return <Table.Cell {...props}>
            <RegularizeLegajoActionButton {...props} disabled={!(row.cod_flujo_fase_estado_dig === 50003 || row.cod_flujo_fase_estado_dig === 80003)}/>
        </Table.Cell>
    }
    // else if(props.column.name === "finish"){
    //     return <Table.Cell {...props}>
    //         <FinishLegajoActionButton {...props} disabled={finishLegajoActionButton}/>
    //     </Table.Cell>
    // }
    if(props.column.name === "comentario"){
        return <Table.Cell {...props}>
            <NewCommentForm {...props}/>
        </Table.Cell>
    }
    return <Table.Cell {...props} />
}

export default Cell;
