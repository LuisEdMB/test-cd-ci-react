import React, { Component } from "react";
import * as moment from "moment";
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
// React Router 
import { connect } from 'react-redux';
// Component
import{ 
    IconButton,
    Grid, 
    LinearProgress,
    Divider
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close'; 
// Components Custom 
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import HeaderReport from '../../../../components/header-report/HeaderReport';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
// Actions 
import { inboxMonitoring } from '../../../../actions/odc-digitization/odc-digitization';
import { bindActionCreators } from "redux";
// Utils
import * as Utils from '../../../../utils/Utils'

const styles = theme => ({
    avatar:{
        width:80, 
        height:48
    },
    modalRoot:{
       
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        odcDigitization : state.odcDigitizationReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        inboxMonitoring: bindActionCreators(inboxMonitoring, dispatch),
    };
    return actions;
}

class ReceiveLegajoPage extends Component {

    state = {
        defaultColumnWidths: [
            { columnName: 'cod_solicitud_completa', width: 125 },
            { columnName: 'des_tipo_documento', width: 130 },
            { columnName: 'des_nro_documento', width: 125 },
            { columnName: 'des_nombre_completo', width: 150 },
            { columnName: 'des_tipo_operacion', width: 130 },
            { columnName: 'des_jerarquias_flujo_fase_estado', width: 250 },
            { columnName: 'tip_tarjeta', width: 200 },
            { columnName: 'des_usu_reg', width: 135 },
            { columnName: 'des_agencia', width: 135 },
            { columnName: 'fec_reg', width: 130 },
            { columnName: 'des_usu_reg_emboce', width: 135 },
            { columnName: 'des_agencia_emboce', width: 135 },
            { columnName: 'fec_reg_emboce', width: 130 },
            { columnName: 'send', width: 130 },
            { columnName: 'des_observacion_legajo', width: 150},
            { columnName: 'observation', width: 130 },
            { columnName: 'finish', width: 130 },
            { columnName: 'validate', width: 130 },
            { columnName: 'des_usu_reg_activacion', width: 150 },
            { columnName: 'des_agencia_activacion', width: 150 },
            { columnName: 'fec_reg_activacion', width: 150 },

            { columnName: 'des_usu_asesor', width: 150},
            { columnName: 'fec_reg_asesor', width:  150},
            { columnName: 'hora_reg_asesor', width:150  },

            { columnName: 'des_usu_digit', width: 150 },
            { columnName: 'fec_reg_digit', width: 150 },
            { columnName: 'hora_reg_digit', width: 150 },
            { columnName: 'des_flujo_fase_estado_dig', width: 200 },

            { columnName: 'num_tarjeta_pmp', width: 180 },
            { columnName: 'num_cuenta_pmp', width: 180 },
            
            { columnName: 'des_usu_opera', width: 150 },
            { columnName: 'fec_reg_opera', width: 150 },
            { columnName: 'hora_reg_opera', width: 150 },
            { columnName: 'observation', width: 150 },         
            
        ],
        columns:[],
        tableColumnExtensions: [
            { columnName: 'des_tipo_documento', align: 'center' },
            { columnName: 'send', align: 'center' },
            { columnName: 'observation', align: 'center' },
            { columnName: 'finish', align: 'center' },
            { columnName: 'observation', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        rowsExcel: [],

        additional: 0, 
        reprint: 0, 
        express: 0,
        pageSizes: [5, 10, 15, 20, 0],
        columnExcel: [],
        showHideSearchInput: false, 
    };
    deleteElementArray = (array, a) => {
        for (var i = 0; i < array.length; i++) {
            if (array[i] === a) {
                for (var i2 = i; i2 < array.length - 1; i2++) {
                    array[i2] = array[i2 + 1];
                }
                array.length = array.length - 1;
                return;
            }
        }
    };
    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }

        if (prevProps.odcDigitization.inboxMonitoring !== this.props.odcDigitization.inboxMonitoring) {	
            
            if(this.props.odcDigitization.inboxMonitoring.loading){
                this.setState(state => ({
                    ...state, 
                    rows: [], 
                    columnExcel: [],
                    rowsExcel: []
                }));
            }
            
            if(!this.props.odcDigitization.inboxMonitoring.loading && 
                this.props.odcDigitization.inboxMonitoring.response && 
                this.props.odcDigitization.inboxMonitoring.success){

                    let { data } = this.props.odcDigitization.inboxMonitoring;  
                    let { solicitudesMonitoreo, solicitudesMonitoreoExcel } = data;

                    if(solicitudesMonitoreo && solicitudesMonitoreoExcel){
                        // Label                
                        let columnExcel = Object.keys(solicitudesMonitoreoExcel.length > 0? solicitudesMonitoreoExcel[0]: []);
                        columnExcel = columnExcel.filter(item => item !== 'Nro. LP').map((item, index) => {
                            return {value: item, label: item}
                        });
                        columnExcel = Utils.addNewObjectIntoArrayByPosition(columnExcel, { value: 'Flujo Fase Estado', label: 'Flujo Fase Estado' }, 5)
                        
                        let rows = solicitudesMonitoreo
                        rows = rows.map(e=>{
                            let ee = {...e}
                            ee.fec_reg_asesor = e.fec_reg_asesor ?  moment(e.fec_reg_asesor.replace("T", " ")).format('DD/MM/YYYY') : "";
                            ee.hora_reg_asesor = e.fec_reg_asesor ?  moment(e.fec_reg_asesor.replace("T", " ")).format('HH:mm') : "";

                            ee.fec_reg_digit = e.fec_reg_digit ?  moment(e.fec_reg_digit.replace("T", " ")).format('DD/MM/YYYY') : "";
                            ee.hora_reg_digit = e.fec_reg_digit ?  moment(e.fec_reg_digit.replace("T", " ")).format('HH:mm') : "";

                            ee.fec_reg_opera = e.fec_reg_opera ?  moment(e.fec_reg_opera.replace("T", " ")).format('DD/MM/YYYY') : "";
                            ee.hora_reg_opera = e.fec_reg_opera ?  moment(e.fec_reg_opera.replace("T", " ")).format('HH:mm') : "";

                            return ee
                        })

                        let rowsExcel = (solicitudesMonitoreoExcel.length > 0 ? solicitudesMonitoreoExcel : []).map(row => {
                            const monit = solicitudesMonitoreo.find(item => item.cod_solicitud_completa === row['Solicitud']) || { }
                            return Utils.addNewPropertyIntoObjectByPosition(row, { 'Flujo Fase Estado': monit.des_jerarquias_flujo_fase_estado || '' }, 5)
                        });
                        
                        this.setState(state => ({
                            ...state, 
                            rows: rows, 
                            additional: this.props.odcDigitization.inboxMonitoring.data.contador_additional, 
                            reprint: this.props.odcDigitization.inboxMonitoring.data.contador_reimpresiones, 
                            express: this.props.odcDigitization.inboxMonitoring.data.contador_express,
                            columnExcel: columnExcel,
                            rowsExcel: rowsExcel 
                        }));
                    }
            }
            else if(!this.props.odcDigitization.inboxMonitoring.loading && 
                     this.props.odcDigitization.inboxMonitoring.response && 
                    !this.props.odcDigitization.inboxMonitoring.success){
                // Notistack
                this.getNotistack(this.props.odcDigitization.inboxMonitoring.error, "error");
            }
            else if(!this.props.odcDigitization.inboxMonitoring.loading && 
                    !this.props.odcDigitization.inboxMonitoring.response && 
                    !this.props.odcDigitization.inboxMonitoring.success){
                // Notistack
                this.getNotistack(this.props.odcDigitization.inboxMonitoring.error, "error");
            }
        }
        
        if(prevProps.odcDigitization.receivedLegajo !== this.props.odcDigitization.receivedLegajo){
            if (!this.props.odcDigitization.receivedLegajo.loading &&
                 this.props.odcDigitization.receivedLegajo.response &&
                 this.props.odcDigitization.receivedLegajo.success) {
                this.props.inboxMonitoring("", "");
            }
            else if (!this.props.odcDigitization.receivedLegajo.loading &&
                      this.props.odcDigitization.receivedLegajo.response &&
                     !this.props.odcDigitization.receivedLegajo.success) {
                this.getNotistack(this.props.odcDigitization.receivedLegajo.error, "error");
            }
            /*else if (!this.props.odcDigitization.receivedLegajo.loading &&
                     this.props.odcDigitization.receivedLegajo.response &&
                     !this.props.odcDigitization.receivedLegajo.success) {
                this.getNotistack(this.props.odcDigitization.receivedLegajo.error, "error");
            }*/
        }
    }

    componentWillMount(){
        let columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns:columns
        }));
    }
    componentDidMount(){
        let sendData = { 
            des_usuario: "",
            cod_agencia: "",
            des_nro_documento: "",
            cod_solicitud_completa: "", 
        }
        this.props.inboxMonitoring(sendData);
    }
    
    getColumnsRender(){
        let columns = [];
        if(this.props.width === "xs"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },

                /*{ name: 'send', title: 'Enviar Legajo' },
                { name: 'observation', title: 'Ver Observación' },
                { name: 'finish', title: 'Cerrado' }*/
            ];
        }
        else if(this.props.width === "sm"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
               
                /*{ name: 'send', title: 'Enviar' },
                { name: 'observation', title: 'Ver Observación' },
                { name: 'finish', title: 'Cerrado' }*/
            ]
        }
        else if(this.props.width === "md"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'observation', title: 'Ver Comentario' },
                
                /*{ name: 'send', title: 'Enviar' },
                { name: 'observation', title: 'Ver Observación' },
                { name: 'finish', title: 'Cerrado' }*/
            ]
        }
        else if(this.props.width === "lg"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'des_tipo_operacion', title: 'Tipo Operación' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo Fase Estado' },
                { name: 'tip_tarjeta', title: 'Tipo Tarjeta' },
                
                { name: 'num_tarjeta_pmp', title:  'Nro. Tarjeta'},
                { name: 'num_cuenta_pmp', title: 'Nro. Cta.' },

                { name: 'des_usu_reg', title: 'Usuario Creador' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_usu_reg_emboce', title: 'Usuario Emboce' },
                { name: 'des_agencia_emboce', title: 'Agencia Emboce' },
                { name: 'fec_reg_emboce', title: 'Fecha Emboce' },
                { name: 'des_observacion_legajo', title: 'Observacion' },
                
                { name: 'des_usu_reg_activacion', title: 'Usuario Activación' },
                { name: 'des_agencia_activacion', title: 'Agencia Activación' },
                { name: 'fec_reg_activacion', title: 'Fecha Activación' },

                { name: 'des_usu_asesor', title: 'Usuario Asesor' },
                { name: 'fec_reg_asesor', title: 'Fecha de envio Asesor' },
                { name: 'hora_reg_asesor', title: 'Hora de envio Asesor' },

                { name: 'des_usu_digit', title: 'Usuario Digitalizador' },
                { name: 'fec_reg_digit', title: 'Fecha de envio Digit.' },
                { name: 'hora_reg_digit', title: 'Hora de envio Digit.' },

                { name: 'des_usu_opera', title: 'Usuario Operador' },
                { name: 'fec_reg_opera', title: 'Fecha Operador' },
                { name: 'hora_reg_opera', title: 'Hora Operador' },

                { name: 'des_flujo_fase_estado_dig', title: 'Estado' },
                { name: 'validate', title: 'Validar' },
                { name: 'observation', title: 'Ver Comentario' },
               
                // { name: 'send', title: 'Enviar Legajo' },
                // { name: 'observation', title: 'Ver Observación' },
                // { name: 'finish', title: 'Cerrado' }
            ]
        }
        else if(this.props.width === "xl"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'des_tipo_operacion', title: 'Tipo Operación' },
                { name: 'des_jerarquias_flujo_fase_estado', title: 'Flujo Fase Estado' },
                { name: 'tip_tarjeta', title: 'Tipo Tarjeta' },
                { name: 'num_tarjeta_pmp', title:  'Nro. Tarjeta'},
                { name: 'num_cuenta_pmp', title: 'Nro. Cta.' },
                { name: 'des_usu_reg', title: 'Usuario Creador' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_usu_reg_emboce', title: 'Usuario Emboce' },
                { name: 'des_agencia_emboce', title: 'Agencia Emboce' },
                { name: 'fec_reg_emboce', title: 'Fecha Emboce' },
                { name: 'des_observacion_legajo', title: 'Observacion' },
                
                { name: 'des_usu_reg_activacion', title: 'Usuario Activación' },
                { name: 'des_agencia_activacion', title: 'Agencia Activación' },
                { name: 'fec_reg_activacion', title: 'Fecha Activación' },

                { name: 'des_usu_asesor', title: 'Usuario Asesor' },
                { name: 'fec_reg_asesor', title: 'Fecha de envio Asesor' },
                { name: 'hora_reg_asesor', title: 'Hora de envio Asesor' },

                { name: 'des_usu_digit', title: 'Usuario Digitalizador' },
                { name: 'fec_reg_digit', title: 'Fecha de envio Digit.' },
                { name: 'hora_reg_digit', title: 'Hora de envio Digit.' },

                { name: 'des_usu_opera', title: 'Usuario Operador' },
                { name: 'fec_reg_opera', title: 'Fecha Operador' },
                { name: 'hora_reg_opera', title: 'Hora Operador' },

                { name: 'des_flujo_fase_estado_dig', title: 'Estado' },
                { name: 'validate', title: 'Validar' },
                { name: 'observation', title: 'Ver Comentario' },
               
                // { name: 'send', title: 'Enviar Legajo' },
                // { name: 'observation', title: 'Ver Observación' },
                // { name: 'finish', title: 'Cerrado' }
            ]
        }
        return columns;
    }
    
    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render() {
        const { width, odcDigitization } = this.props;
        const { 
                tableColumnExtensions,
                columns,
                defaultColumnWidths, 
                defaultHiddenColumnNames, 
                rows,
                columnExcel,
                rowsExcel
            } = this.state;
        
        return (
                <Grid container className="p-1">
                    <Grid item xs={12}>
                        {
                            odcDigitization.inboxMonitoring.loading ?  <LinearProgress /> : ""
                        }
                    </Grid>
                    <Grid item xs={12}>
                        {/* <Header 
                            title="Bandeja de Monitoreo de Legajo" 
                            additional={additional}
                            reprint={reprint}
                            express={express}
                            excel={{
                                workBook: [
                                    {   sheet:{
                                            data: rowsExcel,
                                            columns: columnExcel,
                                            nameSheet: "ODC - Solicitudes"
                                        }
                                    }
                                ],
                                nameFile: "Datos exportados"
                            }}
                        /> */}

                        <HeaderReport 
                            title={"Bandeja de Monitoreo de Legajo" }
                            columnExcel={columnExcel} 
                            dataExcel={rowsExcel}
                            data={rows}
                            nameSheet={"Reporte de Monitoreo de Legajo"} 
                            nameFile={"Datos exportados"}
                        
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Divider className="mb-2"/>  
                    </Grid>
                    <Grid item xs={12}>
                        <DevGridComponent 
                            rows={rows}
                            columns={columns}
                            width={width}
                            search={true}
                            columnExtensions={tableColumnExtensions}
                            defaultColumnWidths={defaultColumnWidths}
                            defaultHiddenColumnNames={defaultHiddenColumnNames}
                            RowDetailComponent={RowDetail}
                            CellComponent={Cell}/>
                    </Grid>
                </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(ReceiveLegajoPage))));

