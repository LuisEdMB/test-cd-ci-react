import React from 'react';
// Components 
import { 
    Fab, 
    withStyles,
    Tooltip
 } from "@material-ui/core";
// Colors
import blue from '@material-ui/core/colors/blue';
// Icons 
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight'

const styles = theme => ({
    modalRoot:{
        minWidth:280, 
        width:350,
        maxWidth:400,
    },
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: blue[700],
        '&:hover': {
            backgroundColor: blue[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    buttonProgressWrapper:{
        position: 'relative'
    },
    button:{
        textTransform: 'none',
    }
});

const SendLegajoActionButton = ({ classes, disabled, row }) => (
    <Tooltip title={
            disabled? `Estado: Legajo por enviar, solicitud: ${row.cod_solicitud_completa}` : 
                      `Estado: Legajo enviado, solicitud: ${row.cod_solicitud_completa}`}
            placement="left">
        <div>
            <Fab className={classes.fab} disabled={disabled}>
                <SubdirectoryArrowRightIcon className={classes.icon} fontSize="small"/>
            </Fab>
        </div>
    </Tooltip>

)       


export default withStyles(styles)(SendLegajoActionButton);