import React, { Component } from "react";
import * as moment from "moment";
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
// React Router 
import { connect } from 'react-redux';
// Component
import{ 
    IconButton,
    Grid, 
    LinearProgress,
    Divider
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close'; 
// Components Custom 
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import HeaderReport from '../../../../components/header-report/HeaderReport';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
// Actions 
import { inboxMonitoring } from '../../../../actions/odc-digitization/odc-digitization';
import { bindActionCreators } from "redux";

const styles = theme => ({
    avatar:{
        width:80, 
        height:48
    },
    modalRoot:{
       
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        odcDigitization : state.odcDigitizationReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        inboxMonitoring: bindActionCreators(inboxMonitoring, dispatch)
    };
    return actions;
}

class ReceiveLegajoPage extends Component {

    state = {
        defaultColumnWidths: [
            { columnName: 'cod_solicitud_completa', width: 125 },
            { columnName: 'des_tipo_documento', width: 130 },
            { columnName: 'des_nro_documento', width: 125 },
            { columnName: 'des_nombre_completo', width: 150 },
            { columnName: 'des_tipo_operacion', width: 130 },
            { columnName: 'tip_tarjeta', width: 200 },
            { columnName: 'des_usu_reg', width: 135 },
            { columnName: 'des_agencia', width: 135 },
            { columnName: 'fec_reg', width: 130 },
            { columnName: 'des_usu_reg_emboce', width: 135 },
            { columnName: 'des_agencia_emboce', width: 135 },
            { columnName: 'fec_reg_emboce', width: 130 },
            { columnName: 'send', width: 130 },
            { columnName: 'des_observacion_legajo', width: 150},
            { columnName: 'observation', width: 130 },
            { columnName: 'finish', width: 130 },
            { columnName: 'validate', width: 130 },
            { columnName: 'estado', width: 200 },

            { columnName: 'num_tarjeta_pmp', width: 180 },
            { columnName: 'num_cuenta_pmp', width: 180 },
            { columnName: 'num_cuenta_lp', width: 180 }
            
        ],
        columns:[],
        tableColumnExtensions: [
            { columnName: 'des_tipo_documento', align: 'center' },
            { columnName: 'send', align: 'center' },
            { columnName: 'observation', align: 'center' },
            { columnName: 'finish', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        rowsExcel: [],

        additional: 0, 
        reprint: 0, 
        express: 0,
        pageSizes: [5, 10, 15, 20, 0],
        columnExcel: [],
        showHideSearchInput: false, 
    };
    deleteElementArray = (array, a) => {
        for (var i = 0; i < array.length; i++) {
            if (array[i] === a) {
                for (var i2 = i; i2 < array.length - 1; i2++) {
                    array[i2] = array[i2 + 1];
                }
                array.length = array.length - 1;
                return;
            }
        }
    };
    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }

        if (prevProps.odcDigitization.inboxMonitoring !== this.props.odcDigitization.inboxMonitoring) {	
            if(!this.props.odcDigitization.inboxMonitoring.loading && 
                this.props.odcDigitization.inboxMonitoring.response && 
                this.props.odcDigitization.inboxMonitoring.success){

                    let { data } = this.props.odcDigitization.inboxMonitoring;  
                    let { solicitudesMonitoreo, solicitudesMonitoreoExcel } = data;

                    if(solicitudesMonitoreo && solicitudesMonitoreoExcel){
                        // Label                
                        let columnExcel = Object.keys(solicitudesMonitoreoExcel.length > 0? solicitudesMonitoreoExcel[0]: []);
                        columnExcel = columnExcel.map((item, index) => {
                            return {value: item, label: item}
                        });
                        
                        let row = solicitudesMonitoreo.map((item, index)=>{
                            let i = {...item};
                            i.est = index%7;
                            return i;
                        })


        
                        let rowsExcel = solicitudesMonitoreoExcel.length > 0 ? solicitudesMonitoreoExcel : [];
                        rowsExcel = rowsExcel.map((item, index) => {
                            item["Fecha Emboce"] = item["Fecha Emboce"]? moment(item["Fecha Emboce"].replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
                            item["Fecha Registro"] = item["Fecha Registro"]? moment(item["Fecha Registro"].replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
                            return item;
                        });
                        
                        this.setState(state => ({
                            ...state, 
                            rows: row, 
                            additional: this.props.odcDigitization.inboxMonitoring.data.contador_additional, 
                            reprint: this.props.odcDigitization.inboxMonitoring.data.contador_reimpresiones, 
                            express: this.props.odcDigitization.inboxMonitoring.data.contador_express,
                            columnExcel: columnExcel,
                            rowsExcel: rowsExcel 
                        }));
                    }
            }
            else if(!this.props.odcDigitization.inboxMonitoring.loading && 
                     this.props.odcDigitization.inboxMonitoring.response && 
                    !this.props.odcDigitization.inboxMonitoring.success){
                // Notistack
                this.getNotistack(this.props.odcDigitization.inboxMonitoring.error, "error");
            }
            else if(!this.props.odcDigitization.inboxMonitoring.loading && 
                    !this.props.odcDigitization.inboxMonitoring.response && 
                    !this.props.odcDigitization.inboxMonitoring.success){
                // Notistack
                this.getNotistack(this.props.odcDigitization.inboxMonitoring.error, "error");
            }
        }
    }

    componentWillMount(){
        let columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns:columns
        }));
    }
    componentDidMount(){
        let sendData = { 
            des_usuario: "",
            cod_agencia: "",
            des_nro_documento: "",
            cod_solicitud_completa: "", 
        }
        this.props.inboxMonitoring(sendData);
    }
    
    getColumnsRender(){
        let columns = [];
        if(this.props.width === "xs"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },

                /*{ name: 'send', title: 'Enviar Legajo' },
                { name: 'observation', title: 'Ver Observación' },
                { name: 'finish', title: 'Cerrado' }*/
            ];
        }
        else if(this.props.width === "sm"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
               
                /*{ name: 'send', title: 'Enviar' },
                { name: 'observation', title: 'Ver Observación' },
                { name: 'finish', title: 'Cerrado' }*/
            ]
        }
        else if(this.props.width === "md"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                
                /*{ name: 'send', title: 'Enviar' },
                { name: 'observation', title: 'Ver Observación' },
                { name: 'finish', title: 'Cerrado' }*/
            ]
        }
        else if(this.props.width === "lg"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'des_tipo_operacion', title: 'Tipo Operación' },
                { name: 'tip_tarjeta', title: 'Tipo Tarjeta' },
                { name: 'num_tarjeta_pmp', title:  'Nro. Tarjeta'},
                { name: 'num_cuenta_pmp', title: 'Nro. Cta.' },
                { name: 'num_cuenta_lp', title: 'Nro. LP' },
                { name: 'des_usu_reg', title: 'Usuario Creador' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_usu_reg_emboce', title: 'Usuario Emboce' },
                { name: 'des_agencia_emboce', title: 'Agencia Emboce' },
                { name: 'fec_reg_emboce', title: 'Fecha Emboce' },
                { name: 'des_observacion_legajo', title: 'Observacion Legajo' },
                { name: 'des_usu_reg', title: 'Usuario Activación' },
                { name: 'des_agencia_emboce', title: 'Agencia Activación' },
                { name: 'fec_reg_emboce', title: 'Fecha Activación' },
                { name: 'estado', title: 'Estado' },
                { name: 'validate', title: 'Validar' },
                
                /*{ name: 'send', title: 'Enviar' },
                { name: 'observation', title: 'Observación' },
                { name: 'finish', title: 'Cerrado' }*/
            ]
        }
        else if(this.props.width === "xl"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'des_tipo_operacion', title: 'Tipo Operación' },
                { name: 'tip_tarjeta', title: 'Tipo Tarjeta' },
                { name: 'des_usu_reg', title: 'Usuario Creador' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_usu_reg_emboce', title: 'Usuario Emboce' },
                { name: 'des_agencia_emboce', title: 'Agencia Emboce' },
                { name: 'fec_reg_emboce', title: 'Fecha Emboce' },
                { name: 'des_observacion_legajo', title: 'Observacion Legajo' },
                { name: 'des_usu_reg', title: 'Usuario Activación' },
                { name: 'des_agencia_emboce', title: 'Agencia Activación' },
                { name: 'fec_reg_emboce', title: 'Fecha Activación' },
                { name: 'estado', title: 'Estado' },
                { name: 'validate', title: 'Validar' },
               
                /*{ name: 'send', title: 'Enviar Legajo' },
                { name: 'observation', title: 'Ver Observación' },
                { name: 'finish', title: 'Cerrado' }*/
            ]
        }
        return columns;
    }
    
    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render() {
        const { width, odcDigitization } = this.props;
        const { 
                tableColumnExtensions,
                columns,
                defaultColumnWidths, 
                defaultHiddenColumnNames, 
                rows,
                columnExcel,
                rowsExcel
            } = this.state;
        
        return (
                <Grid container className="p-1">
                    <Grid item xs={12}>
                        {
                            odcDigitization.inboxMonitoring.loading ?  <LinearProgress /> : ""
                        }
                    </Grid>
                    <Grid item xs={12}>
                        {/* <Header 
                            title="Bandeja de Monitoreo de Legajo" 
                            additional={additional}
                            reprint={reprint}
                            express={express}
                            excel={{
                                workBook: [
                                    {   sheet:{
                                            data: rowsExcel,
                                            columns: columnExcel,
                                            nameSheet: "ODC - Solicitudes"
                                        }
                                    }
                                ],
                                nameFile: "Datos exportados"
                            }}
                        /> */}

                        <HeaderReport 
                            title={"Bandeja de Monitoreo de Legajo (PROTOTYPE)" }
                            columnExcel={columnExcel} 
                            dataExcel={rowsExcel}
                            data={rows}
                            nameSheet={"Reporte de Monitoreo de Legajo"} 
                            nameFile={"Datos exportados"}
                        
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Divider className="mb-2"/>  
                    </Grid>
                    <Grid item xs={12}>
                        <DevGridComponent 
                            rows={rows}
                            columns={columns}
                            width={width}
                            search={true}
                            columnExtensions={tableColumnExtensions}
                            defaultColumnWidths={defaultColumnWidths}
                            defaultHiddenColumnNames={defaultHiddenColumnNames}
                            RowDetailComponent={RowDetail}
                            CellComponent={Cell}/>
                    </Grid>
                </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(ReceiveLegajoPage))));

