import React from 'react';
import ClassNames from 'classnames';
import { 
    Paper,
    Typography,
    withStyles,
    IconButton,
    Badge,
    Tooltip
} from '@material-ui/core';
import {Zoom} from 'react-reveal';
import CencosudScotiaBank from '../../../../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg';
// Icons
import PrintIcon from '@material-ui/icons/Print';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import ExportExcel from '../../../../../components/file/ExportExcel';

const styles = theme =>({
    rootWrapper:{
       // borderBottom:"1px solid #007ab8"
    },
    avatar:{
        [theme.breakpoints.down('md')]: {
            width: 68, 
            height: 36
        },
        width:80, 
        height:48
    },
    root:{
        display: "flex", 
        justifyContent: "space-between", 
        alignItems: "center",
       
    },
    actionRoot:{
        display: "flex", 
        justifyContent: "center", 
        alignItems: "center"
    }, 
    green:{
        background: `radial-gradient(circle at center, red 0, blue, green 100%)`
    },
    title:{
        [theme.breakpoints.down('md')]: {
            fontSize:14, 
        }
    }
});

const Header = ({title="", express = 0, reprint = 0, additional = 0, max = 999, excel, classes}) => (
    <Paper className={ClassNames(classes.rootWrapper, "py-1")} color="primary">
        <figure className={classes.root}>
            <img
                className={classes.avatar}
                src={CencosudScotiaBank}
                alt="Cencosud ScotiaBank" 
                />
            <figcaption className="w-100">
                <Typography align="center" variant="h5" component="h4" className={classes.title}>
                    {title}
                </Typography>
            </figcaption>
            <div className={classes.actionRoot}>
                <Tooltip TransitionComponent={Zoom} title="Exportar a Excel.">
                    <div className="border-right pr-2">
                        <ExportExcel nameFile={excel.nameFile} workBook={excel.workBook}/>
                    </div>
                </Tooltip>
                <Tooltip TransitionComponent={Zoom} title="# Originaciones." className="d-none">
                    <div className="pl-2">
                        <IconButton aria-label={`${express}`}>
                            <Badge badgeContent={express} max={max} color="primary">
                                <CreditCardIcon />
                            </Badge>
                        </IconButton>
                    </div>
                </Tooltip>
                <Tooltip TransitionComponent={Zoom} title="# Reimpresiones." className="d-none">
                    <div>
                        <IconButton aria-label={`${reprint}`}>
                            <Badge badgeContent={reprint} max={max} color="primary">
                                <PrintIcon />
                            </Badge>
                        </IconButton>
                    </div>
                </Tooltip>
                <Tooltip TransitionComponent={Zoom} title="# Adicionales." className="d-none">
                    <div>
                        <IconButton aria-label={`${additional}`}>
                            <Badge badgeContent={additional} max={max} color="primary">
                                <GroupAddIcon />
                            </Badge>
                        </IconButton>
                    </div>
                </Tooltip>
            </div>
        </figure>
    </Paper>
)

export default withStyles(styles)(Header);