import React from 'react';
// Components 
import { 
    Fab, 
    withStyles,
    Tooltip
 } from "@material-ui/core";
// Colors
import orange from '@material-ui/core/colors/orange';
// Icons 
import VisibilityIcon from '@material-ui/icons/Visibility'

const styles = theme => ({
    modalRoot:{
        minWidth:280, 
        width:350,
        maxWidth:400,
    },
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: orange[700],
        '&:hover': {
            backgroundColor: orange[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    buttonProgressWrapper:{
        position: 'relative'
    },
    button:{
        textTransform: 'none',
    }
});

const RegularizeLegajoActionButton = ({ classes, disabled, row}) => (
    <Tooltip title={
        !disabled? `Estado: Legajo observado, solicitud: ${row.cod_solicitud_completa}` : 
                  `Estado: Legajo sin observación, solicitud: ${row.cod_solicitud_completa}`}
        placement="left">
            <div>
                <Fab className={classes.fab} disabled={disabled}>
                    <VisibilityIcon className={classes.icon} fontSize="small"/>
                </Fab>
            </div>
    </Tooltip>

)        


export default withStyles(styles)(RegularizeLegajoActionButton);