import React from 'react';
// Components 
import { 
    Fab, 
    withStyles,
    Tooltip
 } from "@material-ui/core";
// Colors
import green from '@material-ui/core/colors/green';
// Icons 
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

const styles = theme => ({
    modalRoot:{
        minWidth:280, 
        width:350,
        maxWidth:400,
    },
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: green[700],
        '&:hover': {
            backgroundColor: green[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    buttonProgressWrapper:{
        position: 'relative'
    },
    button:{
        textTransform: 'none',
    }
});

const FinishLegajoActionButton = ({ classes, disabled, row }) => {
    return  <Tooltip title={
                !disabled? `Estado: Legajo Cerrado/Finalizado, solicitud: ${row.cod_solicitud_completa}` : 
                          `Estado: por cerrar/finalizar, solicitud: ${row.cod_solicitud_completa}`}
                placement="left">
                    <div>
                        <Fab className={classes.fab} disabled={disabled}>
                            <CheckCircleIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
            </Tooltip>  
}

export default withStyles(styles)(FinishLegajoActionButton);