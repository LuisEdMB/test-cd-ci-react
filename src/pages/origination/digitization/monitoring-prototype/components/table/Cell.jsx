import React from "react"
import * as moment from "moment"
import { Table } from '@devexpress/dx-react-grid-material-ui'
import SendLegajoActionButton from '../action-button/SendLegajoActionButton'
import RegularizeLegajoActionButton from '../action-button/RegularizeLegajoActionButton'
import FinishLegajoActionButton from '../action-button/FinishLegajoActionButton'
import ValidateLegajoActionButton from '../action-button/ValidateLegajoActionButton'

const MyField = (props) => (
    <span style={{color: props.color === "yellow" ?  'black' : 'white', padding: '.2em .5em', backgroundColor: props.color, borderRadius: '.2em'}}>
        { props.value }
    </span>
)

const TipoTC = (props) => (
    <span style={{padding: '.2em .5em'}}>
        { props.value ? 'CENCOSUD VISA' : 'CENCOSUD MASTERCARD' }
    </span>
)

const estado = ["Legajo Pendiente", "Legajo Enviado", "Legajo Observado Digitalización", "Legajo Regularizado Asesor", "Legajo Observado Operaciones", "Legajo Finalizado Operaciones", "Legajo Finalizado Digitalización"]

const color = ["gray", "darkblue", "deepskyblue", "green", "yellow", "red", "red"]

const  Cell = ({...props}) => {
    let { row } = props;
    let sendLegajoDisabled = true;
    let regularizeLegajoDisabled = true;
    let finishLegajoActionButton = true;

    const {est} = row; 
    if(row.cod_flujo_fase_estado_dig === 50001){
        sendLegajoDisabled = false;
    }
    if(row.cod_flujo_fase_estado_dig === 50002){
        regularizeLegajoDisabled = false;
    }
    if(row.cod_flujo_fase_estado_dig === 50005){
        finishLegajoActionButton = false;
    }

    if(props.column.name === "fec_reg"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    else if(props.column.name === "fec_reg_emboce"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    else if(props.column.name === "send"){
        return <Table.Cell {...props}>
                <SendLegajoActionButton {...props}  disabled={sendLegajoDisabled}/>
            </Table.Cell>
    }
    else if(props.column.name === "observation"){
        return <Table.Cell {...props}>
            <RegularizeLegajoActionButton {...props} disabled={regularizeLegajoDisabled}/>
        </Table.Cell>
    }
  
    else if(props.column.name === "finish"){
        return <Table.Cell {...props}>
            <FinishLegajoActionButton {...props} disabled={finishLegajoActionButton}/>
        </Table.Cell>
    }
    else if(props.column.name === "validate"){
        return <Table.Cell {...props}>
            <ValidateLegajoActionButton {...props} disabled={est !== 3} />
        </Table.Cell>
    }
    else if(props.column.name === 'estado') {
        return <Table.Cell {...props} value={<MyField value={estado[est]} color={color[est]} />} />
    }
    else if(props.column.name === 'tipo_TC') {
        return <Table.Cell {...props} value={<TipoTC /*value={props.value}*/ />} />
    }

    return <Table.Cell {...props} />
}

export default Cell;
