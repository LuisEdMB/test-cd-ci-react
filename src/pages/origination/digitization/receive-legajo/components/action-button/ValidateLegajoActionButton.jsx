import React, { Component } from "react";
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
// React Router 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components 
import { 
    AppBar,
    Fab, 
    Tooltip, 
    Button,
    withStyles, 
    Dialog,
    DialogTitle,
    Typography,
    DialogContent,
    Grid,
    DialogActions,
    CircularProgress, 
    TextField, 
    IconButton,
    FormHelperText
 } from "@material-ui/core";
// Colors
import green from '@material-ui/core/colors/green';
// Icons 
import CheckCircleOutlineIcon from '@material-ui/icons/VerifiedUser'; 
import SendIcon from '@material-ui/icons/Send';
import VisibilityIcon from '@material-ui/icons/Visibility';
import CloseIcon from '@material-ui/icons/Close'; 
// Utils
import { onlyTextKeyCode, checkInputKeyCode } from '../../../../../../utils/Utils';
// Actions 
import { observeLegajo, receiveLegajo } from '../../../../../../actions/odc-digitization/odc-digitization';
const styles = theme => ({
    fab:{
        minHeight:0, 
        height:24,
        width:24, 
        color:"white",
        transition:".3s",
        backgroundColor: green[700],
        '&:hover': {
            backgroundColor: green[900],
        },
        textTransform: 'none',
    }, 
    icon:{
        minHeight: 0, 
        height: 12, 
        width: 12
    }, 
    buttonProgressWrapper:{
        position: 'relative'
    },
    button:{
        textTransform: 'none',
    },
    buttonCustom:{
        color: theme.palette.getContrastText(green[500]),
        '&:hover': {
            backgroundColor: green[700],
        },
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
        textTransform: 'none',
    },
    appBar: {
        position: 'relative',
    }
});

const mapStateToProps = (state, props) => {
    return {
        odcDigitization: state.odcDigitizationReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        observeLegajo: bindActionCreators(observeLegajo, dispatch),
        receiveLegajo: bindActionCreators(receiveLegajo, dispatch)
    };
  return actions;
}

class ValidateLegajoActionButton extends Component {
    state = {
        openObservationModal: false,
        openValidateModal: false,
        
        observationButtonLoading: false, 
        observationButtonDisabled: false, 

        validateButtonLoading: false, 
        validateButtonDisabled: false, 
        observation: "",
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.odcDigitization.receivedLegajo !== this.props.odcDigitization.receivedLegajo) {	
            if(this.props.odcDigitization.receivedLegajo.loading){
                this.setState(state => ({
                    ...state,
                    observationButtonLoading: true, 
                    observationButtonDisabled: true, 
                }));
            }
            
            if(!this.props.odcDigitization.receivedLegajo.loading && 
                this.props.odcDigitization.receivedLegajo.response && 
                this.props.odcDigitization.receivedLegajo.success){
                this.setState(state => ({
                    ...state,
                    openObservationModal: false,
                    openValidateModal: false,
                    observationButtonLoading: false, 
                    observationButtonDisabled: false, 
                    observation: "",
                }));
            }
            else if(!this.props.odcDigitization.receivedLegajo.loading && 
                     this.props.odcDigitization.receivedLegajo.response && 
                    !this.props.odcDigitization.receivedLegajo.success){
                this.setState(state => ({
                    ...state,
                    observationButtonLoading: false, 
                    observationButtonDisabled: false, 
                }));
            }
            else if(!this.props.odcDigitization.receivedLegajo.loading && 
                    !this.props.odcDigitization.receivedLegajo.response && 
                    !this.props.odcDigitization.receivedLegajo.success){
                this.setState(state => ({
                    ...state,
                    observationButtonLoading: false, 
                    observationButtonDisabled: false, 
                }));
            }
        }
    }
    handleSubmitValidateLegajo = row => e => {
        e.preventDefault();
        let sendData = {
            tipo_doc: row.cod_tipo_documento? row.cod_tipo_documento: 1,
            nro_doc: row.des_nro_documento? row.des_nro_documento: 1,
            tipo_doc_letra: row.des_tipo_documento? row.des_tipo_documento: "DNI",
            cod_solicitud: row.cod_solicitud? row.cod_solicitud: 0,
            cod_solicitud_completa: row.cod_solicitud_completa? row.cod_solicitud_completa: "",
            cod_solicitud_digitalizacion: 0,
            digitalizacionActividad: {
                nom_actividad: "Legajo Confirmar Digitalización",
                cod_flujo_fase_estado_actual: 50004,
                cod_flujo_fase_estado_anterior: 50002,
                des_observacion_legajo: null,
            }
        }
        this.props.receiveLegajo(sendData)
        .then(response => {
            if(response){
                if(response.data){
                    if(response.data.success){
                        this.getNotistack(`Digitalización: Solicitud ${row.cod_solicitud_completa} - Legajo Confirmar Digitalización.`, "success");
                    }
                }
            }
        });
    }
    handleSubmitObservationLegajo = row => e => {
        e.preventDefault();
        let sendData = {
            tipo_doc: row.cod_tipo_documento? row.cod_tipo_documento: 1,
            nro_doc: row.des_nro_documento? row.des_nro_documento: 1,
            tipo_doc_letra: row.des_tipo_documento? row.des_tipo_documento: "DNI",
            cod_solicitud: row.cod_solicitud? row.cod_solicitud: 0,
            cod_solicitud_completa: row.cod_solicitud_completa? row.cod_solicitud_completa: "",
            cod_solicitud_digitalizacion: 0,
            digitalizacionActividad: {
                nom_actividad: "Legajo Observado Digitalización",
                cod_flujo_fase_estado_actual: 50003,
                cod_flujo_fase_estado_anterior: 50002,
                des_observacion_legajo: this.state.observation,
            }
        }
        this.props.observeLegajo(sendData)
        .then(response => {
            if(response){
                if(response.data){
                    if(response.data.success){
                        this.getNotistack(`Digitalización: Solicitud ${row.cod_solicitud_completa} - Legajo Observado Digitalización.`, "warning");
                    }
                }
            }
        });
        
    }
    handleClickObservationToggleModal = e => {
        this.setState(state => ({
            openObservationModal: !state.openObservationModal
        }));
    }
    handleClickValidateToggleModal = e => {
        this.setState(state => ({
            openValidateModal: !state.openValidateModal
        }));
    }
    // Client - TextField - Event Change - Required    
    handleChangeTextFieldRequired = name => e => {
        e.persist();
        let { value } = e.target;
        this.setState(state => ({
            ...state,
            [name]: value, 
        }));
    }
    // Only Text
    handleKeyPressTextFieldOnlyText = name => e =>{
        let code = (e.which) ? e.which : e.keyCode;
        if(!onlyTextKeyCode(code)){
            e.preventDefault();
        }       
    }
    // Check Input
    handleKeyPressTextFieldCheckInput = name => e => {
        let code = (e.which) ? e.which : e.keyCode;
        if(!checkInputKeyCode(code)){
            e.preventDefault();
        }   
    }
    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render = () => {
        const { classes, row, disabled } = this.props;
        let { openObservationModal, openValidateModal, observation,
            observationButtonLoading, observationButtonDisabled, 
            validateButtonLoading, validateButtonDisabled } = this.state;
        return  (
            <React.Fragment>
                <Tooltip title={`Enviar Legajo: ${row.cod_solicitud_completa}`} placement="left">
                    <div>
                        <Fab className={classes.fab} onClick={this.handleClickValidateToggleModal} disabled={disabled}>
                            <CheckCircleOutlineIcon className={classes.icon} fontSize="small"/>
                        </Fab>
                    </div>
                </Tooltip>  
                            
                <Dialog
                    onClose={this.handleClickValidateToggleModal}
                    open={openValidateModal}
                    maxWidth='xs'
                    aria-labelledby="form-send-legajo">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-send-legajo" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                Validar Legajo
                            </Typography>
                        </DialogTitle>
                    </AppBar>
                    <form onSubmit={this.handleSubmitValidateLegajo(row)}>
                        <DialogContent>
                            <Typography align="center">
                                ¿Desea registrar el resultado de la validación del legajo para la solicitud: <strong> {row.cod_solicitud_completa}</strong>?
                            </Typography>
                            <br />
                            <Typography align="justify">
                                Si es conforme, seleccione <strong>"Confirmar"</strong>. Si se presenta observaciones, seleccione el botón <strong>"Observar"</strong>.
                            </Typography>
                        </DialogContent>
                        <DialogActions>
                            <Grid container >
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            disabled={validateButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickObservationToggleModal}
                                            margin="normal">
                                            Observar
                                            <VisibilityIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={validateButtonDisabled}
                                            className={classNames(classes.button)}
                                            type="submit" 
                                            margin="normal"
                                            color="primary" 
                                            size="small"
                                            fullWidth>
                                            Confirmar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                               validateButtonLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  

                <Dialog
                    open={openObservationModal}
                    maxWidth='xs'
                    aria-labelledby="form-observation-legajo">
                    <AppBar className={classes.appBar}>
                        <DialogTitle id="form-observation-legajo" disableTypography>
                            <Typography align="center" component="span" variant="h6" color="inherit" className="text-shadow-black">
                                Observaciones del Legajo
                            </Typography>
                        </DialogTitle>
                    </AppBar>

                    <form onSubmit={this.handleSubmitObservationLegajo(row)}>
                        <DialogContent>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    autoFocus
                                    fullWidth
                                    multiline
                                    rows="4"
                                    label="Observaciones"
                                    type="text"
                                    margin="normal"
                                    color="default"
                                    name="observation"
                                    variant="outlined"
                                    value={observation}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    InputProps={{
                                        inputProps:{
                                            maxLength: 300,
                                        },
                                    }}
                                    onChange={this.handleChangeTextFieldRequired("observation")}
                                />
                                <FormHelperText className="d-flex justify-content-end">
                                    {observation.toString().length}/300
                                </FormHelperText>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Grid container spacing={8}>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button 
                                            disabled={observationButtonDisabled}
                                            className={classNames(classes.button)}
                                            fullWidth
                                            color="secondary"
                                            size="small" 
                                            onClick={this.handleClickObservationToggleModal}
                                            margin="normal">
                                            Cancelar
                                            <CloseIcon fontSize="small" className="ml-2" />
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className={classNames(classes.buttonProgressWrapper)}>
                                        <Button
                                            disabled={observationButtonDisabled}
                                            className={classNames(classes.button)}
                                            type="submit" 
                                            margin="normal"
                                            color="primary" 
                                            size="small"
                                            fullWidth>
                                            Aceptar
                                            <SendIcon fontSize="small" className="ml-2" />
                                            {
                                               observationButtonLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                                            }
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </form>
                </Dialog>  
            </React.Fragment>
        )        
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(ValidateLegajoActionButton)));
