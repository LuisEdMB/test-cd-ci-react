import React from "react";
import * as moment from "moment";
import { Table } from '@devexpress/dx-react-grid-material-ui';
import ValidateLegajoActionButton from '../action-button/ValidateLegajoActionButton';
import NewCommentForm from '../../../../../../components/NewCommentForm/NewCommentForm';


const MyField = (props) => (
    <span style={{color: props.fontColor , padding: '.2em .5em', backgroundColor: props.bg, borderRadius: '.2em'}}>
        {props.value}
    </span>
)

const  Cell = ({...props}) => {
    if(props.column.name === "fec_reg"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    if(props.column.name === "fec_reg_emboce"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    if(props.column.name === "fec_reg_act"){
        const value = props.value? moment(props.value.replace("T", " ")).format('DD/MM/YYYY HH:mm') : "";
        return <Table.Cell {...props} value={value} />
    }
    if(props.column.name === 'des_flujo_fase_estado_dig'){
        return <Table.Cell {...props} value={<MyField  bg={props.row.des_color_dig} value={props.value} fontColor={props.row.cod_flujo_fase_estado_dig === 50002 || props.row.cod_flujo_fase_estado_dig === 80002 ?  '#ffffff' : '#363636'} />} />
    }
    if(props.column.name === "validate"){
        return <Table.Cell {...props}>
            <ValidateLegajoActionButton {...props} disabled={!(props.row.cod_flujo_fase_estado_dig === 50002 || props.row.cod_flujo_fase_estado_dig === 80002)}/>
        </Table.Cell>

    }
    if(props.column.name === "observation"){
        return <Table.Cell {...props}>
            <NewCommentForm {...props}/>
        </Table.Cell>
    }
    return <Table.Cell {...props} />
}

export default Cell;
