import React from "react";
import { withStyles, Typography } from "@material-ui/core";

const styles = theme => ({
    fontSizeDefault: {
        fontSize:12
    },
});

const RowDetail = ({ row, classes }) => (
    <ul>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Cliente:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.des_nombre_completo }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Tipo Operación:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_tipo_operacion }
            </Typography>
        </li>

        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Usuario:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_usu_reg }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Agencia:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.des_agencia }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.fec_reg }
            </Typography>
        </li>

        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Usuario Emboce:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_usu_reg_emboce }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Agencia Emboce:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                {  row.des_agencia_emboce }
            </Typography>
        </li>
        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Fecha Emboce:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.fec_reg_emboce }
            </Typography>
        </li>

        <li className="mb-2">
            <Typography className={classes.fontSizeDefault}>
                Flujo - Fase - Estado:
            </Typography>
            <Typography className={classes.fontSizeDefault} color="textSecondary">
                { row.des_jerarquias_flujo_fase_estado }
            </Typography>
        </li>
    </ul>
);

export default withStyles(styles)(RowDetail);

