import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; //{ isWidthUp, isWidthDown }
// React Router 
import { connect } from 'react-redux';
// Component
import{ 
    IconButton,
    Grid, 
    LinearProgress,
    Divider
} from '@material-ui/core';
// Icons
import CloseIcon from '@material-ui/icons/Close'; 
// Components Custom 
import DevGridComponent from '../../../../components/dev-grid/DevGridComponent';
import Cell from './components/table/Cell';
import RowDetail from './components/table/RowDetail';
import HeaderReport from '../../../../components/header-report/HeaderReport';

// Actions 
import { inboxReceived } from '../../../../actions/odc-digitization/odc-digitization';
import { bindActionCreators } from "redux";
const styles = theme => ({
    avatar:{
        width:80, 
        height:48
    },
    modalRoot:{
       
    },
    button: {
        textTransform: 'none',
    },
});

const mapStateToProps = (state, props) => {
    return {
        odcDigitization : state.odcDigitizationReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        inboxReceived: bindActionCreators(inboxReceived, dispatch)
    };
    return actions;
}

class ReceiveLegajoPage extends Component {

    state = {
        defaultColumnWidths: [
            { columnName: 'cod_solicitud_completa', width: 125 },
            { columnName: 'des_tipo_documento', width: 130 },
            { columnName: 'des_nro_documento', width: 125 },
            { columnName: 'des_nombre_completo', width: 150 },
            { columnName: 'des_tipo_operacion', width: 130 },
            { columnName: 'des_usu_reg', width: 135 },
            { columnName: 'des_agencia', width: 135 },
            { columnName: 'fec_reg', width: 130 },
            { columnName: 'des_usu_reg_emboce', width: 135 },
            { columnName: 'des_agencia_emboce', width: 135 },
            { columnName: 'fec_reg_emboce', width: 130 },
            // { columnName: 'des_observacion_legajo', width: 150 },
            { columnName: 'des_usu_act', width: 150 },
            { columnName: 'des_age_act', width: 150 },
            { columnName: 'fec_reg_act', width: 150 },
            { columnName: 'num_tarjeta_pmp', width: 150 },
            { columnName: 'num_cuenta_pmp', width: 150 },
            { columnName: 'num_cuenta_lp', width: 150 },
            { columnName: 'des_flujo_fase_estado_dig', width: 150 },

            { columnName: 'des_nombre_producto', width: 200 },
            { columnName: 'validate', width: 120 },
            { columnName: 'observation', width: 150 },
            { columnName: 'observation', width: 150 },
        ],
        columns:[],
        tableColumnExtensions: [
            { columnName: 'des_tipo_documento', align: 'center' },
            { columnName: 'validate', align: 'center' },
        ],
        defaultHiddenColumnNames: [],
        rows: [],
        rowsExcel: [],
        additional: 0, 
        reprint: 0, 
        express: 0,
        pageSizes: [5, 10, 15, 20, 0],
        columnExcel: [],
        showHideSearchInput: false, 
    };
    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.width !== this.props.width) {
            let columns = this.getColumnsRender();
            this.setState(state => ({
                ...state,
                columns: columns
            }));
        }

        if (prevProps.odcDigitization.inboxReceived !== this.props.odcDigitization.inboxReceived) {	
            
            if(this.props.odcDigitization.inboxReceived.loading){
                this.setState(state => ({
                    ...state, 
                    rows: [], 
                    columnExcel: [],
                    rowsExcel: []
                }));
            }

            if(!this.props.odcDigitization.inboxReceived.loading && 
                this.props.odcDigitization.inboxReceived.response && 
                this.props.odcDigitization.inboxReceived.success){
                    let { data } = this.props.odcDigitization.inboxReceived;  
                    let { solicitudesRecepcionarLegajo, solicitudesRecepcionarLegajoExcel } = data;

                    if(solicitudesRecepcionarLegajo && solicitudesRecepcionarLegajoExcel){
                        // Label                
                        let columnExcel = Object.keys(solicitudesRecepcionarLegajoExcel.length > 0 ? solicitudesRecepcionarLegajoExcel[0]: []);

                        columnExcel = columnExcel.filter(item => item !== 'Nro. Cuenta LP').map((item, index) => {
                            return {value: item, label: item}
                        });

                        let rowsExcel = solicitudesRecepcionarLegajoExcel.length > 0 ? solicitudesRecepcionarLegajoExcel : [];

                        this.setState(state => ({
                            ...state, 
                            rows: solicitudesRecepcionarLegajo, 
                            additional: this.props.odcDigitization.inboxReceived.data.contador_additional, 
                            reprint: this.props.odcDigitization.inboxReceived.data.contador_reimpresiones, 
                            express: this.props.odcDigitization.inboxReceived.data.contador_express,
                            columnExcel: columnExcel,
                            rowsExcel: rowsExcel 
                        }));
                    }
            }
            else if(!this.props.odcDigitization.inboxReceived.loading && 
                     this.props.odcDigitization.inboxReceived.response && 
                    !this.props.odcDigitization.inboxReceived.success){
                // Notistack
                this.getNotistack(this.props.odcDigitization.inboxReceived.error, "error");
            }
            else if(!this.props.odcDigitization.inboxReceived.loading && 
                    !this.props.odcDigitization.inboxReceived.response && 
                    !this.props.odcDigitization.inboxReceived.success){
                // Notistack
                this.getNotistack(this.props.odcDigitization.inboxReceived.error, "error");
            }
        }

        if(prevProps.odcDigitization.receivedLegajo !== this.props.odcDigitization.receivedLegajo){
            if(!this.props.odcDigitization.receivedLegajo.loading && 
                this.props.odcDigitization.receivedLegajo.response && 
                this.props.odcDigitization.receivedLegajo.success){
                this.props.inboxReceived("", "");
            }
            else if(!this.props.odcDigitization.receivedLegajo.loading && 
                     this.props.odcDigitization.receivedLegajo.response && 
                    !this.props.odcDigitization.receivedLegajo.success){
                this.getNotistack(this.props.odcDigitization.receivedLegajo.error, "error");
            }
            else if(!this.props.odcDigitization.receivedLegajo.loading && 
                    !this.props.odcDigitization.receivedLegajo.response && 
                    !this.props.odcDigitization.receivedLegajo.success){
                this.getNotistack(this.props.odcDigitization.receivedLegajo.error, "error");
            }
        }

        if(prevProps.odcDigitization.observeLegajo !== this.props.odcDigitization.observeLegajo){
            if(!this.props.odcDigitization.observeLegajo.loading && 
                this.props.odcDigitization.observeLegajo.response && 
                this.props.odcDigitization.observeLegajo.success){
                this.props.inboxReceived("", "");
            }
            else if (!this.props.odcDigitization.observeLegajo.loading &&
                     this.props.odcDigitization.observeLegajo.response &&
                    !this.props.odcDigitization.observeLegajo.success) {
                this.getNotistack(this.props.odcDigitization.observeLegajo.error, "error");
            }
            else if (!this.props.odcDigitization.observeLegajo.loading &&
                     !this.props.odcDigitization.observeLegajo.response &&
                     !this.props.odcDigitization.observeLegajo.success) {
                this.getNotistack(this.props.odcDigitization.observeLegajo.error, "error");
            }
        }
    }

    componentWillMount(){
        let columns = this.getColumnsRender();
        this.setState(state => ({
            ...state,
            columns:columns
        }));
    }
    componentDidMount(){
        this.props.inboxReceived("", "");
    }
    
    getColumnsRender(){
        let columns = [];
        if(this.props.width === "xs"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'validate', title: 'Validar Legajo' }
            ];
        }
        else if(this.props.width === "sm"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Nro Documento' },
                { name: 'validate', title: 'Validar Legajo' }
            ]
        }
        else if(this.props.width === "md"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Nro Documento ' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'validate', title: 'Validar Legajo' }
            ]
        }
        else if(this.props.width === "lg"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'des_tipo_operacion', title: 'Tipo Operación' },
                { name: 'des_nombre_producto', title: 'Tipo Tarjeta' },

                { name: 'des_usu_reg', title: 'Usuario Creador' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_usu_reg_emboce', title: 'Usuario Emboce' },
                { name: 'des_agencia_emboce', title: 'Agencia Emboce' },
                { name: 'fec_reg_emboce', title: 'Fecha Emboce' },
                // { name: 'des_observacion_legajo', title: "Observación" },
                { name: 'des_usu_act', title: "Usuario Activación" },
                { name: 'des_age_act', title: "Agencia Activación" },
                { name: 'fec_reg_act', title: "Fecha Activación" },
                { name: 'num_tarjeta_pmp', title: "Nro. Tarjeta" },
                { name: 'num_cuenta_pmp', title: "Nro. Cta." },
                { name: 'des_flujo_fase_estado_dig', title: 'Estado' },
                { name: 'validate', title: 'Validar Legajo' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        else if(this.props.width === "xl"){
            columns = [
                { name: 'cod_solicitud_completa', title: 'Solicitud' },
                { name: 'des_tipo_documento', title: 'Tipo Documento' },
                { name: 'des_nro_documento', title: 'Nro Documento' },
                { name: 'des_nombre_completo', title: 'Cliente' },
                { name: 'des_tipo_operacion', title: 'Tipo Operación' },
                { name: 'des_nombre_producto', title: 'Tipo Tarjeta' },

                { name: 'des_usu_reg', title: 'Usuario Creador' },
                { name: 'des_agencia', title: 'Agencia Registro' },
                { name: 'fec_reg', title: 'Fecha Registro' },
                { name: 'des_usu_reg_emboce', title: 'Usuario Emboce' },
                { name: 'des_agencia_emboce', title: 'Agencia Emboce' },
                { name: 'fec_reg_emboce', title: 'Fecha Emboce' },
                // { name: 'des_observacion_legajo', title: "Observación" },
                { name: 'des_usu_act', title: "Usuario Activación" },
                { name: 'des_age_act', title: "Agencia Activación" },
                { name: 'fec_reg_act', title: "Fecha Activación" },
                { name: 'num_tarjeta_pmp', title: "Nro. Tarjeta" },
                { name: 'num_cuenta_pmp', title: "Nro. Cta." },
                { name: 'des_flujo_fase_estado_dig', title: 'Estado' },
                { name: 'validate', title: 'Validar Legajo' },
                { name: 'observation', title: 'Ver Comentario' },
            ]
        }
        return columns;
    }

    // Notistack 
    getNotistack(message, variant="default", duration = 6000){
        let select = "default";
        switch(variant){
            case "error": 
                select = variant; break;
            case "success":
                select = variant; break;
            case "warning":
                select = variant; break;
            case "info":
                select = variant; break;
            default: 
                select = variant; break;
        }
        // Notistack
        this.props.enqueueSnackbar(message, {
            variant: select,
            autoHideDuration: duration,
            action: (
                <IconButton>
                    <CloseIcon size="small" className="text-white" color="inherit"/>
                </IconButton>
            ),
        });
    }
    render() {
        const { width, odcDigitization } = this.props;
        const { 
                columns,
                tableColumnExtensions,
                defaultColumnWidths, 
                defaultHiddenColumnNames, 
                rows, 
                columnExcel,
                rowsExcel
            } = this.state;
        
        return (
                <Grid container className="p-1">
                    <Grid item xs={12}>
                        {
                            odcDigitization.inboxReceived.loading ?  <LinearProgress /> : ""
                        }
                    </Grid>
                    <Grid item xs={12}>
                        {/* <Header 
                            title="Bandeja de Recepción de Legajo" 
                            additional={additional}
                            reprint={reprint}
                            express={express}
                            excel={{
                                workBook: [
                                    {   sheet:{
                                            data: rowsExcel,
                                            columns: columnExcel,
                                            nameSheet: "ODC - Solicitudes"
                                        }
                                    }
                                ],
                                nameFile: "Datos exportados"
                            }}
                        /> */}

                        <HeaderReport 
                            title={"Bandeja de Recepción de Legajo" }
                            columnExcel={columnExcel} 
                            dataExcel={rowsExcel}
                            data={rows}
                            nameSheet={"Reporte de Recepción de Legajo"} 
                            nameFile={"Datos exportados"}
                        
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Divider className="mb-2"/>  
                    </Grid>
                    <Grid item xs={12}>
                        <DevGridComponent 
                            rows={rows}
                            columns={columns}
                            width={width}
                            search={true}
                            columnExtensions={tableColumnExtensions}
                            defaultColumnWidths={defaultColumnWidths}
                            defaultHiddenColumnNames={defaultHiddenColumnNames}
                            RowDetailComponent={RowDetail}
                            CellComponent={Cell}/>
                    </Grid>
                </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(ReceiveLegajoPage))));

