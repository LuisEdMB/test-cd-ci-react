import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Switch, Redirect } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
import PrivateRoute from '../../utils/PrivateRoute';
import AdditionalPage from './additional/AdditionalPage';
import AdditionalPageExternal from './additional/AdditionalPageExternal';
import AuthorizePage from './additional/authorize/AuthorizePage';
import PendingPage from './additional/pending/PendingPage';

const mapStateToProps = state => {
    return {
        session: state.sessionReducer
    }
}

const AdditionalPageRoute = (props) => {
    let { match, session } = props;
    return( <Switch>
                <Redirect exact from={`${match.path}`} to={`${match.path}/nueva-solicitud`}/>
                <PrivateRoute 
                    exact
                    path={`${match.path}/nueva-solicitud`} 
                    component={ props => <SnackbarProvider max={ 3 }> <AdditionalPage { ...props }/> </SnackbarProvider> }
                    authenticated={session.authenticated}
                />
                 <PrivateRoute 
                    exact
                    path={`${match.path}/nueva-solicitud/:solicitud`} 
                    component={ props => <SnackbarProvider max={ 3 }> <AdditionalPage { ...props }/> </SnackbarProvider> }
                    authenticated={session.authenticated}
                />
                <PrivateRoute 
                    exact
                    path={`${match.path}/nueva-solicitud-ext`} 
                    component={ props => <SnackbarProvider max={ 3 }> <AdditionalPageExternal { ...props }/> </SnackbarProvider> }
                    authenticated={session.authenticated}
                />
                 <PrivateRoute 
                    exact
                    path={`${match.path}/nueva-solicitud-ext/:solicitud`} 
                    component={ props => <SnackbarProvider max={ 3 }> <AdditionalPageExternal { ...props }/> </SnackbarProvider> }
                    authenticated={session.authenticated}
                />
                <PrivateRoute 
                    path={`${match.path}/en-proceso`} 
                    component={(props) => <SnackbarProvider max={3} > <PendingPage {...props}/> </SnackbarProvider>}
                    authenticated={session.authenticated}
                />
                 <PrivateRoute 
                    path={`${match.path}/por-autorizar`} 
                    component={(props) => <SnackbarProvider max={3} > <AuthorizePage {...props}/> </SnackbarProvider>}
                    authenticated={session.authenticated}
                />
        </Switch>
    )
}

export default withRouter(connect(mapStateToProps)(AdditionalPageRoute));
