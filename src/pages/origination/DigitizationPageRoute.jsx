import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Switch, Redirect } from 'react-router-dom';
import PrivateRoute from '../../utils/PrivateRoute';
import { SnackbarProvider } from 'notistack';
import SubmitLegajoPage from './digitization/submit-legajo/SubmitLegajoPage';
import ReceiveLegajoPage from './digitization/receive-legajo/ReceiveLegajoPage';
import MonitoringPage from './digitization/monitoring/MonitoringPage';
import MonitoringPagePrototype from './digitization/monitoring-prototype/MonitoringPagePrototype';
const mapStateToProps = (state, props) => {
    return {
        session: state.sessionReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {

    };
    return actions;
}

const DigitizationPageRoute = (props) => {
    let { match, session } = props;
    return( <Switch>
                <Redirect exact from={`${match.path}`} to={`${match.path}/envio-legajo`}/>
                <PrivateRoute 
                    exact
                    path={`${match.path}/envio-legajo`} 
                    component={(props) => <SnackbarProvider max={3} > <SubmitLegajoPage {...props}/> </SnackbarProvider>}
                    authenticated={session.authenticated}
                />
                <PrivateRoute 
                    path={`${match.path}/recepcion-legajo`} 
                    component={(props) => <SnackbarProvider max={3} > <ReceiveLegajoPage {...props}/> </SnackbarProvider>}
                    authenticated={session.authenticated}
                />
                <PrivateRoute 
                    path={`${match.path}/monitoreo`} 
                    component={(props) => <SnackbarProvider max={3} > <MonitoringPage {...props}/> </SnackbarProvider>}
                    authenticated={session.authenticated}
                />
                <PrivateRoute 
                    path={`${match.path}/monitoreo-prototype`} 
                    component={(props) => <SnackbarProvider max={3} > <MonitoringPagePrototype {...props}/> </SnackbarProvider>}
                    authenticated={session.authenticated}
                />
        </Switch>
    )
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DigitizationPageRoute));
