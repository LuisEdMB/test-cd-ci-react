import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Switch, Redirect } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
import PrivateRoute from '../../utils/PrivateRoute';
import ReprintPage from './re-print/ReprintPage';
import ReprintPageExternal from './re-print/ReprintPageExternal';
import PendingPage from './re-print/pending/PendingPage';
import AuthorizePage from './re-print/authorize/AuthorizePage';
import SearchPage from './re-print/search/SearchPage';

const mapStateToProps = state => {
    return {
        session: state.sessionReducer
    }
}

const RePrintPageRoute = (props) => {
    let { match, session } = props;
    return(
        <Switch>
            <Redirect exact from={`${match.path}`} to={`${match.path}/nueva-solicitud`}/>
            <PrivateRoute 
                exact
                path={`${match.path}/nueva-solicitud`} 
                component={ props => <SnackbarProvider max={ 3 }> <ReprintPage { ...props }/> </SnackbarProvider> }
                authenticated={session.authenticated}
            />
             <PrivateRoute 
                exact
                path={`${match.path}/nueva-solicitud/:solicitud`} 
                component={ props => <SnackbarProvider max={ 3 }> <ReprintPage {...props }/> </SnackbarProvider> }
                authenticated={session.authenticated}
            />
            <PrivateRoute 
                exact
                path={`${match.path}/nueva-solicitud-ext`} 
                component={ props => <SnackbarProvider max={ 3 }> <ReprintPageExternal { ...props }/> </SnackbarProvider> }
                authenticated={session.authenticated}
            />
             <PrivateRoute 
                exact
                path={`${match.path}/nueva-solicitud-ext/:solicitud`} 
                component={ props => <SnackbarProvider max={ 3 }> <ReprintPageExternal {...props }/> </SnackbarProvider> }
                authenticated={session.authenticated}
            />
            <PrivateRoute 
                path={`${match.path}/en-proceso`} 
                component={(props) => <SnackbarProvider max={3} > <PendingPage {...props}/> </SnackbarProvider>}
                authenticated={session.authenticated}
            />
            <PrivateRoute 
                path={`${match.path}/por-autorizar`} 
                component={(props) => <SnackbarProvider max={3} > <AuthorizePage {...props}/> </SnackbarProvider>}
                authenticated={session.authenticated}
            />
            <PrivateRoute 
                path={`${match.path}/consultas`} 
                component={(props) => <SnackbarProvider max={3} > <SearchPage {...props}/> </SnackbarProvider>}
                authenticated={session.authenticated}
            /> 
        </Switch>
    )
}

export default withRouter(connect(mapStateToProps)(RePrintPageRoute));
