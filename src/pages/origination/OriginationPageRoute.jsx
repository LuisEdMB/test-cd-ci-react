import React from "react";
import { SnackbarProvider } from 'notistack';
import Drawer from '../../components/drawer/Drawer2';
import { withRouter, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PrivateRoute from '../../utils/PrivateRoute';
// Pages
import IndexPage from './index/IndexPage';
import SearchPage from './search/SearchPage';
import DashboardPage from './dashboard/DashboardPage';
import ErrorPage from '../error/ErrorPage';
// Modules 
import ExpressPageRoute from './ExpressPageRoute';
import RePrintPageRoute from './RePrintPageRoute';
import RegularPageRoute from './RegularPageRoute';
import AdditionalPageRoute from './AdditionalPageRoute';
import DigitizationPageRoute from './DigitizationPageRoute';
import ReportPageRoute from './ReportPageRoute';
import mantenimientoPageRoute from './MantenimientoPageRoute'
// Actions
import { logout } from '../../actions/session';

const mapStateToProps = (state, props) => {
    return {
        session: state.sessionReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
    const actions = {
        logout:bindActionCreators(logout, dispatch),
    };
    return actions;
}

const OriginationPageRoute = ({match, session, ...props}) => {
    const handleLogout = (data) => {
        props.logout()
    }
    return(
        <Drawer session={session} handleLogout={handleLogout}>
            <Switch>
                <PrivateRoute  
                    exact
                    path={`${match.path}/`} 
                    component={IndexPage}
                    authenticated={session.authenticated}
                />
                <PrivateRoute 
                    path={`${match.path}/express`}
                    component={ExpressPageRoute}
                    authenticated={session.authenticated}
                    />
                <PrivateRoute 
                    path={`${match.path}/regular`}
                    component={RegularPageRoute}
                    authenticated={session.authenticated}
                    />
                <PrivateRoute 
                    path={`${match.path}/re-impresion`}
                    component={RePrintPageRoute}
                    authenticated={session.authenticated}
                    />
                <PrivateRoute 
                    path={`${match.path}/adicional`}
                    component={AdditionalPageRoute}
                    authenticated={session.authenticated}
                    />
                <PrivateRoute 
                    path={`${match.path}/mantenimiento`}
                    component={mantenimientoPageRoute}
                    authenticated={session.authenticated}
                    />
                <PrivateRoute
                    path={`${match.path}/digitalizacion`}
                    component={DigitizationPageRoute}
                    authenticated={session.authenticated}
                    />
                <PrivateRoute 
                    path={`${match.path}/reportes`} 
                    component={(props) => <SnackbarProvider max={3}> <ReportPageRoute session={session}  {...props} /> </SnackbarProvider>}
                    authenticated={session.authenticated}
                    />
                <PrivateRoute 
                    path={`${match.path}/dashboard`} 
                    component={(props) => <SnackbarProvider max={3}> <DashboardPage session={session}  {...props} /> </SnackbarProvider>}
                    authenticated={session.authenticated} 
                    />
                <PrivateRoute 
                    path={`${match.path}/consultas`} 
                    component={(props) => <SnackbarProvider max={3}> <SearchPage session={session}  {...props} /> </SnackbarProvider>}
                    authenticated={session.authenticated}
                    />
                <PrivateRoute 
                    path={`${match.path}/error`} 
                    component={ErrorPage}
                    authenticated={session.authenticated}
                    />
            </Switch>
        </Drawer>
    )
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OriginationPageRoute));