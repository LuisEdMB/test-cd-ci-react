import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';

import { 
    Grid,
    Paper,
    Typography, 
    Divider} from '@material-ui/core';
import {
  SortingState,
} from '@devexpress/dx-react-grid';
import {
  Grid as GridDev,
  Table,
  TableHeaderRow,
} from '@devexpress/dx-react-grid-material-ui';

const styles = theme => ({
    root:{
        borderRadius:".5em",
        padding: "0 0 3em 0"
    }
});
class TableDashboard extends React.PureComponent {
    state = {
        columns: [
            { name: 'index', title: '#' },
            { name: 'agency', title: 'Agencia' },
            { name: 'quantity', title: 'Cantidad' },
        ],
        currencyColumns: ['SaleAmount'],
        tableColumnExtensions: [
            { columnName: 'index', align: 'right' },
            { columnName: 'agency', align: 'right' },
            { columnName: 'quantity', align: 'center' }
        ],
        rows: [
            { index: 1, agency: "Agencia 1", quantity: 450},
            { index: 2, agency: "Agencia 2", quantity: 300},
            { index: 3, agency: "Agencia 3", quantity: 250},
            { index: 4, agency: "Agencia 4", quantity: 220},
            { index: 5, agency: "Agencia 5", quantity: 190},
            { index: 6, agency: "Agencia 6", quantity: 180},
            { index: 7, agency: "Agencia 7", quantity: 156},
            { index: 8, agency: "Agencia 8", quantity: 140},
            { index: 9, agency: "Agencia 9", quantity: 120},
            { index: 10, agency: "Agencia 10", quantity: 100},
        ],
        sorting: [{ columnName: 'index', direction: 'asc' }],
        totalCount: 0,
        pageSize: 10,
        pageSizes: [5, 10, 15],
        currentPage: 0,
        loading: true,
    }

    changeSorting = (sorting)  => {
        this.setState({
            loading: true,
            sorting,
        });
    }


    render() {
        const {
            rows,
            columns,
            tableColumnExtensions,
            sorting
        } = this.state;
        let { classes } = this.props;
        return (
            <React.Fragment>
                <Paper className={classes.root}>
                    <Grid container spacing={8}>
                        <Grid item xs={12}>
                            <Typography variant="h6" align="center"> 
                                Top 10 Agencia 
                            </Typography>
                        </Grid>
                    <Grid item xs={12} className="mb-2">
                            <Divider />
                    </Grid>
                        <Grid item xs={12}>
                            <GridDev rows={rows} columns={columns}>
                                <SortingState sorting={sorting} onSortingChange={this.changeSorting}/>
                                <Table columnExtensions={tableColumnExtensions}/>
                                <TableHeaderRow showSortingControls />
                            </GridDev>
                        </Grid>
                    </Grid>
                </Paper>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(TableDashboard);