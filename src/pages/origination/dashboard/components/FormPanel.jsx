import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Autocomplete from '../../../../components/Autocomplete';
import {  
    Grid,
    Paper,
    Button,
    TextField, 
    InputAdornment,
    Typography,
} from '@material-ui/core';

import CalendarTodayIcon from '@material-ui/icons/CalendarToday'

const styles = theme => ({
    paper:{
        padding: 20,
        borderRadius:".5em",
        border:"1px solid #80808033",
    },
    buttonWrapper:{
        diplay:"center",
        alignItem:"center"
    }
});

const FormPanel = ({
    classes,
    form, 
    handleChangeTextFieldInitialDate,
    handleChangeTextFieldFinalDate
}) => (
    <Paper className={classes.paper}>
        <Typography component="h3" variant="h5">
            Dashboard:
        </Typography>
        <form method="post" autoComplete="off">
            <Grid container spacing={8}>
                <Grid item xs={12} sm={12} md={3} lg={3}>
                    <Autocomplete 
                        data={[
                            { label:"Agencia 1", value:1 },
                            { label:"Agencia 2", value:2 },
                            { label:"Agencia 3", value:3 },
                            { label:"Agencia 4", value:4 }
                        ]}
                        placeholder="Agencia"
                        value={form.agencyId}
                    />
                </Grid>
                <Grid item xs={6} sm={6} md={3} lg={2}>
                    <TextField
                        error={form.initialDateError}
                        fullWidth={true}
                        name="initialDate"
                        label="Fecha Inicial"
                        type="date"
                        margin="normal"
                        required
                        format="DD-MM-YYYY"
                        defaultValue={form.initialDate}
                        onChange={handleChangeTextFieldInitialDate}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{
                            inputProps:{
                                min:"2019-01-01",
                            },
                            endAdornment: (
                                <InputAdornment position="end">
                                    <CalendarTodayIcon color={form.startDateError? "secondary": "inherit"} fontSize="small" />
                                </InputAdornment>
                            )
                        }}
                    />
                </Grid>  
                <Grid item xs={6} sm={6} md={3} lg={2}>
                    <TextField
                        error={form.finalDateError}
                        fullWidth={true}
                        name="finalDate"
                        label="Fecha Inicial"
                        type="date"
                        margin="normal"
                        required
                        format="DD-MM-YYYY"
                        defaultValue={form.finalDate}
                        onChange={handleChangeTextFieldFinalDate}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{
                            inputProps:{
                                min:"2019-01-01",
                            },
                            endAdornment: (
                                <InputAdornment position="end">
                                    <CalendarTodayIcon color={form.finalDateError? "secondary": "inherit"} fontSize="small" />
                                </InputAdornment>
                            )
                        }}
                    />
                </Grid>  
                <Grid item xs={6} sm={6} md={3} lg={3}>
                    <Autocomplete 
                        data={[
                            { label:"VISA", value:1 },
                            { label:"MASTERCARD", value:2 }
                        ]}
                        placeholder="Marca"
                        value={form.agencyId}
                    />
                </Grid>
                <Grid item xs={6} sm={6} md={3} lg={2} className={classes.buttonWrapper}>
                    <Button
                        fullWidth
                        variant="contained" 
                        color="primary">
                        Consultar
                    </Button>
                </Grid>
            </Grid>
        </form>
    </Paper>

)

FormPanel.defaultProps = {
    form:{
        agencyId:0,
        initialDateError:false,
        finalDateError:false,
        initialDate:"",
        finalDate:""
    }
};

export default withStyles(styles)(FormPanel);