import React from 'react';
import Highcharts from 'highcharts';
import {
  HighchartsChart, Chart, withHighcharts, XAxis, YAxis, Title, ColumnSeries, Legend, SplineSeries
} from 'react-jsx-highcharts';

const App = () => (
  <div className="app">
    <HighchartsChart >
        <Chart />

        <Title></Title>

        <Legend />

        <XAxis categories={['Agencia 1', 'Agencia 2', 'Agencia 3', 'Agencia 4', 'Agencia 5']} />

        <YAxis>
        <ColumnSeries name="MasterdCard" data={[3000, 2000, 1000, 2500, 3500]} color="orange"/>
        <ColumnSeries name="Visa" data={[2000, 3000, 5000, 4000, 3000]} color="rgb(0, 122, 184)"/>
        <SplineSeries name="Promedio" data={[3300, 2670, 3000, 4500, 2500]} />
        </YAxis>
    </HighchartsChart>
  </div>
);

export default withHighcharts(App, Highcharts);