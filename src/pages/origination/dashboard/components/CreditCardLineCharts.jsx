import React from 'react';
import Highcharts from 'highcharts';
import {
  HighchartsChart, Chart, withHighcharts, XAxis, YAxis, Title, Subtitle, Legend, LineSeries
} from 'react-jsx-highcharts';

const plotOptions = {
  series: {
    pointStart: 2019
  }
};

const App = () => (
  <div className="app">
    <HighchartsChart plotOptions={plotOptions}>
      <Chart />

      <Title>Solar Employment Growth by Sector, 2010-2016</Title>

      <Subtitle>Source: thesolarfoundation.com</Subtitle>

      <Legend layout="vertical" align="right" verticalAlign="middle" />

      <XAxis>
        <XAxis.Title>Time</XAxis.Title>
      </XAxis>

      <YAxis>
        <YAxis.Title>Number of employees</YAxis.Title>
        <LineSeries name="Entregados" data={[43934,]} />
        <LineSeries name="En Proceso" data={[null, ]} />
        <LineSeries name="Pendientes" data={[24916,]} />
        <LineSeries name="Cancelados" data={[11744,]} />
      </YAxis>
    </HighchartsChart>
  </div>
);

export default withHighcharts(App, Highcharts);