import React from 'react';
import Highcharts from 'highcharts';
import {
  HighchartsChart, Chart, withHighcharts, XAxis, YAxis, Title, ColumnSeries, Legend, 
   SplineSeries
   //PieSeries,
} from 'react-jsx-highcharts';

const App = () => (
  <div className="app">
    <HighchartsChart >
        <Chart />

        <Title></Title>

        <Legend />

        <XAxis categories={['Agencia 1', 'Agencia 2', 'Agencia 3', 'Agencia 4', 'Agencia 5']} />

        <YAxis>
        <ColumnSeries name="Clásica" data={[3000, 2000, 1000, 2500, 3500]} color="#BDBDBD"/>
        <ColumnSeries name="Gold" data={[2000, 3000, 5000, 4000, 3000]} color="#FFEB3B"/>
        <ColumnSeries name="Black" data={[4000, 3000, 3000, 4500, 2000]} color="rgba(0, 0, 0, 0.72)" />
        <SplineSeries name="Promedio" data={[3300, 2670, 3000, 4500, 2500]} />
        </YAxis>
    </HighchartsChart>
  </div>
);

export default withHighcharts(App, Highcharts);