import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import withWidth from '@material-ui/core/withWidth'; 
import {
    Pie, 
    //Line, 
    HorizontalBar
} from 'react-chartjs-2';
//Components Custom
import FormPanel from './components/FormPanel';
import CreditCardGeneratedBarCharts from './components/CreditCardGeneratedBarCharts';
import CreditCardBrandBarCharts from './components/CreditCardBrandBarCharts';
import CreditCardLineCharts from './components/CreditCardLineCharts';
//Icon 
import ErrorIcon from '@material-ui/icons/Error'
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline'
import HistoryIcon from '@material-ui/icons/History';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled'
// React Router 
//import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//import classNames from 'classnames';
//import IconButton from '@material-ui/core/IconButton';

import Grid from '@material-ui/core/Grid';
//import CencosudScotiaBank from '../../../assets/media/images/jpg/Cencosud-Scotiabank-1.jpeg';
// Components Custom 
import CardSummary from '../../../components/CardSummary';
import { Typography, Paper } from "@material-ui/core";

const styles = theme => ({
    paper:{
        padding: 20,
        borderRadius:".5em",
        border:"1px solid #80808033",
    },
    searchInput:{
        width: "100%",
        [theme.breakpoints.down('sm')]: {
            
        },
        [theme.breakpoints.up('md')]: {
        },
        [theme.breakpoints.up('lg')]: {
            width: 320,
        },
    },
    avatar:{
        width:80, 
        height:48
    },
    pieWrapper:{
        height:350,
        width:350,
    }
});


const mapStateToProps = (state, props) => {
    return {
        session: state.sessionReducer
    }
}
const mapDispatchToProps = (dispatch, props) => {
  const actions = {
  
  };
  return actions;
}


class DashboardPage extends Component {
    render(){
        let { classes } = this.props;
        let pie = {
            data:[3000, 1050],
            total: 750
        }
        const pieData = {
            labels: [
                'VISA (%)',
                'MasterCard(%)'
            ],
            datasets: [{
                data: pie.data,
                backgroundColor: [
                    '#FFCE56',
                    '#36A2EB'
                ],
                hoverBackgroundColor: [
                    '#FFCE56',
                    '#36A2EB'
                ]
            }]
        };
        /*
        const LineData = {
            labels: [
                'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 
                'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
            ],
            datasets: [
              {
                label: 'Ventas x Mes',
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: '#36A2EB',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: [1500, 2300, 3500, 2200, 4500],
              }
            ]
        };*/
        const HBardata1 = {
            labels: ['Agencia 1', 'Agencia 2', 'Agencia 3', 'Agencia 4', 'Agencia 5'],
            datasets: [
                {
                    label: 'Ventas TC x Agencia',
                    backgroundColor: '#ffce56',
                    borderColor: 'orange',
                    borderWidth: 1,
                    hoverBackgroundColor: 'orange',
                    hoverBorderColor: 'orange',
                    data: [6500, 5900, 8000, 8100, 5600, 5500, 4000],
                }
            ]
        };
        const HBardata2 = {
            labels: ['Clásica - Naranja', 'Clásica - Azul', 'Clásica - Gris', 'Gold', 'Black'],
            datasets: [
                {
                    label: 'Ventas x TC',
                    backgroundColor: ['orange', 'rgb(0, 122, 184)', "#BDBDBD", "#FFEB3B", "rgba(0, 0, 0, 0.72)"],
                    borderColor: ['orange', 'rgb(0, 122, 184)', "#BDBDBD", "#FFEB3B", "rgba(0, 0, 0, 0.72)"],
                    borderWidth: 1,
                    hoverBackgroundColor: ['#ffa50057', 'rgba(0, 122, 184, 0.38)', "#bdbdbd42", "rgba(255, 235, 59, 0.36)", "rgba(0, 0, 0, 0.3)"], //'#00BCD4',
                    hoverBorderColor: ['orange', 'rgb(0, 122, 184)', "#BDBDBD", "#FFEB3B", "rgba(0, 0, 0, 0.72)"],
                    data: [6500, 5900, 8000, 8100, 5600, 5500, 4000]
                }
            ]
        };
        const HBardata3 = {
            labels: ['Clásica', 'Gold', 'Black'],
            datasets: [
                {
                    label: 'Línea de Crédito',
                    backgroundColor: ["#BDBDBD", "#FFEB3B", "rgba(0, 0, 0, 0.72)"],
                    borderColor: ["#BDBDBD", "#FFEB3B", "rgba(0, 0, 0, 0.72)"],
                    borderWidth: 1,
                    hoverBackgroundColor: ["#bdbdbd42", "rgba(255, 235, 59, 0.36)", "rgba(0, 0, 0, 0.3)"], //'#00BCD4',
                    hoverBorderColor: ["#BDBDBD", "#FFEB3B", "rgba(0, 0, 0, 0.72)"],
                    data: [6500, 5900, 8000, 8100, 5600, 5500, 4000]
                }
            ]
        };
        return (
           <Grid container spacing={8} className="p-2">
                <Grid item xs={12}>
                    <Grid container spacing={8}>
                        <Grid item xs={12} md={6} lg={3}>
                            <CardSummary 
                                icon={<CheckCircleOutlineIcon style={{fontSize:36}} />} 
                                total={150} 
                                color="green" 
                                title="Habilitadas" 
                                description="Tarjetas entregadas"/>
                        </Grid>
                        <Grid item xs={12} md={6} lg={3} >
                            <CardSummary 
                                icon={<PlayCircleFilledIcon style={{fontSize:36}} />} 
                                total={200} 
                                color="#36A2EB" 
                                title="En Proceso" 
                                description="Tarjetas en proceso"/>
                        </Grid>
                        <Grid item xs={12} md={6} lg={3} >
                            <CardSummary 
                                icon={<HistoryIcon style={{fontSize:36}} />} 
                                total={100} 
                                color="orange" 
                                title="Pendientes" 
                                description="Tarjetas pendientes de activación"/>
                        </Grid>
                        
                        <Grid item xs={12} md={6} lg={3}>
                            <CardSummary  
                                icon={<ErrorIcon style={{fontSize:36}} />} 
                                total={300} 
                                color="red" 
                                title="Canceladas" 
                                description="Tarjetas Canceladas"/>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <FormPanel />
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={8}>
                        <Grid item xs={12} md={4} lg={4}>
                            <Paper className={classes.paper}>
                                <Typography variant="h6" align="center"> 
                                        Marca TC (%)
                                </Typography>
                                <Pie 
                                    options={
                                    {
                                        legend: {
                                            display: true,
                                            position: "bottom"
                                        }
                                    }
                                    }
                                    data={pieData} />
                            </Paper>    
                        </Grid>
                        <Grid item xs={12} md={4} lg={4}>
                            <Paper className={classes.paper}>
                                <Typography variant="h6" align="center"> 
                                    TC Generadas
                                </Typography>
                                <HorizontalBar 
                                    options={{
                                        legend: {
                                            display: false,
                                        },
                                        scales: {
                                            xAxes: [{
                                                stacked: true,
                                                ticks: {
                                                    beginAtZero:true,
                                                    min: 0,
                                                    max: 10000    
                                                }
                                            }]
                                        }
                                    }}
                                    data={HBardata2} />
                            </Paper>          
                        </Grid>
                        <Grid item xs={12} md={4} lg={4}>
                            <Paper className={classes.paper}>
                                <Typography variant="h6" align="center"> 
                                    Total Línea Credito 
                                </Typography>
                                <HorizontalBar 
                                    options={{
                                        legend: {
                                            display: false,
                                        },
                                        scales: {
                                            xAxes: [{
                                                stacked: true,
                                                ticks: {
                                                    beginAtZero:true,
                                                    min: 0,
                                                    max: 10000    
                                                }
                                            }]
                                        }
                                    }}
                                    data={HBardata3} />
                            </Paper>          
                        </Grid>
                    </Grid>
                </Grid>
               
                            
              
                        

                <Grid item xs={12} md={12} lg={12}>
                    <Grid container>
                        <Grid item xs={12}>
                            <CreditCardLineCharts />
                        </Grid>
                    </Grid>                   
                    
                </Grid>

                <Grid item xs={12}>
                    <Grid container spacing={8}>
                        <Grid item xs={12} md={6} lg={4}>
                            <Paper className={classes.paper}>
                                <Typography variant="h6" align="center"> 
                                    Top 5 - TC Generadas x Agencia 
                                </Typography>
                                <CreditCardGeneratedBarCharts />
                            </Paper>
                        </Grid>
                        <Grid item xs={12} md={6} lg={4}>
                            <Paper className={classes.paper}>
                                <Typography variant="h6" align="center"> 
                                    Top 5 - TC Marcas Generadas x Agencia
                                </Typography>
                                <CreditCardBrandBarCharts />
                            </Paper>
                        </Grid>
                        <Grid item xs={12} md={6} lg={4}>
                            <Paper className={classes.paper}>
                                <Typography variant="h6" align="center"> 
                                    Top 5 - TC Generadas x Agencia
                                </Typography>
                                <HorizontalBar 
                                    options={{
                                        legend: {
                                            display: false,
                                        },
                                        scales: {
                                            xAxes: [{
                                                stacked: true,
                                                ticks: {
                                                    beginAtZero:true,
                                                    min: 0,
                                                    max: 10000    
                                                }
                                            }]
                                        }
                                    }}
                                    data={HBardata1} />
                            </Paper>          
                        </Grid>
                    </Grid>
                </Grid>
         
           </Grid>
        );
    }
}

export default withStyles(styles)(withSnackbar(connect(mapStateToProps, mapDispatchToProps)(withWidth()(DashboardPage))));

